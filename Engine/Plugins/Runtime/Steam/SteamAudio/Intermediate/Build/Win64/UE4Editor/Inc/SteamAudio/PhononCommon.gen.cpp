// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Private/PhononCommon.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononCommon() {}
// Cross Module References
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplAudioEngine();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplRayTracerType();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplConvolutionType();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplSimulationType();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMode();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMethod();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplHrtfInterpolationMethod();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplSpatializationMethod();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EQualitySettings();
// End Cross Module References
	static UEnum* EIplAudioEngine_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplAudioEngine, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplAudioEngine"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplAudioEngine>()
	{
		return EIplAudioEngine_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplAudioEngine(EIplAudioEngine_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplAudioEngine"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplAudioEngine_Hash() { return 3141857809U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplAudioEngine()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplAudioEngine"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplAudioEngine_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplAudioEngine::UNREAL", (int64)EIplAudioEngine::UNREAL },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "UNREAL.Comment", "// Native Unreal audio engine.\n" },
				{ "UNREAL.DisplayName", "Unreal" },
				{ "UNREAL.Name", "EIplAudioEngine::UNREAL" },
				{ "UNREAL.ToolTip", "Native Unreal audio engine." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplAudioEngine",
				"EIplAudioEngine",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EIplRayTracerType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplRayTracerType, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplRayTracerType"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplRayTracerType>()
	{
		return EIplRayTracerType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplRayTracerType(EIplRayTracerType_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplRayTracerType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplRayTracerType_Hash() { return 2126063413U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplRayTracerType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplRayTracerType"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplRayTracerType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplRayTracerType::PHONON", (int64)EIplRayTracerType::PHONON },
				{ "EIplRayTracerType::EMBREE", (int64)EIplRayTracerType::EMBREE },
				{ "EIplRayTracerType::RADEONRAYS", (int64)EIplRayTracerType::RADEONRAYS },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "EMBREE.Comment", "// Intel Embree Ray Tracer\n" },
				{ "EMBREE.DisplayName", "Intel Embree" },
				{ "EMBREE.Name", "EIplRayTracerType::EMBREE" },
				{ "EMBREE.ToolTip", "Intel Embree Ray Tracer" },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "PHONON.Comment", "// Default Ray Tracer\n" },
				{ "PHONON.DisplayName", "Phonon" },
				{ "PHONON.Name", "EIplRayTracerType::PHONON" },
				{ "PHONON.ToolTip", "Default Ray Tracer" },
				{ "RADEONRAYS.Comment", "// AMD Radeon Rays ray tracer, implemented in OpenCL for both CPU and GPU.\n" },
				{ "RADEONRAYS.DisplayName", "AMD Radeon Rays" },
				{ "RADEONRAYS.Name", "EIplRayTracerType::RADEONRAYS" },
				{ "RADEONRAYS.ToolTip", "AMD Radeon Rays ray tracer, implemented in OpenCL for both CPU and GPU." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplRayTracerType",
				"EIplRayTracerType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EIplConvolutionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplConvolutionType, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplConvolutionType"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplConvolutionType>()
	{
		return EIplConvolutionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplConvolutionType(EIplConvolutionType_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplConvolutionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplConvolutionType_Hash() { return 2212093948U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplConvolutionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplConvolutionType"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplConvolutionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplConvolutionType::PHONON", (int64)EIplConvolutionType::PHONON },
				{ "EIplConvolutionType::TRUEAUDIONEXT", (int64)EIplConvolutionType::TRUEAUDIONEXT },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "PHONON.Comment", "// Default CPU convolution renderer.\n" },
				{ "PHONON.DisplayName", "Phonon" },
				{ "PHONON.Name", "EIplConvolutionType::PHONON" },
				{ "PHONON.ToolTip", "Default CPU convolution renderer." },
				{ "TRUEAUDIONEXT.Comment", "// AMD TrueAudio Next GPU convolution renderer.\n" },
				{ "TRUEAUDIONEXT.DisplayName", "AMD TrueAudio Next" },
				{ "TRUEAUDIONEXT.Name", "EIplConvolutionType::TRUEAUDIONEXT" },
				{ "TRUEAUDIONEXT.ToolTip", "AMD TrueAudio Next GPU convolution renderer." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplConvolutionType",
				"EIplConvolutionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EIplSimulationType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplSimulationType, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplSimulationType"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplSimulationType>()
	{
		return EIplSimulationType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplSimulationType(EIplSimulationType_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplSimulationType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplSimulationType_Hash() { return 1246087503U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplSimulationType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplSimulationType"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplSimulationType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplSimulationType::REALTIME", (int64)EIplSimulationType::REALTIME },
				{ "EIplSimulationType::BAKED", (int64)EIplSimulationType::BAKED },
				{ "EIplSimulationType::DISABLED", (int64)EIplSimulationType::DISABLED },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BAKED.Comment", "// Precompute indirect sound.\n" },
				{ "BAKED.DisplayName", "Baked" },
				{ "BAKED.Name", "EIplSimulationType::BAKED" },
				{ "BAKED.ToolTip", "Precompute indirect sound." },
				{ "BlueprintType", "true" },
				{ "DISABLED.Comment", "// Do not simulate indirect sound.\n" },
				{ "DISABLED.DisplayName", "Disabled" },
				{ "DISABLED.Name", "EIplSimulationType::DISABLED" },
				{ "DISABLED.ToolTip", "Do not simulate indirect sound." },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "REALTIME.Comment", "// Simulate indirect sound at run time.\n" },
				{ "REALTIME.DisplayName", "Real-Time" },
				{ "REALTIME.Name", "EIplSimulationType::REALTIME" },
				{ "REALTIME.ToolTip", "Simulate indirect sound at run time." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplSimulationType",
				"EIplSimulationType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EIplDirectOcclusionMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMode, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplDirectOcclusionMode"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplDirectOcclusionMode>()
	{
		return EIplDirectOcclusionMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplDirectOcclusionMode(EIplDirectOcclusionMode_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplDirectOcclusionMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMode_Hash() { return 556431179U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplDirectOcclusionMode"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplDirectOcclusionMode::NONE", (int64)EIplDirectOcclusionMode::NONE },
				{ "EIplDirectOcclusionMode::DIRECTOCCLUSION_NOTRANSMISSION", (int64)EIplDirectOcclusionMode::DIRECTOCCLUSION_NOTRANSMISSION },
				{ "EIplDirectOcclusionMode::DIRECTOCCLUSION_TRANSMISSIONBYVOLUME", (int64)EIplDirectOcclusionMode::DIRECTOCCLUSION_TRANSMISSIONBYVOLUME },
				{ "EIplDirectOcclusionMode::DIRECTOCCLUSION_TRANSMISSIONBYFREQUENCY", (int64)EIplDirectOcclusionMode::DIRECTOCCLUSION_TRANSMISSIONBYFREQUENCY },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "DIRECTOCCLUSION_NOTRANSMISSION.Comment", "// Perform occlusion checks but do not model transmission.\n" },
				{ "DIRECTOCCLUSION_NOTRANSMISSION.DisplayName", "Direct Occlusion, No Transmission" },
				{ "DIRECTOCCLUSION_NOTRANSMISSION.Name", "EIplDirectOcclusionMode::DIRECTOCCLUSION_NOTRANSMISSION" },
				{ "DIRECTOCCLUSION_NOTRANSMISSION.ToolTip", "Perform occlusion checks but do not model transmission." },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYFREQUENCY.Comment", "// Perform occlusion checks and model transmission; occluded sound will be rendered with a frequency-dependent transmission filter.\n" },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYFREQUENCY.DisplayName", "Direct Occlusion, Frequency-Dependent Transmission" },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYFREQUENCY.Name", "EIplDirectOcclusionMode::DIRECTOCCLUSION_TRANSMISSIONBYFREQUENCY" },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYFREQUENCY.ToolTip", "Perform occlusion checks and model transmission; occluded sound will be rendered with a frequency-dependent transmission filter." },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYVOLUME.Comment", "// Perform occlusion checks and model transmission; occluded sound will be scaled by a frequency-independent attenuation value.\n" },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYVOLUME.DisplayName", "Direct Occlusion, Frequency-Independent Transmission" },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYVOLUME.Name", "EIplDirectOcclusionMode::DIRECTOCCLUSION_TRANSMISSIONBYVOLUME" },
				{ "DIRECTOCCLUSION_TRANSMISSIONBYVOLUME.ToolTip", "Perform occlusion checks and model transmission; occluded sound will be scaled by a frequency-independent attenuation value." },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "NONE.Comment", "// Do not perform any occlusion checks.\n" },
				{ "NONE.DisplayName", "None" },
				{ "NONE.Name", "EIplDirectOcclusionMode::NONE" },
				{ "NONE.ToolTip", "Do not perform any occlusion checks." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplDirectOcclusionMode",
				"EIplDirectOcclusionMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EIplDirectOcclusionMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMethod, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplDirectOcclusionMethod"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplDirectOcclusionMethod>()
	{
		return EIplDirectOcclusionMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplDirectOcclusionMethod(EIplDirectOcclusionMethod_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplDirectOcclusionMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMethod_Hash() { return 2864095223U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplDirectOcclusionMethod"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplDirectOcclusionMethod::RAYCAST", (int64)EIplDirectOcclusionMethod::RAYCAST },
				{ "EIplDirectOcclusionMethod::VOLUMETRIC", (int64)EIplDirectOcclusionMethod::VOLUMETRIC },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "RAYCAST.Comment", "// Binary visible or not test. Adjusts direct volume accordingly.\n" },
				{ "RAYCAST.DisplayName", "Raycast" },
				{ "RAYCAST.Name", "EIplDirectOcclusionMethod::RAYCAST" },
				{ "RAYCAST.ToolTip", "Binary visible or not test. Adjusts direct volume accordingly." },
				{ "VOLUMETRIC.Comment", "// Treats the source as a sphere instead of a point. Smoothly ramps up volume as source becomes visible to listener.\n" },
				{ "VOLUMETRIC.DisplayName", "Partial" },
				{ "VOLUMETRIC.Name", "EIplDirectOcclusionMethod::VOLUMETRIC" },
				{ "VOLUMETRIC.ToolTip", "Treats the source as a sphere instead of a point. Smoothly ramps up volume as source becomes visible to listener." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplDirectOcclusionMethod",
				"EIplDirectOcclusionMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EIplHrtfInterpolationMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplHrtfInterpolationMethod, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplHrtfInterpolationMethod"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplHrtfInterpolationMethod>()
	{
		return EIplHrtfInterpolationMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplHrtfInterpolationMethod(EIplHrtfInterpolationMethod_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplHrtfInterpolationMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplHrtfInterpolationMethod_Hash() { return 2605084951U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplHrtfInterpolationMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplHrtfInterpolationMethod"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplHrtfInterpolationMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplHrtfInterpolationMethod::NEAREST", (int64)EIplHrtfInterpolationMethod::NEAREST },
				{ "EIplHrtfInterpolationMethod::BILINEAR", (int64)EIplHrtfInterpolationMethod::BILINEAR },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BILINEAR.Comment", "// Bilinearly interpolates the HRTF before processing. Slower, but can result in a smoother sound as the listener rotates.\n" },
				{ "BILINEAR.DisplayName", "Bilinear" },
				{ "BILINEAR.Name", "EIplHrtfInterpolationMethod::BILINEAR" },
				{ "BILINEAR.ToolTip", "Bilinearly interpolates the HRTF before processing. Slower, but can result in a smoother sound as the listener rotates." },
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "NEAREST.Comment", "// Uses a nearest neighbor lookup - fast.\n" },
				{ "NEAREST.DisplayName", "Nearest" },
				{ "NEAREST.Name", "EIplHrtfInterpolationMethod::NEAREST" },
				{ "NEAREST.ToolTip", "Uses a nearest neighbor lookup - fast." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplHrtfInterpolationMethod",
				"EIplHrtfInterpolationMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EIplSpatializationMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EIplSpatializationMethod, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EIplSpatializationMethod"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EIplSpatializationMethod>()
	{
		return EIplSpatializationMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EIplSpatializationMethod(EIplSpatializationMethod_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EIplSpatializationMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EIplSpatializationMethod_Hash() { return 3334460610U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EIplSpatializationMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EIplSpatializationMethod"), 0, Get_Z_Construct_UEnum_SteamAudio_EIplSpatializationMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EIplSpatializationMethod::PANNING", (int64)EIplSpatializationMethod::PANNING },
				{ "EIplSpatializationMethod::HRTF", (int64)EIplSpatializationMethod::HRTF },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "HRTF.Comment", "// Full 3D audio processing with HRTF.\n" },
				{ "HRTF.DisplayName", "HRTF" },
				{ "HRTF.Name", "EIplSpatializationMethod::HRTF" },
				{ "HRTF.ToolTip", "Full 3D audio processing with HRTF." },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
				{ "PANNING.Comment", "// Classic 2D panning - fast.\n" },
				{ "PANNING.DisplayName", "Panning" },
				{ "PANNING.Name", "EIplSpatializationMethod::PANNING" },
				{ "PANNING.ToolTip", "Classic 2D panning - fast." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EIplSpatializationMethod",
				"EIplSpatializationMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EQualitySettings_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EQualitySettings, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EQualitySettings"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EQualitySettings>()
	{
		return EQualitySettings_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EQualitySettings(EQualitySettings_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EQualitySettings"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EQualitySettings_Hash() { return 2954700654U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EQualitySettings()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EQualitySettings"), 0, Get_Z_Construct_UEnum_SteamAudio_EQualitySettings_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EQualitySettings::LOW", (int64)EQualitySettings::LOW },
				{ "EQualitySettings::MEDIUM", (int64)EQualitySettings::MEDIUM },
				{ "EQualitySettings::HIGH", (int64)EQualitySettings::HIGH },
				{ "EQualitySettings::CUSTOM", (int64)EQualitySettings::CUSTOM },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CUSTOM.DisplayName", "Custom" },
				{ "CUSTOM.Name", "EQualitySettings::CUSTOM" },
				{ "HIGH.DisplayName", "High" },
				{ "HIGH.Name", "EQualitySettings::HIGH" },
				{ "LOW.DisplayName", "Low" },
				{ "LOW.Name", "EQualitySettings::LOW" },
				{ "MEDIUM.DisplayName", "Medium" },
				{ "MEDIUM.Name", "EQualitySettings::MEDIUM" },
				{ "ModuleRelativePath", "Private/PhononCommon.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EQualitySettings",
				"EQualitySettings",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
