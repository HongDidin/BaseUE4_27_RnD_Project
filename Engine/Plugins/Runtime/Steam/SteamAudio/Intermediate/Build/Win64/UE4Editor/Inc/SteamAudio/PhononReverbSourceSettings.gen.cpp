// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Public/PhononReverbSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononReverbSourceSettings() {}
// Cross Module References
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononReverbSourceSettings_NoRegister();
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononReverbSourceSettings();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_UReverbPluginSourceSettingsBase();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplSimulationType();
// End Cross Module References
	void UPhononReverbSourceSettings::StaticRegisterNativesUPhononReverbSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UPhononReverbSourceSettings_NoRegister()
	{
		return UPhononReverbSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPhononReverbSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SourceReverbSimulationType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceReverbSimulationType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SourceReverbSimulationType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceReverbContribution_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SourceReverbContribution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhononReverbSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UReverbPluginSourceSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononReverbSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PhononReverbSourceSettings.h" },
		{ "ModuleRelativePath", "Public/PhononReverbSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbSimulationType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbSimulationType_MetaData[] = {
		{ "Category", "ReverbSettings" },
		{ "Comment", "// How to simulate source-centric reverb.\n" },
		{ "DisplayName", "Source-Centric Reverb Simulation" },
		{ "ModuleRelativePath", "Public/PhononReverbSourceSettings.h" },
		{ "ToolTip", "How to simulate source-centric reverb." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbSimulationType = { "SourceReverbSimulationType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononReverbSourceSettings, SourceReverbSimulationType), Z_Construct_UEnum_SteamAudio_EIplSimulationType, METADATA_PARAMS(Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbSimulationType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbSimulationType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbContribution_MetaData[] = {
		{ "Category", "ReverbSettings" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "// Scale factor to make the indirect contribution louder or softer.\n" },
		{ "DisplayName", "Source-Centric Reverb Contribution" },
		{ "ModuleRelativePath", "Public/PhononReverbSourceSettings.h" },
		{ "ToolTip", "Scale factor to make the indirect contribution louder or softer." },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbContribution = { "SourceReverbContribution", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononReverbSourceSettings, SourceReverbContribution), METADATA_PARAMS(Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbContribution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbContribution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPhononReverbSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbSimulationType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbSimulationType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononReverbSourceSettings_Statics::NewProp_SourceReverbContribution,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhononReverbSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhononReverbSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhononReverbSourceSettings_Statics::ClassParams = {
		&UPhononReverbSourceSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPhononReverbSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPhononReverbSourceSettings_Statics::PropPointers),
		0,
		0x001010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPhononReverbSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononReverbSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhononReverbSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhononReverbSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhononReverbSourceSettings, 732518548);
	template<> STEAMAUDIO_API UClass* StaticClass<UPhononReverbSourceSettings>()
	{
		return UPhononReverbSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhononReverbSourceSettings(Z_Construct_UClass_UPhononReverbSourceSettings, &UPhononReverbSourceSettings::StaticClass, TEXT("/Script/SteamAudio"), TEXT("UPhononReverbSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhononReverbSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
