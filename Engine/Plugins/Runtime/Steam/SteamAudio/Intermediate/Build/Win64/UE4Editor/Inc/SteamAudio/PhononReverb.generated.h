// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMAUDIO_PhononReverb_generated_h
#error "PhononReverb.generated.h already included, missing '#pragma once' in PhononReverb.h"
#endif
#define STEAMAUDIO_PhononReverb_generated_h

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_120_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics; \
	STEAMAUDIO_API static class UScriptStruct* StaticStruct();


template<> STEAMAUDIO_API UScriptStruct* StaticStruct<struct FSubmixEffectReverbPluginSettings>();

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSubmixEffectReverbPluginPreset(); \
	friend struct Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics; \
public: \
	DECLARE_CLASS(USubmixEffectReverbPluginPreset, USoundEffectSubmixPreset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(USubmixEffectReverbPluginPreset)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_INCLASS \
private: \
	static void StaticRegisterNativesUSubmixEffectReverbPluginPreset(); \
	friend struct Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics; \
public: \
	DECLARE_CLASS(USubmixEffectReverbPluginPreset, USoundEffectSubmixPreset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(USubmixEffectReverbPluginPreset)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USubmixEffectReverbPluginPreset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USubmixEffectReverbPluginPreset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubmixEffectReverbPluginPreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubmixEffectReverbPluginPreset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubmixEffectReverbPluginPreset(USubmixEffectReverbPluginPreset&&); \
	NO_API USubmixEffectReverbPluginPreset(const USubmixEffectReverbPluginPreset&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USubmixEffectReverbPluginPreset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USubmixEffectReverbPluginPreset(USubmixEffectReverbPluginPreset&&); \
	NO_API USubmixEffectReverbPluginPreset(const USubmixEffectReverbPluginPreset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USubmixEffectReverbPluginPreset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USubmixEffectReverbPluginPreset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USubmixEffectReverbPluginPreset)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_123_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h_126_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMAUDIO_API UClass* StaticClass<class USubmixEffectReverbPluginPreset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononReverb_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
