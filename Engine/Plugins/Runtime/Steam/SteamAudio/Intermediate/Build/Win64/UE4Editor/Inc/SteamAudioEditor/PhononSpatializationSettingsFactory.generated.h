// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMAUDIOEDITOR_PhononSpatializationSettingsFactory_generated_h
#error "PhononSpatializationSettingsFactory.generated.h already included, missing '#pragma once' in PhononSpatializationSettingsFactory.h"
#endif
#define STEAMAUDIOEDITOR_PhononSpatializationSettingsFactory_generated_h

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhononSpatializationSettingsFactory(); \
	friend struct Z_Construct_UClass_UPhononSpatializationSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UPhononSpatializationSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudioEditor"), STEAMAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UPhononSpatializationSettingsFactory)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUPhononSpatializationSettingsFactory(); \
	friend struct Z_Construct_UClass_UPhononSpatializationSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UPhononSpatializationSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudioEditor"), STEAMAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UPhononSpatializationSettingsFactory)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STEAMAUDIOEDITOR_API UPhononSpatializationSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhononSpatializationSettingsFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STEAMAUDIOEDITOR_API, UPhononSpatializationSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononSpatializationSettingsFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STEAMAUDIOEDITOR_API UPhononSpatializationSettingsFactory(UPhononSpatializationSettingsFactory&&); \
	STEAMAUDIOEDITOR_API UPhononSpatializationSettingsFactory(const UPhononSpatializationSettingsFactory&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STEAMAUDIOEDITOR_API UPhononSpatializationSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STEAMAUDIOEDITOR_API UPhononSpatializationSettingsFactory(UPhononSpatializationSettingsFactory&&); \
	STEAMAUDIOEDITOR_API UPhononSpatializationSettingsFactory(const UPhononSpatializationSettingsFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STEAMAUDIOEDITOR_API, UPhononSpatializationSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononSpatializationSettingsFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhononSpatializationSettingsFactory)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_27_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h_30_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PhononSpatializationSettingsFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMAUDIOEDITOR_API UClass* StaticClass<class UPhononSpatializationSettingsFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononSpatializationSettingsFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
