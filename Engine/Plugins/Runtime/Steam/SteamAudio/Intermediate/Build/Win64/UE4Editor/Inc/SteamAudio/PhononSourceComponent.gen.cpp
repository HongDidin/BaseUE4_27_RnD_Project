// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Private/PhononSourceComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononSourceComponent() {}
// Cross Module References
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononSourceComponent_NoRegister();
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononSourceComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
// End Cross Module References
	void UPhononSourceComponent::StaticRegisterNativesUPhononSourceComponent()
	{
	}
	UClass* Z_Construct_UClass_UPhononSourceComponent_NoRegister()
	{
		return UPhononSourceComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPhononSourceComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BakingRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BakingRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniqueIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_UniqueIdentifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhononSourceComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononSourceComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Audio" },
		{ "Comment", "/**\n * Phonon Source Components can be placed alongside statically positioned Audio Components in order to bake impulse response data\n * to be applied at runtime.\n */" },
		{ "HideCategories", "Activation Collision Tags Rendering Physics LOD Mobility Cooking AssetUserData Trigger PhysicsVolume" },
		{ "IncludePath", "PhononSourceComponent.h" },
		{ "ModuleRelativePath", "Private/PhononSourceComponent.h" },
		{ "ToolTip", "Phonon Source Components can be placed alongside statically positioned Audio Components in order to bake impulse response data\nto be applied at runtime." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_BakingRadius_MetaData[] = {
		{ "Category", "Baking" },
		{ "Comment", "// Any Phonon probes that lie within the baking radius will be used to produce baked impulse response data for this source location.\n" },
		{ "ModuleRelativePath", "Private/PhononSourceComponent.h" },
		{ "ToolTip", "Any Phonon probes that lie within the baking radius will be used to produce baked impulse response data for this source location." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_BakingRadius = { "BakingRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononSourceComponent, BakingRadius), METADATA_PARAMS(Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_BakingRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_BakingRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_UniqueIdentifier_MetaData[] = {
		{ "Category", "Baking" },
		{ "Comment", "// Users must specify a unique identifier for baked data lookup at runtime.\n" },
		{ "ModuleRelativePath", "Private/PhononSourceComponent.h" },
		{ "ToolTip", "Users must specify a unique identifier for baked data lookup at runtime." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_UniqueIdentifier = { "UniqueIdentifier", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononSourceComponent, UniqueIdentifier), METADATA_PARAMS(Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_UniqueIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_UniqueIdentifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPhononSourceComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_BakingRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononSourceComponent_Statics::NewProp_UniqueIdentifier,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhononSourceComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhononSourceComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhononSourceComponent_Statics::ClassParams = {
		&UPhononSourceComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPhononSourceComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSourceComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPhononSourceComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSourceComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhononSourceComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhononSourceComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhononSourceComponent, 2359215576);
	template<> STEAMAUDIO_API UClass* StaticClass<UPhononSourceComponent>()
	{
		return UPhononSourceComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhononSourceComponent(Z_Construct_UClass_UPhononSourceComponent, &UPhononSourceComponent::StaticClass, TEXT("/Script/SteamAudio"), TEXT("UPhononSourceComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhononSourceComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
