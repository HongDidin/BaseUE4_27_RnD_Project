// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Private/PhononMaterial.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononMaterial() {}
// Cross Module References
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EPhononMaterial();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
// End Cross Module References
	static UEnum* EPhononMaterial_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EPhononMaterial, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EPhononMaterial"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EPhononMaterial>()
	{
		return EPhononMaterial_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPhononMaterial(EPhononMaterial_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EPhononMaterial"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EPhononMaterial_Hash() { return 2505908958U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EPhononMaterial()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPhononMaterial"), 0, Get_Z_Construct_UEnum_SteamAudio_EPhononMaterial_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPhononMaterial::GENERIC", (int64)EPhononMaterial::GENERIC },
				{ "EPhononMaterial::BRICK", (int64)EPhononMaterial::BRICK },
				{ "EPhononMaterial::CONCRETE", (int64)EPhononMaterial::CONCRETE },
				{ "EPhononMaterial::CERAMIC", (int64)EPhononMaterial::CERAMIC },
				{ "EPhononMaterial::GRAVEL", (int64)EPhononMaterial::GRAVEL },
				{ "EPhononMaterial::CARPET", (int64)EPhononMaterial::CARPET },
				{ "EPhononMaterial::GLASS", (int64)EPhononMaterial::GLASS },
				{ "EPhononMaterial::PLASTER", (int64)EPhononMaterial::PLASTER },
				{ "EPhononMaterial::WOOD", (int64)EPhononMaterial::WOOD },
				{ "EPhononMaterial::METAL", (int64)EPhononMaterial::METAL },
				{ "EPhononMaterial::ROCK", (int64)EPhononMaterial::ROCK },
				{ "EPhononMaterial::CUSTOM", (int64)EPhononMaterial::CUSTOM },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BRICK.DisplayName", "Brick" },
				{ "BRICK.Name", "EPhononMaterial::BRICK" },
				{ "CARPET.DisplayName", "Carpet" },
				{ "CARPET.Name", "EPhononMaterial::CARPET" },
				{ "CERAMIC.DisplayName", "Ceramic" },
				{ "CERAMIC.Name", "EPhononMaterial::CERAMIC" },
				{ "CONCRETE.DisplayName", "Concrete" },
				{ "CONCRETE.Name", "EPhononMaterial::CONCRETE" },
				{ "CUSTOM.DisplayName", "Custom" },
				{ "CUSTOM.Name", "EPhononMaterial::CUSTOM" },
				{ "GENERIC.DisplayName", "Generic" },
				{ "GENERIC.Name", "EPhononMaterial::GENERIC" },
				{ "GLASS.DisplayName", "Glass" },
				{ "GLASS.Name", "EPhononMaterial::GLASS" },
				{ "GRAVEL.DisplayName", "Gravel" },
				{ "GRAVEL.Name", "EPhononMaterial::GRAVEL" },
				{ "METAL.DisplayName", "Metal" },
				{ "METAL.Name", "EPhononMaterial::METAL" },
				{ "ModuleRelativePath", "Private/PhononMaterial.h" },
				{ "PLASTER.DisplayName", "Plaster" },
				{ "PLASTER.Name", "EPhononMaterial::PLASTER" },
				{ "ROCK.DisplayName", "Rock" },
				{ "ROCK.Name", "EPhononMaterial::ROCK" },
				{ "WOOD.DisplayName", "Wood" },
				{ "WOOD.Name", "EPhononMaterial::WOOD" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EPhononMaterial",
				"EPhononMaterial",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
