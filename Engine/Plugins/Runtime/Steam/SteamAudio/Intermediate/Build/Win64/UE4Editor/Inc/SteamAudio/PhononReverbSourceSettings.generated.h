// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMAUDIO_PhononReverbSourceSettings_generated_h
#error "PhononReverbSourceSettings.generated.h already included, missing '#pragma once' in PhononReverbSourceSettings.h"
#endif
#define STEAMAUDIO_PhononReverbSourceSettings_generated_h

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhononReverbSourceSettings(); \
	friend struct Z_Construct_UClass_UPhononReverbSourceSettings_Statics; \
public: \
	DECLARE_CLASS(UPhononReverbSourceSettings, UReverbPluginSourceSettingsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(UPhononReverbSourceSettings)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUPhononReverbSourceSettings(); \
	friend struct Z_Construct_UClass_UPhononReverbSourceSettings_Statics; \
public: \
	DECLARE_CLASS(UPhononReverbSourceSettings, UReverbPluginSourceSettingsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(UPhononReverbSourceSettings)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhononReverbSourceSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhononReverbSourceSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhononReverbSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononReverbSourceSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhononReverbSourceSettings(UPhononReverbSourceSettings&&); \
	NO_API UPhononReverbSourceSettings(const UPhononReverbSourceSettings&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhononReverbSourceSettings(UPhononReverbSourceSettings&&); \
	NO_API UPhononReverbSourceSettings(const UPhononReverbSourceSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhononReverbSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononReverbSourceSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPhononReverbSourceSettings)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_11_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMAUDIO_API UClass* StaticClass<class UPhononReverbSourceSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononReverbSourceSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
