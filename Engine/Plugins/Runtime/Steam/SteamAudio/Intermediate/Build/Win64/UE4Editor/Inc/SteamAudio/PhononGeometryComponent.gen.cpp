// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Private/PhononGeometryComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononGeometryComponent() {}
// Cross Module References
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononGeometryComponent_NoRegister();
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononGeometryComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
// End Cross Module References
	void UPhononGeometryComponent::StaticRegisterNativesUPhononGeometryComponent()
	{
	}
	UClass* Z_Construct_UClass_UPhononGeometryComponent_NoRegister()
	{
		return UPhononGeometryComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPhononGeometryComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportAllChildren_MetaData[];
#endif
		static void NewProp_ExportAllChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ExportAllChildren;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumVertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_NumVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumTriangles_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_NumTriangles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhononGeometryComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononGeometryComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Audio" },
		{ "Comment", "/**\n * Phonon Geometry components are used to tag an actor as containing geometry relevant to acoustics calculations.\n * Should be placed on Static Mesh actors.\n */" },
		{ "HideCategories", "Activation Collision Cooking Trigger PhysicsVolume" },
		{ "IncludePath", "PhononGeometryComponent.h" },
		{ "ModuleRelativePath", "Private/PhononGeometryComponent.h" },
		{ "ToolTip", "Phonon Geometry components are used to tag an actor as containing geometry relevant to acoustics calculations.\nShould be placed on Static Mesh actors." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_ExportAllChildren_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Whether or not to export all actors attached to this actor.\n" },
		{ "ModuleRelativePath", "Private/PhononGeometryComponent.h" },
		{ "ToolTip", "Whether or not to export all actors attached to this actor." },
	};
#endif
	void Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_ExportAllChildren_SetBit(void* Obj)
	{
		((UPhononGeometryComponent*)Obj)->ExportAllChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_ExportAllChildren = { "ExportAllChildren", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPhononGeometryComponent), &Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_ExportAllChildren_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_ExportAllChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_ExportAllChildren_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumVertices_MetaData[] = {
		{ "Category", "GeometryStatistics" },
		{ "Comment", "// The number of vertices exported to Steam Audio.\n" },
		{ "DisplayName", "Vertices" },
		{ "ModuleRelativePath", "Private/PhononGeometryComponent.h" },
		{ "ToolTip", "The number of vertices exported to Steam Audio." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumVertices = { "NumVertices", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononGeometryComponent, NumVertices), METADATA_PARAMS(Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumTriangles_MetaData[] = {
		{ "Category", "GeometryStatistics" },
		{ "Comment", "// The number of triangles exported to Steam Audio.\n" },
		{ "DisplayName", "Triangles" },
		{ "ModuleRelativePath", "Private/PhononGeometryComponent.h" },
		{ "ToolTip", "The number of triangles exported to Steam Audio." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumTriangles = { "NumTriangles", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononGeometryComponent, NumTriangles), METADATA_PARAMS(Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumTriangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumTriangles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPhononGeometryComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_ExportAllChildren,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononGeometryComponent_Statics::NewProp_NumTriangles,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhononGeometryComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhononGeometryComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhononGeometryComponent_Statics::ClassParams = {
		&UPhononGeometryComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPhononGeometryComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPhononGeometryComponent_Statics::PropPointers),
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPhononGeometryComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononGeometryComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhononGeometryComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhononGeometryComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhononGeometryComponent, 900752067);
	template<> STEAMAUDIO_API UClass* StaticClass<UPhononGeometryComponent>()
	{
		return UPhononGeometryComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhononGeometryComponent(Z_Construct_UClass_UPhononGeometryComponent, &UPhononGeometryComponent::StaticClass, TEXT("/Script/SteamAudio"), TEXT("UPhononGeometryComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhononGeometryComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
