// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Private/PhononReverb.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononReverb() {}
// Cross Module References
	STEAMAUDIO_API UScriptStruct* Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
	STEAMAUDIO_API UClass* Z_Construct_UClass_USubmixEffectReverbPluginPreset_NoRegister();
	STEAMAUDIO_API UClass* Z_Construct_UClass_USubmixEffectReverbPluginPreset();
	ENGINE_API UClass* Z_Construct_UClass_USoundEffectSubmixPreset();
// End Cross Module References
class UScriptStruct* FSubmixEffectReverbPluginSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STEAMAUDIO_API uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings, Z_Construct_UPackage__Script_SteamAudio(), TEXT("SubmixEffectReverbPluginSettings"), sizeof(FSubmixEffectReverbPluginSettings), Get_Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Hash());
	}
	return Singleton;
}
template<> STEAMAUDIO_API UScriptStruct* StaticStruct<FSubmixEffectReverbPluginSettings>()
{
	return FSubmixEffectReverbPluginSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSubmixEffectReverbPluginSettings(FSubmixEffectReverbPluginSettings::StaticStruct, TEXT("/Script/SteamAudio"), TEXT("SubmixEffectReverbPluginSettings"), false, nullptr, nullptr);
static struct FScriptStruct_SteamAudio_StaticRegisterNativesFSubmixEffectReverbPluginSettings
{
	FScriptStruct_SteamAudio_StaticRegisterNativesFSubmixEffectReverbPluginSettings()
	{
		UScriptStruct::DeferCppStructOps<FSubmixEffectReverbPluginSettings>(FName(TEXT("SubmixEffectReverbPluginSettings")));
	}
} ScriptStruct_SteamAudio_StaticRegisterNativesFSubmixEffectReverbPluginSettings;
	struct Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/PhononReverb.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSubmixEffectReverbPluginSettings>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
		nullptr,
		&NewStructOps,
		"SubmixEffectReverbPluginSettings",
		sizeof(FSubmixEffectReverbPluginSettings),
		alignof(FSubmixEffectReverbPluginSettings),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SubmixEffectReverbPluginSettings"), sizeof(FSubmixEffectReverbPluginSettings), Get_Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings_Hash() { return 3439349467U; }
	void USubmixEffectReverbPluginPreset::StaticRegisterNativesUSubmixEffectReverbPluginPreset()
	{
	}
	UClass* Z_Construct_UClass_USubmixEffectReverbPluginPreset_NoRegister()
	{
		return USubmixEffectReverbPluginPreset::StaticClass();
	}
	struct Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundEffectSubmixPreset,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "PhononReverb.h" },
		{ "ModuleRelativePath", "Private/PhononReverb.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "SubmixEffectPreset" },
		{ "ModuleRelativePath", "Private/PhononReverb.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USubmixEffectReverbPluginPreset, Settings), Z_Construct_UScriptStruct_FSubmixEffectReverbPluginSettings, METADATA_PARAMS(Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::NewProp_Settings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USubmixEffectReverbPluginPreset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::ClassParams = {
		&USubmixEffectReverbPluginPreset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::PropPointers),
		0,
		0x000010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USubmixEffectReverbPluginPreset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USubmixEffectReverbPluginPreset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USubmixEffectReverbPluginPreset, 3719505576);
	template<> STEAMAUDIO_API UClass* StaticClass<USubmixEffectReverbPluginPreset>()
	{
		return USubmixEffectReverbPluginPreset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USubmixEffectReverbPluginPreset(Z_Construct_UClass_USubmixEffectReverbPluginPreset, &USubmixEffectReverbPluginPreset::StaticClass, TEXT("/Script/SteamAudio"), TEXT("USubmixEffectReverbPluginPreset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USubmixEffectReverbPluginPreset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
