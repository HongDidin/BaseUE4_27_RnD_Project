// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMAUDIOEDITOR_PhononOcclusionSettingsFactory_generated_h
#error "PhononOcclusionSettingsFactory.generated.h already included, missing '#pragma once' in PhononOcclusionSettingsFactory.h"
#endif
#define STEAMAUDIOEDITOR_PhononOcclusionSettingsFactory_generated_h

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhononOcclusionSettingsFactory(); \
	friend struct Z_Construct_UClass_UPhononOcclusionSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UPhononOcclusionSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudioEditor"), STEAMAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UPhononOcclusionSettingsFactory)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUPhononOcclusionSettingsFactory(); \
	friend struct Z_Construct_UClass_UPhononOcclusionSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UPhononOcclusionSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudioEditor"), STEAMAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UPhononOcclusionSettingsFactory)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STEAMAUDIOEDITOR_API UPhononOcclusionSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhononOcclusionSettingsFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STEAMAUDIOEDITOR_API, UPhononOcclusionSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononOcclusionSettingsFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STEAMAUDIOEDITOR_API UPhononOcclusionSettingsFactory(UPhononOcclusionSettingsFactory&&); \
	STEAMAUDIOEDITOR_API UPhononOcclusionSettingsFactory(const UPhononOcclusionSettingsFactory&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	STEAMAUDIOEDITOR_API UPhononOcclusionSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	STEAMAUDIOEDITOR_API UPhononOcclusionSettingsFactory(UPhononOcclusionSettingsFactory&&); \
	STEAMAUDIOEDITOR_API UPhononOcclusionSettingsFactory(const UPhononOcclusionSettingsFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(STEAMAUDIOEDITOR_API, UPhononOcclusionSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononOcclusionSettingsFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhononOcclusionSettingsFactory)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_27_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h_30_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PhononOcclusionSettingsFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMAUDIOEDITOR_API UClass* StaticClass<class UPhononOcclusionSettingsFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudioEditor_Private_PhononOcclusionSettingsFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
