// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMAUDIO_PhononProbeVolume_generated_h
#error "PhononProbeVolume.generated.h already included, missing '#pragma once' in PhononProbeVolume.h"
#endif
#define STEAMAUDIO_PhononProbeVolume_generated_h

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_32_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBakedDataInfo_Statics; \
	STEAMAUDIO_API static class UScriptStruct* StaticStruct();


template<> STEAMAUDIO_API UScriptStruct* StaticStruct<struct FBakedDataInfo>();

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPhononProbeVolume(); \
	friend struct Z_Construct_UClass_APhononProbeVolume_Statics; \
public: \
	DECLARE_CLASS(APhononProbeVolume, AVolume, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(APhononProbeVolume)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_INCLASS \
private: \
	static void StaticRegisterNativesAPhononProbeVolume(); \
	friend struct Z_Construct_UClass_APhononProbeVolume_Statics; \
public: \
	DECLARE_CLASS(APhononProbeVolume, AVolume, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(APhononProbeVolume)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APhononProbeVolume(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APhononProbeVolume) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APhononProbeVolume); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APhononProbeVolume); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APhononProbeVolume(APhononProbeVolume&&); \
	NO_API APhononProbeVolume(const APhononProbeVolume&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APhononProbeVolume(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APhononProbeVolume(APhononProbeVolume&&); \
	NO_API APhononProbeVolume(const APhononProbeVolume&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APhononProbeVolume); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APhononProbeVolume); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APhononProbeVolume)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_55_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h_58_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PhononProbeVolume."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMAUDIO_API UClass* StaticClass<class APhononProbeVolume>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_PhononProbeVolume_h


#define FOREACH_ENUM_EPHONONPROBEMOBILITY(op) \
	op(EPhononProbeMobility::STATIC) \
	op(EPhononProbeMobility::DYNAMIC) 

enum class EPhononProbeMobility : uint8;
template<> STEAMAUDIO_API UEnum* StaticEnum<EPhononProbeMobility>();

#define FOREACH_ENUM_EPHONONPROBEPLACEMENTSTRATEGY(op) \
	op(EPhononProbePlacementStrategy::CENTROID) \
	op(EPhononProbePlacementStrategy::UNIFORM_FLOOR) 

enum class EPhononProbePlacementStrategy : uint8;
template<> STEAMAUDIO_API UEnum* StaticEnum<EPhononProbePlacementStrategy>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
