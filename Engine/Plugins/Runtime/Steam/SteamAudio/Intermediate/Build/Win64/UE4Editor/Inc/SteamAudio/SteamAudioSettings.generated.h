// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMAUDIO_SteamAudioSettings_generated_h
#error "SteamAudioSettings.generated.h already included, missing '#pragma once' in SteamAudioSettings.h"
#endif
#define STEAMAUDIO_SteamAudioSettings_generated_h

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSteamAudioSettings(); \
	friend struct Z_Construct_UClass_USteamAudioSettings_Statics; \
public: \
	DECLARE_CLASS(USteamAudioSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(USteamAudioSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSteamAudioSettings(); \
	friend struct Z_Construct_UClass_USteamAudioSettings_Statics; \
public: \
	DECLARE_CLASS(USteamAudioSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(USteamAudioSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USteamAudioSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USteamAudioSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USteamAudioSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USteamAudioSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USteamAudioSettings(USteamAudioSettings&&); \
	NO_API USteamAudioSettings(const USteamAudioSettings&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USteamAudioSettings(USteamAudioSettings&&); \
	NO_API USteamAudioSettings(const USteamAudioSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USteamAudioSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USteamAudioSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USteamAudioSettings)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_11_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMAUDIO_API UClass* StaticClass<class USteamAudioSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Private_SteamAudioSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
