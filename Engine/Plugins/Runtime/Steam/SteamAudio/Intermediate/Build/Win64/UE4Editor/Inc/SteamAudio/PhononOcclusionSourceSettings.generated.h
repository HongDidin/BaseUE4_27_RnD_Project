// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef STEAMAUDIO_PhononOcclusionSourceSettings_generated_h
#error "PhononOcclusionSourceSettings.generated.h already included, missing '#pragma once' in PhononOcclusionSourceSettings.h"
#endif
#define STEAMAUDIO_PhononOcclusionSourceSettings_generated_h

#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPhononOcclusionSourceSettings(); \
	friend struct Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics; \
public: \
	DECLARE_CLASS(UPhononOcclusionSourceSettings, UOcclusionPluginSourceSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(UPhononOcclusionSourceSettings)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUPhononOcclusionSourceSettings(); \
	friend struct Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics; \
public: \
	DECLARE_CLASS(UPhononOcclusionSourceSettings, UOcclusionPluginSourceSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SteamAudio"), NO_API) \
	DECLARE_SERIALIZER(UPhononOcclusionSourceSettings)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPhononOcclusionSourceSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPhononOcclusionSourceSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhononOcclusionSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononOcclusionSourceSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhononOcclusionSourceSettings(UPhononOcclusionSourceSettings&&); \
	NO_API UPhononOcclusionSourceSettings(const UPhononOcclusionSourceSettings&); \
public:


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPhononOcclusionSourceSettings(UPhononOcclusionSourceSettings&&); \
	NO_API UPhononOcclusionSourceSettings(const UPhononOcclusionSourceSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPhononOcclusionSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPhononOcclusionSourceSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPhononOcclusionSourceSettings)


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_11_PROLOG
#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_INCLASS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> STEAMAUDIO_API UClass* StaticClass<class UPhononOcclusionSourceSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Steam_SteamAudio_Source_SteamAudio_Public_PhononOcclusionSourceSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
