// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Public/PhononSpatializationSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononSpatializationSourceSettings() {}
// Cross Module References
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononSpatializationSourceSettings_NoRegister();
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononSpatializationSourceSettings();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USpatializationPluginSourceSettingsBase();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplSpatializationMethod();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplHrtfInterpolationMethod();
// End Cross Module References
	void UPhononSpatializationSourceSettings::StaticRegisterNativesUPhononSpatializationSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UPhononSpatializationSourceSettings_NoRegister()
	{
		return UPhononSpatializationSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SpatializationMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpatializationMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SpatializationMethod;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_HrtfInterpolationMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HrtfInterpolationMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_HrtfInterpolationMethod;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USpatializationPluginSourceSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PhononSpatializationSourceSettings.h" },
		{ "ModuleRelativePath", "Public/PhononSpatializationSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_SpatializationMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_SpatializationMethod_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "ModuleRelativePath", "Public/PhononSpatializationSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_SpatializationMethod = { "SpatializationMethod", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononSpatializationSourceSettings, SpatializationMethod), Z_Construct_UEnum_SteamAudio_EIplSpatializationMethod, METADATA_PARAMS(Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_SpatializationMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_SpatializationMethod_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_HrtfInterpolationMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_HrtfInterpolationMethod_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "DisplayName", "HRTF Interpolation Method" },
		{ "ModuleRelativePath", "Public/PhononSpatializationSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_HrtfInterpolationMethod = { "HrtfInterpolationMethod", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononSpatializationSourceSettings, HrtfInterpolationMethod), Z_Construct_UEnum_SteamAudio_EIplHrtfInterpolationMethod, METADATA_PARAMS(Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_HrtfInterpolationMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_HrtfInterpolationMethod_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_SpatializationMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_SpatializationMethod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_HrtfInterpolationMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::NewProp_HrtfInterpolationMethod,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhononSpatializationSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::ClassParams = {
		&UPhononSpatializationSourceSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::PropPointers),
		0,
		0x001010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhononSpatializationSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhononSpatializationSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhononSpatializationSourceSettings, 1875613715);
	template<> STEAMAUDIO_API UClass* StaticClass<UPhononSpatializationSourceSettings>()
	{
		return UPhononSpatializationSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhononSpatializationSourceSettings(Z_Construct_UClass_UPhononSpatializationSourceSettings, &UPhononSpatializationSourceSettings::StaticClass, TEXT("/Script/SteamAudio"), TEXT("UPhononSpatializationSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhononSpatializationSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
