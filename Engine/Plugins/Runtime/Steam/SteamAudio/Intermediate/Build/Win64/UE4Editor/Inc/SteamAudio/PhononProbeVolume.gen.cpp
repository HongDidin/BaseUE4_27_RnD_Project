// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Private/PhononProbeVolume.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononProbeVolume() {}
// Cross Module References
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EPhononProbeMobility();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EPhononProbePlacementStrategy();
	STEAMAUDIO_API UScriptStruct* Z_Construct_UScriptStruct_FBakedDataInfo();
	STEAMAUDIO_API UClass* Z_Construct_UClass_APhononProbeVolume_NoRegister();
	STEAMAUDIO_API UClass* Z_Construct_UClass_APhononProbeVolume();
	ENGINE_API UClass* Z_Construct_UClass_AVolume();
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononProbeComponent_NoRegister();
// End Cross Module References
	static UEnum* EPhononProbeMobility_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EPhononProbeMobility, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EPhononProbeMobility"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EPhononProbeMobility>()
	{
		return EPhononProbeMobility_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPhononProbeMobility(EPhononProbeMobility_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EPhononProbeMobility"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EPhononProbeMobility_Hash() { return 3888457770U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EPhononProbeMobility()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPhononProbeMobility"), 0, Get_Z_Construct_UEnum_SteamAudio_EPhononProbeMobility_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPhononProbeMobility::STATIC", (int64)EPhononProbeMobility::STATIC },
				{ "EPhononProbeMobility::DYNAMIC", (int64)EPhononProbeMobility::DYNAMIC },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "DYNAMIC.Comment", "// Dynamic probes inherit this volume's offset at runtime.\n" },
				{ "DYNAMIC.DisplayName", "Dynamic" },
				{ "DYNAMIC.Name", "EPhononProbeMobility::DYNAMIC" },
				{ "DYNAMIC.ToolTip", "Dynamic probes inherit this volume's offset at runtime." },
				{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
				{ "STATIC.Comment", "// Static probes remain fixed at runtime.\n" },
				{ "STATIC.DisplayName", "Static" },
				{ "STATIC.Name", "EPhononProbeMobility::STATIC" },
				{ "STATIC.ToolTip", "Static probes remain fixed at runtime." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EPhononProbeMobility",
				"EPhononProbeMobility",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPhononProbePlacementStrategy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SteamAudio_EPhononProbePlacementStrategy, Z_Construct_UPackage__Script_SteamAudio(), TEXT("EPhononProbePlacementStrategy"));
		}
		return Singleton;
	}
	template<> STEAMAUDIO_API UEnum* StaticEnum<EPhononProbePlacementStrategy>()
	{
		return EPhononProbePlacementStrategy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPhononProbePlacementStrategy(EPhononProbePlacementStrategy_StaticEnum, TEXT("/Script/SteamAudio"), TEXT("EPhononProbePlacementStrategy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SteamAudio_EPhononProbePlacementStrategy_Hash() { return 591942127U; }
	UEnum* Z_Construct_UEnum_SteamAudio_EPhononProbePlacementStrategy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPhononProbePlacementStrategy"), 0, Get_Z_Construct_UEnum_SteamAudio_EPhononProbePlacementStrategy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPhononProbePlacementStrategy::CENTROID", (int64)EPhononProbePlacementStrategy::CENTROID },
				{ "EPhononProbePlacementStrategy::UNIFORM_FLOOR", (int64)EPhononProbePlacementStrategy::UNIFORM_FLOOR },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CENTROID.Comment", "// Places a single probe at the centroid of the volume.\n" },
				{ "CENTROID.DisplayName", "Centroid" },
				{ "CENTROID.Name", "EPhononProbePlacementStrategy::CENTROID" },
				{ "CENTROID.ToolTip", "Places a single probe at the centroid of the volume." },
				{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
				{ "UNIFORM_FLOOR.Comment", "// Places uniformly spaced probes along the floor at a specified height.\n" },
				{ "UNIFORM_FLOOR.DisplayName", "Uniform Floor" },
				{ "UNIFORM_FLOOR.Name", "EPhononProbePlacementStrategy::UNIFORM_FLOOR" },
				{ "UNIFORM_FLOOR.ToolTip", "Places uniformly spaced probes along the floor at a specified height." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SteamAudio,
				nullptr,
				"EPhononProbePlacementStrategy",
				"EPhononProbePlacementStrategy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FBakedDataInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern STEAMAUDIO_API uint32 Get_Z_Construct_UScriptStruct_FBakedDataInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBakedDataInfo, Z_Construct_UPackage__Script_SteamAudio(), TEXT("BakedDataInfo"), sizeof(FBakedDataInfo), Get_Z_Construct_UScriptStruct_FBakedDataInfo_Hash());
	}
	return Singleton;
}
template<> STEAMAUDIO_API UScriptStruct* StaticStruct<FBakedDataInfo>()
{
	return FBakedDataInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBakedDataInfo(FBakedDataInfo::StaticStruct, TEXT("/Script/SteamAudio"), TEXT("BakedDataInfo"), false, nullptr, nullptr);
static struct FScriptStruct_SteamAudio_StaticRegisterNativesFBakedDataInfo
{
	FScriptStruct_SteamAudio_StaticRegisterNativesFBakedDataInfo()
	{
		UScriptStruct::DeferCppStructOps<FBakedDataInfo>(FName(TEXT("BakedDataInfo")));
	}
} ScriptStruct_SteamAudio_StaticRegisterNativesFBakedDataInfo;
	struct Z_Construct_UScriptStruct_FBakedDataInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBakedDataInfo_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBakedDataInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBakedDataInfo, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Size_MetaData[] = {
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBakedDataInfo, Size), METADATA_PARAMS(Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBakedDataInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBakedDataInfo_Statics::NewProp_Size,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBakedDataInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
		nullptr,
		&NewStructOps,
		"BakedDataInfo",
		sizeof(FBakedDataInfo),
		alignof(FBakedDataInfo),
		Z_Construct_UScriptStruct_FBakedDataInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBakedDataInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBakedDataInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBakedDataInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBakedDataInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBakedDataInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SteamAudio();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BakedDataInfo"), sizeof(FBakedDataInfo), Get_Z_Construct_UScriptStruct_FBakedDataInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBakedDataInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBakedDataInfo_Hash() { return 356967363U; }
	void APhononProbeVolume::StaticRegisterNativesAPhononProbeVolume()
	{
	}
	UClass* Z_Construct_UClass_APhononProbeVolume_NoRegister()
	{
		return APhononProbeVolume::StaticClass();
	}
	struct Z_Construct_UClass_APhononProbeVolume_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlacementStrategy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlacementStrategy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PlacementStrategy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HorizontalSpacing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HorizontalSpacing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightAboveFloor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeightAboveFloor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumProbes_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumProbes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProbeDataSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ProbeDataSize;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BakedDataInfo_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BakedDataInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BakedDataInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhononProbeComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PhononProbeComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProbeBoxFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProbeBoxFileName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProbeBatchFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProbeBatchFileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APhononProbeVolume_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AVolume,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "Comment", "/**\n * Phonon Probe volumes generate a set of probes at which acoustic information will be sampled\n * at bake time.\n */" },
		{ "HideCategories", "Actor Advanced Attachment Collision Brush Physics Object Blueprint Display Rendering Physics Input" },
		{ "IncludePath", "PhononProbeVolume.h" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
		{ "ToolTip", "Phonon Probe volumes generate a set of probes at which acoustic information will be sampled\nat bake time." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PlacementStrategy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PlacementStrategy_MetaData[] = {
		{ "Category", "ProbeGeneration" },
		{ "Comment", "// Method by which probes are placed within the volume.\n" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "Method by which probes are placed within the volume." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PlacementStrategy = { "PlacementStrategy", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, PlacementStrategy), Z_Construct_UEnum_SteamAudio_EPhononProbePlacementStrategy, METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PlacementStrategy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PlacementStrategy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HorizontalSpacing_MetaData[] = {
		{ "Category", "ProbeGeneration" },
		{ "ClampMax", "5000.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "// How far apart to place probes.\n" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "How far apart to place probes." },
		{ "UIMax", "5000.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HorizontalSpacing = { "HorizontalSpacing", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, HorizontalSpacing), METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HorizontalSpacing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HorizontalSpacing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HeightAboveFloor_MetaData[] = {
		{ "Category", "ProbeGeneration" },
		{ "ClampMax", "5000.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "// How high above the floor to place probes.\n" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "How high above the floor to place probes." },
		{ "UIMax", "5000.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HeightAboveFloor = { "HeightAboveFloor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, HeightAboveFloor), METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HeightAboveFloor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HeightAboveFloor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_NumProbes_MetaData[] = {
		{ "Category", "ProbeVolumeStatistics" },
		{ "Comment", "// Number of probes contained in this probe volume.\n" },
		{ "DisplayName", "Probe Points" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "Number of probes contained in this probe volume." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_NumProbes = { "NumProbes", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, NumProbes), METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_NumProbes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_NumProbes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeDataSize_MetaData[] = {
		{ "Category", "ProbeVolumeStatistics" },
		{ "Comment", "// Size of probe data in bytes.\n" },
		{ "DisplayName", "Probe Data Size" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "Size of probe data in bytes." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeDataSize = { "ProbeDataSize", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, ProbeDataSize), METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeDataSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeDataSize_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_BakedDataInfo_Inner = { "BakedDataInfo", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FBakedDataInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_BakedDataInfo_MetaData[] = {
		{ "Category", "ProbeVolumeStatistics" },
		{ "Comment", "// Useful information for each baked source.\n" },
		{ "DisplayName", "Detailed Statistics" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "Useful information for each baked source." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_BakedDataInfo = { "BakedDataInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, BakedDataInfo), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_BakedDataInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_BakedDataInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PhononProbeComponent_MetaData[] = {
		{ "Comment", "// Component used for visualization.\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "Component used for visualization." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PhononProbeComponent = { "PhononProbeComponent", nullptr, (EPropertyFlags)0x0010000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, PhononProbeComponent), Z_Construct_UClass_UPhononProbeComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PhononProbeComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PhononProbeComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBoxFileName_MetaData[] = {
		{ "Comment", "// Current filename where probe box data is stored. Used to maintain connection if volume is renamed.\n" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "Current filename where probe box data is stored. Used to maintain connection if volume is renamed." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBoxFileName = { "ProbeBoxFileName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, ProbeBoxFileName), METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBoxFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBoxFileName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBatchFileName_MetaData[] = {
		{ "Comment", "// Current filename where probe batch data is stored. Used to maintain connection if volume is renamed.\n" },
		{ "ModuleRelativePath", "Private/PhononProbeVolume.h" },
		{ "ToolTip", "Current filename where probe batch data is stored. Used to maintain connection if volume is renamed." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBatchFileName = { "ProbeBatchFileName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(APhononProbeVolume, ProbeBatchFileName), METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBatchFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBatchFileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_APhononProbeVolume_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PlacementStrategy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PlacementStrategy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HorizontalSpacing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_HeightAboveFloor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_NumProbes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeDataSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_BakedDataInfo_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_BakedDataInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_PhononProbeComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBoxFileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_APhononProbeVolume_Statics::NewProp_ProbeBatchFileName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_APhononProbeVolume_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APhononProbeVolume>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APhononProbeVolume_Statics::ClassParams = {
		&APhononProbeVolume::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_APhononProbeVolume_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_APhononProbeVolume_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APhononProbeVolume_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APhononProbeVolume()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APhononProbeVolume_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APhononProbeVolume, 2339889692);
	template<> STEAMAUDIO_API UClass* StaticClass<APhononProbeVolume>()
	{
		return APhononProbeVolume::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APhononProbeVolume(Z_Construct_UClass_APhononProbeVolume, &APhononProbeVolume::StaticClass, TEXT("/Script/SteamAudio"), TEXT("APhononProbeVolume"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APhononProbeVolume);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
