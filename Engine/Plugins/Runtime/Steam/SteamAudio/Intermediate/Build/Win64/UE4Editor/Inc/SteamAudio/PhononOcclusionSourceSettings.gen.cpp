// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamAudio/Public/PhononOcclusionSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhononOcclusionSourceSettings() {}
// Cross Module References
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononOcclusionSourceSettings_NoRegister();
	STEAMAUDIO_API UClass* Z_Construct_UClass_UPhononOcclusionSourceSettings();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_UOcclusionPluginSourceSettingsBase();
	UPackage* Z_Construct_UPackage__Script_SteamAudio();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMode();
	STEAMAUDIO_API UEnum* Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMethod();
// End Cross Module References
	void UPhononOcclusionSourceSettings::StaticRegisterNativesUPhononOcclusionSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UPhononOcclusionSourceSettings_NoRegister()
	{
		return UPhononOcclusionSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DirectOcclusionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectOcclusionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DirectOcclusionMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DirectOcclusionMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectOcclusionMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DirectOcclusionMethod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectOcclusionSourceRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DirectOcclusionSourceRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectAttenuation_MetaData[];
#endif
		static void NewProp_DirectAttenuation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DirectAttenuation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AirAbsorption_MetaData[];
#endif
		static void NewProp_AirAbsorption_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AirAbsorption;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOcclusionPluginSourceSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PhononOcclusionSourceSettings.h" },
		{ "ModuleRelativePath", "Public/PhononOcclusionSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMode_MetaData[] = {
		{ "Category", "OcclusionSettings" },
		{ "ModuleRelativePath", "Public/PhononOcclusionSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMode = { "DirectOcclusionMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononOcclusionSourceSettings, DirectOcclusionMode), Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMode, METADATA_PARAMS(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMethod_MetaData[] = {
		{ "Category", "OcclusionSettings" },
		{ "ModuleRelativePath", "Public/PhononOcclusionSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMethod = { "DirectOcclusionMethod", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononOcclusionSourceSettings, DirectOcclusionMethod), Z_Construct_UEnum_SteamAudio_EIplDirectOcclusionMethod, METADATA_PARAMS(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMethod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionSourceRadius_MetaData[] = {
		{ "Category", "OcclusionSettings" },
		{ "ModuleRelativePath", "Public/PhononOcclusionSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionSourceRadius = { "DirectOcclusionSourceRadius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhononOcclusionSourceSettings, DirectOcclusionSourceRadius), METADATA_PARAMS(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionSourceRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionSourceRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectAttenuation_MetaData[] = {
		{ "Category", "OcclusionSettings" },
		{ "DisplayName", "Physics-based Attenuation" },
		{ "ModuleRelativePath", "Public/PhononOcclusionSourceSettings.h" },
	};
#endif
	void Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectAttenuation_SetBit(void* Obj)
	{
		((UPhononOcclusionSourceSettings*)Obj)->DirectAttenuation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectAttenuation = { "DirectAttenuation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPhononOcclusionSourceSettings), &Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectAttenuation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectAttenuation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectAttenuation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_AirAbsorption_MetaData[] = {
		{ "Category", "OcclusionSettings" },
		{ "ModuleRelativePath", "Public/PhononOcclusionSourceSettings.h" },
	};
#endif
	void Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_AirAbsorption_SetBit(void* Obj)
	{
		((UPhononOcclusionSourceSettings*)Obj)->AirAbsorption = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_AirAbsorption = { "AirAbsorption", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPhononOcclusionSourceSettings), &Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_AirAbsorption_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_AirAbsorption_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_AirAbsorption_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionMethod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectOcclusionSourceRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_DirectAttenuation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::NewProp_AirAbsorption,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhononOcclusionSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::ClassParams = {
		&UPhononOcclusionSourceSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhononOcclusionSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhononOcclusionSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhononOcclusionSourceSettings, 3692630654);
	template<> STEAMAUDIO_API UClass* StaticClass<UPhononOcclusionSourceSettings>()
	{
		return UPhononOcclusionSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhononOcclusionSourceSettings(Z_Construct_UClass_UPhononOcclusionSourceSettings, &UPhononOcclusionSourceSettings::StaticClass, TEXT("/Script/SteamAudio"), TEXT("UPhononOcclusionSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhononOcclusionSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
