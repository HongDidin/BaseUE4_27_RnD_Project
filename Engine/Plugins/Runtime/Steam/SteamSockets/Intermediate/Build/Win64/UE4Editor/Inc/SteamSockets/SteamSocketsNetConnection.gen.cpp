// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamSockets/Public/SteamSocketsNetConnection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSteamSocketsNetConnection() {}
// Cross Module References
	STEAMSOCKETS_API UClass* Z_Construct_UClass_USteamSocketsNetConnection_NoRegister();
	STEAMSOCKETS_API UClass* Z_Construct_UClass_USteamSocketsNetConnection();
	ENGINE_API UClass* Z_Construct_UClass_UNetConnection();
	UPackage* Z_Construct_UPackage__Script_SteamSockets();
// End Cross Module References
	void USteamSocketsNetConnection::StaticRegisterNativesUSteamSocketsNetConnection()
	{
	}
	UClass* Z_Construct_UClass_USteamSocketsNetConnection_NoRegister()
	{
		return USteamSocketsNetConnection::StaticClass();
	}
	struct Z_Construct_UClass_USteamSocketsNetConnection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USteamSocketsNetConnection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNetConnection,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamSockets,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USteamSocketsNetConnection_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SteamSocketsNetConnection.h" },
		{ "ModuleRelativePath", "Public/SteamSocketsNetConnection.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USteamSocketsNetConnection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USteamSocketsNetConnection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USteamSocketsNetConnection_Statics::ClassParams = {
		&USteamSocketsNetConnection::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_USteamSocketsNetConnection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USteamSocketsNetConnection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USteamSocketsNetConnection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USteamSocketsNetConnection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USteamSocketsNetConnection, 3464041804);
	template<> STEAMSOCKETS_API UClass* StaticClass<USteamSocketsNetConnection>()
	{
		return USteamSocketsNetConnection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USteamSocketsNetConnection(Z_Construct_UClass_USteamSocketsNetConnection, &USteamSocketsNetConnection::StaticClass, TEXT("/Script/SteamSockets"), TEXT("USteamSocketsNetConnection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USteamSocketsNetConnection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
