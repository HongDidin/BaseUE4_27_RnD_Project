// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SteamSockets/Public/SteamSocketsNetDriver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSteamSocketsNetDriver() {}
// Cross Module References
	STEAMSOCKETS_API UClass* Z_Construct_UClass_USteamSocketsNetDriver_NoRegister();
	STEAMSOCKETS_API UClass* Z_Construct_UClass_USteamSocketsNetDriver();
	ENGINE_API UClass* Z_Construct_UClass_UNetDriver();
	UPackage* Z_Construct_UPackage__Script_SteamSockets();
// End Cross Module References
	void USteamSocketsNetDriver::StaticRegisterNativesUSteamSocketsNetDriver()
	{
	}
	UClass* Z_Construct_UClass_USteamSocketsNetDriver_NoRegister()
	{
		return USteamSocketsNetDriver::StaticClass();
	}
	struct Z_Construct_UClass_USteamSocketsNetDriver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USteamSocketsNetDriver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNetDriver,
		(UObject* (*)())Z_Construct_UPackage__Script_SteamSockets,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USteamSocketsNetDriver_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SteamSocketsNetDriver.h" },
		{ "ModuleRelativePath", "Public/SteamSocketsNetDriver.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USteamSocketsNetDriver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USteamSocketsNetDriver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USteamSocketsNetDriver_Statics::ClassParams = {
		&USteamSocketsNetDriver::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_USteamSocketsNetDriver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USteamSocketsNetDriver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USteamSocketsNetDriver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USteamSocketsNetDriver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USteamSocketsNetDriver, 916331384);
	template<> STEAMSOCKETS_API UClass* StaticClass<USteamSocketsNetDriver>()
	{
		return USteamSocketsNetDriver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USteamSocketsNetDriver(Z_Construct_UClass_USteamSocketsNetDriver, &USteamSocketsNetDriver::StaticClass, TEXT("/Script/SteamSockets"), TEXT("USteamSocketsNetDriver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USteamSocketsNetDriver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
