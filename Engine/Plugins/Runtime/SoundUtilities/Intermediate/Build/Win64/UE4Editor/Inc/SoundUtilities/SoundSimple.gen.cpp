// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoundUtilities/Public/SoundSimple.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundSimple() {}
// Cross Module References
	SOUNDUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FSoundVariation();
	UPackage* Z_Construct_UPackage__Script_SoundUtilities();
	ENGINE_API UClass* Z_Construct_UClass_USoundWave_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	SOUNDUTILITIES_API UClass* Z_Construct_UClass_USoundSimple_NoRegister();
	SOUNDUTILITIES_API UClass* Z_Construct_UClass_USoundSimple();
	ENGINE_API UClass* Z_Construct_UClass_USoundBase();
// End Cross Module References
class UScriptStruct* FSoundVariation::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SOUNDUTILITIES_API uint32 Get_Z_Construct_UScriptStruct_FSoundVariation_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundVariation, Z_Construct_UPackage__Script_SoundUtilities(), TEXT("SoundVariation"), sizeof(FSoundVariation), Get_Z_Construct_UScriptStruct_FSoundVariation_Hash());
	}
	return Singleton;
}
template<> SOUNDUTILITIES_API UScriptStruct* StaticStruct<FSoundVariation>()
{
	return FSoundVariation::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundVariation(FSoundVariation::StaticStruct, TEXT("/Script/SoundUtilities"), TEXT("SoundVariation"), false, nullptr, nullptr);
static struct FScriptStruct_SoundUtilities_StaticRegisterNativesFSoundVariation
{
	FScriptStruct_SoundUtilities_StaticRegisterNativesFSoundVariation()
	{
		UScriptStruct::DeferCppStructOps<FSoundVariation>(FName(TEXT("SoundVariation")));
	}
} ScriptStruct_SoundUtilities_StaticRegisterNativesFSoundVariation;
	struct Z_Construct_UScriptStruct_FSoundVariation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundWave_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SoundWave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProbabilityWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ProbabilityWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VolumeRange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PitchRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PitchRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundVariation_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundVariation_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundVariation>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_SoundWave_MetaData[] = {
		{ "Category", "SoundVariation" },
		{ "Comment", "// The sound wave asset to use for this variation\n" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
		{ "ToolTip", "The sound wave asset to use for this variation" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_SoundWave = { "SoundWave", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundVariation, SoundWave), Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_SoundWave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_SoundWave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_ProbabilityWeight_MetaData[] = {
		{ "Category", "Synth|Preset" },
		{ "Comment", "// The probability weight to use for this variation\n" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
		{ "ToolTip", "The probability weight to use for this variation" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_ProbabilityWeight = { "ProbabilityWeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundVariation, ProbabilityWeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_ProbabilityWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_ProbabilityWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_VolumeRange_MetaData[] = {
		{ "Category", "Synth|Preset" },
		{ "Comment", "// The volume range to use for this variation\n" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
		{ "ToolTip", "The volume range to use for this variation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_VolumeRange = { "VolumeRange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundVariation, VolumeRange), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_VolumeRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_VolumeRange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_PitchRange_MetaData[] = {
		{ "Category", "Synth|Preset" },
		{ "Comment", "// The pitch range to use for this variation\n" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
		{ "ToolTip", "The pitch range to use for this variation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_PitchRange = { "PitchRange", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundVariation, PitchRange), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_PitchRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_PitchRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundVariation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_SoundWave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_ProbabilityWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_VolumeRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundVariation_Statics::NewProp_PitchRange,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundVariation_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SoundUtilities,
		nullptr,
		&NewStructOps,
		"SoundVariation",
		sizeof(FSoundVariation),
		alignof(FSoundVariation),
		Z_Construct_UScriptStruct_FSoundVariation_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundVariation_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundVariation_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundVariation_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundVariation()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundVariation_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SoundUtilities();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundVariation"), sizeof(FSoundVariation), Get_Z_Construct_UScriptStruct_FSoundVariation_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundVariation_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundVariation_Hash() { return 1207423870U; }
	void USoundSimple::StaticRegisterNativesUSoundSimple()
	{
	}
	UClass* Z_Construct_UClass_USoundSimple_NoRegister()
	{
		return USoundSimple::StaticClass();
	}
	struct Z_Construct_UClass_USoundSimple_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Variations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Variations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Variations;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoundWave_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SoundWave;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundSimple_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundBase,
		(UObject* (*)())Z_Construct_UPackage__Script_SoundUtilities,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundSimple_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Sound" },
		{ "Comment", "// Class which contains a simple list of sound wave variations\n" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "SoundSimple.h" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
		{ "ToolTip", "Class which contains a simple list of sound wave variations" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundSimple_Statics::NewProp_Variations_Inner = { "Variations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundVariation, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundSimple_Statics::NewProp_Variations_MetaData[] = {
		{ "Category", "Variations" },
		{ "Comment", "// List of variations for the simple sound\n" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
		{ "ToolTip", "List of variations for the simple sound" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USoundSimple_Statics::NewProp_Variations = { "Variations", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundSimple, Variations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USoundSimple_Statics::NewProp_Variations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundSimple_Statics::NewProp_Variations_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundSimple_Statics::NewProp_SoundWave_MetaData[] = {
		{ "Comment", "// The current chosen sound wave\n" },
		{ "ModuleRelativePath", "Public/SoundSimple.h" },
		{ "ToolTip", "The current chosen sound wave" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USoundSimple_Statics::NewProp_SoundWave = { "SoundWave", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundSimple, SoundWave), Z_Construct_UClass_USoundWave_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USoundSimple_Statics::NewProp_SoundWave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundSimple_Statics::NewProp_SoundWave_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundSimple_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundSimple_Statics::NewProp_Variations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundSimple_Statics::NewProp_Variations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundSimple_Statics::NewProp_SoundWave,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundSimple_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundSimple>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundSimple_Statics::ClassParams = {
		&USoundSimple::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundSimple_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundSimple_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundSimple_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundSimple_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundSimple()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundSimple_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundSimple, 4223739809);
	template<> SOUNDUTILITIES_API UClass* StaticClass<USoundSimple>()
	{
		return USoundSimple::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundSimple(Z_Construct_UClass_USoundSimple, &USoundSimple::StaticClass, TEXT("/Script/SoundUtilities"), TEXT("USoundSimple"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundSimple);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(USoundSimple)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
