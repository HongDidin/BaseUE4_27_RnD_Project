// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOUNDUTILITIESEDITOR_SoundSimpleFactory_generated_h
#error "SoundSimpleFactory.generated.h already included, missing '#pragma once' in SoundSimpleFactory.h"
#endif
#define SOUNDUTILITIESEDITOR_SoundSimpleFactory_generated_h

#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_RPC_WRAPPERS
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundSimpleFactory(); \
	friend struct Z_Construct_UClass_USoundSimpleFactory_Statics; \
public: \
	DECLARE_CLASS(USoundSimpleFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundUtilitiesEditor"), SOUNDUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(USoundSimpleFactory)


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSoundSimpleFactory(); \
	friend struct Z_Construct_UClass_USoundSimpleFactory_Statics; \
public: \
	DECLARE_CLASS(USoundSimpleFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundUtilitiesEditor"), SOUNDUTILITIESEDITOR_API) \
	DECLARE_SERIALIZER(USoundSimpleFactory)


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SOUNDUTILITIESEDITOR_API USoundSimpleFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundSimpleFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOUNDUTILITIESEDITOR_API, USoundSimpleFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundSimpleFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOUNDUTILITIESEDITOR_API USoundSimpleFactory(USoundSimpleFactory&&); \
	SOUNDUTILITIESEDITOR_API USoundSimpleFactory(const USoundSimpleFactory&); \
public:


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SOUNDUTILITIESEDITOR_API USoundSimpleFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOUNDUTILITIESEDITOR_API USoundSimpleFactory(USoundSimpleFactory&&); \
	SOUNDUTILITIESEDITOR_API USoundSimpleFactory(const USoundSimpleFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOUNDUTILITIESEDITOR_API, USoundSimpleFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundSimpleFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundSimpleFactory)


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_12_PROLOG
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_INCLASS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundSimpleFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDUTILITIESEDITOR_API UClass* StaticClass<class USoundSimpleFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilitiesEditor_Private_SoundSimpleFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
