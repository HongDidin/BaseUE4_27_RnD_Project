// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector2D;
#ifdef SOUNDUTILITIES_SoundUtilities_generated_h
#error "SoundUtilities.generated.h already included, missing '#pragma once' in SoundUtilities.h"
#endif
#define SOUNDUTILITIES_SoundUtilities_generated_h

#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetQFromBandwidth); \
	DECLARE_FUNCTION(execGetBandwidthFromQ); \
	DECLARE_FUNCTION(execGetFrequencyMultiplierFromSemitones); \
	DECLARE_FUNCTION(execGetLinearFrequencyClamped); \
	DECLARE_FUNCTION(execGetLogFrequencyClamped); \
	DECLARE_FUNCTION(execConvertDecibelsToLinear); \
	DECLARE_FUNCTION(execConvertLinearToDecibels); \
	DECLARE_FUNCTION(execGetGainFromMidiVelocity); \
	DECLARE_FUNCTION(execGetPitchScaleFromMIDIPitch); \
	DECLARE_FUNCTION(execGetMIDIPitchFromFrequency); \
	DECLARE_FUNCTION(execGetFrequencyFromMIDIPitch); \
	DECLARE_FUNCTION(execGetBeatTempo);


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetQFromBandwidth); \
	DECLARE_FUNCTION(execGetBandwidthFromQ); \
	DECLARE_FUNCTION(execGetFrequencyMultiplierFromSemitones); \
	DECLARE_FUNCTION(execGetLinearFrequencyClamped); \
	DECLARE_FUNCTION(execGetLogFrequencyClamped); \
	DECLARE_FUNCTION(execConvertDecibelsToLinear); \
	DECLARE_FUNCTION(execConvertLinearToDecibels); \
	DECLARE_FUNCTION(execGetGainFromMidiVelocity); \
	DECLARE_FUNCTION(execGetPitchScaleFromMIDIPitch); \
	DECLARE_FUNCTION(execGetMIDIPitchFromFrequency); \
	DECLARE_FUNCTION(execGetFrequencyFromMIDIPitch); \
	DECLARE_FUNCTION(execGetBeatTempo);


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundUtilitiesBPFunctionLibrary(); \
	friend struct Z_Construct_UClass_USoundUtilitiesBPFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(USoundUtilitiesBPFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundUtilities"), NO_API) \
	DECLARE_SERIALIZER(USoundUtilitiesBPFunctionLibrary)


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSoundUtilitiesBPFunctionLibrary(); \
	friend struct Z_Construct_UClass_USoundUtilitiesBPFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(USoundUtilitiesBPFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SoundUtilities"), NO_API) \
	DECLARE_SERIALIZER(USoundUtilitiesBPFunctionLibrary)


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundUtilitiesBPFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundUtilitiesBPFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundUtilitiesBPFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundUtilitiesBPFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundUtilitiesBPFunctionLibrary(USoundUtilitiesBPFunctionLibrary&&); \
	NO_API USoundUtilitiesBPFunctionLibrary(const USoundUtilitiesBPFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundUtilitiesBPFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundUtilitiesBPFunctionLibrary(USoundUtilitiesBPFunctionLibrary&&); \
	NO_API USoundUtilitiesBPFunctionLibrary(const USoundUtilitiesBPFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundUtilitiesBPFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundUtilitiesBPFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundUtilitiesBPFunctionLibrary)


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_11_PROLOG
#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_INCLASS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOUNDUTILITIES_API UClass* StaticClass<class USoundUtilitiesBPFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_SoundUtilities_Source_SoundUtilities_Public_SoundUtilities_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
