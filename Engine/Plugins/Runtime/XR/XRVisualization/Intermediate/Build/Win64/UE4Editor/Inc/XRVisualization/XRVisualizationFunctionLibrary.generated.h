// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FXRMotionControllerData;
struct FXRHMDData;
#ifdef XRVISUALIZATION_XRVisualizationFunctionLibrary_generated_h
#error "XRVisualizationFunctionLibrary.generated.h already included, missing '#pragma once' in XRVisualizationFunctionLibrary.h"
#endif
#define XRVISUALIZATION_XRVisualizationFunctionLibrary_generated_h

#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUXRVisualizationLoadHelper(); \
	friend struct Z_Construct_UClass_UXRVisualizationLoadHelper_Statics; \
public: \
	DECLARE_CLASS(UXRVisualizationLoadHelper, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/XRVisualization"), NO_API) \
	DECLARE_SERIALIZER(UXRVisualizationLoadHelper)


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUXRVisualizationLoadHelper(); \
	friend struct Z_Construct_UClass_UXRVisualizationLoadHelper_Statics; \
public: \
	DECLARE_CLASS(UXRVisualizationLoadHelper, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/XRVisualization"), NO_API) \
	DECLARE_SERIALIZER(UXRVisualizationLoadHelper)


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UXRVisualizationLoadHelper(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UXRVisualizationLoadHelper) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UXRVisualizationLoadHelper); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UXRVisualizationLoadHelper); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UXRVisualizationLoadHelper(UXRVisualizationLoadHelper&&); \
	NO_API UXRVisualizationLoadHelper(const UXRVisualizationLoadHelper&); \
public:


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UXRVisualizationLoadHelper(UXRVisualizationLoadHelper&&); \
	NO_API UXRVisualizationLoadHelper(const UXRVisualizationLoadHelper&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UXRVisualizationLoadHelper); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UXRVisualizationLoadHelper); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UXRVisualizationLoadHelper)


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_11_PROLOG
#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_INCLASS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> XRVISUALIZATION_API UClass* StaticClass<class UXRVisualizationLoadHelper>();

#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_SPARSE_DATA
#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execRenderMotionController); \
	DECLARE_FUNCTION(execRenderHMD);


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execRenderMotionController); \
	DECLARE_FUNCTION(execRenderHMD);


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUXRVisualizationFunctionLibrary(); \
	friend struct Z_Construct_UClass_UXRVisualizationFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UXRVisualizationFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/XRVisualization"), NO_API) \
	DECLARE_SERIALIZER(UXRVisualizationFunctionLibrary)


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUXRVisualizationFunctionLibrary(); \
	friend struct Z_Construct_UClass_UXRVisualizationFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UXRVisualizationFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/XRVisualization"), NO_API) \
	DECLARE_SERIALIZER(UXRVisualizationFunctionLibrary)


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UXRVisualizationFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UXRVisualizationFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UXRVisualizationFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UXRVisualizationFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UXRVisualizationFunctionLibrary(UXRVisualizationFunctionLibrary&&); \
	NO_API UXRVisualizationFunctionLibrary(const UXRVisualizationFunctionLibrary&); \
public:


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UXRVisualizationFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UXRVisualizationFunctionLibrary(UXRVisualizationFunctionLibrary&&); \
	NO_API UXRVisualizationFunctionLibrary(const UXRVisualizationFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UXRVisualizationFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UXRVisualizationFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UXRVisualizationFunctionLibrary)


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LoadHelper() { return STRUCT_OFFSET(UXRVisualizationFunctionLibrary, LoadHelper); }


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_30_PROLOG
#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_SPARSE_DATA \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_RPC_WRAPPERS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_INCLASS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_SPARSE_DATA \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> XRVISUALIZATION_API UClass* StaticClass<class UXRVisualizationFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_XR_XRVisualization_Source_XRVisualization_Public_XRVisualizationFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
