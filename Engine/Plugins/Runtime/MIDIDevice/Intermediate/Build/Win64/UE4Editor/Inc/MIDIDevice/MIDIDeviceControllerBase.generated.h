// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MIDIDEVICE_MIDIDeviceControllerBase_generated_h
#error "MIDIDeviceControllerBase.generated.h already included, missing '#pragma once' in MIDIDeviceControllerBase.h"
#endif
#define MIDIDEVICE_MIDIDeviceControllerBase_generated_h

#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMIDIDeviceControllerBase(); \
	friend struct Z_Construct_UClass_UMIDIDeviceControllerBase_Statics; \
public: \
	DECLARE_CLASS(UMIDIDeviceControllerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MIDIDevice"), NO_API) \
	DECLARE_SERIALIZER(UMIDIDeviceControllerBase)


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUMIDIDeviceControllerBase(); \
	friend struct Z_Construct_UClass_UMIDIDeviceControllerBase_Statics; \
public: \
	DECLARE_CLASS(UMIDIDeviceControllerBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MIDIDevice"), NO_API) \
	DECLARE_SERIALIZER(UMIDIDeviceControllerBase)


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMIDIDeviceControllerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMIDIDeviceControllerBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMIDIDeviceControllerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMIDIDeviceControllerBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMIDIDeviceControllerBase(UMIDIDeviceControllerBase&&); \
	NO_API UMIDIDeviceControllerBase(const UMIDIDeviceControllerBase&); \
public:


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMIDIDeviceControllerBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMIDIDeviceControllerBase(UMIDIDeviceControllerBase&&); \
	NO_API UMIDIDeviceControllerBase(const UMIDIDeviceControllerBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMIDIDeviceControllerBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMIDIDeviceControllerBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMIDIDeviceControllerBase)


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_10_PROLOG
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_INCLASS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIDIDEVICE_API UClass* StaticClass<class UMIDIDeviceControllerBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceControllerBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
