// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MIDIDevice/Public/MIDIDeviceControllerBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMIDIDeviceControllerBase() {}
// Cross Module References
	MIDIDEVICE_API UClass* Z_Construct_UClass_UMIDIDeviceControllerBase_NoRegister();
	MIDIDEVICE_API UClass* Z_Construct_UClass_UMIDIDeviceControllerBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MIDIDevice();
// End Cross Module References
	void UMIDIDeviceControllerBase::StaticRegisterNativesUMIDIDeviceControllerBase()
	{
	}
	UClass* Z_Construct_UClass_UMIDIDeviceControllerBase_NoRegister()
	{
		return UMIDIDeviceControllerBase::StaticClass();
	}
	struct Z_Construct_UClass_UMIDIDeviceControllerBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMIDIDeviceControllerBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MIDIDevice,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMIDIDeviceControllerBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Common functionality for the different MIDI Device Controllers. */" },
		{ "IncludePath", "MIDIDeviceControllerBase.h" },
		{ "ModuleRelativePath", "Public/MIDIDeviceControllerBase.h" },
		{ "ToolTip", "Common functionality for the different MIDI Device Controllers." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMIDIDeviceControllerBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMIDIDeviceControllerBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMIDIDeviceControllerBase_Statics::ClassParams = {
		&UMIDIDeviceControllerBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMIDIDeviceControllerBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMIDIDeviceControllerBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMIDIDeviceControllerBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMIDIDeviceControllerBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMIDIDeviceControllerBase, 1517637392);
	template<> MIDIDEVICE_API UClass* StaticClass<UMIDIDeviceControllerBase>()
	{
		return UMIDIDeviceControllerBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMIDIDeviceControllerBase(Z_Construct_UClass_UMIDIDeviceControllerBase, &UMIDIDeviceControllerBase::StaticClass, TEXT("/Script/MIDIDevice"), TEXT("UMIDIDeviceControllerBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMIDIDeviceControllerBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
