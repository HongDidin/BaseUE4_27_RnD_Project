// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MIDIDevice/Public/MIDIDeviceController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMIDIDeviceController() {}
// Cross Module References
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MIDIDevice();
	MIDIDEVICE_API UClass* Z_Construct_UClass_UMIDIDeviceController_NoRegister();
	MIDIDEVICE_API UEnum* Z_Construct_UEnum_MIDIDevice_EMIDIEventType();
	MIDIDEVICE_API UClass* Z_Construct_UClass_UMIDIDeviceController();
	MIDIDEVICE_API UClass* Z_Construct_UClass_UMIDIDeviceControllerBase();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics
	{
		struct _Script_MIDIDevice_eventOnMIDIEvent_Parms
		{
			UMIDIDeviceController* MIDIDeviceController;
			int32 Timestamp;
			EMIDIEventType EventType;
			int32 Channel;
			int32 ControlID;
			int32 Velocity;
			int32 RawEventType;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MIDIDeviceController;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Timestamp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventType_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventType;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Channel;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ControlID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Velocity;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RawEventType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_MIDIDeviceController = { "MIDIDeviceController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MIDIDevice_eventOnMIDIEvent_Parms, MIDIDeviceController), Z_Construct_UClass_UMIDIDeviceController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_Timestamp = { "Timestamp", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MIDIDevice_eventOnMIDIEvent_Parms, Timestamp), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_EventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_EventType = { "EventType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MIDIDevice_eventOnMIDIEvent_Parms, EventType), Z_Construct_UEnum_MIDIDevice_EMIDIEventType, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MIDIDevice_eventOnMIDIEvent_Parms, Channel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_ControlID = { "ControlID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MIDIDevice_eventOnMIDIEvent_Parms, ControlID), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_Velocity = { "Velocity", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MIDIDevice_eventOnMIDIEvent_Parms, Velocity), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_RawEventType = { "RawEventType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MIDIDevice_eventOnMIDIEvent_Parms, RawEventType), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_MIDIDeviceController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_Timestamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_EventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_EventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_ControlID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_Velocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::NewProp_RawEventType,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Callback function for received MIDI events */" },
		{ "ModuleRelativePath", "Public/MIDIDeviceController.h" },
		{ "ToolTip", "Callback function for received MIDI events" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MIDIDevice, nullptr, "OnMIDIEvent__DelegateSignature", nullptr, nullptr, sizeof(_Script_MIDIDevice_eventOnMIDIEvent_Parms), Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EMIDIEventType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MIDIDevice_EMIDIEventType, Z_Construct_UPackage__Script_MIDIDevice(), TEXT("EMIDIEventType"));
		}
		return Singleton;
	}
	template<> MIDIDEVICE_API UEnum* StaticEnum<EMIDIEventType>()
	{
		return EMIDIEventType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMIDIEventType(EMIDIEventType_StaticEnum, TEXT("/Script/MIDIDevice"), TEXT("EMIDIEventType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MIDIDevice_EMIDIEventType_Hash() { return 2244012993U; }
	UEnum* Z_Construct_UEnum_MIDIDevice_EMIDIEventType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MIDIDevice();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMIDIEventType"), 0, Get_Z_Construct_UEnum_MIDIDevice_EMIDIEventType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMIDIEventType::Unknown", (int64)EMIDIEventType::Unknown },
				{ "EMIDIEventType::NoteOff", (int64)EMIDIEventType::NoteOff },
				{ "EMIDIEventType::NoteOn", (int64)EMIDIEventType::NoteOn },
				{ "EMIDIEventType::NoteAfterTouch", (int64)EMIDIEventType::NoteAfterTouch },
				{ "EMIDIEventType::ControlChange", (int64)EMIDIEventType::ControlChange },
				{ "EMIDIEventType::ProgramChange", (int64)EMIDIEventType::ProgramChange },
				{ "EMIDIEventType::ChannelAfterTouch", (int64)EMIDIEventType::ChannelAfterTouch },
				{ "EMIDIEventType::PitchBend", (int64)EMIDIEventType::PitchBend },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ChannelAfterTouch.Comment", "/** Channel pressure value.  This is sent after a channel button 'bottoms out' for devices that support it.  Velocity will contain the pressure value. */" },
				{ "ChannelAfterTouch.Name", "EMIDIEventType::ChannelAfterTouch" },
				{ "ChannelAfterTouch.ToolTip", "Channel pressure value.  This is sent after a channel button 'bottoms out' for devices that support it.  Velocity will contain the pressure value." },
				{ "ControlChange.Comment", "/** This is sent for things like pedals when their controller state changes.  Velocity will contain the new value for the controller.  This event also is used for 'Channel Mode Changes' (Channels between 120-127), which encompass a variety of different features.  For those events, you'll need to interpret the values yourself. */" },
				{ "ControlChange.Name", "EMIDIEventType::ControlChange" },
				{ "ControlChange.ToolTip", "This is sent for things like pedals when their controller state changes.  Velocity will contain the new value for the controller.  This event also is used for 'Channel Mode Changes' (Channels between 120-127), which encompass a variety of different features.  For those events, you'll need to interpret the values yourself." },
				{ "ModuleRelativePath", "Public/MIDIDeviceController.h" },
				{ "NoteAfterTouch.Comment", "/** Polyphonic key pressure.  This is sent after a key 'bottoms out' for devices that support it.  Velocity will contain the pressure value. */" },
				{ "NoteAfterTouch.Name", "EMIDIEventType::NoteAfterTouch" },
				{ "NoteAfterTouch.ToolTip", "Polyphonic key pressure.  This is sent after a key 'bottoms out' for devices that support it.  Velocity will contain the pressure value." },
				{ "NoteOff.Comment", "/** Note is released.  Velocity will contain the key pressure for devices that support that. */" },
				{ "NoteOff.Name", "EMIDIEventType::NoteOff" },
				{ "NoteOff.ToolTip", "Note is released.  Velocity will contain the key pressure for devices that support that." },
				{ "NoteOn.Comment", "/** Note is pressed down.  Velocity will contain the key pressure for devices that support that. */" },
				{ "NoteOn.Name", "EMIDIEventType::NoteOn" },
				{ "NoteOn.ToolTip", "Note is pressed down.  Velocity will contain the key pressure for devices that support that." },
				{ "PitchBend.Comment", "/** For devices with levers or wheels, this indicates a change of state.  The data is interpreted a bit differently here, where the new value is actually 14-bits that contained within both the Control ID and Velocity */// @todo midi: Ideally set velocity to correct values in this case so Blueprints don't have to\n" },
				{ "PitchBend.Name", "EMIDIEventType::PitchBend" },
				{ "PitchBend.ToolTip", "For devices with levers or wheels, this indicates a change of state.  The data is interpreted a bit differently here, where the new value is actually 14-bits that contained within both the Control ID and Velocity // @todo midi: Ideally set velocity to correct values in this case so Blueprints don't have to" },
				{ "ProgramChange.Comment", "/** This is sent for some devices that support changing patches.  Velocity is usually ignored */" },
				{ "ProgramChange.Name", "EMIDIEventType::ProgramChange" },
				{ "ProgramChange.ToolTip", "This is sent for some devices that support changing patches.  Velocity is usually ignored" },
				{ "Unknown.Comment", "/** Unrecognized MIDI event type.  You can look at Raw Event Type to see what it is. */" },
				{ "Unknown.Hidden", "" },
				{ "Unknown.Name", "EMIDIEventType::Unknown" },
				{ "Unknown.ToolTip", "Unrecognized MIDI event type.  You can look at Raw Event Type to see what it is." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MIDIDevice,
				nullptr,
				"EMIDIEventType",
				"EMIDIEventType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMIDIDeviceController::StaticRegisterNativesUMIDIDeviceController()
	{
	}
	UClass* Z_Construct_UClass_UMIDIDeviceController_NoRegister()
	{
		return UMIDIDeviceController::StaticClass();
	}
	struct Z_Construct_UClass_UMIDIDeviceController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnMIDIEvent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMIDIEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMIDIDeviceController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMIDIDeviceControllerBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MIDIDevice,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMIDIDeviceController_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MIDIDeviceController.h" },
		{ "ModuleRelativePath", "Public/MIDIDeviceController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_OnMIDIEvent_MetaData[] = {
		{ "Category", "MIDI Device Controller" },
		{ "Comment", "/** Register with this to find out about incoming MIDI events from this device */" },
		{ "ModuleRelativePath", "Public/MIDIDeviceController.h" },
		{ "ToolTip", "Register with this to find out about incoming MIDI events from this device" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_OnMIDIEvent = { "OnMIDIEvent", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMIDIDeviceController, OnMIDIEvent), Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_OnMIDIEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_OnMIDIEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceID_MetaData[] = {
		{ "Category", "MIDI Device Controller" },
		{ "Comment", "/** The unique ID of this device */" },
		{ "ModuleRelativePath", "Public/MIDIDeviceController.h" },
		{ "ToolTip", "The unique ID of this device" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMIDIDeviceController, DeviceID), METADATA_PARAMS(Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "MIDI Device Controller" },
		{ "Comment", "/** The name of this device.  This name comes from the MIDI hardware, any might not be unique */" },
		{ "ModuleRelativePath", "Public/MIDIDeviceController.h" },
		{ "ToolTip", "The name of this device.  This name comes from the MIDI hardware, any might not be unique" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMIDIDeviceController, DeviceName), METADATA_PARAMS(Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMIDIDeviceController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_OnMIDIEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMIDIDeviceController_Statics::NewProp_DeviceName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMIDIDeviceController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMIDIDeviceController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMIDIDeviceController_Statics::ClassParams = {
		&UMIDIDeviceController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMIDIDeviceController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMIDIDeviceController_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMIDIDeviceController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMIDIDeviceController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMIDIDeviceController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMIDIDeviceController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMIDIDeviceController, 2185487733);
	template<> MIDIDEVICE_API UClass* StaticClass<UMIDIDeviceController>()
	{
		return UMIDIDeviceController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMIDIDeviceController(Z_Construct_UClass_UMIDIDeviceController, &UMIDIDeviceController::StaticClass, TEXT("/Script/MIDIDevice"), TEXT("UMIDIDeviceController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMIDIDeviceController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
