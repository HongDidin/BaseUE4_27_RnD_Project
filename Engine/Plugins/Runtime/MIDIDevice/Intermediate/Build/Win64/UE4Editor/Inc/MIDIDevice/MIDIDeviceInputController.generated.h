// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMIDIDeviceInputController;
#ifdef MIDIDEVICE_MIDIDeviceInputController_generated_h
#error "MIDIDeviceInputController.generated.h already included, missing '#pragma once' in MIDIDeviceInputController.h"
#endif
#define MIDIDEVICE_MIDIDeviceInputController_generated_h

#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_33_DELEGATE \
struct _Script_MIDIDevice_eventOnMIDIChannelAftertouch_Parms \
{ \
	UMIDIDeviceInputController* MIDIDeviceController; \
	int32 Timestamp; \
	int32 Channel; \
	int32 Amount; \
}; \
static inline void FOnMIDIChannelAftertouch_DelegateWrapper(const FMulticastScriptDelegate& OnMIDIChannelAftertouch, UMIDIDeviceInputController* MIDIDeviceController, int32 Timestamp, int32 Channel, int32 Amount) \
{ \
	_Script_MIDIDevice_eventOnMIDIChannelAftertouch_Parms Parms; \
	Parms.MIDIDeviceController=MIDIDeviceController; \
	Parms.Timestamp=Timestamp; \
	Parms.Channel=Channel; \
	Parms.Amount=Amount; \
	OnMIDIChannelAftertouch.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_30_DELEGATE \
struct _Script_MIDIDevice_eventOnMIDIProgramChange_Parms \
{ \
	UMIDIDeviceInputController* MIDIDeviceController; \
	int32 Timestamp; \
	int32 Channel; \
	int32 ControlID; \
	int32 Velocity; \
}; \
static inline void FOnMIDIProgramChange_DelegateWrapper(const FMulticastScriptDelegate& OnMIDIProgramChange, UMIDIDeviceInputController* MIDIDeviceController, int32 Timestamp, int32 Channel, int32 ControlID, int32 Velocity) \
{ \
	_Script_MIDIDevice_eventOnMIDIProgramChange_Parms Parms; \
	Parms.MIDIDeviceController=MIDIDeviceController; \
	Parms.Timestamp=Timestamp; \
	Parms.Channel=Channel; \
	Parms.ControlID=ControlID; \
	Parms.Velocity=Velocity; \
	OnMIDIProgramChange.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_27_DELEGATE \
struct _Script_MIDIDevice_eventOnMIDIControlChange_Parms \
{ \
	UMIDIDeviceInputController* MIDIDeviceController; \
	int32 Timestamp; \
	int32 Channel; \
	int32 Type; \
	int32 Value; \
}; \
static inline void FOnMIDIControlChange_DelegateWrapper(const FMulticastScriptDelegate& OnMIDIControlChange, UMIDIDeviceInputController* MIDIDeviceController, int32 Timestamp, int32 Channel, int32 Type, int32 Value) \
{ \
	_Script_MIDIDevice_eventOnMIDIControlChange_Parms Parms; \
	Parms.MIDIDeviceController=MIDIDeviceController; \
	Parms.Timestamp=Timestamp; \
	Parms.Channel=Channel; \
	Parms.Type=Type; \
	Parms.Value=Value; \
	OnMIDIControlChange.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_24_DELEGATE \
struct _Script_MIDIDevice_eventOnMIDIAftertouch_Parms \
{ \
	UMIDIDeviceInputController* MIDIDeviceController; \
	int32 Timestamp; \
	int32 Channel; \
	int32 Note; \
	int32 Amount; \
}; \
static inline void FOnMIDIAftertouch_DelegateWrapper(const FMulticastScriptDelegate& OnMIDIAftertouch, UMIDIDeviceInputController* MIDIDeviceController, int32 Timestamp, int32 Channel, int32 Note, int32 Amount) \
{ \
	_Script_MIDIDevice_eventOnMIDIAftertouch_Parms Parms; \
	Parms.MIDIDeviceController=MIDIDeviceController; \
	Parms.Timestamp=Timestamp; \
	Parms.Channel=Channel; \
	Parms.Note=Note; \
	Parms.Amount=Amount; \
	OnMIDIAftertouch.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_21_DELEGATE \
struct _Script_MIDIDevice_eventOnMIDIPitchBend_Parms \
{ \
	UMIDIDeviceInputController* MIDIDeviceController; \
	int32 Timestamp; \
	int32 Channel; \
	int32 Pitch; \
}; \
static inline void FOnMIDIPitchBend_DelegateWrapper(const FMulticastScriptDelegate& OnMIDIPitchBend, UMIDIDeviceInputController* MIDIDeviceController, int32 Timestamp, int32 Channel, int32 Pitch) \
{ \
	_Script_MIDIDevice_eventOnMIDIPitchBend_Parms Parms; \
	Parms.MIDIDeviceController=MIDIDeviceController; \
	Parms.Timestamp=Timestamp; \
	Parms.Channel=Channel; \
	Parms.Pitch=Pitch; \
	OnMIDIPitchBend.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_18_DELEGATE \
struct _Script_MIDIDevice_eventOnMIDINoteOff_Parms \
{ \
	UMIDIDeviceInputController* MIDIDeviceController; \
	int32 Timestamp; \
	int32 Channel; \
	int32 Note; \
	int32 Velocity; \
}; \
static inline void FOnMIDINoteOff_DelegateWrapper(const FMulticastScriptDelegate& OnMIDINoteOff, UMIDIDeviceInputController* MIDIDeviceController, int32 Timestamp, int32 Channel, int32 Note, int32 Velocity) \
{ \
	_Script_MIDIDevice_eventOnMIDINoteOff_Parms Parms; \
	Parms.MIDIDeviceController=MIDIDeviceController; \
	Parms.Timestamp=Timestamp; \
	Parms.Channel=Channel; \
	Parms.Note=Note; \
	Parms.Velocity=Velocity; \
	OnMIDINoteOff.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_15_DELEGATE \
struct _Script_MIDIDevice_eventOnMIDINoteOn_Parms \
{ \
	UMIDIDeviceInputController* MIDIDeviceController; \
	int32 Timestamp; \
	int32 Channel; \
	int32 Note; \
	int32 Velocity; \
}; \
static inline void FOnMIDINoteOn_DelegateWrapper(const FMulticastScriptDelegate& OnMIDINoteOn, UMIDIDeviceInputController* MIDIDeviceController, int32 Timestamp, int32 Channel, int32 Note, int32 Velocity) \
{ \
	_Script_MIDIDevice_eventOnMIDINoteOn_Parms Parms; \
	Parms.MIDIDeviceController=MIDIDeviceController; \
	Parms.Timestamp=Timestamp; \
	Parms.Channel=Channel; \
	Parms.Note=Note; \
	Parms.Velocity=Velocity; \
	OnMIDINoteOn.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_SPARSE_DATA
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_RPC_WRAPPERS
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMIDIDeviceInputController(); \
	friend struct Z_Construct_UClass_UMIDIDeviceInputController_Statics; \
public: \
	DECLARE_CLASS(UMIDIDeviceInputController, UMIDIDeviceControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MIDIDevice"), NO_API) \
	DECLARE_SERIALIZER(UMIDIDeviceInputController)


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUMIDIDeviceInputController(); \
	friend struct Z_Construct_UClass_UMIDIDeviceInputController_Statics; \
public: \
	DECLARE_CLASS(UMIDIDeviceInputController, UMIDIDeviceControllerBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MIDIDevice"), NO_API) \
	DECLARE_SERIALIZER(UMIDIDeviceInputController)


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMIDIDeviceInputController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMIDIDeviceInputController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMIDIDeviceInputController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMIDIDeviceInputController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMIDIDeviceInputController(UMIDIDeviceInputController&&); \
	NO_API UMIDIDeviceInputController(const UMIDIDeviceInputController&); \
public:


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMIDIDeviceInputController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMIDIDeviceInputController(UMIDIDeviceInputController&&); \
	NO_API UMIDIDeviceInputController(const UMIDIDeviceInputController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMIDIDeviceInputController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMIDIDeviceInputController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMIDIDeviceInputController)


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DeviceID() { return STRUCT_OFFSET(UMIDIDeviceInputController, DeviceID); } \
	FORCEINLINE static uint32 __PPO__DeviceName() { return STRUCT_OFFSET(UMIDIDeviceInputController, DeviceName); }


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_39_PROLOG
#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_SPARSE_DATA \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_RPC_WRAPPERS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_INCLASS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_SPARSE_DATA \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h_42_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MIDIDEVICE_API UClass* StaticClass<class UMIDIDeviceInputController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_MIDIDevice_Source_MIDIDevice_Public_MIDIDeviceInputController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
