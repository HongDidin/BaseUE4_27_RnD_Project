// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMIDIDevice_init() {}
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature();
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDINoteOn__DelegateSignature();
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDINoteOff__DelegateSignature();
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIPitchBend__DelegateSignature();
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIAftertouch__DelegateSignature();
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIControlChange__DelegateSignature();
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIProgramChange__DelegateSignature();
	MIDIDEVICE_API UFunction* Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIChannelAftertouch__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MIDIDevice()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIEvent__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDINoteOn__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDINoteOff__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIPitchBend__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIAftertouch__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIControlChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIProgramChange__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_MIDIDevice_OnMIDIChannelAftertouch__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/MIDIDevice",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xB1D4BA50,
				0xE7FE7D05,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
