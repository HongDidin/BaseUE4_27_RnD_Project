// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioSynesthesiaEditor/Classes/AudioSynesthesiaNRTFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAudioSynesthesiaNRTFactory() {}
// Cross Module References
	AUDIOSYNESTHESIAEDITOR_API UClass* Z_Construct_UClass_UAudioSynesthesiaNRTFactory_NoRegister();
	AUDIOSYNESTHESIAEDITOR_API UClass* Z_Construct_UClass_UAudioSynesthesiaNRTFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AudioSynesthesiaEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	AUDIOSYNESTHESIA_API UClass* Z_Construct_UClass_UAudioSynesthesiaNRT_NoRegister();
// End Cross Module References
	void UAudioSynesthesiaNRTFactory::StaticRegisterNativesUAudioSynesthesiaNRTFactory()
	{
	}
	UClass* Z_Construct_UClass_UAudioSynesthesiaNRTFactory_NoRegister()
	{
		return UAudioSynesthesiaNRTFactory::StaticClass();
	}
	struct Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioSynesthesiaNRTClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_AudioSynesthesiaNRTClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioSynesthesiaEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UAudioSynesthesiaNRTFactory\n *\n * UAudioSynesthesiaNRTFactory implements the factory for UAudioSynesthesiaNRT assets.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "AudioSynesthesiaNRTFactory.h" },
		{ "ModuleRelativePath", "Classes/AudioSynesthesiaNRTFactory.h" },
		{ "ToolTip", "UAudioSynesthesiaNRTFactory\n\nUAudioSynesthesiaNRTFactory implements the factory for UAudioSynesthesiaNRT assets." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::NewProp_AudioSynesthesiaNRTClass_MetaData[] = {
		{ "Category", "AudioSynesthesiaFactory" },
		{ "Comment", "/** The type of audio analyzer settings that will be created */" },
		{ "ModuleRelativePath", "Classes/AudioSynesthesiaNRTFactory.h" },
		{ "ToolTip", "The type of audio analyzer settings that will be created" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::NewProp_AudioSynesthesiaNRTClass = { "AudioSynesthesiaNRTClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAudioSynesthesiaNRTFactory, AudioSynesthesiaNRTClass), Z_Construct_UClass_UAudioSynesthesiaNRT_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::NewProp_AudioSynesthesiaNRTClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::NewProp_AudioSynesthesiaNRTClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::NewProp_AudioSynesthesiaNRTClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAudioSynesthesiaNRTFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::ClassParams = {
		&UAudioSynesthesiaNRTFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAudioSynesthesiaNRTFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAudioSynesthesiaNRTFactory, 2966968520);
	template<> AUDIOSYNESTHESIAEDITOR_API UClass* StaticClass<UAudioSynesthesiaNRTFactory>()
	{
		return UAudioSynesthesiaNRTFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAudioSynesthesiaNRTFactory(Z_Construct_UClass_UAudioSynesthesiaNRTFactory, &UAudioSynesthesiaNRTFactory::StaticClass, TEXT("/Script/AudioSynesthesiaEditor"), TEXT("UAudioSynesthesiaNRTFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAudioSynesthesiaNRTFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
