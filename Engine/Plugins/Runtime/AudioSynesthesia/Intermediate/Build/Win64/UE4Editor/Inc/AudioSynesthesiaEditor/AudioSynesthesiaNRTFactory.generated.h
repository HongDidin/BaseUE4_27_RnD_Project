// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOSYNESTHESIAEDITOR_AudioSynesthesiaNRTFactory_generated_h
#error "AudioSynesthesiaNRTFactory.generated.h already included, missing '#pragma once' in AudioSynesthesiaNRTFactory.h"
#endif
#define AUDIOSYNESTHESIAEDITOR_AudioSynesthesiaNRTFactory_generated_h

#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAudioSynesthesiaNRTFactory(); \
	friend struct Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics; \
public: \
	DECLARE_CLASS(UAudioSynesthesiaNRTFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioSynesthesiaEditor"), AUDIOSYNESTHESIAEDITOR_API) \
	DECLARE_SERIALIZER(UAudioSynesthesiaNRTFactory)


#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUAudioSynesthesiaNRTFactory(); \
	friend struct Z_Construct_UClass_UAudioSynesthesiaNRTFactory_Statics; \
public: \
	DECLARE_CLASS(UAudioSynesthesiaNRTFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioSynesthesiaEditor"), AUDIOSYNESTHESIAEDITOR_API) \
	DECLARE_SERIALIZER(UAudioSynesthesiaNRTFactory)


#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOSYNESTHESIAEDITOR_API UAudioSynesthesiaNRTFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioSynesthesiaNRTFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOSYNESTHESIAEDITOR_API, UAudioSynesthesiaNRTFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioSynesthesiaNRTFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOSYNESTHESIAEDITOR_API UAudioSynesthesiaNRTFactory(UAudioSynesthesiaNRTFactory&&); \
	AUDIOSYNESTHESIAEDITOR_API UAudioSynesthesiaNRTFactory(const UAudioSynesthesiaNRTFactory&); \
public:


#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOSYNESTHESIAEDITOR_API UAudioSynesthesiaNRTFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOSYNESTHESIAEDITOR_API UAudioSynesthesiaNRTFactory(UAudioSynesthesiaNRTFactory&&); \
	AUDIOSYNESTHESIAEDITOR_API UAudioSynesthesiaNRTFactory(const UAudioSynesthesiaNRTFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOSYNESTHESIAEDITOR_API, UAudioSynesthesiaNRTFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioSynesthesiaNRTFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioSynesthesiaNRTFactory)


#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_13_PROLOG
#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_INCLASS \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AudioSynesthesiaNRTFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOSYNESTHESIAEDITOR_API UClass* StaticClass<class UAudioSynesthesiaNRTFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioSynesthesia_Source_AudioSynesthesiaEditor_Classes_AudioSynesthesiaNRTFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
