// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioSynesthesiaEditor/Classes/AudioSynesthesiaNRTSettingsFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAudioSynesthesiaNRTSettingsFactory() {}
// Cross Module References
	AUDIOSYNESTHESIAEDITOR_API UClass* Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_NoRegister();
	AUDIOSYNESTHESIAEDITOR_API UClass* Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AudioSynesthesiaEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	AUDIOSYNESTHESIA_API UClass* Z_Construct_UClass_UAudioSynesthesiaNRTSettings_NoRegister();
// End Cross Module References
	void UAudioSynesthesiaNRTSettingsFactory::StaticRegisterNativesUAudioSynesthesiaNRTSettingsFactory()
	{
	}
	UClass* Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_NoRegister()
	{
		return UAudioSynesthesiaNRTSettingsFactory::StaticClass();
	}
	struct Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioSynesthesiaNRTSettingsClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_AudioSynesthesiaNRTSettingsClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioSynesthesiaEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UAudioSynesthesiaNRTSettingsFactory\n *\n * UAudioSynesthesiaNRTSettingsFactory implements the factory for UAudioSynesthesiaNRTSettings assets.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "AudioSynesthesiaNRTSettingsFactory.h" },
		{ "ModuleRelativePath", "Classes/AudioSynesthesiaNRTSettingsFactory.h" },
		{ "ToolTip", "UAudioSynesthesiaNRTSettingsFactory\n\nUAudioSynesthesiaNRTSettingsFactory implements the factory for UAudioSynesthesiaNRTSettings assets." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::NewProp_AudioSynesthesiaNRTSettingsClass_MetaData[] = {
		{ "Category", "AudioSynesthesiaFactory" },
		{ "Comment", "/** The type of audio analyzer settings that will be created */" },
		{ "ModuleRelativePath", "Classes/AudioSynesthesiaNRTSettingsFactory.h" },
		{ "ToolTip", "The type of audio analyzer settings that will be created" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::NewProp_AudioSynesthesiaNRTSettingsClass = { "AudioSynesthesiaNRTSettingsClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAudioSynesthesiaNRTSettingsFactory, AudioSynesthesiaNRTSettingsClass), Z_Construct_UClass_UAudioSynesthesiaNRTSettings_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::NewProp_AudioSynesthesiaNRTSettingsClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::NewProp_AudioSynesthesiaNRTSettingsClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::NewProp_AudioSynesthesiaNRTSettingsClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAudioSynesthesiaNRTSettingsFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::ClassParams = {
		&UAudioSynesthesiaNRTSettingsFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAudioSynesthesiaNRTSettingsFactory, 2049000453);
	template<> AUDIOSYNESTHESIAEDITOR_API UClass* StaticClass<UAudioSynesthesiaNRTSettingsFactory>()
	{
		return UAudioSynesthesiaNRTSettingsFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAudioSynesthesiaNRTSettingsFactory(Z_Construct_UClass_UAudioSynesthesiaNRTSettingsFactory, &UAudioSynesthesiaNRTSettingsFactory::StaticClass, TEXT("/Script/AudioSynesthesiaEditor"), TEXT("UAudioSynesthesiaNRTSettingsFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAudioSynesthesiaNRTSettingsFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
