// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAREGISTRY_DataRegistrySettings_generated_h
#error "DataRegistrySettings.generated.h already included, missing '#pragma once' in DataRegistrySettings.h"
#endif
#define DATAREGISTRY_DataRegistrySettings_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataRegistrySettings(); \
	friend struct Z_Construct_UClass_UDataRegistrySettings_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDataRegistrySettings(); \
	friend struct Z_Construct_UClass_UDataRegistrySettings_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySettings(UDataRegistrySettings&&); \
	NO_API UDataRegistrySettings(const UDataRegistrySettings&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySettings(UDataRegistrySettings&&); \
	NO_API UDataRegistrySettings(const UDataRegistrySettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySettings)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_11_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UDataRegistrySettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
