// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistry/Public/DataRegistrySource_DataTable.h"
#include "DataRegistry/Public/DataRegistry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistrySource_DataTable() {}
// Cross Module References
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules();
	UPackage* Z_Construct_UPackage__Script_DataRegistry();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource_DataTable_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource_DataTable();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource_DataTable_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource_DataTable();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
class UScriptStruct* FDataRegistrySource_DataTableRules::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRY_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules, Z_Construct_UPackage__Script_DataRegistry(), TEXT("DataRegistrySource_DataTableRules"), sizeof(FDataRegistrySource_DataTableRules), Get_Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRY_API UScriptStruct* StaticStruct<FDataRegistrySource_DataTableRules>()
{
	return FDataRegistrySource_DataTableRules::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistrySource_DataTableRules(FDataRegistrySource_DataTableRules::StaticStruct, TEXT("/Script/DataRegistry"), TEXT("DataRegistrySource_DataTableRules"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistrySource_DataTableRules
{
	FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistrySource_DataTableRules()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistrySource_DataTableRules>(FName(TEXT("DataRegistrySource_DataTableRules")));
	}
} ScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistrySource_DataTableRules;
	struct Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPrecacheTable_MetaData[];
#endif
		static void NewProp_bPrecacheTable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPrecacheTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedTableKeepSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CachedTableKeepSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Rules struct for data table access */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Rules struct for data table access" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistrySource_DataTableRules>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_bPrecacheTable_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** True if the entire table should be loaded into memory when the source is loaded, false if the table is loaded on demand */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "True if the entire table should be loaded into memory when the source is loaded, false if the table is loaded on demand" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_bPrecacheTable_SetBit(void* Obj)
	{
		((FDataRegistrySource_DataTableRules*)Obj)->bPrecacheTable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_bPrecacheTable = { "bPrecacheTable", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDataRegistrySource_DataTableRules), &Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_bPrecacheTable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_bPrecacheTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_bPrecacheTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_CachedTableKeepSeconds_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Time in seconds to keep cached table alive if hard reference is off. 0 will release immediately, -1 will never release */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Time in seconds to keep cached table alive if hard reference is off. 0 will release immediately, -1 will never release" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_CachedTableKeepSeconds = { "CachedTableKeepSeconds", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistrySource_DataTableRules, CachedTableKeepSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_CachedTableKeepSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_CachedTableKeepSeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_bPrecacheTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::NewProp_CachedTableKeepSeconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
		nullptr,
		&NewStructOps,
		"DataRegistrySource_DataTableRules",
		sizeof(FDataRegistrySource_DataTableRules),
		alignof(FDataRegistrySource_DataTableRules),
		Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistrySource_DataTableRules"), sizeof(FDataRegistrySource_DataTableRules), Get_Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Hash() { return 1510758192U; }
	void UDataRegistrySource_DataTable::StaticRegisterNativesUDataRegistrySource_DataTable()
	{
	}
	UClass* Z_Construct_UClass_UDataRegistrySource_DataTable_NoRegister()
	{
		return UDataRegistrySource_DataTable::StaticClass();
	}
	struct Z_Construct_UClass_UDataRegistrySource_DataTable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SourceTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TableRules_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TableRules;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreloadTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreloadTable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataRegistrySource,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Data source that loads from a specific data table containing the same type of structs as the registry */" },
		{ "DisplayName", "DataTable Source" },
		{ "IncludePath", "DataRegistrySource_DataTable.h" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Data source that loads from a specific data table containing the same type of structs as the registry" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_SourceTable_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** What table to load from */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "What table to load from" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_SourceTable = { "SourceTable", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_DataTable, SourceTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_SourceTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_SourceTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_TableRules_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Access rules */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Access rules" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_TableRules = { "TableRules", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_DataTable, TableRules), Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_TableRules_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_TableRules_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_CachedTable_MetaData[] = {
		{ "Comment", "/** Hard ref to loaded table */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Hard ref to loaded table" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_CachedTable = { "CachedTable", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_DataTable, CachedTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_CachedTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_CachedTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_PreloadTable_MetaData[] = {
		{ "Comment", "/** Preload table ref, will be set if this is a hard source */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Preload table ref, will be set if this is a hard source" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_PreloadTable = { "PreloadTable", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_DataTable, PreloadTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_PreloadTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_PreloadTable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_SourceTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_TableRules,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_CachedTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::NewProp_PreloadTable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataRegistrySource_DataTable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::ClassParams = {
		&UDataRegistrySource_DataTable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::PropPointers),
		0,
		0x00B030A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataRegistrySource_DataTable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataRegistrySource_DataTable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataRegistrySource_DataTable, 1887352929);
	template<> DATAREGISTRY_API UClass* StaticClass<UDataRegistrySource_DataTable>()
	{
		return UDataRegistrySource_DataTable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataRegistrySource_DataTable(Z_Construct_UClass_UDataRegistrySource_DataTable, &UDataRegistrySource_DataTable::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UDataRegistrySource_DataTable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataRegistrySource_DataTable);
	void UMetaDataRegistrySource_DataTable::StaticRegisterNativesUMetaDataRegistrySource_DataTable()
	{
	}
	UClass* Z_Construct_UClass_UMetaDataRegistrySource_DataTable_NoRegister()
	{
		return UMetaDataRegistrySource_DataTable::StaticClass();
	}
	struct Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreatedSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CreatedSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TableRules_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TableRules;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMetaDataRegistrySource,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Meta source that will generate DataTable sources at runtime based on a directory scan or asset registration */" },
		{ "DisplayName", "DataTable Meta Source" },
		{ "IncludePath", "DataRegistrySource_DataTable.h" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Meta source that will generate DataTable sources at runtime based on a directory scan or asset registration" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_CreatedSource_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** What specific source class to spawn */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "What specific source class to spawn" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_CreatedSource = { "CreatedSource", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMetaDataRegistrySource_DataTable, CreatedSource), Z_Construct_UClass_UDataRegistrySource_DataTable_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_CreatedSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_CreatedSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_TableRules_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Access rules */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_DataTable.h" },
		{ "ToolTip", "Access rules" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_TableRules = { "TableRules", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMetaDataRegistrySource_DataTable, TableRules), Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules, METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_TableRules_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_TableRules_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_CreatedSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::NewProp_TableRules,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMetaDataRegistrySource_DataTable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::ClassParams = {
		&UMetaDataRegistrySource_DataTable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::PropPointers),
		0,
		0x00B030A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMetaDataRegistrySource_DataTable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMetaDataRegistrySource_DataTable, 2880582164);
	template<> DATAREGISTRY_API UClass* StaticClass<UMetaDataRegistrySource_DataTable>()
	{
		return UMetaDataRegistrySource_DataTable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMetaDataRegistrySource_DataTable(Z_Construct_UClass_UMetaDataRegistrySource_DataTable, &UMetaDataRegistrySource_DataTable::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UMetaDataRegistrySource_DataTable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMetaDataRegistrySource_DataTable);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
