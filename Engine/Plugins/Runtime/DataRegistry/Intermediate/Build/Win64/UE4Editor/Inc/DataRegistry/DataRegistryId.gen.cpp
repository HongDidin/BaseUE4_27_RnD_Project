// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistry/Public/DataRegistryId.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistryId() {}
// Cross Module References
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryId();
	UPackage* Z_Construct_UPackage__Script_DataRegistry();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryType();
// End Cross Module References
class UScriptStruct* FDataRegistryId::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRY_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistryId_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistryId, Z_Construct_UPackage__Script_DataRegistry(), TEXT("DataRegistryId"), sizeof(FDataRegistryId), Get_Z_Construct_UScriptStruct_FDataRegistryId_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRY_API UScriptStruct* StaticStruct<FDataRegistryId>()
{
	return FDataRegistryId::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistryId(FDataRegistryId::StaticStruct, TEXT("/Script/DataRegistry"), TEXT("DataRegistryId"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryId
{
	FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryId()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistryId>(FName(TEXT("DataRegistryId")));
	}
} ScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryId;
	struct Z_Construct_UScriptStruct_FDataRegistryId_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegistryType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RegistryType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ItemName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryId_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Identifier for a specific DataRegistryItem, provides the user with a Tag or dropdown-based UI for selecting based on the available index info */" },
		{ "ModuleRelativePath", "Public/DataRegistryId.h" },
		{ "ToolTip", "Identifier for a specific DataRegistryItem, provides the user with a Tag or dropdown-based UI for selecting based on the available index info" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistryId>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_RegistryType_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** The type of this item, used to look up the correct registry */" },
		{ "ModuleRelativePath", "Public/DataRegistryId.h" },
		{ "ToolTip", "The type of this item, used to look up the correct registry" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_RegistryType = { "RegistryType", nullptr, (EPropertyFlags)0x0010000001000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryId, RegistryType), Z_Construct_UScriptStruct_FDataRegistryType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_RegistryType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_RegistryType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_ItemName_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** The name of this object, may be a leaf gameplay tag or a raw name depending on the type */" },
		{ "ModuleRelativePath", "Public/DataRegistryId.h" },
		{ "ToolTip", "The name of this object, may be a leaf gameplay tag or a raw name depending on the type" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_ItemName = { "ItemName", nullptr, (EPropertyFlags)0x0010000001000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryId, ItemName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_ItemName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_ItemName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataRegistryId_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_RegistryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryId_Statics::NewProp_ItemName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistryId_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
		nullptr,
		&NewStructOps,
		"DataRegistryId",
		sizeof(FDataRegistryId),
		alignof(FDataRegistryId),
		Z_Construct_UScriptStruct_FDataRegistryId_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryId_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryId_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryId_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryId()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistryId_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistryId"), sizeof(FDataRegistryId), Get_Z_Construct_UScriptStruct_FDataRegistryId_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistryId_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistryId_Hash() { return 1781691234U; }
class UScriptStruct* FDataRegistryType::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRY_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistryType_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistryType, Z_Construct_UPackage__Script_DataRegistry(), TEXT("DataRegistryType"), sizeof(FDataRegistryType), Get_Z_Construct_UScriptStruct_FDataRegistryType_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRY_API UScriptStruct* StaticStruct<FDataRegistryType>()
{
	return FDataRegistryType::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistryType(FDataRegistryType::StaticStruct, TEXT("/Script/DataRegistry"), TEXT("DataRegistryType"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryType
{
	FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryType()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistryType>(FName(TEXT("DataRegistryType")));
	}
} ScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryType;
	struct Z_Construct_UScriptStruct_FDataRegistryType_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryType_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Wrapper struct to represent a global data registry, represented as an FName internally and implicitly convertible back and forth.\n * This exists so the blueprint API can understand it's not a normal FName.\n */" },
		{ "ModuleRelativePath", "Public/DataRegistryId.h" },
		{ "ToolTip", "Wrapper struct to represent a global data registry, represented as an FName internally and implicitly convertible back and forth.\nThis exists so the blueprint API can understand it's not a normal FName." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistryType_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistryType>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryType_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "DataRegistryType" },
		{ "Comment", "/** The FName representing this type */" },
		{ "ModuleRelativePath", "Public/DataRegistryId.h" },
		{ "ToolTip", "The FName representing this type" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FDataRegistryType_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0020080001000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryType, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryType_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryType_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataRegistryType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryType_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistryType_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
		nullptr,
		&NewStructOps,
		"DataRegistryType",
		sizeof(FDataRegistryType),
		alignof(FDataRegistryType),
		Z_Construct_UScriptStruct_FDataRegistryType_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryType_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryType_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryType_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryType()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistryType_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistryType"), sizeof(FDataRegistryType), Get_Z_Construct_UScriptStruct_FDataRegistryType_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistryType_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistryType_Hash() { return 1401849847U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
