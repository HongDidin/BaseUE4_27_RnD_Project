// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistryEditor/Private/DataRegistryFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistryFactory() {}
// Cross Module References
	DATAREGISTRYEDITOR_API UClass* Z_Construct_UClass_UDataRegistryFactory_NoRegister();
	DATAREGISTRYEDITOR_API UClass* Z_Construct_UClass_UDataRegistryFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_DataRegistryEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistry_NoRegister();
// End Cross Module References
	void UDataRegistryFactory::StaticRegisterNativesUDataRegistryFactory()
	{
	}
	UClass* Z_Construct_UClass_UDataRegistryFactory_NoRegister()
	{
		return UDataRegistryFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDataRegistryFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataRegistryClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DataRegistryClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataRegistryFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistryEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistryFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "DataRegistryFactory.h" },
		{ "ModuleRelativePath", "Private/DataRegistryFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistryFactory_Statics::NewProp_DataRegistryClass_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "ModuleRelativePath", "Private/DataRegistryFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UDataRegistryFactory_Statics::NewProp_DataRegistryClass = { "DataRegistryClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistryFactory, DataRegistryClass), Z_Construct_UClass_UDataRegistry_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UDataRegistryFactory_Statics::NewProp_DataRegistryClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistryFactory_Statics::NewProp_DataRegistryClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataRegistryFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistryFactory_Statics::NewProp_DataRegistryClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataRegistryFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataRegistryFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataRegistryFactory_Statics::ClassParams = {
		&UDataRegistryFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataRegistryFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistryFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataRegistryFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistryFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataRegistryFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataRegistryFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataRegistryFactory, 4016347616);
	template<> DATAREGISTRYEDITOR_API UClass* StaticClass<UDataRegistryFactory>()
	{
		return UDataRegistryFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataRegistryFactory(Z_Construct_UClass_UDataRegistryFactory, &UDataRegistryFactory::StaticClass, TEXT("/Script/DataRegistryEditor"), TEXT("UDataRegistryFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataRegistryFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
