// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAREGISTRYEDITOR_DataRegistryFactory_generated_h
#error "DataRegistryFactory.generated.h already included, missing '#pragma once' in DataRegistryFactory.h"
#endif
#define DATAREGISTRYEDITOR_DataRegistryFactory_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataRegistryFactory(); \
	friend struct Z_Construct_UClass_UDataRegistryFactory_Statics; \
public: \
	DECLARE_CLASS(UDataRegistryFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistryEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistryFactory)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUDataRegistryFactory(); \
	friend struct Z_Construct_UClass_UDataRegistryFactory_Statics; \
public: \
	DECLARE_CLASS(UDataRegistryFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistryEditor"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistryFactory)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistryFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistryFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistryFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistryFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistryFactory(UDataRegistryFactory&&); \
	NO_API UDataRegistryFactory(const UDataRegistryFactory&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistryFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistryFactory(UDataRegistryFactory&&); \
	NO_API UDataRegistryFactory(const UDataRegistryFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistryFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistryFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistryFactory)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_11_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DataRegistryFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRYEDITOR_API UClass* StaticClass<class UDataRegistryFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistryEditor_Private_DataRegistryFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
