// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistry/Public/DataRegistrySettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistrySettings() {}
// Cross Module References
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySettings_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_DataRegistry();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UDataRegistrySettings::StaticRegisterNativesUDataRegistrySettings()
	{
	}
	UClass* Z_Construct_UClass_UDataRegistrySettings_NoRegister()
	{
		return UDataRegistrySettings::StaticClass();
	}
	struct Z_Construct_UClass_UDataRegistrySettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DirectoriesToScan_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DirectoriesToScan_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DirectoriesToScan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInitializeAllLoadedRegistries_MetaData[];
#endif
		static void NewProp_bInitializeAllLoadedRegistries_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInitializeAllLoadedRegistries;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreMissingCookedAssetRegistryData_MetaData[];
#endif
		static void NewProp_bIgnoreMissingCookedAssetRegistryData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreMissingCookedAssetRegistryData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataRegistrySettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings for the Data Registry subsystem, these settings are used to scan for registry assets and set runtime access rules */" },
		{ "DisplayName", "Data Registry" },
		{ "IncludePath", "DataRegistrySettings.h" },
		{ "ModuleRelativePath", "Public/DataRegistrySettings.h" },
		{ "ToolTip", "Settings for the Data Registry subsystem, these settings are used to scan for registry assets and set runtime access rules" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_DirectoriesToScan_Inner = { "DirectoriesToScan", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_DirectoriesToScan_MetaData[] = {
		{ "Category", "Data Registry" },
		{ "Comment", "/** List of directories to scan for data registry assets */" },
		{ "LongPackageName", "" },
		{ "ModuleRelativePath", "Public/DataRegistrySettings.h" },
		{ "RelativeToGameContentDir", "" },
		{ "ToolTip", "List of directories to scan for data registry assets" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_DirectoriesToScan = { "DirectoriesToScan", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySettings, DirectoriesToScan), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_DirectoriesToScan_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_DirectoriesToScan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bInitializeAllLoadedRegistries_MetaData[] = {
		{ "Category", "Data Registry" },
		{ "Comment", "/** If false, only registry assets inside DirectoriesToScan will be initialized. If true, it will also initialize any in-memory DataRegistry assets outside the scan paths */" },
		{ "ModuleRelativePath", "Public/DataRegistrySettings.h" },
		{ "ToolTip", "If false, only registry assets inside DirectoriesToScan will be initialized. If true, it will also initialize any in-memory DataRegistry assets outside the scan paths" },
	};
#endif
	void Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bInitializeAllLoadedRegistries_SetBit(void* Obj)
	{
		((UDataRegistrySettings*)Obj)->bInitializeAllLoadedRegistries = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bInitializeAllLoadedRegistries = { "bInitializeAllLoadedRegistries", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataRegistrySettings), &Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bInitializeAllLoadedRegistries_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bInitializeAllLoadedRegistries_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bInitializeAllLoadedRegistries_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bIgnoreMissingCookedAssetRegistryData_MetaData[] = {
		{ "Category", "Data Registry" },
		{ "Comment", "/** If true, cooked builds will ignore errors with missing AssetRegistry data for specific registered assets like DataTables as it may have been stripped out */" },
		{ "ModuleRelativePath", "Public/DataRegistrySettings.h" },
		{ "ToolTip", "If true, cooked builds will ignore errors with missing AssetRegistry data for specific registered assets like DataTables as it may have been stripped out" },
	};
#endif
	void Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bIgnoreMissingCookedAssetRegistryData_SetBit(void* Obj)
	{
		((UDataRegistrySettings*)Obj)->bIgnoreMissingCookedAssetRegistryData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bIgnoreMissingCookedAssetRegistryData = { "bIgnoreMissingCookedAssetRegistryData", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataRegistrySettings), &Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bIgnoreMissingCookedAssetRegistryData_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bIgnoreMissingCookedAssetRegistryData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bIgnoreMissingCookedAssetRegistryData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataRegistrySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_DirectoriesToScan_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_DirectoriesToScan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bInitializeAllLoadedRegistries,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySettings_Statics::NewProp_bIgnoreMissingCookedAssetRegistryData,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataRegistrySettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataRegistrySettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataRegistrySettings_Statics::ClassParams = {
		&UDataRegistrySettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataRegistrySettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySettings_Statics::PropPointers),
		0,
		0x001002A6u,
		METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataRegistrySettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataRegistrySettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataRegistrySettings, 4047136338);
	template<> DATAREGISTRY_API UClass* StaticClass<UDataRegistrySettings>()
	{
		return UDataRegistrySettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataRegistrySettings(Z_Construct_UClass_UDataRegistrySettings, &UDataRegistrySettings::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UDataRegistrySettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataRegistrySettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
