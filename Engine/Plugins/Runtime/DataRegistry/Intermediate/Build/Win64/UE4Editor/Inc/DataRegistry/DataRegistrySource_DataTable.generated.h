// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAREGISTRY_DataRegistrySource_DataTable_generated_h
#error "DataRegistrySource_DataTable.generated.h already included, missing '#pragma once' in DataRegistrySource_DataTable.h"
#endif
#define DATAREGISTRY_DataRegistrySource_DataTable_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DATAREGISTRY_API UScriptStruct* StaticStruct<struct FDataRegistrySource_DataTableRules>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataRegistrySource_DataTable(); \
	friend struct Z_Construct_UClass_UDataRegistrySource_DataTable_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySource_DataTable, UDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySource_DataTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUDataRegistrySource_DataTable(); \
	friend struct Z_Construct_UClass_UDataRegistrySource_DataTable_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySource_DataTable, UDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySource_DataTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySource_DataTable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySource_DataTable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySource_DataTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySource_DataTable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySource_DataTable(UDataRegistrySource_DataTable&&); \
	NO_API UDataRegistrySource_DataTable(const UDataRegistrySource_DataTable&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySource_DataTable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySource_DataTable(UDataRegistrySource_DataTable&&); \
	NO_API UDataRegistrySource_DataTable(const UDataRegistrySource_DataTable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySource_DataTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySource_DataTable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySource_DataTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CachedTable() { return STRUCT_OFFSET(UDataRegistrySource_DataTable, CachedTable); } \
	FORCEINLINE static uint32 __PPO__PreloadTable() { return STRUCT_OFFSET(UDataRegistrySource_DataTable, PreloadTable); }


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_26_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UDataRegistrySource_DataTable>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMetaDataRegistrySource_DataTable(); \
	friend struct Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics; \
public: \
	DECLARE_CLASS(UMetaDataRegistrySource_DataTable, UMetaDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UMetaDataRegistrySource_DataTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_INCLASS \
private: \
	static void StaticRegisterNativesUMetaDataRegistrySource_DataTable(); \
	friend struct Z_Construct_UClass_UMetaDataRegistrySource_DataTable_Statics; \
public: \
	DECLARE_CLASS(UMetaDataRegistrySource_DataTable, UMetaDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UMetaDataRegistrySource_DataTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMetaDataRegistrySource_DataTable(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMetaDataRegistrySource_DataTable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMetaDataRegistrySource_DataTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMetaDataRegistrySource_DataTable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMetaDataRegistrySource_DataTable(UMetaDataRegistrySource_DataTable&&); \
	NO_API UMetaDataRegistrySource_DataTable(const UMetaDataRegistrySource_DataTable&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMetaDataRegistrySource_DataTable(UMetaDataRegistrySource_DataTable&&); \
	NO_API UMetaDataRegistrySource_DataTable(const UMetaDataRegistrySource_DataTable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMetaDataRegistrySource_DataTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMetaDataRegistrySource_DataTable); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMetaDataRegistrySource_DataTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_96_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h_99_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UMetaDataRegistrySource_DataTable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_DataTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
