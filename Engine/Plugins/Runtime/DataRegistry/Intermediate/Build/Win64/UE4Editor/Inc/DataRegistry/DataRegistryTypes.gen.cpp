// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistry/Public/DataRegistryTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistryTypes() {}
// Cross Module References
	DATAREGISTRY_API UFunction* Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_DataRegistry();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryId();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryLookup();
	DATAREGISTRY_API UEnum* Z_Construct_UEnum_DataRegistry_EDataRegistryAcquireStatus();
	DATAREGISTRY_API UEnum* Z_Construct_UEnum_DataRegistry_EDataRegistryAvailability();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistrySourceItemId();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryCachePolicy();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryIdFormat();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics
	{
		struct _Script_DataRegistry_eventDataRegistryItemAcquiredBPCallback_Parms
		{
			FDataRegistryId ItemId;
			FDataRegistryLookup ResolvedLookup;
			EDataRegistryAcquireStatus Status;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ItemId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ResolvedLookup;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Status_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_ItemId = { "ItemId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_DataRegistry_eventDataRegistryItemAcquiredBPCallback_Parms, ItemId), Z_Construct_UScriptStruct_FDataRegistryId, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_ResolvedLookup = { "ResolvedLookup", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_DataRegistry_eventDataRegistryItemAcquiredBPCallback_Parms, ResolvedLookup), Z_Construct_UScriptStruct_FDataRegistryLookup, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_Status_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_Status = { "Status", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_DataRegistry_eventDataRegistryItemAcquiredBPCallback_Parms, Status), Z_Construct_UEnum_DataRegistry_EDataRegistryAcquireStatus, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_ItemId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_ResolvedLookup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_Status_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::NewProp_Status,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** Blueprint delegate called when item is loaded, you will need to re-query the cache */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "Blueprint delegate called when item is loaded, you will need to re-query the cache" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_DataRegistry, nullptr, "DataRegistryItemAcquiredBPCallback__DelegateSignature", nullptr, nullptr, sizeof(_Script_DataRegistry_eventDataRegistryItemAcquiredBPCallback_Parms), Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_DataRegistry_DataRegistryItemAcquiredBPCallback__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EDataRegistryAcquireStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataRegistry_EDataRegistryAcquireStatus, Z_Construct_UPackage__Script_DataRegistry(), TEXT("EDataRegistryAcquireStatus"));
		}
		return Singleton;
	}
	template<> DATAREGISTRY_API UEnum* StaticEnum<EDataRegistryAcquireStatus>()
	{
		return EDataRegistryAcquireStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataRegistryAcquireStatus(EDataRegistryAcquireStatus_StaticEnum, TEXT("/Script/DataRegistry"), TEXT("EDataRegistryAcquireStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataRegistry_EDataRegistryAcquireStatus_Hash() { return 2188846781U; }
	UEnum* Z_Construct_UEnum_DataRegistry_EDataRegistryAcquireStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataRegistryAcquireStatus"), 0, Get_Z_Construct_UEnum_DataRegistry_EDataRegistryAcquireStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataRegistryAcquireStatus::NotStarted", (int64)EDataRegistryAcquireStatus::NotStarted },
				{ "EDataRegistryAcquireStatus::WaitingForInitialAcquire", (int64)EDataRegistryAcquireStatus::WaitingForInitialAcquire },
				{ "EDataRegistryAcquireStatus::InitialAcquireFinished", (int64)EDataRegistryAcquireStatus::InitialAcquireFinished },
				{ "EDataRegistryAcquireStatus::WaitingForResources", (int64)EDataRegistryAcquireStatus::WaitingForResources },
				{ "EDataRegistryAcquireStatus::AcquireFinished", (int64)EDataRegistryAcquireStatus::AcquireFinished },
				{ "EDataRegistryAcquireStatus::AcquireError", (int64)EDataRegistryAcquireStatus::AcquireError },
				{ "EDataRegistryAcquireStatus::DoesNotExist", (int64)EDataRegistryAcquireStatus::DoesNotExist },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AcquireError.Comment", "/** Failed to acquire, may have timed out or had network issues, can be retried later */" },
				{ "AcquireError.Name", "EDataRegistryAcquireStatus::AcquireError" },
				{ "AcquireError.ToolTip", "Failed to acquire, may have timed out or had network issues, can be retried later" },
				{ "AcquireFinished.Comment", "/** Fully loaded */" },
				{ "AcquireFinished.Name", "EDataRegistryAcquireStatus::AcquireFinished" },
				{ "AcquireFinished.ToolTip", "Fully loaded" },
				{ "Comment", "/** State of a registry async request */" },
				{ "DoesNotExist.Comment", "/** Known to not exist, cannot be retried */" },
				{ "DoesNotExist.Name", "EDataRegistryAcquireStatus::DoesNotExist" },
				{ "DoesNotExist.ToolTip", "Known to not exist, cannot be retried" },
				{ "InitialAcquireFinished.Comment", "/** Temporary state, finished acquiring data but need to check resources */" },
				{ "InitialAcquireFinished.Name", "EDataRegistryAcquireStatus::InitialAcquireFinished" },
				{ "InitialAcquireFinished.ToolTip", "Temporary state, finished acquiring data but need to check resources" },
				{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
				{ "NotStarted.Comment", "/** Not started yet */" },
				{ "NotStarted.Name", "EDataRegistryAcquireStatus::NotStarted" },
				{ "NotStarted.ToolTip", "Not started yet" },
				{ "ToolTip", "State of a registry async request" },
				{ "WaitingForInitialAcquire.Comment", "/** Initial acquire still in progress */" },
				{ "WaitingForInitialAcquire.Name", "EDataRegistryAcquireStatus::WaitingForInitialAcquire" },
				{ "WaitingForInitialAcquire.ToolTip", "Initial acquire still in progress" },
				{ "WaitingForResources.Comment", "/** Data requested and returned, still loading dependent resources */" },
				{ "WaitingForResources.Name", "EDataRegistryAcquireStatus::WaitingForResources" },
				{ "WaitingForResources.ToolTip", "Data requested and returned, still loading dependent resources" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataRegistry,
				nullptr,
				"EDataRegistryAcquireStatus",
				"EDataRegistryAcquireStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDataRegistryAvailability_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataRegistry_EDataRegistryAvailability, Z_Construct_UPackage__Script_DataRegistry(), TEXT("EDataRegistryAvailability"));
		}
		return Singleton;
	}
	template<> DATAREGISTRY_API UEnum* StaticEnum<EDataRegistryAvailability>()
	{
		return EDataRegistryAvailability_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDataRegistryAvailability(EDataRegistryAvailability_StaticEnum, TEXT("/Script/DataRegistry"), TEXT("EDataRegistryAvailability"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataRegistry_EDataRegistryAvailability_Hash() { return 1068538838U; }
	UEnum* Z_Construct_UEnum_DataRegistry_EDataRegistryAvailability()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDataRegistryAvailability"), 0, Get_Z_Construct_UEnum_DataRegistry_EDataRegistryAvailability_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDataRegistryAvailability::DoesNotExist", (int64)EDataRegistryAvailability::DoesNotExist },
				{ "EDataRegistryAvailability::Unknown", (int64)EDataRegistryAvailability::Unknown },
				{ "EDataRegistryAvailability::Remote", (int64)EDataRegistryAvailability::Remote },
				{ "EDataRegistryAvailability::OnDisk", (int64)EDataRegistryAvailability::OnDisk },
				{ "EDataRegistryAvailability::LocalAsset", (int64)EDataRegistryAvailability::LocalAsset },
				{ "EDataRegistryAvailability::PreCached", (int64)EDataRegistryAvailability::PreCached },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** General rule about how hard it is to access an item, with later entries being the most available and faster to access */" },
				{ "DoesNotExist.Comment", "/** Item definitely does not exist */" },
				{ "DoesNotExist.Name", "EDataRegistryAvailability::DoesNotExist" },
				{ "DoesNotExist.ToolTip", "Item definitely does not exist" },
				{ "LocalAsset.Comment", "/** Comes from a local asset, can be sync loaded as needed */" },
				{ "LocalAsset.Name", "EDataRegistryAvailability::LocalAsset" },
				{ "LocalAsset.ToolTip", "Comes from a local asset, can be sync loaded as needed" },
				{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
				{ "OnDisk.Comment", "/** From some other asset such as a json file available without internet access */" },
				{ "OnDisk.Name", "EDataRegistryAvailability::OnDisk" },
				{ "OnDisk.ToolTip", "From some other asset such as a json file available without internet access" },
				{ "PreCached.Comment", "/** This item has already been loaded into memory by a different system and is immediately available */" },
				{ "PreCached.Name", "EDataRegistryAvailability::PreCached" },
				{ "PreCached.ToolTip", "This item has already been loaded into memory by a different system and is immediately available" },
				{ "Remote.Comment", "/** From a database or website with very high latency */" },
				{ "Remote.Name", "EDataRegistryAvailability::Remote" },
				{ "Remote.ToolTip", "From a database or website with very high latency" },
				{ "ToolTip", "General rule about how hard it is to access an item, with later entries being the most available and faster to access" },
				{ "Unknown.Comment", "/** Not sure where item is located or if it exists at all */" },
				{ "Unknown.Name", "EDataRegistryAvailability::Unknown" },
				{ "Unknown.ToolTip", "Not sure where item is located or if it exists at all" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataRegistry,
				nullptr,
				"EDataRegistryAvailability",
				"EDataRegistryAvailability",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FDataRegistrySourceItemId::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRY_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistrySourceItemId, Z_Construct_UPackage__Script_DataRegistry(), TEXT("DataRegistrySourceItemId"), sizeof(FDataRegistrySourceItemId), Get_Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRY_API UScriptStruct* StaticStruct<FDataRegistrySourceItemId>()
{
	return FDataRegistrySourceItemId::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistrySourceItemId(FDataRegistrySourceItemId::StaticStruct, TEXT("/Script/DataRegistry"), TEXT("DataRegistrySourceItemId"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistrySourceItemId
{
	FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistrySourceItemId()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistrySourceItemId>(FName(TEXT("DataRegistrySourceItemId")));
	}
} ScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistrySourceItemId;
	struct Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** A debugging/editor struct used to describe the source for a single data registry item */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "A debugging/editor struct used to describe the source for a single data registry item" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistrySourceItemId>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
		nullptr,
		&NewStructOps,
		"DataRegistrySourceItemId",
		sizeof(FDataRegistrySourceItemId),
		alignof(FDataRegistrySourceItemId),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistrySourceItemId()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistrySourceItemId"), sizeof(FDataRegistrySourceItemId), Get_Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Hash() { return 967079280U; }
class UScriptStruct* FDataRegistryLookup::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRY_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistryLookup_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistryLookup, Z_Construct_UPackage__Script_DataRegistry(), TEXT("DataRegistryLookup"), sizeof(FDataRegistryLookup), Get_Z_Construct_UScriptStruct_FDataRegistryLookup_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRY_API UScriptStruct* StaticStruct<FDataRegistryLookup>()
{
	return FDataRegistryLookup::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistryLookup(FDataRegistryLookup::StaticStruct, TEXT("/Script/DataRegistry"), TEXT("DataRegistryLookup"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryLookup
{
	FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryLookup()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistryLookup>(FName(TEXT("DataRegistryLookup")));
	}
} ScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryLookup;
	struct Z_Construct_UScriptStruct_FDataRegistryLookup_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryLookup_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** A resolved unique id for a specific source, explains how to look it up. This type is opaque in blueprints and cannot be saved across runs */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "A resolved unique id for a specific source, explains how to look it up. This type is opaque in blueprints and cannot be saved across runs" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistryLookup_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistryLookup>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistryLookup_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
		nullptr,
		&NewStructOps,
		"DataRegistryLookup",
		sizeof(FDataRegistryLookup),
		alignof(FDataRegistryLookup),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryLookup_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryLookup_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryLookup()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistryLookup_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistryLookup"), sizeof(FDataRegistryLookup), Get_Z_Construct_UScriptStruct_FDataRegistryLookup_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistryLookup_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistryLookup_Hash() { return 1633684134U; }
class UScriptStruct* FDataRegistryCachePolicy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRY_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistryCachePolicy, Z_Construct_UPackage__Script_DataRegistry(), TEXT("DataRegistryCachePolicy"), sizeof(FDataRegistryCachePolicy), Get_Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRY_API UScriptStruct* StaticStruct<FDataRegistryCachePolicy>()
{
	return FDataRegistryCachePolicy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistryCachePolicy(FDataRegistryCachePolicy::StaticStruct, TEXT("/Script/DataRegistry"), TEXT("DataRegistryCachePolicy"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryCachePolicy
{
	FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryCachePolicy()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistryCachePolicy>(FName(TEXT("DataRegistryCachePolicy")));
	}
} ScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryCachePolicy;
	struct Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCacheIsAlwaysVolatile_MetaData[];
#endif
		static void NewProp_bCacheIsAlwaysVolatile_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCacheIsAlwaysVolatile;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCurveTableCacheVersion_MetaData[];
#endif
		static void NewProp_bUseCurveTableCacheVersion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCurveTableCacheVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinNumberKept_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MinNumberKept;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumberKept_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxNumberKept;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForceKeepSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ForceKeepSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForceReleaseSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ForceReleaseSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Rules to use when deciding how to unload registry items and related assets */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "Rules to use when deciding how to unload registry items and related assets" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistryCachePolicy>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bCacheIsAlwaysVolatile_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** If this is true, the cache is always considered volatile when returning EDataRegistryCacheResult */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "If this is true, the cache is always considered volatile when returning EDataRegistryCacheResult" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bCacheIsAlwaysVolatile_SetBit(void* Obj)
	{
		((FDataRegistryCachePolicy*)Obj)->bCacheIsAlwaysVolatile = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bCacheIsAlwaysVolatile = { "bCacheIsAlwaysVolatile", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDataRegistryCachePolicy), &Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bCacheIsAlwaysVolatile_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bCacheIsAlwaysVolatile_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bCacheIsAlwaysVolatile_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bUseCurveTableCacheVersion_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** If this is true, the cache version is synchronized with the global CurveTable cache version */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "If this is true, the cache version is synchronized with the global CurveTable cache version" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bUseCurveTableCacheVersion_SetBit(void* Obj)
	{
		((FDataRegistryCachePolicy*)Obj)->bUseCurveTableCacheVersion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bUseCurveTableCacheVersion = { "bUseCurveTableCacheVersion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDataRegistryCachePolicy), &Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bUseCurveTableCacheVersion_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bUseCurveTableCacheVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bUseCurveTableCacheVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MinNumberKept_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** Will not release items if fewer then this number loaded, 0 means infinite */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "Will not release items if fewer then this number loaded, 0 means infinite" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MinNumberKept = { "MinNumberKept", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryCachePolicy, MinNumberKept), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MinNumberKept_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MinNumberKept_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MaxNumberKept_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** Maximum number of items to keep loaded, 0 means infinite */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "Maximum number of items to keep loaded, 0 means infinite" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MaxNumberKept = { "MaxNumberKept", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryCachePolicy, MaxNumberKept), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MaxNumberKept_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MaxNumberKept_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceKeepSeconds_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** Any item accessed within this amount of seconds is never unloaded */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "Any item accessed within this amount of seconds is never unloaded" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceKeepSeconds = { "ForceKeepSeconds", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryCachePolicy, ForceKeepSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceKeepSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceKeepSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceReleaseSeconds_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** Any item not accessed within this amount of seconds is always unloaded */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "Any item not accessed within this amount of seconds is always unloaded" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceReleaseSeconds = { "ForceReleaseSeconds", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryCachePolicy, ForceReleaseSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceReleaseSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceReleaseSeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bCacheIsAlwaysVolatile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_bUseCurveTableCacheVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MinNumberKept,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_MaxNumberKept,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceKeepSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::NewProp_ForceReleaseSeconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
		nullptr,
		&NewStructOps,
		"DataRegistryCachePolicy",
		sizeof(FDataRegistryCachePolicy),
		alignof(FDataRegistryCachePolicy),
		Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryCachePolicy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistryCachePolicy"), sizeof(FDataRegistryCachePolicy), Get_Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Hash() { return 1519725903U; }
class UScriptStruct* FDataRegistryIdFormat::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRY_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistryIdFormat_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistryIdFormat, Z_Construct_UPackage__Script_DataRegistry(), TEXT("DataRegistryIdFormat"), sizeof(FDataRegistryIdFormat), Get_Z_Construct_UScriptStruct_FDataRegistryIdFormat_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRY_API UScriptStruct* StaticStruct<FDataRegistryIdFormat>()
{
	return FDataRegistryIdFormat::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistryIdFormat(FDataRegistryIdFormat::StaticStruct, TEXT("/Script/DataRegistry"), TEXT("DataRegistryIdFormat"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryIdFormat
{
	FScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryIdFormat()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistryIdFormat>(FName(TEXT("DataRegistryIdFormat")));
	}
} ScriptStruct_DataRegistry_StaticRegisterNativesFDataRegistryIdFormat;
	struct Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseGameplayTag_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseGameplayTag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Struct representing how a unique id is formatted and picked in the editor */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "Struct representing how a unique id is formatted and picked in the editor" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistryIdFormat>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::NewProp_BaseGameplayTag_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** If this is not empty, all ids are part of this hierarchy */" },
		{ "ModuleRelativePath", "Public/DataRegistryTypes.h" },
		{ "ToolTip", "If this is not empty, all ids are part of this hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::NewProp_BaseGameplayTag = { "BaseGameplayTag", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryIdFormat, BaseGameplayTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::NewProp_BaseGameplayTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::NewProp_BaseGameplayTag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::NewProp_BaseGameplayTag,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
		nullptr,
		&NewStructOps,
		"DataRegistryIdFormat",
		sizeof(FDataRegistryIdFormat),
		alignof(FDataRegistryIdFormat),
		Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryIdFormat()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistryIdFormat_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistryIdFormat"), sizeof(FDataRegistryIdFormat), Get_Z_Construct_UScriptStruct_FDataRegistryIdFormat_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistryIdFormat_Hash() { return 925351350U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
