// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAREGISTRY_DataRegistry_generated_h
#error "DataRegistry.generated.h already included, missing '#pragma once' in DataRegistry.h"
#endif
#define DATAREGISTRY_DataRegistry_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataRegistry(); \
	friend struct Z_Construct_UClass_UDataRegistry_Statics; \
public: \
	DECLARE_CLASS(UDataRegistry, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistry)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUDataRegistry(); \
	friend struct Z_Construct_UClass_UDataRegistry_Statics; \
public: \
	DECLARE_CLASS(UDataRegistry, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistry)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistry(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistry(UDataRegistry&&); \
	NO_API UDataRegistry(const UDataRegistry&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistry(UDataRegistry&&); \
	NO_API UDataRegistry(const UDataRegistry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistry); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDataRegistry)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RegistryType() { return STRUCT_OFFSET(UDataRegistry, RegistryType); } \
	FORCEINLINE static uint32 __PPO__IdFormat() { return STRUCT_OFFSET(UDataRegistry, IdFormat); } \
	FORCEINLINE static uint32 __PPO__ItemStruct() { return STRUCT_OFFSET(UDataRegistry, ItemStruct); } \
	FORCEINLINE static uint32 __PPO__DataSources() { return STRUCT_OFFSET(UDataRegistry, DataSources); } \
	FORCEINLINE static uint32 __PPO__RuntimeSources() { return STRUCT_OFFSET(UDataRegistry, RuntimeSources); } \
	FORCEINLINE static uint32 __PPO__TimerUpdateFrequency() { return STRUCT_OFFSET(UDataRegistry, TimerUpdateFrequency); } \
	FORCEINLINE static uint32 __PPO__DefaultCachePolicy() { return STRUCT_OFFSET(UDataRegistry, DefaultCachePolicy); }


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_16_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UDataRegistry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistry_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
