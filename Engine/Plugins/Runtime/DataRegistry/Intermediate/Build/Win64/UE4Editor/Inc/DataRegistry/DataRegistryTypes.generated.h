// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDataRegistryId;
struct FDataRegistryLookup;
enum class EDataRegistryAcquireStatus : uint8;
#ifdef DATAREGISTRY_DataRegistryTypes_generated_h
#error "DataRegistryTypes.generated.h already included, missing '#pragma once' in DataRegistryTypes.h"
#endif
#define DATAREGISTRY_DataRegistryTypes_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryTypes_h_171_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataRegistrySourceItemId_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DATAREGISTRY_API UScriptStruct* StaticStruct<struct FDataRegistrySourceItemId>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryTypes_h_90_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataRegistryLookup_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DATAREGISTRY_API UScriptStruct* StaticStruct<struct FDataRegistryLookup>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryTypes_h_58_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataRegistryCachePolicy_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DATAREGISTRY_API UScriptStruct* StaticStruct<struct FDataRegistryCachePolicy>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryTypes_h_42_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataRegistryIdFormat_Statics; \
	DATAREGISTRY_API static class UScriptStruct* StaticStruct();


template<> DATAREGISTRY_API UScriptStruct* StaticStruct<struct FDataRegistryIdFormat>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryTypes_h_504_DELEGATE \
struct _Script_DataRegistry_eventDataRegistryItemAcquiredBPCallback_Parms \
{ \
	FDataRegistryId ItemId; \
	FDataRegistryLookup ResolvedLookup; \
	EDataRegistryAcquireStatus Status; \
}; \
static inline void FDataRegistryItemAcquiredBPCallback_DelegateWrapper(const FScriptDelegate& DataRegistryItemAcquiredBPCallback, FDataRegistryId ItemId, FDataRegistryLookup ResolvedLookup, EDataRegistryAcquireStatus Status) \
{ \
	_Script_DataRegistry_eventDataRegistryItemAcquiredBPCallback_Parms Parms; \
	Parms.ItemId=ItemId; \
	Parms.ResolvedLookup=ResolvedLookup; \
	Parms.Status=Status; \
	DataRegistryItemAcquiredBPCallback.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryTypes_h


#define FOREACH_ENUM_EDATAREGISTRYACQUIRESTATUS(op) \
	op(EDataRegistryAcquireStatus::NotStarted) \
	op(EDataRegistryAcquireStatus::WaitingForInitialAcquire) \
	op(EDataRegistryAcquireStatus::InitialAcquireFinished) \
	op(EDataRegistryAcquireStatus::WaitingForResources) \
	op(EDataRegistryAcquireStatus::AcquireFinished) \
	op(EDataRegistryAcquireStatus::AcquireError) \
	op(EDataRegistryAcquireStatus::DoesNotExist) 

enum class EDataRegistryAcquireStatus : uint8;
template<> DATAREGISTRY_API UEnum* StaticEnum<EDataRegistryAcquireStatus>();

#define FOREACH_ENUM_EDATAREGISTRYAVAILABILITY(op) \
	op(EDataRegistryAvailability::DoesNotExist) \
	op(EDataRegistryAvailability::Unknown) \
	op(EDataRegistryAvailability::Remote) \
	op(EDataRegistryAvailability::OnDisk) \
	op(EDataRegistryAvailability::LocalAsset) \
	op(EDataRegistryAvailability::PreCached) 

enum class EDataRegistryAvailability : uint8;
template<> DATAREGISTRY_API UEnum* StaticEnum<EDataRegistryAvailability>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
