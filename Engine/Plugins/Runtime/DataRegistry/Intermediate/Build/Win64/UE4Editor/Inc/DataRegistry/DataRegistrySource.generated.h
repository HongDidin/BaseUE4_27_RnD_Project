// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAREGISTRY_DataRegistrySource_generated_h
#error "DataRegistrySource.generated.h already included, missing '#pragma once' in DataRegistrySource.h"
#endif
#define DATAREGISTRY_DataRegistrySource_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataRegistrySource(); \
	friend struct Z_Construct_UClass_UDataRegistrySource_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySource, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySource) \
	DECLARE_WITHIN(UDataRegistry)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUDataRegistrySource(); \
	friend struct Z_Construct_UClass_UDataRegistrySource_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySource, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySource) \
	DECLARE_WITHIN(UDataRegistry)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySource(UDataRegistrySource&&); \
	NO_API UDataRegistrySource(const UDataRegistrySource&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySource(UDataRegistrySource&&); \
	NO_API UDataRegistrySource(const UDataRegistrySource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySource); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySource)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ParentSource() { return STRUCT_OFFSET(UDataRegistrySource, ParentSource); }


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_9_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UDataRegistrySource>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMetaDataRegistrySource(); \
	friend struct Z_Construct_UClass_UMetaDataRegistrySource_Statics; \
public: \
	DECLARE_CLASS(UMetaDataRegistrySource, UDataRegistrySource, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UMetaDataRegistrySource)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_INCLASS \
private: \
	static void StaticRegisterNativesUMetaDataRegistrySource(); \
	friend struct Z_Construct_UClass_UMetaDataRegistrySource_Statics; \
public: \
	DECLARE_CLASS(UMetaDataRegistrySource, UDataRegistrySource, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UMetaDataRegistrySource)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMetaDataRegistrySource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMetaDataRegistrySource) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMetaDataRegistrySource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMetaDataRegistrySource); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMetaDataRegistrySource(UMetaDataRegistrySource&&); \
	NO_API UMetaDataRegistrySource(const UMetaDataRegistrySource&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMetaDataRegistrySource(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMetaDataRegistrySource(UMetaDataRegistrySource&&); \
	NO_API UMetaDataRegistrySource(const UMetaDataRegistrySource&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMetaDataRegistrySource); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMetaDataRegistrySource); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMetaDataRegistrySource)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RuntimeChildren() { return STRUCT_OFFSET(UMetaDataRegistrySource, RuntimeChildren); }


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_118_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h_121_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UMetaDataRegistrySource>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_h


#define FOREACH_ENUM_EMETADATAREGISTRYSOURCEASSETUSAGE(op) \
	op(EMetaDataRegistrySourceAssetUsage::NoAssets) \
	op(EMetaDataRegistrySourceAssetUsage::SearchAssets) \
	op(EMetaDataRegistrySourceAssetUsage::RegisterAssets) \
	op(EMetaDataRegistrySourceAssetUsage::SearchAndRegisterAssets) 

enum class EMetaDataRegistrySourceAssetUsage : uint8;
template<> DATAREGISTRY_API UEnum* StaticEnum<EMetaDataRegistrySourceAssetUsage>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
