// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistry/Public/DataRegistrySource_CurveTable.h"
#include "DataRegistry/Public/DataRegistry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistrySource_CurveTable() {}
// Cross Module References
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource_CurveTable_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource_CurveTable();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource();
	UPackage* Z_Construct_UPackage__Script_DataRegistry();
	ENGINE_API UClass* Z_Construct_UClass_UCurveTable_NoRegister();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource_CurveTable();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
// End Cross Module References
	void UDataRegistrySource_CurveTable::StaticRegisterNativesUDataRegistrySource_CurveTable()
	{
	}
	UClass* Z_Construct_UClass_UDataRegistrySource_CurveTable_NoRegister()
	{
		return UDataRegistrySource_CurveTable::StaticClass();
	}
	struct Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SourceTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TableRules_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TableRules;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedTable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreloadTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreloadTable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataRegistrySource,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Data source that loads from a specific curve table asset */" },
		{ "DisplayName", "CurveTable Source" },
		{ "IncludePath", "DataRegistrySource_CurveTable.h" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "Data source that loads from a specific curve table asset" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_SourceTable_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** What table to load from */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "What table to load from" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_SourceTable = { "SourceTable", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_CurveTable, SourceTable), Z_Construct_UClass_UCurveTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_SourceTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_SourceTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_TableRules_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Access rules */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "Access rules" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_TableRules = { "TableRules", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_CurveTable, TableRules), Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_TableRules_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_TableRules_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_CachedTable_MetaData[] = {
		{ "Comment", "/** Hard ref to loaded table */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "Hard ref to loaded table" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_CachedTable = { "CachedTable", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_CurveTable, CachedTable), Z_Construct_UClass_UCurveTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_CachedTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_CachedTable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_PreloadTable_MetaData[] = {
		{ "Comment", "/** Preload table ref, will be set if this is a hard source */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "Preload table ref, will be set if this is a hard source" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_PreloadTable = { "PreloadTable", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource_CurveTable, PreloadTable), Z_Construct_UClass_UCurveTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_PreloadTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_PreloadTable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_SourceTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_TableRules,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_CachedTable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::NewProp_PreloadTable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataRegistrySource_CurveTable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::ClassParams = {
		&UDataRegistrySource_CurveTable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::PropPointers),
		0,
		0x00B030A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataRegistrySource_CurveTable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataRegistrySource_CurveTable, 3322632223);
	template<> DATAREGISTRY_API UClass* StaticClass<UDataRegistrySource_CurveTable>()
	{
		return UDataRegistrySource_CurveTable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataRegistrySource_CurveTable(Z_Construct_UClass_UDataRegistrySource_CurveTable, &UDataRegistrySource_CurveTable::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UDataRegistrySource_CurveTable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataRegistrySource_CurveTable);
	void UMetaDataRegistrySource_CurveTable::StaticRegisterNativesUMetaDataRegistrySource_CurveTable()
	{
	}
	UClass* Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_NoRegister()
	{
		return UMetaDataRegistrySource_CurveTable::StaticClass();
	}
	struct Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CreatedSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CreatedSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TableRules_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TableRules;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMetaDataRegistrySource,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Meta source that will generate CurveTable sources at runtime based on a directory scan or asset registration */" },
		{ "DisplayName", "CurveTable Meta Source" },
		{ "IncludePath", "DataRegistrySource_CurveTable.h" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "Meta source that will generate CurveTable sources at runtime based on a directory scan or asset registration" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_CreatedSource_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** What specific source class to spawn */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "What specific source class to spawn" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_CreatedSource = { "CreatedSource", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMetaDataRegistrySource_CurveTable, CreatedSource), Z_Construct_UClass_UDataRegistrySource_CurveTable_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_CreatedSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_CreatedSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_TableRules_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Access rules */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource_CurveTable.h" },
		{ "ToolTip", "Access rules" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_TableRules = { "TableRules", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMetaDataRegistrySource_CurveTable, TableRules), Z_Construct_UScriptStruct_FDataRegistrySource_DataTableRules, METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_TableRules_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_TableRules_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_CreatedSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::NewProp_TableRules,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMetaDataRegistrySource_CurveTable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::ClassParams = {
		&UMetaDataRegistrySource_CurveTable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::PropPointers),
		0,
		0x00B030A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMetaDataRegistrySource_CurveTable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMetaDataRegistrySource_CurveTable, 3213074402);
	template<> DATAREGISTRY_API UClass* StaticClass<UMetaDataRegistrySource_CurveTable>()
	{
		return UMetaDataRegistrySource_CurveTable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMetaDataRegistrySource_CurveTable(Z_Construct_UClass_UMetaDataRegistrySource_CurveTable, &UMetaDataRegistrySource_CurveTable::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UMetaDataRegistrySource_CurveTable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMetaDataRegistrySource_CurveTable);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
