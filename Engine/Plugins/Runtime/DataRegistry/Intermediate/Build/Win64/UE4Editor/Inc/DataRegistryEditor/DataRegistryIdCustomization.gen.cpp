// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistryEditor/Private/DataRegistryIdCustomization.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistryIdCustomization() {}
// Cross Module References
	DATAREGISTRYEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper();
	UPackage* Z_Construct_UPackage__Script_DataRegistryEditor();
	GRAPHEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FPinStructEditWrapper();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryId();
// End Cross Module References

static_assert(std::is_polymorphic<FDataRegistryIdEditWrapper>() == std::is_polymorphic<FPinStructEditWrapper>(), "USTRUCT FDataRegistryIdEditWrapper cannot be polymorphic unless super FPinStructEditWrapper is polymorphic");

class UScriptStruct* FDataRegistryIdEditWrapper::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DATAREGISTRYEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper, Z_Construct_UPackage__Script_DataRegistryEditor(), TEXT("DataRegistryIdEditWrapper"), sizeof(FDataRegistryIdEditWrapper), Get_Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Hash());
	}
	return Singleton;
}
template<> DATAREGISTRYEDITOR_API UScriptStruct* StaticStruct<FDataRegistryIdEditWrapper>()
{
	return FDataRegistryIdEditWrapper::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataRegistryIdEditWrapper(FDataRegistryIdEditWrapper::StaticStruct, TEXT("/Script/DataRegistryEditor"), TEXT("DataRegistryIdEditWrapper"), false, nullptr, nullptr);
static struct FScriptStruct_DataRegistryEditor_StaticRegisterNativesFDataRegistryIdEditWrapper
{
	FScriptStruct_DataRegistryEditor_StaticRegisterNativesFDataRegistryIdEditWrapper()
	{
		UScriptStruct::DeferCppStructOps<FDataRegistryIdEditWrapper>(FName(TEXT("DataRegistryIdEditWrapper")));
	}
} ScriptStruct_DataRegistryEditor_StaticRegisterNativesFDataRegistryIdEditWrapper;
	struct Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegistryId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RegistryId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** This allows using this customization with blueprint pins */" },
		{ "ModuleRelativePath", "Private/DataRegistryIdCustomization.h" },
		{ "ToolTip", "This allows using this customization with blueprint pins" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataRegistryIdEditWrapper>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::NewProp_RegistryId_MetaData[] = {
		{ "Category", "Registry" },
		{ "Comment", "/** Actual id to edit */" },
		{ "ModuleRelativePath", "Private/DataRegistryIdCustomization.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Actual id to edit" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::NewProp_RegistryId = { "RegistryId", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataRegistryIdEditWrapper, RegistryId), Z_Construct_UScriptStruct_FDataRegistryId, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::NewProp_RegistryId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::NewProp_RegistryId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::NewProp_RegistryId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistryEditor,
		Z_Construct_UScriptStruct_FPinStructEditWrapper,
		&NewStructOps,
		"DataRegistryIdEditWrapper",
		sizeof(FDataRegistryIdEditWrapper),
		alignof(FDataRegistryIdEditWrapper),
		Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistryEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataRegistryIdEditWrapper"), sizeof(FDataRegistryIdEditWrapper), Get_Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataRegistryIdEditWrapper_Hash() { return 2700427481U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
