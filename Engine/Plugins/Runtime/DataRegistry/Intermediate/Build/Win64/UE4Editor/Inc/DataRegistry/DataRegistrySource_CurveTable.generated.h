// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAREGISTRY_DataRegistrySource_CurveTable_generated_h
#error "DataRegistrySource_CurveTable.generated.h already included, missing '#pragma once' in DataRegistrySource_CurveTable.h"
#endif
#define DATAREGISTRY_DataRegistrySource_CurveTable_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataRegistrySource_CurveTable(); \
	friend struct Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySource_CurveTable, UDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySource_CurveTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUDataRegistrySource_CurveTable(); \
	friend struct Z_Construct_UClass_UDataRegistrySource_CurveTable_Statics; \
public: \
	DECLARE_CLASS(UDataRegistrySource_CurveTable, UDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UDataRegistrySource_CurveTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySource_CurveTable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySource_CurveTable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySource_CurveTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySource_CurveTable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySource_CurveTable(UDataRegistrySource_CurveTable&&); \
	NO_API UDataRegistrySource_CurveTable(const UDataRegistrySource_CurveTable&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataRegistrySource_CurveTable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataRegistrySource_CurveTable(UDataRegistrySource_CurveTable&&); \
	NO_API UDataRegistrySource_CurveTable(const UDataRegistrySource_CurveTable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataRegistrySource_CurveTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataRegistrySource_CurveTable); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataRegistrySource_CurveTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CachedTable() { return STRUCT_OFFSET(UDataRegistrySource_CurveTable, CachedTable); } \
	FORCEINLINE static uint32 __PPO__PreloadTable() { return STRUCT_OFFSET(UDataRegistrySource_CurveTable, PreloadTable); }


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_12_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UDataRegistrySource_CurveTable>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_SPARSE_DATA
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_RPC_WRAPPERS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMetaDataRegistrySource_CurveTable(); \
	friend struct Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics; \
public: \
	DECLARE_CLASS(UMetaDataRegistrySource_CurveTable, UMetaDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UMetaDataRegistrySource_CurveTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_INCLASS \
private: \
	static void StaticRegisterNativesUMetaDataRegistrySource_CurveTable(); \
	friend struct Z_Construct_UClass_UMetaDataRegistrySource_CurveTable_Statics; \
public: \
	DECLARE_CLASS(UMetaDataRegistrySource_CurveTable, UMetaDataRegistrySource, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataRegistry"), NO_API) \
	DECLARE_SERIALIZER(UMetaDataRegistrySource_CurveTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMetaDataRegistrySource_CurveTable(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMetaDataRegistrySource_CurveTable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMetaDataRegistrySource_CurveTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMetaDataRegistrySource_CurveTable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMetaDataRegistrySource_CurveTable(UMetaDataRegistrySource_CurveTable&&); \
	NO_API UMetaDataRegistrySource_CurveTable(const UMetaDataRegistrySource_CurveTable&); \
public:


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMetaDataRegistrySource_CurveTable(UMetaDataRegistrySource_CurveTable&&); \
	NO_API UMetaDataRegistrySource_CurveTable(const UMetaDataRegistrySource_CurveTable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMetaDataRegistrySource_CurveTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMetaDataRegistrySource_CurveTable); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMetaDataRegistrySource_CurveTable)


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_82_PROLOG
#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_RPC_WRAPPERS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_INCLASS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_SPARSE_DATA \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h_85_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAREGISTRY_API UClass* StaticClass<class UMetaDataRegistrySource_CurveTable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistrySource_CurveTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
