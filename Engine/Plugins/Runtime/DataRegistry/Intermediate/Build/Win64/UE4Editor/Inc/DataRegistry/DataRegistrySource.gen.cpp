// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistry/Public/DataRegistrySource.h"
#include "DataRegistry/Public/DataRegistry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistrySource() {}
// Cross Module References
	DATAREGISTRY_API UEnum* Z_Construct_UEnum_DataRegistry_EMetaDataRegistrySourceAssetUsage();
	UPackage* Z_Construct_UPackage__Script_DataRegistry();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UMetaDataRegistrySource();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FAssetManagerSearchRules();
// End Cross Module References
	static UEnum* EMetaDataRegistrySourceAssetUsage_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataRegistry_EMetaDataRegistrySourceAssetUsage, Z_Construct_UPackage__Script_DataRegistry(), TEXT("EMetaDataRegistrySourceAssetUsage"));
		}
		return Singleton;
	}
	template<> DATAREGISTRY_API UEnum* StaticEnum<EMetaDataRegistrySourceAssetUsage>()
	{
		return EMetaDataRegistrySourceAssetUsage_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMetaDataRegistrySourceAssetUsage(EMetaDataRegistrySourceAssetUsage_StaticEnum, TEXT("/Script/DataRegistry"), TEXT("EMetaDataRegistrySourceAssetUsage"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataRegistry_EMetaDataRegistrySourceAssetUsage_Hash() { return 1222150766U; }
	UEnum* Z_Construct_UEnum_DataRegistry_EMetaDataRegistrySourceAssetUsage()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataRegistry();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMetaDataRegistrySourceAssetUsage"), 0, Get_Z_Construct_UEnum_DataRegistry_EMetaDataRegistrySourceAssetUsage_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMetaDataRegistrySourceAssetUsage::NoAssets", (int64)EMetaDataRegistrySourceAssetUsage::NoAssets },
				{ "EMetaDataRegistrySourceAssetUsage::SearchAssets", (int64)EMetaDataRegistrySourceAssetUsage::SearchAssets },
				{ "EMetaDataRegistrySourceAssetUsage::RegisterAssets", (int64)EMetaDataRegistrySourceAssetUsage::RegisterAssets },
				{ "EMetaDataRegistrySourceAssetUsage::SearchAndRegisterAssets", (int64)EMetaDataRegistrySourceAssetUsage::SearchAndRegisterAssets },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Rules specifying how a meta source will deal with assets, arranged as a semi-bitfield */" },
				{ "ModuleRelativePath", "Public/DataRegistrySource.h" },
				{ "NoAssets.Comment", "/** Does not use assets, gets sources from somewhere else like a web database */" },
				{ "NoAssets.Name", "EMetaDataRegistrySourceAssetUsage::NoAssets" },
				{ "NoAssets.ToolTip", "Does not use assets, gets sources from somewhere else like a web database" },
				{ "RegisterAssets.Comment", "/** Only accepts registered assets, does not do any scanning */" },
				{ "RegisterAssets.Name", "EMetaDataRegistrySourceAssetUsage::RegisterAssets" },
				{ "RegisterAssets.ToolTip", "Only accepts registered assets, does not do any scanning" },
				{ "SearchAndRegisterAssets.Comment", "/** Both does search and will accept registered assets, using search rules as filter */" },
				{ "SearchAndRegisterAssets.Name", "EMetaDataRegistrySourceAssetUsage::SearchAndRegisterAssets" },
				{ "SearchAndRegisterAssets.ToolTip", "Both does search and will accept registered assets, using search rules as filter" },
				{ "SearchAssets.Comment", "/** Only loads assets off disk, does not accept registered assets */" },
				{ "SearchAssets.Name", "EMetaDataRegistrySourceAssetUsage::SearchAssets" },
				{ "SearchAssets.ToolTip", "Only loads assets off disk, does not accept registered assets" },
				{ "ToolTip", "Rules specifying how a meta source will deal with assets, arranged as a semi-bitfield" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataRegistry,
				nullptr,
				"EMetaDataRegistrySourceAssetUsage",
				"EMetaDataRegistrySourceAssetUsage",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDataRegistrySource::StaticRegisterNativesUDataRegistrySource()
	{
	}
	UClass* Z_Construct_UClass_UDataRegistrySource_NoRegister()
	{
		return UDataRegistrySource::StaticClass();
	}
	struct Z_Construct_UClass_UDataRegistrySource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParentSource;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataRegistrySource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Specifies a source for DataRegistry items, which is an interface that provides asynchronous access to individual structs */" },
		{ "IncludePath", "DataRegistrySource.h" },
		{ "ModuleRelativePath", "Public/DataRegistrySource.h" },
		{ "ToolTip", "Specifies a source for DataRegistry items, which is an interface that provides asynchronous access to individual structs" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistrySource_Statics::NewProp_ParentSource_MetaData[] = {
		{ "Comment", "/** What data source we were created from, if this is a transient source */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DataRegistrySource.h" },
		{ "ToolTip", "What data source we were created from, if this is a transient source" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistrySource_Statics::NewProp_ParentSource = { "ParentSource", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistrySource, ParentSource), Z_Construct_UClass_UDataRegistrySource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_Statics::NewProp_ParentSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_Statics::NewProp_ParentSource_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataRegistrySource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistrySource_Statics::NewProp_ParentSource,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataRegistrySource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataRegistrySource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataRegistrySource_Statics::ClassParams = {
		&UDataRegistrySource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataRegistrySource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_Statics::PropPointers),
		0,
		0x00B030A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataRegistrySource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistrySource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataRegistrySource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataRegistrySource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataRegistrySource, 2757306318);
	template<> DATAREGISTRY_API UClass* StaticClass<UDataRegistrySource>()
	{
		return UDataRegistrySource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataRegistrySource(Z_Construct_UClass_UDataRegistrySource, &UDataRegistrySource::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UDataRegistrySource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataRegistrySource);
	void UMetaDataRegistrySource::StaticRegisterNativesUMetaDataRegistrySource()
	{
	}
	UClass* Z_Construct_UClass_UMetaDataRegistrySource_NoRegister()
	{
		return UMetaDataRegistrySource::StaticClass();
	}
	struct Z_Construct_UClass_UMetaDataRegistrySource_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AssetUsage_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetUsage_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AssetUsage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SearchRules_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SearchRules;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RuntimeChildren_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RuntimeChildren_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RuntimeChildren_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RuntimeChildren;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMetaDataRegistrySource_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataRegistrySource,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Base class for a data source that generates additional data sources at runtime */" },
		{ "IncludePath", "DataRegistrySource.h" },
		{ "ModuleRelativePath", "Public/DataRegistrySource.h" },
		{ "ToolTip", "Base class for a data source that generates additional data sources at runtime" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_AssetUsage_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_AssetUsage_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Asset usage */" },
		{ "ModuleRelativePath", "Public/DataRegistrySource.h" },
		{ "ToolTip", "Asset usage" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_AssetUsage = { "AssetUsage", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMetaDataRegistrySource, AssetUsage), Z_Construct_UEnum_DataRegistry_EMetaDataRegistrySourceAssetUsage, METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_AssetUsage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_AssetUsage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_SearchRules_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Asset registry scan rules */" },
		{ "EditCondition", "AssetUsage != EMetaDataRegistrySourceAssetUsage::NoAssets" },
		{ "ModuleRelativePath", "Public/DataRegistrySource.h" },
		{ "ToolTip", "Asset registry scan rules" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_SearchRules = { "SearchRules", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMetaDataRegistrySource, SearchRules), Z_Construct_UScriptStruct_FAssetManagerSearchRules, METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_SearchRules_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_SearchRules_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren_ValueProp = { "RuntimeChildren", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDataRegistrySource_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren_Key_KeyProp = { "RuntimeChildren_Key", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren_MetaData[] = {
		{ "Comment", "/** Map from source identifier such as package name to registered child */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DataRegistrySource.h" },
		{ "ToolTip", "Map from source identifier such as package name to registered child" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren = { "RuntimeChildren", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMetaDataRegistrySource, RuntimeChildren), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMetaDataRegistrySource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_AssetUsage_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_AssetUsage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_SearchRules,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMetaDataRegistrySource_Statics::NewProp_RuntimeChildren,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMetaDataRegistrySource_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMetaDataRegistrySource>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMetaDataRegistrySource_Statics::ClassParams = {
		&UMetaDataRegistrySource::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMetaDataRegistrySource_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_Statics::PropPointers),
		0,
		0x00B030A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMetaDataRegistrySource_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMetaDataRegistrySource_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMetaDataRegistrySource()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMetaDataRegistrySource_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMetaDataRegistrySource, 1914426822);
	template<> DATAREGISTRY_API UClass* StaticClass<UMetaDataRegistrySource>()
	{
		return UMetaDataRegistrySource::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMetaDataRegistrySource(Z_Construct_UClass_UMetaDataRegistrySource, &UMetaDataRegistrySource::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UMetaDataRegistrySource"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMetaDataRegistrySource);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
