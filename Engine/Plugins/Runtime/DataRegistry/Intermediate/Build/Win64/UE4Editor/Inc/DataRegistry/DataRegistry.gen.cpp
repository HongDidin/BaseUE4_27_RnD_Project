// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DataRegistry/Public/DataRegistry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataRegistry() {}
// Cross Module References
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistry_NoRegister();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistry();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DataRegistry();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryIdFormat();
	COREUOBJECT_API UClass* Z_Construct_UClass_UScriptStruct();
	DATAREGISTRY_API UClass* Z_Construct_UClass_UDataRegistrySource_NoRegister();
	DATAREGISTRY_API UScriptStruct* Z_Construct_UScriptStruct_FDataRegistryCachePolicy();
// End Cross Module References
	void UDataRegistry::StaticRegisterNativesUDataRegistry()
	{
	}
	UClass* Z_Construct_UClass_UDataRegistry_NoRegister()
	{
		return UDataRegistry::StaticClass();
	}
	struct Z_Construct_UClass_UDataRegistry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RegistryType_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RegistryType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IdFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IdFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ItemStruct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataSources_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DataSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DataSources;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RuntimeSources_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RuntimeSources_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RuntimeSources_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RuntimeSources;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimerUpdateFrequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimerUpdateFrequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultCachePolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultCachePolicy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataRegistry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DataRegistry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Defines a place to efficiently store and retrieve structure data, can be used as a wrapper around Data/Curve Tables or extended with other sources\n */" },
		{ "IncludePath", "DataRegistry.h" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "Defines a place to efficiently store and retrieve structure data, can be used as a wrapper around Data/Curve Tables or extended with other sources" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_RegistryType_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Globally unique name used to identify this registry */" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "Globally unique name used to identify this registry" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_RegistryType = { "RegistryType", nullptr, (EPropertyFlags)0x0020090000010001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistry, RegistryType), METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_RegistryType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_RegistryType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_IdFormat_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Rules for specifying valid item Ids, if default than any name can be used */" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "Rules for specifying valid item Ids, if default than any name can be used" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_IdFormat = { "IdFormat", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistry, IdFormat), Z_Construct_UScriptStruct_FDataRegistryIdFormat, METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_IdFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_IdFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_ItemStruct_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Structure type of all for items in this registry */" },
		{ "DisplayThumbnail", "false" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "NativeConst", "" },
		{ "ToolTip", "Structure type of all for items in this registry" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_ItemStruct = { "ItemStruct", nullptr, (EPropertyFlags)0x0020090000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistry, ItemStruct), Z_Construct_UClass_UScriptStruct, METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_ItemStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_ItemStruct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_Inner_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** List of data sources to search for items */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "List of data sources to search for items" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_Inner = { "DataSources", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataRegistrySource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** List of data sources to search for items */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "List of data sources to search for items" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources = { "DataSources", nullptr, (EPropertyFlags)0x0020088000010009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistry, DataSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_Inner_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Runtime list of data sources, created from above list and includes sources added at runtime */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "Runtime list of data sources, created from above list and includes sources added at runtime" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_Inner = { "RuntimeSources", nullptr, (EPropertyFlags)0x00020000000a0008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataRegistrySource_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_MetaData[] = {
		{ "Category", "DataRegistry" },
		{ "Comment", "/** Runtime list of data sources, created from above list and includes sources added at runtime */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "Runtime list of data sources, created from above list and includes sources added at runtime" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources = { "RuntimeSources", nullptr, (EPropertyFlags)0x0020088000032009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistry, RuntimeSources), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_TimerUpdateFrequency_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** How often to check for cache updates */" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "How often to check for cache updates" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_TimerUpdateFrequency = { "TimerUpdateFrequency", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistry, TimerUpdateFrequency), METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_TimerUpdateFrequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_TimerUpdateFrequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataRegistry_Statics::NewProp_DefaultCachePolicy_MetaData[] = {
		{ "Category", "Cache" },
		{ "Comment", "/** Editor-set cache policy */" },
		{ "ModuleRelativePath", "Public/DataRegistry.h" },
		{ "ToolTip", "Editor-set cache policy" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataRegistry_Statics::NewProp_DefaultCachePolicy = { "DefaultCachePolicy", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataRegistry, DefaultCachePolicy), Z_Construct_UScriptStruct_FDataRegistryCachePolicy, METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::NewProp_DefaultCachePolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::NewProp_DefaultCachePolicy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataRegistry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_RegistryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_IdFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_ItemStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_DataSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_RuntimeSources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_TimerUpdateFrequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataRegistry_Statics::NewProp_DefaultCachePolicy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataRegistry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataRegistry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataRegistry_Statics::ClassParams = {
		&UDataRegistry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataRegistry_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataRegistry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataRegistry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataRegistry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataRegistry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataRegistry, 4255691701);
	template<> DATAREGISTRY_API UClass* StaticClass<UDataRegistry>()
	{
		return UDataRegistry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataRegistry(Z_Construct_UClass_UDataRegistry, &UDataRegistry::StaticClass, TEXT("/Script/DataRegistry"), TEXT("UDataRegistry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataRegistry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
