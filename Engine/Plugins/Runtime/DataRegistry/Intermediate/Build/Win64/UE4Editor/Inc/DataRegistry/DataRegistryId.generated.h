// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAREGISTRY_DataRegistryId_generated_h
#error "DataRegistryId.generated.h already included, missing '#pragma once' in DataRegistryId.h"
#endif
#define DATAREGISTRY_DataRegistryId_generated_h

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryId_h_122_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataRegistryId_Statics; \
	DATAREGISTRY_API static class UScriptStruct* StaticStruct();


template<> DATAREGISTRY_API UScriptStruct* StaticStruct<struct FDataRegistryId>();

#define Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryId_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDataRegistryType_Statics; \
	DATAREGISTRY_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Name() { return STRUCT_OFFSET(FDataRegistryType, Name); }


template<> DATAREGISTRY_API UScriptStruct* StaticStruct<struct FDataRegistryType>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_DataRegistry_Source_DataRegistry_Public_DataRegistryId_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
