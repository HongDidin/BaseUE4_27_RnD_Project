// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FVector;
struct FRotator;
class AActor;
#ifdef WARPUTILS_WarpUtilsBlueprintLibrary_generated_h
#error "WarpUtilsBlueprintLibrary.generated.h already included, missing '#pragma once' in WarpUtilsBlueprintLibrary.h"
#endif
#define WARPUTILS_WarpUtilsBlueprintLibrary_generated_h

#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGeneratePFMEx); \
	DECLARE_FUNCTION(execGeneratePFM); \
	DECLARE_FUNCTION(execSavePFMEx); \
	DECLARE_FUNCTION(execSavePFM);


#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGeneratePFMEx); \
	DECLARE_FUNCTION(execGeneratePFM); \
	DECLARE_FUNCTION(execSavePFMEx); \
	DECLARE_FUNCTION(execSavePFM);


#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUWarpUtilsBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UWarpUtilsBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UWarpUtilsBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WarpUtils"), NO_API) \
	DECLARE_SERIALIZER(UWarpUtilsBlueprintLibrary)


#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUWarpUtilsBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UWarpUtilsBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UWarpUtilsBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/WarpUtils"), NO_API) \
	DECLARE_SERIALIZER(UWarpUtilsBlueprintLibrary)


#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWarpUtilsBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWarpUtilsBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWarpUtilsBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWarpUtilsBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWarpUtilsBlueprintLibrary(UWarpUtilsBlueprintLibrary&&); \
	NO_API UWarpUtilsBlueprintLibrary(const UWarpUtilsBlueprintLibrary&); \
public:


#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UWarpUtilsBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UWarpUtilsBlueprintLibrary(UWarpUtilsBlueprintLibrary&&); \
	NO_API UWarpUtilsBlueprintLibrary(const UWarpUtilsBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UWarpUtilsBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UWarpUtilsBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UWarpUtilsBlueprintLibrary)


#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_10_PROLOG
#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_INCLASS \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WARPUTILS_API UClass* StaticClass<class UWarpUtilsBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WarpUtils_Source_WarpUtils_Public_Blueprints_WarpUtilsBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
