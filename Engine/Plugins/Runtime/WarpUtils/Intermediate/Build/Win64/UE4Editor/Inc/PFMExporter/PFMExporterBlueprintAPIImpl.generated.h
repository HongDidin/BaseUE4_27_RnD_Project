// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UStaticMeshComponent;
class USceneComponent;
#ifdef PFMEXPORTER_PFMExporterBlueprintAPIImpl_generated_h
#error "PFMExporterBlueprintAPIImpl.generated.h already included, missing '#pragma once' in PFMExporterBlueprintAPIImpl.h"
#endif
#define PFMEXPORTER_PFMExporterBlueprintAPIImpl_generated_h

#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_SPARSE_DATA
#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExportPFM);


#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExportPFM);


#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPFMExporterAPIImpl(); \
	friend struct Z_Construct_UClass_UPFMExporterAPIImpl_Statics; \
public: \
	DECLARE_CLASS(UPFMExporterAPIImpl, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PFMExporter"), NO_API) \
	DECLARE_SERIALIZER(UPFMExporterAPIImpl) \
	virtual UObject* _getUObject() const override { return const_cast<UPFMExporterAPIImpl*>(this); }


#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUPFMExporterAPIImpl(); \
	friend struct Z_Construct_UClass_UPFMExporterAPIImpl_Statics; \
public: \
	DECLARE_CLASS(UPFMExporterAPIImpl, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PFMExporter"), NO_API) \
	DECLARE_SERIALIZER(UPFMExporterAPIImpl) \
	virtual UObject* _getUObject() const override { return const_cast<UPFMExporterAPIImpl*>(this); }


#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPFMExporterAPIImpl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPFMExporterAPIImpl) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPFMExporterAPIImpl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPFMExporterAPIImpl); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPFMExporterAPIImpl(UPFMExporterAPIImpl&&); \
	NO_API UPFMExporterAPIImpl(const UPFMExporterAPIImpl&); \
public:


#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPFMExporterAPIImpl(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPFMExporterAPIImpl(UPFMExporterAPIImpl&&); \
	NO_API UPFMExporterAPIImpl(const UPFMExporterAPIImpl&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPFMExporterAPIImpl); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPFMExporterAPIImpl); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPFMExporterAPIImpl)


#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_14_PROLOG
#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_RPC_WRAPPERS \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_INCLASS \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_SPARSE_DATA \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PFMEXPORTER_API UClass* StaticClass<class UPFMExporterAPIImpl>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_WarpUtils_Source_PFMExporter_Private_Blueprints_PFMExporterBlueprintAPIImpl_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
