// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PFMExporter/Public/Blueprints/IPFMExporterBlueprintAPI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIPFMExporterBlueprintAPI() {}
// Cross Module References
	PFMEXPORTER_API UClass* Z_Construct_UClass_UPFMExporterBlueprintAPI_NoRegister();
	PFMEXPORTER_API UClass* Z_Construct_UClass_UPFMExporterBlueprintAPI();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_PFMExporter();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(IPFMExporterBlueprintAPI::execExportPFM)
	{
		P_GET_OBJECT(UStaticMeshComponent,Z_Param_SrcMesh);
		P_GET_OBJECT(USceneComponent,Z_Param_Origin);
		P_GET_PROPERTY(FIntProperty,Z_Param_Width);
		P_GET_PROPERTY(FIntProperty,Z_Param_Height);
		P_GET_PROPERTY(FStrProperty,Z_Param_FileName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ExportPFM(Z_Param_SrcMesh,Z_Param_Origin,Z_Param_Width,Z_Param_Height,Z_Param_FileName);
		P_NATIVE_END;
	}
	void UPFMExporterBlueprintAPI::StaticRegisterNativesUPFMExporterBlueprintAPI()
	{
		UClass* Class = UPFMExporterBlueprintAPI::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ExportPFM", &IPFMExporterBlueprintAPI::execExportPFM },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics
	{
		struct PFMExporterBlueprintAPI_eventExportPFM_Parms
		{
			UStaticMeshComponent* SrcMesh;
			USceneComponent* Origin;
			int32 Width;
			int32 Height;
			FString FileName;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SrcMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SrcMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Origin;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Width;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_SrcMesh_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_SrcMesh = { "SrcMesh", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PFMExporterBlueprintAPI_eventExportPFM_Parms, SrcMesh), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_SrcMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_SrcMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Origin_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Origin = { "Origin", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PFMExporterBlueprintAPI_eventExportPFM_Parms, Origin), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Origin_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Origin_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PFMExporterBlueprintAPI_eventExportPFM_Parms, Width), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PFMExporterBlueprintAPI_eventExportPFM_Parms, Height), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_FileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PFMExporterBlueprintAPI_eventExportPFM_Parms, FileName), METADATA_PARAMS(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_FileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_FileName_MetaData)) };
	void Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PFMExporterBlueprintAPI_eventExportPFM_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PFMExporterBlueprintAPI_eventExportPFM_Parms), &Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_SrcMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Origin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_FileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::Function_MetaDataParams[] = {
		{ "Category", "PFMExporter" },
		{ "Comment", "/**\n\x09* Generate PFM file from static mesh.\n\x09* The UV channel must be defined, assigned range 0..1 used as screen surface.\n\x09* Origin assigned by function arg, or by default used mesh parent.\n\x09*\n\x09* @param SrcMesh - Static mesh with assigned UV channel, used as export source of PFM file\n\x09* @param Origin - (Optional) Custom cave origin node, if not defined, used SrcMesh parent\n\x09* @param Width - Output PFM mesh texture width\n\x09* @param Height - Output PFM mesh texture height\n\x09* @param FileName - Output PFM file name\n\x09*\n\x09* @return true, if success\n\x09*/" },
		{ "DisplayName", "Export Static Mesh to PFM file" },
		{ "ModuleRelativePath", "Public/Blueprints/IPFMExporterBlueprintAPI.h" },
		{ "ToolTip", "Generate PFM file from static mesh.\nThe UV channel must be defined, assigned range 0..1 used as screen surface.\nOrigin assigned by function arg, or by default used mesh parent.\n\n@param SrcMesh - Static mesh with assigned UV channel, used as export source of PFM file\n@param Origin - (Optional) Custom cave origin node, if not defined, used SrcMesh parent\n@param Width - Output PFM mesh texture width\n@param Height - Output PFM mesh texture height\n@param FileName - Output PFM file name\n\n@return true, if success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPFMExporterBlueprintAPI, nullptr, "ExportPFM", nullptr, nullptr, sizeof(PFMExporterBlueprintAPI_eventExportPFM_Parms), Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPFMExporterBlueprintAPI_NoRegister()
	{
		return UPFMExporterBlueprintAPI::StaticClass();
	}
	struct Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_PFMExporter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPFMExporterBlueprintAPI_ExportPFM, "ExportPFM" }, // 1254256299
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Public/Blueprints/IPFMExporterBlueprintAPI.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IPFMExporterBlueprintAPI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::ClassParams = {
		&UPFMExporterBlueprintAPI::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPFMExporterBlueprintAPI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPFMExporterBlueprintAPI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPFMExporterBlueprintAPI, 4223385374);
	template<> PFMEXPORTER_API UClass* StaticClass<UPFMExporterBlueprintAPI>()
	{
		return UPFMExporterBlueprintAPI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPFMExporterBlueprintAPI(Z_Construct_UClass_UPFMExporterBlueprintAPI, &UPFMExporterBlueprintAPI::StaticClass, TEXT("/Script/PFMExporter"), TEXT("UPFMExporterBlueprintAPI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPFMExporterBlueprintAPI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
