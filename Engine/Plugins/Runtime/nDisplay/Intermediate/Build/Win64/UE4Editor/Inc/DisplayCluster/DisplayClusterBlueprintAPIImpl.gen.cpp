// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Private/Blueprints/DisplayClusterBlueprintAPIImpl.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterBlueprintAPIImpl() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterClusterEventListener_NoRegister();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson();
	DISPLAYCLUSTER_API UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	DISPLAYCLUSTER_API UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_ADisplayClusterRootActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPostProcessSettings();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPI_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSceneViewExtensionIsActiveInContextFunction)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_ViewportIDs);
		P_GET_STRUCT_REF(FSceneViewExtensionIsActiveFunctor,Z_Param_Out_OutIsActiveFunction);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SceneViewExtensionIsActiveInContextFunction(Z_Param_Out_ViewportIDs,Z_Param_Out_OutIsActiveFunction);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetLocalViewports)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_ViewportIDs);
		P_GET_TARRAY_REF(FString,Z_Param_Out_ProjectionTypes);
		P_GET_TARRAY_REF(FIntPoint,Z_Param_Out_ViewportLocations);
		P_GET_TARRAY_REF(FIntPoint,Z_Param_Out_ViewportSizes);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetLocalViewports(Z_Param_Out_ViewportIDs,Z_Param_Out_ProjectionTypes,Z_Param_Out_ViewportLocations,Z_Param_Out_ViewportSizes);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetViewportRect)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_ViewportLoc);
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_ViewportSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetViewportRect(Z_Param_ViewportId,Z_Param_Out_ViewportLoc,Z_Param_Out_ViewportSize);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSetFinalPostProcessingSettings)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FPostProcessSettings,Z_Param_Out_FinalPostProcessingSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFinalPostProcessingSettings(Z_Param_ViewportId,Z_Param_Out_FinalPostProcessingSettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSetOverridePostProcessingSettings)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FPostProcessSettings,Z_Param_Out_OverridePostProcessingSettings);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BlendWeight);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetOverridePostProcessingSettings(Z_Param_ViewportId,Z_Param_Out_OverridePostProcessingSettings,Z_Param_BlendWeight);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSetStartPostProcessingSettings)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FPostProcessSettings,Z_Param_Out_StartPostProcessingSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStartPostProcessingSettings(Z_Param_ViewportId,Z_Param_Out_StartPostProcessingSettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSetBufferRatio)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BufferRatio);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetBufferRatio(Z_Param_ViewportId,Z_Param_BufferRatio);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetBufferRatio)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_BufferRatio);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetBufferRatio(Z_Param_ViewportId,Z_Param_Out_BufferRatio);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSetViewportCamera)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_CameraId);
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetViewportCamera(Z_Param_CameraId,Z_Param_ViewportId);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetTrackerQuat)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_STRUCT_REF(FQuat,Z_Param_Out_Rotation);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetTrackerQuat(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_Rotation,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetTrackerLocation)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetTrackerLocation(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_Location,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetAxis)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_Value);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAxis(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_Value,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execWasButtonReleased)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_WasReleased);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WasButtonReleased(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_WasReleased,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execWasButtonPressed)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_WasPressed);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WasButtonPressed(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_WasPressed,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execIsButtonReleased)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_IsReleasedCurrently);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IsButtonReleased(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_IsReleasedCurrently,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execIsButtonPressed)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_IsPressedCurrently);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IsButtonPressed(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_IsPressedCurrently,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetButtonState)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_CurrentState);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetButtonState(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_CurrentState,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetTrackerDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetTrackerDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetKeyboardDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetKeyboardDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetButtonDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetButtonDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetAxisDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAxisDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetTrackerDeviceAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetTrackerDeviceAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetButtonDeviceAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetButtonDeviceAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetAxisDeviceAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetAxisDeviceAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetRootActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ADisplayClusterRootActor**)Z_Param__Result=P_THIS->GetRootActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetConfig)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterConfigurationData**)Z_Param__Result=P_THIS->GetConfig();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSendClusterEventBinaryTo)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Address);
		P_GET_PROPERTY(FIntProperty,Z_Param_Port);
		P_GET_STRUCT_REF(FDisplayClusterClusterEventBinary,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendClusterEventBinaryTo(Z_Param_Address,Z_Param_Port,Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execSendClusterEventJsonTo)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Address);
		P_GET_PROPERTY(FIntProperty,Z_Param_Port);
		P_GET_STRUCT_REF(FDisplayClusterClusterEventJson,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendClusterEventJsonTo(Z_Param_Address,Z_Param_Port,Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execEmitClusterEventBinary)
	{
		P_GET_STRUCT_REF(FDisplayClusterClusterEventBinary,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EmitClusterEventBinary(Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execEmitClusterEventJson)
	{
		P_GET_STRUCT_REF(FDisplayClusterClusterEventJson,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EmitClusterEventJson(Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execRemoveClusterEventListener)
	{
		P_GET_TINTERFACE(IDisplayClusterClusterEventListener,Z_Param_Listener);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveClusterEventListener(Z_Param_Listener);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execAddClusterEventListener)
	{
		P_GET_TINTERFACE(IDisplayClusterClusterEventListener,Z_Param_Listener);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddClusterEventListener(Z_Param_Listener);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetNodesAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetNodesAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetNodeId)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetNodeId();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetNodeIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutNodeIds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetNodeIds(Z_Param_Out_OutNodeIds);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetClusterRole)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDisplayClusterNodeRole*)Z_Param__Result=P_THIS->GetClusterRole();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execIsBackup)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsBackup();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execIsSlave)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsSlave();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execIsMaster)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsMaster();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execGetOperationMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDisplayClusterOperationMode*)Z_Param__Result=P_THIS->GetOperationMode();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterBlueprintAPIImpl::execIsModuleInitialized)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsModuleInitialized();
		P_NATIVE_END;
	}
	void UDisplayClusterBlueprintAPIImpl::StaticRegisterNativesUDisplayClusterBlueprintAPIImpl()
	{
		UClass* Class = UDisplayClusterBlueprintAPIImpl::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddClusterEventListener", &UDisplayClusterBlueprintAPIImpl::execAddClusterEventListener },
			{ "EmitClusterEventBinary", &UDisplayClusterBlueprintAPIImpl::execEmitClusterEventBinary },
			{ "EmitClusterEventJson", &UDisplayClusterBlueprintAPIImpl::execEmitClusterEventJson },
			{ "GetAxis", &UDisplayClusterBlueprintAPIImpl::execGetAxis },
			{ "GetAxisDeviceAmount", &UDisplayClusterBlueprintAPIImpl::execGetAxisDeviceAmount },
			{ "GetAxisDeviceIds", &UDisplayClusterBlueprintAPIImpl::execGetAxisDeviceIds },
			{ "GetBufferRatio", &UDisplayClusterBlueprintAPIImpl::execGetBufferRatio },
			{ "GetButtonDeviceAmount", &UDisplayClusterBlueprintAPIImpl::execGetButtonDeviceAmount },
			{ "GetButtonDeviceIds", &UDisplayClusterBlueprintAPIImpl::execGetButtonDeviceIds },
			{ "GetButtonState", &UDisplayClusterBlueprintAPIImpl::execGetButtonState },
			{ "GetClusterRole", &UDisplayClusterBlueprintAPIImpl::execGetClusterRole },
			{ "GetConfig", &UDisplayClusterBlueprintAPIImpl::execGetConfig },
			{ "GetKeyboardDeviceIds", &UDisplayClusterBlueprintAPIImpl::execGetKeyboardDeviceIds },
			{ "GetLocalViewports", &UDisplayClusterBlueprintAPIImpl::execGetLocalViewports },
			{ "GetNodeId", &UDisplayClusterBlueprintAPIImpl::execGetNodeId },
			{ "GetNodeIds", &UDisplayClusterBlueprintAPIImpl::execGetNodeIds },
			{ "GetNodesAmount", &UDisplayClusterBlueprintAPIImpl::execGetNodesAmount },
			{ "GetOperationMode", &UDisplayClusterBlueprintAPIImpl::execGetOperationMode },
			{ "GetRootActor", &UDisplayClusterBlueprintAPIImpl::execGetRootActor },
			{ "GetTrackerDeviceAmount", &UDisplayClusterBlueprintAPIImpl::execGetTrackerDeviceAmount },
			{ "GetTrackerDeviceIds", &UDisplayClusterBlueprintAPIImpl::execGetTrackerDeviceIds },
			{ "GetTrackerLocation", &UDisplayClusterBlueprintAPIImpl::execGetTrackerLocation },
			{ "GetTrackerQuat", &UDisplayClusterBlueprintAPIImpl::execGetTrackerQuat },
			{ "GetViewportRect", &UDisplayClusterBlueprintAPIImpl::execGetViewportRect },
			{ "IsBackup", &UDisplayClusterBlueprintAPIImpl::execIsBackup },
			{ "IsButtonPressed", &UDisplayClusterBlueprintAPIImpl::execIsButtonPressed },
			{ "IsButtonReleased", &UDisplayClusterBlueprintAPIImpl::execIsButtonReleased },
			{ "IsMaster", &UDisplayClusterBlueprintAPIImpl::execIsMaster },
			{ "IsModuleInitialized", &UDisplayClusterBlueprintAPIImpl::execIsModuleInitialized },
			{ "IsSlave", &UDisplayClusterBlueprintAPIImpl::execIsSlave },
			{ "RemoveClusterEventListener", &UDisplayClusterBlueprintAPIImpl::execRemoveClusterEventListener },
			{ "SceneViewExtensionIsActiveInContextFunction", &UDisplayClusterBlueprintAPIImpl::execSceneViewExtensionIsActiveInContextFunction },
			{ "SendClusterEventBinaryTo", &UDisplayClusterBlueprintAPIImpl::execSendClusterEventBinaryTo },
			{ "SendClusterEventJsonTo", &UDisplayClusterBlueprintAPIImpl::execSendClusterEventJsonTo },
			{ "SetBufferRatio", &UDisplayClusterBlueprintAPIImpl::execSetBufferRatio },
			{ "SetFinalPostProcessingSettings", &UDisplayClusterBlueprintAPIImpl::execSetFinalPostProcessingSettings },
			{ "SetOverridePostProcessingSettings", &UDisplayClusterBlueprintAPIImpl::execSetOverridePostProcessingSettings },
			{ "SetStartPostProcessingSettings", &UDisplayClusterBlueprintAPIImpl::execSetStartPostProcessingSettings },
			{ "SetViewportCamera", &UDisplayClusterBlueprintAPIImpl::execSetViewportCamera },
			{ "WasButtonPressed", &UDisplayClusterBlueprintAPIImpl::execWasButtonPressed },
			{ "WasButtonReleased", &UDisplayClusterBlueprintAPIImpl::execWasButtonReleased },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventAddClusterEventListener_Parms
		{
			TScriptInterface<IDisplayClusterClusterEventListener> Listener;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_Listener;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::NewProp_Listener = { "Listener", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventAddClusterEventListener_Parms, Listener), Z_Construct_UClass_UDisplayClusterClusterEventListener_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::NewProp_Listener,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Add cluster event listener" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "AddClusterEventListener", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventAddClusterEventListener_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventEmitClusterEventBinary_Parms
		{
			FDisplayClusterClusterEventBinary Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventEmitClusterEventBinary_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventEmitClusterEventBinary_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventEmitClusterEventBinary_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Emit binary cluster event" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "EmitClusterEventBinary", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventEmitClusterEventBinary_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventEmitClusterEventJson_Parms
		{
			FDisplayClusterClusterEventJson Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventEmitClusterEventJson_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventEmitClusterEventJson_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventEmitClusterEventJson_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Emit JSON cluster event" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "EmitClusterEventJson", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventEmitClusterEventJson_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetAxis_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			float Value;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetAxis_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetAxis_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetAxis_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventGetAxis_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventGetAxis_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "// Axes\n" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN axis value" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "Axes" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetAxis", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetAxis_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetAxisDeviceAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetAxisDeviceAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Input API\n//////////////////////////////////////////////////////////////////////////////////////////////\n// Device information\n" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get amount of VRPN axis devices" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "/\n Input API\n/\n Device information" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetAxisDeviceAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetAxisDeviceAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetAxisDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetAxisDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of VRPN axis devices" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetAxisDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetAxisDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetBufferRatio_Parms
		{
			FString ViewportId;
			float BufferRatio;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetBufferRatio_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetBufferRatio_Parms, BufferRatio), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventGetBufferRatio_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventGetBufferRatio_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Get viewport's buffer ratio" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetBufferRatio", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetBufferRatio_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetButtonDeviceAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetButtonDeviceAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get amount of VRPN button devices" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetButtonDeviceAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetButtonDeviceAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetButtonDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetButtonDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of VRPN button devices" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetButtonDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetButtonDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool CurrentState;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_CurrentState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CurrentState;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_CurrentState_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms*)Obj)->CurrentState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_CurrentState = { "CurrentState", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_CurrentState_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_CurrentState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "// Buttons\n" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN button state" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "Buttons" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetButtonState", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetButtonState_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetClusterRole_Parms
		{
			EDisplayClusterNodeRole ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetClusterRole_Parms, ReturnValue), Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Get cluster role" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetClusterRole", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetClusterRole_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetConfig_Parms
		{
			UDisplayClusterConfigurationData* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetConfig_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Config API\n//////////////////////////////////////////////////////////////////////////////////////////////\n" },
		{ "DisplayName", "Get config data" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "/\n Config API\n/" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetConfig", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetConfig_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetKeyboardDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetKeyboardDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of keyboard devices" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetKeyboardDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetKeyboardDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetLocalViewports_Parms
		{
			TArray<FString> ViewportIDs;
			TArray<FString> ProjectionTypes;
			TArray<FIntPoint> ViewportLocations;
			TArray<FIntPoint> ViewportSizes;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportIDs;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProjectionTypes_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ProjectionTypes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportLocations_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportLocations;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportSizes_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportSizes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportIDs_Inner = { "ViewportIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportIDs = { "ViewportIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetLocalViewports_Parms, ViewportIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ProjectionTypes_Inner = { "ProjectionTypes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ProjectionTypes = { "ProjectionTypes", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetLocalViewports_Parms, ProjectionTypes), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportLocations_Inner = { "ViewportLocations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportLocations = { "ViewportLocations", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetLocalViewports_Parms, ViewportLocations), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportSizes_Inner = { "ViewportSizes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportSizes = { "ViewportSizes", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetLocalViewports_Parms, ViewportSizes), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportIDs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ProjectionTypes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ProjectionTypes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportLocations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportLocations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportSizes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::NewProp_ViewportSizes,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Config" },
		{ "Comment", "/** Returns list of local viewports. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use new api" },
		{ "DisplayName", "Get local viewports" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "Returns list of local viewports." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetLocalViewports", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetLocalViewports_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetNodeId_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetNodeId_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Get node ID" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetNodeId", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetNodeId_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetNodeIds_Parms
		{
			TArray<FString> OutNodeIds;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutNodeIds_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutNodeIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::NewProp_OutNodeIds_Inner = { "OutNodeIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::NewProp_OutNodeIds = { "OutNodeIds", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetNodeIds_Parms, OutNodeIds), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::NewProp_OutNodeIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::NewProp_OutNodeIds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Get cluster node IDs" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetNodeIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetNodeIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetNodesAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetNodesAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Get nodes amount" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetNodesAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetNodesAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetOperationMode_Parms
		{
			EDisplayClusterOperationMode ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetOperationMode_Parms, ReturnValue), Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay" },
		{ "DisplayName", "Get operation mode" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetOperationMode", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetOperationMode_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetRootActor_Parms
		{
			ADisplayClusterRootActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetRootActor_Parms, ReturnValue), Z_Construct_UClass_ADisplayClusterRootActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Game API\n//////////////////////////////////////////////////////////////////////////////////////////////\n// Root\n" },
		{ "DisplayName", "Get root actor" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "/\n Game API\n/\n Root" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetRootActor", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetRootActor_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetTrackerDeviceAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerDeviceAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get amount of VRPN tracker devices" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetTrackerDeviceAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetTrackerDeviceAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetTrackerDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of VRPN tracker devices" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetTrackerDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetTrackerDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetTrackerLocation_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			FVector Location;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerLocation_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerLocation_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerLocation_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventGetTrackerLocation_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventGetTrackerLocation_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "// Trackers\n" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN tracker location" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "Trackers" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetTrackerLocation", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetTrackerLocation_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetTrackerQuat_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			FQuat Rotation;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerQuat_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerQuat_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetTrackerQuat_Parms, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventGetTrackerQuat_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventGetTrackerQuat_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN tracker rotation (as quaternion)" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetTrackerQuat", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetTrackerQuat_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventGetViewportRect_Parms
		{
			FString ViewportId;
			FIntPoint ViewportLoc;
			FIntPoint ViewportSize;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportLoc;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportSize;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetViewportRect_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportLoc = { "ViewportLoc", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetViewportRect_Parms, ViewportLoc), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportSize = { "ViewportSize", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventGetViewportRect_Parms, ViewportSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventGetViewportRect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventGetViewportRect_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportLoc,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ViewportSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use new api" },
		{ "DisplayName", "Get Updated Post Processing" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "GetViewportRect", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventGetViewportRect_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventIsBackup_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsBackup_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsBackup_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns true if current node is a backup node in a cluster. */" },
		{ "DisplayName", "Is backup node" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "Returns true if current node is a backup node in a cluster." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "IsBackup", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventIsBackup_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool IsPressedCurrently;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_IsPressedCurrently_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsPressedCurrently;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsPressedCurrently_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms*)Obj)->IsPressedCurrently = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsPressedCurrently = { "IsPressedCurrently", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsPressedCurrently_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsPressedCurrently,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Is VRPN button pressed" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "IsButtonPressed", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventIsButtonPressed_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool IsReleasedCurrently;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_IsReleasedCurrently_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsReleasedCurrently;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsReleasedCurrently_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms*)Obj)->IsReleasedCurrently = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsReleasedCurrently = { "IsReleasedCurrently", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsReleasedCurrently_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsReleasedCurrently,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Is VRPN button released" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "IsButtonReleased", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventIsButtonReleased_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventIsMaster_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsMaster_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsMaster_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Cluster API\n//////////////////////////////////////////////////////////////////////////////////////////////\n" },
		{ "DisplayName", "Is master node" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "/\n Cluster API\n/" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "IsMaster", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventIsMaster_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventIsModuleInitialized_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsModuleInitialized_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsModuleInitialized_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// DisplayCluster module API\n//////////////////////////////////////////////////////////////////////////////////////////////\n" },
		{ "DisplayName", "Is module initialized" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "/\n DisplayCluster module API\n/" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "IsModuleInitialized", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventIsModuleInitialized_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventIsSlave_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventIsSlave_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventIsSlave_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Is slave node" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "IsSlave", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventIsSlave_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventRemoveClusterEventListener_Parms
		{
			TScriptInterface<IDisplayClusterClusterEventListener> Listener;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_Listener;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::NewProp_Listener = { "Listener", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventRemoveClusterEventListener_Parms, Listener), Z_Construct_UClass_UDisplayClusterClusterEventListener_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::NewProp_Listener,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Remove cluster event listener" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "RemoveClusterEventListener", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventRemoveClusterEventListener_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSceneViewExtensionIsActiveInContextFunction_Parms
		{
			TArray<FString> ViewportIDs;
			FSceneViewExtensionIsActiveFunctor OutIsActiveFunction;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportIDs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportIDs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportIDs;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutIsActiveFunction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_Inner = { "ViewportIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs = { "ViewportIDs", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSceneViewExtensionIsActiveInContextFunction_Parms, ViewportIDs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_OutIsActiveFunction = { "OutIsActiveFunction", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSceneViewExtensionIsActiveInContextFunction_Parms, OutIsActiveFunction), Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_OutIsActiveFunction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Scene View Extension Is Active In Context Function" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SceneViewExtensionIsActiveInContextFunction", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSceneViewExtensionIsActiveInContextFunction_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSendClusterEventBinaryTo_Parms
		{
			FString Address;
			int32 Port;
			FDisplayClusterClusterEventBinary Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Address;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Port_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Port;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Address_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSendClusterEventBinaryTo_Parms, Address), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Address_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Port_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Port = { "Port", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSendClusterEventBinaryTo_Parms, Port), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Port_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Port_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSendClusterEventBinaryTo_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventSendClusterEventBinaryTo_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventSendClusterEventBinaryTo_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Address,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Port,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Emit binary cluster event" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SendClusterEventBinaryTo", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSendClusterEventBinaryTo_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSendClusterEventJsonTo_Parms
		{
			FString Address;
			int32 Port;
			FDisplayClusterClusterEventJson Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Address;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Port_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Port;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Address_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSendClusterEventJsonTo_Parms, Address), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Address_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Port_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Port = { "Port", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSendClusterEventJsonTo_Parms, Port), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Port_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Port_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSendClusterEventJsonTo_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventSendClusterEventJsonTo_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventSendClusterEventJsonTo_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Address,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Port,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "DisplayName", "Emit JSON cluster event" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SendClusterEventJsonTo", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSendClusterEventJsonTo_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSetBufferRatio_Parms
		{
			FString ViewportId;
			float BufferRatio;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetBufferRatio_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetBufferRatio_Parms, BufferRatio), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventSetBufferRatio_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventSetBufferRatio_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set viewport's buffer ratio" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SetBufferRatio", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSetBufferRatio_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSetFinalPostProcessingSettings_Parms
		{
			FString ViewportId;
			FPostProcessSettings FinalPostProcessingSettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalPostProcessingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FinalPostProcessingSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetFinalPostProcessingSettings_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings = { "FinalPostProcessingSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetFinalPostProcessingSettings_Parms, FinalPostProcessingSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set final post processing settings for viewport" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SetFinalPostProcessingSettings", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSetFinalPostProcessingSettings_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSetOverridePostProcessingSettings_Parms
		{
			FString ViewportId;
			FPostProcessSettings OverridePostProcessingSettings;
			float BlendWeight;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverridePostProcessingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OverridePostProcessingSettings;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BlendWeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetOverridePostProcessingSettings_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings = { "OverridePostProcessingSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetOverridePostProcessingSettings_Parms, OverridePostProcessingSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_BlendWeight = { "BlendWeight", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetOverridePostProcessingSettings_Parms, BlendWeight), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::NewProp_BlendWeight,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "CPP_Default_BlendWeight", "1.000000" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set override post processing settings for viewport" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SetOverridePostProcessingSettings", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSetOverridePostProcessingSettings_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSetStartPostProcessingSettings_Parms
		{
			FString ViewportId;
			FPostProcessSettings StartPostProcessingSettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartPostProcessingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StartPostProcessingSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetStartPostProcessingSettings_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_ViewportId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings = { "StartPostProcessingSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetStartPostProcessingSettings_Parms, StartPostProcessingSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set start post processing settings for viewport" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SetStartPostProcessingSettings", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSetStartPostProcessingSettings_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventSetViewportCamera_Parms
		{
			FString CameraId;
			FString ViewportId;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CameraId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_CameraId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_CameraId = { "CameraId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetViewportCamera_Parms, CameraId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_CameraId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_CameraId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventSetViewportCamera_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_CameraId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::NewProp_ViewportId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Render API\n//////////////////////////////////////////////////////////////////////////////////////////////\n" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set viewport camera" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "/\n Render API\n/" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "SetViewportCamera", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventSetViewportCamera_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool WasPressed;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_WasPressed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WasPressed;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_WasPressed_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms*)Obj)->WasPressed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_WasPressed = { "WasPressed", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_WasPressed_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_WasPressed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Was VRPN button pressed" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "WasButtonPressed", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventWasButtonPressed_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics
	{
		struct DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool WasReleased;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_WasReleased_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WasReleased;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_WasReleased_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms*)Obj)->WasReleased = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_WasReleased = { "WasReleased", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_WasReleased_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_WasReleased,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Was VRPN button released" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, nullptr, "WasButtonReleased", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPIImpl_eventWasButtonReleased_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_NoRegister()
	{
		return UDisplayClusterBlueprintAPIImpl::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_AddClusterEventListener, "AddClusterEventListener" }, // 1990634387
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventBinary, "EmitClusterEventBinary" }, // 2561630775
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_EmitClusterEventJson, "EmitClusterEventJson" }, // 3532127302
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxis, "GetAxis" }, // 3570341035
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceAmount, "GetAxisDeviceAmount" }, // 3687322622
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetAxisDeviceIds, "GetAxisDeviceIds" }, // 1849812595
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetBufferRatio, "GetBufferRatio" }, // 1249835262
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceAmount, "GetButtonDeviceAmount" }, // 2845761566
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonDeviceIds, "GetButtonDeviceIds" }, // 650008529
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetButtonState, "GetButtonState" }, // 2874898458
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetClusterRole, "GetClusterRole" }, // 3111672827
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetConfig, "GetConfig" }, // 3516891228
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetKeyboardDeviceIds, "GetKeyboardDeviceIds" }, // 3298717957
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetLocalViewports, "GetLocalViewports" }, // 3982760128
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeId, "GetNodeId" }, // 3520365805
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodeIds, "GetNodeIds" }, // 150661731
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetNodesAmount, "GetNodesAmount" }, // 1052741393
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetOperationMode, "GetOperationMode" }, // 2607270506
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetRootActor, "GetRootActor" }, // 2100514059
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceAmount, "GetTrackerDeviceAmount" }, // 2813398431
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerDeviceIds, "GetTrackerDeviceIds" }, // 2492619717
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerLocation, "GetTrackerLocation" }, // 3141885284
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetTrackerQuat, "GetTrackerQuat" }, // 3470338236
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_GetViewportRect, "GetViewportRect" }, // 3999472363
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsBackup, "IsBackup" }, // 3239247013
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonPressed, "IsButtonPressed" }, // 1356666529
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsButtonReleased, "IsButtonReleased" }, // 631072163
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsMaster, "IsMaster" }, // 1055884812
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsModuleInitialized, "IsModuleInitialized" }, // 2380018636
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_IsSlave, "IsSlave" }, // 4293308551
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_RemoveClusterEventListener, "RemoveClusterEventListener" }, // 3996671639
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SceneViewExtensionIsActiveInContextFunction, "SceneViewExtensionIsActiveInContextFunction" }, // 3295080900
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventBinaryTo, "SendClusterEventBinaryTo" }, // 2678606353
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SendClusterEventJsonTo, "SendClusterEventJsonTo" }, // 985741224
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetBufferRatio, "SetBufferRatio" }, // 1598142413
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetFinalPostProcessingSettings, "SetFinalPostProcessingSettings" }, // 2422048409
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetOverridePostProcessingSettings, "SetOverridePostProcessingSettings" }, // 4237445867
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetStartPostProcessingSettings, "SetStartPostProcessingSettings" }, // 3759203675
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_SetViewportCamera, "SetViewportCamera" }, // 189509434
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonPressed, "WasButtonPressed" }, // 3815308032
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPIImpl_WasButtonReleased, "WasButtonReleased" }, // 1148710658
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Blueprint API interface implementation\n */" },
		{ "IncludePath", "Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ModuleRelativePath", "Private/Blueprints/DisplayClusterBlueprintAPIImpl.h" },
		{ "ToolTip", "Blueprint API interface implementation" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UDisplayClusterBlueprintAPI_NoRegister, (int32)VTABLE_OFFSET(UDisplayClusterBlueprintAPIImpl, IDisplayClusterBlueprintAPI), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterBlueprintAPIImpl>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::ClassParams = {
		&UDisplayClusterBlueprintAPIImpl::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterBlueprintAPIImpl, 3591509786);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterBlueprintAPIImpl>()
	{
		return UDisplayClusterBlueprintAPIImpl::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterBlueprintAPIImpl(Z_Construct_UClass_UDisplayClusterBlueprintAPIImpl, &UDisplayClusterBlueprintAPIImpl::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterBlueprintAPIImpl"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterBlueprintAPIImpl);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
