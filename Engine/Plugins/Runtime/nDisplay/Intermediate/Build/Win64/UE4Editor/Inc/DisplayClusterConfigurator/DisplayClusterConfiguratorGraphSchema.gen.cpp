// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfigurator/Private/Views/OutputMapping/DisplayClusterConfiguratorGraphSchema.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfiguratorGraphSchema() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATOR_API UEnum* Z_Construct_UEnum_DisplayClusterConfigurator_EClusterItemType();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfigurator();
	DISPLAYCLUSTERCONFIGURATOR_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	DISPLAYCLUSTERCONFIGURATOR_API UClass* Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_NoRegister();
	DISPLAYCLUSTERCONFIGURATOR_API UClass* Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema();
	ENGINE_API UClass* Z_Construct_UClass_UEdGraphSchema();
// End Cross Module References
	static UEnum* EClusterItemType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfigurator_EClusterItemType, Z_Construct_UPackage__Script_DisplayClusterConfigurator(), TEXT("EClusterItemType"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATOR_API UEnum* StaticEnum<EClusterItemType>()
	{
		return EClusterItemType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EClusterItemType(EClusterItemType_StaticEnum, TEXT("/Script/DisplayClusterConfigurator"), TEXT("EClusterItemType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfigurator_EClusterItemType_Hash() { return 3207476541U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfigurator_EClusterItemType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfigurator();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EClusterItemType"), 0, Get_Z_Construct_UEnum_DisplayClusterConfigurator_EClusterItemType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EClusterItemType::ClusterNode", (int64)EClusterItemType::ClusterNode },
				{ "EClusterItemType::Viewport", (int64)EClusterItemType::Viewport },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ClusterNode.Name", "EClusterItemType::ClusterNode" },
				{ "ModuleRelativePath", "Private/Views/OutputMapping/DisplayClusterConfiguratorGraphSchema.h" },
				{ "Viewport.Name", "EClusterItemType::Viewport" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfigurator,
				nullptr,
				"EClusterItemType",
				"EClusterItemType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FDisplayClusterConfiguratorSchemaAction_NewNode>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FDisplayClusterConfiguratorSchemaAction_NewNode cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FDisplayClusterConfiguratorSchemaAction_NewNode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATOR_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode, Z_Construct_UPackage__Script_DisplayClusterConfigurator(), TEXT("DisplayClusterConfiguratorSchemaAction_NewNode"), sizeof(FDisplayClusterConfiguratorSchemaAction_NewNode), Get_Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATOR_API UScriptStruct* StaticStruct<FDisplayClusterConfiguratorSchemaAction_NewNode>()
{
	return FDisplayClusterConfiguratorSchemaAction_NewNode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode(FDisplayClusterConfiguratorSchemaAction_NewNode::StaticStruct, TEXT("/Script/DisplayClusterConfigurator"), TEXT("DisplayClusterConfiguratorSchemaAction_NewNode"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfigurator_StaticRegisterNativesFDisplayClusterConfiguratorSchemaAction_NewNode
{
	FScriptStruct_DisplayClusterConfigurator_StaticRegisterNativesFDisplayClusterConfiguratorSchemaAction_NewNode()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfiguratorSchemaAction_NewNode>(FName(TEXT("DisplayClusterConfiguratorSchemaAction_NewNode")));
	}
} ScriptStruct_DisplayClusterConfigurator_StaticRegisterNativesFDisplayClusterConfiguratorSchemaAction_NewNode;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ItemType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ItemType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PresetSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Views/OutputMapping/DisplayClusterConfiguratorGraphSchema.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfiguratorSchemaAction_NewNode>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_ItemType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_ItemType_MetaData[] = {
		{ "ModuleRelativePath", "Private/Views/OutputMapping/DisplayClusterConfiguratorGraphSchema.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_ItemType = { "ItemType", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfiguratorSchemaAction_NewNode, ItemType), Z_Construct_UEnum_DisplayClusterConfigurator_EClusterItemType, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_ItemType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_ItemType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_PresetSize_MetaData[] = {
		{ "ModuleRelativePath", "Private/Views/OutputMapping/DisplayClusterConfiguratorGraphSchema.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_PresetSize = { "PresetSize", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfiguratorSchemaAction_NewNode, PresetSize), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_PresetSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_PresetSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_ItemType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_ItemType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::NewProp_PresetSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfigurator,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"DisplayClusterConfiguratorSchemaAction_NewNode",
		sizeof(FDisplayClusterConfiguratorSchemaAction_NewNode),
		alignof(FDisplayClusterConfiguratorSchemaAction_NewNode),
		Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfigurator();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfiguratorSchemaAction_NewNode"), sizeof(FDisplayClusterConfiguratorSchemaAction_NewNode), Get_Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Hash() { return 2394394745U; }
	void UDisplayClusterConfiguratorGraphSchema::StaticRegisterNativesUDisplayClusterConfiguratorGraphSchema()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_NoRegister()
	{
		return UDisplayClusterConfiguratorGraphSchema::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdGraphSchema,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfigurator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Views/OutputMapping/DisplayClusterConfiguratorGraphSchema.h" },
		{ "ModuleRelativePath", "Private/Views/OutputMapping/DisplayClusterConfiguratorGraphSchema.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfiguratorGraphSchema>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics::ClassParams = {
		&UDisplayClusterConfiguratorGraphSchema::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfiguratorGraphSchema, 337268026);
	template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<UDisplayClusterConfiguratorGraphSchema>()
	{
		return UDisplayClusterConfiguratorGraphSchema::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfiguratorGraphSchema(Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema, &UDisplayClusterConfiguratorGraphSchema::StaticClass, TEXT("/Script/DisplayClusterConfigurator"), TEXT("UDisplayClusterConfiguratorGraphSchema"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfiguratorGraphSchema);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
