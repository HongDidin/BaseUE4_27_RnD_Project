// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationJsonTypes_426() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426();
// End Cross Module References
class UScriptStruct* FDisplayClusterConfigurationJsonContainer_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonContainer_426"), sizeof(FDisplayClusterConfigurationJsonContainer_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonContainer_426>()
{
	return FDisplayClusterConfigurationJsonContainer_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426(FDisplayClusterConfigurationJsonContainer_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonContainer_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonContainer_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonContainer_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonContainer_426>(FName(TEXT("DisplayClusterConfigurationJsonContainer_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonContainer_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nDisplay_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_nDisplay;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// The main nDisplay configuration structure. It's supposed to extract nDisplay related data from a collecting JSON file.\n" },
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
		{ "ToolTip", "The main nDisplay configuration structure. It's supposed to extract nDisplay related data from a collecting JSON file." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonContainer_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::NewProp_nDisplay_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::NewProp_nDisplay = { "nDisplay", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonContainer_426, nDisplay), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::NewProp_nDisplay_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::NewProp_nDisplay_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::NewProp_nDisplay,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonContainer_426",
		sizeof(FDisplayClusterConfigurationJsonContainer_426),
		alignof(FDisplayClusterConfigurationJsonContainer_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonContainer_426"), sizeof(FDisplayClusterConfigurationJsonContainer_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonContainer_426_Hash() { return 1614430807U; }
class UScriptStruct* FDisplayClusterConfigurationJsonNdisplay_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonNdisplay_426"), sizeof(FDisplayClusterConfigurationJsonNdisplay_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonNdisplay_426>()
{
	return FDisplayClusterConfigurationJsonNdisplay_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426(FDisplayClusterConfigurationJsonNdisplay_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonNdisplay_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonNdisplay_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonNdisplay_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonNdisplay_426>(FName(TEXT("DisplayClusterConfigurationJsonNdisplay_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonNdisplay_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Version;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scene_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Scene;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cluster_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cluster;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CustomParameters_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CustomParameters_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CustomParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Diagnostics_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Diagnostics;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// \"nDisplay\" category structure\n" },
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
		{ "ToolTip", "\"nDisplay\" category structure" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonNdisplay_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Description_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonNdisplay_426, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Version_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonNdisplay_426, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Version_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_AssetPath_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonNdisplay_426, AssetPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_AssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_AssetPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Scene_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Scene = { "Scene", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonNdisplay_426, Scene), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Scene_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Scene_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Cluster_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Cluster = { "Cluster", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonNdisplay_426, Cluster), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Cluster_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Cluster_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters_ValueProp = { "CustomParameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters_Key_KeyProp = { "CustomParameters_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters = { "CustomParameters", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonNdisplay_426, CustomParameters), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Diagnostics_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Diagnostics = { "Diagnostics", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonNdisplay_426, Diagnostics), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Diagnostics_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Diagnostics_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Version,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_AssetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Scene,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Cluster,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_CustomParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::NewProp_Diagnostics,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonNdisplay_426",
		sizeof(FDisplayClusterConfigurationJsonNdisplay_426),
		alignof(FDisplayClusterConfigurationJsonNdisplay_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonNdisplay_426"), sizeof(FDisplayClusterConfigurationJsonNdisplay_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonNdisplay_426_Hash() { return 4061252609U; }
class UScriptStruct* FDisplayClusterConfigurationJsonDiagnostics_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonDiagnostics_426"), sizeof(FDisplayClusterConfigurationJsonDiagnostics_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonDiagnostics_426>()
{
	return FDisplayClusterConfigurationJsonDiagnostics_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426(FDisplayClusterConfigurationJsonDiagnostics_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonDiagnostics_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonDiagnostics_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonDiagnostics_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonDiagnostics_426>(FName(TEXT("DisplayClusterConfigurationJsonDiagnostics_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonDiagnostics_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimulateLag_MetaData[];
#endif
		static void NewProp_SimulateLag_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SimulateLag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinLagTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinLagTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxLagTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxLagTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonDiagnostics_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_SimulateLag_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_SimulateLag_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationJsonDiagnostics_426*)Obj)->SimulateLag = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_SimulateLag = { "SimulateLag", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationJsonDiagnostics_426), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_SimulateLag_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_SimulateLag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_SimulateLag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MinLagTime_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MinLagTime = { "MinLagTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonDiagnostics_426, MinLagTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MinLagTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MinLagTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MaxLagTime_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MaxLagTime = { "MaxLagTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonDiagnostics_426, MaxLagTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MaxLagTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MaxLagTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_SimulateLag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MinLagTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::NewProp_MaxLagTime,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonDiagnostics_426",
		sizeof(FDisplayClusterConfigurationJsonDiagnostics_426),
		alignof(FDisplayClusterConfigurationJsonDiagnostics_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonDiagnostics_426"), sizeof(FDisplayClusterConfigurationJsonDiagnostics_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonDiagnostics_426_Hash() { return 2792818793U; }
class UScriptStruct* FDisplayClusterConfigurationJsonCluster_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonCluster_426"), sizeof(FDisplayClusterConfigurationJsonCluster_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonCluster_426>()
{
	return FDisplayClusterConfigurationJsonCluster_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426(FDisplayClusterConfigurationJsonCluster_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonCluster_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonCluster_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonCluster_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonCluster_426>(FName(TEXT("DisplayClusterConfigurationJsonCluster_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonCluster_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MasterNode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sync_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Sync;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Network_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Network_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Network_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Network;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Nodes_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Nodes_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Nodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Nodes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonCluster_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_MasterNode_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_MasterNode = { "MasterNode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonCluster_426, MasterNode), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_MasterNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_MasterNode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Sync_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Sync = { "Sync", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonCluster_426, Sync), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Sync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Sync_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network_ValueProp = { "Network", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network_Key_KeyProp = { "Network_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network = { "Network", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonCluster_426, Network), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes_ValueProp = { "Nodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes_Key_KeyProp = { "Nodes_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes = { "Nodes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonCluster_426, Nodes), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_MasterNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Sync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Network,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::NewProp_Nodes,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonCluster_426",
		sizeof(FDisplayClusterConfigurationJsonCluster_426),
		alignof(FDisplayClusterConfigurationJsonCluster_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonCluster_426"), sizeof(FDisplayClusterConfigurationJsonCluster_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonCluster_426_Hash() { return 437241111U; }
class UScriptStruct* FDisplayClusterConfigurationJsonClusterNode_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonClusterNode_426"), sizeof(FDisplayClusterConfigurationJsonClusterNode_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonClusterNode_426>()
{
	return FDisplayClusterConfigurationJsonClusterNode_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426(FDisplayClusterConfigurationJsonClusterNode_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonClusterNode_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterNode_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterNode_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonClusterNode_426>(FName(TEXT("DisplayClusterConfigurationJsonClusterNode_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterNode_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Host_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Host;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sound_MetaData[];
#endif
		static void NewProp_Sound_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Sound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FullScreen_MetaData[];
#endif
		static void NewProp_FullScreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_FullScreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Window_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Window;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Postprocess_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Postprocess_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Postprocess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Postprocess;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Viewports_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Viewports_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Viewports_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Viewports;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonClusterNode_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Host_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Host = { "Host", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonClusterNode_426, Host), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Host_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Host_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Sound_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Sound_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationJsonClusterNode_426*)Obj)->Sound = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Sound = { "Sound", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationJsonClusterNode_426), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Sound_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Sound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Sound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_FullScreen_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_FullScreen_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationJsonClusterNode_426*)Obj)->FullScreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_FullScreen = { "FullScreen", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationJsonClusterNode_426), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_FullScreen_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_FullScreen_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_FullScreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Window_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Window = { "Window", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonClusterNode_426, Window), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Window_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Window_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess_ValueProp = { "Postprocess", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess_Key_KeyProp = { "Postprocess_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess = { "Postprocess", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonClusterNode_426, Postprocess), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports_ValueProp = { "Viewports", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports_Key_KeyProp = { "Viewports_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports = { "Viewports", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonClusterNode_426, Viewports), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Host,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Sound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_FullScreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Window,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Postprocess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::NewProp_Viewports,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonClusterNode_426",
		sizeof(FDisplayClusterConfigurationJsonClusterNode_426),
		alignof(FDisplayClusterConfigurationJsonClusterNode_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonClusterNode_426"), sizeof(FDisplayClusterConfigurationJsonClusterNode_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterNode_426_Hash() { return 2111736828U; }
class UScriptStruct* FDisplayClusterConfigurationJsonViewport_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonViewport_426"), sizeof(FDisplayClusterConfigurationJsonViewport_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonViewport_426>()
{
	return FDisplayClusterConfigurationJsonViewport_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426(FDisplayClusterConfigurationJsonViewport_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonViewport_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonViewport_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonViewport_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonViewport_426>(FName(TEXT("DisplayClusterConfigurationJsonViewport_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonViewport_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Camera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BufferRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GPUIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_GPUIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureShare_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureShare;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Region;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionPolicy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonViewport_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Camera_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonViewport_426, Camera), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Camera_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Camera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_BufferRatio_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonViewport_426, BufferRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_BufferRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_BufferRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_GPUIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_GPUIndex = { "GPUIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonViewport_426, GPUIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_GPUIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_GPUIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_TextureShare_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_TextureShare = { "TextureShare", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonViewport_426, TextureShare), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_TextureShare_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_TextureShare_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Region_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Region = { "Region", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonViewport_426, Region), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Region_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Region_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_ProjectionPolicy_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_ProjectionPolicy = { "ProjectionPolicy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonViewport_426, ProjectionPolicy), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_ProjectionPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_ProjectionPolicy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_GPUIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_TextureShare,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_Region,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::NewProp_ProjectionPolicy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonViewport_426",
		sizeof(FDisplayClusterConfigurationJsonViewport_426),
		alignof(FDisplayClusterConfigurationJsonViewport_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonViewport_426"), sizeof(FDisplayClusterConfigurationJsonViewport_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonViewport_426_Hash() { return 737561667U; }
class UScriptStruct* FDisplayClusterConfigurationJsonTextureShare_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonTextureShare_426"), sizeof(FDisplayClusterConfigurationJsonTextureShare_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonTextureShare_426>()
{
	return FDisplayClusterConfigurationJsonTextureShare_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426(FDisplayClusterConfigurationJsonTextureShare_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonTextureShare_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonTextureShare_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonTextureShare_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonTextureShare_426>(FName(TEXT("DisplayClusterConfigurationJsonTextureShare_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonTextureShare_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SyncPolicy_Connection_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SyncPolicy_Connection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SyncPolicy_Frame_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SyncPolicy_Frame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SyncPolicy_Texture_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_SyncPolicy_Texture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonTextureShare_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationJsonTextureShare_426*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationJsonTextureShare_426), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Connection_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Connection = { "SyncPolicy_Connection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonTextureShare_426, SyncPolicy_Connection), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Connection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Connection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Frame_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Frame = { "SyncPolicy_Frame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonTextureShare_426, SyncPolicy_Frame), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Frame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Frame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Texture_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Texture = { "SyncPolicy_Texture", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonTextureShare_426, SyncPolicy_Texture), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Texture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Texture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Connection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Frame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::NewProp_SyncPolicy_Texture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonTextureShare_426",
		sizeof(FDisplayClusterConfigurationJsonTextureShare_426),
		alignof(FDisplayClusterConfigurationJsonTextureShare_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonTextureShare_426"), sizeof(FDisplayClusterConfigurationJsonTextureShare_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonTextureShare_426_Hash() { return 1506521557U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationJsonProjectionPolicy_426>() == std::is_polymorphic<FDisplayClusterConfigurationJsonPolymorphicEntity_426>(), "USTRUCT FDisplayClusterConfigurationJsonProjectionPolicy_426 cannot be polymorphic unless super FDisplayClusterConfigurationJsonPolymorphicEntity_426 is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationJsonProjectionPolicy_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonProjectionPolicy_426"), sizeof(FDisplayClusterConfigurationJsonProjectionPolicy_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonProjectionPolicy_426>()
{
	return FDisplayClusterConfigurationJsonProjectionPolicy_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426(FDisplayClusterConfigurationJsonProjectionPolicy_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonProjectionPolicy_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonProjectionPolicy_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonProjectionPolicy_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonProjectionPolicy_426>(FName(TEXT("DisplayClusterConfigurationJsonProjectionPolicy_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonProjectionPolicy_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonProjectionPolicy_426>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426,
		&NewStructOps,
		"DisplayClusterConfigurationJsonProjectionPolicy_426",
		sizeof(FDisplayClusterConfigurationJsonProjectionPolicy_426),
		alignof(FDisplayClusterConfigurationJsonProjectionPolicy_426),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonProjectionPolicy_426"), sizeof(FDisplayClusterConfigurationJsonProjectionPolicy_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonProjectionPolicy_426_Hash() { return 669417447U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationJsonPostprocess_426>() == std::is_polymorphic<FDisplayClusterConfigurationJsonPolymorphicEntity_426>(), "USTRUCT FDisplayClusterConfigurationJsonPostprocess_426 cannot be polymorphic unless super FDisplayClusterConfigurationJsonPolymorphicEntity_426 is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationJsonPostprocess_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonPostprocess_426"), sizeof(FDisplayClusterConfigurationJsonPostprocess_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonPostprocess_426>()
{
	return FDisplayClusterConfigurationJsonPostprocess_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426(FDisplayClusterConfigurationJsonPostprocess_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonPostprocess_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonPostprocess_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonPostprocess_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonPostprocess_426>(FName(TEXT("DisplayClusterConfigurationJsonPostprocess_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonPostprocess_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonPostprocess_426>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426,
		&NewStructOps,
		"DisplayClusterConfigurationJsonPostprocess_426",
		sizeof(FDisplayClusterConfigurationJsonPostprocess_426),
		alignof(FDisplayClusterConfigurationJsonPostprocess_426),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonPostprocess_426"), sizeof(FDisplayClusterConfigurationJsonPostprocess_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPostprocess_426_Hash() { return 4034284776U; }
class UScriptStruct* FDisplayClusterConfigurationJsonClusterSync_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonClusterSync_426"), sizeof(FDisplayClusterConfigurationJsonClusterSync_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonClusterSync_426>()
{
	return FDisplayClusterConfigurationJsonClusterSync_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426(FDisplayClusterConfigurationJsonClusterSync_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonClusterSync_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterSync_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterSync_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonClusterSync_426>(FName(TEXT("DisplayClusterConfigurationJsonClusterSync_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterSync_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSyncPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderSyncPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputSyncPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputSyncPolicy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonClusterSync_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_RenderSyncPolicy_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_RenderSyncPolicy = { "RenderSyncPolicy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonClusterSync_426, RenderSyncPolicy), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_RenderSyncPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_RenderSyncPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_InputSyncPolicy_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_InputSyncPolicy = { "InputSyncPolicy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonClusterSync_426, InputSyncPolicy), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_InputSyncPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_InputSyncPolicy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_RenderSyncPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::NewProp_InputSyncPolicy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonClusterSync_426",
		sizeof(FDisplayClusterConfigurationJsonClusterSync_426),
		alignof(FDisplayClusterConfigurationJsonClusterSync_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonClusterSync_426"), sizeof(FDisplayClusterConfigurationJsonClusterSync_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSync_426_Hash() { return 1106318249U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationJsonClusterSyncPolicy_426>() == std::is_polymorphic<FDisplayClusterConfigurationJsonPolymorphicEntity_426>(), "USTRUCT FDisplayClusterConfigurationJsonClusterSyncPolicy_426 cannot be polymorphic unless super FDisplayClusterConfigurationJsonPolymorphicEntity_426 is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationJsonClusterSyncPolicy_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonClusterSyncPolicy_426"), sizeof(FDisplayClusterConfigurationJsonClusterSyncPolicy_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonClusterSyncPolicy_426>()
{
	return FDisplayClusterConfigurationJsonClusterSyncPolicy_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426(FDisplayClusterConfigurationJsonClusterSyncPolicy_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonClusterSyncPolicy_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterSyncPolicy_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterSyncPolicy_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonClusterSyncPolicy_426>(FName(TEXT("DisplayClusterConfigurationJsonClusterSyncPolicy_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonClusterSyncPolicy_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonClusterSyncPolicy_426>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426,
		&NewStructOps,
		"DisplayClusterConfigurationJsonClusterSyncPolicy_426",
		sizeof(FDisplayClusterConfigurationJsonClusterSyncPolicy_426),
		alignof(FDisplayClusterConfigurationJsonClusterSyncPolicy_426),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonClusterSyncPolicy_426"), sizeof(FDisplayClusterConfigurationJsonClusterSyncPolicy_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonClusterSyncPolicy_426_Hash() { return 109080703U; }
class UScriptStruct* FDisplayClusterConfigurationJsonMasterNode_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonMasterNode_426"), sizeof(FDisplayClusterConfigurationJsonMasterNode_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonMasterNode_426>()
{
	return FDisplayClusterConfigurationJsonMasterNode_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426(FDisplayClusterConfigurationJsonMasterNode_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonMasterNode_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonMasterNode_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonMasterNode_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonMasterNode_426>(FName(TEXT("DisplayClusterConfigurationJsonMasterNode_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonMasterNode_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Ports_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Ports_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ports_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Ports;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonMasterNode_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Id_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonMasterNode_426, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports_ValueProp = { "Ports", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports_Key_KeyProp = { "Ports_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports = { "Ports", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonMasterNode_426, Ports), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::NewProp_Ports,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonMasterNode_426",
		sizeof(FDisplayClusterConfigurationJsonMasterNode_426),
		alignof(FDisplayClusterConfigurationJsonMasterNode_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonMasterNode_426"), sizeof(FDisplayClusterConfigurationJsonMasterNode_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonMasterNode_426_Hash() { return 471320569U; }
class UScriptStruct* FDisplayClusterConfigurationJsonScene_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonScene_426"), sizeof(FDisplayClusterConfigurationJsonScene_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonScene_426>()
{
	return FDisplayClusterConfigurationJsonScene_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426(FDisplayClusterConfigurationJsonScene_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonScene_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonScene_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonScene_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonScene_426>(FName(TEXT("DisplayClusterConfigurationJsonScene_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonScene_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Xforms_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Xforms_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Xforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Xforms;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cameras_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Cameras_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cameras_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Cameras;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Screens_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Screens_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Screens_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Screens;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonScene_426>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms_ValueProp = { "Xforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms_Key_KeyProp = { "Xforms_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms = { "Xforms", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonScene_426, Xforms), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras_ValueProp = { "Cameras", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras_Key_KeyProp = { "Cameras_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras = { "Cameras", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonScene_426, Cameras), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens_ValueProp = { "Screens", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens_Key_KeyProp = { "Screens_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens = { "Screens", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonScene_426, Screens), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Xforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Cameras,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::NewProp_Screens,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonScene_426",
		sizeof(FDisplayClusterConfigurationJsonScene_426),
		alignof(FDisplayClusterConfigurationJsonScene_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonScene_426"), sizeof(FDisplayClusterConfigurationJsonScene_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonScene_426_Hash() { return 3145198128U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationJsonSceneComponentScreen_426>() == std::is_polymorphic<FDisplayClusterConfigurationJsonSceneComponentXform_426>(), "USTRUCT FDisplayClusterConfigurationJsonSceneComponentScreen_426 cannot be polymorphic unless super FDisplayClusterConfigurationJsonSceneComponentXform_426 is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationJsonSceneComponentScreen_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonSceneComponentScreen_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponentScreen_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonSceneComponentScreen_426>()
{
	return FDisplayClusterConfigurationJsonSceneComponentScreen_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426(FDisplayClusterConfigurationJsonSceneComponentScreen_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonSceneComponentScreen_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentScreen_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentScreen_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonSceneComponentScreen_426>(FName(TEXT("DisplayClusterConfigurationJsonSceneComponentScreen_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentScreen_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonSceneComponentScreen_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::NewProp_Size_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSceneComponentScreen_426, Size), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::NewProp_Size,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426,
		&NewStructOps,
		"DisplayClusterConfigurationJsonSceneComponentScreen_426",
		sizeof(FDisplayClusterConfigurationJsonSceneComponentScreen_426),
		alignof(FDisplayClusterConfigurationJsonSceneComponentScreen_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonSceneComponentScreen_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponentScreen_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentScreen_426_Hash() { return 3796111568U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationJsonSceneComponentCamera_426>() == std::is_polymorphic<FDisplayClusterConfigurationJsonSceneComponentXform_426>(), "USTRUCT FDisplayClusterConfigurationJsonSceneComponentCamera_426 cannot be polymorphic unless super FDisplayClusterConfigurationJsonSceneComponentXform_426 is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationJsonSceneComponentCamera_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonSceneComponentCamera_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponentCamera_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonSceneComponentCamera_426>()
{
	return FDisplayClusterConfigurationJsonSceneComponentCamera_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426(FDisplayClusterConfigurationJsonSceneComponentCamera_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonSceneComponentCamera_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentCamera_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentCamera_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonSceneComponentCamera_426>(FName(TEXT("DisplayClusterConfigurationJsonSceneComponentCamera_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentCamera_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpupillaryDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InterpupillaryDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SwapEyes_MetaData[];
#endif
		static void NewProp_SwapEyes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SwapEyes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StereoOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonSceneComponentCamera_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_InterpupillaryDistance_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_InterpupillaryDistance = { "InterpupillaryDistance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSceneComponentCamera_426, InterpupillaryDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_InterpupillaryDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_InterpupillaryDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_SwapEyes_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_SwapEyes_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationJsonSceneComponentCamera_426*)Obj)->SwapEyes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_SwapEyes = { "SwapEyes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationJsonSceneComponentCamera_426), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_SwapEyes_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_SwapEyes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_SwapEyes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_StereoOffset_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_StereoOffset = { "StereoOffset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSceneComponentCamera_426, StereoOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_StereoOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_StereoOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_InterpupillaryDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_SwapEyes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::NewProp_StereoOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426,
		&NewStructOps,
		"DisplayClusterConfigurationJsonSceneComponentCamera_426",
		sizeof(FDisplayClusterConfigurationJsonSceneComponentCamera_426),
		alignof(FDisplayClusterConfigurationJsonSceneComponentCamera_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonSceneComponentCamera_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponentCamera_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentCamera_426_Hash() { return 1373795034U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationJsonSceneComponentXform_426>() == std::is_polymorphic<FDisplayClusterConfigurationJsonSceneComponent_426>(), "USTRUCT FDisplayClusterConfigurationJsonSceneComponentXform_426 cannot be polymorphic unless super FDisplayClusterConfigurationJsonSceneComponent_426 is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationJsonSceneComponentXform_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonSceneComponentXform_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponentXform_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonSceneComponentXform_426>()
{
	return FDisplayClusterConfigurationJsonSceneComponentXform_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426(FDisplayClusterConfigurationJsonSceneComponentXform_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonSceneComponentXform_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentXform_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentXform_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonSceneComponentXform_426>(FName(TEXT("DisplayClusterConfigurationJsonSceneComponentXform_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponentXform_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonSceneComponentXform_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Parent_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSceneComponentXform_426, Parent), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Parent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Location_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSceneComponentXform_426, Location), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Rotation_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSceneComponentXform_426, Rotation), Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Rotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Parent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::NewProp_Rotation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426,
		&NewStructOps,
		"DisplayClusterConfigurationJsonSceneComponentXform_426",
		sizeof(FDisplayClusterConfigurationJsonSceneComponentXform_426),
		alignof(FDisplayClusterConfigurationJsonSceneComponentXform_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonSceneComponentXform_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponentXform_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponentXform_426_Hash() { return 1524033721U; }
class UScriptStruct* FDisplayClusterConfigurationJsonSceneComponent_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonSceneComponent_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponent_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonSceneComponent_426>()
{
	return FDisplayClusterConfigurationJsonSceneComponent_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426(FDisplayClusterConfigurationJsonSceneComponent_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonSceneComponent_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponent_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponent_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonSceneComponent_426>(FName(TEXT("DisplayClusterConfigurationJsonSceneComponent_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSceneComponent_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonSceneComponent_426>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonSceneComponent_426",
		sizeof(FDisplayClusterConfigurationJsonSceneComponent_426),
		alignof(FDisplayClusterConfigurationJsonSceneComponent_426),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonSceneComponent_426"), sizeof(FDisplayClusterConfigurationJsonSceneComponent_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSceneComponent_426_Hash() { return 805605461U; }
class UScriptStruct* FDisplayClusterConfigurationJsonPolymorphicEntity_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonPolymorphicEntity_426"), sizeof(FDisplayClusterConfigurationJsonPolymorphicEntity_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonPolymorphicEntity_426>()
{
	return FDisplayClusterConfigurationJsonPolymorphicEntity_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426(FDisplayClusterConfigurationJsonPolymorphicEntity_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonPolymorphicEntity_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonPolymorphicEntity_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonPolymorphicEntity_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonPolymorphicEntity_426>(FName(TEXT("DisplayClusterConfigurationJsonPolymorphicEntity_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonPolymorphicEntity_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonPolymorphicEntity_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonPolymorphicEntity_426, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters_ValueProp = { "Parameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters_Key_KeyProp = { "Parameters_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonPolymorphicEntity_426, Parameters), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::NewProp_Parameters,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonPolymorphicEntity_426",
		sizeof(FDisplayClusterConfigurationJsonPolymorphicEntity_426),
		alignof(FDisplayClusterConfigurationJsonPolymorphicEntity_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonPolymorphicEntity_426"), sizeof(FDisplayClusterConfigurationJsonPolymorphicEntity_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonPolymorphicEntity_426_Hash() { return 54295050U; }
class UScriptStruct* FDisplayClusterConfigurationJsonSizeFloat_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonSizeFloat_426"), sizeof(FDisplayClusterConfigurationJsonSizeFloat_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonSizeFloat_426>()
{
	return FDisplayClusterConfigurationJsonSizeFloat_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426(FDisplayClusterConfigurationJsonSizeFloat_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonSizeFloat_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSizeFloat_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSizeFloat_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonSizeFloat_426>(FName(TEXT("DisplayClusterConfigurationJsonSizeFloat_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSizeFloat_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Height;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonSizeFloat_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Width_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSizeFloat_426, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Height_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSizeFloat_426, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Height_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::NewProp_Height,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonSizeFloat_426",
		sizeof(FDisplayClusterConfigurationJsonSizeFloat_426),
		alignof(FDisplayClusterConfigurationJsonSizeFloat_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonSizeFloat_426"), sizeof(FDisplayClusterConfigurationJsonSizeFloat_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeFloat_426_Hash() { return 1345715666U; }
class UScriptStruct* FDisplayClusterConfigurationJsonSizeInt_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonSizeInt_426"), sizeof(FDisplayClusterConfigurationJsonSizeInt_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonSizeInt_426>()
{
	return FDisplayClusterConfigurationJsonSizeInt_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426(FDisplayClusterConfigurationJsonSizeInt_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonSizeInt_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSizeInt_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSizeInt_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonSizeInt_426>(FName(TEXT("DisplayClusterConfigurationJsonSizeInt_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonSizeInt_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Height;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonSizeInt_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Width_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSizeInt_426, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Height_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonSizeInt_426, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Height_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::NewProp_Height,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonSizeInt_426",
		sizeof(FDisplayClusterConfigurationJsonSizeInt_426),
		alignof(FDisplayClusterConfigurationJsonSizeInt_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonSizeInt_426"), sizeof(FDisplayClusterConfigurationJsonSizeInt_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonSizeInt_426_Hash() { return 2409501840U; }
class UScriptStruct* FDisplayClusterConfigurationJsonRotator_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonRotator_426"), sizeof(FDisplayClusterConfigurationJsonRotator_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonRotator_426>()
{
	return FDisplayClusterConfigurationJsonRotator_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426(FDisplayClusterConfigurationJsonRotator_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonRotator_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonRotator_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonRotator_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonRotator_426>(FName(TEXT("DisplayClusterConfigurationJsonRotator_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonRotator_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pitch_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Pitch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Yaw_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Yaw;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Roll_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Roll;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonRotator_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Pitch_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Pitch = { "Pitch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonRotator_426, Pitch), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Pitch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Pitch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Yaw_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Yaw = { "Yaw", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonRotator_426, Yaw), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Yaw_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Yaw_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Roll_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Roll = { "Roll", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonRotator_426, Roll), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Roll_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Roll_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Pitch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Yaw,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::NewProp_Roll,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonRotator_426",
		sizeof(FDisplayClusterConfigurationJsonRotator_426),
		alignof(FDisplayClusterConfigurationJsonRotator_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonRotator_426"), sizeof(FDisplayClusterConfigurationJsonRotator_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRotator_426_Hash() { return 1261432616U; }
class UScriptStruct* FDisplayClusterConfigurationJsonVector_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonVector_426"), sizeof(FDisplayClusterConfigurationJsonVector_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonVector_426>()
{
	return FDisplayClusterConfigurationJsonVector_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426(FDisplayClusterConfigurationJsonVector_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonVector_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonVector_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonVector_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonVector_426>(FName(TEXT("DisplayClusterConfigurationJsonVector_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonVector_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Z_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Z;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonVector_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_X_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_X = { "X", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonVector_426, X), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Y_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Y = { "Y", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonVector_426, Y), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Z_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Z = { "Z", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonVector_426, Z), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Z_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Z_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::NewProp_Z,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonVector_426",
		sizeof(FDisplayClusterConfigurationJsonVector_426),
		alignof(FDisplayClusterConfigurationJsonVector_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonVector_426"), sizeof(FDisplayClusterConfigurationJsonVector_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonVector_426_Hash() { return 2927412579U; }
class UScriptStruct* FDisplayClusterConfigurationJsonRectangle_426::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationJsonRectangle_426"), sizeof(FDisplayClusterConfigurationJsonRectangle_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationJsonRectangle_426>()
{
	return FDisplayClusterConfigurationJsonRectangle_426::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426(FDisplayClusterConfigurationJsonRectangle_426::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationJsonRectangle_426"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonRectangle_426
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonRectangle_426()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationJsonRectangle_426>(FName(TEXT("DisplayClusterConfigurationJsonRectangle_426")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationJsonRectangle_426;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_W_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_W;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_H_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_H;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationJsonRectangle_426>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_X_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_X = { "X", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonRectangle_426, X), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_Y_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_Y = { "Y", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonRectangle_426, Y), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_W_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_W = { "W", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonRectangle_426, W), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_W_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_W_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_H_MetaData[] = {
		{ "ModuleRelativePath", "Private/Formats/JSON426/DisplayClusterConfigurationJsonTypes_426.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_H = { "H", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationJsonRectangle_426, H), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_H_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_H_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_W,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::NewProp_H,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationJsonRectangle_426",
		sizeof(FDisplayClusterConfigurationJsonRectangle_426),
		alignof(FDisplayClusterConfigurationJsonRectangle_426),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationJsonRectangle_426"), sizeof(FDisplayClusterConfigurationJsonRectangle_426), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationJsonRectangle_426_Hash() { return 1183414617U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
