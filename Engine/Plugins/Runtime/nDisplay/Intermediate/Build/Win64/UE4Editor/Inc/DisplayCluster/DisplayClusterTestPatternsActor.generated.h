// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterTestPatternsActor_generated_h
#error "DisplayClusterTestPatternsActor.generated.h already included, missing '#pragma once' in DisplayClusterTestPatternsActor.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterTestPatternsActor_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADisplayClusterTestPatternsActor(); \
	friend struct Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics; \
public: \
	DECLARE_CLASS(ADisplayClusterTestPatternsActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(ADisplayClusterTestPatternsActor)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_INCLASS \
private: \
	static void StaticRegisterNativesADisplayClusterTestPatternsActor(); \
	friend struct Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics; \
public: \
	DECLARE_CLASS(ADisplayClusterTestPatternsActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(ADisplayClusterTestPatternsActor)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADisplayClusterTestPatternsActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADisplayClusterTestPatternsActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADisplayClusterTestPatternsActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADisplayClusterTestPatternsActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADisplayClusterTestPatternsActor(ADisplayClusterTestPatternsActor&&); \
	NO_API ADisplayClusterTestPatternsActor(const ADisplayClusterTestPatternsActor&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADisplayClusterTestPatternsActor(ADisplayClusterTestPatternsActor&&); \
	NO_API ADisplayClusterTestPatternsActor(const ADisplayClusterTestPatternsActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADisplayClusterTestPatternsActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADisplayClusterTestPatternsActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADisplayClusterTestPatternsActor)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PostProcessComponent() { return STRUCT_OFFSET(ADisplayClusterTestPatternsActor, PostProcessComponent); } \
	FORCEINLINE static uint32 __PPO__CalibrationPatterns() { return STRUCT_OFFSET(ADisplayClusterTestPatternsActor, CalibrationPatterns); } \
	FORCEINLINE static uint32 __PPO__ViewportPPSettings() { return STRUCT_OFFSET(ADisplayClusterTestPatternsActor, ViewportPPSettings); }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_29_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class ADisplayClusterTestPatternsActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterTestPatternsActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
