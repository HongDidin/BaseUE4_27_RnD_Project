// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfigurator/Private/DisplayClusterConfiguratorVersionUtils.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfiguratorVersionUtils() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATOR_API UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_NoRegister();
	DISPLAYCLUSTERCONFIGURATOR_API UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfigurator();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister();
// End Cross Module References
	void UDisplayClusterConfiguratorEditorData::StaticRegisterNativesUDisplayClusterConfiguratorEditorData()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_NoRegister()
	{
		return UDisplayClusterConfiguratorEditorData::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_nDisplayConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_nDisplayConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathToConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PathToConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bConvertedToBlueprint_MetaData[];
#endif
		static void NewProp_bConvertedToBlueprint_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bConvertedToBlueprint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfigurator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The original format of the DisplayCluster config data UAsset, used only for importing 4.26 assets.\n */" },
		{ "IncludePath", "DisplayClusterConfiguratorVersionUtils.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Private/DisplayClusterConfiguratorVersionUtils.h" },
		{ "ToolTip", "The original format of the DisplayCluster config data UAsset, used only for importing 4.26 assets." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_nDisplayConfig_MetaData[] = {
		{ "ModuleRelativePath", "Private/DisplayClusterConfiguratorVersionUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_nDisplayConfig = { "nDisplayConfig", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfiguratorEditorData, nDisplayConfig), Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_nDisplayConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_nDisplayConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_PathToConfig_MetaData[] = {
		{ "ModuleRelativePath", "Private/DisplayClusterConfiguratorVersionUtils.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_PathToConfig = { "PathToConfig", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfiguratorEditorData, PathToConfig), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_PathToConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_PathToConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_bConvertedToBlueprint_MetaData[] = {
		{ "Comment", "/**\n\x09 * True if the original asset is imported but could not be deleted.\n\x09 */" },
		{ "ModuleRelativePath", "Private/DisplayClusterConfiguratorVersionUtils.h" },
		{ "ToolTip", "True if the original asset is imported but could not be deleted." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_bConvertedToBlueprint_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorData*)Obj)->bConvertedToBlueprint = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_bConvertedToBlueprint = { "bConvertedToBlueprint", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorData), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_bConvertedToBlueprint_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_bConvertedToBlueprint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_bConvertedToBlueprint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_nDisplayConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_PathToConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::NewProp_bConvertedToBlueprint,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfiguratorEditorData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::ClassParams = {
		&UDisplayClusterConfiguratorEditorData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::PropPointers),
		0,
		0x000002A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfiguratorEditorData, 3803734895);
	template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<UDisplayClusterConfiguratorEditorData>()
	{
		return UDisplayClusterConfiguratorEditorData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfiguratorEditorData(Z_Construct_UClass_UDisplayClusterConfiguratorEditorData, &UDisplayClusterConfiguratorEditorData::StaticClass, TEXT("/Script/DisplayClusterConfigurator"), TEXT("UDisplayClusterConfiguratorEditorData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfiguratorEditorData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
