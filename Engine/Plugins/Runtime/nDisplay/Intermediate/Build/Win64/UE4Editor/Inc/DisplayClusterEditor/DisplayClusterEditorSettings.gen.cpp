// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterEditor/Private/Settings/DisplayClusterEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterEditorSettings() {}
// Cross Module References
	DISPLAYCLUSTEREDITOR_API UClass* Z_Construct_UClass_UDisplayClusterEditorSettings_NoRegister();
	DISPLAYCLUSTEREDITOR_API UClass* Z_Construct_UClass_UDisplayClusterEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterEditor();
// End Cross Module References
	void UDisplayClusterEditorSettings::StaticRegisterNativesUDisplayClusterEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterEditorSettings_NoRegister()
	{
		return UDisplayClusterEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements nDisplay settings\n **/" },
		{ "IncludePath", "Settings/DisplayClusterEditorSettings.h" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterEditorSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Implements nDisplay settings" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Main" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterEditorSettings.h" },
		{ "ToolTip", "When enabled, replaces the original GameEngine to DisplayClusterGameEngine" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((UDisplayClusterEditorSettings*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterEditorSettings), &Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::NewProp_bEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::NewProp_bEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::ClassParams = {
		&UDisplayClusterEditorSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterEditorSettings, 4096513871);
	template<> DISPLAYCLUSTEREDITOR_API UClass* StaticClass<UDisplayClusterEditorSettings>()
	{
		return UDisplayClusterEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterEditorSettings(Z_Construct_UClass_UDisplayClusterEditorSettings, &UDisplayClusterEditorSettings::StaticClass, TEXT("/Script/DisplayClusterEditor"), TEXT("UDisplayClusterEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
