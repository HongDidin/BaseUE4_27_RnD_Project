// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTEREDITOR_DisplayClusterEditorEngine_generated_h
#error "DisplayClusterEditorEngine.generated.h already included, missing '#pragma once' in DisplayClusterEditorEngine.h"
#endif
#define DISPLAYCLUSTEREDITOR_DisplayClusterEditorEngine_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterEditorEngine(); \
	friend struct Z_Construct_UClass_UDisplayClusterEditorEngine_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterEditorEngine, UUnrealEdEngine, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterEditor"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterEditorEngine)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterEditorEngine(); \
	friend struct Z_Construct_UClass_UDisplayClusterEditorEngine_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterEditorEngine, UUnrealEdEngine, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterEditor"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterEditorEngine)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterEditorEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterEditorEngine) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterEditorEngine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterEditorEngine); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterEditorEngine(UDisplayClusterEditorEngine&&); \
	NO_API UDisplayClusterEditorEngine(const UDisplayClusterEditorEngine&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterEditorEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterEditorEngine(UDisplayClusterEditorEngine&&); \
	NO_API UDisplayClusterEditorEngine(const UDisplayClusterEditorEngine&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterEditorEngine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterEditorEngine); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterEditorEngine)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_16_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTEREDITOR_API UClass* StaticClass<class UDisplayClusterEditorEngine>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_DisplayClusterEditorEngine_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
