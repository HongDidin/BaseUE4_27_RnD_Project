// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterSceneComponentSyncParent_generated_h
#error "DisplayClusterSceneComponentSyncParent.generated.h already included, missing '#pragma once' in DisplayClusterSceneComponentSyncParent.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterSceneComponentSyncParent_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterSceneComponentSyncParent(); \
	friend struct Z_Construct_UClass_UDisplayClusterSceneComponentSyncParent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterSceneComponentSyncParent, UDisplayClusterSceneComponentSync, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterSceneComponentSyncParent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterSceneComponentSyncParent(); \
	friend struct Z_Construct_UClass_UDisplayClusterSceneComponentSyncParent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterSceneComponentSyncParent, UDisplayClusterSceneComponentSync, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterSceneComponentSyncParent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterSceneComponentSyncParent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterSceneComponentSyncParent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterSceneComponentSyncParent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterSceneComponentSyncParent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterSceneComponentSyncParent(UDisplayClusterSceneComponentSyncParent&&); \
	NO_API UDisplayClusterSceneComponentSyncParent(const UDisplayClusterSceneComponentSyncParent&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterSceneComponentSyncParent(UDisplayClusterSceneComponentSyncParent&&); \
	NO_API UDisplayClusterSceneComponentSyncParent(const UDisplayClusterSceneComponentSyncParent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterSceneComponentSyncParent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterSceneComponentSyncParent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterSceneComponentSyncParent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_12_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterSceneComponentSyncParent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSyncParent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
