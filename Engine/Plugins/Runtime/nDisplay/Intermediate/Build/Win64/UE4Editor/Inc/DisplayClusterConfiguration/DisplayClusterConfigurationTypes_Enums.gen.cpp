// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_Enums.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_Enums() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMode();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewportOverscanMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMGPUMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationCameraMotionBlurMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderFamilyMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_LightcardRenderMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_ChromakeySource();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfiguration_PostRenderBlur();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationEyeStereoOffset();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationTrackerMapping();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationKeyboardReflectionType();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationDataSource();
// End Cross Module References
	static UEnum* EDisplayClusterConfigurationRenderMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationRenderMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationRenderMode>()
	{
		return EDisplayClusterConfigurationRenderMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationRenderMode(EDisplayClusterConfigurationRenderMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationRenderMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMode_Hash() { return 1146984219U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationRenderMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationRenderMode::Mono", (int64)EDisplayClusterConfigurationRenderMode::Mono },
				{ "EDisplayClusterConfigurationRenderMode::SideBySide", (int64)EDisplayClusterConfigurationRenderMode::SideBySide },
				{ "EDisplayClusterConfigurationRenderMode::TopBottom", (int64)EDisplayClusterConfigurationRenderMode::TopBottom },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "Mono.DisplayName", "Mono" },
				{ "Mono.Name", "EDisplayClusterConfigurationRenderMode::Mono" },
				{ "SideBySide.DisplayName", "Stereo: Side By Side" },
				{ "SideBySide.Name", "EDisplayClusterConfigurationRenderMode::SideBySide" },
				{ "TopBottom.DisplayName", "Stereo: Top Bottom" },
				{ "TopBottom.Name", "EDisplayClusterConfigurationRenderMode::TopBottom" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationRenderMode",
				"EDisplayClusterConfigurationRenderMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationViewportOverscanMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewportOverscanMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationViewportOverscanMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationViewportOverscanMode>()
	{
		return EDisplayClusterConfigurationViewportOverscanMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationViewportOverscanMode(EDisplayClusterConfigurationViewportOverscanMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationViewportOverscanMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewportOverscanMode_Hash() { return 641783875U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewportOverscanMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationViewportOverscanMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewportOverscanMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationViewportOverscanMode::None", (int64)EDisplayClusterConfigurationViewportOverscanMode::None },
				{ "EDisplayClusterConfigurationViewportOverscanMode::Pixels", (int64)EDisplayClusterConfigurationViewportOverscanMode::Pixels },
				{ "EDisplayClusterConfigurationViewportOverscanMode::Percent", (int64)EDisplayClusterConfigurationViewportOverscanMode::Percent },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "None.DisplayName", "Disabled" },
				{ "None.Name", "EDisplayClusterConfigurationViewportOverscanMode::None" },
				{ "Percent.DisplayName", "Enabled: Percent values" },
				{ "Percent.Name", "EDisplayClusterConfigurationViewportOverscanMode::Percent" },
				{ "Pixels.DisplayName", "Enabled: Pixels values" },
				{ "Pixels.Name", "EDisplayClusterConfigurationViewportOverscanMode::Pixels" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationViewportOverscanMode",
				"EDisplayClusterConfigurationViewportOverscanMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationRenderMGPUMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMGPUMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationRenderMGPUMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationRenderMGPUMode>()
	{
		return EDisplayClusterConfigurationRenderMGPUMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationRenderMGPUMode(EDisplayClusterConfigurationRenderMGPUMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationRenderMGPUMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMGPUMode_Hash() { return 1694126821U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMGPUMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationRenderMGPUMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMGPUMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationRenderMGPUMode::None", (int64)EDisplayClusterConfigurationRenderMGPUMode::None },
				{ "EDisplayClusterConfigurationRenderMGPUMode::Enabled", (int64)EDisplayClusterConfigurationRenderMGPUMode::Enabled },
				{ "EDisplayClusterConfigurationRenderMGPUMode::Optimized_EnabledLockSteps", (int64)EDisplayClusterConfigurationRenderMGPUMode::Optimized_EnabledLockSteps },
				{ "EDisplayClusterConfigurationRenderMGPUMode::Optimized_DisabledLockSteps", (int64)EDisplayClusterConfigurationRenderMGPUMode::Optimized_DisabledLockSteps },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Enabled.Comment", "// Use default UE crossGPU transfer\n" },
				{ "Enabled.DisplayName", "Enabled" },
				{ "Enabled.Name", "EDisplayClusterConfigurationRenderMGPUMode::Enabled" },
				{ "Enabled.ToolTip", "Use default UE crossGPU transfer" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "None.Comment", "// Disable multi GPU rendering\n" },
				{ "None.DisplayName", "Disabled" },
				{ "None.Name", "EDisplayClusterConfigurationRenderMGPUMode::None" },
				{ "None.ToolTip", "Disable multi GPU rendering" },
				{ "Optimized_DisabledLockSteps.Comment", "// Performance (Experimental): Use optimized transfer once per frame with bLockStepGPUs=false \n" },
				{ "Optimized_DisabledLockSteps.DisplayName", "Optimization: Disabled Lockstep" },
				{ "Optimized_DisabledLockSteps.Name", "EDisplayClusterConfigurationRenderMGPUMode::Optimized_DisabledLockSteps" },
				{ "Optimized_DisabledLockSteps.ToolTip", "Performance (Experimental): Use optimized transfer once per frame with bLockStepGPUs=false" },
				{ "Optimized_EnabledLockSteps.Comment", "// Performance (Experimental): Use optimized transfer once per frame with bLockStepGPUs=true\n" },
				{ "Optimized_EnabledLockSteps.DisplayName", "Optimization: Enabled Lockstep" },
				{ "Optimized_EnabledLockSteps.Name", "EDisplayClusterConfigurationRenderMGPUMode::Optimized_EnabledLockSteps" },
				{ "Optimized_EnabledLockSteps.ToolTip", "Performance (Experimental): Use optimized transfer once per frame with bLockStepGPUs=true" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationRenderMGPUMode",
				"EDisplayClusterConfigurationRenderMGPUMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationCameraMotionBlurMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationCameraMotionBlurMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationCameraMotionBlurMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationCameraMotionBlurMode>()
	{
		return EDisplayClusterConfigurationCameraMotionBlurMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationCameraMotionBlurMode(EDisplayClusterConfigurationCameraMotionBlurMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationCameraMotionBlurMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationCameraMotionBlurMode_Hash() { return 3608266969U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationCameraMotionBlurMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationCameraMotionBlurMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationCameraMotionBlurMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationCameraMotionBlurMode::Off", (int64)EDisplayClusterConfigurationCameraMotionBlurMode::Off },
				{ "EDisplayClusterConfigurationCameraMotionBlurMode::On", (int64)EDisplayClusterConfigurationCameraMotionBlurMode::On },
				{ "EDisplayClusterConfigurationCameraMotionBlurMode::Override", (int64)EDisplayClusterConfigurationCameraMotionBlurMode::Override },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "Off.Comment", "/** Subtract blur due to all global motion of the ICVFX camera, but preserve blur from object motion. */" },
				{ "Off.DisplayName", "All Camera Blur Off" },
				{ "Off.Name", "EDisplayClusterConfigurationCameraMotionBlurMode::Off" },
				{ "Off.ToolTip", "Subtract blur due to all global motion of the ICVFX camera, but preserve blur from object motion." },
				{ "On.Comment", "/** Allow blur from camera motion. This option should not normally be used for shooting, but may be useful for diagnostic purposes. */" },
				{ "On.DisplayName", "ICVFX Camera Blur On" },
				{ "On.Name", "EDisplayClusterConfigurationCameraMotionBlurMode::On" },
				{ "On.ToolTip", "Allow blur from camera motion. This option should not normally be used for shooting, but may be useful for diagnostic purposes." },
				{ "Override.Comment", "/** Subtract blur due to motion of the ICVFX camera relative to the nDisplay root, but preserve blur from both object motion and movement of the nDisplay root, which can be animated to represent vehicular motion through an environment. */" },
				{ "Override.DisplayName", "ICVFX Camera Blur Off" },
				{ "Override.Name", "EDisplayClusterConfigurationCameraMotionBlurMode::Override" },
				{ "Override.ToolTip", "Subtract blur due to motion of the ICVFX camera relative to the nDisplay root, but preserve blur from both object motion and movement of the nDisplay root, which can be animated to represent vehicular motion through an environment." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationCameraMotionBlurMode",
				"EDisplayClusterConfigurationCameraMotionBlurMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationRenderFamilyMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderFamilyMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationRenderFamilyMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationRenderFamilyMode>()
	{
		return EDisplayClusterConfigurationRenderFamilyMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationRenderFamilyMode(EDisplayClusterConfigurationRenderFamilyMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationRenderFamilyMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderFamilyMode_Hash() { return 3492379251U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderFamilyMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationRenderFamilyMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderFamilyMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationRenderFamilyMode::None", (int64)EDisplayClusterConfigurationRenderFamilyMode::None },
				{ "EDisplayClusterConfigurationRenderFamilyMode::AllowMergeForGroups", (int64)EDisplayClusterConfigurationRenderFamilyMode::AllowMergeForGroups },
				{ "EDisplayClusterConfigurationRenderFamilyMode::AllowMergeForGroupsAndStereo", (int64)EDisplayClusterConfigurationRenderFamilyMode::AllowMergeForGroupsAndStereo },
				{ "EDisplayClusterConfigurationRenderFamilyMode::MergeAnyPossible", (int64)EDisplayClusterConfigurationRenderFamilyMode::MergeAnyPossible },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AllowMergeForGroups.Comment", "// Merge views by ViewFamilyGroupNum\n" },
				{ "AllowMergeForGroups.DisplayName", "Groups" },
				{ "AllowMergeForGroups.Name", "EDisplayClusterConfigurationRenderFamilyMode::AllowMergeForGroups" },
				{ "AllowMergeForGroups.ToolTip", "Merge views by ViewFamilyGroupNum" },
				{ "AllowMergeForGroupsAndStereo.Comment", "// Merge views by ViewFamilyGroupNum and stereo\n" },
				{ "AllowMergeForGroupsAndStereo.DisplayName", "GroupsAndStereo" },
				{ "AllowMergeForGroupsAndStereo.Name", "EDisplayClusterConfigurationRenderFamilyMode::AllowMergeForGroupsAndStereo" },
				{ "AllowMergeForGroupsAndStereo.ToolTip", "Merge views by ViewFamilyGroupNum and stereo" },
				{ "MergeAnyPossible.Comment", "// Use rules to merge views to minimal num of families (separate by: buffer_ratio, viewExtension, max RTT size)\n" },
				{ "MergeAnyPossible.DisplayName", "AnyPossible" },
				{ "MergeAnyPossible.Name", "EDisplayClusterConfigurationRenderFamilyMode::MergeAnyPossible" },
				{ "MergeAnyPossible.ToolTip", "Use rules to merge views to minimal num of families (separate by: buffer_ratio, viewExtension, max RTT size)" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "None.Comment", "// Render all viewports to unique RenderTargets\n" },
				{ "None.DisplayName", "Disabled" },
				{ "None.Name", "EDisplayClusterConfigurationRenderFamilyMode::None" },
				{ "None.ToolTip", "Render all viewports to unique RenderTargets" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationRenderFamilyMode",
				"EDisplayClusterConfigurationRenderFamilyMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationViewport_StereoMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationViewport_StereoMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationViewport_StereoMode>()
	{
		return EDisplayClusterConfigurationViewport_StereoMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationViewport_StereoMode(EDisplayClusterConfigurationViewport_StereoMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationViewport_StereoMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode_Hash() { return 147626339U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationViewport_StereoMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationViewport_StereoMode::Default", (int64)EDisplayClusterConfigurationViewport_StereoMode::Default },
				{ "EDisplayClusterConfigurationViewport_StereoMode::ForceMono", (int64)EDisplayClusterConfigurationViewport_StereoMode::ForceMono },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Default.Comment", "// Render incamera frame over lightcard\n" },
				{ "Default.DisplayName", "Default" },
				{ "Default.Name", "EDisplayClusterConfigurationViewport_StereoMode::Default" },
				{ "Default.ToolTip", "Render incamera frame over lightcard" },
				{ "ForceMono.Comment", "// Force monoscopic render mode for this viewport (performance)\n" },
				{ "ForceMono.DisplayName", "Force Mono" },
				{ "ForceMono.Name", "EDisplayClusterConfigurationViewport_StereoMode::ForceMono" },
				{ "ForceMono.ToolTip", "Force monoscopic render mode for this viewport (performance)" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationViewport_StereoMode",
				"EDisplayClusterConfigurationViewport_StereoMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode>()
	{
		return EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode(EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode_Hash() { return 19270234U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::Default", (int64)EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::Default },
				{ "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::Disabled", (int64)EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::Disabled },
				{ "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::DisableChromakey", (int64)EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::DisableChromakey },
				{ "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::DisableChromakeyMarkers", (int64)EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::DisableChromakeyMarkers },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Default.Comment", "// Use default rendering rules\n" },
				{ "Default.DisplayName", "Default" },
				{ "Default.Name", "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::Default" },
				{ "Default.ToolTip", "Use default rendering rules" },
				{ "DisableChromakey.Comment", "// Disable chromakey render for this viewport\n" },
				{ "DisableChromakey.DisplayName", "Disabled Chromakey" },
				{ "DisableChromakey.Name", "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::DisableChromakey" },
				{ "DisableChromakey.ToolTip", "Disable chromakey render for this viewport" },
				{ "DisableChromakeyMarkers.Comment", "// Disable chromakey markers render for this viewport\n" },
				{ "DisableChromakeyMarkers.DisplayName", "Disabled Markers" },
				{ "DisableChromakeyMarkers.Name", "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::DisableChromakeyMarkers" },
				{ "DisableChromakeyMarkers.ToolTip", "Disable chromakey markers render for this viewport" },
				{ "Disabled.Comment", "// Disable camera frame render for this viewport\n" },
				{ "Disabled.DisplayName", "Disabled" },
				{ "Disabled.Name", "EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode::Disabled" },
				{ "Disabled.ToolTip", "Disable camera frame render for this viewport" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode",
				"EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode>()
	{
		return EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode(EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode_Hash() { return 1440620419U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Default", (int64)EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Default },
				{ "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Disabled", (int64)EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Disabled },
				{ "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Over", (int64)EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Over },
				{ "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Under", (int64)EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Under },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Default.Comment", "// Use global lightcard mode from StageSettings for this viewport\n" },
				{ "Default.DisplayName", "Default" },
				{ "Default.Name", "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Default" },
				{ "Default.ToolTip", "Use global lightcard mode from StageSettings for this viewport" },
				{ "Disabled.Comment", "// Disable lightcard rendering for this viewport\n" },
				{ "Disabled.DisplayName", "Disabled" },
				{ "Disabled.Name", "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Disabled" },
				{ "Disabled.ToolTip", "Disable lightcard rendering for this viewport" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "Over.Comment", "// Render incamera frame over lightcard for this viewport\n" },
				{ "Over.DisplayName", "Lightcard Over Frustum" },
				{ "Over.Name", "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Over" },
				{ "Over.ToolTip", "Render incamera frame over lightcard for this viewport" },
				{ "Under.Comment", "// Over lightcard over incamera frame  for this viewport\n" },
				{ "Under.DisplayName", "Lightcard Under Frustum" },
				{ "Under.Name", "EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode::Under" },
				{ "Under.ToolTip", "Over lightcard over incamera frame  for this viewport" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode",
				"EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationICVFX_LightcardRenderMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_LightcardRenderMode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationICVFX_LightcardRenderMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationICVFX_LightcardRenderMode>()
	{
		return EDisplayClusterConfigurationICVFX_LightcardRenderMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationICVFX_LightcardRenderMode(EDisplayClusterConfigurationICVFX_LightcardRenderMode_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationICVFX_LightcardRenderMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_LightcardRenderMode_Hash() { return 3280199600U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_LightcardRenderMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationICVFX_LightcardRenderMode"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_LightcardRenderMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationICVFX_LightcardRenderMode::Over", (int64)EDisplayClusterConfigurationICVFX_LightcardRenderMode::Over },
				{ "EDisplayClusterConfigurationICVFX_LightcardRenderMode::Under", (int64)EDisplayClusterConfigurationICVFX_LightcardRenderMode::Under },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "Over.Comment", "/** Render Light Cards over the inner frustum.  Light Cards can be directly visible in camera. */" },
				{ "Over.DisplayName", "Lightcard Over Frustum" },
				{ "Over.Name", "EDisplayClusterConfigurationICVFX_LightcardRenderMode::Over" },
				{ "Over.ToolTip", "Render Light Cards over the inner frustum.  Light Cards can be directly visible in camera." },
				{ "Under.Comment", "/** Render Light Cards under the inner frustum. Light Cards will not be directly visible in camera. */" },
				{ "Under.DisplayName", "Lightcard Under Frustum" },
				{ "Under.Name", "EDisplayClusterConfigurationICVFX_LightcardRenderMode::Under" },
				{ "Under.ToolTip", "Render Light Cards under the inner frustum. Light Cards will not be directly visible in camera." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationICVFX_LightcardRenderMode",
				"EDisplayClusterConfigurationICVFX_LightcardRenderMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationICVFX_ChromakeySource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_ChromakeySource, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationICVFX_ChromakeySource"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationICVFX_ChromakeySource>()
	{
		return EDisplayClusterConfigurationICVFX_ChromakeySource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationICVFX_ChromakeySource(EDisplayClusterConfigurationICVFX_ChromakeySource_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationICVFX_ChromakeySource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_ChromakeySource_Hash() { return 2282033485U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_ChromakeySource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationICVFX_ChromakeySource"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_ChromakeySource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationICVFX_ChromakeySource::None", (int64)EDisplayClusterConfigurationICVFX_ChromakeySource::None },
				{ "EDisplayClusterConfigurationICVFX_ChromakeySource::FrameColor", (int64)EDisplayClusterConfigurationICVFX_ChromakeySource::FrameColor },
				{ "EDisplayClusterConfigurationICVFX_ChromakeySource::ChromakeyRenderTexture", (int64)EDisplayClusterConfigurationICVFX_ChromakeySource::ChromakeyRenderTexture },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ChromakeyRenderTexture.Comment", "// Capture to texture RTT from scene layers\n" },
				{ "ChromakeyRenderTexture.DisplayName", "Chromakey Render Texture" },
				{ "ChromakeyRenderTexture.Name", "EDisplayClusterConfigurationICVFX_ChromakeySource::ChromakeyRenderTexture" },
				{ "ChromakeyRenderTexture.ToolTip", "Capture to texture RTT from scene layers" },
				{ "FrameColor.Comment", "// Fill whole camera frame with chromakey color\n" },
				{ "FrameColor.DisplayName", "Chromakey Color" },
				{ "FrameColor.Name", "EDisplayClusterConfigurationICVFX_ChromakeySource::FrameColor" },
				{ "FrameColor.ToolTip", "Fill whole camera frame with chromakey color" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "None.Comment", "// Disable chromakey rendering\n" },
				{ "None.DisplayName", "None" },
				{ "None.Name", "EDisplayClusterConfigurationICVFX_ChromakeySource::None" },
				{ "None.ToolTip", "Disable chromakey rendering" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationICVFX_ChromakeySource",
				"EDisplayClusterConfigurationICVFX_ChromakeySource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfiguration_PostRenderBlur_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfiguration_PostRenderBlur, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfiguration_PostRenderBlur"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfiguration_PostRenderBlur>()
	{
		return EDisplayClusterConfiguration_PostRenderBlur_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfiguration_PostRenderBlur(EDisplayClusterConfiguration_PostRenderBlur_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfiguration_PostRenderBlur"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfiguration_PostRenderBlur_Hash() { return 1484797168U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfiguration_PostRenderBlur()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfiguration_PostRenderBlur"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfiguration_PostRenderBlur_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfiguration_PostRenderBlur::None", (int64)EDisplayClusterConfiguration_PostRenderBlur::None },
				{ "EDisplayClusterConfiguration_PostRenderBlur::Gaussian", (int64)EDisplayClusterConfiguration_PostRenderBlur::Gaussian },
				{ "EDisplayClusterConfiguration_PostRenderBlur::Dilate", (int64)EDisplayClusterConfiguration_PostRenderBlur::Dilate },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Dilate.Comment", "// Blur viewport using Dilate method\n" },
				{ "Dilate.DisplayName", "Dilate" },
				{ "Dilate.Name", "EDisplayClusterConfiguration_PostRenderBlur::Dilate" },
				{ "Dilate.ToolTip", "Blur viewport using Dilate method" },
				{ "Gaussian.Comment", "// Blur viewport using Gaussian method\n" },
				{ "Gaussian.DisplayName", "Gaussian" },
				{ "Gaussian.Name", "EDisplayClusterConfiguration_PostRenderBlur::Gaussian" },
				{ "Gaussian.ToolTip", "Blur viewport using Gaussian method" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "None.Comment", "// Not use blur postprocess\n" },
				{ "None.DisplayName", "None" },
				{ "None.Name", "EDisplayClusterConfiguration_PostRenderBlur::None" },
				{ "None.ToolTip", "Not use blur postprocess" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfiguration_PostRenderBlur",
				"EDisplayClusterConfiguration_PostRenderBlur",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationEyeStereoOffset_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationEyeStereoOffset, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationEyeStereoOffset"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationEyeStereoOffset>()
	{
		return EDisplayClusterConfigurationEyeStereoOffset_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationEyeStereoOffset(EDisplayClusterConfigurationEyeStereoOffset_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationEyeStereoOffset"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationEyeStereoOffset_Hash() { return 497836128U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationEyeStereoOffset()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationEyeStereoOffset"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationEyeStereoOffset_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationEyeStereoOffset::None", (int64)EDisplayClusterConfigurationEyeStereoOffset::None },
				{ "EDisplayClusterConfigurationEyeStereoOffset::Left", (int64)EDisplayClusterConfigurationEyeStereoOffset::Left },
				{ "EDisplayClusterConfigurationEyeStereoOffset::Right", (int64)EDisplayClusterConfigurationEyeStereoOffset::Right },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Left.DisplayName", "Left eye of a stereo pair" },
				{ "Left.Name", "EDisplayClusterConfigurationEyeStereoOffset::Left" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "None.DisplayName", "No offset" },
				{ "None.Name", "EDisplayClusterConfigurationEyeStereoOffset::None" },
				{ "Right.DisplayName", "Right eye of a stereo pair" },
				{ "Right.Name", "EDisplayClusterConfigurationEyeStereoOffset::Right" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationEyeStereoOffset",
				"EDisplayClusterConfigurationEyeStereoOffset",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationTrackerMapping_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationTrackerMapping, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationTrackerMapping"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationTrackerMapping>()
	{
		return EDisplayClusterConfigurationTrackerMapping_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationTrackerMapping(EDisplayClusterConfigurationTrackerMapping_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationTrackerMapping"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationTrackerMapping_Hash() { return 2974038870U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationTrackerMapping()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationTrackerMapping"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationTrackerMapping_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationTrackerMapping::X", (int64)EDisplayClusterConfigurationTrackerMapping::X },
				{ "EDisplayClusterConfigurationTrackerMapping::NX", (int64)EDisplayClusterConfigurationTrackerMapping::NX },
				{ "EDisplayClusterConfigurationTrackerMapping::Y", (int64)EDisplayClusterConfigurationTrackerMapping::Y },
				{ "EDisplayClusterConfigurationTrackerMapping::NY", (int64)EDisplayClusterConfigurationTrackerMapping::NY },
				{ "EDisplayClusterConfigurationTrackerMapping::Z", (int64)EDisplayClusterConfigurationTrackerMapping::Z },
				{ "EDisplayClusterConfigurationTrackerMapping::NZ", (int64)EDisplayClusterConfigurationTrackerMapping::NZ },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "NX.DisplayName", "Negative X" },
				{ "NX.Name", "EDisplayClusterConfigurationTrackerMapping::NX" },
				{ "NY.DisplayName", "Negative Y" },
				{ "NY.Name", "EDisplayClusterConfigurationTrackerMapping::NY" },
				{ "NZ.DisplayName", "Negative Z" },
				{ "NZ.Name", "EDisplayClusterConfigurationTrackerMapping::NZ" },
				{ "X.DisplayName", "Positive X" },
				{ "X.Name", "EDisplayClusterConfigurationTrackerMapping::X" },
				{ "Y.DisplayName", "Positive Y" },
				{ "Y.Name", "EDisplayClusterConfigurationTrackerMapping::Y" },
				{ "Z.DisplayName", "Positive Z" },
				{ "Z.Name", "EDisplayClusterConfigurationTrackerMapping::Z" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationTrackerMapping",
				"EDisplayClusterConfigurationTrackerMapping",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationKeyboardReflectionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationKeyboardReflectionType, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationKeyboardReflectionType"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationKeyboardReflectionType>()
	{
		return EDisplayClusterConfigurationKeyboardReflectionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationKeyboardReflectionType(EDisplayClusterConfigurationKeyboardReflectionType_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationKeyboardReflectionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationKeyboardReflectionType_Hash() { return 307998743U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationKeyboardReflectionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationKeyboardReflectionType"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationKeyboardReflectionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationKeyboardReflectionType::None", (int64)EDisplayClusterConfigurationKeyboardReflectionType::None },
				{ "EDisplayClusterConfigurationKeyboardReflectionType::nDisplay", (int64)EDisplayClusterConfigurationKeyboardReflectionType::nDisplay },
				{ "EDisplayClusterConfigurationKeyboardReflectionType::Core", (int64)EDisplayClusterConfigurationKeyboardReflectionType::Core },
				{ "EDisplayClusterConfigurationKeyboardReflectionType::All", (int64)EDisplayClusterConfigurationKeyboardReflectionType::All },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "All.DisplayName", "Both nDisplay and UE core events" },
				{ "All.Name", "EDisplayClusterConfigurationKeyboardReflectionType::All" },
				{ "Core.DisplayName", "UE core keyboard events only" },
				{ "Core.Name", "EDisplayClusterConfigurationKeyboardReflectionType::Core" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "nDisplay.DisplayName", "nDisplay buttons only" },
				{ "nDisplay.Name", "EDisplayClusterConfigurationKeyboardReflectionType::nDisplay" },
				{ "None.DisplayName", "No reflection" },
				{ "None.Name", "EDisplayClusterConfigurationKeyboardReflectionType::None" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationKeyboardReflectionType",
				"EDisplayClusterConfigurationKeyboardReflectionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterConfigurationDataSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationDataSource, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationDataSource"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationDataSource>()
	{
		return EDisplayClusterConfigurationDataSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationDataSource(EDisplayClusterConfigurationDataSource_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationDataSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationDataSource_Hash() { return 3096317292U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationDataSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationDataSource"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationDataSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationDataSource::Text", (int64)EDisplayClusterConfigurationDataSource::Text },
				{ "EDisplayClusterConfigurationDataSource::Json", (int64)EDisplayClusterConfigurationDataSource::Json },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Json.DisplayName", "JSON file" },
				{ "Json.Name", "EDisplayClusterConfigurationDataSource::Json" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Enums.h" },
				{ "Text.DisplayName", "Text file" },
				{ "Text.Name", "EDisplayClusterConfigurationDataSource::Text" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationDataSource",
				"EDisplayClusterConfigurationDataSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
