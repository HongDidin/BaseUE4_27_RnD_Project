// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorEditorSubsystem_generated_h
#error "DisplayClusterConfiguratorEditorSubsystem.generated.h already included, missing '#pragma once' in DisplayClusterConfiguratorEditorSubsystem.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorEditorSubsystem_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorEditorSubsystem(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorSubsystem_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorEditorSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorEditorSubsystem)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorEditorSubsystem(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorSubsystem_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorEditorSubsystem, UEditorSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorEditorSubsystem)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorEditorSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorEditorSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorEditorSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorEditorSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorEditorSubsystem(UDisplayClusterConfiguratorEditorSubsystem&&); \
	NO_API UDisplayClusterConfiguratorEditorSubsystem(const UDisplayClusterConfiguratorEditorSubsystem&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorEditorSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorEditorSubsystem(UDisplayClusterConfiguratorEditorSubsystem&&); \
	NO_API UDisplayClusterConfiguratorEditorSubsystem(const UDisplayClusterConfiguratorEditorSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorEditorSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorEditorSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorEditorSubsystem)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_13_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorEditorSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorEditorSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
