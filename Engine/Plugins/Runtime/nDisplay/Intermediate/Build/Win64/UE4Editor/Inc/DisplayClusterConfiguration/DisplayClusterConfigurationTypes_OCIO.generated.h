// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationTypes_OCIO_generated_h
#error "DisplayClusterConfigurationTypes_OCIO.generated.h already included, missing '#pragma once' in DisplayClusterConfigurationTypes_OCIO.h"
#endif
#define DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationTypes_OCIO_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_OCIO_h_36_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationOCIOProfile>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_OCIO_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationOCIOConfiguration>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_OCIO_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
