// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_PostRender.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_PostRender() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_TextureFilter();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_TextureAddress();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfiguration_PostRenderBlur();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle();
// End Cross Module References
class UScriptStruct* FDisplayClusterConfigurationPostRender_GenerateMips::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationPostRender_GenerateMips"), sizeof(FDisplayClusterConfigurationPostRender_GenerateMips), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationPostRender_GenerateMips>()
{
	return FDisplayClusterConfigurationPostRender_GenerateMips::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips(FDisplayClusterConfigurationPostRender_GenerateMips::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationPostRender_GenerateMips"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_GenerateMips
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_GenerateMips()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationPostRender_GenerateMips>(FName(TEXT("DisplayClusterConfigurationPostRender_GenerateMips")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_GenerateMips;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoGenerateMips_MetaData[];
#endif
		static void NewProp_bAutoGenerateMips_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoGenerateMips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MipsSamplerFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MipsSamplerFilter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MipsAddressU_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MipsAddressU;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MipsAddressV_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MipsAddressV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabledMaxNumMips_MetaData[];
#endif
		static void NewProp_bEnabledMaxNumMips_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabledMaxNumMips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxNumMips_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaxNumMips;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationPostRender_GenerateMips>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bAutoGenerateMips_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Generate and use mipmaps for the inner frustum.  Disabling this can improve performance but result in visual artifacts on the inner frustum. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Generate and use mipmaps for the inner frustum.  Disabling this can improve performance but result in visual artifacts on the inner frustum." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bAutoGenerateMips_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationPostRender_GenerateMips*)Obj)->bAutoGenerateMips = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bAutoGenerateMips = { "bAutoGenerateMips", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationPostRender_GenerateMips), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bAutoGenerateMips_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bAutoGenerateMips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bAutoGenerateMips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsSamplerFilter_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Mips Sampler Filter */" },
		{ "EditCondition", "bAutoGenerateMips" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Mips Sampler Filter" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsSamplerFilter = { "MipsSamplerFilter", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_GenerateMips, MipsSamplerFilter), Z_Construct_UEnum_Engine_TextureFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsSamplerFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsSamplerFilter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressU_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** AutoGenerateMips sampler address mode for U channel. Defaults to clamp. */" },
		{ "EditCondition", "bAutoGenerateMips" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "AutoGenerateMips sampler address mode for U channel. Defaults to clamp." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressU = { "MipsAddressU", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_GenerateMips, MipsAddressU), Z_Construct_UEnum_Engine_TextureAddress, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressU_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressU_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressV_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** AutoGenerateMips sampler address mode for V channel. Defaults to clamp. */" },
		{ "EditCondition", "bAutoGenerateMips" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "AutoGenerateMips sampler address mode for V channel. Defaults to clamp." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressV = { "MipsAddressV", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_GenerateMips, MipsAddressV), Z_Construct_UEnum_Engine_TextureAddress, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bEnabledMaxNumMips_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Performance: Allows a limited number of MIPs for high resolution. */" },
		{ "DisplayName", "Enable Maximum Number of Mips" },
		{ "EditCondition", "bAutoGenerateMips" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Performance: Allows a limited number of MIPs for high resolution." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bEnabledMaxNumMips_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationPostRender_GenerateMips*)Obj)->bEnabledMaxNumMips = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bEnabledMaxNumMips = { "bEnabledMaxNumMips", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationPostRender_GenerateMips), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bEnabledMaxNumMips_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bEnabledMaxNumMips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bEnabledMaxNumMips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MaxNumMips_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Performance: Use this value as the maximum number of MIPs for high resolution.  */" },
		{ "DisplayName", "Maximum Number of Mips" },
		{ "EditCondition", "bAutoGenerateMips && bEnabledMaxNumMips" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Performance: Use this value as the maximum number of MIPs for high resolution." },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MaxNumMips = { "MaxNumMips", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_GenerateMips, MaxNumMips), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MaxNumMips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MaxNumMips_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bAutoGenerateMips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsSamplerFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressU,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MipsAddressV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_bEnabledMaxNumMips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::NewProp_MaxNumMips,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationPostRender_GenerateMips",
		sizeof(FDisplayClusterConfigurationPostRender_GenerateMips),
		alignof(FDisplayClusterConfigurationPostRender_GenerateMips),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationPostRender_GenerateMips"), sizeof(FDisplayClusterConfigurationPostRender_GenerateMips), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips_Hash() { return 462290702U; }
class UScriptStruct* FDisplayClusterConfigurationPostRender_BlurPostprocess::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationPostRender_BlurPostprocess"), sizeof(FDisplayClusterConfigurationPostRender_BlurPostprocess), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationPostRender_BlurPostprocess>()
{
	return FDisplayClusterConfigurationPostRender_BlurPostprocess::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess(FDisplayClusterConfigurationPostRender_BlurPostprocess::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationPostRender_BlurPostprocess"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_BlurPostprocess
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_BlurPostprocess()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationPostRender_BlurPostprocess>(FName(TEXT("DisplayClusterConfigurationPostRender_BlurPostprocess")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_BlurPostprocess;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Mode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KernelRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_KernelRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KernelScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_KernelScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationPostRender_BlurPostprocess>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_Mode_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Enable/disable Post Process Blur and specify method. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Enable/disable Post Process Blur and specify method." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_BlurPostprocess, Mode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfiguration_PostRenderBlur, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_Mode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_Mode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelRadius_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Kernel Radius */" },
		{ "EditCondition", "Mode != EDisplayClusterConfiguration_PostRenderBlur::None" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Kernel Radius" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelRadius = { "KernelRadius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_BlurPostprocess, KernelRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelScale_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Kernel Scale */" },
		{ "EditCondition", "Mode != EDisplayClusterConfiguration_PostRenderBlur::None" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Kernel Scale" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelScale = { "KernelScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_BlurPostprocess, KernelScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_Mode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_Mode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::NewProp_KernelScale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationPostRender_BlurPostprocess",
		sizeof(FDisplayClusterConfigurationPostRender_BlurPostprocess),
		alignof(FDisplayClusterConfigurationPostRender_BlurPostprocess),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationPostRender_BlurPostprocess"), sizeof(FDisplayClusterConfigurationPostRender_BlurPostprocess), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess_Hash() { return 1494380744U; }
class UScriptStruct* FDisplayClusterConfigurationPostRender_Override::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationPostRender_Override"), sizeof(FDisplayClusterConfigurationPostRender_Override), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationPostRender_Override>()
{
	return FDisplayClusterConfigurationPostRender_Override::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override(FDisplayClusterConfigurationPostRender_Override::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationPostRender_Override"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_Override
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_Override()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationPostRender_Override>(FName(TEXT("DisplayClusterConfigurationPostRender_Override")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostRender_Override;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowReplace_MetaData[];
#endif
		static void NewProp_bAllowReplace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowReplace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldUseTextureRegion_MetaData[];
#endif
		static void NewProp_bShouldUseTextureRegion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldUseTextureRegion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureRegion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureRegion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationPostRender_Override>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bAllowReplace_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Disable default render, and resolve SourceTexture to viewport */" },
		{ "DisplayName", "Enable Viewport Texture Replacement" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Disable default render, and resolve SourceTexture to viewport" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bAllowReplace_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationPostRender_Override*)Obj)->bAllowReplace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bAllowReplace = { "bAllowReplace", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationPostRender_Override), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bAllowReplace_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bAllowReplace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bAllowReplace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_SourceTexture_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Texture to use in place of the inner frustum. */" },
		{ "EditCondition", "bAllowReplace" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Texture to use in place of the inner frustum." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_SourceTexture = { "SourceTexture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_Override, SourceTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_SourceTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_SourceTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bShouldUseTextureRegion_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Set to True to crop the texture for the inner frustum as specified below. */" },
		{ "DisplayName", "Use Texture Crop" },
		{ "EditCondition", "bAllowReplace" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Set to True to crop the texture for the inner frustum as specified below." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bShouldUseTextureRegion_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationPostRender_Override*)Obj)->bShouldUseTextureRegion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bShouldUseTextureRegion = { "bShouldUseTextureRegion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationPostRender_Override), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bShouldUseTextureRegion_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bShouldUseTextureRegion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bShouldUseTextureRegion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_TextureRegion_MetaData[] = {
		{ "Category", "NDisplay Render" },
		{ "Comment", "/** Texture Crop */" },
		{ "DisplayName", "Texture Crop" },
		{ "EditCondition", "bAllowReplace && bShouldUseTextureRegion" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_PostRender.h" },
		{ "ToolTip", "Texture Crop" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_TextureRegion = { "TextureRegion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostRender_Override, TextureRegion), Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_TextureRegion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_TextureRegion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bAllowReplace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_SourceTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_bShouldUseTextureRegion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::NewProp_TextureRegion,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationPostRender_Override",
		sizeof(FDisplayClusterConfigurationPostRender_Override),
		alignof(FDisplayClusterConfigurationPostRender_Override),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationPostRender_Override"), sizeof(FDisplayClusterConfigurationPostRender_Override), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override_Hash() { return 3714981035U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
