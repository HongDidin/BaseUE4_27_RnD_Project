// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_Base.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_Base() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FTextureCropOrigin();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FTextureCropSize();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_Base_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_Base();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FDisplayClusterConfigurationPostprocess>() == std::is_polymorphic<FDisplayClusterConfigurationPolymorphicEntity>(), "USTRUCT FDisplayClusterConfigurationPostprocess cannot be polymorphic unless super FDisplayClusterConfigurationPolymorphicEntity is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationPostprocess::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationPostprocess"), sizeof(FDisplayClusterConfigurationPostprocess), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationPostprocess>()
{
	return FDisplayClusterConfigurationPostprocess::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationPostprocess(FDisplayClusterConfigurationPostprocess::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationPostprocess"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostprocess
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostprocess()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationPostprocess>(FName(TEXT("DisplayClusterConfigurationPostprocess")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPostprocess;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Order_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Order;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationPostprocess>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::NewProp_Order_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Control postprocess rendering order. Bigger value rendered last\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Control postprocess rendering order. Bigger value rendered last" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::NewProp_Order = { "Order", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPostprocess, Order), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::NewProp_Order_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::NewProp_Order_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::NewProp_Order,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity,
		&NewStructOps,
		"DisplayClusterConfigurationPostprocess",
		sizeof(FDisplayClusterConfigurationPostprocess),
		alignof(FDisplayClusterConfigurationPostprocess),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationPostprocess"), sizeof(FDisplayClusterConfigurationPostprocess), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess_Hash() { return 418518585U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationProjection>() == std::is_polymorphic<FDisplayClusterConfigurationPolymorphicEntity>(), "USTRUCT FDisplayClusterConfigurationProjection cannot be polymorphic unless super FDisplayClusterConfigurationPolymorphicEntity is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationProjection::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationProjection"), sizeof(FDisplayClusterConfigurationProjection), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationProjection>()
{
	return FDisplayClusterConfigurationProjection::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationProjection(FDisplayClusterConfigurationProjection::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationProjection"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationProjection
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationProjection()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationProjection>(FName(TEXT("DisplayClusterConfigurationProjection")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationProjection;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationProjection>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity,
		&NewStructOps,
		"DisplayClusterConfigurationProjection",
		sizeof(FDisplayClusterConfigurationProjection),
		alignof(FDisplayClusterConfigurationProjection),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationProjection"), sizeof(FDisplayClusterConfigurationProjection), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection_Hash() { return 725329584U; }
class UScriptStruct* FDisplayClusterConfigurationPolymorphicEntity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationPolymorphicEntity"), sizeof(FDisplayClusterConfigurationPolymorphicEntity), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationPolymorphicEntity>()
{
	return FDisplayClusterConfigurationPolymorphicEntity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity(FDisplayClusterConfigurationPolymorphicEntity::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationPolymorphicEntity"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPolymorphicEntity
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPolymorphicEntity()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationPolymorphicEntity>(FName(TEXT("DisplayClusterConfigurationPolymorphicEntity")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationPolymorphicEntity;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Parameters;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsCustom_MetaData[];
#endif
		static void NewProp_bIsCustom_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsCustom;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationPolymorphicEntity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Polymorphic entity type\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Polymorphic entity type" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPolymorphicEntity, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters_ValueProp = { "Parameters", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters_Key_KeyProp = { "Parameters_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Generic parameters map\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Generic parameters map" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationPolymorphicEntity, Parameters), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_bIsCustom_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/**\n\x09 * When a custom policy is selected from the details panel.\n\x09 * This is needed in the event a custom policy is selected\n\x09 * but the custom type is a default policy. This allows users\n\x09 * to further customize default policies if necessary.\n\x09 *\n\x09 * EditAnywhere is required so we can manipulate the property\n\x09 * through a handle. Details will hide it from showing.\n\x09 */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "When a custom policy is selected from the details panel.\nThis is needed in the event a custom policy is selected\nbut the custom type is a default policy. This allows users\nto further customize default policies if necessary.\n\nEditAnywhere is required so we can manipulate the property\nthrough a handle. Details will hide it from showing." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_bIsCustom_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationPolymorphicEntity*)Obj)->bIsCustom = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_bIsCustom = { "bIsCustom", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationPolymorphicEntity), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_bIsCustom_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_bIsCustom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_bIsCustom_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_Parameters,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::NewProp_bIsCustom,
#endif // WITH_EDITORONLY_DATA
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationPolymorphicEntity",
		sizeof(FDisplayClusterConfigurationPolymorphicEntity),
		alignof(FDisplayClusterConfigurationPolymorphicEntity),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationPolymorphicEntity"), sizeof(FDisplayClusterConfigurationPolymorphicEntity), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity_Hash() { return 1896559202U; }
class UScriptStruct* FDisplayClusterConfigurationRectangle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationRectangle"), sizeof(FDisplayClusterConfigurationRectangle), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationRectangle>()
{
	return FDisplayClusterConfigurationRectangle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationRectangle(FDisplayClusterConfigurationRectangle::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationRectangle"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRectangle
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRectangle()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationRectangle>(FName(TEXT("DisplayClusterConfigurationRectangle")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRectangle;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Y;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_W_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_W;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_H_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_H;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * All configuration UObjects should inherit from this class.\n */" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "All configuration UObjects should inherit from this class." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationRectangle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_X_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_X = { "X", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRectangle, X), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_Y_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_Y = { "Y", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRectangle, Y), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_Y_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_W_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_W = { "W", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRectangle, W), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_W_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_W_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_H_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_H = { "H", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRectangle, H), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_H_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_H_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_Y,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_W,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::NewProp_H,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationRectangle",
		sizeof(FDisplayClusterConfigurationRectangle),
		alignof(FDisplayClusterConfigurationRectangle),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationRectangle"), sizeof(FDisplayClusterConfigurationRectangle), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle_Hash() { return 2048198467U; }
class UScriptStruct* FDisplayClusterReplaceTextureCropRectangle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterReplaceTextureCropRectangle"), sizeof(FDisplayClusterReplaceTextureCropRectangle), Get_Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterReplaceTextureCropRectangle>()
{
	return FDisplayClusterReplaceTextureCropRectangle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle(FDisplayClusterReplaceTextureCropRectangle::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterReplaceTextureCropRectangle"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterReplaceTextureCropRectangle
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterReplaceTextureCropRectangle()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterReplaceTextureCropRectangle>(FName(TEXT("DisplayClusterReplaceTextureCropRectangle")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterReplaceTextureCropRectangle;
	struct Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Origin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Texture Replace Crop parameters container\n */" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Texture Replace Crop parameters container" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterReplaceTextureCropRectangle>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Origin_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Texture Crop Origin */" },
		{ "DisplayName", "Texture Crop Origin" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Texture Crop Origin" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Origin = { "Origin", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterReplaceTextureCropRectangle, Origin), Z_Construct_UScriptStruct_FTextureCropOrigin, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Origin_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Origin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Size_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Texture Crop Size */" },
		{ "DisplayName", "Texture Crop Size" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Texture Crop Size" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterReplaceTextureCropRectangle, Size), Z_Construct_UScriptStruct_FTextureCropSize, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Origin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::NewProp_Size,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterReplaceTextureCropRectangle",
		sizeof(FDisplayClusterReplaceTextureCropRectangle),
		alignof(FDisplayClusterReplaceTextureCropRectangle),
		Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterReplaceTextureCropRectangle"), sizeof(FDisplayClusterReplaceTextureCropRectangle), Get_Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterReplaceTextureCropRectangle_Hash() { return 3685084067U; }
class UScriptStruct* FTextureCropSize::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FTextureCropSize_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureCropSize, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("TextureCropSize"), sizeof(FTextureCropSize), Get_Z_Construct_UScriptStruct_FTextureCropSize_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FTextureCropSize>()
{
	return FTextureCropSize::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureCropSize(FTextureCropSize::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("TextureCropSize"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureCropSize
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureCropSize()
	{
		UScriptStruct::DeferCppStructOps<FTextureCropSize>(FName(TEXT("TextureCropSize")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureCropSize;
	struct Z_Construct_UScriptStruct_FTextureCropSize_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_W_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_W;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_H_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_H;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCropSize_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Texture Replace Crop Size parameter container\n */" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Texture Replace Crop Size parameter container" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureCropSize>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_W_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Replace texture crop width, in pixels\n" },
		{ "DisplayName", "W" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Replace texture crop width, in pixels" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_W = { "W", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureCropSize, W), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_W_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_W_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_H_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Replace texture crop height, in pixels\n" },
		{ "DisplayName", "H" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Replace texture crop height, in pixels" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_H = { "H", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureCropSize, H), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_H_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_H_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureCropSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_W,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureCropSize_Statics::NewProp_H,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureCropSize_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"TextureCropSize",
		sizeof(FTextureCropSize),
		alignof(FTextureCropSize),
		Z_Construct_UScriptStruct_FTextureCropSize_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropSize_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCropSize_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropSize_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureCropSize()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureCropSize_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureCropSize"), sizeof(FTextureCropSize), Get_Z_Construct_UScriptStruct_FTextureCropSize_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureCropSize_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureCropSize_Hash() { return 3993617790U; }
class UScriptStruct* FTextureCropOrigin::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FTextureCropOrigin_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureCropOrigin, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("TextureCropOrigin"), sizeof(FTextureCropOrigin), Get_Z_Construct_UScriptStruct_FTextureCropOrigin_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FTextureCropOrigin>()
{
	return FTextureCropOrigin::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureCropOrigin(FTextureCropOrigin::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("TextureCropOrigin"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureCropOrigin
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureCropOrigin()
	{
		UScriptStruct::DeferCppStructOps<FTextureCropOrigin>(FName(TEXT("TextureCropOrigin")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureCropOrigin;
	struct Z_Construct_UScriptStruct_FTextureCropOrigin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_X_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_X;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Y_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Y;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Texture Replace Crop Origin parameter container\n */" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Texture Replace Crop Origin parameter container" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureCropOrigin>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_X_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Replace texture origin X location, in pixels\n" },
		{ "DisplayName", "X" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Replace texture origin X location, in pixels" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_X = { "X", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureCropOrigin, X), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_X_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_X_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_Y_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Replace texture origin Y position, in pixels\n" },
		{ "DisplayName", "Y" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "Replace texture origin Y position, in pixels" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_Y = { "Y", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureCropOrigin, Y), METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_Y_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_Y_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_X,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::NewProp_Y,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"TextureCropOrigin",
		sizeof(FTextureCropOrigin),
		alignof(FTextureCropOrigin),
		Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureCropOrigin()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureCropOrigin_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureCropOrigin"), sizeof(FTextureCropOrigin), Get_Z_Construct_UScriptStruct_FTextureCropOrigin_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureCropOrigin_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureCropOrigin_Hash() { return 1988074398U; }
	void UDisplayClusterConfigurationData_Base::StaticRegisterNativesUDisplayClusterConfigurationData_Base()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_Base_NoRegister()
	{
		return UDisplayClusterConfigurationData_Base::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExportedObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportedObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExportedObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * All configuration UObjects should inherit from this class.\n */" },
		{ "IncludePath", "DisplayClusterConfigurationTypes_Base.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
		{ "ToolTip", "All configuration UObjects should inherit from this class." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::NewProp_ExportedObjects_Inner = { "ExportedObjects", nullptr, (EPropertyFlags)0x0000000000000008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::NewProp_ExportedObjects_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Base.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::NewProp_ExportedObjects = { "ExportedObjects", nullptr, (EPropertyFlags)0x0040000000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData_Base, ExportedObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::NewProp_ExportedObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::NewProp_ExportedObjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::NewProp_ExportedObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::NewProp_ExportedObjects,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationData_Base>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::ClassParams = {
		&UDisplayClusterConfigurationData_Base::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_Base()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationData_Base_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationData_Base, 3731885966);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationData_Base>()
	{
		return UDisplayClusterConfigurationData_Base::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationData_Base(Z_Construct_UClass_UDisplayClusterConfigurationData_Base, &UDisplayClusterConfigurationData_Base::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationData_Base"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationData_Base);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDisplayClusterConfigurationData_Base)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
