// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationVersionCheckerTypes_generated_h
#error "DisplayClusterConfigurationVersionCheckerTypes.generated.h already included, missing '#pragma once' in DisplayClusterConfigurationVersionCheckerTypes.h"
#endif
#define DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationVersionCheckerTypes_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Private_VersionChecker_DisplayClusterConfigurationVersionCheckerTypes_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationVersionContainer_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationVersionContainer>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Private_VersionChecker_DisplayClusterConfigurationVersionCheckerTypes_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationVersion_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationVersion>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Private_VersionChecker_DisplayClusterConfigurationVersionCheckerTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
