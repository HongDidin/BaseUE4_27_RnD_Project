// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Misc/DisplayClusterObjectRef.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterObjectRef() {}
// Cross Module References
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterComponentRef();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
// End Cross Module References
class UScriptStruct* FDisplayClusterComponentRef::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTER_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterComponentRef, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("DisplayClusterComponentRef"), sizeof(FDisplayClusterComponentRef), Get_Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<FDisplayClusterComponentRef>()
{
	return FDisplayClusterComponentRef::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterComponentRef(FDisplayClusterComponentRef::StaticStruct, TEXT("/Script/DisplayCluster"), TEXT("DisplayClusterComponentRef"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterComponentRef
{
	FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterComponentRef()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterComponentRef>(FName(TEXT("DisplayClusterComponentRef")));
	}
} ScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterComponentRef;
	struct Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Misc/DisplayClusterObjectRef.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterComponentRef>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Component" },
		{ "ModuleRelativePath", "Public/Misc/DisplayClusterObjectRef.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterComponentRef, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
		nullptr,
		&NewStructOps,
		"DisplayClusterComponentRef",
		sizeof(FDisplayClusterComponentRef),
		alignof(FDisplayClusterComponentRef),
		Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterComponentRef()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterComponentRef"), sizeof(FDisplayClusterComponentRef), Get_Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Hash() { return 1716588855U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
