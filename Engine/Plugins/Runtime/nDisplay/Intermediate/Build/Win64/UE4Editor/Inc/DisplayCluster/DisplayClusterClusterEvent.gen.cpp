// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Cluster/DisplayClusterClusterEvent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterClusterEvent() {}
// Cross Module References
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson();
// End Cross Module References

static_assert(std::is_polymorphic<FDisplayClusterClusterEventBinary>() == std::is_polymorphic<FDisplayClusterClusterEventBase>(), "USTRUCT FDisplayClusterClusterEventBinary cannot be polymorphic unless super FDisplayClusterClusterEventBase is polymorphic");

class UScriptStruct* FDisplayClusterClusterEventBinary::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTER_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("DisplayClusterClusterEventBinary"), sizeof(FDisplayClusterClusterEventBinary), Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<FDisplayClusterClusterEventBinary>()
{
	return FDisplayClusterClusterEventBinary::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterClusterEventBinary(FDisplayClusterClusterEventBinary::StaticStruct, TEXT("/Script/DisplayCluster"), TEXT("DisplayClusterClusterEventBinary"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventBinary
{
	FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventBinary()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterClusterEventBinary>(FName(TEXT("DisplayClusterClusterEventBinary")));
	}
} ScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventBinary;
	struct Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EventId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EventData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Cluster event BINARY\n//////////////////////////////////////////////////////////////////////////////////////////////\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "/\n Cluster event BINARY\n/" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterClusterEventBinary>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventId_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Event ID (used for discarding outdated events)\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Event ID (used for discarding outdated events)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventId = { "EventId", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterClusterEventBinary, EventId), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventId_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventData_Inner = { "EventData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventData_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Binary event data\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Binary event data" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventData = { "EventData", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterClusterEventBinary, EventData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::NewProp_EventData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
		Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase,
		&NewStructOps,
		"DisplayClusterClusterEventBinary",
		sizeof(FDisplayClusterClusterEventBinary),
		alignof(FDisplayClusterClusterEventBinary),
		Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterClusterEventBinary"), sizeof(FDisplayClusterClusterEventBinary), Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Hash() { return 814997327U; }

static_assert(std::is_polymorphic<FDisplayClusterClusterEventJson>() == std::is_polymorphic<FDisplayClusterClusterEventBase>(), "USTRUCT FDisplayClusterClusterEventJson cannot be polymorphic unless super FDisplayClusterClusterEventBase is polymorphic");

class UScriptStruct* FDisplayClusterClusterEventJson::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTER_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("DisplayClusterClusterEventJson"), sizeof(FDisplayClusterClusterEventJson), Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<FDisplayClusterClusterEventJson>()
{
	return FDisplayClusterClusterEventJson::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterClusterEventJson(FDisplayClusterClusterEventJson::StaticStruct, TEXT("/Script/DisplayCluster"), TEXT("DisplayClusterClusterEventJson"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventJson
{
	FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventJson()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterClusterEventJson>(FName(TEXT("DisplayClusterClusterEventJson")));
	}
} ScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventJson;
	struct Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Category_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Category;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Cluster event JSON\n//////////////////////////////////////////////////////////////////////////////////////////////\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "/\n Cluster event JSON\n/" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterClusterEventJson>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Event name (used for discarding outdated events)\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Event name (used for discarding outdated events)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterClusterEventJson, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Event type (used for discarding outdated events)\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Event type (used for discarding outdated events)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterClusterEventJson, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Category_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Event category (used for discarding outdated events)\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Event category (used for discarding outdated events)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Category = { "Category", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterClusterEventJson, Category), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Category_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Category_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters_ValueProp = { "Parameters", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters_Key_KeyProp = { "Parameters_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Event parameters\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Event parameters" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterClusterEventJson, Parameters), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Category,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::NewProp_Parameters,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
		Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase,
		&NewStructOps,
		"DisplayClusterClusterEventJson",
		sizeof(FDisplayClusterClusterEventJson),
		alignof(FDisplayClusterClusterEventJson),
		Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterClusterEventJson"), sizeof(FDisplayClusterClusterEventJson), Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Hash() { return 3750328784U; }
class UScriptStruct* FDisplayClusterClusterEventBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTER_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("DisplayClusterClusterEventBase"), sizeof(FDisplayClusterClusterEventBase), Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<FDisplayClusterClusterEventBase>()
{
	return FDisplayClusterClusterEventBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterClusterEventBase(FDisplayClusterClusterEventBase::StaticStruct, TEXT("/Script/DisplayCluster"), TEXT("DisplayClusterClusterEventBase"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventBase
{
	FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventBase()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterClusterEventBase>(FName(TEXT("DisplayClusterClusterEventBase")));
	}
} ScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterClusterEventBase;
	struct Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSystemEvent_MetaData[];
#endif
		static void NewProp_bIsSystemEvent_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSystemEvent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldDiscardOnRepeat_MetaData[];
#endif
		static void NewProp_bShouldDiscardOnRepeat_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldDiscardOnRepeat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////////////////////////////\n// Common cluster event data\n//////////////////////////////////////////////////////////////////////////////////////////////\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "/\n Common cluster event data\n/" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterClusterEventBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bIsSystemEvent_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Is nDisplay internal event (should never be true for end users)\n" },
		{ "DisplayName", "Is Sytem Event. 'True' is reserved for nDisplay internals." },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Is nDisplay internal event (should never be true for end users)" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bIsSystemEvent_SetBit(void* Obj)
	{
		((FDisplayClusterClusterEventBase*)Obj)->bIsSystemEvent = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bIsSystemEvent = { "bIsSystemEvent", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterClusterEventBase), &Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bIsSystemEvent_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bIsSystemEvent_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bIsSystemEvent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bShouldDiscardOnRepeat_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Should older events with the same Name/Type/Category (for JSON) or ID (for binary) be discarded if a new one received\n" },
		{ "ModuleRelativePath", "Public/Cluster/DisplayClusterClusterEvent.h" },
		{ "ToolTip", "Should older events with the same Name/Type/Category (for JSON) or ID (for binary) be discarded if a new one received" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bShouldDiscardOnRepeat_SetBit(void* Obj)
	{
		((FDisplayClusterClusterEventBase*)Obj)->bShouldDiscardOnRepeat = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bShouldDiscardOnRepeat = { "bShouldDiscardOnRepeat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterClusterEventBase), &Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bShouldDiscardOnRepeat_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bShouldDiscardOnRepeat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bShouldDiscardOnRepeat_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bIsSystemEvent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::NewProp_bShouldDiscardOnRepeat,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
		nullptr,
		&NewStructOps,
		"DisplayClusterClusterEventBase",
		sizeof(FDisplayClusterClusterEventBase),
		alignof(FDisplayClusterClusterEventBase),
		Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterClusterEventBase"), sizeof(FDisplayClusterClusterEventBase), Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Hash() { return 1998172203U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
