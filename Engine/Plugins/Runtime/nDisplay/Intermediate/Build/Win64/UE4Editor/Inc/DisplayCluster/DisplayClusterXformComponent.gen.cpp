// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Components/DisplayClusterXformComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterXformComponent() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterXformComponent_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterXformComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterComponent_NoRegister();
// End Cross Module References
	void UDisplayClusterXformComponent::StaticRegisterNativesUDisplayClusterXformComponent()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterXformComponent_NoRegister()
	{
		return UDisplayClusterXformComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterXformComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableGizmo_MetaData[];
#endif
		static void NewProp_bEnableGizmo_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseGizmoScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BaseGizmoScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GizmoScaleMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_GizmoScaleMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProxyMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProxyMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProxyMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ProxyMeshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterXformComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterXformComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "DisplayCluster" },
		{ "Comment", "/**\n * nDisplay Transform component\n */" },
		{ "DisplayName", "NDisplay Transform" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Components/DisplayClusterXformComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterXformComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "nDisplay Transform component" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_bEnableGizmo_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "Comment", "/** Gizmo visibility */" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterXformComponent.h" },
		{ "ToolTip", "Gizmo visibility" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_bEnableGizmo_SetBit(void* Obj)
	{
		((UDisplayClusterXformComponent*)Obj)->bEnableGizmo = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_bEnableGizmo = { "bEnableGizmo", nullptr, (EPropertyFlags)0x0020080800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UDisplayClusterXformComponent), &Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_bEnableGizmo_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_bEnableGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_bEnableGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_BaseGizmoScale_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "Comment", "/** Base gizmo scale */" },
		{ "EditCondition", "bEnableGizmo" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterXformComponent.h" },
		{ "ToolTip", "Base gizmo scale" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_BaseGizmoScale = { "BaseGizmoScale", nullptr, (EPropertyFlags)0x0020080800000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterXformComponent, BaseGizmoScale), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_BaseGizmoScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_BaseGizmoScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_GizmoScaleMultiplier_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.01" },
		{ "Comment", "/** Gizmo scale multiplier */" },
		{ "EditCondition", "bEnableGizmo" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterXformComponent.h" },
		{ "ToolTip", "Gizmo scale multiplier" },
		{ "UIMax", "2.0" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_GizmoScaleMultiplier = { "GizmoScaleMultiplier", nullptr, (EPropertyFlags)0x0020080800000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterXformComponent, GizmoScaleMultiplier), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_GizmoScaleMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_GizmoScaleMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMesh_MetaData[] = {
		{ "Category", "Gizmo" },
		{ "Comment", "/** Proxy mesh to render */" },
		{ "EditCondition", "bEnableGizmo" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterXformComponent.h" },
		{ "ToolTip", "Proxy mesh to render" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMesh = { "ProxyMesh", nullptr, (EPropertyFlags)0x0020080800000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterXformComponent, ProxyMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMeshComponent_MetaData[] = {
		{ "Comment", "/** Proxy mesh component */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterXformComponent.h" },
		{ "ToolTip", "Proxy mesh component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMeshComponent = { "ProxyMeshComponent", nullptr, (EPropertyFlags)0x0020080800082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterXformComponent, ProxyMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMeshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterXformComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_bEnableGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_BaseGizmoScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_GizmoScaleMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterXformComponent_Statics::NewProp_ProxyMeshComponent,
	};
#endif // WITH_EDITORONLY_DATA
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UDisplayClusterXformComponent_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UDisplayClusterComponent_NoRegister, (int32)VTABLE_OFFSET(UDisplayClusterXformComponent, IDisplayClusterComponent), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterXformComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterXformComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterXformComponent_Statics::ClassParams = {
		&UDisplayClusterXformComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::PropPointers, nullptr),
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::PropPointers), 0),
		UE_ARRAY_COUNT(InterfaceParams),
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterXformComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterXformComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterXformComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterXformComponent, 1363702371);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterXformComponent>()
	{
		return UDisplayClusterXformComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterXformComponent(Z_Construct_UClass_UDisplayClusterXformComponent, &UDisplayClusterXformComponent::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterXformComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterXformComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
