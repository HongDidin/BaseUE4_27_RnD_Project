// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterPlayerInput_generated_h
#error "DisplayClusterPlayerInput.generated.h already included, missing '#pragma once' in DisplayClusterPlayerInput.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterPlayerInput_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterPlayerInput(); \
	friend struct Z_Construct_UClass_UDisplayClusterPlayerInput_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterPlayerInput, UPlayerInput, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterPlayerInput)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterPlayerInput(); \
	friend struct Z_Construct_UClass_UDisplayClusterPlayerInput_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterPlayerInput, UPlayerInput, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterPlayerInput)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterPlayerInput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterPlayerInput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterPlayerInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterPlayerInput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterPlayerInput(UDisplayClusterPlayerInput&&); \
	NO_API UDisplayClusterPlayerInput(const UDisplayClusterPlayerInput&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterPlayerInput(UDisplayClusterPlayerInput&&); \
	NO_API UDisplayClusterPlayerInput(const UDisplayClusterPlayerInput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterPlayerInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterPlayerInput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterPlayerInput)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_12_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterPlayerInput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterPlayerInput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
