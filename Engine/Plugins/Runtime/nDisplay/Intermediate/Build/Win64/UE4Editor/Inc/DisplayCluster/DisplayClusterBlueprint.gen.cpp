// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Blueprints/DisplayClusterBlueprint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterBlueprint() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterBlueprint_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterBlueprint();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprint();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister();
// End Cross Module References
	void UDisplayClusterBlueprint::StaticRegisterNativesUDisplayClusterBlueprint()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterBlueprint_NoRegister()
	{
		return UDisplayClusterBlueprint::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterBlueprint_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigExport_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ConfigExport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConfigData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AssetVersion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterBlueprint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprint,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterBlueprint_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "nDisplay Blueprint" },
		{ "IncludePath", "Blueprints/DisplayClusterBlueprint.h" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprint.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigExport_MetaData[] = {
		{ "Comment", "// Holds the last saved config export. In the AssetRegistry to allow parsing without loading.\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprint.h" },
		{ "ToolTip", "Holds the last saved config export. In the AssetRegistry to allow parsing without loading." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigExport = { "ConfigExport", nullptr, (EPropertyFlags)0x0010010000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterBlueprint, ConfigExport), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigExport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigExport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigData_MetaData[] = {
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigData = { "ConfigData", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterBlueprint, ConfigData), Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_AssetVersion_MetaData[] = {
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprint.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_AssetVersion = { "AssetVersion", nullptr, (EPropertyFlags)0x0040010000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterBlueprint, AssetVersion), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_AssetVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_AssetVersion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterBlueprint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigExport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_ConfigData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterBlueprint_Statics::NewProp_AssetVersion,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterBlueprint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterBlueprint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterBlueprint_Statics::ClassParams = {
		&UDisplayClusterBlueprint::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterBlueprint_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterBlueprint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterBlueprint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterBlueprint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterBlueprint, 4279633584);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterBlueprint>()
	{
		return UDisplayClusterBlueprint::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterBlueprint(Z_Construct_UClass_UDisplayClusterBlueprint, &UDisplayClusterBlueprint::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterBlueprint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterBlueprint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
