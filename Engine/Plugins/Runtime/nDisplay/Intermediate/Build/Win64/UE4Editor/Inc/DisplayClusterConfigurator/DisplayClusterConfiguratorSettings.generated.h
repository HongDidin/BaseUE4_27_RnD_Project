// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorSettings_generated_h
#error "DisplayClusterConfiguratorSettings.generated.h already included, missing '#pragma once' in DisplayClusterConfiguratorSettings.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorSettings_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorEditorSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorEditorSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorEditorSettings(UDisplayClusterConfiguratorEditorSettings&&); \
	NO_API UDisplayClusterConfiguratorEditorSettings(const UDisplayClusterConfiguratorEditorSettings&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorEditorSettings(UDisplayClusterConfiguratorEditorSettings&&); \
	NO_API UDisplayClusterConfiguratorEditorSettings(const UDisplayClusterConfiguratorEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorEditorSettings)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_9_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Settings_DisplayClusterConfiguratorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
