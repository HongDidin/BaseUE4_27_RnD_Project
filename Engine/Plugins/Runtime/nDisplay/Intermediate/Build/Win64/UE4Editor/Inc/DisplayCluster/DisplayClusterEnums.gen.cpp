// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/DisplayClusterEnums.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterEnums() {}
// Cross Module References
	DISPLAYCLUSTER_API UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterSyncGroup();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	DISPLAYCLUSTER_API UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole();
	DISPLAYCLUSTER_API UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode();
// End Cross Module References
	static UEnum* EDisplayClusterSyncGroup_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayCluster_EDisplayClusterSyncGroup, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("EDisplayClusterSyncGroup"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTER_API UEnum* StaticEnum<EDisplayClusterSyncGroup>()
	{
		return EDisplayClusterSyncGroup_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterSyncGroup(EDisplayClusterSyncGroup_StaticEnum, TEXT("/Script/DisplayCluster"), TEXT("EDisplayClusterSyncGroup"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayCluster_EDisplayClusterSyncGroup_Hash() { return 1762327756U; }
	UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterSyncGroup()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterSyncGroup"), 0, Get_Z_Construct_UEnum_DisplayCluster_EDisplayClusterSyncGroup_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterSyncGroup::PreTick", (int64)EDisplayClusterSyncGroup::PreTick },
				{ "EDisplayClusterSyncGroup::Tick", (int64)EDisplayClusterSyncGroup::Tick },
				{ "EDisplayClusterSyncGroup::PostTick", (int64)EDisplayClusterSyncGroup::PostTick },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * Display cluster synchronization groups\n */" },
				{ "ModuleRelativePath", "Public/DisplayClusterEnums.h" },
				{ "PostTick.Name", "EDisplayClusterSyncGroup::PostTick" },
				{ "PreTick.Name", "EDisplayClusterSyncGroup::PreTick" },
				{ "Tick.Name", "EDisplayClusterSyncGroup::Tick" },
				{ "ToolTip", "Display cluster synchronization groups" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayCluster,
				nullptr,
				"EDisplayClusterSyncGroup",
				"EDisplayClusterSyncGroup",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterNodeRole_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("EDisplayClusterNodeRole"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTER_API UEnum* StaticEnum<EDisplayClusterNodeRole>()
	{
		return EDisplayClusterNodeRole_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterNodeRole(EDisplayClusterNodeRole_StaticEnum, TEXT("/Script/DisplayCluster"), TEXT("EDisplayClusterNodeRole"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole_Hash() { return 1573131300U; }
	UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterNodeRole"), 0, Get_Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterNodeRole::None", (int64)EDisplayClusterNodeRole::None },
				{ "EDisplayClusterNodeRole::Master", (int64)EDisplayClusterNodeRole::Master },
				{ "EDisplayClusterNodeRole::Slave", (int64)EDisplayClusterNodeRole::Slave },
				{ "EDisplayClusterNodeRole::Backup", (int64)EDisplayClusterNodeRole::Backup },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Backup.Name", "EDisplayClusterNodeRole::Backup" },
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * Display cluster node role\n */" },
				{ "Master.Name", "EDisplayClusterNodeRole::Master" },
				{ "ModuleRelativePath", "Public/DisplayClusterEnums.h" },
				{ "None.Name", "EDisplayClusterNodeRole::None" },
				{ "Slave.Name", "EDisplayClusterNodeRole::Slave" },
				{ "ToolTip", "Display cluster node role" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayCluster,
				nullptr,
				"EDisplayClusterNodeRole",
				"EDisplayClusterNodeRole",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EDisplayClusterOperationMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("EDisplayClusterOperationMode"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTER_API UEnum* StaticEnum<EDisplayClusterOperationMode>()
	{
		return EDisplayClusterOperationMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterOperationMode(EDisplayClusterOperationMode_StaticEnum, TEXT("/Script/DisplayCluster"), TEXT("EDisplayClusterOperationMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode_Hash() { return 1340398591U; }
	UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterOperationMode"), 0, Get_Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterOperationMode::Cluster", (int64)EDisplayClusterOperationMode::Cluster },
				{ "EDisplayClusterOperationMode::Editor", (int64)EDisplayClusterOperationMode::Editor },
				{ "EDisplayClusterOperationMode::Disabled", (int64)EDisplayClusterOperationMode::Disabled },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Cluster.Name", "EDisplayClusterOperationMode::Cluster" },
				{ "Comment", "/**\n * Display cluster operation mode\n */" },
				{ "Disabled.Name", "EDisplayClusterOperationMode::Disabled" },
				{ "Editor.Name", "EDisplayClusterOperationMode::Editor" },
				{ "ModuleRelativePath", "Public/DisplayClusterEnums.h" },
				{ "ToolTip", "Display cluster operation mode" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayCluster,
				nullptr,
				"EDisplayClusterOperationMode",
				"EDisplayClusterOperationMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
