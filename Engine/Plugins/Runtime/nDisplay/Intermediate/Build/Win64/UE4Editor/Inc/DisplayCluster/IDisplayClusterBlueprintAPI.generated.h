// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FSceneViewExtensionIsActiveFunctor;
struct FIntPoint;
struct FPostProcessSettings;
struct FQuat;
struct FVector;
class UDisplayClusterCameraComponent;
class UDisplayClusterScreenComponent;
class ADisplayClusterRootActor;
class UDisplayClusterConfigurationData;
struct FDisplayClusterClusterEventBinary;
struct FDisplayClusterClusterEventJson;
class IDisplayClusterClusterEventListener;
enum class EDisplayClusterNodeRole : uint8;
enum class EDisplayClusterOperationMode : uint8;
#ifdef DISPLAYCLUSTER_IDisplayClusterBlueprintAPI_generated_h
#error "IDisplayClusterBlueprintAPI.generated.h already included, missing '#pragma once' in IDisplayClusterBlueprintAPI.h"
#endif
#define DISPLAYCLUSTER_IDisplayClusterBlueprintAPI_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSceneViewExtensionIsActiveInContextFunction); \
	DECLARE_FUNCTION(execGetLocalViewports); \
	DECLARE_FUNCTION(execGetViewportRect); \
	DECLARE_FUNCTION(execSetFinalPostProcessingSettings); \
	DECLARE_FUNCTION(execSetOverridePostProcessingSettings); \
	DECLARE_FUNCTION(execSetStartPostProcessingSettings); \
	DECLARE_FUNCTION(execSetBufferRatio); \
	DECLARE_FUNCTION(execGetBufferRatio); \
	DECLARE_FUNCTION(execSetViewportCamera); \
	DECLARE_FUNCTION(execGetTrackerQuat); \
	DECLARE_FUNCTION(execGetTrackerLocation); \
	DECLARE_FUNCTION(execGetAxis); \
	DECLARE_FUNCTION(execWasButtonReleased); \
	DECLARE_FUNCTION(execWasButtonPressed); \
	DECLARE_FUNCTION(execIsButtonReleased); \
	DECLARE_FUNCTION(execIsButtonPressed); \
	DECLARE_FUNCTION(execGetButtonState); \
	DECLARE_FUNCTION(execGetTrackerDeviceIds); \
	DECLARE_FUNCTION(execGetKeyboardDeviceIds); \
	DECLARE_FUNCTION(execGetButtonDeviceIds); \
	DECLARE_FUNCTION(execGetAxisDeviceIds); \
	DECLARE_FUNCTION(execGetTrackerDeviceAmount); \
	DECLARE_FUNCTION(execGetButtonDeviceAmount); \
	DECLARE_FUNCTION(execGetAxisDeviceAmount); \
	DECLARE_FUNCTION(execSetDefaultCameraById); \
	DECLARE_FUNCTION(execGetDefaultCamera); \
	DECLARE_FUNCTION(execGetCamerasAmount); \
	DECLARE_FUNCTION(execGetCameraById); \
	DECLARE_FUNCTION(execGetAllCameras); \
	DECLARE_FUNCTION(execGetScreensAmount); \
	DECLARE_FUNCTION(execGetAllScreens); \
	DECLARE_FUNCTION(execGetScreenById); \
	DECLARE_FUNCTION(execGetRootActor); \
	DECLARE_FUNCTION(execGetConfig); \
	DECLARE_FUNCTION(execSendClusterEventBinaryTo); \
	DECLARE_FUNCTION(execSendClusterEventJsonTo); \
	DECLARE_FUNCTION(execEmitClusterEventBinary); \
	DECLARE_FUNCTION(execEmitClusterEventJson); \
	DECLARE_FUNCTION(execRemoveClusterEventListener); \
	DECLARE_FUNCTION(execAddClusterEventListener); \
	DECLARE_FUNCTION(execGetNodesAmount); \
	DECLARE_FUNCTION(execGetNodeId); \
	DECLARE_FUNCTION(execGetNodeIds); \
	DECLARE_FUNCTION(execGetClusterRole); \
	DECLARE_FUNCTION(execIsBackup); \
	DECLARE_FUNCTION(execIsSlave); \
	DECLARE_FUNCTION(execIsMaster); \
	DECLARE_FUNCTION(execGetOperationMode); \
	DECLARE_FUNCTION(execIsModuleInitialized);


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSceneViewExtensionIsActiveInContextFunction); \
	DECLARE_FUNCTION(execGetLocalViewports); \
	DECLARE_FUNCTION(execGetViewportRect); \
	DECLARE_FUNCTION(execSetFinalPostProcessingSettings); \
	DECLARE_FUNCTION(execSetOverridePostProcessingSettings); \
	DECLARE_FUNCTION(execSetStartPostProcessingSettings); \
	DECLARE_FUNCTION(execSetBufferRatio); \
	DECLARE_FUNCTION(execGetBufferRatio); \
	DECLARE_FUNCTION(execSetViewportCamera); \
	DECLARE_FUNCTION(execGetTrackerQuat); \
	DECLARE_FUNCTION(execGetTrackerLocation); \
	DECLARE_FUNCTION(execGetAxis); \
	DECLARE_FUNCTION(execWasButtonReleased); \
	DECLARE_FUNCTION(execWasButtonPressed); \
	DECLARE_FUNCTION(execIsButtonReleased); \
	DECLARE_FUNCTION(execIsButtonPressed); \
	DECLARE_FUNCTION(execGetButtonState); \
	DECLARE_FUNCTION(execGetTrackerDeviceIds); \
	DECLARE_FUNCTION(execGetKeyboardDeviceIds); \
	DECLARE_FUNCTION(execGetButtonDeviceIds); \
	DECLARE_FUNCTION(execGetAxisDeviceIds); \
	DECLARE_FUNCTION(execGetTrackerDeviceAmount); \
	DECLARE_FUNCTION(execGetButtonDeviceAmount); \
	DECLARE_FUNCTION(execGetAxisDeviceAmount); \
	DECLARE_FUNCTION(execSetDefaultCameraById); \
	DECLARE_FUNCTION(execGetDefaultCamera); \
	DECLARE_FUNCTION(execGetCamerasAmount); \
	DECLARE_FUNCTION(execGetCameraById); \
	DECLARE_FUNCTION(execGetAllCameras); \
	DECLARE_FUNCTION(execGetScreensAmount); \
	DECLARE_FUNCTION(execGetAllScreens); \
	DECLARE_FUNCTION(execGetScreenById); \
	DECLARE_FUNCTION(execGetRootActor); \
	DECLARE_FUNCTION(execGetConfig); \
	DECLARE_FUNCTION(execSendClusterEventBinaryTo); \
	DECLARE_FUNCTION(execSendClusterEventJsonTo); \
	DECLARE_FUNCTION(execEmitClusterEventBinary); \
	DECLARE_FUNCTION(execEmitClusterEventJson); \
	DECLARE_FUNCTION(execRemoveClusterEventListener); \
	DECLARE_FUNCTION(execAddClusterEventListener); \
	DECLARE_FUNCTION(execGetNodesAmount); \
	DECLARE_FUNCTION(execGetNodeId); \
	DECLARE_FUNCTION(execGetNodeIds); \
	DECLARE_FUNCTION(execGetClusterRole); \
	DECLARE_FUNCTION(execIsBackup); \
	DECLARE_FUNCTION(execIsSlave); \
	DECLARE_FUNCTION(execIsMaster); \
	DECLARE_FUNCTION(execGetOperationMode); \
	DECLARE_FUNCTION(execIsModuleInitialized);


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterBlueprintAPI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterBlueprintAPI) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterBlueprintAPI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterBlueprintAPI); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterBlueprintAPI(UDisplayClusterBlueprintAPI&&); \
	NO_API UDisplayClusterBlueprintAPI(const UDisplayClusterBlueprintAPI&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterBlueprintAPI(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterBlueprintAPI(UDisplayClusterBlueprintAPI&&); \
	NO_API UDisplayClusterBlueprintAPI(const UDisplayClusterBlueprintAPI&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterBlueprintAPI); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterBlueprintAPI); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterBlueprintAPI)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUDisplayClusterBlueprintAPI(); \
	friend struct Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterBlueprintAPI, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterBlueprintAPI)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IDisplayClusterBlueprintAPI() {} \
public: \
	typedef UDisplayClusterBlueprintAPI UClassType; \
	typedef IDisplayClusterBlueprintAPI ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_INCLASS_IINTERFACE \
protected: \
	virtual ~IDisplayClusterBlueprintAPI() {} \
public: \
	typedef UDisplayClusterBlueprintAPI UClassType; \
	typedef IDisplayClusterBlueprintAPI ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_29_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h_32_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterBlueprintAPI>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Blueprints_IDisplayClusterBlueprintAPI_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
