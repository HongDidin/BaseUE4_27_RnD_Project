// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_ICVFX.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_ICVFX() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_EntireClusterColorGrading();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_PerViewportColorGrading();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_ACineCameraActor_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_AllNodesColorGrading();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_PerNodeColorGrading();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationCameraMotionBlurMode();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_CustomPostprocess();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_LightcardRenderMode();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ACTORLAYERUTILITIES_API UScriptStruct* Z_Construct_UScriptStruct_FActorLayer();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
class UScriptStruct* FDisplayClusterConfigurationICVFX_StageSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_StageSettings"), sizeof(FDisplayClusterConfigurationICVFX_StageSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_StageSettings>()
{
	return FDisplayClusterConfigurationICVFX_StageSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings(FDisplayClusterConfigurationICVFX_StageSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_StageSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_StageSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_StageSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_StageSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_StageSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_StageSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableInnerFrustums_MetaData[];
#endif
		static void NewProp_bEnableInnerFrustums_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableInnerFrustums;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultFrameSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultFrameSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Lightcard_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Lightcard;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HideList_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HideList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OuterViewportHideList_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OuterViewportHideList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EntireClusterColorGrading_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EntireClusterColorGrading;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PerViewportColorGrading_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerViewportColorGrading_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PerViewportColorGrading;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseOverallClusterOCIOConfiguration_MetaData[];
#endif
		static void NewProp_bUseOverallClusterOCIOConfiguration_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseOverallClusterOCIOConfiguration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllViewportsOCIOConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AllViewportsOCIOConfiguration;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PerViewportOCIOProfiles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerViewportOCIOProfiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PerViewportOCIOProfiles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_StageSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bEnableInnerFrustums_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Enable/disable the inner frustum on all ICVFX cameras. */" },
		{ "DisplayName", "Enable Inner Frustum" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Enable/disable the inner frustum on all ICVFX cameras." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bEnableInnerFrustums_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_StageSettings*)Obj)->bEnableInnerFrustums = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bEnableInnerFrustums = { "bEnableInnerFrustums", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_StageSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bEnableInnerFrustums_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bEnableInnerFrustums_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bEnableInnerFrustums_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_DefaultFrameSize_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Default incameras RTT texture size. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Default incameras RTT texture size." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_DefaultFrameSize = { "DefaultFrameSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, DefaultFrameSize), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_DefaultFrameSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_DefaultFrameSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_Lightcard_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_Lightcard = { "Lightcard", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, Lightcard), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_Lightcard_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_Lightcard_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_HideList_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Hide list for all icvfx viewports (outer, inner, cameras, etc)\n// (This allow to hide all actors from layers for icvfx render logic)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Hide list for all icvfx viewports (outer, inner, cameras, etc)\n(This allow to hide all actors from layers for icvfx render logic)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_HideList = { "HideList", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, HideList), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_HideList_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_HideList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_OuterViewportHideList_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Special hide list for Outer viewports */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Special hide list for Outer viewports" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_OuterViewportHideList = { "OuterViewportHideList", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, OuterViewportHideList), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_OuterViewportHideList_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_OuterViewportHideList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_EntireClusterColorGrading_MetaData[] = {
		{ "Category", "Viewport Color Grading" },
		{ "Comment", "/** Entire Cluster Color Grading */" },
		{ "DisplayName", "Entire Cluster" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Entire Cluster Color Grading" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_EntireClusterColorGrading = { "EntireClusterColorGrading", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, EntireClusterColorGrading), Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_EntireClusterColorGrading, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_EntireClusterColorGrading_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_EntireClusterColorGrading_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportColorGrading_Inner = { "PerViewportColorGrading", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_PerViewportColorGrading, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportColorGrading_MetaData[] = {
		{ "Category", "Viewport Color Grading" },
		{ "Comment", "/** Perform advanced color grading operations on a per-viewport or group-of-viewports basis. */" },
		{ "ConfigurationMode", "Viewports" },
		{ "DisplayName", "Per-Viewport Color Grading" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Perform advanced color grading operations on a per-viewport or group-of-viewports basis." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportColorGrading = { "PerViewportColorGrading", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, PerViewportColorGrading), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportColorGrading_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportColorGrading_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bUseOverallClusterOCIOConfiguration_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** Enable the application of an OpenColorIO configuration to all viewports. */" },
		{ "DisplayName", "Enable Viewport OCIO" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Enable the application of an OpenColorIO configuration to all viewports." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bUseOverallClusterOCIOConfiguration_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_StageSettings*)Obj)->bUseOverallClusterOCIOConfiguration = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bUseOverallClusterOCIOConfiguration = { "bUseOverallClusterOCIOConfiguration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_StageSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bUseOverallClusterOCIOConfiguration_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bUseOverallClusterOCIOConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bUseOverallClusterOCIOConfiguration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_AllViewportsOCIOConfiguration_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** Apply this OpenColorIO configuration to all viewports. */" },
		{ "DisplayName", "All Viewports Color Configuration" },
		{ "EditCondition", "bUseOverallClusterOCIOConfiguration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Apply this OpenColorIO configuration to all viewports." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_AllViewportsOCIOConfiguration = { "AllViewportsOCIOConfiguration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, AllViewportsOCIOConfiguration), Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_AllViewportsOCIOConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_AllViewportsOCIOConfiguration_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportOCIOProfiles_Inner = { "PerViewportOCIOProfiles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportOCIOProfiles_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** Apply an OpenColorIO configuration on a per-viewport or group-of-viewports basis. */" },
		{ "ConfigurationMode", "Viewports" },
		{ "DisplayName", "Per-Viewport OCIO Overrides" },
		{ "EditCondition", "bUseOverallClusterOCIOConfiguration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Apply an OpenColorIO configuration on a per-viewport or group-of-viewports basis." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportOCIOProfiles = { "PerViewportOCIOProfiles", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_StageSettings, PerViewportOCIOProfiles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportOCIOProfiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportOCIOProfiles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bEnableInnerFrustums,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_DefaultFrameSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_Lightcard,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_HideList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_OuterViewportHideList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_EntireClusterColorGrading,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportColorGrading_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportColorGrading,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_bUseOverallClusterOCIOConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_AllViewportsOCIOConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportOCIOProfiles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::NewProp_PerViewportOCIOProfiles,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_StageSettings",
		sizeof(FDisplayClusterConfigurationICVFX_StageSettings),
		alignof(FDisplayClusterConfigurationICVFX_StageSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_StageSettings"), sizeof(FDisplayClusterConfigurationICVFX_StageSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings_Hash() { return 213475353U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_CameraSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_CameraSettings"), sizeof(FDisplayClusterConfigurationICVFX_CameraSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_CameraSettings>()
{
	return FDisplayClusterConfigurationICVFX_CameraSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings(FDisplayClusterConfigurationICVFX_CameraSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_CameraSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_CameraSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_CameraSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalCameraActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_ExternalCameraActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BufferRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FieldOfViewMultiplier_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FieldOfViewMultiplier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SoftEdge_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SoftEdge;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrustumRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrustumRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrustumOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FrustumOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraMotionBlur_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraMotionBlur;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Chromakey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Chromakey;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllNodesOCIOConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AllNodesOCIOConfiguration;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PerNodeOCIOProfiles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerNodeOCIOProfiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PerNodeOCIOProfiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllNodesColorGrading_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AllNodesColorGrading;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PerNodeColorGrading_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerNodeColorGrading_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PerNodeColorGrading;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraHideList_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraHideList;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_CameraSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_bEnable_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Render the inner frustum for this ICVFX camera. */" },
		{ "DisplayName", "Enable Inner Frustum" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Render the inner frustum for this ICVFX camera." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_CameraSettings*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_CameraSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_bEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_ExternalCameraActor_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Specify a Cine Camera Actor for this ICVFX camera to use instead of the default nDisplay camera. */" },
		{ "DisplayName", "Cine Camera Actor" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Specify a Cine Camera Actor for this ICVFX camera to use instead of the default nDisplay camera." },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_ExternalCameraActor = { "ExternalCameraActor", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, ExternalCameraActor), Z_Construct_UClass_ACineCameraActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_ExternalCameraActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_ExternalCameraActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_BufferRatio_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.05" },
		{ "Comment", "/** Adjust resolution scaling for the inner frustum. */" },
		{ "DisplayName", "Inner Frustum Screen Percentage" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Adjust resolution scaling for the inner frustum." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, BufferRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_BufferRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_BufferRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FieldOfViewMultiplier_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "5.0" },
		{ "ClampMin", "0.05" },
		{ "Comment", "/** Multiply the field of view for the ICVFX camera by this value.  This can increase the overall size of the inner frustum to help provide a buffer against latency when moving the camera. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Multiply the field of view for the ICVFX camera by this value.  This can increase the overall size of the inner frustum to help provide a buffer against latency when moving the camera." },
		{ "UIMax", "5.0" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FieldOfViewMultiplier = { "FieldOfViewMultiplier", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, FieldOfViewMultiplier), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FieldOfViewMultiplier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FieldOfViewMultiplier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_SoftEdge_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Soften the edges of the inner frustum to help avoid hard lines in reflections seen by the live-action camera. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Soften the edges of the inner frustum to help avoid hard lines in reflections seen by the live-action camera." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_SoftEdge = { "SoftEdge", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, SoftEdge), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_SoftEdge_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_SoftEdge_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumRotation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Rotate the inner frustum. */" },
		{ "DisplayName", "Inner Frustum Rotation" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Rotate the inner frustum." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumRotation = { "FrustumRotation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, FrustumRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumOffset_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Specify an offset on the inner frustum. */" },
		{ "DisplayName", "Inner Frustum Offset" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Specify an offset on the inner frustum." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumOffset = { "FrustumOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, FrustumOffset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraMotionBlur_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Render motion blur more accurately by subtracting blur from camera motion and avoiding amplification of blur by the physical camera. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Render motion blur more accurately by subtracting blur from camera motion and avoiding amplification of blur by the physical camera." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraMotionBlur = { "CameraMotionBlur", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, CameraMotionBlur), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraMotionBlur_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraMotionBlur_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_RenderSettings_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Configure global render settings for this viewport */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Configure global render settings for this viewport" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_RenderSettings = { "RenderSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, RenderSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_RenderSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_RenderSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_Chromakey_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_Chromakey = { "Chromakey", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, Chromakey), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_Chromakey_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_Chromakey_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesOCIOConfiguration_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** OCIO Display look configuration for this camera */" },
		{ "DisplayName", "All Nodes OCIO Configuration" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "OCIO Display look configuration for this camera" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesOCIOConfiguration = { "AllNodesOCIOConfiguration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, AllNodesOCIOConfiguration), Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesOCIOConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesOCIOConfiguration_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeOCIOProfiles_Inner = { "PerNodeOCIOProfiles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeOCIOProfiles_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** Apply an OpenColorIO configuration on a per-node or group-of-nodes basis. */" },
		{ "ConfigurationMode", "ClusterNodes" },
		{ "DisplayName", "Per-Node OCIO Overrides" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Apply an OpenColorIO configuration on a per-node or group-of-nodes basis." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeOCIOProfiles = { "PerNodeOCIOProfiles", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, PerNodeOCIOProfiles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeOCIOProfiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeOCIOProfiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesColorGrading_MetaData[] = {
		{ "Category", "Inner Frustum Color Grading" },
		{ "Comment", "/** All Nodes Color Grading */" },
		{ "DisplayName", "All Nodes Color Grading" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "All Nodes Color Grading" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesColorGrading = { "AllNodesColorGrading", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, AllNodesColorGrading), Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_AllNodesColorGrading, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesColorGrading_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesColorGrading_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeColorGrading_Inner = { "PerNodeColorGrading", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_PerNodeColorGrading, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeColorGrading_MetaData[] = {
		{ "Category", "Inner Frustum Color Grading" },
		{ "Comment", "/** Perform advanced color grading operations for the inner frustum on a per-node or group-of-nodes basis. */" },
		{ "ConfigurationMode", "ClusterNodes" },
		{ "DisplayName", "Per-Node Color Grading" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Perform advanced color grading operations for the inner frustum on a per-node or group-of-nodes basis." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeColorGrading = { "PerNodeColorGrading", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, PerNodeColorGrading), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeColorGrading_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeColorGrading_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraHideList_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Content specified here will not appear in the inner frustum, but can appear in the nDisplay viewports. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Content specified here will not appear in the inner frustum, but can appear in the nDisplay viewports." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraHideList = { "CameraHideList", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSettings, CameraHideList), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraHideList_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraHideList_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_ExternalCameraActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FieldOfViewMultiplier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_SoftEdge,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_FrustumOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraMotionBlur,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_RenderSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_Chromakey,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesOCIOConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeOCIOProfiles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeOCIOProfiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_AllNodesColorGrading,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeColorGrading_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_PerNodeColorGrading,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::NewProp_CameraHideList,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_CameraSettings",
		sizeof(FDisplayClusterConfigurationICVFX_CameraSettings),
		alignof(FDisplayClusterConfigurationICVFX_CameraSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_CameraSettings"), sizeof(FDisplayClusterConfigurationICVFX_CameraSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings_Hash() { return 1025488896U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_CameraSoftEdge::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_CameraSoftEdge"), sizeof(FDisplayClusterConfigurationICVFX_CameraSoftEdge), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_CameraSoftEdge>()
{
	return FDisplayClusterConfigurationICVFX_CameraSoftEdge::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge(FDisplayClusterConfigurationICVFX_CameraSoftEdge::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_CameraSoftEdge"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraSoftEdge
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraSoftEdge()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_CameraSoftEdge>(FName(TEXT("DisplayClusterConfigurationICVFX_CameraSoftEdge")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraSoftEdge;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertical_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Vertical;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Horizontal_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Horizontal;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_CameraSoftEdge>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Vertical_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Adjust blur amount to the top and bottom edges of the inner frustum. */" },
		{ "DisplayName", "Top and Bottom" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Adjust blur amount to the top and bottom edges of the inner frustum." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Vertical = { "Vertical", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSoftEdge, Vertical), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Vertical_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Vertical_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Horizontal_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Adjust blur amount to the left and right side edges of the inner frustum. */" },
		{ "DisplayName", "Sides" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Adjust blur amount to the left and right side edges of the inner frustum." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Horizontal = { "Horizontal", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraSoftEdge, Horizontal), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Horizontal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Horizontal_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Vertical,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::NewProp_Horizontal,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_CameraSoftEdge",
		sizeof(FDisplayClusterConfigurationICVFX_CameraSoftEdge),
		alignof(FDisplayClusterConfigurationICVFX_CameraSoftEdge),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_CameraSoftEdge"), sizeof(FDisplayClusterConfigurationICVFX_CameraSoftEdge), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSoftEdge_Hash() { return 2458340746U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_CameraMotionBlur::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlur"), sizeof(FDisplayClusterConfigurationICVFX_CameraMotionBlur), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_CameraMotionBlur>()
{
	return FDisplayClusterConfigurationICVFX_CameraMotionBlur::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur(FDisplayClusterConfigurationICVFX_CameraMotionBlur::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlur"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraMotionBlur
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraMotionBlur()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_CameraMotionBlur>(FName(TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlur")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraMotionBlur;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MotionBlurMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MotionBlurMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MotionBlurMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TranslationScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TranslationScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MotionBlurPPS_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MotionBlurPPS;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_CameraMotionBlur>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurMode_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Specify the motion blur mode for the inner frustum, correcting for the motion of the camera. Blur due to camera motion will be incorrectly doubled in the physically exposed image if there is already camera blur applied to the inner frustum. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Specify the motion blur mode for the inner frustum, correcting for the motion of the camera. Blur due to camera motion will be incorrectly doubled in the physically exposed image if there is already camera blur applied to the inner frustum." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurMode = { "MotionBlurMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraMotionBlur, MotionBlurMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationCameraMotionBlurMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_TranslationScale_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Translation Scale */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Translation Scale" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_TranslationScale = { "TranslationScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraMotionBlur, TranslationScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_TranslationScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_TranslationScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurPPS_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Motion Blur Settings Override */" },
		{ "DisplayName", "Motion Blur Settings Override" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Motion Blur Settings Override" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurPPS = { "MotionBlurPPS", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraMotionBlur, MotionBlurPPS), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurPPS_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurPPS_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_TranslationScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::NewProp_MotionBlurPPS,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_CameraMotionBlur",
		sizeof(FDisplayClusterConfigurationICVFX_CameraMotionBlur),
		alignof(FDisplayClusterConfigurationICVFX_CameraMotionBlur),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlur"), sizeof(FDisplayClusterConfigurationICVFX_CameraMotionBlur), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlur_Hash() { return 3302034205U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS"), sizeof(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS>()
{
	return FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS>(FName(TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReplaceEnable_MetaData[];
#endif
		static void NewProp_bReplaceEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReplaceEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MotionBlurAmount_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MotionBlurAmount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MotionBlurMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MotionBlurMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MotionBlurPerObjectSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MotionBlurPerObjectSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_bReplaceEnable_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** If enabled, override the overall motion blur settings that would otherwise come from the current post-process volume or Cine Camera. */" },
		{ "DisplayName", "Enable Settings Override" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "If enabled, override the overall motion blur settings that would otherwise come from the current post-process volume or Cine Camera." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_bReplaceEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS*)Obj)->bReplaceEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_bReplaceEnable = { "bReplaceEnable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_bReplaceEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_bReplaceEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_bReplaceEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurAmount_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of motion blur, 0:off. */" },
		{ "DisplayName", "Intensity" },
		{ "editcondition", "bReplaceEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Strength of motion blur, 0:off." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurAmount = { "MotionBlurAmount", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS, MotionBlurAmount), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurAmount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurAmount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurMax_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "100.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Max distortion caused by motion blur in percent of the screen width, 0:off */" },
		{ "DisplayName", "Max" },
		{ "editcondition", "bReplaceEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Max distortion caused by motion blur in percent of the screen width, 0:off" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurMax = { "MotionBlurMax", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS, MotionBlurMax), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurPerObjectSize_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** The minimum projected screen radius for a primitive to be drawn in the velocity pass.Percentage of screen width, smaller numbers cause more draw calls, default: 4 % */" },
		{ "DisplayName", "Per Object Size" },
		{ "editcondition", "bReplaceEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "The minimum projected screen radius for a primitive to be drawn in the velocity pass.Percentage of screen width, smaller numbers cause more draw calls, default: 4 %" },
		{ "UIMax", "100.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurPerObjectSize = { "MotionBlurPerObjectSize", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS, MotionBlurPerObjectSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurPerObjectSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurPerObjectSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_bReplaceEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurAmount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::NewProp_MotionBlurPerObjectSize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS",
		sizeof(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS),
		alignof(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS"), sizeof(FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraMotionBlurOverridePPS_Hash() { return 1329172790U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_CameraRenderSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_CameraRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_CameraRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_CameraRenderSettings>()
{
	return FDisplayClusterConfigurationICVFX_CameraRenderSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings(FDisplayClusterConfigurationICVFX_CameraRenderSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_CameraRenderSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraRenderSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraRenderSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_CameraRenderSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_CameraRenderSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraRenderSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomFrameSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomFrameSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RenderOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomPostprocess_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomPostprocess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCameraComponentPostprocess_MetaData[];
#endif
		static void NewProp_bUseCameraComponentPostprocess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCameraComponentPostprocess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Replace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Replace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostprocessBlur_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PostprocessBlur;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateMips_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GenerateMips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvancedRenderSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdvancedRenderSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_CameraRenderSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomFrameSize_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Define custom inner camera viewport size\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Define custom inner camera viewport size" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomFrameSize = { "CustomFrameSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraRenderSettings, CustomFrameSize), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomFrameSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomFrameSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_RenderOrder_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Camera render order, bigger value is over\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Camera render order, bigger value is over" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_RenderOrder = { "RenderOrder", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraRenderSettings, RenderOrder), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_RenderOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_RenderOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomPostprocess_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomPostprocess = { "CustomPostprocess", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraRenderSettings, CustomPostprocess), Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_CustomPostprocess, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomPostprocess_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomPostprocess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_bUseCameraComponentPostprocess_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Use postprocess settings from camera component\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Use postprocess settings from camera component" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_bUseCameraComponentPostprocess_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_CameraRenderSettings*)Obj)->bUseCameraComponentPostprocess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_bUseCameraComponentPostprocess = { "bUseCameraComponentPostprocess", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_CameraRenderSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_bUseCameraComponentPostprocess_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_bUseCameraComponentPostprocess_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_bUseCameraComponentPostprocess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_Replace_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Replace viewport render from source texture\n" },
		{ "DisplayName", "Mipmapping" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Replace viewport render from source texture" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_Replace = { "Replace", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraRenderSettings, Replace), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_Replace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_Replace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_PostprocessBlur_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_PostprocessBlur = { "PostprocessBlur", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraRenderSettings, PostprocessBlur), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_PostprocessBlur_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_PostprocessBlur_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_GenerateMips_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Mipmapping can help avoid visual artifacts when the inner frustum is rendered at a lower resolution than specified in the configuration and is smaller on screen than the available pixels on the display device. */" },
		{ "DisplayName", "Mipmapping" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Mipmapping can help avoid visual artifacts when the inner frustum is rendered at a lower resolution than specified in the configuration and is smaller on screen than the available pixels on the display device." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_GenerateMips = { "GenerateMips", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraRenderSettings, GenerateMips), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_GenerateMips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_GenerateMips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Advanced render settings\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Advanced render settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_AdvancedRenderSettings = { "AdvancedRenderSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraRenderSettings, AdvancedRenderSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomFrameSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_RenderOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_CustomPostprocess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_bUseCameraComponentPostprocess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_Replace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_PostprocessBlur,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_GenerateMips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::NewProp_AdvancedRenderSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_CameraRenderSettings",
		sizeof(FDisplayClusterConfigurationICVFX_CameraRenderSettings),
		alignof(FDisplayClusterConfigurationICVFX_CameraRenderSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_CameraRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_CameraRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraRenderSettings_Hash() { return 3557961826U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings>()
{
	return FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTargetRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RenderTargetRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GPUIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_GPUIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoGPUIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_StereoGPUIndex;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StereoMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StereoMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderFamilyGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RenderFamilyGroup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.01" },
		{ "Comment", "// Performance: Render to scale RTT, resolved with shader to viewport (Custom value)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance: Render to scale RTT, resolved with shader to viewport (Custom value)" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio = { "RenderTargetRatio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings, RenderTargetRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_GPUIndex_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Performance, Multi-GPU: Asign GPU for viewport rendering. The Value '-1' used to default gpu mapping (EYE_LEFT and EYE_RIGHT GPU)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance, Multi-GPU: Asign GPU for viewport rendering. The Value '-1' used to default gpu mapping (EYE_LEFT and EYE_RIGHT GPU)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_GPUIndex = { "GPUIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings, GPUIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_GPUIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_GPUIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Performance, Multi-GPU: Customize GPU for stereo mode second view (EYE_RIGHT GPU)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance, Multi-GPU: Customize GPU for stereo mode second view (EYE_RIGHT GPU)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex = { "StereoGPUIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings, StereoGPUIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoMode_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Performance: force monoscopic render, resolved to stereo viewport\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance: force monoscopic render, resolved to stereo viewport" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoMode = { "StereoMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings, StereoMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData[] = {
		{ "Comment", "// Experimental: Support special frame builder mode - merge viewports to single viewfamily by group num\n// [not implemented yet]\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Experimental: Support special frame builder mode - merge viewports to single viewfamily by group num\n[not implemented yet]" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup = { "RenderFamilyGroup", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings, RenderFamilyGroup), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_GPUIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_StereoMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings",
		sizeof(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings),
		alignof(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraAdvancedRenderSettings_Hash() { return 1571085177U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_LightcardSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_LightcardSettings"), sizeof(FDisplayClusterConfigurationICVFX_LightcardSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_LightcardSettings>()
{
	return FDisplayClusterConfigurationICVFX_LightcardSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings(FDisplayClusterConfigurationICVFX_LightcardSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_LightcardSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_LightcardSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_LightcardSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_LightcardSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_LightcardSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_LightcardSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Blendingmode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blendingmode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Blendingmode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShowOnlyList_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ShowOnlyList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableOuterViewportOCIO_MetaData[];
#endif
		static void NewProp_bEnableOuterViewportOCIO_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableOuterViewportOCIO;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableOuterViewportColorGrading_MetaData[];
#endif
		static void NewProp_bEnableOuterViewportColorGrading_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableOuterViewportColorGrading;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_LightcardSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnable_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Enable Light Cards */" },
		{ "DisplayName", "Enable Light Cards" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Enable Light Cards" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_LightcardSettings*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_LightcardSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnable_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_Blendingmode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_Blendingmode_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Specify how to render Light Cards in relation to the inner frustum. */" },
		{ "DisplayName", "Blending Mode" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Specify how to render Light Cards in relation to the inner frustum." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_Blendingmode = { "Blendingmode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_LightcardSettings, Blendingmode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_LightcardRenderMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_Blendingmode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_Blendingmode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_ShowOnlyList_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Render actors from this layers to lightcard textures\n" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Render actors from this layers to lightcard textures" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_ShowOnlyList = { "ShowOnlyList", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_LightcardSettings, ShowOnlyList), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_ShowOnlyList_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_ShowOnlyList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_RenderSettings_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Configure global render settings for this viewport\n" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Configure global render settings for this viewport" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_RenderSettings = { "RenderSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_LightcardSettings, RenderSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_RenderSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_RenderSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportOCIO_MetaData[] = {
		{ "Comment", "// Enable using outer viewport OCIO from DCRA for lightcard rendering\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Enable using outer viewport OCIO from DCRA for lightcard rendering" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportOCIO_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_LightcardSettings*)Obj)->bEnableOuterViewportOCIO = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportOCIO = { "bEnableOuterViewportOCIO", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_LightcardSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportOCIO_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportOCIO_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportOCIO_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportColorGrading_MetaData[] = {
		{ "Comment", "// Enable using outer viewport Color Grading from DCRA for lightcard rendering\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Enable using outer viewport Color Grading from DCRA for lightcard rendering" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportColorGrading_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_LightcardSettings*)Obj)->bEnableOuterViewportColorGrading = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportColorGrading = { "bEnableOuterViewportColorGrading", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_LightcardSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportColorGrading_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportColorGrading_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportColorGrading_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_Blendingmode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_Blendingmode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_ShowOnlyList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_RenderSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportOCIO,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::NewProp_bEnableOuterViewportColorGrading,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_LightcardSettings",
		sizeof(FDisplayClusterConfigurationICVFX_LightcardSettings),
		alignof(FDisplayClusterConfigurationICVFX_LightcardSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_LightcardSettings"), sizeof(FDisplayClusterConfigurationICVFX_LightcardSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardSettings_Hash() { return 1106626090U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_LightcardRenderSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_LightcardRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_LightcardRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_LightcardRenderSettings>()
{
	return FDisplayClusterConfigurationICVFX_LightcardRenderSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings(FDisplayClusterConfigurationICVFX_LightcardRenderSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_LightcardRenderSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_LightcardRenderSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_LightcardRenderSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_LightcardRenderSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_LightcardRenderSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_LightcardRenderSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReplaceViewport_MetaData[];
#endif
		static void NewProp_bReplaceViewport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReplaceViewport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Replace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Replace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostprocessBlur_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PostprocessBlur;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateMips_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GenerateMips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvancedRenderSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdvancedRenderSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_LightcardRenderSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_bReplaceViewport_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// override the texture of the target viewport from this lightcard RTT\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "override the texture of the target viewport from this lightcard RTT" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_bReplaceViewport_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_LightcardRenderSettings*)Obj)->bReplaceViewport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_bReplaceViewport = { "bReplaceViewport", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_LightcardRenderSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_bReplaceViewport_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_bReplaceViewport_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_bReplaceViewport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_Replace_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Override viewport render from source texture\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Override viewport render from source texture" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_Replace = { "Replace", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_LightcardRenderSettings, Replace), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_Replace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_Replace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_PostprocessBlur_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_PostprocessBlur = { "PostprocessBlur", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_LightcardRenderSettings, PostprocessBlur), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_PostprocessBlur_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_PostprocessBlur_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_GenerateMips_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_GenerateMips = { "GenerateMips", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_LightcardRenderSettings, GenerateMips), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_GenerateMips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_GenerateMips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Advanced render settings\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Advanced render settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_AdvancedRenderSettings = { "AdvancedRenderSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_LightcardRenderSettings, AdvancedRenderSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_bReplaceViewport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_Replace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_PostprocessBlur,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_GenerateMips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::NewProp_AdvancedRenderSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_LightcardRenderSettings",
		sizeof(FDisplayClusterConfigurationICVFX_LightcardRenderSettings),
		alignof(FDisplayClusterConfigurationICVFX_LightcardRenderSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_LightcardRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_LightcardRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_LightcardRenderSettings_Hash() { return 2880523391U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_ChromakeySettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_ChromakeySettings"), sizeof(FDisplayClusterConfigurationICVFX_ChromakeySettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_ChromakeySettings>()
{
	return FDisplayClusterConfigurationICVFX_ChromakeySettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings(FDisplayClusterConfigurationICVFX_ChromakeySettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_ChromakeySettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeySettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeySettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_ChromakeySettings>(FName(TEXT("DisplayClusterConfigurationICVFX_ChromakeySettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeySettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChromakeyColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChromakeyColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChromakeyRenderTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChromakeyRenderTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChromakeyMarkers_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChromakeyMarkers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_ChromakeySettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_bEnable_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Set to True to fill the inner frustum with the specified Chromakey Color. */" },
		{ "DisplayName", "Enable Inner Frustum Chromakey" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Set to True to fill the inner frustum with the specified Chromakey Color." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_ChromakeySettings*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_ChromakeySettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_bEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyColor_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Chromakey Color */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Chromakey Color" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyColor = { "ChromakeyColor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeySettings, ChromakeyColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyRenderTexture_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Configure a custom chromakey based on content that will appear in the inner frustum, rather than the entire inner frustum. */" },
		{ "DisplayName", "Custom Chromakey" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Configure a custom chromakey based on content that will appear in the inner frustum, rather than the entire inner frustum." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyRenderTexture = { "ChromakeyRenderTexture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeySettings, ChromakeyRenderTexture), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyRenderTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyRenderTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyMarkers_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Display Chromakey Markers to facilitate camera tracking in post production. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Display Chromakey Markers to facilitate camera tracking in post production." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyMarkers = { "ChromakeyMarkers", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeySettings, ChromakeyMarkers), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyMarkers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyMarkers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyRenderTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::NewProp_ChromakeyMarkers,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_ChromakeySettings",
		sizeof(FDisplayClusterConfigurationICVFX_ChromakeySettings),
		alignof(FDisplayClusterConfigurationICVFX_ChromakeySettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_ChromakeySettings"), sizeof(FDisplayClusterConfigurationICVFX_ChromakeySettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeySettings_Hash() { return 2400582667U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_ChromakeyMarkers::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_ChromakeyMarkers"), sizeof(FDisplayClusterConfigurationICVFX_ChromakeyMarkers), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_ChromakeyMarkers>()
{
	return FDisplayClusterConfigurationICVFX_ChromakeyMarkers::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers(FDisplayClusterConfigurationICVFX_ChromakeyMarkers::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_ChromakeyMarkers"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeyMarkers
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeyMarkers()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_ChromakeyMarkers>(FName(TEXT("DisplayClusterConfigurationICVFX_ChromakeyMarkers")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeyMarkers;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarkerColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MarkerColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarkerTileRGBA_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MarkerTileRGBA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarkerSizeScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MarkerSizeScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarkerTileDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MarkerTileDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarkerTileOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MarkerTileOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_ChromakeyMarkers>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_bEnable_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** True to display Chromakey Markers within the inner frustum. */" },
		{ "DisplayName", "Enable Chromakey Markers" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "True to display Chromakey Markers within the inner frustum." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_ChromakeyMarkers*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_ChromakeyMarkers), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_bEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerColor_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Marker Color */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Marker Color" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerColor = { "MarkerColor", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyMarkers, MarkerColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileRGBA_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Texture to use as the chromakey marker tile. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Texture to use as the chromakey marker tile." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileRGBA = { "MarkerTileRGBA", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyMarkers, MarkerTileRGBA), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileRGBA_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileRGBA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerSizeScale_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Scale value for the size of each chromakey marker tile. */" },
		{ "DisplayName", "Marker Scale" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Scale value for the size of each chromakey marker tile." },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerSizeScale = { "MarkerSizeScale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyMarkers, MarkerSizeScale), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerSizeScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerSizeScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileDistance_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Distance value between each chromakey marker tile. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Distance value between each chromakey marker tile." },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileDistance = { "MarkerTileDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyMarkers, MarkerTileDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileOffset_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Offset value for the chromakey marker tiles, normalized to the tile distance.  Adjust placement of the chromakey markers within the composition of the camera framing.  Whole numbers will offset chromakey markers by a cyclical amount and have no visual change. */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Offset value for the chromakey marker tiles, normalized to the tile distance.  Adjust placement of the chromakey markers within the composition of the camera framing.  Whole numbers will offset chromakey markers by a cyclical amount and have no visual change." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileOffset = { "MarkerTileOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyMarkers, MarkerTileOffset), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileRGBA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerSizeScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::NewProp_MarkerTileOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_ChromakeyMarkers",
		sizeof(FDisplayClusterConfigurationICVFX_ChromakeyMarkers),
		alignof(FDisplayClusterConfigurationICVFX_ChromakeyMarkers),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_ChromakeyMarkers"), sizeof(FDisplayClusterConfigurationICVFX_ChromakeyMarkers), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyMarkers_Hash() { return 1132413051U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_ChromakeyRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings>()
{
	return FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_ChromakeyRenderSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeyRenderSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeyRenderSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_ChromakeyRenderSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_ChromakeyRenderSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReplaceCameraViewport_MetaData[];
#endif
		static void NewProp_bReplaceCameraViewport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReplaceCameraViewport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShowOnlyList_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ShowOnlyList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Replace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Replace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostprocessBlur_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PostprocessBlur;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateMips_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GenerateMips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvancedRenderSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdvancedRenderSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bEnable_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Set to True to use custom chromakey content. */" },
		{ "DisplayName", "Use Custom Chromakey" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Set to True to use custom chromakey content." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bReplaceCameraViewport_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Replace the texture of the camera viewport from this chromakey RTT\n" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Replace the texture of the camera viewport from this chromakey RTT" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bReplaceCameraViewport_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings*)Obj)->bReplaceCameraViewport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bReplaceCameraViewport = { "bReplaceCameraViewport", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bReplaceCameraViewport_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bReplaceCameraViewport_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bReplaceCameraViewport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_CustomSize_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Performance: Use custom size (low-res) for chromakey RTT frame. Default size same as camera frame\n" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance: Use custom size (low-res) for chromakey RTT frame. Default size same as camera frame" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_CustomSize = { "CustomSize", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, CustomSize), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_CustomSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_CustomSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_ShowOnlyList_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Content specified here will be overridden to use the chromakey color specified and include chromakey markers if enabled. */" },
		{ "DisplayName", "Custom Chromakey Content" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Content specified here will be overridden to use the chromakey color specified and include chromakey markers if enabled." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_ShowOnlyList = { "ShowOnlyList", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, ShowOnlyList), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_ShowOnlyList_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_ShowOnlyList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_Replace_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Replace viewport render from source texture\n" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Replace viewport render from source texture" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_Replace = { "Replace", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, Replace), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_Replace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_Replace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_PostprocessBlur_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Apply blur to the Custom Chromakey content. */" },
		{ "DisplayName", "Post Process Blur" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Apply blur to the Custom Chromakey content." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_PostprocessBlur = { "PostprocessBlur", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, PostprocessBlur), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_PostprocessBlur_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_PostprocessBlur_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_GenerateMips_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_GenerateMips = { "GenerateMips", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, GenerateMips), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_GenerateMips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_GenerateMips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Advanced render settings\n" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Advanced render settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_AdvancedRenderSettings = { "AdvancedRenderSettings", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings, AdvancedRenderSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_AdvancedRenderSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_bReplaceCameraViewport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_CustomSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_ShowOnlyList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_Replace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_PostprocessBlur,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_GenerateMips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::NewProp_AdvancedRenderSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_ChromakeyRenderSettings",
		sizeof(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings),
		alignof(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_ChromakeyRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_ChromakeyRenderSettings_Hash() { return 583907606U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings>()
{
	return FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings>(FName(TEXT("DisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BufferRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTargetRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RenderTargetRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GPUIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_GPUIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoGPUIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_StereoGPUIndex;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StereoMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StereoMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderFamilyGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RenderFamilyGroup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_BufferRatio_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.05" },
		{ "Comment", "// Allow ScreenPercentage \n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Allow ScreenPercentage" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, BufferRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_BufferRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_BufferRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.01" },
		{ "Comment", "// Performance: Render to scale RTT, resolved with shader to viewport (Custom value)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance: Render to scale RTT, resolved with shader to viewport (Custom value)" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio = { "RenderTargetRatio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, RenderTargetRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_GPUIndex_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Performance, Multi-GPU: Asign GPU for viewport rendering. The Value '-1' used to default gpu mapping (EYE_LEFT and EYE_RIGHT GPU)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance, Multi-GPU: Asign GPU for viewport rendering. The Value '-1' used to default gpu mapping (EYE_LEFT and EYE_RIGHT GPU)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_GPUIndex = { "GPUIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, GPUIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_GPUIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_GPUIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Performance, Multi-GPU: Customize GPU for stereo mode second view (EYE_RIGHT GPU)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance, Multi-GPU: Customize GPU for stereo mode second view (EYE_RIGHT GPU)" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex = { "StereoGPUIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, StereoGPUIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoMode_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Performance: force monoscopic render, resolved to stereo viewport\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Performance: force monoscopic render, resolved to stereo viewport" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoMode = { "StereoMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, StereoMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData[] = {
		{ "Comment", "// Experimental: Support special frame builder mode - merge viewports to single viewfamily by group num\n// [not implemented yet]\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Experimental: Support special frame builder mode - merge viewports to single viewfamily by group num\n[not implemented yet]" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup = { "RenderFamilyGroup", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings, RenderFamilyGroup), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderTargetRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_GPUIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoGPUIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_StereoMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::NewProp_RenderFamilyGroup,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings",
		sizeof(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings),
		alignof(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings"), sizeof(FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_OverlayAdvancedRenderSettings_Hash() { return 2101694713U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_Size::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_Size"), sizeof(FDisplayClusterConfigurationICVFX_Size), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_Size>()
{
	return FDisplayClusterConfigurationICVFX_Size::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size(FDisplayClusterConfigurationICVFX_Size::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_Size"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_Size
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_Size()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_Size>(FName(TEXT("DisplayClusterConfigurationICVFX_Size")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_Size;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Height;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_Size>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "32" },
		{ "Comment", "// Viewport width in pixels\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Viewport width in pixels" },
		{ "UIMin", "32" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_Size, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "32" },
		{ "Comment", "// Viewport height  in pixels\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Viewport height  in pixels" },
		{ "UIMin", "32" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_Size, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Height_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::NewProp_Height,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_Size",
		sizeof(FDisplayClusterConfigurationICVFX_Size),
		alignof(FDisplayClusterConfigurationICVFX_Size),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_Size"), sizeof(FDisplayClusterConfigurationICVFX_Size), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_Size_Hash() { return 2837235453U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_CustomSize::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_CustomSize"), sizeof(FDisplayClusterConfigurationICVFX_CustomSize), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_CustomSize>()
{
	return FDisplayClusterConfigurationICVFX_CustomSize::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize(FDisplayClusterConfigurationICVFX_CustomSize::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_CustomSize"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CustomSize
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CustomSize()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_CustomSize>(FName(TEXT("DisplayClusterConfigurationICVFX_CustomSize")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_CustomSize;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCustomSize_MetaData[];
#endif
		static void NewProp_bUseCustomSize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCustomSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CustomWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_CustomHeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_CustomSize>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_bUseCustomSize_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Use custom size\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Use custom size" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_bUseCustomSize_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationICVFX_CustomSize*)Obj)->bUseCustomSize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_bUseCustomSize = { "bUseCustomSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationICVFX_CustomSize), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_bUseCustomSize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_bUseCustomSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_bUseCustomSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomWidth_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "32" },
		{ "Comment", "// Used when enabled \"bUseCustomSize\"\n" },
		{ "EditCondition", "bUseCustomSize" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Used when enabled \"bUseCustomSize\"" },
		{ "UIMin", "32" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomWidth = { "CustomWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CustomSize, CustomWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomHeight_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "32" },
		{ "Comment", "// Used when enabled \"bUseCustomSize\"\n" },
		{ "EditCondition", "bUseCustomSize" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Used when enabled \"bUseCustomSize\"" },
		{ "UIMin", "32" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomHeight = { "CustomHeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_CustomSize, CustomHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomHeight_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_bUseCustomSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::NewProp_CustomHeight,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_CustomSize",
		sizeof(FDisplayClusterConfigurationICVFX_CustomSize),
		alignof(FDisplayClusterConfigurationICVFX_CustomSize),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_CustomSize"), sizeof(FDisplayClusterConfigurationICVFX_CustomSize), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CustomSize_Hash() { return 3793006626U; }
class UScriptStruct* FDisplayClusterConfigurationICVFX_VisibilityList::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationICVFX_VisibilityList"), sizeof(FDisplayClusterConfigurationICVFX_VisibilityList), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationICVFX_VisibilityList>()
{
	return FDisplayClusterConfigurationICVFX_VisibilityList::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList(FDisplayClusterConfigurationICVFX_VisibilityList::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationICVFX_VisibilityList"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_VisibilityList
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_VisibilityList()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationICVFX_VisibilityList>(FName(TEXT("DisplayClusterConfigurationICVFX_VisibilityList")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationICVFX_VisibilityList;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActorLayers_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorLayers_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActorLayers;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_Actors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actors;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RootActorComponentNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootActorComponentNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RootActorComponentNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationICVFX_VisibilityList>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_ActorLayers_Inner = { "ActorLayers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FActorLayer, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_ActorLayers_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Actor Layers */" },
		{ "DisplayName", "Layers" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Actor Layers" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_ActorLayers = { "ActorLayers", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_VisibilityList, ActorLayers), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_ActorLayers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_ActorLayers_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_Actors_Inner = { "Actors", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_Actors_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Actor references */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Actor references" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_Actors = { "Actors", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_VisibilityList, Actors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_Actors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_Actors_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_RootActorComponentNames_Inner = { "RootActorComponentNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_RootActorComponentNames_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Reference to RootActor components by names */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_ICVFX.h" },
		{ "ToolTip", "Reference to RootActor components by names" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_RootActorComponentNames = { "RootActorComponentNames", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationICVFX_VisibilityList, RootActorComponentNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_RootActorComponentNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_RootActorComponentNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_ActorLayers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_ActorLayers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_Actors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_Actors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_RootActorComponentNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::NewProp_RootActorComponentNames,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationICVFX_VisibilityList",
		sizeof(FDisplayClusterConfigurationICVFX_VisibilityList),
		alignof(FDisplayClusterConfigurationICVFX_VisibilityList),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationICVFX_VisibilityList"), sizeof(FDisplayClusterConfigurationICVFX_VisibilityList), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_VisibilityList_Hash() { return 3335312813U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
