// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_OutputRemap.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_OutputRemap() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationFramePostProcess_OutputRemapSource();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
// End Cross Module References
	static UEnum* EDisplayClusterConfigurationFramePostProcess_OutputRemapSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationFramePostProcess_OutputRemapSource, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("EDisplayClusterConfigurationFramePostProcess_OutputRemapSource"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<EDisplayClusterConfigurationFramePostProcess_OutputRemapSource>()
	{
		return EDisplayClusterConfigurationFramePostProcess_OutputRemapSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDisplayClusterConfigurationFramePostProcess_OutputRemapSource(EDisplayClusterConfigurationFramePostProcess_OutputRemapSource_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("EDisplayClusterConfigurationFramePostProcess_OutputRemapSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationFramePostProcess_OutputRemapSource_Hash() { return 1456598506U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationFramePostProcess_OutputRemapSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDisplayClusterConfigurationFramePostProcess_OutputRemapSource"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationFramePostProcess_OutputRemapSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::StaticMesh", (int64)EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::StaticMesh },
				{ "EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::ExternalFile", (int64)EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::ExternalFile },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/* Source types of the output remapping */" },
				{ "ExternalFile.Comment", "/** Use an external .obj file for output remapping when the Data Source is set to File */" },
				{ "ExternalFile.DisplayName", "External File" },
				{ "ExternalFile.Name", "EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::ExternalFile" },
				{ "ExternalFile.ToolTip", "Use an external .obj file for output remapping when the Data Source is set to File" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OutputRemap.h" },
				{ "StaticMesh.Comment", "/** Use a Static Mesh reference for output remapping when the Data Source is set to Mesh */" },
				{ "StaticMesh.DisplayName", "Static Mesh" },
				{ "StaticMesh.Name", "EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::StaticMesh" },
				{ "StaticMesh.ToolTip", "Use a Static Mesh reference for output remapping when the Data Source is set to Mesh" },
				{ "ToolTip", "Source types of the output remapping" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"EDisplayClusterConfigurationFramePostProcess_OutputRemapSource",
				"EDisplayClusterConfigurationFramePostProcess_OutputRemapSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FDisplayClusterConfigurationFramePostProcess_OutputRemap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationFramePostProcess_OutputRemap"), sizeof(FDisplayClusterConfigurationFramePostProcess_OutputRemap), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationFramePostProcess_OutputRemap>()
{
	return FDisplayClusterConfigurationFramePostProcess_OutputRemap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap(FDisplayClusterConfigurationFramePostProcess_OutputRemap::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationFramePostProcess_OutputRemap"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationFramePostProcess_OutputRemap
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationFramePostProcess_OutputRemap()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationFramePostProcess_OutputRemap>(FName(TEXT("DisplayClusterConfigurationFramePostProcess_OutputRemap")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationFramePostProcess_OutputRemap;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataSource_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DataSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExternalFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ExternalFile;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* Screen space remapping of the final backbuffer output. Applied at the whole window */" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OutputRemap.h" },
		{ "ToolTip", "Screen space remapping of the final backbuffer output. Applied at the whole window" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationFramePostProcess_OutputRemap>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_bEnable_MetaData[] = {
		{ "Category", "NDisplay OutputRemap" },
		{ "Comment", "/** Enables or disables output remapping */" },
		{ "DisplayName", "Enable Output Remapping" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OutputRemap.h" },
		{ "ToolTip", "Enables or disables output remapping" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationFramePostProcess_OutputRemap*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationFramePostProcess_OutputRemap), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_bEnable_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_DataSource_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_DataSource_MetaData[] = {
		{ "Category", "NDisplay OutputRemap" },
		{ "Comment", "/** Selects either the Static Mesh or External File setting as the source for output remapping */" },
		{ "EditCondition", "bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OutputRemap.h" },
		{ "ToolTip", "Selects either the Static Mesh or External File setting as the source for output remapping" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_DataSource = { "DataSource", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationFramePostProcess_OutputRemap, DataSource), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationFramePostProcess_OutputRemapSource, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_DataSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_DataSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_StaticMesh_MetaData[] = {
		{ "Category", "NDisplay OutputRemap" },
		{ "Comment", "/** The Static Mesh reference to use for output remapping when the Data Source is set to Static Mesh */" },
		{ "EditCondition", "DataSource == EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::StaticMesh && bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OutputRemap.h" },
		{ "ToolTip", "The Static Mesh reference to use for output remapping when the Data Source is set to Static Mesh" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_StaticMesh = { "StaticMesh", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationFramePostProcess_OutputRemap, StaticMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_StaticMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_StaticMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_ExternalFile_MetaData[] = {
		{ "Category", "NDisplay OutputRemap" },
		{ "Comment", "/** The external .obj file to use for output remapping when the Data Source is set to File */" },
		{ "EditCondition", "DataSource == EDisplayClusterConfigurationFramePostProcess_OutputRemapSource::ExternalFile && bEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OutputRemap.h" },
		{ "ToolTip", "The external .obj file to use for output remapping when the Data Source is set to File" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_ExternalFile = { "ExternalFile", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationFramePostProcess_OutputRemap, ExternalFile), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_ExternalFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_ExternalFile_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_DataSource_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_DataSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_StaticMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::NewProp_ExternalFile,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationFramePostProcess_OutputRemap",
		sizeof(FDisplayClusterConfigurationFramePostProcess_OutputRemap),
		alignof(FDisplayClusterConfigurationFramePostProcess_OutputRemap),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationFramePostProcess_OutputRemap"), sizeof(FDisplayClusterConfigurationFramePostProcess_OutputRemap), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap_Hash() { return 2109120885U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
