// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterStageMonitoring/Private/NvidiaSyncWatchdog.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNvidiaSyncWatchdog() {}
// Cross Module References
	DISPLAYCLUSTERSTAGEMONITORING_API UScriptStruct* Z_Construct_UScriptStruct_FNvidiaSyncEvent();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterStageMonitoring();
	STAGEDATACORE_API UScriptStruct* Z_Construct_UScriptStruct_FStageProviderEventMessage();
// End Cross Module References

static_assert(std::is_polymorphic<FNvidiaSyncEvent>() == std::is_polymorphic<FStageProviderEventMessage>(), "USTRUCT FNvidiaSyncEvent cannot be polymorphic unless super FStageProviderEventMessage is polymorphic");

class UScriptStruct* FNvidiaSyncEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERSTAGEMONITORING_API uint32 Get_Z_Construct_UScriptStruct_FNvidiaSyncEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNvidiaSyncEvent, Z_Construct_UPackage__Script_DisplayClusterStageMonitoring(), TEXT("NvidiaSyncEvent"), sizeof(FNvidiaSyncEvent), Get_Z_Construct_UScriptStruct_FNvidiaSyncEvent_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERSTAGEMONITORING_API UScriptStruct* StaticStruct<FNvidiaSyncEvent>()
{
	return FNvidiaSyncEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNvidiaSyncEvent(FNvidiaSyncEvent::StaticStruct, TEXT("/Script/DisplayClusterStageMonitoring"), TEXT("NvidiaSyncEvent"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterStageMonitoring_StaticRegisterNativesFNvidiaSyncEvent
{
	FScriptStruct_DisplayClusterStageMonitoring_StaticRegisterNativesFNvidiaSyncEvent()
	{
		UScriptStruct::DeferCppStructOps<FNvidiaSyncEvent>(FName(TEXT("NvidiaSyncEvent")));
	}
} ScriptStruct_DisplayClusterStageMonitoring_StaticRegisterNativesFNvidiaSyncEvent;
	struct Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MissedFrames_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MissedFrames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastFrameDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LastFrameDuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SynchronizationDuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SynchronizationDuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/NvidiaSyncWatchdog.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNvidiaSyncEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_MissedFrames_MetaData[] = {
		{ "Category", "NvidiaSync" },
		{ "Comment", "/** Number of sync counts that were missed between presents */" },
		{ "ModuleRelativePath", "Private/NvidiaSyncWatchdog.h" },
		{ "ToolTip", "Number of sync counts that were missed between presents" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_MissedFrames = { "MissedFrames", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNvidiaSyncEvent, MissedFrames), METADATA_PARAMS(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_MissedFrames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_MissedFrames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_LastFrameDuration_MetaData[] = {
		{ "Category", "NvidiaSync" },
		{ "ModuleRelativePath", "Private/NvidiaSyncWatchdog.h" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_LastFrameDuration = { "LastFrameDuration", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNvidiaSyncEvent, LastFrameDuration), METADATA_PARAMS(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_LastFrameDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_LastFrameDuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_SynchronizationDuration_MetaData[] = {
		{ "Category", "NvidiaSync" },
		{ "ModuleRelativePath", "Private/NvidiaSyncWatchdog.h" },
		{ "Unit", "ms" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_SynchronizationDuration = { "SynchronizationDuration", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNvidiaSyncEvent, SynchronizationDuration), METADATA_PARAMS(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_SynchronizationDuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_SynchronizationDuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_MissedFrames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_LastFrameDuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::NewProp_SynchronizationDuration,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterStageMonitoring,
		Z_Construct_UScriptStruct_FStageProviderEventMessage,
		&NewStructOps,
		"NvidiaSyncEvent",
		sizeof(FNvidiaSyncEvent),
		alignof(FNvidiaSyncEvent),
		Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNvidiaSyncEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNvidiaSyncEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterStageMonitoring();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NvidiaSyncEvent"), sizeof(FNvidiaSyncEvent), Get_Z_Construct_UScriptStruct_FNvidiaSyncEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNvidiaSyncEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNvidiaSyncEvent_Hash() { return 1726809493U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
