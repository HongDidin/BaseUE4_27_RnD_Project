// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERMESSAGEINTERCEPTION_DisplayClusterMessageInterceptionSettings_generated_h
#error "DisplayClusterMessageInterceptionSettings.generated.h already included, missing '#pragma once' in DisplayClusterMessageInterceptionSettings.h"
#endif
#define DISPLAYCLUSTERMESSAGEINTERCEPTION_DisplayClusterMessageInterceptionSettings_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics; \
	DISPLAYCLUSTERMESSAGEINTERCEPTION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERMESSAGEINTERCEPTION_API UScriptStruct* StaticStruct<struct FMessageInterceptionSettings>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterMessageInterceptionSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterMessageInterceptionSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterMessageInterception"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterMessageInterceptionSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterMessageInterceptionSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterMessageInterceptionSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterMessageInterception"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterMessageInterceptionSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterMessageInterceptionSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterMessageInterceptionSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterMessageInterceptionSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterMessageInterceptionSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterMessageInterceptionSettings(UDisplayClusterMessageInterceptionSettings&&); \
	NO_API UDisplayClusterMessageInterceptionSettings(const UDisplayClusterMessageInterceptionSettings&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterMessageInterceptionSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterMessageInterceptionSettings(UDisplayClusterMessageInterceptionSettings&&); \
	NO_API UDisplayClusterMessageInterceptionSettings(const UDisplayClusterMessageInterceptionSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterMessageInterceptionSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterMessageInterceptionSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterMessageInterceptionSettings)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_34_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERMESSAGEINTERCEPTION_API UClass* StaticClass<class UDisplayClusterMessageInterceptionSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterMessageInterceptor_Public_DisplayClusterMessageInterceptionSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
