// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterEditor/Private/DisplayClusterEditorEngine.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterEditorEngine() {}
// Cross Module References
	DISPLAYCLUSTEREDITOR_API UClass* Z_Construct_UClass_UDisplayClusterEditorEngine_NoRegister();
	DISPLAYCLUSTEREDITOR_API UClass* Z_Construct_UClass_UDisplayClusterEditorEngine();
	UNREALED_API UClass* Z_Construct_UClass_UUnrealEdEngine();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterEditor();
// End Cross Module References
	void UDisplayClusterEditorEngine::StaticRegisterNativesUDisplayClusterEditorEngine()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterEditorEngine_NoRegister()
	{
		return UDisplayClusterEditorEngine::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterEditorEngine_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterEditorEngine_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUnrealEdEngine,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterEditorEngine_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Extended editor engine\n */" },
		{ "IncludePath", "DisplayClusterEditorEngine.h" },
		{ "ModuleRelativePath", "Private/DisplayClusterEditorEngine.h" },
		{ "ToolTip", "Extended editor engine" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterEditorEngine_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterEditorEngine>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterEditorEngine_Statics::ClassParams = {
		&UDisplayClusterEditorEngine::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008000AEu,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterEditorEngine_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterEditorEngine_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterEditorEngine()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterEditorEngine_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterEditorEngine, 2716414058);
	template<> DISPLAYCLUSTEREDITOR_API UClass* StaticClass<UDisplayClusterEditorEngine>()
	{
		return UDisplayClusterEditorEngine::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterEditorEngine(Z_Construct_UClass_UDisplayClusterEditorEngine, &UDisplayClusterEditorEngine::StaticClass, TEXT("/Script/DisplayClusterEditor"), TEXT("UDisplayClusterEditorEngine"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterEditorEngine);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
