// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_IDisplayClusterComponent_generated_h
#error "IDisplayClusterComponent.generated.h already included, missing '#pragma once' in IDisplayClusterComponent.h"
#endif
#define DISPLAYCLUSTER_IDisplayClusterComponent_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DISPLAYCLUSTER_API UDisplayClusterComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DISPLAYCLUSTER_API, UDisplayClusterComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DISPLAYCLUSTER_API UDisplayClusterComponent(UDisplayClusterComponent&&); \
	DISPLAYCLUSTER_API UDisplayClusterComponent(const UDisplayClusterComponent&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DISPLAYCLUSTER_API UDisplayClusterComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DISPLAYCLUSTER_API UDisplayClusterComponent(UDisplayClusterComponent&&); \
	DISPLAYCLUSTER_API UDisplayClusterComponent(const UDisplayClusterComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DISPLAYCLUSTER_API, UDisplayClusterComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterComponent); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUDisplayClusterComponent(); \
	friend struct Z_Construct_UClass_UDisplayClusterComponent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterComponent, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/DisplayCluster"), DISPLAYCLUSTER_API) \
	DECLARE_SERIALIZER(UDisplayClusterComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IDisplayClusterComponent() {} \
public: \
	typedef UDisplayClusterComponent UClassType; \
	typedef IDisplayClusterComponent ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_INCLASS_IINTERFACE \
protected: \
	virtual ~IDisplayClusterComponent() {} \
public: \
	typedef UDisplayClusterComponent UClassType; \
	typedef IDisplayClusterComponent ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_12_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_IDisplayClusterComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
