// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Components/DisplayClusterICVFXCameraComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterICVFXCameraComponent() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterICVFXCameraComponent();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings();
// End Cross Module References
	void UDisplayClusterICVFXCameraComponent::StaticRegisterNativesUDisplayClusterICVFXCameraComponent()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_NoRegister()
	{
		return UDisplayClusterICVFXCameraComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCineCameraComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "DisplayCluster" },
		{ "Comment", "/**\n * nDisplay in-camera VFX camera representation\n */" },
		{ "DisplayName", "ICVFX Camera" },
		{ "HideCategories", "CameraSettings Mobility Rendering LOD Trigger PhysicsVolume" },
		{ "HideFunctions", "SetFieldOfView SetAspectRatio" },
		{ "IncludePath", "Components/DisplayClusterICVFXCameraComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterICVFXCameraComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "nDisplay in-camera VFX camera representation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::NewProp_CameraSettings_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterICVFXCameraComponent.h" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::NewProp_CameraSettings = { "CameraSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterICVFXCameraComponent, CameraSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_CameraSettings, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::NewProp_CameraSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::NewProp_CameraSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::NewProp_CameraSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterICVFXCameraComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::ClassParams = {
		&UDisplayClusterICVFXCameraComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterICVFXCameraComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterICVFXCameraComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterICVFXCameraComponent, 528781080);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterICVFXCameraComponent>()
	{
		return UDisplayClusterICVFXCameraComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterICVFXCameraComponent(Z_Construct_UClass_UDisplayClusterICVFXCameraComponent, &UDisplayClusterICVFXCameraComponent::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterICVFXCameraComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterICVFXCameraComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
