// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterClusterEvent_generated_h
#error "DisplayClusterClusterEvent.generated.h already included, missing '#pragma once' in DisplayClusterClusterEvent.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterClusterEvent_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Cluster_DisplayClusterClusterEvent_h_81_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary_Statics; \
	DISPLAYCLUSTER_API static class UScriptStruct* StaticStruct(); \
	typedef FDisplayClusterClusterEventBase Super;


template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<struct FDisplayClusterClusterEventBinary>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Cluster_DisplayClusterClusterEvent_h_44_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson_Statics; \
	DISPLAYCLUSTER_API static class UScriptStruct* StaticStruct(); \
	typedef FDisplayClusterClusterEventBase Super;


template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<struct FDisplayClusterClusterEventJson>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Cluster_DisplayClusterClusterEvent_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterClusterEventBase_Statics; \
	DISPLAYCLUSTER_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<struct FDisplayClusterClusterEventBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Cluster_DisplayClusterClusterEvent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
