// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERSHADERS_MPCDIGeometryData_generated_h
#error "MPCDIGeometryData.generated.h already included, missing '#pragma once' in MPCDIGeometryData.h"
#endif
#define DISPLAYCLUSTERSHADERS_MPCDIGeometryData_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterShaders_Public_Blueprints_MPCDIGeometryData_h_29_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics; \
	DISPLAYCLUSTERSHADERS_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERSHADERS_API UScriptStruct* StaticStruct<struct FMPCDIGeometryExportData>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterShaders_Public_Blueprints_MPCDIGeometryData_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics; \
	DISPLAYCLUSTERSHADERS_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERSHADERS_API UScriptStruct* StaticStruct<struct FMPCDIGeometryImportData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterShaders_Public_Blueprints_MPCDIGeometryData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
