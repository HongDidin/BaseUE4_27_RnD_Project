// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class IDisplayClusterProjectionBlueprintAPI;
#ifdef DISPLAYCLUSTERPROJECTION_DisplayClusterProjectionBlueprintLib_generated_h
#error "DisplayClusterProjectionBlueprintLib.generated.h already included, missing '#pragma once' in DisplayClusterProjectionBlueprintLib.h"
#endif
#define DISPLAYCLUSTERPROJECTION_DisplayClusterProjectionBlueprintLib_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAPI);


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAPI);


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterProjectionBlueprintLib(); \
	friend struct Z_Construct_UClass_UDisplayClusterProjectionBlueprintLib_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterProjectionBlueprintLib, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterProjection"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterProjectionBlueprintLib)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterProjectionBlueprintLib(); \
	friend struct Z_Construct_UClass_UDisplayClusterProjectionBlueprintLib_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterProjectionBlueprintLib, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterProjection"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterProjectionBlueprintLib)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterProjectionBlueprintLib(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterProjectionBlueprintLib) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterProjectionBlueprintLib); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterProjectionBlueprintLib); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterProjectionBlueprintLib(UDisplayClusterProjectionBlueprintLib&&); \
	NO_API UDisplayClusterProjectionBlueprintLib(const UDisplayClusterProjectionBlueprintLib&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterProjectionBlueprintLib(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterProjectionBlueprintLib(UDisplayClusterProjectionBlueprintLib&&); \
	NO_API UDisplayClusterProjectionBlueprintLib(const UDisplayClusterProjectionBlueprintLib&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterProjectionBlueprintLib); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterProjectionBlueprintLib); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterProjectionBlueprintLib)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_14_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class DisplayClusterProjectionBlueprintLib."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERPROJECTION_API UClass* StaticClass<class UDisplayClusterProjectionBlueprintLib>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterProjection_Public_Blueprints_DisplayClusterProjectionBlueprintLib_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
