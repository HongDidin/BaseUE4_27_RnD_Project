// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_Viewport.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_Viewport() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMGPUMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderFamilyMode();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_CustomPostprocess();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewportOverscanMode();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationViewport_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationViewport();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_Base();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle();
// End Cross Module References
class UScriptStruct* FDisplayClusterConfigurationViewportPreview::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationViewportPreview"), sizeof(FDisplayClusterConfigurationViewportPreview), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationViewportPreview>()
{
	return FDisplayClusterConfigurationViewportPreview::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationViewportPreview(FDisplayClusterConfigurationViewportPreview::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationViewportPreview"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewportPreview
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewportPreview()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationViewportPreview>(FName(TEXT("DisplayClusterConfigurationViewportPreview")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewportPreview;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnable_MetaData[];
#endif
		static void NewProp_bEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewNodeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PreviewNodeId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TickPerFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TickPerFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewRenderTargetRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreviewRenderTargetRatioMult;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationViewportPreview>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_bEnable_MetaData[] = {
		{ "Comment", "// Allow preview render\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Allow preview render" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_bEnable_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationViewportPreview*)Obj)->bEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_bEnable = { "bEnable", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationViewportPreview), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_bEnable_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_bEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_bEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewNodeId_MetaData[] = {
		{ "Comment", "// Render single node preview or whole cluster\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Render single node preview or whole cluster" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewNodeId = { "PreviewNodeId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewportPreview, PreviewNodeId), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewNodeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewNodeId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_TickPerFrame_MetaData[] = {
		{ "Comment", "// Update preview texture period in tick\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Update preview texture period in tick" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_TickPerFrame = { "TickPerFrame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewportPreview, TickPerFrame), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_TickPerFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_TickPerFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewRenderTargetRatioMult_MetaData[] = {
		{ "Comment", "// Preview texture size get from viewport, and scaled by this value\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Preview texture size get from viewport, and scaled by this value" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewRenderTargetRatioMult = { "PreviewRenderTargetRatioMult", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewportPreview, PreviewRenderTargetRatioMult), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewRenderTargetRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewRenderTargetRatioMult_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_bEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewNodeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_TickPerFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::NewProp_PreviewRenderTargetRatioMult,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationViewportPreview",
		sizeof(FDisplayClusterConfigurationViewportPreview),
		alignof(FDisplayClusterConfigurationViewportPreview),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationViewportPreview"), sizeof(FDisplayClusterConfigurationViewportPreview), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewportPreview_Hash() { return 2451544443U; }
class UScriptStruct* FDisplayClusterConfigurationRenderFrame::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationRenderFrame"), sizeof(FDisplayClusterConfigurationRenderFrame), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationRenderFrame>()
{
	return FDisplayClusterConfigurationRenderFrame::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationRenderFrame(FDisplayClusterConfigurationRenderFrame::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationRenderFrame"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRenderFrame
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRenderFrame()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationRenderFrame>(FName(TEXT("DisplayClusterConfigurationRenderFrame")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRenderFrame;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MultiGPUMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MultiGPUMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MultiGPUMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowRenderTargetAtlasing_MetaData[];
#endif
		static void NewProp_bAllowRenderTargetAtlasing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowRenderTargetAtlasing;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ViewFamilyMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewFamilyMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ViewFamilyMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldUseParentViewportRenderFamily_MetaData[];
#endif
		static void NewProp_bShouldUseParentViewportRenderFamily_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldUseParentViewportRenderFamily;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterRenderTargetRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterRenderTargetRatioMult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterICVFXInnerViewportRenderTargetRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterICVFXInnerViewportRenderTargetRatioMult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterICVFXOuterViewportRenderTargetRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterICVFXOuterViewportRenderTargetRatioMult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterBufferRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterBufferRatioMult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterICVFXInnerFrustumBufferRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterICVFXInnerFrustumBufferRatioMult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterICVFXOuterViewportBufferRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterICVFXOuterViewportBufferRatioMult;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowWarpBlend_MetaData[];
#endif
		static void NewProp_bAllowWarpBlend_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowWarpBlend;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// This struct now stored in UDisplayClusterConfigurationData, and replicated with MultiUser\n" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "This struct now stored in UDisplayClusterConfigurationData, and replicated with MultiUser" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationRenderFrame>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_MultiGPUMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_MultiGPUMode_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "// Performance: Allow change global MGPU settings\n" },
		{ "DisplayName", "Multi GPU Mode" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Performance: Allow change global MGPU settings" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_MultiGPUMode = { "MultiGPUMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, MultiGPUMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMGPUMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_MultiGPUMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_MultiGPUMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowRenderTargetAtlasing_MetaData[] = {
		{ "Comment", "// Performance: Allow merge multiple viewports on single RTT with atlasing (required for bAllowViewFamilyMergeOptimization)\n// [not implemented yet] Experimental\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Performance: Allow merge multiple viewports on single RTT with atlasing (required for bAllowViewFamilyMergeOptimization)\n[not implemented yet] Experimental" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowRenderTargetAtlasing_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationRenderFrame*)Obj)->bAllowRenderTargetAtlasing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowRenderTargetAtlasing = { "bAllowRenderTargetAtlasing", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationRenderFrame), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowRenderTargetAtlasing_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowRenderTargetAtlasing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowRenderTargetAtlasing_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ViewFamilyMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ViewFamilyMode_MetaData[] = {
		{ "Comment", "// Performance: Allow viewfamily merge optimization (render multiple viewports contexts within single family)\n// [not implemented yet] Experimental\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Performance: Allow viewfamily merge optimization (render multiple viewports contexts within single family)\n[not implemented yet] Experimental" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ViewFamilyMode = { "ViewFamilyMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, ViewFamilyMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderFamilyMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ViewFamilyMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ViewFamilyMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bShouldUseParentViewportRenderFamily_MetaData[] = {
		{ "Comment", "// Performance: Allow to use parent ViewFamily from parent viewport \n// (icvfx has child viewports: lightcard and chromakey with prj_view matrices copied from parent viewport. May sense to use same viewfamily?)\n// [not implemented yet] Experimental\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Performance: Allow to use parent ViewFamily from parent viewport\n(icvfx has child viewports: lightcard and chromakey with prj_view matrices copied from parent viewport. May sense to use same viewfamily?)\n[not implemented yet] Experimental" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bShouldUseParentViewportRenderFamily_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationRenderFrame*)Obj)->bShouldUseParentViewportRenderFamily = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bShouldUseParentViewportRenderFamily = { "bShouldUseParentViewportRenderFamily", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationRenderFrame), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bShouldUseParentViewportRenderFamily_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bShouldUseParentViewportRenderFamily_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bShouldUseParentViewportRenderFamily_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterRenderTargetRatioMult_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10" },
		{ "ClampMin", "0.05" },
		{ "Comment", "// Multiply all viewports RTT size's for whole cluster by this value\n" },
		{ "DisplayName", "Global Viewport RTT Size Multiplier" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Multiply all viewports RTT size's for whole cluster by this value" },
		{ "UIMax", "10" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterRenderTargetRatioMult = { "ClusterRenderTargetRatioMult", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, ClusterRenderTargetRatioMult), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterRenderTargetRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterRenderTargetRatioMult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerViewportRenderTargetRatioMult_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10" },
		{ "ClampMin", "0.05" },
		{ "Comment", "// Multiply inner frustum RTT size's for whole cluster by this value\n" },
		{ "DisplayName", "Inner Frustum RTT Size Multiplier" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Multiply inner frustum RTT size's for whole cluster by this value" },
		{ "UIMax", "10" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerViewportRenderTargetRatioMult = { "ClusterICVFXInnerViewportRenderTargetRatioMult", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, ClusterICVFXInnerViewportRenderTargetRatioMult), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerViewportRenderTargetRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerViewportRenderTargetRatioMult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportRenderTargetRatioMult_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10" },
		{ "ClampMin", "0.05" },
		{ "Comment", "// Multiply outer viewports RTT size's for whole cluster by this value\n" },
		{ "DisplayName", "Outer Viewport RTT Size Multiplier" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Multiply outer viewports RTT size's for whole cluster by this value" },
		{ "UIMax", "10" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportRenderTargetRatioMult = { "ClusterICVFXOuterViewportRenderTargetRatioMult", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, ClusterICVFXOuterViewportRenderTargetRatioMult), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportRenderTargetRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportRenderTargetRatioMult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterBufferRatioMult_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10" },
		{ "ClampMin", "0.05" },
		{ "Comment", "// Multiply all buffer ratios for whole cluster by this value\n" },
		{ "DisplayName", "Global Viewport Screen Percentage Multiplier" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Multiply all buffer ratios for whole cluster by this value" },
		{ "UIMax", "10" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterBufferRatioMult = { "ClusterBufferRatioMult", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, ClusterBufferRatioMult), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterBufferRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterBufferRatioMult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerFrustumBufferRatioMult_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10" },
		{ "ClampMin", "0.05" },
		{ "Comment", "// Multiply inner frustums buffer ratios for whole cluster by this value\n" },
		{ "DisplayName", "Inner Frustum Screen Percentage Multiplier" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Multiply inner frustums buffer ratios for whole cluster by this value" },
		{ "UIMax", "10" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerFrustumBufferRatioMult = { "ClusterICVFXInnerFrustumBufferRatioMult", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, ClusterICVFXInnerFrustumBufferRatioMult), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerFrustumBufferRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerFrustumBufferRatioMult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportBufferRatioMult_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "10" },
		{ "ClampMin", "0.05" },
		{ "Comment", "// Multiply the screen percentage for all viewports in the cluster by this value.\n" },
		{ "DisplayName", "Viewport Screen Percentage Multiplier" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Multiply the screen percentage for all viewports in the cluster by this value." },
		{ "UIMax", "1" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportBufferRatioMult = { "ClusterICVFXOuterViewportBufferRatioMult", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationRenderFrame, ClusterICVFXOuterViewportBufferRatioMult), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportBufferRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportBufferRatioMult_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowWarpBlend_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "// Allow warpblend render\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Allow warpblend render" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowWarpBlend_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationRenderFrame*)Obj)->bAllowWarpBlend = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowWarpBlend = { "bAllowWarpBlend", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationRenderFrame), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowWarpBlend_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowWarpBlend_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowWarpBlend_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_MultiGPUMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_MultiGPUMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowRenderTargetAtlasing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ViewFamilyMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ViewFamilyMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bShouldUseParentViewportRenderFamily,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterRenderTargetRatioMult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerViewportRenderTargetRatioMult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportRenderTargetRatioMult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterBufferRatioMult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXInnerFrustumBufferRatioMult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_ClusterICVFXOuterViewportBufferRatioMult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::NewProp_bAllowWarpBlend,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationRenderFrame",
		sizeof(FDisplayClusterConfigurationRenderFrame),
		alignof(FDisplayClusterConfigurationRenderFrame),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationRenderFrame"), sizeof(FDisplayClusterConfigurationRenderFrame), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame_Hash() { return 3171848479U; }
class UScriptStruct* FDisplayClusterConfigurationViewport_RenderSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationViewport_RenderSettings"), sizeof(FDisplayClusterConfigurationViewport_RenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationViewport_RenderSettings>()
{
	return FDisplayClusterConfigurationViewport_RenderSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings(FDisplayClusterConfigurationViewport_RenderSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationViewport_RenderSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_RenderSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_RenderSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationViewport_RenderSettings>(FName(TEXT("DisplayClusterConfigurationViewport_RenderSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_RenderSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoGPUIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_StereoGPUIndex;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StereoMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StereoMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BufferRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTargetRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RenderTargetRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomPostprocess_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomPostprocess;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Replace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Replace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostprocessBlur_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PostprocessBlur;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenerateMips_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_GenerateMips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Overscan_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Overscan;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderFamilyGroup_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RenderFamilyGroup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationViewport_RenderSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoGPUIndex_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Specify which GPU should render the second Stereo eye */" },
		{ "DisplayName", "Stereo GPU Index" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Specify which GPU should render the second Stereo eye" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoGPUIndex = { "StereoGPUIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, StereoGPUIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoGPUIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoGPUIndex_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoMode_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Enables and sets Stereo mode */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Enables and sets Stereo mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoMode = { "StereoMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, StereoMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewport_StereoMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_BufferRatio_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.05" },
		{ "Comment", "/** Adjust resolution scaling for an individual viewport.  Viewport Screen Percentage Multiplier is applied to this value. */" },
		{ "DisplayName", "Screen Percentage" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Adjust resolution scaling for an individual viewport.  Viewport Screen Percentage Multiplier is applied to this value." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, BufferRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_BufferRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_BufferRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderTargetRatio_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.01" },
		{ "Comment", "/** Adjust resolution scaling for an individual viewport.  Viewport Screen Percentage Multiplier is applied to this value. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Adjust resolution scaling for an individual viewport.  Viewport Screen Percentage Multiplier is applied to this value." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.01" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderTargetRatio = { "RenderTargetRatio", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, RenderTargetRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderTargetRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderTargetRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_CustomPostprocess_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_CustomPostprocess = { "CustomPostprocess", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, CustomPostprocess), Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_CustomPostprocess, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_CustomPostprocess_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_CustomPostprocess_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Replace_MetaData[] = {
		{ "Category", "Texture Replacement" },
		{ "Comment", "/** Override viewport render from source texture */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Override viewport render from source texture" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Replace = { "Replace", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, Replace), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_Override, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Replace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Replace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_PostprocessBlur_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Add postprocess blur to viewport */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Add postprocess blur to viewport" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_PostprocessBlur = { "PostprocessBlur", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, PostprocessBlur), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_BlurPostprocess, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_PostprocessBlur_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_PostprocessBlur_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_GenerateMips_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Generate Mips texture for this viewport (used, only if projection policy supports this feature) */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Generate Mips texture for this viewport (used, only if projection policy supports this feature)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_GenerateMips = { "GenerateMips", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, GenerateMips), Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostRender_GenerateMips, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_GenerateMips_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_GenerateMips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Overscan_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Render a larger frame than specified in the configuration to achieve continuity across displays when using post-processing effects. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Render a larger frame than specified in the configuration to achieve continuity across displays when using post-processing effects." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Overscan = { "Overscan", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, Overscan), Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Overscan_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Overscan_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData[] = {
		{ "Comment", "// Experimental: Support special frame builder mode - merge viewports to single viewfamily by group num\n// [not implemented yet]\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Experimental: Support special frame builder mode - merge viewports to single viewfamily by group num\n[not implemented yet]" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderFamilyGroup = { "RenderFamilyGroup", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_RenderSettings, RenderFamilyGroup), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderFamilyGroup_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoGPUIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_StereoMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderTargetRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_CustomPostprocess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Replace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_PostprocessBlur,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_GenerateMips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_Overscan,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::NewProp_RenderFamilyGroup,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationViewport_RenderSettings",
		sizeof(FDisplayClusterConfigurationViewport_RenderSettings),
		alignof(FDisplayClusterConfigurationViewport_RenderSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationViewport_RenderSettings"), sizeof(FDisplayClusterConfigurationViewport_RenderSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings_Hash() { return 2907140984U; }
class UScriptStruct* FDisplayClusterConfigurationViewport_ICVFX::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationViewport_ICVFX"), sizeof(FDisplayClusterConfigurationViewport_ICVFX), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationViewport_ICVFX>()
{
	return FDisplayClusterConfigurationViewport_ICVFX::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX(FDisplayClusterConfigurationViewport_ICVFX::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationViewport_ICVFX"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_ICVFX
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_ICVFX()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationViewport_ICVFX>(FName(TEXT("DisplayClusterConfigurationViewport_ICVFX")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_ICVFX;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowICVFX_MetaData[];
#endif
		static void NewProp_bAllowICVFX_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowICVFX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowInnerFrustum_MetaData[];
#endif
		static void NewProp_bAllowInnerFrustum_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowInnerFrustum;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CameraRenderMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraRenderMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CameraRenderMode;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LightcardRenderMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LightcardRenderMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LightcardRenderMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationViewport_ICVFX>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowICVFX_MetaData[] = {
		{ "Category", "In Camera VFX" },
		{ "Comment", "/** Enable in-camera VFX for this Viewport (works only with supported Projection Policies) */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Enable in-camera VFX for this Viewport (works only with supported Projection Policies)" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowICVFX_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationViewport_ICVFX*)Obj)->bAllowICVFX = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowICVFX = { "bAllowICVFX", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationViewport_ICVFX), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowICVFX_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowICVFX_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowICVFX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowInnerFrustum_MetaData[] = {
		{ "Category", "In Camera VFX" },
		{ "Comment", "/** Allow the inner frustum to appear on this Viewport */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Allow the inner frustum to appear on this Viewport" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowInnerFrustum_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationViewport_ICVFX*)Obj)->bAllowInnerFrustum = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowInnerFrustum = { "bAllowInnerFrustum", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationViewport_ICVFX), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowInnerFrustum_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowInnerFrustum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowInnerFrustum_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_CameraRenderMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_CameraRenderMode_MetaData[] = {
		{ "Category", "In Camera VFX" },
		{ "Comment", "/** Disable incamera render to this viewport */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Disable incamera render to this viewport" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_CameraRenderMode = { "CameraRenderMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_ICVFX, CameraRenderMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideCameraRenderMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_CameraRenderMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_CameraRenderMode_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_LightcardRenderMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_LightcardRenderMode_MetaData[] = {
		{ "Category", "In Camera VFX" },
		{ "Comment", "/** Use unique lightcard mode for this viewport */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Use unique lightcard mode for this viewport" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_LightcardRenderMode = { "LightcardRenderMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_ICVFX, LightcardRenderMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationICVFX_OverrideLightcardRenderMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_LightcardRenderMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_LightcardRenderMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowICVFX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_bAllowInnerFrustum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_CameraRenderMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_CameraRenderMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_LightcardRenderMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::NewProp_LightcardRenderMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationViewport_ICVFX",
		sizeof(FDisplayClusterConfigurationViewport_ICVFX),
		alignof(FDisplayClusterConfigurationViewport_ICVFX),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationViewport_ICVFX"), sizeof(FDisplayClusterConfigurationViewport_ICVFX), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX_Hash() { return 3792019895U; }
class UScriptStruct* FDisplayClusterConfigurationViewport_Overscan::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationViewport_Overscan"), sizeof(FDisplayClusterConfigurationViewport_Overscan), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationViewport_Overscan>()
{
	return FDisplayClusterConfigurationViewport_Overscan::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan(FDisplayClusterConfigurationViewport_Overscan::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationViewport_Overscan"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_Overscan
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_Overscan()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationViewport_Overscan>(FName(TEXT("DisplayClusterConfigurationViewport_Overscan")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationViewport_Overscan;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Mode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Left_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Left;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Right_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Right;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Top_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Top;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bottom_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Bottom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOversize_MetaData[];
#endif
		static void NewProp_bOversize_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOversize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationViewport_Overscan>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Mode_MetaData[] = {
		{ "Category", "NDisplay Viewport" },
		{ "Comment", "/** Enable/disable Viewport Overscan and specify units as percent or pixel values. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Enable/disable Viewport Overscan and specify units as percent or pixel values." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_Overscan, Mode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationViewportOverscanMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Mode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Mode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Left_MetaData[] = {
		{ "Category", "NDisplay Viewport" },
		{ "Comment", "/** Left */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Left" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Left = { "Left", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_Overscan, Left), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Left_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Left_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Right_MetaData[] = {
		{ "Category", "NDisplay Viewport" },
		{ "Comment", "/** Right */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Right" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Right = { "Right", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_Overscan, Right), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Right_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Right_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Top_MetaData[] = {
		{ "Category", "NDisplay Viewport" },
		{ "Comment", "/** Top */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Top" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Top = { "Top", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_Overscan, Top), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Top_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Top_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Bottom_MetaData[] = {
		{ "Category", "NDisplay Viewport" },
		{ "Comment", "/** Bottom */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Bottom" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Bottom = { "Bottom", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationViewport_Overscan, Bottom), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Bottom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Bottom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_bOversize_MetaData[] = {
		{ "Category", "NDisplay Viewport" },
		{ "Comment", "/** Set to True to render at the overscan resolution, set to false to render at the resolution in the configuration and scale for overscan. */" },
		{ "DisplayAfter", "Mode" },
		{ "DisplayName", "Adapt Resolution" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Set to True to render at the overscan resolution, set to false to render at the resolution in the configuration and scale for overscan." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_bOversize_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationViewport_Overscan*)Obj)->bOversize = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_bOversize = { "bOversize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationViewport_Overscan), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_bOversize_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_bOversize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_bOversize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Mode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Mode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Left,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Right,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Top,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_Bottom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::NewProp_bOversize,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationViewport_Overscan",
		sizeof(FDisplayClusterConfigurationViewport_Overscan),
		alignof(FDisplayClusterConfigurationViewport_Overscan),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationViewport_Overscan"), sizeof(FDisplayClusterConfigurationViewport_Overscan), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_Overscan_Hash() { return 862662847U; }
	void UDisplayClusterConfigurationViewport::StaticRegisterNativesUDisplayClusterConfigurationViewport()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationViewport_NoRegister()
	{
		return UDisplayClusterConfigurationViewport::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowRendering_MetaData[];
#endif
		static void NewProp_bAllowRendering_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowRendering;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Camera_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Camera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureShare_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TextureShare;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFixedAspectRatio_MetaData[];
#endif
		static void NewProp_bFixedAspectRatio_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFixedAspectRatio;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Region;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlapOrder_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_OverlapOrder;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GPUIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_GPUIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ICVFX_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ICVFX;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVisible_MetaData[];
#endif
		static void NewProp_bIsVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVisible;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationData_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DisplayClusterConfigurationTypes_Viewport.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bAllowRendering_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Enables or disables rendering of this specific Viewport */" },
		{ "DisplayName", "Enable Viewport" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Enables or disables rendering of this specific Viewport" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bAllowRendering_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationViewport*)Obj)->bAllowRendering = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bAllowRendering = { "bAllowRendering", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationViewport), &Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bAllowRendering_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bAllowRendering_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bAllowRendering_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Camera_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Reference to the nDisplay View Origin */" },
		{ "DisplayName", "View Origin" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Reference to the nDisplay View Origin" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Camera = { "Camera", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, Camera), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Camera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Camera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ProjectionPolicy_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Specify your Projection Policy Settings */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Specify your Projection Policy Settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ProjectionPolicy = { "ProjectionPolicy", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, ProjectionPolicy), Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ProjectionPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ProjectionPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_TextureShare_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Enable or disable compatibility with inter process GPU Texture share */" },
		{ "DisplayName", "Shared Texture" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Enable or disable compatibility with inter process GPU Texture share" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_TextureShare = { "TextureShare", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, TextureShare), Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_TextureShare_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_TextureShare_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bFixedAspectRatio_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Locks the Viewport aspect ratio for easier resizing */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Locks the Viewport aspect ratio for easier resizing" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bFixedAspectRatio_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationViewport*)Obj)->bFixedAspectRatio = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bFixedAspectRatio = { "bFixedAspectRatio", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationViewport), &Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bFixedAspectRatio_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bFixedAspectRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bFixedAspectRatio_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Region_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Define the Viewport 2D coordinates */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Define the Viewport 2D coordinates" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Region = { "Region", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, Region), Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Region_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Region_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_OverlapOrder_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Allows Viewports to overlap and sets Viewport overlapping order priority */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Allows Viewports to overlap and sets Viewport overlapping order priority" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_OverlapOrder = { "OverlapOrder", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, OverlapOrder), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_OverlapOrder_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_OverlapOrder_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_GPUIndex_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Specify which GPU should render this Viewport. \"-1\" is default. */" },
		{ "DisplayName", "GPU Index" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ToolTip", "Specify which GPU should render this Viewport. \"-1\" is default." },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_GPUIndex = { "GPUIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, GPUIndex), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_GPUIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_GPUIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_RenderSettings_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "// Configure render for this viewport\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Configure render for this viewport" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_RenderSettings = { "RenderSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, RenderSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_RenderSettings, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_RenderSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_RenderSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ICVFX_MetaData[] = {
		{ "Category", "In Camera VFX" },
		{ "Comment", "// Configure ICVFX for this viewport\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Configure ICVFX for this viewport" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ICVFX = { "ICVFX", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationViewport, ICVFX), Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ICVFX, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ICVFX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ICVFX_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "nDisplayHidden", "" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationViewport*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000800010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationViewport), &Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsVisible_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_Viewport.h" },
		{ "nDisplayHidden", "" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsVisible_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationViewport*)Obj)->bIsVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsVisible = { "bIsVisible", nullptr, (EPropertyFlags)0x0010000800010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationViewport), &Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsVisible_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsVisible_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bAllowRendering,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Camera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ProjectionPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_TextureShare,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bFixedAspectRatio,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_Region,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_OverlapOrder,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_GPUIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_RenderSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_ICVFX,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::NewProp_bIsVisible,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationViewport>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::ClassParams = {
		&UDisplayClusterConfigurationViewport::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationViewport()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationViewport_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationViewport, 915033694);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationViewport>()
	{
		return UDisplayClusterConfigurationViewport::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationViewport(Z_Construct_UClass_UDisplayClusterConfigurationViewport, &UDisplayClusterConfigurationViewport::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationViewport"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationViewport);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
