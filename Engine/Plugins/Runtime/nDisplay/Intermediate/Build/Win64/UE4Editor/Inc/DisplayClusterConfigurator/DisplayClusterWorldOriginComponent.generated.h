// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterWorldOriginComponent_generated_h
#error "DisplayClusterWorldOriginComponent.generated.h already included, missing '#pragma once' in DisplayClusterWorldOriginComponent.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterWorldOriginComponent_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterWorldOriginComponent(); \
	friend struct Z_Construct_UClass_UDisplayClusterWorldOriginComponent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterWorldOriginComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterWorldOriginComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterWorldOriginComponent(); \
	friend struct Z_Construct_UClass_UDisplayClusterWorldOriginComponent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterWorldOriginComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterWorldOriginComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterWorldOriginComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterWorldOriginComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterWorldOriginComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterWorldOriginComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterWorldOriginComponent(UDisplayClusterWorldOriginComponent&&); \
	NO_API UDisplayClusterWorldOriginComponent(const UDisplayClusterWorldOriginComponent&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterWorldOriginComponent(UDisplayClusterWorldOriginComponent&&); \
	NO_API UDisplayClusterWorldOriginComponent(const UDisplayClusterWorldOriginComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterWorldOriginComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterWorldOriginComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterWorldOriginComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_13_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterWorldOriginComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_Viewport_DisplayClusterWorldOriginComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
