// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorGraphSchema_generated_h
#error "DisplayClusterConfiguratorGraphSchema.generated.h already included, missing '#pragma once' in DisplayClusterConfiguratorGraphSchema.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorGraphSchema_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfiguratorSchemaAction_NewNode_Statics; \
	DISPLAYCLUSTERCONFIGURATOR_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__ItemType() { return STRUCT_OFFSET(FDisplayClusterConfiguratorSchemaAction_NewNode, ItemType); } \
	FORCEINLINE static uint32 __PPO__PresetSize() { return STRUCT_OFFSET(FDisplayClusterConfiguratorSchemaAction_NewNode, PresetSize); } \
	typedef FEdGraphSchemaAction Super;


template<> DISPLAYCLUSTERCONFIGURATOR_API UScriptStruct* StaticStruct<struct FDisplayClusterConfiguratorSchemaAction_NewNode>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorGraphSchema(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorGraphSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorGraphSchema)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorGraphSchema(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorGraphSchema, UEdGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorGraphSchema)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorGraphSchema) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorGraphSchema); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorGraphSchema(UDisplayClusterConfiguratorGraphSchema&&); \
	NO_API UDisplayClusterConfiguratorGraphSchema(const UDisplayClusterConfiguratorGraphSchema&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorGraphSchema(UDisplayClusterConfiguratorGraphSchema&&); \
	NO_API UDisplayClusterConfiguratorGraphSchema(const UDisplayClusterConfiguratorGraphSchema&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorGraphSchema); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorGraphSchema)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_42_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h_46_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorGraphSchema>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraphSchema_h


#define FOREACH_ENUM_ECLUSTERITEMTYPE(op) \
	op(EClusterItemType::ClusterNode) \
	op(EClusterItemType::Viewport) 

enum class EClusterItemType : uint8;
template<> DISPLAYCLUSTERCONFIGURATOR_API UEnum* StaticEnum<EClusterItemType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
