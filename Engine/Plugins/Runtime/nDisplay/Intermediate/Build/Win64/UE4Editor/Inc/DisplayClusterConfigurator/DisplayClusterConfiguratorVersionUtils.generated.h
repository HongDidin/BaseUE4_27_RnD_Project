// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorVersionUtils_generated_h
#error "DisplayClusterConfiguratorVersionUtils.generated.h already included, missing '#pragma once' in DisplayClusterConfiguratorVersionUtils.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorVersionUtils_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorEditorData(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorEditorData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorEditorData)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorEditorData(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorData_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorEditorData, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorEditorData)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorEditorData(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorEditorData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorEditorData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorEditorData(UDisplayClusterConfiguratorEditorData&&); \
	NO_API UDisplayClusterConfiguratorEditorData(const UDisplayClusterConfiguratorEditorData&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorEditorData(UDisplayClusterConfiguratorEditorData&&); \
	NO_API UDisplayClusterConfiguratorEditorData(const UDisplayClusterConfiguratorEditorData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorEditorData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorEditorData); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorEditorData)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_20_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorEditorData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorVersionUtils_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
