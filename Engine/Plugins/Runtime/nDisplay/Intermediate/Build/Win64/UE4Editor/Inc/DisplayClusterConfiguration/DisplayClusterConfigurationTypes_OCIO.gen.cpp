// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_OCIO.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_OCIO() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration();
// End Cross Module References
class UScriptStruct* FDisplayClusterConfigurationOCIOProfile::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationOCIOProfile"), sizeof(FDisplayClusterConfigurationOCIOProfile), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationOCIOProfile>()
{
	return FDisplayClusterConfigurationOCIOProfile::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile(FDisplayClusterConfigurationOCIOProfile::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationOCIOProfile"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationOCIOProfile
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationOCIOProfile()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationOCIOProfile>(FName(TEXT("DisplayClusterConfigurationOCIOProfile")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationOCIOProfile;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OCIOConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OCIOConfiguration;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ApplyOCIOToObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ApplyOCIOToObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ApplyOCIOToObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OCIO.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationOCIOProfile>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** Enable the application of an OpenColorIO configuration for the viewport(s) specified. */" },
		{ "DisplayName", "Enable OCIO" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OCIO.h" },
		{ "ToolTip", "Enable the application of an OpenColorIO configuration for the viewport(s) specified." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationOCIOProfile*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationOCIOProfile), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_OCIOConfiguration_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** OpenColorIO Configuration */" },
		{ "DisplayName", "OCIO Configuration" },
		{ "EditCondition", "bIsEnabled" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OCIO.h" },
		{ "ToolTip", "OpenColorIO Configuration" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_OCIOConfiguration = { "OCIOConfiguration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationOCIOProfile, OCIOConfiguration), Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_OCIOConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_OCIOConfiguration_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_ApplyOCIOToObjects_Inner = { "ApplyOCIOToObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_ApplyOCIOToObjects_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** Specify the viewports to apply this OpenColorIO configuration. */" },
		{ "EditCondition", "bIsEnabled" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OCIO.h" },
		{ "ToolTip", "Specify the viewports to apply this OpenColorIO configuration." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_ApplyOCIOToObjects = { "ApplyOCIOToObjects", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationOCIOProfile, ApplyOCIOToObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_ApplyOCIOToObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_ApplyOCIOToObjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_OCIOConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_ApplyOCIOToObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::NewProp_ApplyOCIOToObjects,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationOCIOProfile",
		sizeof(FDisplayClusterConfigurationOCIOProfile),
		alignof(FDisplayClusterConfigurationOCIOProfile),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationOCIOProfile"), sizeof(FDisplayClusterConfigurationOCIOProfile), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOProfile_Hash() { return 2410885545U; }
class UScriptStruct* FDisplayClusterConfigurationOCIOConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationOCIOConfiguration"), sizeof(FDisplayClusterConfigurationOCIOConfiguration), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationOCIOConfiguration>()
{
	return FDisplayClusterConfigurationOCIOConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration(FDisplayClusterConfigurationOCIOConfiguration::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationOCIOConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationOCIOConfiguration
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationOCIOConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationOCIOConfiguration>(FName(TEXT("DisplayClusterConfigurationOCIOConfiguration")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationOCIOConfiguration;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OCIOConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OCIOConfiguration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/*\n * OCIO profile structure. Can be configured for viewports or cluster nodes.\n * To enable viewport configuration when using as a UPROPERTY set meta = (ConfigurationMode = \"Viewports\")\n * To enable cluster node configuration when using as a UPROPERTY set meta = (ConfigurationMode = \"ClusterNodes\")\n */" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OCIO.h" },
		{ "ToolTip", "* OCIO profile structure. Can be configured for viewports or cluster nodes.\n* To enable viewport configuration when using as a UPROPERTY set meta = (ConfigurationMode = \"Viewports\")\n* To enable cluster node configuration when using as a UPROPERTY set meta = (ConfigurationMode = \"ClusterNodes\")" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationOCIOConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** Enable the application of an OpenColorIO configuration to all viewports. */" },
		{ "DisplayName", "Enable Viewport OCIO" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OCIO.h" },
		{ "ToolTip", "Enable the application of an OpenColorIO configuration to all viewports." },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationOCIOConfiguration*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationOCIOConfiguration), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_OCIOConfiguration_MetaData[] = {
		{ "Category", "OCIO" },
		{ "Comment", "/** OpenColorIO Configuration */" },
		{ "DisplayName", "OCIO Configuration" },
		{ "EditCondition", "bIsEnabled" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_OCIO.h" },
		{ "ToolTip", "OpenColorIO Configuration" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_OCIOConfiguration = { "OCIOConfiguration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationOCIOConfiguration, OCIOConfiguration), Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_OCIOConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_OCIOConfiguration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::NewProp_OCIOConfiguration,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationOCIOConfiguration",
		sizeof(FDisplayClusterConfigurationOCIOConfiguration),
		alignof(FDisplayClusterConfigurationOCIOConfiguration),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationOCIOConfiguration"), sizeof(FDisplayClusterConfigurationOCIOConfiguration), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationOCIOConfiguration_Hash() { return 2314549612U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
