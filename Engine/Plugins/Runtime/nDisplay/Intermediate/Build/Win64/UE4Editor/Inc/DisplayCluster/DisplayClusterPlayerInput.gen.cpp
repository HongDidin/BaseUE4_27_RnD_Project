// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/DisplayClusterPlayerInput.h"
#include "Engine/Classes/GameFramework/PlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterPlayerInput() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterPlayerInput_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterPlayerInput();
	ENGINE_API UClass* Z_Construct_UClass_UPlayerInput();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
// End Cross Module References
	void UDisplayClusterPlayerInput::StaticRegisterNativesUDisplayClusterPlayerInput()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterPlayerInput_NoRegister()
	{
		return UDisplayClusterPlayerInput::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterPlayerInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterPlayerInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPlayerInput,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPlayerInput_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Object within PlayerController that processes player input.\n */" },
		{ "IncludePath", "DisplayClusterPlayerInput.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterPlayerInput.h" },
		{ "ToolTip", "Object within PlayerController that processes player input." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterPlayerInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterPlayerInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterPlayerInput_Statics::ClassParams = {
		&UDisplayClusterPlayerInput::StaticClass,
		"Input",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000ACu,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPlayerInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPlayerInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterPlayerInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterPlayerInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterPlayerInput, 287227310);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterPlayerInput>()
	{
		return UDisplayClusterPlayerInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterPlayerInput(Z_Construct_UClass_UDisplayClusterPlayerInput, &UDisplayClusterPlayerInput::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterPlayerInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterPlayerInput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
