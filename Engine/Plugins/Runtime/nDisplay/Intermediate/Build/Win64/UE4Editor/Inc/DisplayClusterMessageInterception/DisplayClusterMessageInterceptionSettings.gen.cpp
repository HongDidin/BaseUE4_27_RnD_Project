// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterMessageInterceptor/Public/DisplayClusterMessageInterceptionSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterMessageInterceptionSettings() {}
// Cross Module References
	DISPLAYCLUSTERMESSAGEINTERCEPTION_API UScriptStruct* Z_Construct_UScriptStruct_FMessageInterceptionSettings();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterMessageInterception();
	DISPLAYCLUSTERMESSAGEINTERCEPTION_API UClass* Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_NoRegister();
	DISPLAYCLUSTERMESSAGEINTERCEPTION_API UClass* Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FMessageInterceptionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERMESSAGEINTERCEPTION_API uint32 Get_Z_Construct_UScriptStruct_FMessageInterceptionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMessageInterceptionSettings, Z_Construct_UPackage__Script_DisplayClusterMessageInterception(), TEXT("MessageInterceptionSettings"), sizeof(FMessageInterceptionSettings), Get_Z_Construct_UScriptStruct_FMessageInterceptionSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERMESSAGEINTERCEPTION_API UScriptStruct* StaticStruct<FMessageInterceptionSettings>()
{
	return FMessageInterceptionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMessageInterceptionSettings(FMessageInterceptionSettings::StaticStruct, TEXT("/Script/DisplayClusterMessageInterception"), TEXT("MessageInterceptionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterMessageInterception_StaticRegisterNativesFMessageInterceptionSettings
{
	FScriptStruct_DisplayClusterMessageInterception_StaticRegisterNativesFMessageInterceptionSettings()
	{
		UScriptStruct::DeferCppStructOps<FMessageInterceptionSettings>(FName(TEXT("MessageInterceptionSettings")));
	}
} ScriptStruct_DisplayClusterMessageInterception_StaticRegisterNativesFMessageInterceptionSettings;
	struct Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInterceptMultiUserMessages_MetaData[];
#endif
		static void NewProp_bInterceptMultiUserMessages_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInterceptMultiUserMessages;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeoutSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeoutSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterMessageInterceptionSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMessageInterceptionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "Interception Settings" },
		{ "Comment", "/** Indicates if message interception is enabled. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterMessageInterceptionSettings.h" },
		{ "ToolTip", "Indicates if message interception is enabled." },
	};
#endif
	void Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FMessageInterceptionSettings*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMessageInterceptionSettings), &Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bInterceptMultiUserMessages_MetaData[] = {
		{ "Category", "Interception Settings" },
		{ "Comment", "/** Indicates whether messages from multi user are intercepted. */" },
		{ "EditCondition", "bIsEnabled" },
		{ "ModuleRelativePath", "Public/DisplayClusterMessageInterceptionSettings.h" },
		{ "ToolTip", "Indicates whether messages from multi user are intercepted." },
	};
#endif
	void Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bInterceptMultiUserMessages_SetBit(void* Obj)
	{
		((FMessageInterceptionSettings*)Obj)->bInterceptMultiUserMessages = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bInterceptMultiUserMessages = { "bInterceptMultiUserMessages", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMessageInterceptionSettings), &Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bInterceptMultiUserMessages_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bInterceptMultiUserMessages_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bInterceptMultiUserMessages_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_TimeoutSeconds_MetaData[] = {
		{ "Category", "Interception Settings" },
		{ "Comment", "/** Maximum seconds to keep intercepted message to be synchronized across cluster. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterMessageInterceptionSettings.h" },
		{ "ToolTip", "Maximum seconds to keep intercepted message to be synchronized across cluster." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_TimeoutSeconds = { "TimeoutSeconds", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMessageInterceptionSettings, TimeoutSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_TimeoutSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_TimeoutSeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_bInterceptMultiUserMessages,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::NewProp_TimeoutSeconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterMessageInterception,
		nullptr,
		&NewStructOps,
		"MessageInterceptionSettings",
		sizeof(FMessageInterceptionSettings),
		alignof(FMessageInterceptionSettings),
		Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMessageInterceptionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMessageInterceptionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterMessageInterception();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MessageInterceptionSettings"), sizeof(FMessageInterceptionSettings), Get_Z_Construct_UScriptStruct_FMessageInterceptionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMessageInterceptionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMessageInterceptionSettings_Hash() { return 2516223001U; }
	void UDisplayClusterMessageInterceptionSettings::StaticRegisterNativesUDisplayClusterMessageInterceptionSettings()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_NoRegister()
	{
		return UDisplayClusterMessageInterceptionSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterceptionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InterceptionSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterMessageInterception,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DisplayClusterMessageInterceptionSettings.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterMessageInterceptionSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::NewProp_InterceptionSettings_MetaData[] = {
		{ "Category", "Interception Settings" },
		{ "Comment", "/**\n\x09 * Settings related to interception. \n\x09 * @note Settings from master will be synchronized across the cluster\n\x09 */" },
		{ "ModuleRelativePath", "Public/DisplayClusterMessageInterceptionSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Settings related to interception.\n@note Settings from master will be synchronized across the cluster" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::NewProp_InterceptionSettings = { "InterceptionSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterMessageInterceptionSettings, InterceptionSettings), Z_Construct_UScriptStruct_FMessageInterceptionSettings, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::NewProp_InterceptionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::NewProp_InterceptionSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::NewProp_InterceptionSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterMessageInterceptionSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::ClassParams = {
		&UDisplayClusterMessageInterceptionSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterMessageInterceptionSettings, 367291833);
	template<> DISPLAYCLUSTERMESSAGEINTERCEPTION_API UClass* StaticClass<UDisplayClusterMessageInterceptionSettings>()
	{
		return UDisplayClusterMessageInterceptionSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterMessageInterceptionSettings(Z_Construct_UClass_UDisplayClusterMessageInterceptionSettings, &UDisplayClusterMessageInterceptionSettings::StaticClass, TEXT("/Script/DisplayClusterMessageInterception"), TEXT("UDisplayClusterMessageInterceptionSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterMessageInterceptionSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
