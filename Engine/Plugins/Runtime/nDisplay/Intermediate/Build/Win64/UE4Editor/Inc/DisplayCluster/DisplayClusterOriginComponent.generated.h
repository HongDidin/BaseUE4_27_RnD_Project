// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterOriginComponent_generated_h
#error "DisplayClusterOriginComponent.generated.h already included, missing '#pragma once' in DisplayClusterOriginComponent.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterOriginComponent_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterOriginComponent(); \
	friend struct Z_Construct_UClass_UDisplayClusterOriginComponent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterOriginComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterOriginComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UDisplayClusterOriginComponent*>(this); }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterOriginComponent(); \
	friend struct Z_Construct_UClass_UDisplayClusterOriginComponent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterOriginComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterOriginComponent) \
	virtual UObject* _getUObject() const override { return const_cast<UDisplayClusterOriginComponent*>(this); }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterOriginComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterOriginComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterOriginComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterOriginComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterOriginComponent(UDisplayClusterOriginComponent&&); \
	NO_API UDisplayClusterOriginComponent(const UDisplayClusterOriginComponent&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterOriginComponent(UDisplayClusterOriginComponent&&); \
	NO_API UDisplayClusterOriginComponent(const UDisplayClusterOriginComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterOriginComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterOriginComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterOriginComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_18_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterOriginComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterOriginComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
