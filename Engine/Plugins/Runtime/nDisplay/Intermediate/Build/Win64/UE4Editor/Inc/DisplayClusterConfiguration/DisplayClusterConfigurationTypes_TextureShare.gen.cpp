// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes_TextureShare.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes_TextureShare() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncSurfaceDisplayCluster();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncFrameDisplayCluster();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncConnectDisplayCluster();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster();
// End Cross Module References
	static UEnum* ETextureShareSyncSurfaceDisplayCluster_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncSurfaceDisplayCluster, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("ETextureShareSyncSurfaceDisplayCluster"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<ETextureShareSyncSurfaceDisplayCluster>()
	{
		return ETextureShareSyncSurfaceDisplayCluster_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETextureShareSyncSurfaceDisplayCluster(ETextureShareSyncSurfaceDisplayCluster_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("ETextureShareSyncSurfaceDisplayCluster"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncSurfaceDisplayCluster_Hash() { return 2179132713U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncSurfaceDisplayCluster()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETextureShareSyncSurfaceDisplayCluster"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncSurfaceDisplayCluster_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETextureShareSyncSurfaceDisplayCluster::Default", (int64)ETextureShareSyncSurfaceDisplayCluster::Default },
				{ "ETextureShareSyncSurfaceDisplayCluster::None", (int64)ETextureShareSyncSurfaceDisplayCluster::None },
				{ "ETextureShareSyncSurfaceDisplayCluster::SyncRead", (int64)ETextureShareSyncSurfaceDisplayCluster::SyncRead },
				{ "ETextureShareSyncSurfaceDisplayCluster::SyncPairingRead", (int64)ETextureShareSyncSurfaceDisplayCluster::SyncPairingRead },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "TextureShare" },
				{ "Comment", "/** Synchronize texture events (LockTexture/UnlockTexture) */" },
				{ "Default.Comment", "/** [Default] - Use module global settings */" },
				{ "Default.Name", "ETextureShareSyncSurfaceDisplayCluster::Default" },
				{ "Default.ToolTip", "[Default] - Use module global settings" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
				{ "None.Comment", "/** [None] - Skip unpaired texture. Unordered read\\write operations */" },
				{ "None.Name", "ETextureShareSyncSurfaceDisplayCluster::None" },
				{ "None.ToolTip", "[None] - Skip unpaired texture. Unordered read\\write operations" },
				{ "SyncPairingRead.Comment", "/** [SyncPairingRead] - Required texture pairing. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed) */" },
				{ "SyncPairingRead.Name", "ETextureShareSyncSurfaceDisplayCluster::SyncPairingRead" },
				{ "SyncPairingRead.ToolTip", "[SyncPairingRead] - Required texture pairing. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed)" },
				{ "SyncRead.Comment", "/** [SyncRead] - Skip unpaired texture. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed) */" },
				{ "SyncRead.Name", "ETextureShareSyncSurfaceDisplayCluster::SyncRead" },
				{ "SyncRead.ToolTip", "[SyncRead] - Skip unpaired texture. Waiting until other process changed texture (readOP is wait for writeOP from remote process completed)" },
				{ "ToolTip", "Synchronize texture events (LockTexture/UnlockTexture)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"ETextureShareSyncSurfaceDisplayCluster",
				"ETextureShareSyncSurfaceDisplayCluster",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETextureShareSyncFrameDisplayCluster_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncFrameDisplayCluster, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("ETextureShareSyncFrameDisplayCluster"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<ETextureShareSyncFrameDisplayCluster>()
	{
		return ETextureShareSyncFrameDisplayCluster_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETextureShareSyncFrameDisplayCluster(ETextureShareSyncFrameDisplayCluster_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("ETextureShareSyncFrameDisplayCluster"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncFrameDisplayCluster_Hash() { return 717233468U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncFrameDisplayCluster()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETextureShareSyncFrameDisplayCluster"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncFrameDisplayCluster_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETextureShareSyncFrameDisplayCluster::Default", (int64)ETextureShareSyncFrameDisplayCluster::Default },
				{ "ETextureShareSyncFrameDisplayCluster::None", (int64)ETextureShareSyncFrameDisplayCluster::None },
				{ "ETextureShareSyncFrameDisplayCluster::FrameSync", (int64)ETextureShareSyncFrameDisplayCluster::FrameSync },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "TextureShare" },
				{ "Comment", "/** Synchronize frame events (BeginFrame/EndFrame) */" },
				{ "Default.Comment", "/** [Default] - Use module global settings */" },
				{ "Default.Name", "ETextureShareSyncFrameDisplayCluster::Default" },
				{ "Default.ToolTip", "[Default] - Use module global settings" },
				{ "FrameSync.Comment", "/** [FrameSync] - waiting until remote process frame index is equal */" },
				{ "FrameSync.Name", "ETextureShareSyncFrameDisplayCluster::FrameSync" },
				{ "FrameSync.ToolTip", "[FrameSync] - waiting until remote process frame index is equal" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
				{ "None.Comment", "/** [None] - Unordered frames */" },
				{ "None.Name", "ETextureShareSyncFrameDisplayCluster::None" },
				{ "None.ToolTip", "[None] - Unordered frames" },
				{ "ToolTip", "Synchronize frame events (BeginFrame/EndFrame)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"ETextureShareSyncFrameDisplayCluster",
				"ETextureShareSyncFrameDisplayCluster",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETextureShareSyncConnectDisplayCluster_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncConnectDisplayCluster, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("ETextureShareSyncConnectDisplayCluster"));
		}
		return Singleton;
	}
	template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<ETextureShareSyncConnectDisplayCluster>()
	{
		return ETextureShareSyncConnectDisplayCluster_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETextureShareSyncConnectDisplayCluster(ETextureShareSyncConnectDisplayCluster_StaticEnum, TEXT("/Script/DisplayClusterConfiguration"), TEXT("ETextureShareSyncConnectDisplayCluster"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncConnectDisplayCluster_Hash() { return 1773706096U; }
	UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncConnectDisplayCluster()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETextureShareSyncConnectDisplayCluster"), 0, Get_Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncConnectDisplayCluster_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETextureShareSyncConnectDisplayCluster::Default", (int64)ETextureShareSyncConnectDisplayCluster::Default },
				{ "ETextureShareSyncConnectDisplayCluster::None", (int64)ETextureShareSyncConnectDisplayCluster::None },
				{ "ETextureShareSyncConnectDisplayCluster::SyncSession", (int64)ETextureShareSyncConnectDisplayCluster::SyncSession },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Category", "TextureShare" },
				{ "Comment", "/** Synchronize Session state events (BeginSession/EndSession) */" },
				{ "Default.Comment", "/** [Default] - Use module global settings */" },
				{ "Default.Name", "ETextureShareSyncConnectDisplayCluster::Default" },
				{ "Default.ToolTip", "[Default] - Use module global settings" },
				{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
				{ "None.Comment", "/** [None] - do not wait for remote process */" },
				{ "None.Name", "ETextureShareSyncConnectDisplayCluster::None" },
				{ "None.ToolTip", "[None] - do not wait for remote process" },
				{ "SyncSession.Comment", "/** [SyncSession] - waiting until remote process not inside BeginSession()/EndSession() */" },
				{ "SyncSession.Name", "ETextureShareSyncConnectDisplayCluster::SyncSession" },
				{ "SyncSession.ToolTip", "[SyncSession] - waiting until remote process not inside BeginSession()/EndSession()" },
				{ "ToolTip", "Synchronize Session state events (BeginSession/EndSession)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
				nullptr,
				"ETextureShareSyncConnectDisplayCluster",
				"ETextureShareSyncConnectDisplayCluster",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FDisplayClusterConfigurationTextureShare_Viewport::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationTextureShare_Viewport"), sizeof(FDisplayClusterConfigurationTextureShare_Viewport), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationTextureShare_Viewport>()
{
	return FDisplayClusterConfigurationTextureShare_Viewport::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport(FDisplayClusterConfigurationTextureShare_Viewport::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationTextureShare_Viewport"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationTextureShare_Viewport
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationTextureShare_Viewport()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationTextureShare_Viewport>(FName(TEXT("DisplayClusterConfigurationTextureShare_Viewport")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationTextureShare_Viewport;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SyncSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SyncSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationTextureShare_Viewport>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Allow this viewport to be shared through the TextureShare\n" },
		{ "DisplayName", "Share Viewport" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
		{ "ToolTip", "Allow this viewport to be shared through the TextureShare" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationTextureShare_Viewport*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationTextureShare_Viewport), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_SyncSettings_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// TextureShare synchronization settings, custom gui added in the configurator for Linux compatibility\n" },
		{ "DisplayName", "Sync Settings" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
		{ "ToolTip", "TextureShare synchronization settings, custom gui added in the configurator for Linux compatibility" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_SyncSettings = { "SyncSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationTextureShare_Viewport, SyncSettings), Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_SyncSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_SyncSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::NewProp_SyncSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationTextureShare_Viewport",
		sizeof(FDisplayClusterConfigurationTextureShare_Viewport),
		alignof(FDisplayClusterConfigurationTextureShare_Viewport),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationTextureShare_Viewport"), sizeof(FDisplayClusterConfigurationTextureShare_Viewport), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Hash() { return 3467770110U; }
class UScriptStruct* FTextureShareSyncPolicyDisplayCluster::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("TextureShareSyncPolicyDisplayCluster"), sizeof(FTextureShareSyncPolicyDisplayCluster), Get_Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FTextureShareSyncPolicyDisplayCluster>()
{
	return FTextureShareSyncPolicyDisplayCluster::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster(FTextureShareSyncPolicyDisplayCluster::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("TextureShareSyncPolicyDisplayCluster"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureShareSyncPolicyDisplayCluster
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureShareSyncPolicyDisplayCluster()
	{
		UScriptStruct::DeferCppStructOps<FTextureShareSyncPolicyDisplayCluster>(FName(TEXT("TextureShareSyncPolicyDisplayCluster")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFTextureShareSyncPolicyDisplayCluster;
	struct Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Connection_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Connection_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Connection;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Frame_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frame_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Frame;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Texture_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Texture_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Texture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTextureShareSyncPolicyDisplayCluster>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Connection_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Connection_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Synchronize Session state events (BeginSession/EndSession)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
		{ "ToolTip", "Synchronize Session state events (BeginSession/EndSession)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Connection = { "Connection", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareSyncPolicyDisplayCluster, Connection), Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncConnectDisplayCluster, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Connection_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Connection_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Frame_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Frame_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Synchronize frame events (BeginFrame/EndFrame)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
		{ "ToolTip", "Synchronize frame events (BeginFrame/EndFrame)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Frame = { "Frame", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareSyncPolicyDisplayCluster, Frame), Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncFrameDisplayCluster, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Frame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Frame_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Texture_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Texture_MetaData[] = {
		{ "Category", "TextureShare" },
		{ "Comment", "// Synchronize texture events (LockTexture/UnlockTexture)\n" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes_TextureShare.h" },
		{ "ToolTip", "Synchronize texture events (LockTexture/UnlockTexture)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTextureShareSyncPolicyDisplayCluster, Texture), Z_Construct_UEnum_DisplayClusterConfiguration_ETextureShareSyncSurfaceDisplayCluster, METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Texture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Texture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Connection_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Connection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Frame_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Frame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Texture_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::NewProp_Texture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"TextureShareSyncPolicyDisplayCluster",
		sizeof(FTextureShareSyncPolicyDisplayCluster),
		alignof(FTextureShareSyncPolicyDisplayCluster),
		Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TextureShareSyncPolicyDisplayCluster"), sizeof(FTextureShareSyncPolicyDisplayCluster), Get_Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Hash() { return 555398380U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
