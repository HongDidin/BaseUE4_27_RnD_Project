// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfigurator/Private/Settings/DisplayClusterConfiguratorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfiguratorSettings() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATOR_API UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_NoRegister();
	DISPLAYCLUSTERCONFIGURATOR_API UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfigurator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
// End Cross Module References
	void UDisplayClusterConfiguratorEditorSettings::StaticRegisterNativesUDisplayClusterConfiguratorEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_NoRegister()
	{
		return UDisplayClusterConfiguratorEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditorDefaultCameraLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EditorDefaultCameraLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditorDefaultCameraRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EditorDefaultCameraRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEditorShowFloor_MetaData[];
#endif
		static void NewProp_bEditorShowFloor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEditorShowFloor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEditorShowGrid_MetaData[];
#endif
		static void NewProp_bEditorShowGrid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEditorShowGrid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEditorShowWorldOrigin_MetaData[];
#endif
		static void NewProp_bEditorShowWorldOrigin_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEditorShowWorldOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEditorShowPreview_MetaData[];
#endif
		static void NewProp_bEditorShowPreview_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEditorShowPreview;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEditorShow3DViewportNames_MetaData[];
#endif
		static void NewProp_bEditorShow3DViewportNames_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEditorShow3DViewportNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportOnSave_MetaData[];
#endif
		static void NewProp_bExportOnSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportOnSave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUpdateAssetsOnStartup_MetaData[];
#endif
		static void NewProp_bUpdateAssetsOnStartup_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUpdateAssetsOnStartup;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisplayAssetUpdateProgress_MetaData[];
#endif
		static void NewProp_bDisplayAssetUpdateProgress_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisplayAssetUpdateProgress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisXformScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VisXformScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowVisXforms_MetaData[];
#endif
		static void NewProp_bShowVisXforms_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowVisXforms;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEditorEnableAA_MetaData[];
#endif
		static void NewProp_bEditorEnableAA_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEditorEnableAA;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewAssetIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewAssetIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfigurator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraLocation_MetaData[] = {
		{ "Category", "Viewport" },
		{ "Comment", "/** Camera view location when resetting the camera. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Camera view location when resetting the camera." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraLocation = { "EditorDefaultCameraLocation", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfiguratorEditorSettings, EditorDefaultCameraLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraRotation_MetaData[] = {
		{ "Category", "Viewport" },
		{ "Comment", "/** Camera view rotation when resetting the camera. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Camera view rotation when resetting the camera." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraRotation = { "EditorDefaultCameraRotation", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfiguratorEditorSettings, EditorDefaultCameraRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowFloor_MetaData[] = {
		{ "Comment", "/** Shows the floor in the 3d editor viewport. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Shows the floor in the 3d editor viewport." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowFloor_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bEditorShowFloor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowFloor = { "bEditorShowFloor", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowFloor_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowFloor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowFloor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowGrid_MetaData[] = {
		{ "Comment", "/** Shows the grid in the 3d editor viewport. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Shows the grid in the 3d editor viewport." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowGrid_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bEditorShowGrid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowGrid = { "bEditorShowGrid", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowGrid_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowGrid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowGrid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowWorldOrigin_MetaData[] = {
		{ "Comment", "/** Displays the world origin at 0, 0, 0 */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Displays the world origin at 0, 0, 0" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowWorldOrigin_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bEditorShowWorldOrigin = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowWorldOrigin = { "bEditorShowWorldOrigin", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowWorldOrigin_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowWorldOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowWorldOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowPreview_MetaData[] = {
		{ "Comment", "/** Shows the preview in editor */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Shows the preview in editor" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowPreview_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bEditorShowPreview = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowPreview = { "bEditorShowPreview", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowPreview_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowPreview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowPreview_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShow3DViewportNames_MetaData[] = {
		{ "Comment", "/** Shows names of the viewport in 3d space. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Shows names of the viewport in 3d space." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShow3DViewportNames_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bEditorShow3DViewportNames = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShow3DViewportNames = { "bEditorShow3DViewportNames", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShow3DViewportNames_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShow3DViewportNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShow3DViewportNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bExportOnSave_MetaData[] = {
		{ "Category", "Config File" },
		{ "Comment", "/** Export a config automatically on save. Requires a config initially exported. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Export a config automatically on save. Requires a config initially exported." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bExportOnSave_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bExportOnSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bExportOnSave = { "bExportOnSave", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bExportOnSave_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bExportOnSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bExportOnSave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bUpdateAssetsOnStartup_MetaData[] = {
		{ "Category", "Version Updates" },
		{ "Comment", "/**\n\x09 * Automatically update assets saved by older versions to the most current version. It is strongly recommended to leave this on.\n\x09 */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Automatically update assets saved by older versions to the most current version. It is strongly recommended to leave this on." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bUpdateAssetsOnStartup_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bUpdateAssetsOnStartup = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bUpdateAssetsOnStartup = { "bUpdateAssetsOnStartup", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bUpdateAssetsOnStartup_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bUpdateAssetsOnStartup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bUpdateAssetsOnStartup_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bDisplayAssetUpdateProgress_MetaData[] = {
		{ "Category", "Version Updates" },
		{ "Comment", "/**\n\x09 * Display a progress bar when updating assets to a new version.\n\x09 */" },
		{ "EditCondition", "bUpdateAssetsOnStartup" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Display a progress bar when updating assets to a new version." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bDisplayAssetUpdateProgress_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bDisplayAssetUpdateProgress = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bDisplayAssetUpdateProgress = { "bDisplayAssetUpdateProgress", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bDisplayAssetUpdateProgress_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bDisplayAssetUpdateProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bDisplayAssetUpdateProgress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_VisXformScale_MetaData[] = {
		{ "Comment", "/** The visual scale of the Xform static mesh */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "The visual scale of the Xform static mesh" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_VisXformScale = { "VisXformScale", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfiguratorEditorSettings, VisXformScale), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_VisXformScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_VisXformScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bShowVisXforms_MetaData[] = {
		{ "Comment", "/** Whether to show the Xform static mesh */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Whether to show the Xform static mesh" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bShowVisXforms_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bShowVisXforms = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bShowVisXforms = { "bShowVisXforms", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bShowVisXforms_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bShowVisXforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bShowVisXforms_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorEnableAA_MetaData[] = {
		{ "Comment", "/** Anti aliasing in 3d viewport. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "Anti aliasing in 3d viewport." },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorEnableAA_SetBit(void* Obj)
	{
		((UDisplayClusterConfiguratorEditorSettings*)Obj)->bEditorEnableAA = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorEnableAA = { "bEditorEnableAA", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfiguratorEditorSettings), &Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorEnableAA_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorEnableAA_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorEnableAA_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_NewAssetIndex_MetaData[] = {
		{ "Comment", "/** The last position on the new asset dialog box. */" },
		{ "ModuleRelativePath", "Private/Settings/DisplayClusterConfiguratorSettings.h" },
		{ "ToolTip", "The last position on the new asset dialog box." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_NewAssetIndex = { "NewAssetIndex", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfiguratorEditorSettings, NewAssetIndex), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_NewAssetIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_NewAssetIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_EditorDefaultCameraRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowFloor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowGrid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowWorldOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShowPreview,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorShow3DViewportNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bExportOnSave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bUpdateAssetsOnStartup,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bDisplayAssetUpdateProgress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_VisXformScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bShowVisXforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_bEditorEnableAA,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::NewProp_NewAssetIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfiguratorEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::ClassParams = {
		&UDisplayClusterConfiguratorEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfiguratorEditorSettings, 1799020127);
	template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<UDisplayClusterConfiguratorEditorSettings>()
	{
		return UDisplayClusterConfiguratorEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfiguratorEditorSettings(Z_Construct_UClass_UDisplayClusterConfiguratorEditorSettings, &UDisplayClusterConfiguratorEditorSettings::StaticClass, TEXT("/Script/DisplayClusterConfigurator"), TEXT("UDisplayClusterConfiguratorEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfiguratorEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
