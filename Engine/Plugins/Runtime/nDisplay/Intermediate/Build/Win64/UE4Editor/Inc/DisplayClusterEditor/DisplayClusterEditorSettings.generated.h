// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTEREDITOR_DisplayClusterEditorSettings_generated_h
#error "DisplayClusterEditorSettings.generated.h already included, missing '#pragma once' in DisplayClusterEditorSettings.h"
#endif
#define DISPLAYCLUSTEREDITOR_DisplayClusterEditorSettings_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterEditorSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterEditor"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterEditorSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterEditor"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterEditorSettings(UDisplayClusterEditorSettings&&); \
	NO_API UDisplayClusterEditorSettings(const UDisplayClusterEditorSettings&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterEditorSettings(UDisplayClusterEditorSettings&&); \
	NO_API UDisplayClusterEditorSettings(const UDisplayClusterEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterEditorSettings)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bEnabled() { return STRUCT_OFFSET(UDisplayClusterEditorSettings, bEnabled); }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_13_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTEREDITOR_API UClass* StaticClass<class UDisplayClusterEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterEditor_Private_Settings_DisplayClusterEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
