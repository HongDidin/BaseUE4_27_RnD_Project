// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Blueprints/IDisplayClusterBlueprintAPI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIDisplayClusterBlueprintAPI() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPI_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPI();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterClusterEventListener_NoRegister();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterCameraComponent_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterScreenComponent_NoRegister();
	DISPLAYCLUSTER_API UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	DISPLAYCLUSTER_API UEnum* Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_ADisplayClusterRootActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPostProcessSettings();
// End Cross Module References
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSceneViewExtensionIsActiveInContextFunction)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_ViewportIDs);
		P_GET_STRUCT_REF(FSceneViewExtensionIsActiveFunctor,Z_Param_Out_OutIsActiveFunction);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SceneViewExtensionIsActiveInContextFunction(Z_Param_Out_ViewportIDs,Z_Param_Out_OutIsActiveFunction);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetLocalViewports)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_ViewportIDs);
		P_GET_TARRAY_REF(FString,Z_Param_Out_ProjectionTypes);
		P_GET_TARRAY_REF(FIntPoint,Z_Param_Out_ViewportLocations);
		P_GET_TARRAY_REF(FIntPoint,Z_Param_Out_ViewportSizes);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetLocalViewports(Z_Param_Out_ViewportIDs,Z_Param_Out_ProjectionTypes,Z_Param_Out_ViewportLocations,Z_Param_Out_ViewportSizes);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetViewportRect)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_ViewportLoc);
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_ViewportSize);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetViewportRect(Z_Param_ViewportId,Z_Param_Out_ViewportLoc,Z_Param_Out_ViewportSize);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSetFinalPostProcessingSettings)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FPostProcessSettings,Z_Param_Out_FinalPostProcessingSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetFinalPostProcessingSettings(Z_Param_ViewportId,Z_Param_Out_FinalPostProcessingSettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSetOverridePostProcessingSettings)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FPostProcessSettings,Z_Param_Out_OverridePostProcessingSettings);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BlendWeight);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetOverridePostProcessingSettings(Z_Param_ViewportId,Z_Param_Out_OverridePostProcessingSettings,Z_Param_BlendWeight);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSetStartPostProcessingSettings)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_STRUCT_REF(FPostProcessSettings,Z_Param_Out_StartPostProcessingSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStartPostProcessingSettings(Z_Param_ViewportId,Z_Param_Out_StartPostProcessingSettings);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSetBufferRatio)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_PROPERTY(FFloatProperty,Z_Param_BufferRatio);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetBufferRatio(Z_Param_ViewportId,Z_Param_BufferRatio);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetBufferRatio)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_BufferRatio);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetBufferRatio(Z_Param_ViewportId,Z_Param_Out_BufferRatio);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSetViewportCamera)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_CameraId);
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetViewportCamera(Z_Param_CameraId,Z_Param_ViewportId);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetTrackerQuat)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_STRUCT_REF(FQuat,Z_Param_Out_Rotation);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetTrackerQuat(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_Rotation,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetTrackerLocation)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_Location);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetTrackerLocation(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_Location,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetAxis)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_Value);
		P_GET_UBOOL_REF(Z_Param_Out_IsAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAxis(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_Value,Z_Param_Out_IsAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execWasButtonReleased)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_WasReleased);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WasButtonReleased(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_WasReleased,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execWasButtonPressed)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_WasPressed);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WasButtonPressed(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_WasPressed,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execIsButtonReleased)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_IsReleasedCurrently);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IsButtonReleased(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_IsReleasedCurrently,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execIsButtonPressed)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_IsPressedCurrently);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->IsButtonPressed(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_IsPressedCurrently,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetButtonState)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_DeviceID);
		P_GET_PROPERTY(FIntProperty,Z_Param_DeviceChannel);
		P_GET_UBOOL_REF(Z_Param_Out_CurrentState);
		P_GET_UBOOL_REF(Z_Param_Out_IsChannelAvailable);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetButtonState(Z_Param_DeviceID,Z_Param_DeviceChannel,Z_Param_Out_CurrentState,Z_Param_Out_IsChannelAvailable);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetTrackerDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetTrackerDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetKeyboardDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetKeyboardDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetButtonDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetButtonDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetAxisDeviceIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_DeviceIDs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAxisDeviceIds(Z_Param_Out_DeviceIDs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetTrackerDeviceAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetTrackerDeviceAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetButtonDeviceAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetButtonDeviceAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetAxisDeviceAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetAxisDeviceAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSetDefaultCameraById)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_CameraID);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDefaultCameraById(Z_Param_CameraID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetDefaultCamera)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterCameraComponent**)Z_Param__Result=P_THIS->GetDefaultCamera();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetCamerasAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetCamerasAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetCameraById)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_CameraID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterCameraComponent**)Z_Param__Result=P_THIS->GetCameraById(Z_Param_CameraID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetAllCameras)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UDisplayClusterCameraComponent*>*)Z_Param__Result=P_THIS->GetAllCameras();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetScreensAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetScreensAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetAllScreens)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UDisplayClusterScreenComponent*>*)Z_Param__Result=P_THIS->GetAllScreens();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetScreenById)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_ScreenID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterScreenComponent**)Z_Param__Result=P_THIS->GetScreenById(Z_Param_ScreenID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetRootActor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ADisplayClusterRootActor**)Z_Param__Result=P_THIS->GetRootActor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetConfig)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterConfigurationData**)Z_Param__Result=P_THIS->GetConfig();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSendClusterEventBinaryTo)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Address);
		P_GET_PROPERTY(FIntProperty,Z_Param_Port);
		P_GET_STRUCT_REF(FDisplayClusterClusterEventBinary,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendClusterEventBinaryTo(Z_Param_Address,Z_Param_Port,Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execSendClusterEventJsonTo)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_Address);
		P_GET_PROPERTY(FIntProperty,Z_Param_Port);
		P_GET_STRUCT_REF(FDisplayClusterClusterEventJson,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SendClusterEventJsonTo(Z_Param_Address,Z_Param_Port,Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execEmitClusterEventBinary)
	{
		P_GET_STRUCT_REF(FDisplayClusterClusterEventBinary,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EmitClusterEventBinary(Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execEmitClusterEventJson)
	{
		P_GET_STRUCT_REF(FDisplayClusterClusterEventJson,Z_Param_Out_Event);
		P_GET_UBOOL(Z_Param_bMasterOnly);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->EmitClusterEventJson(Z_Param_Out_Event,Z_Param_bMasterOnly);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execRemoveClusterEventListener)
	{
		P_GET_TINTERFACE(IDisplayClusterClusterEventListener,Z_Param_Listener);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveClusterEventListener(Z_Param_Listener);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execAddClusterEventListener)
	{
		P_GET_TINTERFACE(IDisplayClusterClusterEventListener,Z_Param_Listener);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AddClusterEventListener(Z_Param_Listener);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetNodesAmount)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetNodesAmount();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetNodeId)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetNodeId();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetNodeIds)
	{
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutNodeIds);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetNodeIds(Z_Param_Out_OutNodeIds);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetClusterRole)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDisplayClusterNodeRole*)Z_Param__Result=P_THIS->GetClusterRole();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execIsBackup)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsBackup();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execIsSlave)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsSlave();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execIsMaster)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsMaster();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execGetOperationMode)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EDisplayClusterOperationMode*)Z_Param__Result=P_THIS->GetOperationMode();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDisplayClusterBlueprintAPI::execIsModuleInitialized)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsModuleInitialized();
		P_NATIVE_END;
	}
	void UDisplayClusterBlueprintAPI::StaticRegisterNativesUDisplayClusterBlueprintAPI()
	{
		UClass* Class = UDisplayClusterBlueprintAPI::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddClusterEventListener", &IDisplayClusterBlueprintAPI::execAddClusterEventListener },
			{ "EmitClusterEventBinary", &IDisplayClusterBlueprintAPI::execEmitClusterEventBinary },
			{ "EmitClusterEventJson", &IDisplayClusterBlueprintAPI::execEmitClusterEventJson },
			{ "GetAllCameras", &IDisplayClusterBlueprintAPI::execGetAllCameras },
			{ "GetAllScreens", &IDisplayClusterBlueprintAPI::execGetAllScreens },
			{ "GetAxis", &IDisplayClusterBlueprintAPI::execGetAxis },
			{ "GetAxisDeviceAmount", &IDisplayClusterBlueprintAPI::execGetAxisDeviceAmount },
			{ "GetAxisDeviceIds", &IDisplayClusterBlueprintAPI::execGetAxisDeviceIds },
			{ "GetBufferRatio", &IDisplayClusterBlueprintAPI::execGetBufferRatio },
			{ "GetButtonDeviceAmount", &IDisplayClusterBlueprintAPI::execGetButtonDeviceAmount },
			{ "GetButtonDeviceIds", &IDisplayClusterBlueprintAPI::execGetButtonDeviceIds },
			{ "GetButtonState", &IDisplayClusterBlueprintAPI::execGetButtonState },
			{ "GetCameraById", &IDisplayClusterBlueprintAPI::execGetCameraById },
			{ "GetCamerasAmount", &IDisplayClusterBlueprintAPI::execGetCamerasAmount },
			{ "GetClusterRole", &IDisplayClusterBlueprintAPI::execGetClusterRole },
			{ "GetConfig", &IDisplayClusterBlueprintAPI::execGetConfig },
			{ "GetDefaultCamera", &IDisplayClusterBlueprintAPI::execGetDefaultCamera },
			{ "GetKeyboardDeviceIds", &IDisplayClusterBlueprintAPI::execGetKeyboardDeviceIds },
			{ "GetLocalViewports", &IDisplayClusterBlueprintAPI::execGetLocalViewports },
			{ "GetNodeId", &IDisplayClusterBlueprintAPI::execGetNodeId },
			{ "GetNodeIds", &IDisplayClusterBlueprintAPI::execGetNodeIds },
			{ "GetNodesAmount", &IDisplayClusterBlueprintAPI::execGetNodesAmount },
			{ "GetOperationMode", &IDisplayClusterBlueprintAPI::execGetOperationMode },
			{ "GetRootActor", &IDisplayClusterBlueprintAPI::execGetRootActor },
			{ "GetScreenById", &IDisplayClusterBlueprintAPI::execGetScreenById },
			{ "GetScreensAmount", &IDisplayClusterBlueprintAPI::execGetScreensAmount },
			{ "GetTrackerDeviceAmount", &IDisplayClusterBlueprintAPI::execGetTrackerDeviceAmount },
			{ "GetTrackerDeviceIds", &IDisplayClusterBlueprintAPI::execGetTrackerDeviceIds },
			{ "GetTrackerLocation", &IDisplayClusterBlueprintAPI::execGetTrackerLocation },
			{ "GetTrackerQuat", &IDisplayClusterBlueprintAPI::execGetTrackerQuat },
			{ "GetViewportRect", &IDisplayClusterBlueprintAPI::execGetViewportRect },
			{ "IsBackup", &IDisplayClusterBlueprintAPI::execIsBackup },
			{ "IsButtonPressed", &IDisplayClusterBlueprintAPI::execIsButtonPressed },
			{ "IsButtonReleased", &IDisplayClusterBlueprintAPI::execIsButtonReleased },
			{ "IsMaster", &IDisplayClusterBlueprintAPI::execIsMaster },
			{ "IsModuleInitialized", &IDisplayClusterBlueprintAPI::execIsModuleInitialized },
			{ "IsSlave", &IDisplayClusterBlueprintAPI::execIsSlave },
			{ "RemoveClusterEventListener", &IDisplayClusterBlueprintAPI::execRemoveClusterEventListener },
			{ "SceneViewExtensionIsActiveInContextFunction", &IDisplayClusterBlueprintAPI::execSceneViewExtensionIsActiveInContextFunction },
			{ "SendClusterEventBinaryTo", &IDisplayClusterBlueprintAPI::execSendClusterEventBinaryTo },
			{ "SendClusterEventJsonTo", &IDisplayClusterBlueprintAPI::execSendClusterEventJsonTo },
			{ "SetBufferRatio", &IDisplayClusterBlueprintAPI::execSetBufferRatio },
			{ "SetDefaultCameraById", &IDisplayClusterBlueprintAPI::execSetDefaultCameraById },
			{ "SetFinalPostProcessingSettings", &IDisplayClusterBlueprintAPI::execSetFinalPostProcessingSettings },
			{ "SetOverridePostProcessingSettings", &IDisplayClusterBlueprintAPI::execSetOverridePostProcessingSettings },
			{ "SetStartPostProcessingSettings", &IDisplayClusterBlueprintAPI::execSetStartPostProcessingSettings },
			{ "SetViewportCamera", &IDisplayClusterBlueprintAPI::execSetViewportCamera },
			{ "WasButtonPressed", &IDisplayClusterBlueprintAPI::execWasButtonPressed },
			{ "WasButtonReleased", &IDisplayClusterBlueprintAPI::execWasButtonReleased },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics
	{
		struct DisplayClusterBlueprintAPI_eventAddClusterEventListener_Parms
		{
			TScriptInterface<IDisplayClusterClusterEventListener> Listener;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_Listener;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::NewProp_Listener = { "Listener", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventAddClusterEventListener_Parms, Listener), Z_Construct_UClass_UDisplayClusterClusterEventListener_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::NewProp_Listener,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Adds cluster event listener. */" },
		{ "DisplayName", "Add cluster event listener" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Adds cluster event listener." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "AddClusterEventListener", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventAddClusterEventListener_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics
	{
		struct DisplayClusterBlueprintAPI_eventEmitClusterEventBinary_Parms
		{
			FDisplayClusterClusterEventBinary Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventEmitClusterEventBinary_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventEmitClusterEventBinary_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventEmitClusterEventBinary_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Emits binary cluster event. */" },
		{ "DisplayName", "Emit binary cluster event" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Emits binary cluster event." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "EmitClusterEventBinary", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventEmitClusterEventBinary_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics
	{
		struct DisplayClusterBlueprintAPI_eventEmitClusterEventJson_Parms
		{
			FDisplayClusterClusterEventJson Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventEmitClusterEventJson_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventEmitClusterEventJson_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventEmitClusterEventJson_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Emits JSON cluster event. */" },
		{ "DisplayName", "Emit JSON cluster event" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Emits JSON cluster event." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "EmitClusterEventJson", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventEmitClusterEventJson_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetAllCameras_Parms
		{
			TArray<UDisplayClusterCameraComponent*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDisplayClusterCameraComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetAllCameras_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns array of all available camera components. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Get all cameras" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns array of all available camera components." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetAllCameras", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetAllCameras_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetAllScreens_Parms
		{
			TArray<UDisplayClusterScreenComponent*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDisplayClusterScreenComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetAllScreens_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns array of all screen components. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Get all screens" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns array of all screen components." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetAllScreens", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetAllScreens_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetAxis_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			float Value;
			bool IsAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static void NewProp_IsAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetAxis_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetAxis_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetAxis_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_IsAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventGetAxis_Parms*)Obj)->IsAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_IsAvailable = { "IsAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventGetAxis_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_IsAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::NewProp_IsAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns axis value at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN axis value" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns axis value at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetAxis", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetAxis_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetAxisDeviceAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetAxisDeviceAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns amount of VRPN axis devices. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get amount of VRPN axis devices" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns amount of VRPN axis devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetAxisDeviceAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetAxisDeviceAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetAxisDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetAxisDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns array of names of all VRPN axis devices. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of VRPN axis devices" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns array of names of all VRPN axis devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetAxisDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetAxisDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetBufferRatio_Parms
		{
			FString ViewportId;
			float BufferRatio;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetBufferRatio_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetBufferRatio_Parms, BufferRatio), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventGetBufferRatio_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventGetBufferRatio_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Returns current buffer ratio for specified viewport. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Get viewport's buffer ratio" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns current buffer ratio for specified viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetBufferRatio", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetBufferRatio_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetButtonDeviceAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetButtonDeviceAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns amount of VRPN button devices. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get amount of VRPN button devices" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns amount of VRPN button devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetButtonDeviceAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetButtonDeviceAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetButtonDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetButtonDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns array of names of all VRPN button devices. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of VRPN button devices" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns array of names of all VRPN button devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetButtonDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetButtonDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetButtonState_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool CurrentState;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_CurrentState_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CurrentState;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetButtonState_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetButtonState_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_CurrentState_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventGetButtonState_Parms*)Obj)->CurrentState = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_CurrentState = { "CurrentState", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventGetButtonState_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_CurrentState_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventGetButtonState_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventGetButtonState_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_CurrentState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns state of VRPN button at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN button state" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns state of VRPN button at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetButtonState", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetButtonState_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetCameraById_Parms
		{
			FString CameraID;
			UDisplayClusterCameraComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CameraID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_CameraID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_CameraID = { "CameraID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetCameraById_Parms, CameraID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_CameraID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_CameraID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetCameraById_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_CameraID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns camera component with specified ID. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Get camera by ID" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns camera component with specified ID." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetCameraById", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetCameraById_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetCamerasAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetCamerasAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns amount of cameras. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Get cameras amount" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns amount of cameras." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetCamerasAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetCamerasAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetClusterRole_Parms
		{
			EDisplayClusterNodeRole ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetClusterRole_Parms, ReturnValue), Z_Construct_UEnum_DisplayCluster_EDisplayClusterNodeRole, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns the role of the current cluster node. */" },
		{ "DisplayName", "Get cluster role" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns the role of the current cluster node." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetClusterRole", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetClusterRole_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetConfig_Parms
		{
			UDisplayClusterConfigurationData* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetConfig_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Return current configuration data */" },
		{ "DisplayName", "Get config data" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Return current configuration data" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetConfig", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetConfig_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetDefaultCamera_Parms
		{
			UDisplayClusterCameraComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetDefaultCamera_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns default camera component. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Get default camera" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns default camera component." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetDefaultCamera", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetDefaultCamera_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetKeyboardDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetKeyboardDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns array of names of all keyboard devices. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of keyboard devices" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns array of names of all keyboard devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetKeyboardDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetKeyboardDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetLocalViewports_Parms
		{
			TArray<FString> ViewportIDs;
			TArray<FString> ProjectionTypes;
			TArray<FIntPoint> ViewportLocations;
			TArray<FIntPoint> ViewportSizes;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportIDs;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProjectionTypes_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ProjectionTypes;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportLocations_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportLocations;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportSizes_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportSizes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportIDs_Inner = { "ViewportIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportIDs = { "ViewportIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetLocalViewports_Parms, ViewportIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ProjectionTypes_Inner = { "ProjectionTypes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ProjectionTypes = { "ProjectionTypes", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetLocalViewports_Parms, ProjectionTypes), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportLocations_Inner = { "ViewportLocations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportLocations = { "ViewportLocations", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetLocalViewports_Parms, ViewportLocations), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportSizes_Inner = { "ViewportSizes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportSizes = { "ViewportSizes", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetLocalViewports_Parms, ViewportSizes), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportIDs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ProjectionTypes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ProjectionTypes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportLocations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportLocations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportSizes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::NewProp_ViewportSizes,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Config" },
		{ "Comment", "/** Returns list of local viewports. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use new api" },
		{ "DisplayName", "Get local viewports" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns list of local viewports." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetLocalViewports", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetLocalViewports_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetNodeId_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetNodeId_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns cluster node name of the current application instance. */" },
		{ "DisplayName", "Get node ID" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns cluster node name of the current application instance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetNodeId", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetNodeId_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetNodeIds_Parms
		{
			TArray<FString> OutNodeIds;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutNodeIds_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutNodeIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::NewProp_OutNodeIds_Inner = { "OutNodeIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::NewProp_OutNodeIds = { "OutNodeIds", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetNodeIds_Parms, OutNodeIds), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::NewProp_OutNodeIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::NewProp_OutNodeIds,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns the role of the current cluster node. */" },
		{ "DisplayName", "Get cluster node IDs" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns the role of the current cluster node." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetNodeIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetNodeIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetNodesAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetNodesAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns amount of nodes in a cluster. */" },
		{ "DisplayName", "Get nodes amount" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns amount of nodes in a cluster." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetNodesAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetNodesAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetOperationMode_Parms
		{
			EDisplayClusterOperationMode ReturnValue;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetOperationMode_Parms, ReturnValue), Z_Construct_UEnum_DisplayCluster_EDisplayClusterOperationMode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Returns current operation mode. */" },
		{ "DisplayName", "Get operation mode" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns current operation mode." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetOperationMode", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetOperationMode_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetRootActor_Parms
		{
			ADisplayClusterRootActor* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetRootActor_Parms, ReturnValue), Z_Construct_UClass_ADisplayClusterRootActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns DisplayCluster root actor. */" },
		{ "DisplayName", "Get root actor" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns DisplayCluster root actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetRootActor", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetRootActor_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetScreenById_Parms
		{
			FString ScreenID;
			UDisplayClusterScreenComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScreenID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ScreenID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ScreenID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ScreenID = { "ScreenID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetScreenById_Parms, ScreenID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ScreenID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ScreenID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetScreenById_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterScreenComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ScreenID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns screen component by ID. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Get screen by ID" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns screen component by ID." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetScreenById", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetScreenById_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetScreensAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetScreensAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Returns amount of screens defined in current configuration file. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Get amount of screens" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns amount of screens defined in current configuration file." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetScreensAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetScreensAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetTrackerDeviceAmount_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerDeviceAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns amount of VRPN tracker devices. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get amount of VRPN tracker devices" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns amount of VRPN tracker devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetTrackerDeviceAmount", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetTrackerDeviceAmount_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetTrackerDeviceIds_Parms
		{
			TArray<FString> DeviceIDs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceIDs_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DeviceIDs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs_Inner = { "DeviceIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs = { "DeviceIDs", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerDeviceIds_Parms, DeviceIDs), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::NewProp_DeviceIDs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns array of names of all VRPN tracker devices. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get IDs of VRPN tracker devices" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns array of names of all VRPN tracker devices." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetTrackerDeviceIds", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetTrackerDeviceIds_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetTrackerLocation_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			FVector Location;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerLocation_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerLocation_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerLocation_Parms, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventGetTrackerLocation_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventGetTrackerLocation_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns tracker location values at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN tracker location" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns tracker location values at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetTrackerLocation", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetTrackerLocation_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetTrackerQuat_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			FQuat Rotation;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerQuat_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerQuat_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetTrackerQuat_Parms, Rotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventGetTrackerQuat_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventGetTrackerQuat_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns tracker quaternion values at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Get VRPN tracker rotation (as quaternion)" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns tracker quaternion values at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetTrackerQuat", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetTrackerQuat_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics
	{
		struct DisplayClusterBlueprintAPI_eventGetViewportRect_Parms
		{
			FString ViewportId;
			FIntPoint ViewportLoc;
			FIntPoint ViewportSize;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportLoc;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportSize;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetViewportRect_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportLoc = { "ViewportLoc", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetViewportRect_Parms, ViewportLoc), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportSize = { "ViewportSize", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventGetViewportRect_Parms, ViewportSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventGetViewportRect_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventGetViewportRect_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportLoc,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ViewportSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Returns location and size of specified viewport. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use new api" },
		{ "DisplayName", "Get viewport rectangle" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns location and size of specified viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "GetViewportRect", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventGetViewportRect_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics
	{
		struct DisplayClusterBlueprintAPI_eventIsBackup_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsBackup_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsBackup_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns true if current node is a backup node in a cluster. */" },
		{ "DisplayName", "Is backup node" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns true if current node is a backup node in a cluster." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "IsBackup", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventIsBackup_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics
	{
		struct DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool IsPressedCurrently;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_IsPressedCurrently_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsPressedCurrently;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsPressedCurrently_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms*)Obj)->IsPressedCurrently = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsPressedCurrently = { "IsPressedCurrently", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsPressedCurrently_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsPressedCurrently,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns whether VRPN button is pressed at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Is VRPN button pressed" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns whether VRPN button is pressed at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "IsButtonPressed", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventIsButtonPressed_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics
	{
		struct DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool IsReleasedCurrently;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_IsReleasedCurrently_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsReleasedCurrently;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsReleasedCurrently_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms*)Obj)->IsReleasedCurrently = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsReleasedCurrently = { "IsReleasedCurrently", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsReleasedCurrently_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsReleasedCurrently,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns whether VRPN button is released at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Is VRPN button released" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns whether VRPN button is released at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "IsButtonReleased", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventIsButtonReleased_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics
	{
		struct DisplayClusterBlueprintAPI_eventIsMaster_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsMaster_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsMaster_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns true if current node is a master node in a cluster. */" },
		{ "DisplayName", "Is master node" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns true if current node is a master node in a cluster." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "IsMaster", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventIsMaster_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics
	{
		struct DisplayClusterBlueprintAPI_eventIsModuleInitialized_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsModuleInitialized_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsModuleInitialized_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/* Returns true if the module has been initialized. */" },
		{ "DisplayName", "Is module initialized" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns true if the module has been initialized." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "IsModuleInitialized", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventIsModuleInitialized_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics
	{
		struct DisplayClusterBlueprintAPI_eventIsSlave_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventIsSlave_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventIsSlave_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Returns true if current node is a slave node in a cluster. */" },
		{ "DisplayName", "Is slave node" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns true if current node is a slave node in a cluster." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "IsSlave", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventIsSlave_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics
	{
		struct DisplayClusterBlueprintAPI_eventRemoveClusterEventListener_Parms
		{
			TScriptInterface<IDisplayClusterClusterEventListener> Listener;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_Listener;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::NewProp_Listener = { "Listener", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventRemoveClusterEventListener_Parms, Listener), Z_Construct_UClass_UDisplayClusterClusterEventListener_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::NewProp_Listener,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Removes cluster event listener. */" },
		{ "DisplayName", "Remove cluster event listener" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Removes cluster event listener." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "RemoveClusterEventListener", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventRemoveClusterEventListener_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSceneViewExtensionIsActiveInContextFunction_Parms
		{
			TArray<FString> ViewportIDs;
			FSceneViewExtensionIsActiveFunctor OutIsActiveFunction;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportIDs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportIDs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportIDs;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutIsActiveFunction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_Inner = { "ViewportIDs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs = { "ViewportIDs", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSceneViewExtensionIsActiveInContextFunction_Parms, ViewportIDs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_OutIsActiveFunction = { "OutIsActiveFunction", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSceneViewExtensionIsActiveInContextFunction_Parms, OutIsActiveFunction), Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_ViewportIDs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::NewProp_OutIsActiveFunction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Returns a functor that determines if any given scene view extension should be active in the given context for the current frame */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Scene View Extension Is Active In Context Function" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns a functor that determines if any given scene view extension should be active in the given context for the current frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SceneViewExtensionIsActiveInContextFunction", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSceneViewExtensionIsActiveInContextFunction_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSendClusterEventBinaryTo_Parms
		{
			FString Address;
			int32 Port;
			FDisplayClusterClusterEventBinary Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Address;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Port_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Port;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Address_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSendClusterEventBinaryTo_Parms, Address), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Address_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Port_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Port = { "Port", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSendClusterEventBinaryTo_Parms, Port), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Port_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Port_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSendClusterEventBinaryTo_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventBinary, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventSendClusterEventBinaryTo_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventSendClusterEventBinaryTo_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Address,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Port,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Sends binary cluster event to a specific target (outside of the cluster). */" },
		{ "DisplayName", "Send binary event to a specific host" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Sends binary cluster event to a specific target (outside of the cluster)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SendClusterEventBinaryTo", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSendClusterEventBinaryTo_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSendClusterEventJsonTo_Parms
		{
			FString Address;
			int32 Port;
			FDisplayClusterClusterEventJson Event;
			bool bMasterOnly;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Address;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Port_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Port;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static void NewProp_bMasterOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Address_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSendClusterEventJsonTo_Parms, Address), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Address_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Port_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Port = { "Port", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSendClusterEventJsonTo_Parms, Port), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Port_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Port_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Event_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSendClusterEventJsonTo_Parms, Event), Z_Construct_UScriptStruct_FDisplayClusterClusterEventJson, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Event_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventSendClusterEventJsonTo_Parms*)Obj)->bMasterOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly = { "bMasterOnly", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventSendClusterEventJsonTo_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Address,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Port,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_Event,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::NewProp_bMasterOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Cluster" },
		{ "Comment", "/** Sends JSON cluster event to a specific target (outside of the cluster). */" },
		{ "DisplayName", "Send JSON event to a specific host" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Sends JSON cluster event to a specific target (outside of the cluster)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SendClusterEventJsonTo", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSendClusterEventJsonTo_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSetBufferRatio_Parms
		{
			FString ViewportId;
			float BufferRatio;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BufferRatio;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetBufferRatio_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_BufferRatio = { "BufferRatio", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetBufferRatio_Parms, BufferRatio), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventSetBufferRatio_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventSetBufferRatio_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_BufferRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Sets buffer ratio for specified viewport. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set viewport's buffer ratio" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Sets buffer ratio for specified viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SetBufferRatio", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSetBufferRatio_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSetDefaultCameraById_Parms
		{
			FString CameraID;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CameraID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::NewProp_CameraID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::NewProp_CameraID = { "CameraID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetDefaultCameraById_Parms, CameraID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::NewProp_CameraID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::NewProp_CameraID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::NewProp_CameraID,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Game" },
		{ "Comment", "/** Sets default camera component specified by ID. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "This function has been moved to ADisplayClusterRootActor." },
		{ "DisplayName", "Set default camera by ID" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Sets default camera component specified by ID." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SetDefaultCameraById", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSetDefaultCameraById_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSetFinalPostProcessingSettings_Parms
		{
			FString ViewportId;
			FPostProcessSettings FinalPostProcessingSettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalPostProcessingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FinalPostProcessingSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetFinalPostProcessingSettings_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings = { "FinalPostProcessingSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetFinalPostProcessingSettings_Parms, FinalPostProcessingSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::NewProp_FinalPostProcessingSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Overrides postprocess settings for specified viewport. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set final post processing settings for viewport" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Overrides postprocess settings for specified viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SetFinalPostProcessingSettings", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSetFinalPostProcessingSettings_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSetOverridePostProcessingSettings_Parms
		{
			FString ViewportId;
			FPostProcessSettings OverridePostProcessingSettings;
			float BlendWeight;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverridePostProcessingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OverridePostProcessingSettings;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BlendWeight;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetOverridePostProcessingSettings_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings = { "OverridePostProcessingSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetOverridePostProcessingSettings_Parms, OverridePostProcessingSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_BlendWeight = { "BlendWeight", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetOverridePostProcessingSettings_Parms, BlendWeight), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_OverridePostProcessingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::NewProp_BlendWeight,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Overrides postprocess settings for specified viewport. */" },
		{ "CPP_Default_BlendWeight", "1.000000" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set override post processing settings for viewport" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Overrides postprocess settings for specified viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SetOverridePostProcessingSettings", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSetOverridePostProcessingSettings_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSetStartPostProcessingSettings_Parms
		{
			FString ViewportId;
			FPostProcessSettings StartPostProcessingSettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartPostProcessingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StartPostProcessingSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetStartPostProcessingSettings_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_ViewportId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings = { "StartPostProcessingSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetStartPostProcessingSettings_Parms, StartPostProcessingSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::NewProp_StartPostProcessingSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Overrides postprocess settings for specified viewport. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set start post processing settings for viewport" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Overrides postprocess settings for specified viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SetStartPostProcessingSettings", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSetStartPostProcessingSettings_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics
	{
		struct DisplayClusterBlueprintAPI_eventSetViewportCamera_Parms
		{
			FString CameraId;
			FString ViewportId;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CameraId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_CameraId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_CameraId = { "CameraId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetViewportCamera_Parms, CameraId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_CameraId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_CameraId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventSetViewportCamera_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_CameraId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::NewProp_ViewportId,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "Comment", "/** Binds camera to a viewport. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use Configuration structures" },
		{ "DisplayName", "Set viewport camera" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Binds camera to a viewport." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "SetViewportCamera", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventSetViewportCamera_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics
	{
		struct DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool WasPressed;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_WasPressed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WasPressed;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_WasPressed_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms*)Obj)->WasPressed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_WasPressed = { "WasPressed", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_WasPressed_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_WasPressed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns whether VRPN button was released at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Was VRPN button pressed" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns whether VRPN button was released at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "WasButtonPressed", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventWasButtonPressed_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics
	{
		struct DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms
		{
			FString DeviceID;
			int32 DeviceChannel;
			bool WasReleased;
			bool IsChannelAvailable;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceID;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DeviceChannel;
		static void NewProp_WasReleased_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WasReleased;
		static void NewProp_IsChannelAvailable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsChannelAvailable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_DeviceID_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_DeviceID = { "DeviceID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms, DeviceID), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_DeviceID_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_DeviceID_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_DeviceChannel = { "DeviceChannel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms, DeviceChannel), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_WasReleased_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms*)Obj)->WasReleased = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_WasReleased = { "WasReleased", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_WasReleased_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit(void* Obj)
	{
		((DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms*)Obj)->IsChannelAvailable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_IsChannelAvailable = { "IsChannelAvailable", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms), &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_IsChannelAvailable_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_DeviceID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_DeviceChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_WasReleased,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::NewProp_IsChannelAvailable,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Input" },
		{ "Comment", "/** Returns whether VRPN button was released at specified device and channel. */" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "VRPN functionality has been moved to LiveLinkVRPN" },
		{ "DisplayName", "Was VRPN button released" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
		{ "ToolTip", "Returns whether VRPN button was released at specified device and channel." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterBlueprintAPI, nullptr, "WasButtonReleased", nullptr, nullptr, sizeof(DisplayClusterBlueprintAPI_eventWasButtonReleased_Parms), Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPI_NoRegister()
	{
		return UDisplayClusterBlueprintAPI::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_AddClusterEventListener, "AddClusterEventListener" }, // 3683736208
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventBinary, "EmitClusterEventBinary" }, // 3914605097
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_EmitClusterEventJson, "EmitClusterEventJson" }, // 318355914
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllCameras, "GetAllCameras" }, // 3818475230
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAllScreens, "GetAllScreens" }, // 1375230631
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxis, "GetAxis" }, // 761390381
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceAmount, "GetAxisDeviceAmount" }, // 4120318690
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetAxisDeviceIds, "GetAxisDeviceIds" }, // 569685118
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetBufferRatio, "GetBufferRatio" }, // 1783552849
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceAmount, "GetButtonDeviceAmount" }, // 1962919465
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonDeviceIds, "GetButtonDeviceIds" }, // 1136207230
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetButtonState, "GetButtonState" }, // 2648365353
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCameraById, "GetCameraById" }, // 1739771355
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetCamerasAmount, "GetCamerasAmount" }, // 3201373073
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetClusterRole, "GetClusterRole" }, // 1492483170
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetConfig, "GetConfig" }, // 1075927577
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetDefaultCamera, "GetDefaultCamera" }, // 817373689
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetKeyboardDeviceIds, "GetKeyboardDeviceIds" }, // 1767900634
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetLocalViewports, "GetLocalViewports" }, // 3161791207
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeId, "GetNodeId" }, // 10727729
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodeIds, "GetNodeIds" }, // 623909402
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetNodesAmount, "GetNodesAmount" }, // 863764695
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetOperationMode, "GetOperationMode" }, // 4218386429
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetRootActor, "GetRootActor" }, // 3271072548
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreenById, "GetScreenById" }, // 451837825
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetScreensAmount, "GetScreensAmount" }, // 1773931329
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceAmount, "GetTrackerDeviceAmount" }, // 2458022812
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerDeviceIds, "GetTrackerDeviceIds" }, // 3771110627
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerLocation, "GetTrackerLocation" }, // 3122231357
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetTrackerQuat, "GetTrackerQuat" }, // 2499274248
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_GetViewportRect, "GetViewportRect" }, // 3737868996
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsBackup, "IsBackup" }, // 1138190363
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonPressed, "IsButtonPressed" }, // 915838123
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsButtonReleased, "IsButtonReleased" }, // 2473347701
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsMaster, "IsMaster" }, // 3521215683
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsModuleInitialized, "IsModuleInitialized" }, // 1741903992
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_IsSlave, "IsSlave" }, // 3145669877
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_RemoveClusterEventListener, "RemoveClusterEventListener" }, // 1349241779
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SceneViewExtensionIsActiveInContextFunction, "SceneViewExtensionIsActiveInContextFunction" }, // 3849499529
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventBinaryTo, "SendClusterEventBinaryTo" }, // 943339166
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SendClusterEventJsonTo, "SendClusterEventJsonTo" }, // 2482479693
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetBufferRatio, "SetBufferRatio" }, // 1535834932
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetDefaultCameraById, "SetDefaultCameraById" }, // 2711436365
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetFinalPostProcessingSettings, "SetFinalPostProcessingSettings" }, // 2823570920
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetOverridePostProcessingSettings, "SetOverridePostProcessingSettings" }, // 4017743369
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetStartPostProcessingSettings, "SetStartPostProcessingSettings" }, // 1042453660
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_SetViewportCamera, "SetViewportCamera" }, // 1089693893
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonPressed, "WasButtonPressed" }, // 1550785306
		{ &Z_Construct_UFunction_UDisplayClusterBlueprintAPI_WasButtonReleased, "WasButtonReleased" }, // 2680843815
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Public/Blueprints/IDisplayClusterBlueprintAPI.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IDisplayClusterBlueprintAPI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::ClassParams = {
		&UDisplayClusterBlueprintAPI::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterBlueprintAPI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterBlueprintAPI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterBlueprintAPI, 2199820131);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterBlueprintAPI>()
	{
		return UDisplayClusterBlueprintAPI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterBlueprintAPI(Z_Construct_UClass_UDisplayClusterBlueprintAPI, &UDisplayClusterBlueprintAPI::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterBlueprintAPI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterBlueprintAPI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
