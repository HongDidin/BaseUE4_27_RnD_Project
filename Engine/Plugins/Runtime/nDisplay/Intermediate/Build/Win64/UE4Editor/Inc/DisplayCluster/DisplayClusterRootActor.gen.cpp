// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/DisplayClusterRootActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterRootActor() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_ADisplayClusterRootActor_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_ADisplayClusterRootActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterCameraComponent_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterSyncTickComponent_NoRegister();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterComponentRef();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMode();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterPreviewComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ADisplayClusterRootActor::execSetReplaceTextureFlagForAllViewports)
	{
		P_GET_UBOOL(Z_Param_bReplace);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetReplaceTextureFlagForAllViewports(Z_Param_bReplace);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ADisplayClusterRootActor::execGetDefaultCamera)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterCameraComponent**)Z_Param__Result=P_THIS->GetDefaultCamera();
		P_NATIVE_END;
	}
	void ADisplayClusterRootActor::StaticRegisterNativesADisplayClusterRootActor()
	{
		UClass* Class = ADisplayClusterRootActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDefaultCamera", &ADisplayClusterRootActor::execGetDefaultCamera },
			{ "SetReplaceTextureFlagForAllViewports", &ADisplayClusterRootActor::execSetReplaceTextureFlagForAllViewports },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics
	{
		struct DisplayClusterRootActor_eventGetDefaultCamera_Parms
		{
			UDisplayClusterCameraComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterRootActor_eventGetDefaultCamera_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Components" },
		{ "DisplayName", "Get Default Camera" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADisplayClusterRootActor, nullptr, "GetDefaultCamera", nullptr, nullptr, sizeof(DisplayClusterRootActor_eventGetDefaultCamera_Parms), Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics
	{
		struct DisplayClusterRootActor_eventSetReplaceTextureFlagForAllViewports_Parms
		{
			bool bReplace;
			bool ReturnValue;
		};
		static void NewProp_bReplace_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReplace;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_bReplace_SetBit(void* Obj)
	{
		((DisplayClusterRootActor_eventSetReplaceTextureFlagForAllViewports_Parms*)Obj)->bReplace = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_bReplace = { "bReplace", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterRootActor_eventSetReplaceTextureFlagForAllViewports_Parms), &Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_bReplace_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterRootActor_eventSetReplaceTextureFlagForAllViewports_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterRootActor_eventSetReplaceTextureFlagForAllViewports_Parms), &Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_bReplace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Render" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ADisplayClusterRootActor, nullptr, "SetReplaceTextureFlagForAllViewports", nullptr, nullptr, sizeof(DisplayClusterRootActor_eventSetReplaceTextureFlagForAllViewports_Parms), Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ADisplayClusterRootActor_NoRegister()
	{
		return ADisplayClusterRootActor::StaticClass();
	}
	struct Z_Construct_UClass_ADisplayClusterRootActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigDataName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ConfigDataName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentConfigData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentConfigData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayClusterRootComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplayClusterRootComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultViewPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultViewPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SyncTickComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SyncTickComponent;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InnerFrustumPriority_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerFrustumPriority_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InnerFrustumPriority;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreviewEnable_MetaData[];
#endif
		static void NewProp_bPreviewEnable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreviewEnable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewNodeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PreviewNodeId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RenderMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RenderMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TickPerFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TickPerFrame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewRenderTargetRatioMult_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PreviewRenderTargetRatioMult;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewComponents_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PreviewComponents_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PreviewComponents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDeferPreviewGeneration_MetaData[];
#endif
		static void NewProp_bDeferPreviewGeneration_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDeferPreviewGeneration;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADisplayClusterRootActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ADisplayClusterRootActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ADisplayClusterRootActor_GetDefaultCamera, "GetDefaultCamera" }, // 2444070351
		{ &Z_Construct_UFunction_ADisplayClusterRootActor_SetReplaceTextureFlagForAllViewports, "SetReplaceTextureFlagForAllViewports" }, // 1287089144
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * VR root. This contains nDisplay VR hierarchy in the game.\n */" },
		{ "DisplayName", "nDisplay Root Actor" },
		{ "IncludePath", "DisplayClusterRootActor.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "VR root. This contains nDisplay VR hierarchy in the game." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_ConfigDataName_MetaData[] = {
		{ "Comment", "/**\n\x09 * Name of the CurrentConfigData asset. Only required if this is a parent of a DisplayClusterBlueprint.\n\x09 * The name is used to lookup the config data as a default sub-object, specifically in packaged builds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Name of the CurrentConfigData asset. Only required if this is a parent of a DisplayClusterBlueprint.\nThe name is used to lookup the config data as a default sub-object, specifically in packaged builds." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_ConfigDataName = { "ConfigDataName", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, ConfigDataName), METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_ConfigDataName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_ConfigDataName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_CurrentConfigData_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "NDisplay" },
		{ "Comment", "/**\n\x09 * If set from the DisplayCluster BP Compiler it will be loaded from the class default subobjects in run-time.\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "If set from the DisplayCluster BP Compiler it will be loaded from the class default subobjects in run-time." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_CurrentConfigData = { "CurrentConfigData", nullptr, (EPropertyFlags)0x00420000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, CurrentConfigData), Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_CurrentConfigData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_CurrentConfigData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DisplayClusterRootComponent_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/**\n\x09 * The root component for our hierarchy.\n\x09 * Must have CPF_Edit(such as VisibleDefaultsOnly) on property for Live Link.\n\x09 * nDisplay details panel will hide this from actually being visible.\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "The root component for our hierarchy.\nMust have CPF_Edit(such as VisibleDefaultsOnly) on property for Live Link.\nnDisplay details panel will hide this from actually being visible." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DisplayClusterRootComponent = { "DisplayClusterRootComponent", nullptr, (EPropertyFlags)0x0040000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, DisplayClusterRootComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DisplayClusterRootComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DisplayClusterRootComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DefaultViewPoint_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/**\n\x09 * Default camera component. It's an outer camera in VP/ICVFX terminology. Always exists on a DCRA instance.\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Default camera component. It's an outer camera in VP/ICVFX terminology. Always exists on a DCRA instance." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DefaultViewPoint = { "DefaultViewPoint", nullptr, (EPropertyFlags)0x00400000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, DefaultViewPoint), Z_Construct_UClass_UDisplayClusterCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DefaultViewPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DefaultViewPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_SyncTickComponent_MetaData[] = {
		{ "Comment", "/**\n\x09 * Helper sync component. Performs sync procedure during Tick phase.\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Helper sync component. Performs sync procedure during Tick phase." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_SyncTickComponent = { "SyncTickComponent", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, SyncTickComponent), Z_Construct_UClass_UDisplayClusterSyncTickComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_SyncTickComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_SyncTickComponent_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_InnerFrustumPriority_Inner = { "InnerFrustumPriority", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisplayClusterComponentRef, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_InnerFrustumPriority_MetaData[] = {
		{ "Category", "In Camera VFX" },
		{ "Comment", "/** Set the priority for inner frustum rendering if there is any overlap when enabling multiple ICVFX cameras. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "TitleProperty", "Name" },
		{ "ToolTip", "Set the priority for inner frustum rendering if there is any overlap when enabling multiple ICVFX cameras." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_InnerFrustumPriority = { "InnerFrustumPriority", nullptr, (EPropertyFlags)0x0010000000000841, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, InnerFrustumPriority), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_InnerFrustumPriority_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_InnerFrustumPriority_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bPreviewEnable_MetaData[] = {
		{ "Category", "Editor Preview" },
		{ "Comment", "/** Render the scene and display it as a preview on the nDisplay root actor in the editor.  This will impact editor performance. */" },
		{ "DisplayName", "Enable Editor Preview" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Render the scene and display it as a preview on the nDisplay root actor in the editor.  This will impact editor performance." },
	};
#endif
	void Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bPreviewEnable_SetBit(void* Obj)
	{
		((ADisplayClusterRootActor*)Obj)->bPreviewEnable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bPreviewEnable = { "bPreviewEnable", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADisplayClusterRootActor), &Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bPreviewEnable_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bPreviewEnable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bPreviewEnable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewNodeId_MetaData[] = {
		{ "Category", "Editor Preview" },
		{ "Comment", "/** Selectively preview a specific viewport or show all/none. */" },
		{ "DisplayName", "Preview Node" },
		{ "EditCondition", "bPreviewEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Selectively preview a specific viewport or show all/none." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewNodeId = { "PreviewNodeId", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, PreviewNodeId), METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewNodeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewNodeId_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_RenderMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_RenderMode_MetaData[] = {
		{ "Category", "Editor Preview" },
		{ "Comment", "/** Render Mode */" },
		{ "EditCondition", "bPreviewEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Render Mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_RenderMode = { "RenderMode", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, RenderMode), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationRenderMode, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_RenderMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_RenderMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_TickPerFrame_MetaData[] = {
		{ "Category", "Editor Preview" },
		{ "ClampMax", "200" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Tick Per Frame */" },
		{ "EditCondition", "bPreviewEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Tick Per Frame" },
		{ "UIMax", "200" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_TickPerFrame = { "TickPerFrame", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, TickPerFrame), METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_TickPerFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_TickPerFrame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewRenderTargetRatioMult_MetaData[] = {
		{ "Category", "Editor Preview" },
		{ "ClampMax", "1" },
		{ "ClampMin", "0.05" },
		{ "Comment", "/** Adjust resolution scaling for the editor preview. */" },
		{ "DisplayName", "Preview Screen Percentage" },
		{ "EditCondition", "bPreviewEnable" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
		{ "ToolTip", "Adjust resolution scaling for the editor preview." },
		{ "UIMax", "1" },
		{ "UIMin", "0.05" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewRenderTargetRatioMult = { "PreviewRenderTargetRatioMult", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, PreviewRenderTargetRatioMult), METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewRenderTargetRatioMult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewRenderTargetRatioMult_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents_ValueProp = { "PreviewComponents", nullptr, (EPropertyFlags)0x0000000800080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDisplayClusterPreviewComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents_Key_KeyProp = { "PreviewComponents_Key", nullptr, (EPropertyFlags)0x0000000800080008, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents = { "PreviewComponents", nullptr, (EPropertyFlags)0x0040008800002008, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterRootActor, PreviewComponents), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bDeferPreviewGeneration_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterRootActor.h" },
	};
#endif
	void Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bDeferPreviewGeneration_SetBit(void* Obj)
	{
		((ADisplayClusterRootActor*)Obj)->bDeferPreviewGeneration = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bDeferPreviewGeneration = { "bDeferPreviewGeneration", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ADisplayClusterRootActor), &Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bDeferPreviewGeneration_SetBit, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bDeferPreviewGeneration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bDeferPreviewGeneration_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADisplayClusterRootActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_ConfigDataName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_CurrentConfigData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DisplayClusterRootComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_DefaultViewPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_SyncTickComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_InnerFrustumPriority_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_InnerFrustumPriority,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bPreviewEnable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewNodeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_RenderMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_RenderMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_TickPerFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewRenderTargetRatioMult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_PreviewComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterRootActor_Statics::NewProp_bDeferPreviewGeneration,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADisplayClusterRootActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADisplayClusterRootActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADisplayClusterRootActor_Statics::ClassParams = {
		&ADisplayClusterRootActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ADisplayClusterRootActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterRootActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterRootActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADisplayClusterRootActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADisplayClusterRootActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADisplayClusterRootActor, 3650924302);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<ADisplayClusterRootActor>()
	{
		return ADisplayClusterRootActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADisplayClusterRootActor(Z_Construct_UClass_ADisplayClusterRootActor, &ADisplayClusterRootActor::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("ADisplayClusterRootActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADisplayClusterRootActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
