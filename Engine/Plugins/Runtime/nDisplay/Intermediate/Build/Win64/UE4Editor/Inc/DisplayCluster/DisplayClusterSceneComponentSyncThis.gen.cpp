// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Components/DisplayClusterSceneComponentSyncThis.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterSceneComponentSyncThis() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSync();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
// End Cross Module References
	void UDisplayClusterSceneComponentSyncThis::StaticRegisterNativesUDisplayClusterSceneComponentSyncThis()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_NoRegister()
	{
		return UDisplayClusterSceneComponentSyncThis::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterSceneComponentSync,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "DisplayCluster" },
		{ "Comment", "/**\n * Synchronization component. Synchronizes himself\n */" },
		{ "DisplayName", "NDisplay Sync This" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Components/DisplayClusterSceneComponentSyncThis.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterSceneComponentSyncThis.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Synchronization component. Synchronizes himself" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterSceneComponentSyncThis>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics::ClassParams = {
		&UDisplayClusterSceneComponentSyncThis::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterSceneComponentSyncThis, 3062592396);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterSceneComponentSyncThis>()
	{
		return UDisplayClusterSceneComponentSyncThis::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterSceneComponentSyncThis(Z_Construct_UClass_UDisplayClusterSceneComponentSyncThis, &UDisplayClusterSceneComponentSyncThis::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterSceneComponentSyncThis"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterSceneComponentSyncThis);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
