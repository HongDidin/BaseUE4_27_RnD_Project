// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERSTAGEMONITORING_DisplayClusterStageMonitoringSettings_generated_h
#error "DisplayClusterStageMonitoringSettings.generated.h already included, missing '#pragma once' in DisplayClusterStageMonitoringSettings.h"
#endif
#define DISPLAYCLUSTERSTAGEMONITORING_DisplayClusterStageMonitoringSettings_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterStageMonitoringSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterStageMonitoringSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterStageMonitoringSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterStageMonitoring"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterStageMonitoringSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterStageMonitoringSettings(); \
	friend struct Z_Construct_UClass_UDisplayClusterStageMonitoringSettings_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterStageMonitoringSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayClusterStageMonitoring"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterStageMonitoringSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterStageMonitoringSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterStageMonitoringSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterStageMonitoringSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterStageMonitoringSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterStageMonitoringSettings(UDisplayClusterStageMonitoringSettings&&); \
	NO_API UDisplayClusterStageMonitoringSettings(const UDisplayClusterStageMonitoringSettings&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterStageMonitoringSettings(UDisplayClusterStageMonitoringSettings&&); \
	NO_API UDisplayClusterStageMonitoringSettings(const UDisplayClusterStageMonitoringSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterStageMonitoringSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterStageMonitoringSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterStageMonitoringSettings)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bEnableDWMHitchDetection() { return STRUCT_OFFSET(UDisplayClusterStageMonitoringSettings, bEnableDWMHitchDetection); } \
	FORCEINLINE static uint32 __PPO__bEnableNvidiaHitchDetection() { return STRUCT_OFFSET(UDisplayClusterStageMonitoringSettings, bEnableNvidiaHitchDetection); }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_13_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERSTAGEMONITORING_API UClass* StaticClass<class UDisplayClusterStageMonitoringSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterStageMonitoring_Private_DisplayClusterStageMonitoringSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
