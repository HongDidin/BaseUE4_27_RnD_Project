// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorGraph_generated_h
#error "DisplayClusterConfiguratorGraph.generated.h already included, missing '#pragma once' in DisplayClusterConfiguratorGraph.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorGraph_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorGraph(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorGraph_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorGraph)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorGraph(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorGraph_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorGraph, UEdGraph, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorGraph)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorGraph(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorGraph) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorGraph); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorGraph(UDisplayClusterConfiguratorGraph&&); \
	NO_API UDisplayClusterConfiguratorGraph(const UDisplayClusterConfiguratorGraph&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorGraph(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorGraph(UDisplayClusterConfiguratorGraph&&); \
	NO_API UDisplayClusterConfiguratorGraph(const UDisplayClusterConfiguratorGraph&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorGraph); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorGraph); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorGraph)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_23_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorGraph>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_DisplayClusterConfiguratorGraph_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
