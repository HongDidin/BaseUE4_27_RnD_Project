// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterStageMonitoring/Private/DWMSyncWatchdog.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDWMSyncWatchdog() {}
// Cross Module References
	DISPLAYCLUSTERSTAGEMONITORING_API UScriptStruct* Z_Construct_UScriptStruct_FDWMSyncEvent();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterStageMonitoring();
	STAGEDATACORE_API UScriptStruct* Z_Construct_UScriptStruct_FStageProviderEventMessage();
// End Cross Module References

static_assert(std::is_polymorphic<FDWMSyncEvent>() == std::is_polymorphic<FStageProviderEventMessage>(), "USTRUCT FDWMSyncEvent cannot be polymorphic unless super FStageProviderEventMessage is polymorphic");

class UScriptStruct* FDWMSyncEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERSTAGEMONITORING_API uint32 Get_Z_Construct_UScriptStruct_FDWMSyncEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDWMSyncEvent, Z_Construct_UPackage__Script_DisplayClusterStageMonitoring(), TEXT("DWMSyncEvent"), sizeof(FDWMSyncEvent), Get_Z_Construct_UScriptStruct_FDWMSyncEvent_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERSTAGEMONITORING_API UScriptStruct* StaticStruct<FDWMSyncEvent>()
{
	return FDWMSyncEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDWMSyncEvent(FDWMSyncEvent::StaticStruct, TEXT("/Script/DisplayClusterStageMonitoring"), TEXT("DWMSyncEvent"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterStageMonitoring_StaticRegisterNativesFDWMSyncEvent
{
	FScriptStruct_DisplayClusterStageMonitoring_StaticRegisterNativesFDWMSyncEvent()
	{
		UScriptStruct::DeferCppStructOps<FDWMSyncEvent>(FName(TEXT("DWMSyncEvent")));
	}
} ScriptStruct_DisplayClusterStageMonitoring_StaticRegisterNativesFDWMSyncEvent;
	struct Z_Construct_UScriptStruct_FDWMSyncEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MissedFrames_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_MissedFrames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresentCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_PresentCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastPresentCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_LastPresentCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresentRefreshCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_PresentRefreshCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/DWMSyncWatchdog.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDWMSyncEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_MissedFrames_MetaData[] = {
		{ "Category", "NvidiaSync" },
		{ "Comment", "/** Number of sync counts that were missed between presents */" },
		{ "ModuleRelativePath", "Private/DWMSyncWatchdog.h" },
		{ "ToolTip", "Number of sync counts that were missed between presents" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_MissedFrames = { "MissedFrames", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDWMSyncEvent, MissedFrames), METADATA_PARAMS(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_MissedFrames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_MissedFrames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentCount_MetaData[] = {
		{ "Category", "NvidiaSync" },
		{ "ModuleRelativePath", "Private/DWMSyncWatchdog.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentCount = { "PresentCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDWMSyncEvent, PresentCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_LastPresentCount_MetaData[] = {
		{ "Category", "NvidiaSync" },
		{ "ModuleRelativePath", "Private/DWMSyncWatchdog.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_LastPresentCount = { "LastPresentCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDWMSyncEvent, LastPresentCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_LastPresentCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_LastPresentCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentRefreshCount_MetaData[] = {
		{ "Category", "NvidiaSync" },
		{ "ModuleRelativePath", "Private/DWMSyncWatchdog.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentRefreshCount = { "PresentRefreshCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDWMSyncEvent, PresentRefreshCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentRefreshCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentRefreshCount_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_MissedFrames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_LastPresentCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::NewProp_PresentRefreshCount,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterStageMonitoring,
		Z_Construct_UScriptStruct_FStageProviderEventMessage,
		&NewStructOps,
		"DWMSyncEvent",
		sizeof(FDWMSyncEvent),
		alignof(FDWMSyncEvent),
		Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDWMSyncEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDWMSyncEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterStageMonitoring();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DWMSyncEvent"), sizeof(FDWMSyncEvent), Get_Z_Construct_UScriptStruct_FDWMSyncEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDWMSyncEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDWMSyncEvent_Hash() { return 3557432637U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
