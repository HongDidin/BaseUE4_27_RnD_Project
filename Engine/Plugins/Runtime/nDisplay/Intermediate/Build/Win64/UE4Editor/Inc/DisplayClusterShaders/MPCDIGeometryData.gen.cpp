// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterShaders/Public/Blueprints/MPCDIGeometryData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMPCDIGeometryData() {}
// Cross Module References
	DISPLAYCLUSTERSHADERS_API UScriptStruct* Z_Construct_UScriptStruct_FMPCDIGeometryExportData();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterShaders();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	DISPLAYCLUSTERSHADERS_API UScriptStruct* Z_Construct_UScriptStruct_FMPCDIGeometryImportData();
// End Cross Module References
class UScriptStruct* FMPCDIGeometryExportData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERSHADERS_API uint32 Get_Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMPCDIGeometryExportData, Z_Construct_UPackage__Script_DisplayClusterShaders(), TEXT("MPCDIGeometryExportData"), sizeof(FMPCDIGeometryExportData), Get_Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERSHADERS_API UScriptStruct* StaticStruct<FMPCDIGeometryExportData>()
{
	return FMPCDIGeometryExportData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMPCDIGeometryExportData(FMPCDIGeometryExportData::StaticStruct, TEXT("/Script/DisplayClusterShaders"), TEXT("MPCDIGeometryExportData"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterShaders_StaticRegisterNativesFMPCDIGeometryExportData
{
	FScriptStruct_DisplayClusterShaders_StaticRegisterNativesFMPCDIGeometryExportData()
	{
		UScriptStruct::DeferCppStructOps<FMPCDIGeometryExportData>(FName(TEXT("MPCDIGeometryExportData")));
	}
} ScriptStruct_DisplayClusterShaders_StaticRegisterNativesFMPCDIGeometryExportData;
	struct Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertices_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vertices;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normal_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Normal;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UV_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UV_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UV;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Triangles_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Triangles_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Triangles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMPCDIGeometryExportData>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Vertices_Inner = { "Vertices", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Vertices_MetaData[] = {
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Vertices = { "Vertices", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMPCDIGeometryExportData, Vertices), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Vertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Vertices_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Normal_Inner = { "Normal", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Normal_MetaData[] = {
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMPCDIGeometryExportData, Normal), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Normal_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_UV_Inner = { "UV", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_UV_MetaData[] = {
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_UV = { "UV", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMPCDIGeometryExportData, UV), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_UV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_UV_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Triangles_Inner = { "Triangles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Triangles_MetaData[] = {
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Triangles = { "Triangles", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMPCDIGeometryExportData, Triangles), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Triangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Triangles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Vertices_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Vertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Normal_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Normal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_UV_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_UV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Triangles_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::NewProp_Triangles,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterShaders,
		nullptr,
		&NewStructOps,
		"MPCDIGeometryExportData",
		sizeof(FMPCDIGeometryExportData),
		alignof(FMPCDIGeometryExportData),
		Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMPCDIGeometryExportData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterShaders();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MPCDIGeometryExportData"), sizeof(FMPCDIGeometryExportData), Get_Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMPCDIGeometryExportData_Hash() { return 930929707U; }
class UScriptStruct* FMPCDIGeometryImportData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERSHADERS_API uint32 Get_Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMPCDIGeometryImportData, Z_Construct_UPackage__Script_DisplayClusterShaders(), TEXT("MPCDIGeometryImportData"), sizeof(FMPCDIGeometryImportData), Get_Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERSHADERS_API UScriptStruct* StaticStruct<FMPCDIGeometryImportData>()
{
	return FMPCDIGeometryImportData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMPCDIGeometryImportData(FMPCDIGeometryImportData::StaticStruct, TEXT("/Script/DisplayClusterShaders"), TEXT("MPCDIGeometryImportData"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterShaders_StaticRegisterNativesFMPCDIGeometryImportData
{
	FScriptStruct_DisplayClusterShaders_StaticRegisterNativesFMPCDIGeometryImportData()
	{
		UScriptStruct::DeferCppStructOps<FMPCDIGeometryImportData>(FName(TEXT("MPCDIGeometryImportData")));
	}
} ScriptStruct_DisplayClusterShaders_StaticRegisterNativesFMPCDIGeometryImportData;
	struct Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Height;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Vertices_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Vertices_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Vertices;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMPCDIGeometryImportData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMPCDIGeometryImportData, Width), METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMPCDIGeometryImportData, Height), METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Height_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Vertices_Inner = { "Vertices", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Vertices_MetaData[] = {
		{ "Category", "MPCDI" },
		{ "ModuleRelativePath", "Public/Blueprints/MPCDIGeometryData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Vertices = { "Vertices", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMPCDIGeometryImportData, Vertices), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Vertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Vertices_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Vertices_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::NewProp_Vertices,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterShaders,
		nullptr,
		&NewStructOps,
		"MPCDIGeometryImportData",
		sizeof(FMPCDIGeometryImportData),
		alignof(FMPCDIGeometryImportData),
		Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMPCDIGeometryImportData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterShaders();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MPCDIGeometryImportData"), sizeof(FMPCDIGeometryImportData), Get_Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMPCDIGeometryImportData_Hash() { return 4276705630U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
