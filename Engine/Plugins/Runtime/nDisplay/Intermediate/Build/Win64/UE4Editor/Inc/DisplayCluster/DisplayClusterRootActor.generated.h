// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UDisplayClusterCameraComponent;
#ifdef DISPLAYCLUSTER_DisplayClusterRootActor_generated_h
#error "DisplayClusterRootActor.generated.h already included, missing '#pragma once' in DisplayClusterRootActor.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterRootActor_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetReplaceTextureFlagForAllViewports); \
	DECLARE_FUNCTION(execGetDefaultCamera);


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetReplaceTextureFlagForAllViewports); \
	DECLARE_FUNCTION(execGetDefaultCamera);


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADisplayClusterRootActor(); \
	friend struct Z_Construct_UClass_ADisplayClusterRootActor_Statics; \
public: \
	DECLARE_CLASS(ADisplayClusterRootActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(ADisplayClusterRootActor)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_INCLASS \
private: \
	static void StaticRegisterNativesADisplayClusterRootActor(); \
	friend struct Z_Construct_UClass_ADisplayClusterRootActor_Statics; \
public: \
	DECLARE_CLASS(ADisplayClusterRootActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(ADisplayClusterRootActor)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADisplayClusterRootActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADisplayClusterRootActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADisplayClusterRootActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADisplayClusterRootActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADisplayClusterRootActor(ADisplayClusterRootActor&&); \
	NO_API ADisplayClusterRootActor(const ADisplayClusterRootActor&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADisplayClusterRootActor(ADisplayClusterRootActor&&); \
	NO_API ADisplayClusterRootActor(const ADisplayClusterRootActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADisplayClusterRootActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADisplayClusterRootActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADisplayClusterRootActor)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ConfigDataName() { return STRUCT_OFFSET(ADisplayClusterRootActor, ConfigDataName); } \
	FORCEINLINE static uint32 __PPO__CurrentConfigData() { return STRUCT_OFFSET(ADisplayClusterRootActor, CurrentConfigData); } \
	FORCEINLINE static uint32 __PPO__DisplayClusterRootComponent() { return STRUCT_OFFSET(ADisplayClusterRootActor, DisplayClusterRootComponent); } \
	FORCEINLINE static uint32 __PPO__DefaultViewPoint() { return STRUCT_OFFSET(ADisplayClusterRootActor, DefaultViewPoint); } \
	FORCEINLINE static uint32 __PPO__SyncTickComponent() { return STRUCT_OFFSET(ADisplayClusterRootActor, SyncTickComponent); }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_41_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class ADisplayClusterRootActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterRootActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
