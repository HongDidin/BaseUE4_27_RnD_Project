// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterViewportClient_generated_h
#error "DisplayClusterViewportClient.generated.h already included, missing '#pragma once' in DisplayClusterViewportClient.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterViewportClient_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterViewportClient(); \
	friend struct Z_Construct_UClass_UDisplayClusterViewportClient_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterViewportClient, UGameViewportClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterViewportClient)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterViewportClient(); \
	friend struct Z_Construct_UClass_UDisplayClusterViewportClient_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterViewportClient, UGameViewportClient, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterViewportClient)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterViewportClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterViewportClient) \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterViewportClient); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterViewportClient(UDisplayClusterViewportClient&&); \
	NO_API UDisplayClusterViewportClient(const UDisplayClusterViewportClient&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterViewportClient(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterViewportClient(UDisplayClusterViewportClient&&); \
	NO_API UDisplayClusterViewportClient(const UDisplayClusterViewportClient&); \
public: \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterViewportClient); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterViewportClient)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_9_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterViewportClient>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_DisplayClusterViewportClient_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
