// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationTypes_Postprocess_generated_h
#error "DisplayClusterConfigurationTypes_Postprocess.generated.h already included, missing '#pragma once' in DisplayClusterConfigurationTypes_Postprocess.h"
#endif
#define DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationTypes_Postprocess_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_284_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_PerNodeColorGrading_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_PerNodeColorGrading>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_266_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_AllNodesColorGrading_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_AllNodesColorGrading>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_244_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_PerViewportColorGrading_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_PerViewportColorGrading>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_230_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_EntireClusterColorGrading_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_EntireClusterColorGrading>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_165_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ColorGradingRenderingSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_ColorGradingRenderingSettings>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_130_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ColorGradingMiscSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_ColorGradingMiscSettings>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_95_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ColorGradingWhiteBalanceSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_ColorGradingWhiteBalanceSettings>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_58_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_ColorGradingSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_ColorGradingSettings>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_42_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_CustomPostprocess_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_CustomPostprocess>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationViewport_CustomPostprocessSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationViewport_CustomPostprocessSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_Postprocess_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
