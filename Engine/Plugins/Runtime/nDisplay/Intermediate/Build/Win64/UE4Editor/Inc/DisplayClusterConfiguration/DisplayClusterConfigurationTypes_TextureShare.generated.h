// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationTypes_TextureShare_generated_h
#error "DisplayClusterConfigurationTypes_TextureShare.generated.h already included, missing '#pragma once' in DisplayClusterConfigurationTypes_TextureShare.h"
#endif
#define DISPLAYCLUSTERCONFIGURATION_DisplayClusterConfigurationTypes_TextureShare_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_TextureShare_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationTextureShare_Viewport_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FDisplayClusterConfigurationTextureShare_Viewport>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_TextureShare_h_53_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTextureShareSyncPolicyDisplayCluster_Statics; \
	DISPLAYCLUSTERCONFIGURATION_API static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<struct FTextureShareSyncPolicyDisplayCluster>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfiguration_Public_DisplayClusterConfigurationTypes_TextureShare_h


#define FOREACH_ENUM_ETEXTURESHARESYNCSURFACEDISPLAYCLUSTER(op) \
	op(ETextureShareSyncSurfaceDisplayCluster::Default) \
	op(ETextureShareSyncSurfaceDisplayCluster::None) \
	op(ETextureShareSyncSurfaceDisplayCluster::SyncRead) \
	op(ETextureShareSyncSurfaceDisplayCluster::SyncPairingRead) 

enum class ETextureShareSyncSurfaceDisplayCluster : uint8;
template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<ETextureShareSyncSurfaceDisplayCluster>();

#define FOREACH_ENUM_ETEXTURESHARESYNCFRAMEDISPLAYCLUSTER(op) \
	op(ETextureShareSyncFrameDisplayCluster::Default) \
	op(ETextureShareSyncFrameDisplayCluster::None) \
	op(ETextureShareSyncFrameDisplayCluster::FrameSync) 

enum class ETextureShareSyncFrameDisplayCluster : uint8;
template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<ETextureShareSyncFrameDisplayCluster>();

#define FOREACH_ENUM_ETEXTURESHARESYNCCONNECTDISPLAYCLUSTER(op) \
	op(ETextureShareSyncConnectDisplayCluster::Default) \
	op(ETextureShareSyncConnectDisplayCluster::None) \
	op(ETextureShareSyncConnectDisplayCluster::SyncSession) 

enum class ETextureShareSyncConnectDisplayCluster : uint8;
template<> DISPLAYCLUSTERCONFIGURATION_API UEnum* StaticEnum<ETextureShareSyncConnectDisplayCluster>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
