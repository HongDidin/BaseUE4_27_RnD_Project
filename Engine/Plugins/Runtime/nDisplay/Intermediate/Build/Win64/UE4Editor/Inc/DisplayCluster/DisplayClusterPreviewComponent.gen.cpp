// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Components/DisplayClusterPreviewComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterPreviewComponent() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterPreviewComponent_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterPreviewComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_ADisplayClusterRootActor_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationViewport_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
// End Cross Module References
	void UDisplayClusterPreviewComponent::StaticRegisterNativesUDisplayClusterPreviewComponent()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterPreviewComponent_NoRegister()
	{
		return UDisplayClusterPreviewComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WarpMeshSavedProjectionPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WarpMeshSavedProjectionPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ViewportConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsRootActorPreviewMesh_MetaData[];
#endif
		static void NewProp_bIsRootActorPreviewMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsRootActorPreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OriginalMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMaterialInstance_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMaterialInstance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "DisplayCluster" },
		{ "Comment", "/**\n * nDisplay Viewport preview component (Editor)\n */" },
		{ "IncludePath", "Components/DisplayClusterPreviewComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "nDisplay Viewport preview component (Editor)" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RenderTarget_MetaData[] = {
		{ "Category", "Preview" },
		{ "DisplayName", "Render Target" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0020080800020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RenderTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_WarpMeshSavedProjectionPolicy_MetaData[] = {
		{ "Comment", "// Saved mesh policy params\n" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
		{ "ToolTip", "Saved mesh policy params" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_WarpMeshSavedProjectionPolicy = { "WarpMeshSavedProjectionPolicy", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, WarpMeshSavedProjectionPolicy), Z_Construct_UScriptStruct_FDisplayClusterConfigurationProjection, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_WarpMeshSavedProjectionPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_WarpMeshSavedProjectionPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RootActor_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RootActor = { "RootActor", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, RootActor), Z_Construct_UClass_ADisplayClusterRootActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RootActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RootActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportId_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, ViewportId), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportConfig_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportConfig = { "ViewportConfig", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, ViewportConfig), Z_Construct_UClass_UDisplayClusterConfigurationViewport_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0040000800080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, PreviewMesh), Z_Construct_UClass_UMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_bIsRootActorPreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_bIsRootActorPreviewMesh_SetBit(void* Obj)
	{
		((UDisplayClusterPreviewComponent*)Obj)->bIsRootActorPreviewMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_bIsRootActorPreviewMesh = { "bIsRootActorPreviewMesh", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterPreviewComponent), &Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_bIsRootActorPreviewMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_bIsRootActorPreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_bIsRootActorPreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_OriginalMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_OriginalMaterial = { "OriginalMaterial", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, OriginalMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_OriginalMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_OriginalMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterial = { "PreviewMaterial", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, PreviewMaterial), Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterialInstance_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterialInstance = { "PreviewMaterialInstance", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, PreviewMaterialInstance), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterialInstance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterialInstance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewTexture_MetaData[] = {
		{ "ModuleRelativePath", "Public/Components/DisplayClusterPreviewComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewTexture = { "PreviewTexture", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterPreviewComponent, PreviewTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_WarpMeshSavedProjectionPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_RootActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_ViewportConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_bIsRootActorPreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_OriginalMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewMaterialInstance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::NewProp_PreviewTexture,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterPreviewComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::ClassParams = {
		&UDisplayClusterPreviewComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::PropPointers), 0),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterPreviewComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterPreviewComponent, 1120609390);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterPreviewComponent>()
	{
		return UDisplayClusterPreviewComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterPreviewComponent(Z_Construct_UClass_UDisplayClusterPreviewComponent, &UDisplayClusterPreviewComponent::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterPreviewComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterPreviewComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
