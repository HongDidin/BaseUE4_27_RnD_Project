// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Components/DisplayClusterSceneComponentSync.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterSceneComponentSync() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSync_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSync();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
// End Cross Module References
	void UDisplayClusterSceneComponentSync::StaticRegisterNativesUDisplayClusterSceneComponentSync()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSync_NoRegister()
	{
		return UDisplayClusterSceneComponentSync::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Abstract synchronization component\n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "Components/DisplayClusterSceneComponentSync.h" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterSceneComponentSync.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Abstract synchronization component" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterSceneComponentSync>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics::ClassParams = {
		&UDisplayClusterSceneComponentSync::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A5u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterSceneComponentSync()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterSceneComponentSync, 1841527986);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterSceneComponentSync>()
	{
		return UDisplayClusterSceneComponentSync::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterSceneComponentSync(Z_Construct_UClass_UDisplayClusterSceneComponentSync, &UDisplayClusterSceneComponentSync::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterSceneComponentSync"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterSceneComponentSync);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
