// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorFactory_generated_h
#error "DisplayClusterConfiguratorFactory.generated.h already included, missing '#pragma once' in DisplayClusterConfiguratorFactory.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorFactory_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorFactory(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorFactory_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), DISPLAYCLUSTERCONFIGURATOR_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorFactory)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorFactory(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorFactory_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), DISPLAYCLUSTERCONFIGURATOR_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorFactory)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DISPLAYCLUSTERCONFIGURATOR_API, UDisplayClusterConfiguratorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorFactory(UDisplayClusterConfiguratorFactory&&); \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorFactory(const UDisplayClusterConfiguratorFactory&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorFactory(UDisplayClusterConfiguratorFactory&&); \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorFactory(const UDisplayClusterConfiguratorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DISPLAYCLUSTERCONFIGURATOR_API, UDisplayClusterConfiguratorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorFactory)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ParentClass() { return STRUCT_OFFSET(UDisplayClusterConfiguratorFactory, ParentClass); } \
	FORCEINLINE static uint32 __PPO__BlueprintToCopy() { return STRUCT_OFFSET(UDisplayClusterConfiguratorFactory, BlueprintToCopy); }


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_14_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorFactory>();

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorReimportFactory(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorReimportFactory_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorReimportFactory, UDisplayClusterConfiguratorFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), DISPLAYCLUSTERCONFIGURATOR_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorReimportFactory)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorReimportFactory(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorReimportFactory_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorReimportFactory, UDisplayClusterConfiguratorFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), DISPLAYCLUSTERCONFIGURATOR_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorReimportFactory)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorReimportFactory(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorReimportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DISPLAYCLUSTERCONFIGURATOR_API, UDisplayClusterConfiguratorReimportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorReimportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorReimportFactory(UDisplayClusterConfiguratorReimportFactory&&); \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorReimportFactory(const UDisplayClusterConfiguratorReimportFactory&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorReimportFactory(UDisplayClusterConfiguratorReimportFactory&&); \
	DISPLAYCLUSTERCONFIGURATOR_API UDisplayClusterConfiguratorReimportFactory(const UDisplayClusterConfiguratorReimportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DISPLAYCLUSTERCONFIGURATOR_API, UDisplayClusterConfiguratorReimportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorReimportFactory); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorReimportFactory)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_57_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h_62_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorReimportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_DisplayClusterConfiguratorFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
