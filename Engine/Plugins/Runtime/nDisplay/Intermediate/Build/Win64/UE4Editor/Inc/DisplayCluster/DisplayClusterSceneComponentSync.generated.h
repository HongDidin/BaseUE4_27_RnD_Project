// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterSceneComponentSync_generated_h
#error "DisplayClusterSceneComponentSync.generated.h already included, missing '#pragma once' in DisplayClusterSceneComponentSync.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterSceneComponentSync_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterSceneComponentSync(); \
	friend struct Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterSceneComponentSync, USceneComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterSceneComponentSync)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterSceneComponentSync(); \
	friend struct Z_Construct_UClass_UDisplayClusterSceneComponentSync_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterSceneComponentSync, USceneComponent, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterSceneComponentSync)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterSceneComponentSync(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterSceneComponentSync) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterSceneComponentSync); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterSceneComponentSync); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterSceneComponentSync(UDisplayClusterSceneComponentSync&&); \
	NO_API UDisplayClusterSceneComponentSync(const UDisplayClusterSceneComponentSync&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterSceneComponentSync(UDisplayClusterSceneComponentSync&&); \
	NO_API UDisplayClusterSceneComponentSync(const UDisplayClusterSceneComponentSync&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterSceneComponentSync); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterSceneComponentSync); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterSceneComponentSync)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_16_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterSceneComponentSync>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterSceneComponentSync_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
