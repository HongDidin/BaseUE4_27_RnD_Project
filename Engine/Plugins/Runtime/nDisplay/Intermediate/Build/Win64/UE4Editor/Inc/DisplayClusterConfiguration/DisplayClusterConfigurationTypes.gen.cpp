// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayClusterConfiguration/Public/DisplayClusterConfigurationTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterConfigurationTypes() {}
// Cross Module References
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics();
	UPackage* Z_Construct_UPackage__Script_DisplayClusterConfiguration();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_Base();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera();
	DISPLAYCLUSTERCONFIGURATION_API UEnum* Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationEyeStereoOffset();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationScene_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationScene();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationClusterNode();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationViewport_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationCluster_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationCluster();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister();
	DISPLAYCLUSTERCONFIGURATION_API UClass* Z_Construct_UClass_UDisplayClusterConfigurationData();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame();
	DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings();
// End Cross Module References
class UScriptStruct* FDisplayClusterConfigurationDiagnostics::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationDiagnostics"), sizeof(FDisplayClusterConfigurationDiagnostics), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationDiagnostics>()
{
	return FDisplayClusterConfigurationDiagnostics::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationDiagnostics(FDisplayClusterConfigurationDiagnostics::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationDiagnostics"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationDiagnostics
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationDiagnostics()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationDiagnostics>(FName(TEXT("DisplayClusterConfigurationDiagnostics")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationDiagnostics;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSimulateLag_MetaData[];
#endif
		static void NewProp_bSimulateLag_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSimulateLag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinLagTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinLagTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxLagTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxLagTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationDiagnostics>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_bSimulateLag_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_bSimulateLag_SetBit(void* Obj)
	{
		((FDisplayClusterConfigurationDiagnostics*)Obj)->bSimulateLag = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_bSimulateLag = { "bSimulateLag", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterConfigurationDiagnostics), &Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_bSimulateLag_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_bSimulateLag_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_bSimulateLag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MinLagTime_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MinLagTime = { "MinLagTime", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationDiagnostics, MinLagTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MinLagTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MinLagTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MaxLagTime_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MaxLagTime = { "MaxLagTime", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationDiagnostics, MaxLagTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MaxLagTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MaxLagTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_bSimulateLag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MinLagTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::NewProp_MaxLagTime,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationDiagnostics",
		sizeof(FDisplayClusterConfigurationDiagnostics),
		alignof(FDisplayClusterConfigurationDiagnostics),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationDiagnostics"), sizeof(FDisplayClusterConfigurationDiagnostics), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics_Hash() { return 2435621564U; }
class UScriptStruct* FDisplayClusterConfigurationExternalImage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationExternalImage"), sizeof(FDisplayClusterConfigurationExternalImage), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationExternalImage>()
{
	return FDisplayClusterConfigurationExternalImage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationExternalImage(FDisplayClusterConfigurationExternalImage::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationExternalImage"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationExternalImage
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationExternalImage()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationExternalImage>(FName(TEXT("DisplayClusterConfigurationExternalImage")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationExternalImage;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImagePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ImagePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationExternalImage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::NewProp_ImagePath_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::NewProp_ImagePath = { "ImagePath", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationExternalImage, ImagePath), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::NewProp_ImagePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::NewProp_ImagePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::NewProp_ImagePath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationExternalImage",
		sizeof(FDisplayClusterConfigurationExternalImage),
		alignof(FDisplayClusterConfigurationExternalImage),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationExternalImage"), sizeof(FDisplayClusterConfigurationExternalImage), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage_Hash() { return 2022837547U; }
class UScriptStruct* FDisplayClusterConfigurationNetworkSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationNetworkSettings"), sizeof(FDisplayClusterConfigurationNetworkSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationNetworkSettings>()
{
	return FDisplayClusterConfigurationNetworkSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings(FDisplayClusterConfigurationNetworkSettings::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationNetworkSettings"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationNetworkSettings
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationNetworkSettings()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationNetworkSettings>(FName(TEXT("DisplayClusterConfigurationNetworkSettings")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationNetworkSettings;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectRetriesAmount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConnectRetriesAmount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectRetryDelay_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConnectRetryDelay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameStartBarrierTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_GameStartBarrierTimeout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameStartBarrierTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameStartBarrierTimeout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameEndBarrierTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameEndBarrierTimeout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSyncBarrierTimeout_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RenderSyncBarrierTimeout;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationNetworkSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetriesAmount_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "99" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Advanced: amount of times nDisplay tries to reconnect before dropping */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: amount of times nDisplay tries to reconnect before dropping" },
		{ "UIMax", "99" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetriesAmount = { "ConnectRetriesAmount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationNetworkSettings, ConnectRetriesAmount), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetriesAmount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetriesAmount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetryDelay_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "5000" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Advanced: delay in between connection retries */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: delay in between connection retries" },
		{ "UIMax", "5000" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetryDelay = { "ConnectRetryDelay", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationNetworkSettings, ConnectRetryDelay), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetryDelay_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetryDelay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_GameStartBarrierTimeout_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "5000" },
		{ "Comment", "/** Advanced: timeout for Game Thread Barrier */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: timeout for Game Thread Barrier" },
		{ "UIMin", "5000" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_GameStartBarrierTimeout = { "GameStartBarrierTimeout", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationNetworkSettings, GameStartBarrierTimeout), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_GameStartBarrierTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_GameStartBarrierTimeout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameStartBarrierTimeout_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Advanced: timeout value for Start Frame Barrier */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: timeout value for Start Frame Barrier" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameStartBarrierTimeout = { "FrameStartBarrierTimeout", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationNetworkSettings, FrameStartBarrierTimeout), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameStartBarrierTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameStartBarrierTimeout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameEndBarrierTimeout_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Advanced: timeout value for End Frame Barrier */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: timeout value for End Frame Barrier" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameEndBarrierTimeout = { "FrameEndBarrierTimeout", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationNetworkSettings, FrameEndBarrierTimeout), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameEndBarrierTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameEndBarrierTimeout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_RenderSyncBarrierTimeout_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Advanced: timeout value for Render Sync */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: timeout value for Render Sync" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_RenderSyncBarrierTimeout = { "RenderSyncBarrierTimeout", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationNetworkSettings, RenderSyncBarrierTimeout), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_RenderSyncBarrierTimeout_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_RenderSyncBarrierTimeout_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetriesAmount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_ConnectRetryDelay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_GameStartBarrierTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameStartBarrierTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_FrameEndBarrierTimeout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::NewProp_RenderSyncBarrierTimeout,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationNetworkSettings",
		sizeof(FDisplayClusterConfigurationNetworkSettings),
		alignof(FDisplayClusterConfigurationNetworkSettings),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationNetworkSettings"), sizeof(FDisplayClusterConfigurationNetworkSettings), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings_Hash() { return 2211664112U; }
class UScriptStruct* FDisplayClusterConfigurationClusterSync::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationClusterSync"), sizeof(FDisplayClusterConfigurationClusterSync), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationClusterSync>()
{
	return FDisplayClusterConfigurationClusterSync::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationClusterSync(FDisplayClusterConfigurationClusterSync::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationClusterSync"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationClusterSync
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationClusterSync()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationClusterSync>(FName(TEXT("DisplayClusterConfigurationClusterSync")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationClusterSync;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSyncPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderSyncPolicy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputSyncPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InputSyncPolicy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationClusterSync>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_RenderSyncPolicy_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_RenderSyncPolicy = { "RenderSyncPolicy", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationClusterSync, RenderSyncPolicy), Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_RenderSyncPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_RenderSyncPolicy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_InputSyncPolicy_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_InputSyncPolicy = { "InputSyncPolicy", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationClusterSync, InputSyncPolicy), Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_InputSyncPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_InputSyncPolicy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_RenderSyncPolicy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::NewProp_InputSyncPolicy,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationClusterSync",
		sizeof(FDisplayClusterConfigurationClusterSync),
		alignof(FDisplayClusterConfigurationClusterSync),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationClusterSync"), sizeof(FDisplayClusterConfigurationClusterSync), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync_Hash() { return 152058370U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationInputSyncPolicy>() == std::is_polymorphic<FDisplayClusterConfigurationPolymorphicEntity>(), "USTRUCT FDisplayClusterConfigurationInputSyncPolicy cannot be polymorphic unless super FDisplayClusterConfigurationPolymorphicEntity is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationInputSyncPolicy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationInputSyncPolicy"), sizeof(FDisplayClusterConfigurationInputSyncPolicy), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationInputSyncPolicy>()
{
	return FDisplayClusterConfigurationInputSyncPolicy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy(FDisplayClusterConfigurationInputSyncPolicy::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationInputSyncPolicy"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationInputSyncPolicy
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationInputSyncPolicy()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationInputSyncPolicy>(FName(TEXT("DisplayClusterConfigurationInputSyncPolicy")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationInputSyncPolicy;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationInputSyncPolicy>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity,
		&NewStructOps,
		"DisplayClusterConfigurationInputSyncPolicy",
		sizeof(FDisplayClusterConfigurationInputSyncPolicy),
		alignof(FDisplayClusterConfigurationInputSyncPolicy),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationInputSyncPolicy"), sizeof(FDisplayClusterConfigurationInputSyncPolicy), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInputSyncPolicy_Hash() { return 4009316673U; }

static_assert(std::is_polymorphic<FDisplayClusterConfigurationRenderSyncPolicy>() == std::is_polymorphic<FDisplayClusterConfigurationPolymorphicEntity>(), "USTRUCT FDisplayClusterConfigurationRenderSyncPolicy cannot be polymorphic unless super FDisplayClusterConfigurationPolymorphicEntity is polymorphic");

class UScriptStruct* FDisplayClusterConfigurationRenderSyncPolicy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationRenderSyncPolicy"), sizeof(FDisplayClusterConfigurationRenderSyncPolicy), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationRenderSyncPolicy>()
{
	return FDisplayClusterConfigurationRenderSyncPolicy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy(FDisplayClusterConfigurationRenderSyncPolicy::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationRenderSyncPolicy"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRenderSyncPolicy
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRenderSyncPolicy()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationRenderSyncPolicy>(FName(TEXT("DisplayClusterConfigurationRenderSyncPolicy")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationRenderSyncPolicy;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationRenderSyncPolicy>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationPolymorphicEntity,
		&NewStructOps,
		"DisplayClusterConfigurationRenderSyncPolicy",
		sizeof(FDisplayClusterConfigurationRenderSyncPolicy),
		alignof(FDisplayClusterConfigurationRenderSyncPolicy),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationRenderSyncPolicy"), sizeof(FDisplayClusterConfigurationRenderSyncPolicy), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderSyncPolicy_Hash() { return 1696778158U; }
class UScriptStruct* FDisplayClusterConfigurationMasterNode::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationMasterNode"), sizeof(FDisplayClusterConfigurationMasterNode), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationMasterNode>()
{
	return FDisplayClusterConfigurationMasterNode::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationMasterNode(FDisplayClusterConfigurationMasterNode::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationMasterNode"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationMasterNode
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationMasterNode()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationMasterNode>(FName(TEXT("DisplayClusterConfigurationMasterNode")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationMasterNode;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Ports_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Ports;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationMasterNode>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Id_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationMasterNode, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Ports_MetaData[] = {
		{ "Category", "Configuration" },
		{ "DisplayName", "Master Node Ports" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Ports = { "Ports", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationMasterNode, Ports), Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Ports_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Ports_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::NewProp_Ports,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationMasterNode",
		sizeof(FDisplayClusterConfigurationMasterNode),
		alignof(FDisplayClusterConfigurationMasterNode),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationMasterNode"), sizeof(FDisplayClusterConfigurationMasterNode), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode_Hash() { return 1585243247U; }
class UScriptStruct* FDisplayClusterConfigurationMasterNodePorts::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationMasterNodePorts"), sizeof(FDisplayClusterConfigurationMasterNodePorts), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationMasterNodePorts>()
{
	return FDisplayClusterConfigurationMasterNodePorts::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts(FDisplayClusterConfigurationMasterNodePorts::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationMasterNodePorts"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationMasterNodePorts
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationMasterNodePorts()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationMasterNodePorts>(FName(TEXT("DisplayClusterConfigurationMasterNodePorts")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationMasterNodePorts;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ClusterSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_RenderSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterEventsJson_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ClusterEventsJson;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterEventsBinary_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ClusterEventsBinary;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "////////////////////////////////////////////////////////////////\n// Cluster\n" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "/\n Cluster" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationMasterNodePorts>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterSync_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "65535" },
		{ "ClampMin", "1024" },
		{ "Comment", "/** Advanced: network port for Cluster Sync Events */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: network port for Cluster Sync Events" },
		{ "UIMax", "65535" },
		{ "UIMin", "1024" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterSync = { "ClusterSync", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationMasterNodePorts, ClusterSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_RenderSync_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "65535" },
		{ "ClampMin", "1024" },
		{ "Comment", "/** Advanced: network port for Render Sync Events */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: network port for Render Sync Events" },
		{ "UIMax", "65535" },
		{ "UIMin", "1024" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_RenderSync = { "RenderSync", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationMasterNodePorts, RenderSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_RenderSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_RenderSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsJson_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "65535" },
		{ "ClampMin", "1024" },
		{ "Comment", "/** Advanced: network port for Json Cluster Events */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: network port for Json Cluster Events" },
		{ "UIMax", "65535" },
		{ "UIMin", "1024" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsJson = { "ClusterEventsJson", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationMasterNodePorts, ClusterEventsJson), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsJson_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsJson_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsBinary_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ClampMax", "65535" },
		{ "ClampMin", "1024" },
		{ "Comment", "/** Advanced: network port for Binary Cluster Events */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Advanced: network port for Binary Cluster Events" },
		{ "UIMax", "65535" },
		{ "UIMin", "1024" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsBinary = { "ClusterEventsBinary", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationMasterNodePorts, ClusterEventsBinary), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsBinary_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsBinary_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_RenderSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsJson,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::NewProp_ClusterEventsBinary,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationMasterNodePorts",
		sizeof(FDisplayClusterConfigurationMasterNodePorts),
		alignof(FDisplayClusterConfigurationMasterNodePorts),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationMasterNodePorts"), sizeof(FDisplayClusterConfigurationMasterNodePorts), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNodePorts_Hash() { return 2246828003U; }
class UScriptStruct* FDisplayClusterConfigurationInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTERCONFIGURATION_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo, Z_Construct_UPackage__Script_DisplayClusterConfiguration(), TEXT("DisplayClusterConfigurationInfo"), sizeof(FDisplayClusterConfigurationInfo), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTERCONFIGURATION_API UScriptStruct* StaticStruct<FDisplayClusterConfigurationInfo>()
{
	return FDisplayClusterConfigurationInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterConfigurationInfo(FDisplayClusterConfigurationInfo::StaticStruct, TEXT("/Script/DisplayClusterConfiguration"), TEXT("DisplayClusterConfigurationInfo"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationInfo
{
	FScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationInfo()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterConfigurationInfo>(FName(TEXT("DisplayClusterConfigurationInfo")));
	}
} ScriptStruct_DisplayClusterConfiguration_StaticRegisterNativesFDisplayClusterConfigurationInfo;
	struct Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Version;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterConfigurationInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationInfo, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Version_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationInfo, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Version_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_AssetPath_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterConfigurationInfo, AssetPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_AssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_AssetPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_Version,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::NewProp_AssetPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
		nullptr,
		&NewStructOps,
		"DisplayClusterConfigurationInfo",
		sizeof(FDisplayClusterConfigurationInfo),
		alignof(FDisplayClusterConfigurationInfo),
		Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayClusterConfiguration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterConfigurationInfo"), sizeof(FDisplayClusterConfigurationInfo), Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo_Hash() { return 1087185154U; }
	void UDisplayClusterConfigurationSceneComponent::StaticRegisterNativesUDisplayClusterConfigurationSceneComponent()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_NoRegister()
	{
		return UDisplayClusterConfigurationSceneComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParentId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ParentId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Location_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Location;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Rotation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationData_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Scene hierarchy\n" },
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Scene hierarchy" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_ParentId_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_ParentId = { "ParentId", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationSceneComponent, ParentId), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_ParentId_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_ParentId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Location_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Location = { "Location", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationSceneComponent, Location), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Location_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Location_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationSceneComponent, Rotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Rotation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_ParentId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Location,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::NewProp_Rotation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationSceneComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::ClassParams = {
		&UDisplayClusterConfigurationSceneComponent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationSceneComponent, 3996765632);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationSceneComponent>()
	{
		return UDisplayClusterConfigurationSceneComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationSceneComponent(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent, &UDisplayClusterConfigurationSceneComponent::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationSceneComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationSceneComponent);
	void UDisplayClusterConfigurationSceneComponentXform::StaticRegisterNativesUDisplayClusterConfigurationSceneComponentXform()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_NoRegister()
	{
		return UDisplayClusterConfigurationSceneComponentXform::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationSceneComponentXform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics::ClassParams = {
		&UDisplayClusterConfigurationSceneComponentXform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationSceneComponentXform, 4174966187);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationSceneComponentXform>()
	{
		return UDisplayClusterConfigurationSceneComponentXform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationSceneComponentXform(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform, &UDisplayClusterConfigurationSceneComponentXform::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationSceneComponentXform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationSceneComponentXform);
	void UDisplayClusterConfigurationSceneComponentScreen::StaticRegisterNativesUDisplayClusterConfigurationSceneComponentScreen()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_NoRegister()
	{
		return UDisplayClusterConfigurationSceneComponentScreen::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::NewProp_Size_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationSceneComponentScreen, Size), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::NewProp_Size,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationSceneComponentScreen>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::ClassParams = {
		&UDisplayClusterConfigurationSceneComponentScreen::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationSceneComponentScreen, 1835061682);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationSceneComponentScreen>()
	{
		return UDisplayClusterConfigurationSceneComponentScreen::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationSceneComponentScreen(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen, &UDisplayClusterConfigurationSceneComponentScreen::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationSceneComponentScreen"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationSceneComponentScreen);
	void UDisplayClusterConfigurationSceneComponentCamera::StaticRegisterNativesUDisplayClusterConfigurationSceneComponentCamera()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_NoRegister()
	{
		return UDisplayClusterConfigurationSceneComponentCamera::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterpupillaryDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InterpupillaryDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSwapEyes_MetaData[];
#endif
		static void NewProp_bSwapEyes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSwapEyes;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_StereoOffset_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StereoOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_StereoOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationSceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_InterpupillaryDistance_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_InterpupillaryDistance = { "InterpupillaryDistance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationSceneComponentCamera, InterpupillaryDistance), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_InterpupillaryDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_InterpupillaryDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_bSwapEyes_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_bSwapEyes_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationSceneComponentCamera*)Obj)->bSwapEyes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_bSwapEyes = { "bSwapEyes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationSceneComponentCamera), &Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_bSwapEyes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_bSwapEyes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_bSwapEyes_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_StereoOffset_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_StereoOffset_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_StereoOffset = { "StereoOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationSceneComponentCamera, StereoOffset), Z_Construct_UEnum_DisplayClusterConfiguration_EDisplayClusterConfigurationEyeStereoOffset, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_StereoOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_StereoOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_InterpupillaryDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_bSwapEyes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_StereoOffset_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::NewProp_StereoOffset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationSceneComponentCamera>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::ClassParams = {
		&UDisplayClusterConfigurationSceneComponentCamera::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationSceneComponentCamera, 398790032);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationSceneComponentCamera>()
	{
		return UDisplayClusterConfigurationSceneComponentCamera::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationSceneComponentCamera(Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera, &UDisplayClusterConfigurationSceneComponentCamera::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationSceneComponentCamera"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationSceneComponentCamera);
	void UDisplayClusterConfigurationScene::StaticRegisterNativesUDisplayClusterConfigurationScene()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationScene_NoRegister()
	{
		return UDisplayClusterConfigurationScene::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Xforms_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Xforms_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Xforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Xforms;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Screens_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Screens_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Screens_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Screens;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cameras_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Cameras_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cameras_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Cameras;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationData_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms_ValueProp = { "Xforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentXform_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms_Key_KeyProp = { "Xforms_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms = { "Xforms", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationScene, Xforms), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens_ValueProp = { "Screens", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentScreen_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens_Key_KeyProp = { "Screens_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens = { "Screens", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationScene, Screens), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras_ValueProp = { "Cameras", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDisplayClusterConfigurationSceneComponentCamera_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras_Key_KeyProp = { "Cameras_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras = { "Cameras", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationScene, Cameras), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Xforms,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Screens,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::NewProp_Cameras,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationScene>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::ClassParams = {
		&UDisplayClusterConfigurationScene::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationScene()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationScene_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationScene, 4172470855);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationScene>()
	{
		return UDisplayClusterConfigurationScene::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationScene(Z_Construct_UClass_UDisplayClusterConfigurationScene, &UDisplayClusterConfigurationScene::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationScene"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationScene);
	void UDisplayClusterConfigurationClusterNode::StaticRegisterNativesUDisplayClusterConfigurationClusterNode()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_NoRegister()
	{
		return UDisplayClusterConfigurationClusterNode::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Host_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Host;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSoundEnabled_MetaData[];
#endif
		static void NewProp_bIsSoundEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSoundEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsFullscreen_MetaData[];
#endif
		static void NewProp_bIsFullscreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsFullscreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WindowRect_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WindowRect;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputRemap_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputRemap;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFixedAspectRatio_MetaData[];
#endif
		static void NewProp_bFixedAspectRatio_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFixedAspectRatio;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Viewports_ValueProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Viewports_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Viewports_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Viewports_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Viewports;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Postprocess_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Postprocess_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Postprocess_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Postprocess;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVisible_MetaData[];
#endif
		static void NewProp_bIsVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVisible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewImage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviewImage;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationData_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Host_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** IP address of this specific cluster Node */" },
		{ "DisplayName", "Host IP Address" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "IP address of this specific cluster Node" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Host = { "Host", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationClusterNode, Host), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Host_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Host_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsSoundEnabled_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Enables or disables sound on nDisplay primary Node */" },
		{ "DisplayName", "Enable Sound" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Enables or disables sound on nDisplay primary Node" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsSoundEnabled_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationClusterNode*)Obj)->bIsSoundEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsSoundEnabled = { "bIsSoundEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationClusterNode), &Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsSoundEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsSoundEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsSoundEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsFullscreen_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Enables application window native fullscreen support */" },
		{ "DisplayName", "Fullscreen" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Enables application window native fullscreen support" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsFullscreen_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationClusterNode*)Obj)->bIsFullscreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsFullscreen = { "bIsFullscreen", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationClusterNode), &Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsFullscreen_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsFullscreen_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsFullscreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_WindowRect_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Defines the application window size in pixels */" },
		{ "DisplayName", "Window" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Defines the application window size in pixels" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_WindowRect = { "WindowRect", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationClusterNode, WindowRect), Z_Construct_UScriptStruct_FDisplayClusterConfigurationRectangle, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_WindowRect_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_WindowRect_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_OutputRemap_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Output remapping settings for the selected cluster node */" },
		{ "DisplayName", "Output Remapping" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Output remapping settings for the selected cluster node" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_OutputRemap = { "OutputRemap", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationClusterNode, OutputRemap), Z_Construct_UScriptStruct_FDisplayClusterConfigurationFramePostProcess_OutputRemap, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_OutputRemap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_OutputRemap_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bFixedAspectRatio_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Locks the application window aspect ratio for easier resizing */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Locks the application window aspect ratio for easier resizing" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bFixedAspectRatio_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationClusterNode*)Obj)->bFixedAspectRatio = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bFixedAspectRatio = { "bFixedAspectRatio", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationClusterNode), &Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bFixedAspectRatio_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bFixedAspectRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bFixedAspectRatio_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_ValueProp_MetaData[] = {
		{ "Category", "Configuration" },
		{ "DisplayThumbnail", "FALSE" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "nDisplayInstanceOnly", "" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_ValueProp = { "Viewports", nullptr, (EPropertyFlags)0x00020000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDisplayClusterConfigurationViewport_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_ValueProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_ValueProp_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_Key_KeyProp = { "Viewports_Key", nullptr, (EPropertyFlags)0x00020000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_MetaData[] = {
		{ "Category", "Configuration" },
		{ "DisplayThumbnail", "FALSE" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "nDisplayInstanceOnly", "" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports = { "Viewports", nullptr, (EPropertyFlags)0x001000800002085d, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationClusterNode, Viewports), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess_ValueProp = { "Postprocess", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FDisplayClusterConfigurationPostprocess, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess_Key_KeyProp = { "Postprocess_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess_MetaData[] = {
		{ "Category", "Configuration" },
		{ "DisplayName", "Custom Output Settings" },
		{ "DisplayThumbnail", "FALSE" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess = { "Postprocess", nullptr, (EPropertyFlags)0x0010000000010015, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationClusterNode, Postprocess), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsVisible_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "nDisplayHidden", "" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsVisible_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationClusterNode*)Obj)->bIsVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsVisible = { "bIsVisible", nullptr, (EPropertyFlags)0x0010000800010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationClusterNode), &Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsVisible_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsVisible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "nDisplayHidden", "" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationClusterNode*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000800010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationClusterNode), &Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_PreviewImage_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Binds a background preview image for easier output mapping */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Binds a background preview image for easier output mapping" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_PreviewImage = { "PreviewImage", nullptr, (EPropertyFlags)0x0010000800010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationClusterNode, PreviewImage), Z_Construct_UScriptStruct_FDisplayClusterConfigurationExternalImage, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_PreviewImage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_PreviewImage_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Host,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsSoundEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsFullscreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_WindowRect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_OutputRemap,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bFixedAspectRatio,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Viewports,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_Postprocess,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::NewProp_PreviewImage,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationClusterNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::ClassParams = {
		&UDisplayClusterConfigurationClusterNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationClusterNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationClusterNode, 369199333);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationClusterNode>()
	{
		return UDisplayClusterConfigurationClusterNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationClusterNode(Z_Construct_UClass_UDisplayClusterConfigurationClusterNode, &UDisplayClusterConfigurationClusterNode::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationClusterNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationClusterNode);
	void UDisplayClusterConfigurationHostDisplayData::StaticRegisterNativesUDisplayClusterConfigurationHostDisplayData()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_NoRegister()
	{
		return UDisplayClusterConfigurationHostDisplayData::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HostName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_HostName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowManualPlacement_MetaData[];
#endif
		static void NewProp_bAllowManualPlacement_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowManualPlacement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HostResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HostResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowManualSizing_MetaData[];
#endif
		static void NewProp_bAllowManualSizing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowManualSizing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Origin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Origin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVisible_MetaData[];
#endif
		static void NewProp_bIsVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVisible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostName_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Custom name for the Host PC. No effect on nDisplay */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Custom name for the Host PC. No effect on nDisplay" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostName = { "HostName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationHostDisplayData, HostName), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Position_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Arbitrary position of the Host PC in 2D workspace. No effect on nDisplay */" },
		{ "EditCondition", "bAllowManualPlacement" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Arbitrary position of the Host PC in 2D workspace. No effect on nDisplay" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationHostDisplayData, Position), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualPlacement_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Disables the automatic placement of Host PCs */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Disables the automatic placement of Host PCs" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualPlacement_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationHostDisplayData*)Obj)->bAllowManualPlacement = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualPlacement = { "bAllowManualPlacement", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationHostDisplayData), &Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualPlacement_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualPlacement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualPlacement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostResolution_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Resolution of Host PC in pixels */" },
		{ "EditCondition", "bAllowManualSizing" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Resolution of Host PC in pixels" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostResolution = { "HostResolution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationHostDisplayData, HostResolution), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualSizing_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Allows to manually resize the Host PC resolution */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Allows to manually resize the Host PC resolution" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualSizing_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationHostDisplayData*)Obj)->bAllowManualSizing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualSizing = { "bAllowManualSizing", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationHostDisplayData), &Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualSizing_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualSizing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualSizing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Origin_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Specify coordinates of the Host PC origin in relation to OS configuration */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Specify coordinates of the Host PC origin in relation to OS configuration" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Origin = { "Origin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationHostDisplayData, Origin), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Origin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Origin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Configuration" },
		{ "Comment", "/** Specify custom and arbitrary color for a given Host PC */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Specify custom and arbitrary color for a given Host PC" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationHostDisplayData, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsVisible_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsVisible_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationHostDisplayData*)Obj)->bIsVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsVisible = { "bIsVisible", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationHostDisplayData), &Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsVisible_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsVisible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationHostDisplayData*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationHostDisplayData), &Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualPlacement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_HostResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bAllowManualSizing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Origin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::NewProp_bIsEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationHostDisplayData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::ClassParams = {
		&UDisplayClusterConfigurationHostDisplayData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationHostDisplayData, 4151597642);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationHostDisplayData>()
	{
		return UDisplayClusterConfigurationHostDisplayData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationHostDisplayData(Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData, &UDisplayClusterConfigurationHostDisplayData::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationHostDisplayData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationHostDisplayData);
	void UDisplayClusterConfigurationCluster::StaticRegisterNativesUDisplayClusterConfigurationCluster()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationCluster_NoRegister()
	{
		return UDisplayClusterConfigurationCluster::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MasterNode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sync_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Sync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Network_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Network;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Nodes_ValueProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Nodes_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Nodes_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Nodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Nodes;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HostDisplayData_ValueProp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HostDisplayData_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_HostDisplayData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HostDisplayData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_HostDisplayData;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationData_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_MasterNode_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_MasterNode = { "MasterNode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationCluster, MasterNode), Z_Construct_UScriptStruct_FDisplayClusterConfigurationMasterNode, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_MasterNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_MasterNode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Sync_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Sync = { "Sync", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationCluster, Sync), Z_Construct_UScriptStruct_FDisplayClusterConfigurationClusterSync, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Sync_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Sync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Network_MetaData[] = {
		{ "Category", "Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Network = { "Network", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationCluster, Network), Z_Construct_UScriptStruct_FDisplayClusterConfigurationNetworkSettings, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Network_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Network_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_ValueProp_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "DisplayThumbnail", "FALSE" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "nDisplayInstanceOnly", "" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_ValueProp = { "Nodes", nullptr, (EPropertyFlags)0x00020000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_ValueProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_ValueProp_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_Key_KeyProp = { "Nodes_Key", nullptr, (EPropertyFlags)0x00020000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "DisplayThumbnail", "FALSE" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "nDisplayInstanceOnly", "" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes = { "Nodes", nullptr, (EPropertyFlags)0x001000800002085d, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationCluster, Nodes), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_ValueProp_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_ValueProp = { "HostDisplayData", nullptr, (EPropertyFlags)0x0002000800080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDisplayClusterConfigurationHostDisplayData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_ValueProp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_ValueProp_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_Key_KeyProp = { "HostDisplayData_Key", nullptr, (EPropertyFlags)0x0002000800080008, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData = { "HostDisplayData", nullptr, (EPropertyFlags)0x0010008800000008, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationCluster, HostDisplayData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_MasterNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Sync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Network,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_Nodes,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::NewProp_HostDisplayData,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationCluster>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::ClassParams = {
		&UDisplayClusterConfigurationCluster::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationCluster()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationCluster_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationCluster, 840929221);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationCluster>()
	{
		return UDisplayClusterConfigurationCluster::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationCluster(Z_Construct_UClass_UDisplayClusterConfigurationCluster, &UDisplayClusterConfigurationCluster::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationCluster"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationCluster);
	DEFINE_FUNCTION(UDisplayClusterConfigurationData::execRemovePostprocess)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_NodeId);
		P_GET_PROPERTY(FStrProperty,Z_Param_PostprocessId);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemovePostprocess(Z_Param_NodeId,Z_Param_PostprocessId);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterConfigurationData::execAssignPostprocess)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_NodeId);
		P_GET_PROPERTY(FStrProperty,Z_Param_PostprocessId);
		P_GET_PROPERTY(FStrProperty,Z_Param_Type);
		P_GET_TMAP(FString,FString,Z_Param_Parameters);
		P_GET_PROPERTY(FIntProperty,Z_Param_Order);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->AssignPostprocess(Z_Param_NodeId,Z_Param_PostprocessId,Z_Param_Type,Z_Param_Parameters,Z_Param_Order);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterConfigurationData::execGetClusterNodeConfiguration)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_NodeId);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterConfigurationClusterNode**)Z_Param__Result=P_THIS->GetClusterNodeConfiguration(Z_Param_NodeId);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterConfigurationData::execGetViewportConfiguration)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_NodeId);
		P_GET_PROPERTY(FStrProperty,Z_Param_ViewportId);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDisplayClusterConfigurationViewport**)Z_Param__Result=P_THIS->GetViewportConfiguration(Z_Param_NodeId,Z_Param_ViewportId);
		P_NATIVE_END;
	}
	void UDisplayClusterConfigurationData::StaticRegisterNativesUDisplayClusterConfigurationData()
	{
		UClass* Class = UDisplayClusterConfigurationData::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AssignPostprocess", &UDisplayClusterConfigurationData::execAssignPostprocess },
			{ "GetClusterNodeConfiguration", &UDisplayClusterConfigurationData::execGetClusterNodeConfiguration },
			{ "GetViewportConfiguration", &UDisplayClusterConfigurationData::execGetViewportConfiguration },
			{ "RemovePostprocess", &UDisplayClusterConfigurationData::execRemovePostprocess },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics
	{
		struct DisplayClusterConfigurationData_eventAssignPostprocess_Parms
		{
			FString NodeId;
			FString PostprocessId;
			FString Type;
			TMap<FString,FString> Parameters;
			int32 Order;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NodeId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostprocessId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PostprocessId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Parameters_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Parameters;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Order;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_NodeId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_NodeId = { "NodeId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventAssignPostprocess_Parms, NodeId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_NodeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_NodeId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_PostprocessId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_PostprocessId = { "PostprocessId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventAssignPostprocess_Parms, PostprocessId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_PostprocessId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_PostprocessId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Type_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventAssignPostprocess_Parms, Type), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Parameters_ValueProp = { "Parameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Parameters_Key_KeyProp = { "Parameters_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Parameters = { "Parameters", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventAssignPostprocess_Parms, Parameters), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Order = { "Order", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventAssignPostprocess_Parms, Order), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterConfigurationData_eventAssignPostprocess_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterConfigurationData_eventAssignPostprocess_Parms), &Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_NodeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_PostprocessId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Parameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Parameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Parameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_Order,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Configuration" },
		{ "Comment", "/**\n\x09* Update\\Create node postprocess\n\x09*\n\x09* @param PostprocessId - Unique postprocess name\n\x09* @param Type          - Postprocess type id\n\x09* @param Parameters    - Postprocess parameters\n\x09* @param Order         - Control the rendering order of post-processing. Larger value is displayed last\n\x09*\n\x09* @return - true, if success\n\x09*/" },
		{ "CPP_Default_Order", "-1" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Update\\Create node postprocess\n\n@param PostprocessId - Unique postprocess name\n@param Type          - Postprocess type id\n@param Parameters    - Postprocess parameters\n@param Order         - Control the rendering order of post-processing. Larger value is displayed last\n\n@return - true, if success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterConfigurationData, nullptr, "AssignPostprocess", nullptr, nullptr, sizeof(DisplayClusterConfigurationData_eventAssignPostprocess_Parms), Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics
	{
		struct DisplayClusterConfigurationData_eventGetClusterNodeConfiguration_Parms
		{
			FString NodeId;
			UDisplayClusterConfigurationClusterNode* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NodeId;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::NewProp_NodeId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::NewProp_NodeId = { "NodeId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventGetClusterNodeConfiguration_Parms, NodeId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::NewProp_NodeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::NewProp_NodeId_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventGetClusterNodeConfiguration_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterConfigurationClusterNode_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::NewProp_NodeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterConfigurationData, nullptr, "GetClusterNodeConfiguration", nullptr, nullptr, sizeof(DisplayClusterConfigurationData_eventGetClusterNodeConfiguration_Parms), Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics
	{
		struct DisplayClusterConfigurationData_eventGetViewportConfiguration_Parms
		{
			FString NodeId;
			FString ViewportId;
			UDisplayClusterConfigurationViewport* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NodeId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportId;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_NodeId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_NodeId = { "NodeId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventGetViewportConfiguration_Parms, NodeId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_NodeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_NodeId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_ViewportId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_ViewportId = { "ViewportId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventGetViewportConfiguration_Parms, ViewportId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_ViewportId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_ViewportId_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventGetViewportConfiguration_Parms, ReturnValue), Z_Construct_UClass_UDisplayClusterConfigurationViewport_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_NodeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_ViewportId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Configuration" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterConfigurationData, nullptr, "GetViewportConfiguration", nullptr, nullptr, sizeof(DisplayClusterConfigurationData_eventGetViewportConfiguration_Parms), Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics
	{
		struct DisplayClusterConfigurationData_eventRemovePostprocess_Parms
		{
			FString NodeId;
			FString PostprocessId;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NodeId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostprocessId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PostprocessId;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_NodeId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_NodeId = { "NodeId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventRemovePostprocess_Parms, NodeId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_NodeId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_NodeId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_PostprocessId_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_PostprocessId = { "PostprocessId", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterConfigurationData_eventRemovePostprocess_Parms, PostprocessId), METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_PostprocessId_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_PostprocessId_MetaData)) };
	void Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DisplayClusterConfigurationData_eventRemovePostprocess_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DisplayClusterConfigurationData_eventRemovePostprocess_Parms), &Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_NodeId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_PostprocessId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay|Configuration" },
		{ "Comment", "/**\n\x09* Delet node postprocess\n\x09*\n\x09* @param PostprocessId - Unique postprocess name\n\x09*\n\x09* @return - true, if success\n\x09*/" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "Delet node postprocess\n\n@param PostprocessId - Unique postprocess name\n\n@return - true, if success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterConfigurationData, nullptr, "RemovePostprocess", nullptr, nullptr, sizeof(DisplayClusterConfigurationData_eventRemovePostprocess_Parms), Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationData_NoRegister()
	{
		return UDisplayClusterConfigurationData::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterConfigurationData_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Info_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Info;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scene_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Scene;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cluster_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cluster;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CustomParameters_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CustomParameters_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CustomParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Diagnostics_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Diagnostics;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderFrameSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderFrameSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StageSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StageSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFollowLocalPlayerCamera_MetaData[];
#endif
		static void NewProp_bFollowLocalPlayerCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFollowLocalPlayerCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExitOnEsc_MetaData[];
#endif
		static void NewProp_bExitOnEsc_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExitOnEsc;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PathToConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PathToConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ImportedPath;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDisplayClusterConfigurationData_Base,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayClusterConfiguration,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDisplayClusterConfigurationData_AssignPostprocess, "AssignPostprocess" }, // 1183515366
		{ &Z_Construct_UFunction_UDisplayClusterConfigurationData_GetClusterNodeConfiguration, "GetClusterNodeConfiguration" }, // 3659738980
		{ &Z_Construct_UFunction_UDisplayClusterConfigurationData_GetViewportConfiguration, "GetViewportConfiguration" }, // 3452528568
		{ &Z_Construct_UFunction_UDisplayClusterConfigurationData_RemovePostprocess, "RemovePostprocess" }, // 3561569440
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "////////////////////////////////////////////////////////////////\n// Main configuration data container\n" },
		{ "IncludePath", "DisplayClusterConfigurationTypes.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "/\n Main configuration data container" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Info_MetaData[] = {
		{ "Category", "Advanced" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Info = { "Info", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, Info), Z_Construct_UScriptStruct_FDisplayClusterConfigurationInfo, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Info_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Info_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Scene_MetaData[] = {
		{ "Category", "Advanced" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Scene = { "Scene", nullptr, (EPropertyFlags)0x0010000000022815, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, Scene), Z_Construct_UClass_UDisplayClusterConfigurationScene_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Scene_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Scene_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Cluster_MetaData[] = {
		{ "Category", "Advanced" },
		{ "DisplayThumbnail", "FALSE" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ShowInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Cluster = { "Cluster", nullptr, (EPropertyFlags)0x00120000000a081d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, Cluster), Z_Construct_UClass_UDisplayClusterConfigurationCluster_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Cluster_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Cluster_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters_ValueProp = { "CustomParameters", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters_Key_KeyProp = { "CustomParameters_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters_MetaData[] = {
		{ "Category", "Advanced" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters = { "CustomParameters", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, CustomParameters), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Diagnostics_MetaData[] = {
		{ "Category", "Advanced" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Diagnostics = { "Diagnostics", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, Diagnostics), Z_Construct_UScriptStruct_FDisplayClusterConfigurationDiagnostics, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Diagnostics_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Diagnostics_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_RenderFrameSettings_MetaData[] = {
		{ "Category", "Advanced" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_RenderFrameSettings = { "RenderFrameSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, RenderFrameSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationRenderFrame, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_RenderFrameSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_RenderFrameSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_StageSettings_MetaData[] = {
		{ "Category", "Advanced" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_StageSettings = { "StageSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, StageSettings), Z_Construct_UScriptStruct_FDisplayClusterConfigurationICVFX_StageSettings, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_StageSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_StageSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bFollowLocalPlayerCamera_MetaData[] = {
		{ "Category", "Advanced" },
		{ "DisplayName", "Follow Local Player Camera" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bFollowLocalPlayerCamera_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationData*)Obj)->bFollowLocalPlayerCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bFollowLocalPlayerCamera = { "bFollowLocalPlayerCamera", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationData), &Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bFollowLocalPlayerCamera_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bFollowLocalPlayerCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bFollowLocalPlayerCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bExitOnEsc_MetaData[] = {
		{ "Category", "Advanced" },
		{ "DisplayName", "Exit When ESC Pressed" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	void Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bExitOnEsc_SetBit(void* Obj)
	{
		((UDisplayClusterConfigurationData*)Obj)->bExitOnEsc = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bExitOnEsc = { "bExitOnEsc", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisplayClusterConfigurationData), &Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bExitOnEsc_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bExitOnEsc_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bExitOnEsc_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_PathToConfig_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_PathToConfig = { "PathToConfig", nullptr, (EPropertyFlags)0x0010000800004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, PathToConfig), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_PathToConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_PathToConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_ImportedPath_MetaData[] = {
		{ "Comment", "/** The path used when originally importing. */" },
		{ "ModuleRelativePath", "Public/DisplayClusterConfigurationTypes.h" },
		{ "ToolTip", "The path used when originally importing." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_ImportedPath = { "ImportedPath", nullptr, (EPropertyFlags)0x0010000800000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterConfigurationData, ImportedPath), METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_ImportedPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_ImportedPath_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Info,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Scene,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Cluster,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_CustomParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_Diagnostics,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_RenderFrameSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_StageSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bFollowLocalPlayerCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_bExitOnEsc,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_PathToConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::NewProp_ImportedPath,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterConfigurationData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::ClassParams = {
		&UDisplayClusterConfigurationData::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::PropPointers),
		0,
		0x009004A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterConfigurationData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterConfigurationData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterConfigurationData, 1176720751);
	template<> DISPLAYCLUSTERCONFIGURATION_API UClass* StaticClass<UDisplayClusterConfigurationData>()
	{
		return UDisplayClusterConfigurationData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterConfigurationData(Z_Construct_UClass_UDisplayClusterConfigurationData, &UDisplayClusterConfigurationData::StaticClass, TEXT("/Script/DisplayClusterConfiguration"), TEXT("UDisplayClusterConfigurationData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterConfigurationData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
