// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorHostNode_generated_h
#error "DisplayClusterConfiguratorHostNode.generated.h already included, missing '#pragma once' in DisplayClusterConfiguratorHostNode.h"
#endif
#define DISPLAYCLUSTERCONFIGURATOR_DisplayClusterConfiguratorHostNode_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorHostNode(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorHostNode_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorHostNode, UDisplayClusterConfiguratorBaseNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorHostNode)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterConfiguratorHostNode(); \
	friend struct Z_Construct_UClass_UDisplayClusterConfiguratorHostNode_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterConfiguratorHostNode, UDisplayClusterConfiguratorBaseNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DisplayClusterConfigurator"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterConfiguratorHostNode)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorHostNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorHostNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorHostNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorHostNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorHostNode(UDisplayClusterConfiguratorHostNode&&); \
	NO_API UDisplayClusterConfiguratorHostNode(const UDisplayClusterConfiguratorHostNode&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterConfiguratorHostNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterConfiguratorHostNode(UDisplayClusterConfiguratorHostNode&&); \
	NO_API UDisplayClusterConfiguratorHostNode(const UDisplayClusterConfiguratorHostNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterConfiguratorHostNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterConfiguratorHostNode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterConfiguratorHostNode)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_12_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTERCONFIGURATOR_API UClass* StaticClass<class UDisplayClusterConfiguratorHostNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayClusterConfigurator_Private_Views_OutputMapping_EdNodes_DisplayClusterConfiguratorHostNode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
