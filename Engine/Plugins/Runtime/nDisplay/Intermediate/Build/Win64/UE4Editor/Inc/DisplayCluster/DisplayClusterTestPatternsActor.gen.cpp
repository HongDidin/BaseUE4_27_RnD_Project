// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/DisplayClusterTestPatternsActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterTestPatternsActor() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_ADisplayClusterTestPatternsActor_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_ADisplayClusterTestPatternsActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	ENGINE_API UClass* Z_Construct_UClass_UPostProcessComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterial_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPostProcessSettings();
// End Cross Module References
	void ADisplayClusterTestPatternsActor::StaticRegisterNativesADisplayClusterTestPatternsActor()
	{
	}
	UClass* Z_Construct_UClass_ADisplayClusterTestPatternsActor_NoRegister()
	{
		return ADisplayClusterTestPatternsActor::StaticClass();
	}
	struct Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostProcessComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PostProcessComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CalibrationPatterns_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CalibrationPatterns_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CalibrationPatterns_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_CalibrationPatterns;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportPPSettings_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportPPSettings_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportPPSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ViewportPPSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Test patterns actor\n */" },
		{ "IncludePath", "DisplayClusterTestPatternsActor.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterTestPatternsActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Test patterns actor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_PostProcessComponent_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Postprocess component */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/DisplayClusterTestPatternsActor.h" },
		{ "ToolTip", "Postprocess component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_PostProcessComponent = { "PostProcessComponent", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterTestPatternsActor, PostProcessComponent), Z_Construct_UClass_UPostProcessComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_PostProcessComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_PostProcessComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns_ValueProp = { "CalibrationPatterns", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UMaterial_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns_Key_KeyProp = { "CalibrationPatterns_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "ModuleRelativePath", "Public/DisplayClusterTestPatternsActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns = { "CalibrationPatterns", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterTestPatternsActor, CalibrationPatterns), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings_ValueProp = { "ViewportPPSettings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings_Key_KeyProp = { "ViewportPPSettings_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/DisplayClusterTestPatternsActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings = { "ViewportPPSettings", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ADisplayClusterTestPatternsActor, ViewportPPSettings), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_PostProcessComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_CalibrationPatterns,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::NewProp_ViewportPPSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ADisplayClusterTestPatternsActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::ClassParams = {
		&ADisplayClusterTestPatternsActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ADisplayClusterTestPatternsActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ADisplayClusterTestPatternsActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ADisplayClusterTestPatternsActor, 2101105647);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<ADisplayClusterTestPatternsActor>()
	{
		return ADisplayClusterTestPatternsActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ADisplayClusterTestPatternsActor(Z_Construct_UClass_ADisplayClusterTestPatternsActor, &ADisplayClusterTestPatternsActor::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("ADisplayClusterTestPatternsActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ADisplayClusterTestPatternsActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
