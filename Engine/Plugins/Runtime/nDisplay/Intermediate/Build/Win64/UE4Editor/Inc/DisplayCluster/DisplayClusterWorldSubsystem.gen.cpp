// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/DisplayClusterWorldSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterWorldSubsystem() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterWorldSubsystem_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterWorldSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UWorldSubsystem();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
// End Cross Module References
	void UDisplayClusterWorldSubsystem::StaticRegisterNativesUDisplayClusterWorldSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UDisplayClusterWorldSubsystem_NoRegister()
	{
		return UDisplayClusterWorldSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWorldSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n* World Subsystem used to react to level and world changes.\n* When Concert reloads the packages, streamed levels are removed and re-added without invoiking LoadMap which \n* circumvents FDisplayClusterGameManager::StartScene method invoked inside LoadMap method of DisplayClusterGameEngine.\n* This causes issues such as not updating references to DisplayClusterRootActor which causes memory corruption, crashes \n* and graphic corruption. This Subsystem is used to react to changes in number of levels used in the current world \n* and forces DisplayClusterModule to refresh all of its managers.\n*/" },
		{ "IncludePath", "DisplayClusterWorldSubsystem.h" },
		{ "ModuleRelativePath", "Public/DisplayClusterWorldSubsystem.h" },
		{ "ToolTip", "World Subsystem used to react to level and world changes.\nWhen Concert reloads the packages, streamed levels are removed and re-added without invoiking LoadMap which\ncircumvents FDisplayClusterGameManager::StartScene method invoked inside LoadMap method of DisplayClusterGameEngine.\nThis causes issues such as not updating references to DisplayClusterRootActor which causes memory corruption, crashes\nand graphic corruption. This Subsystem is used to react to changes in number of levels used in the current world\nand forces DisplayClusterModule to refresh all of its managers." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterWorldSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics::ClassParams = {
		&UDisplayClusterWorldSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterWorldSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterWorldSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterWorldSubsystem, 2175680298);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterWorldSubsystem>()
	{
		return UDisplayClusterWorldSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterWorldSubsystem(Z_Construct_UClass_UDisplayClusterWorldSubsystem, &UDisplayClusterWorldSubsystem::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterWorldSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterWorldSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
