// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterObjectRef_generated_h
#error "DisplayClusterObjectRef.generated.h already included, missing '#pragma once' in DisplayClusterObjectRef.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterObjectRef_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Misc_DisplayClusterObjectRef_h_239_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisplayClusterComponentRef_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<struct FDisplayClusterComponentRef>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Misc_DisplayClusterObjectRef_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
