// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISPLAYCLUSTER_DisplayClusterPreviewComponent_generated_h
#error "DisplayClusterPreviewComponent.generated.h already included, missing '#pragma once' in DisplayClusterPreviewComponent.h"
#endif
#define DISPLAYCLUSTER_DisplayClusterPreviewComponent_generated_h

#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_SPARSE_DATA
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_RPC_WRAPPERS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisplayClusterPreviewComponent(); \
	friend struct Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterPreviewComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterPreviewComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_INCLASS \
private: \
	static void StaticRegisterNativesUDisplayClusterPreviewComponent(); \
	friend struct Z_Construct_UClass_UDisplayClusterPreviewComponent_Statics; \
public: \
	DECLARE_CLASS(UDisplayClusterPreviewComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisplayCluster"), NO_API) \
	DECLARE_SERIALIZER(UDisplayClusterPreviewComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisplayClusterPreviewComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterPreviewComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterPreviewComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterPreviewComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterPreviewComponent(UDisplayClusterPreviewComponent&&); \
	NO_API UDisplayClusterPreviewComponent(const UDisplayClusterPreviewComponent&); \
public:


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisplayClusterPreviewComponent(UDisplayClusterPreviewComponent&&); \
	NO_API UDisplayClusterPreviewComponent(const UDisplayClusterPreviewComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisplayClusterPreviewComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisplayClusterPreviewComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisplayClusterPreviewComponent)


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_24_PROLOG
#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_RPC_WRAPPERS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_INCLASS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_SPARSE_DATA \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h_30_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISPLAYCLUSTER_API UClass* StaticClass<class UDisplayClusterPreviewComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_nDisplay_Source_DisplayCluster_Public_Components_DisplayClusterPreviewComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
