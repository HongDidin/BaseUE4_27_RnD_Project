// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Components/DisplayClusterScreenComponent.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterScreenComponent() {}
// Cross Module References
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterScreenComponent_NoRegister();
	DISPLAYCLUSTER_API UClass* Z_Construct_UClass_UDisplayClusterScreenComponent();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	DEFINE_FUNCTION(UDisplayClusterScreenComponent::execSetScreenSize)
	{
		P_GET_STRUCT_REF(FVector2D,Z_Param_Out_Size);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetScreenSize(Z_Param_Out_Size);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UDisplayClusterScreenComponent::execGetScreenSize)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FVector2D*)Z_Param__Result=P_THIS->GetScreenSize();
		P_NATIVE_END;
	}
	void UDisplayClusterScreenComponent::StaticRegisterNativesUDisplayClusterScreenComponent()
	{
		UClass* Class = UDisplayClusterScreenComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetScreenSize", &UDisplayClusterScreenComponent::execGetScreenSize },
			{ "SetScreenSize", &UDisplayClusterScreenComponent::execSetScreenSize },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics
	{
		struct DisplayClusterScreenComponent_eventGetScreenSize_Parms
		{
			FVector2D ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterScreenComponent_eventGetScreenSize_Parms, ReturnValue), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Return the screen size adjusted by its transform scale. */" },
		{ "DisplayName", "Get screen size" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterScreenComponent.h" },
		{ "ToolTip", "Return the screen size adjusted by its transform scale." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterScreenComponent, nullptr, "GetScreenSize", nullptr, nullptr, sizeof(DisplayClusterScreenComponent_eventGetScreenSize_Parms), Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics
	{
		struct DisplayClusterScreenComponent_eventSetScreenSize_Parms
		{
			FVector2D Size;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::NewProp_Size_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DisplayClusterScreenComponent_eventSetScreenSize_Parms, Size), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::NewProp_Size,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "/** Set screen size (update transform scale). */" },
		{ "DisplayName", "Set screen size" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterScreenComponent.h" },
		{ "ToolTip", "Set screen size (update transform scale)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDisplayClusterScreenComponent, nullptr, "SetScreenSize", nullptr, nullptr, sizeof(DisplayClusterScreenComponent_eventSetScreenSize_Parms), Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDisplayClusterScreenComponent_NoRegister()
	{
		return UDisplayClusterScreenComponent::StaticClass();
	}
	struct Z_Construct_UClass_UDisplayClusterScreenComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Size_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UStaticMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDisplayClusterScreenComponent_GetScreenSize, "GetScreenSize" }, // 3009123551
		{ &Z_Construct_UFunction_UDisplayClusterScreenComponent_SetScreenSize, "SetScreenSize" }, // 4085689935
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "DisplayCluster" },
		{ "Comment", "/**\n * Simple projection policy screen component\n */" },
		{ "DisplayName", "NDisplay Screen" },
		{ "HideCategories", "Object Activation Components|Activation Trigger" },
		{ "IncludePath", "Components/DisplayClusterScreenComponent.h" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterScreenComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Simple projection policy screen component" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::NewProp_Size_MetaData[] = {
		{ "AllowPreserveRatio", "" },
		{ "Category", "Screen Size" },
		{ "Comment", "/** Adjust the size of the screen. */" },
		{ "DisplayName", "Size" },
		{ "ModuleRelativePath", "Public/Components/DisplayClusterScreenComponent.h" },
		{ "ToolTip", "Adjust the size of the screen." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0020080800010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisplayClusterScreenComponent, Size), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::NewProp_Size_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::NewProp_Size_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::NewProp_Size,
	};
#endif // WITH_EDITORONLY_DATA
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisplayClusterScreenComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::ClassParams = {
		&UDisplayClusterScreenComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		IF_WITH_EDITORONLY_DATA(Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::PropPointers, nullptr),
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		IF_WITH_EDITORONLY_DATA(UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::PropPointers), 0),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisplayClusterScreenComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisplayClusterScreenComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisplayClusterScreenComponent, 272991257);
	template<> DISPLAYCLUSTER_API UClass* StaticClass<UDisplayClusterScreenComponent>()
	{
		return UDisplayClusterScreenComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisplayClusterScreenComponent(Z_Construct_UClass_UDisplayClusterScreenComponent, &UDisplayClusterScreenComponent::StaticClass, TEXT("/Script/DisplayCluster"), TEXT("UDisplayClusterScreenComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisplayClusterScreenComponent);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UDisplayClusterScreenComponent)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
