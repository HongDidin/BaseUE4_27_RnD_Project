// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisplayCluster/Public/Blueprints/DisplayClusterBlueprintContainers.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisplayClusterBlueprintContainers() {}
// Cross Module References
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext();
	UPackage* Z_Construct_UPackage__Script_DisplayCluster();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
	DISPLAYCLUSTER_API UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterViewportContext();
// End Cross Module References
class UScriptStruct* FDisplayClusterViewportStereoContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTER_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("DisplayClusterViewportStereoContext"), sizeof(FDisplayClusterViewportStereoContext), Get_Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<FDisplayClusterViewportStereoContext>()
{
	return FDisplayClusterViewportStereoContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterViewportStereoContext(FDisplayClusterViewportStereoContext::StaticStruct, TEXT("/Script/DisplayCluster"), TEXT("DisplayClusterViewportStereoContext"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterViewportStereoContext
{
	FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterViewportStereoContext()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterViewportStereoContext>(FName(TEXT("DisplayClusterViewportStereoContext")));
	}
} ScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterViewportStereoContext;
	struct Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RectLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RectLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RectSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RectSize;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewLocation_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewLocation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewRotation_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewRotation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionMatrix_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsRendering_MetaData[];
#endif
		static void NewProp_bIsRendering_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsRendering;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterViewportStereoContext>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewportID_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Viewport Name\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Viewport Name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewportID = { "ViewportID", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportStereoContext, ViewportID), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewportID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewportID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectLocation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Location on a backbuffer.\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Location on a backbuffer." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectLocation = { "RectLocation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportStereoContext, RectLocation), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectSize_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Size on a backbuffer.\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Size on a backbuffer." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectSize = { "RectSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportStereoContext, RectSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectSize_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewLocation_Inner = { "ViewLocation", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewLocation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Camera view location\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Camera view location" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewLocation = { "ViewLocation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportStereoContext, ViewLocation), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewLocation_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewRotation_Inner = { "ViewRotation", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewRotation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Camera view rotation\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Camera view rotation" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewRotation = { "ViewRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportStereoContext, ViewRotation), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewRotation_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ProjectionMatrix_Inner = { "ProjectionMatrix", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ProjectionMatrix_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Camera projection Matrix\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Camera projection Matrix" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ProjectionMatrix = { "ProjectionMatrix", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportStereoContext, ProjectionMatrix), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ProjectionMatrix_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_bIsRendering_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Rendering status for this viewport (Overlay and other configuration rules can disable rendering for this viewport.)\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Rendering status for this viewport (Overlay and other configuration rules can disable rendering for this viewport.)" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_bIsRendering_SetBit(void* Obj)
	{
		((FDisplayClusterViewportStereoContext*)Obj)->bIsRendering = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_bIsRendering = { "bIsRendering", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterViewportStereoContext), &Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_bIsRendering_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_bIsRendering_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_bIsRendering_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewportID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_RectSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewLocation_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewRotation_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ViewRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ProjectionMatrix_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_ProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::NewProp_bIsRendering,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
		nullptr,
		&NewStructOps,
		"DisplayClusterViewportStereoContext",
		sizeof(FDisplayClusterViewportStereoContext),
		alignof(FDisplayClusterViewportStereoContext),
		Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterViewportStereoContext"), sizeof(FDisplayClusterViewportStereoContext), Get_Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterViewportStereoContext_Hash() { return 3954885709U; }
class UScriptStruct* FDisplayClusterViewportContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISPLAYCLUSTER_API uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisplayClusterViewportContext, Z_Construct_UPackage__Script_DisplayCluster(), TEXT("DisplayClusterViewportContext"), sizeof(FDisplayClusterViewportContext), Get_Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Hash());
	}
	return Singleton;
}
template<> DISPLAYCLUSTER_API UScriptStruct* StaticStruct<FDisplayClusterViewportContext>()
{
	return FDisplayClusterViewportContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisplayClusterViewportContext(FDisplayClusterViewportContext::StaticStruct, TEXT("/Script/DisplayCluster"), TEXT("DisplayClusterViewportContext"), false, nullptr, nullptr);
static struct FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterViewportContext
{
	FScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterViewportContext()
	{
		UScriptStruct::DeferCppStructOps<FDisplayClusterViewportContext>(FName(TEXT("DisplayClusterViewportContext")));
	}
} ScriptStruct_DisplayCluster_StaticRegisterNativesFDisplayClusterViewportContext;
	struct Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ViewportID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RectLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RectLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RectSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RectSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectionMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ProjectionMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsRendering_MetaData[];
#endif
		static void NewProp_bIsRendering_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsRendering;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisplayClusterViewportContext>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewportID_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Viewport Name\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Viewport Name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewportID = { "ViewportID", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportContext, ViewportID), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewportID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewportID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectLocation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Location on a backbuffer.\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Location on a backbuffer." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectLocation = { "RectLocation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportContext, RectLocation), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectSize_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Size on a backbuffer.\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Size on a backbuffer." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectSize = { "RectSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportContext, RectSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewLocation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Camera view location\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Camera view location" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewLocation = { "ViewLocation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportContext, ViewLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewRotation_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Camera view rotation\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Camera view rotation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewRotation = { "ViewRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportContext, ViewRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ProjectionMatrix_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Camera projection Matrix\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Camera projection Matrix" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ProjectionMatrix = { "ProjectionMatrix", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisplayClusterViewportContext, ProjectionMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ProjectionMatrix_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ProjectionMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_bIsRendering_MetaData[] = {
		{ "Category", "NDisplay" },
		{ "Comment", "// Rendering status for this viewport (Overlay and other configuration rules can disable rendering for this viewport.)\n" },
		{ "ModuleRelativePath", "Public/Blueprints/DisplayClusterBlueprintContainers.h" },
		{ "ToolTip", "Rendering status for this viewport (Overlay and other configuration rules can disable rendering for this viewport.)" },
	};
#endif
	void Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_bIsRendering_SetBit(void* Obj)
	{
		((FDisplayClusterViewportContext*)Obj)->bIsRendering = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_bIsRendering = { "bIsRendering", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FDisplayClusterViewportContext), &Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_bIsRendering_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_bIsRendering_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_bIsRendering_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewportID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_RectSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ViewRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_ProjectionMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::NewProp_bIsRendering,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisplayCluster,
		nullptr,
		&NewStructOps,
		"DisplayClusterViewportContext",
		sizeof(FDisplayClusterViewportContext),
		alignof(FDisplayClusterViewportContext),
		Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisplayClusterViewportContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisplayCluster();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisplayClusterViewportContext"), sizeof(FDisplayClusterViewportContext), Get_Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisplayClusterViewportContext_Hash() { return 419072688U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
