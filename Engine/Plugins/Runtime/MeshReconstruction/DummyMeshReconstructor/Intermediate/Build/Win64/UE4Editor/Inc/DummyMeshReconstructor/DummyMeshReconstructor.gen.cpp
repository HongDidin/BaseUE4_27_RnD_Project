// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DummyMeshReconstructor/Public/DummyMeshReconstructor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDummyMeshReconstructor() {}
// Cross Module References
	DUMMYMESHRECONSTRUCTOR_API UClass* Z_Construct_UClass_UDummyMeshReconstructor_NoRegister();
	DUMMYMESHRECONSTRUCTOR_API UClass* Z_Construct_UClass_UDummyMeshReconstructor();
	MRMESH_API UClass* Z_Construct_UClass_UMeshReconstructorBase();
	UPackage* Z_Construct_UPackage__Script_DummyMeshReconstructor();
// End Cross Module References
	void UDummyMeshReconstructor::StaticRegisterNativesUDummyMeshReconstructor()
	{
	}
	UClass* Z_Construct_UClass_UDummyMeshReconstructor_NoRegister()
	{
		return UDummyMeshReconstructor::StaticClass();
	}
	struct Z_Construct_UClass_UDummyMeshReconstructor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDummyMeshReconstructor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshReconstructorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_DummyMeshReconstructor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDummyMeshReconstructor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Experimental", "" },
		{ "IncludePath", "DummyMeshReconstructor.h" },
		{ "ModuleRelativePath", "Public/DummyMeshReconstructor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDummyMeshReconstructor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDummyMeshReconstructor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDummyMeshReconstructor_Statics::ClassParams = {
		&UDummyMeshReconstructor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDummyMeshReconstructor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDummyMeshReconstructor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDummyMeshReconstructor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDummyMeshReconstructor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDummyMeshReconstructor, 665074762);
	template<> DUMMYMESHRECONSTRUCTOR_API UClass* StaticClass<UDummyMeshReconstructor>()
	{
		return UDummyMeshReconstructor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDummyMeshReconstructor(Z_Construct_UClass_UDummyMeshReconstructor, &UDummyMeshReconstructor::StaticClass, TEXT("/Script/DummyMeshReconstructor"), TEXT("UDummyMeshReconstructor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDummyMeshReconstructor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
