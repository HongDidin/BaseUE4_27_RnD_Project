// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPATIALIZATION_ITDSpatializationSourceSettings_generated_h
#error "ITDSpatializationSourceSettings.generated.h already included, missing '#pragma once' in ITDSpatializationSourceSettings.h"
#endif
#define SPATIALIZATION_ITDSpatializationSourceSettings_generated_h

#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUITDSpatializationSourceSettings(); \
	friend struct Z_Construct_UClass_UITDSpatializationSourceSettings_Statics; \
public: \
	DECLARE_CLASS(UITDSpatializationSourceSettings, USpatializationPluginSourceSettingsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spatialization"), NO_API) \
	DECLARE_SERIALIZER(UITDSpatializationSourceSettings)


#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUITDSpatializationSourceSettings(); \
	friend struct Z_Construct_UClass_UITDSpatializationSourceSettings_Statics; \
public: \
	DECLARE_CLASS(UITDSpatializationSourceSettings, USpatializationPluginSourceSettingsBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Spatialization"), NO_API) \
	DECLARE_SERIALIZER(UITDSpatializationSourceSettings)


#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UITDSpatializationSourceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UITDSpatializationSourceSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UITDSpatializationSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UITDSpatializationSourceSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UITDSpatializationSourceSettings(UITDSpatializationSourceSettings&&); \
	NO_API UITDSpatializationSourceSettings(const UITDSpatializationSourceSettings&); \
public:


#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UITDSpatializationSourceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UITDSpatializationSourceSettings(UITDSpatializationSourceSettings&&); \
	NO_API UITDSpatializationSourceSettings(const UITDSpatializationSourceSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UITDSpatializationSourceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UITDSpatializationSourceSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UITDSpatializationSourceSettings)


#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_10_PROLOG
#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_INCLASS \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPATIALIZATION_API UClass* StaticClass<class UITDSpatializationSourceSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Spatialization_Source_Spatialization_Public_ITDSpatializationSourceSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
