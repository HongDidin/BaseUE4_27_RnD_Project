// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SPATIALIZATIONEDITOR_ITDSpatializationSourceSettingsFactory_generated_h
#error "ITDSpatializationSourceSettingsFactory.generated.h already included, missing '#pragma once' in ITDSpatializationSourceSettingsFactory.h"
#endif
#define SPATIALIZATIONEDITOR_ITDSpatializationSourceSettingsFactory_generated_h

#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_SPARSE_DATA
#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUITDSpatializationSettingsFactory(); \
	friend struct Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UITDSpatializationSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SpatializationEditor"), SPATIALIZATIONEDITOR_API) \
	DECLARE_SERIALIZER(UITDSpatializationSettingsFactory)


#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUITDSpatializationSettingsFactory(); \
	friend struct Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UITDSpatializationSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SpatializationEditor"), SPATIALIZATIONEDITOR_API) \
	DECLARE_SERIALIZER(UITDSpatializationSettingsFactory)


#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SPATIALIZATIONEDITOR_API UITDSpatializationSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UITDSpatializationSettingsFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SPATIALIZATIONEDITOR_API, UITDSpatializationSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UITDSpatializationSettingsFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SPATIALIZATIONEDITOR_API UITDSpatializationSettingsFactory(UITDSpatializationSettingsFactory&&); \
	SPATIALIZATIONEDITOR_API UITDSpatializationSettingsFactory(const UITDSpatializationSettingsFactory&); \
public:


#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SPATIALIZATIONEDITOR_API UITDSpatializationSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SPATIALIZATIONEDITOR_API UITDSpatializationSettingsFactory(UITDSpatializationSettingsFactory&&); \
	SPATIALIZATIONEDITOR_API UITDSpatializationSettingsFactory(const UITDSpatializationSettingsFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SPATIALIZATIONEDITOR_API, UITDSpatializationSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UITDSpatializationSettingsFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UITDSpatializationSettingsFactory)


#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_23_PROLOG
#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_SPARSE_DATA \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_INCLASS \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_SPARSE_DATA \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h_26_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ITDSpatializationSettingsFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SPATIALIZATIONEDITOR_API UClass* StaticClass<class UITDSpatializationSettingsFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Spatialization_Source_SpatializationEditor_Public_ITDSpatializationSourceSettingsFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
