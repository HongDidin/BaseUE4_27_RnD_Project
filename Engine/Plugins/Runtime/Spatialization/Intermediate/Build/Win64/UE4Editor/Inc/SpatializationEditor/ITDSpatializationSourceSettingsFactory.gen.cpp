// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SpatializationEditor/Public/ITDSpatializationSourceSettingsFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeITDSpatializationSourceSettingsFactory() {}
// Cross Module References
	SPATIALIZATIONEDITOR_API UClass* Z_Construct_UClass_UITDSpatializationSettingsFactory_NoRegister();
	SPATIALIZATIONEDITOR_API UClass* Z_Construct_UClass_UITDSpatializationSettingsFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_SpatializationEditor();
// End Cross Module References
	void UITDSpatializationSettingsFactory::StaticRegisterNativesUITDSpatializationSettingsFactory()
	{
	}
	UClass* Z_Construct_UClass_UITDSpatializationSettingsFactory_NoRegister()
	{
		return UITDSpatializationSettingsFactory::StaticClass();
	}
	struct Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_SpatializationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "ITDSpatializationSourceSettingsFactory.h" },
		{ "ModuleRelativePath", "Public/ITDSpatializationSourceSettingsFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UITDSpatializationSettingsFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics::ClassParams = {
		&UITDSpatializationSettingsFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UITDSpatializationSettingsFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UITDSpatializationSettingsFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UITDSpatializationSettingsFactory, 1333707756);
	template<> SPATIALIZATIONEDITOR_API UClass* StaticClass<UITDSpatializationSettingsFactory>()
	{
		return UITDSpatializationSettingsFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UITDSpatializationSettingsFactory(Z_Construct_UClass_UITDSpatializationSettingsFactory, &UITDSpatializationSettingsFactory::StaticClass, TEXT("/Script/SpatializationEditor"), TEXT("UITDSpatializationSettingsFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UITDSpatializationSettingsFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
