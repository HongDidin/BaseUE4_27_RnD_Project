// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Spatialization/Public/ITDSpatializationSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeITDSpatializationSourceSettings() {}
// Cross Module References
	SPATIALIZATION_API UClass* Z_Construct_UClass_UITDSpatializationSourceSettings_NoRegister();
	SPATIALIZATION_API UClass* Z_Construct_UClass_UITDSpatializationSourceSettings();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USpatializationPluginSourceSettingsBase();
	UPackage* Z_Construct_UPackage__Script_Spatialization();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRuntimeFloatCurve();
// End Cross Module References
	void UITDSpatializationSourceSettings::StaticRegisterNativesUITDSpatializationSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UITDSpatializationSourceSettings_NoRegister()
	{
		return UITDSpatializationSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UITDSpatializationSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableILD_MetaData[];
#endif
		static void NewProp_bEnableILD_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableILD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PanningIntensityOverDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PanningIntensityOverDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USpatializationPluginSourceSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Spatialization,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ITDSpatializationSourceSettings.h" },
		{ "ModuleRelativePath", "Public/ITDSpatializationSourceSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_bEnableILD_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "Comment", "/* Whether we should use any level difference between the left and right channel in our spatialization algorithm. */" },
		{ "DisplayName", "Enable Level Panning" },
		{ "ModuleRelativePath", "Public/ITDSpatializationSourceSettings.h" },
		{ "ToolTip", "Whether we should use any level difference between the left and right channel in our spatialization algorithm." },
	};
#endif
	void Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_bEnableILD_SetBit(void* Obj)
	{
		((UITDSpatializationSourceSettings*)Obj)->bEnableILD = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_bEnableILD = { "bEnableILD", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UITDSpatializationSourceSettings), &Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_bEnableILD_SetBit, METADATA_PARAMS(Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_bEnableILD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_bEnableILD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_PanningIntensityOverDistance_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "Comment", "/* This curve will map the intensity of panning (y-axis, 0.0-1.0) as a factor of distance (in Unreal Units) */" },
		{ "ModuleRelativePath", "Public/ITDSpatializationSourceSettings.h" },
		{ "ToolTip", "This curve will map the intensity of panning (y-axis, 0.0-1.0) as a factor of distance (in Unreal Units)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_PanningIntensityOverDistance = { "PanningIntensityOverDistance", nullptr, (EPropertyFlags)0x0010000000044005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UITDSpatializationSourceSettings, PanningIntensityOverDistance), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_PanningIntensityOverDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_PanningIntensityOverDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_bEnableILD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::NewProp_PanningIntensityOverDistance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UITDSpatializationSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::ClassParams = {
		&UITDSpatializationSourceSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::PropPointers),
		0,
		0x001010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UITDSpatializationSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UITDSpatializationSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UITDSpatializationSourceSettings, 4042530872);
	template<> SPATIALIZATION_API UClass* StaticClass<UITDSpatializationSourceSettings>()
	{
		return UITDSpatializationSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UITDSpatializationSourceSettings(Z_Construct_UClass_UITDSpatializationSourceSettings, &UITDSpatializationSourceSettings::StaticClass, TEXT("/Script/Spatialization"), TEXT("UITDSpatializationSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UITDSpatializationSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
