// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATION_SoundModulationEnvelopeFollower_generated_h
#error "SoundModulationEnvelopeFollower.generated.h already included, missing '#pragma once' in SoundModulationEnvelopeFollower.h"
#endif
#define AUDIOMODULATION_SoundModulationEnvelopeFollower_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_26_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics; \
	AUDIOMODULATION_API static class UScriptStruct* StaticStruct();


template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<struct FEnvelopeFollowerGeneratorParams>();

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationGeneratorEnvelopeFollower(); \
	friend struct Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGeneratorEnvelopeFollower, USoundModulationGenerator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationGeneratorEnvelopeFollower)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationGeneratorEnvelopeFollower(); \
	friend struct Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGeneratorEnvelopeFollower, USoundModulationGenerator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationGeneratorEnvelopeFollower)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationGeneratorEnvelopeFollower(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGeneratorEnvelopeFollower) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationGeneratorEnvelopeFollower); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGeneratorEnvelopeFollower); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationGeneratorEnvelopeFollower(USoundModulationGeneratorEnvelopeFollower&&); \
	NO_API USoundModulationGeneratorEnvelopeFollower(const USoundModulationGeneratorEnvelopeFollower&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationGeneratorEnvelopeFollower(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationGeneratorEnvelopeFollower(USoundModulationGeneratorEnvelopeFollower&&); \
	NO_API USoundModulationGeneratorEnvelopeFollower(const USoundModulationGeneratorEnvelopeFollower&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationGeneratorEnvelopeFollower); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGeneratorEnvelopeFollower); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGeneratorEnvelopeFollower)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_85_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h_88_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATION_API UClass* StaticClass<class USoundModulationGeneratorEnvelopeFollower>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationEnvelopeFollower_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
