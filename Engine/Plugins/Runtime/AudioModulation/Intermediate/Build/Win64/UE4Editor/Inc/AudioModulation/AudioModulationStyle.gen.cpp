// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/AudioModulationStyle.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAudioModulationStyle() {}
// Cross Module References
	AUDIOMODULATION_API UClass* Z_Construct_UClass_UAudioModulationStyle_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_UAudioModulationStyle();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
// End Cross Module References
	DEFINE_FUNCTION(UAudioModulationStyle::execGetParameterColor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FColor*)Z_Param__Result=UAudioModulationStyle::GetParameterColor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStyle::execGetPatchColor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FColor*)Z_Param__Result=UAudioModulationStyle::GetPatchColor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStyle::execGetControlBusMixColor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FColor*)Z_Param__Result=UAudioModulationStyle::GetControlBusMixColor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStyle::execGetControlBusColor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FColor*)Z_Param__Result=UAudioModulationStyle::GetControlBusColor();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStyle::execGetModulationGeneratorColor)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FColor*)Z_Param__Result=UAudioModulationStyle::GetModulationGeneratorColor();
		P_NATIVE_END;
	}
	void UAudioModulationStyle::StaticRegisterNativesUAudioModulationStyle()
	{
		UClass* Class = UAudioModulationStyle::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetControlBusColor", &UAudioModulationStyle::execGetControlBusColor },
			{ "GetControlBusMixColor", &UAudioModulationStyle::execGetControlBusMixColor },
			{ "GetModulationGeneratorColor", &UAudioModulationStyle::execGetModulationGeneratorColor },
			{ "GetParameterColor", &UAudioModulationStyle::execGetParameterColor },
			{ "GetPatchColor", &UAudioModulationStyle::execGetPatchColor },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics
	{
		struct AudioModulationStyle_eventGetControlBusColor_Parms
		{
			FColor ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000582, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStyle_eventGetControlBusColor_Parms, ReturnValue), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|Modulation|Style" },
		{ "ModuleRelativePath", "Public/AudioModulationStyle.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStyle, nullptr, "GetControlBusColor", nullptr, nullptr, sizeof(AudioModulationStyle_eventGetControlBusColor_Parms), Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics
	{
		struct AudioModulationStyle_eventGetControlBusMixColor_Parms
		{
			FColor ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000582, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStyle_eventGetControlBusMixColor_Parms, ReturnValue), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|Modulation|Style" },
		{ "ModuleRelativePath", "Public/AudioModulationStyle.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStyle, nullptr, "GetControlBusMixColor", nullptr, nullptr, sizeof(AudioModulationStyle_eventGetControlBusMixColor_Parms), Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics
	{
		struct AudioModulationStyle_eventGetModulationGeneratorColor_Parms
		{
			FColor ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000582, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStyle_eventGetModulationGeneratorColor_Parms, ReturnValue), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|Modulation|Style" },
		{ "ModuleRelativePath", "Public/AudioModulationStyle.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStyle, nullptr, "GetModulationGeneratorColor", nullptr, nullptr, sizeof(AudioModulationStyle_eventGetModulationGeneratorColor_Parms), Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics
	{
		struct AudioModulationStyle_eventGetParameterColor_Parms
		{
			FColor ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000582, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStyle_eventGetParameterColor_Parms, ReturnValue), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|Modulation|Style" },
		{ "ModuleRelativePath", "Public/AudioModulationStyle.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStyle, nullptr, "GetParameterColor", nullptr, nullptr, sizeof(AudioModulationStyle_eventGetParameterColor_Parms), Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics
	{
		struct AudioModulationStyle_eventGetPatchColor_Parms
		{
			FColor ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000582, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStyle_eventGetPatchColor_Parms, ReturnValue), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio|Modulation|Style" },
		{ "ModuleRelativePath", "Public/AudioModulationStyle.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStyle, nullptr, "GetPatchColor", nullptr, nullptr, sizeof(AudioModulationStyle_eventGetPatchColor_Parms), Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAudioModulationStyle_NoRegister()
	{
		return UAudioModulationStyle::StaticClass();
	}
	struct Z_Construct_UClass_UAudioModulationStyle_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAudioModulationStyle_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAudioModulationStyle_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAudioModulationStyle_GetControlBusColor, "GetControlBusColor" }, // 1644713307
		{ &Z_Construct_UFunction_UAudioModulationStyle_GetControlBusMixColor, "GetControlBusMixColor" }, // 1396037448
		{ &Z_Construct_UFunction_UAudioModulationStyle_GetModulationGeneratorColor, "GetModulationGeneratorColor" }, // 4178104060
		{ &Z_Construct_UFunction_UAudioModulationStyle_GetParameterColor, "GetParameterColor" }, // 1540825362
		{ &Z_Construct_UFunction_UAudioModulationStyle_GetPatchColor, "GetPatchColor" }, // 388474194
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioModulationStyle_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AudioModulationStyle.h" },
		{ "ModuleRelativePath", "Public/AudioModulationStyle.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAudioModulationStyle_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAudioModulationStyle>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAudioModulationStyle_Statics::ClassParams = {
		&UAudioModulationStyle::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAudioModulationStyle_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioModulationStyle_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAudioModulationStyle()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAudioModulationStyle_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAudioModulationStyle, 1940693741);
	template<> AUDIOMODULATION_API UClass* StaticClass<UAudioModulationStyle>()
	{
		return UAudioModulationStyle::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAudioModulationStyle(Z_Construct_UClass_UAudioModulationStyle, &UAudioModulationStyle::StaticClass, TEXT("/Script/AudioModulation"), TEXT("UAudioModulationStyle"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAudioModulationStyle);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
