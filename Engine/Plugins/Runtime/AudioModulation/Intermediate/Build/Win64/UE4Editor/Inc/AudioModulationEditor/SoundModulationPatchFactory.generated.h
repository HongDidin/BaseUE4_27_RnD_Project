// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATIONEDITOR_SoundModulationPatchFactory_generated_h
#error "SoundModulationPatchFactory.generated.h already included, missing '#pragma once' in SoundModulationPatchFactory.h"
#endif
#define AUDIOMODULATIONEDITOR_SoundModulationPatchFactory_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationPatchFactory(); \
	friend struct Z_Construct_UClass_USoundModulationPatchFactory_Statics; \
public: \
	DECLARE_CLASS(USoundModulationPatchFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), AUDIOMODULATIONEDITOR_API) \
	DECLARE_SERIALIZER(USoundModulationPatchFactory)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationPatchFactory(); \
	friend struct Z_Construct_UClass_USoundModulationPatchFactory_Statics; \
public: \
	DECLARE_CLASS(USoundModulationPatchFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), AUDIOMODULATIONEDITOR_API) \
	DECLARE_SERIALIZER(USoundModulationPatchFactory)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOMODULATIONEDITOR_API USoundModulationPatchFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationPatchFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOMODULATIONEDITOR_API, USoundModulationPatchFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationPatchFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOMODULATIONEDITOR_API USoundModulationPatchFactory(USoundModulationPatchFactory&&); \
	AUDIOMODULATIONEDITOR_API USoundModulationPatchFactory(const USoundModulationPatchFactory&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOMODULATIONEDITOR_API USoundModulationPatchFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOMODULATIONEDITOR_API USoundModulationPatchFactory(USoundModulationPatchFactory&&); \
	AUDIOMODULATIONEDITOR_API USoundModulationPatchFactory(const USoundModulationPatchFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOMODULATIONEDITOR_API, USoundModulationPatchFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationPatchFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationPatchFactory)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_11_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundModulationPatchFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<class USoundModulationPatchFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationPatchFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
