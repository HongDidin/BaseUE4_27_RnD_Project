// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATION_SoundModulationLFO_generated_h
#error "SoundModulationLFO.generated.h already included, missing '#pragma once' in SoundModulationLFO.h"
#endif
#define AUDIOMODULATION_SoundModulationLFO_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_32_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics; \
	AUDIOMODULATION_API static class UScriptStruct* StaticStruct();


template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<struct FSoundModulationLFOParams>();

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationGeneratorLFO(); \
	friend struct Z_Construct_UClass_USoundModulationGeneratorLFO_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGeneratorLFO, USoundModulationGenerator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), AUDIOMODULATION_API) \
	DECLARE_SERIALIZER(USoundModulationGeneratorLFO)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationGeneratorLFO(); \
	friend struct Z_Construct_UClass_USoundModulationGeneratorLFO_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGeneratorLFO, USoundModulationGenerator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), AUDIOMODULATION_API) \
	DECLARE_SERIALIZER(USoundModulationGeneratorLFO)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOMODULATION_API USoundModulationGeneratorLFO(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGeneratorLFO) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOMODULATION_API, USoundModulationGeneratorLFO); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGeneratorLFO); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOMODULATION_API USoundModulationGeneratorLFO(USoundModulationGeneratorLFO&&); \
	AUDIOMODULATION_API USoundModulationGeneratorLFO(const USoundModulationGeneratorLFO&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOMODULATION_API USoundModulationGeneratorLFO(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOMODULATION_API USoundModulationGeneratorLFO(USoundModulationGeneratorLFO&&); \
	AUDIOMODULATION_API USoundModulationGeneratorLFO(const USoundModulationGeneratorLFO&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOMODULATION_API, USoundModulationGeneratorLFO); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGeneratorLFO); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGeneratorLFO)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_116_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h_119_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATION_API UClass* StaticClass<class USoundModulationGeneratorLFO>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_Generators_SoundModulationLFO_h


#define FOREACH_ENUM_FSOUNDMODULATIONLFOSHAPE(op) \
	op(FSoundModulationLFOShape::Sine) \
	op(FSoundModulationLFOShape::UpSaw) \
	op(FSoundModulationLFOShape::DownSaw) \
	op(FSoundModulationLFOShape::Square) \
	op(FSoundModulationLFOShape::Triangle) \
	op(FSoundModulationLFOShape::Exponential) \
	op(FSoundModulationLFOShape::RandomSampleHold) \
	op(FSoundModulationLFOShape::COUNT) 

enum class FSoundModulationLFOShape : uint8;
template<> AUDIOMODULATION_API UEnum* StaticEnum<FSoundModulationLFOShape>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
