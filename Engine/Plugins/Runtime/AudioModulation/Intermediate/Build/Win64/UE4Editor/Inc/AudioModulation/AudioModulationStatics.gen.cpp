// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/AudioModulationStatics.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAudioModulationStatics() {}
// Cross Module References
	AUDIOMODULATION_API UClass* Z_Construct_UClass_UAudioModulationStatics_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_UAudioModulationStatics();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundControlBus_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundControlBusMix_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGenerator_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameter_NoRegister();
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundControlBusMixStage();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USoundModulatorBase_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UAudioModulationStatics::execUpdateModulator)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundModulatorBase,Z_Param_Modulator);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::UpdateModulator(Z_Param_WorldContextObject,Z_Param_Modulator);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execUpdateMixFromObject)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_GET_PROPERTY(FFloatProperty,Z_Param_FadeTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::UpdateMixFromObject(Z_Param_WorldContextObject,Z_Param_Mix,Z_Param_FadeTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execUpdateMixByFilter)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_GET_PROPERTY(FStrProperty,Z_Param_AddressFilter);
		P_GET_OBJECT(UClass,Z_Param_ParamClassFilter);
		P_GET_OBJECT(USoundModulationParameter,Z_Param_ParamFilter);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Value);
		P_GET_PROPERTY(FFloatProperty,Z_Param_FadeTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::UpdateMixByFilter(Z_Param_WorldContextObject,Z_Param_Mix,Z_Param_AddressFilter,Z_Param_ParamClassFilter,Z_Param_ParamFilter,Z_Param_Value,Z_Param_FadeTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execUpdateMix)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_GET_TARRAY(FSoundControlBusMixStage,Z_Param_Stages);
		P_GET_PROPERTY(FFloatProperty,Z_Param_FadeTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::UpdateMix(Z_Param_WorldContextObject,Z_Param_Mix,Z_Param_Stages,Z_Param_FadeTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execLoadMixFromProfile)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_GET_UBOOL(Z_Param_bActivate);
		P_GET_PROPERTY(FIntProperty,Z_Param_ProfileIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FSoundControlBusMixStage>*)Z_Param__Result=UAudioModulationStatics::LoadMixFromProfile(Z_Param_WorldContextObject,Z_Param_Mix,Z_Param_bActivate,Z_Param_ProfileIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execSaveMixToProfile)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_GET_PROPERTY(FIntProperty,Z_Param_ProfileIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::SaveMixToProfile(Z_Param_WorldContextObject,Z_Param_Mix,Z_Param_ProfileIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execDeactivateGenerator)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundModulationGenerator,Z_Param_Generator);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::DeactivateGenerator(Z_Param_WorldContextObject,Z_Param_Generator);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execDeactivateBusMix)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::DeactivateBusMix(Z_Param_WorldContextObject,Z_Param_Mix);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execDeactivateBus)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBus,Z_Param_Bus);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::DeactivateBus(Z_Param_WorldContextObject,Z_Param_Bus);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execCreateBusMix)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_TARRAY(FSoundControlBusMixStage,Z_Param_Stages);
		P_GET_UBOOL(Z_Param_Activate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(USoundControlBusMix**)Z_Param__Result=UAudioModulationStatics::CreateBusMix(Z_Param_WorldContextObject,Z_Param_Name,Z_Param_Stages,Z_Param_Activate);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execCreateBusMixStage)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBus,Z_Param_Bus);
		P_GET_PROPERTY(FFloatProperty,Z_Param_Value);
		P_GET_PROPERTY(FFloatProperty,Z_Param_AttackTime);
		P_GET_PROPERTY(FFloatProperty,Z_Param_ReleaseTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FSoundControlBusMixStage*)Z_Param__Result=UAudioModulationStatics::CreateBusMixStage(Z_Param_WorldContextObject,Z_Param_Bus,Z_Param_Value,Z_Param_AttackTime,Z_Param_ReleaseTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execCreateBus)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_OBJECT(USoundModulationParameter,Z_Param_Parameter);
		P_GET_UBOOL(Z_Param_Activate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(USoundControlBus**)Z_Param__Result=UAudioModulationStatics::CreateBus(Z_Param_WorldContextObject,Z_Param_Name,Z_Param_Parameter,Z_Param_Activate);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execActivateGenerator)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundModulationGenerator,Z_Param_Generator);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::ActivateGenerator(Z_Param_WorldContextObject,Z_Param_Generator);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execActivateBusMix)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBusMix,Z_Param_Mix);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::ActivateBusMix(Z_Param_WorldContextObject,Z_Param_Mix);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UAudioModulationStatics::execActivateBus)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(USoundControlBus,Z_Param_Bus);
		P_FINISH;
		P_NATIVE_BEGIN;
		UAudioModulationStatics::ActivateBus(Z_Param_WorldContextObject,Z_Param_Bus);
		P_NATIVE_END;
	}
	void UAudioModulationStatics::StaticRegisterNativesUAudioModulationStatics()
	{
		UClass* Class = UAudioModulationStatics::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ActivateBus", &UAudioModulationStatics::execActivateBus },
			{ "ActivateBusMix", &UAudioModulationStatics::execActivateBusMix },
			{ "ActivateGenerator", &UAudioModulationStatics::execActivateGenerator },
			{ "CreateBus", &UAudioModulationStatics::execCreateBus },
			{ "CreateBusMix", &UAudioModulationStatics::execCreateBusMix },
			{ "CreateBusMixStage", &UAudioModulationStatics::execCreateBusMixStage },
			{ "DeactivateBus", &UAudioModulationStatics::execDeactivateBus },
			{ "DeactivateBusMix", &UAudioModulationStatics::execDeactivateBusMix },
			{ "DeactivateGenerator", &UAudioModulationStatics::execDeactivateGenerator },
			{ "LoadMixFromProfile", &UAudioModulationStatics::execLoadMixFromProfile },
			{ "SaveMixToProfile", &UAudioModulationStatics::execSaveMixToProfile },
			{ "UpdateMix", &UAudioModulationStatics::execUpdateMix },
			{ "UpdateMixByFilter", &UAudioModulationStatics::execUpdateMixByFilter },
			{ "UpdateMixFromObject", &UAudioModulationStatics::execUpdateMixFromObject },
			{ "UpdateModulator", &UAudioModulationStatics::execUpdateModulator },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics
	{
		struct AudioModulationStatics_eventActivateBus_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBus* Bus;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventActivateBus_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::NewProp_Bus = { "Bus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventActivateBus_Parms, Bus), Z_Construct_UClass_USoundControlBus_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::NewProp_Bus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Activates a modulation bus. Does nothing if an instance of the provided bus is already active\n\x09 * @param Bus - Bus to activate\n\x09 */" },
		{ "DisplayName", "Activate Control Bus" },
		{ "Keywords", "activate modulation modulator control bus" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Activates a modulation bus. Does nothing if an instance of the provided bus is already active\n@param Bus - Bus to activate" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "ActivateBus", nullptr, nullptr, sizeof(AudioModulationStatics_eventActivateBus_Parms), Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_ActivateBus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_ActivateBus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics
	{
		struct AudioModulationStatics_eventActivateBusMix_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBusMix* Mix;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventActivateBusMix_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventActivateBusMix_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::NewProp_Mix,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Activates a bus modulator mix. Does nothing if an instance of the provided bus mix is already active\n\x09 * @param BusMix - Mix to activate\n\x09 */" },
		{ "DisplayName", "Activate Control Bus Mix" },
		{ "Keywords", "activate modulation modulator control bus mix" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Activates a bus modulator mix. Does nothing if an instance of the provided bus mix is already active\n@param BusMix - Mix to activate" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "ActivateBusMix", nullptr, nullptr, sizeof(AudioModulationStatics_eventActivateBusMix_Parms), Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics
	{
		struct AudioModulationStatics_eventActivateGenerator_Parms
		{
			const UObject* WorldContextObject;
			USoundModulationGenerator* Generator;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Generator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventActivateGenerator_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::NewProp_Generator = { "Generator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventActivateGenerator_Parms, Generator), Z_Construct_UClass_USoundModulationGenerator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::NewProp_Generator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Activates a modulation generator. Does nothing if an instance of the provided generator is already active\n\x09 * @param Modulator - Modulator to activate\n\x09 */" },
		{ "DisplayName", "Activate Modulation Generator" },
		{ "Keywords", "activate modulation modulator generator lfo" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Activates a modulation generator. Does nothing if an instance of the provided generator is already active\n@param Modulator - Modulator to activate" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "ActivateGenerator", nullptr, nullptr, sizeof(AudioModulationStatics_eventActivateGenerator_Parms), Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics
	{
		struct AudioModulationStatics_eventCreateBus_Parms
		{
			const UObject* WorldContextObject;
			FName Name;
			USoundModulationParameter* Parameter;
			bool Activate;
			USoundControlBus* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parameter;
		static void NewProp_Activate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Activate;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBus_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBus_Parms, Name), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Parameter = { "Parameter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBus_Parms, Parameter), Z_Construct_UClass_USoundModulationParameter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Activate_SetBit(void* Obj)
	{
		((AudioModulationStatics_eventCreateBus_Parms*)Obj)->Activate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Activate = { "Activate", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AudioModulationStatics_eventCreateBus_Parms), &Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Activate_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBus_Parms, ReturnValue), Z_Construct_UClass_USoundControlBus_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Parameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_Activate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "Category", "Audio" },
		{ "Comment", "/** Creates a modulation bus with the provided default value.\n\x09 * @param Name - Name of bus\n\x09 * @param Parameter - Default value for created bus\n\x09 * @param Activate - Whether or not to activate bus on creation.\n\x09 */" },
		{ "CPP_Default_Activate", "true" },
		{ "DisplayName", "Create Control Bus" },
		{ "Keywords", "make create bus modulation LPF modulator" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Creates a modulation bus with the provided default value.\n@param Name - Name of bus\n@param Parameter - Default value for created bus\n@param Activate - Whether or not to activate bus on creation." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "CreateBus", nullptr, nullptr, sizeof(AudioModulationStatics_eventCreateBus_Parms), Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_CreateBus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_CreateBus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics
	{
		struct AudioModulationStatics_eventCreateBusMix_Parms
		{
			const UObject* WorldContextObject;
			FName Name;
			TArray<FSoundControlBusMixStage> Stages;
			bool Activate;
			USoundControlBusMix* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Stages_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Stages;
		static void NewProp_Activate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Activate;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMix_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMix_Parms, Name), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Stages_Inner = { "Stages", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundControlBusMixStage, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Stages = { "Stages", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMix_Parms, Stages), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Activate_SetBit(void* Obj)
	{
		((AudioModulationStatics_eventCreateBusMix_Parms*)Obj)->Activate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Activate = { "Activate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AudioModulationStatics_eventCreateBusMix_Parms), &Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Activate_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMix_Parms, ReturnValue), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Stages_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Stages,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_Activate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Creates a modulation bus mix, with a bus channel set to the provided target value\n\x09 * @param Name - Name of mix.\n\x09 * @param Stages - Stages mix is responsible for.\n\x09 * @param Activate - Whether or not to activate mix on creation.\n\x09 */" },
		{ "DisplayName", "Create Control Bus Mix" },
		{ "Keywords", "make create control bus mix modulation modulator" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Creates a modulation bus mix, with a bus channel set to the provided target value\n@param Name - Name of mix.\n@param Stages - Stages mix is responsible for.\n@param Activate - Whether or not to activate mix on creation." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "CreateBusMix", nullptr, nullptr, sizeof(AudioModulationStatics_eventCreateBusMix_Parms), Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics
	{
		struct AudioModulationStatics_eventCreateBusMixStage_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBus* Bus;
			float Value;
			float AttackTime;
			float ReleaseTime;
			FSoundControlBusMixStage ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bus;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AttackTime;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReleaseTime;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMixStage_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_Bus = { "Bus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMixStage_Parms, Bus), Z_Construct_UClass_USoundControlBus_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMixStage_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_AttackTime = { "AttackTime", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMixStage_Parms, AttackTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_ReleaseTime = { "ReleaseTime", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMixStage_Parms, ReleaseTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventCreateBusMixStage_Parms, ReturnValue), Z_Construct_UScriptStruct_FSoundControlBusMixStage, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_Bus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_AttackTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_ReleaseTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "3" },
		{ "Category", "Audio" },
		{ "Comment", "/** Creates a stage used to mix a control bus.\n\x09 * @param Bus - Bus stage is in charge of applying mix value to.\n\x09 * @param Value - Value for added bus stage to target when mix is active.\n\x09 * @param AttackTime - Time in seconds for stage to mix in.\n\x09 * @param ReleaseTime - Time in seconds for stage to mix out.\n\x09 */" },
		{ "CPP_Default_AttackTime", "0.100000" },
		{ "CPP_Default_ReleaseTime", "0.100000" },
		{ "DisplayName", "Create Control Bus Mix Stage" },
		{ "Keywords", "make create control bus mix modulation modulator stage" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Creates a stage used to mix a control bus.\n@param Bus - Bus stage is in charge of applying mix value to.\n@param Value - Value for added bus stage to target when mix is active.\n@param AttackTime - Time in seconds for stage to mix in.\n@param ReleaseTime - Time in seconds for stage to mix out." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "CreateBusMixStage", nullptr, nullptr, sizeof(AudioModulationStatics_eventCreateBusMixStage_Parms), Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics
	{
		struct AudioModulationStatics_eventDeactivateBus_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBus* Bus;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventDeactivateBus_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::NewProp_Bus = { "Bus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventDeactivateBus_Parms, Bus), Z_Construct_UClass_USoundControlBus_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::NewProp_Bus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Deactivates a bus. Does nothing if the provided bus is already inactive\n\x09 * @param Bus - Scope of modulator\n\x09 */" },
		{ "DisplayName", "Deactivate Control Bus" },
		{ "Keywords", "deactivate modulation modulator bus" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Deactivates a bus. Does nothing if the provided bus is already inactive\n@param Bus - Scope of modulator" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "DeactivateBus", nullptr, nullptr, sizeof(AudioModulationStatics_eventDeactivateBus_Parms), Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics
	{
		struct AudioModulationStatics_eventDeactivateBusMix_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBusMix* Mix;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventDeactivateBusMix_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventDeactivateBusMix_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::NewProp_Mix,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Deactivates a modulation bus mix. Does nothing if an instance of the provided bus mix is already inactive\n\x09 * @param BusMix - Mix to deactivate\n\x09 */" },
		{ "DisplayName", "Deactivate Control Bus Mix" },
		{ "Keywords", "deactivate modulation modulator" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Deactivates a modulation bus mix. Does nothing if an instance of the provided bus mix is already inactive\n@param BusMix - Mix to deactivate" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "DeactivateBusMix", nullptr, nullptr, sizeof(AudioModulationStatics_eventDeactivateBusMix_Parms), Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics
	{
		struct AudioModulationStatics_eventDeactivateGenerator_Parms
		{
			const UObject* WorldContextObject;
			USoundModulationGenerator* Generator;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Generator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventDeactivateGenerator_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::NewProp_Generator = { "Generator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventDeactivateGenerator_Parms, Generator), Z_Construct_UClass_USoundModulationGenerator_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::NewProp_Generator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Deactivates a modulation generator. Does nothing if an instance of the provided generator is already inactive\n\x09 * @param Generator - Generator to activate\n\x09 * @param Scope - Scope of modulator\n\x09 */" },
		{ "DisplayName", "Deactivate Control Bus Modulator" },
		{ "Keywords", "deactivate bus modulation modulator" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Deactivates a modulation generator. Does nothing if an instance of the provided generator is already inactive\n@param Generator - Generator to activate\n@param Scope - Scope of modulator" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "DeactivateGenerator", nullptr, nullptr, sizeof(AudioModulationStatics_eventDeactivateGenerator_Parms), Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics
	{
		struct AudioModulationStatics_eventLoadMixFromProfile_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBusMix* Mix;
			bool bActivate;
			int32 ProfileIndex;
			TArray<FSoundControlBusMixStage> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static void NewProp_bActivate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bActivate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ProfileIndex;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventLoadMixFromProfile_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventLoadMixFromProfile_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_bActivate_SetBit(void* Obj)
	{
		((AudioModulationStatics_eventLoadMixFromProfile_Parms*)Obj)->bActivate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_bActivate = { "bActivate", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AudioModulationStatics_eventLoadMixFromProfile_Parms), &Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_bActivate_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ProfileIndex = { "ProfileIndex", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventLoadMixFromProfile_Parms, ProfileIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundControlBusMixStage, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "DisplayName", "Stages" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventLoadMixFromProfile_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_Mix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_bActivate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ProfileIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "2" },
		{ "Category", "Audio" },
		{ "Comment", "/** Loads control bus mix from a profile into UObject mix definition, deserialized from an ini file.\n\x09 * @param BusMix - Mix object to deserialize profile .ini to.\n\x09 * @param bActivate - If true, activate mix upon loading from profile.\n\x09 * @param ProfileIndex - Index of profile, allowing multiple profiles to be loaded to single mix object. If <= 0, loads from default profile (no suffix).\n\x09 * @return Stages - Stage values loaded from profile (empty if profile did not exist or had no values serialized).\n\x09 */" },
		{ "CPP_Default_bActivate", "true" },
		{ "CPP_Default_ProfileIndex", "0" },
		{ "DisplayName", "Load Control Bus Mix From Profile" },
		{ "Keywords", "load deserialize control bus modulation mix modulator ini" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Loads control bus mix from a profile into UObject mix definition, deserialized from an ini file.\n@param BusMix - Mix object to deserialize profile .ini to.\n@param bActivate - If true, activate mix upon loading from profile.\n@param ProfileIndex - Index of profile, allowing multiple profiles to be loaded to single mix object. If <= 0, loads from default profile (no suffix).\n@return Stages - Stage values loaded from profile (empty if profile did not exist or had no values serialized)." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "LoadMixFromProfile", nullptr, nullptr, sizeof(AudioModulationStatics_eventLoadMixFromProfile_Parms), Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics
	{
		struct AudioModulationStatics_eventSaveMixToProfile_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBusMix* Mix;
			int32 ProfileIndex;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ProfileIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventSaveMixToProfile_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventSaveMixToProfile_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_ProfileIndex = { "ProfileIndex", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventSaveMixToProfile_Parms, ProfileIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_Mix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::NewProp_ProfileIndex,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "2" },
		{ "Category", "Audio" },
		{ "Comment", "/** Saves control bus mix to a profile, serialized to an ini file.  If mix is loaded, uses current proxy's state.\n\x09 * If not, uses default UObject representation.\n\x09 * @param BusMix - Mix object to serialize to profile .ini.\n\x09 * @param ProfileIndex - Index of profile, allowing multiple profiles can be saved for single mix object. If 0, saves to default ini profile (no suffix).\n\x09 */" },
		{ "CPP_Default_ProfileIndex", "0" },
		{ "DisplayName", "Save Control Bus Mix to Profile" },
		{ "Keywords", "save serialize bus control modulation mix modulator ini" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Saves control bus mix to a profile, serialized to an ini file.  If mix is loaded, uses current proxy's state.\nIf not, uses default UObject representation.\n@param BusMix - Mix object to serialize to profile .ini.\n@param ProfileIndex - Index of profile, allowing multiple profiles can be saved for single mix object. If 0, saves to default ini profile (no suffix)." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "SaveMixToProfile", nullptr, nullptr, sizeof(AudioModulationStatics_eventSaveMixToProfile_Parms), Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics
	{
		struct AudioModulationStatics_eventUpdateMix_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBusMix* Mix;
			TArray<FSoundControlBusMixStage> Stages;
			float FadeTime;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Stages_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Stages;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FadeTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMix_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMix_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_Stages_Inner = { "Stages", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundControlBusMixStage, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_Stages = { "Stages", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMix_Parms, Stages), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_FadeTime = { "FadeTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMix_Parms, FadeTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_Mix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_Stages_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_Stages,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::NewProp_FadeTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Sets a Control Bus Mix with the provided channel data, if the channels\n\x09 *  are provided in an active instance proxy of the mix. \n\x09 *  Does not update UObject definition of the mix. \n\x09 * @param Mix - Mix to update\n\x09 * @param Stages - Stages to set.  If stage's bus is not referenced by mix, stage's update request is ignored.\n\x09 * @param InFadeTime - Fade time to user when interpolating between current value and new values.\n\x09 *\x09\x09\x09\x09\x09 If negative, falls back to last fade time set on stage. If fade time never set on stage,\n\x09 *\x09\x09\x09\x09\x09 uses attack time set on stage in mix asset.\n\x09 */" },
		{ "CPP_Default_FadeTime", "-1.000000" },
		{ "DisplayName", "Set Control Bus Mix" },
		{ "Keywords", "set bus control modulation modulator mix stage" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Sets a Control Bus Mix with the provided channel data, if the channels\nare provided in an active instance proxy of the mix.\nDoes not update UObject definition of the mix.\n@param Mix - Mix to update\n@param Stages - Stages to set.  If stage's bus is not referenced by mix, stage's update request is ignored.\n@param InFadeTime - Fade time to user when interpolating between current value and new values.\n                                     If negative, falls back to last fade time set on stage. If fade time never set on stage,\n                                     uses attack time set on stage in mix asset." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "UpdateMix", nullptr, nullptr, sizeof(AudioModulationStatics_eventUpdateMix_Parms), Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_UpdateMix()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_UpdateMix_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics
	{
		struct AudioModulationStatics_eventUpdateMixByFilter_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBusMix* Mix;
			FString AddressFilter;
			TSubclassOf<USoundModulationParameter>  ParamClassFilter;
			USoundModulationParameter* ParamFilter;
			float Value;
			float FadeTime;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AddressFilter;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ParamClassFilter;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParamFilter;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FadeTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixByFilter_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixByFilter_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_AddressFilter = { "AddressFilter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixByFilter_Parms, AddressFilter), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_ParamClassFilter = { "ParamClassFilter", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixByFilter_Parms, ParamClassFilter), Z_Construct_UClass_USoundModulationParameter_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_ParamFilter = { "ParamFilter", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixByFilter_Parms, ParamFilter), Z_Construct_UClass_USoundModulationParameter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixByFilter_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_FadeTime = { "FadeTime", nullptr, (EPropertyFlags)0x0010040000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixByFilter_Parms, FadeTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_Mix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_AddressFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_ParamClassFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_ParamFilter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::NewProp_FadeTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::Function_MetaDataParams[] = {
		{ "AdvancedDisplay", "6" },
		{ "Category", "Audio" },
		{ "Comment", "/** Sets filtered stages of a given class to a provided target value for active instance of mix.\n\x09 * Does not update UObject definition of mix.\n\x09 * @param Mix - Mix to modify\n\x09 * @param AddressFilter - (Optional) Address filter to apply to provided mix's stages.\n\x09 * @param ParamClassFilter - (Optional) Filters buses by parameter class.\n\x09 * @param ParamFilter - (Optional) Filters buses by parameter.\n\x09 * @param Value - Target value to mix filtered stages to.\n\x09 * @param FadeTime - If non-negative, updates the fade time for the resulting bus stages found matching the provided filter.\n\x09 */" },
		{ "CPP_Default_FadeTime", "-1.000000" },
		{ "CPP_Default_Value", "1.000000" },
		{ "DisplayName", "Set Control Bus Mix By Filter" },
		{ "Keywords", "set bus control class modulation modulator mix stage value filter" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Sets filtered stages of a given class to a provided target value for active instance of mix.\nDoes not update UObject definition of mix.\n@param Mix - Mix to modify\n@param AddressFilter - (Optional) Address filter to apply to provided mix's stages.\n@param ParamClassFilter - (Optional) Filters buses by parameter class.\n@param ParamFilter - (Optional) Filters buses by parameter.\n@param Value - Target value to mix filtered stages to.\n@param FadeTime - If non-negative, updates the fade time for the resulting bus stages found matching the provided filter." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "UpdateMixByFilter", nullptr, nullptr, sizeof(AudioModulationStatics_eventUpdateMixByFilter_Parms), Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics
	{
		struct AudioModulationStatics_eventUpdateMixFromObject_Parms
		{
			const UObject* WorldContextObject;
			USoundControlBusMix* Mix;
			float FadeTime;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Mix;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FadeTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixFromObject_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_Mix = { "Mix", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixFromObject_Parms, Mix), Z_Construct_UClass_USoundControlBusMix_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_FadeTime = { "FadeTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateMixFromObject_Parms, FadeTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_Mix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::NewProp_FadeTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Commits updates from a UObject definition of a bus mix to active instance in audio thread\n\x09 * (ignored if mix has not been activated).\n\x09 * @param Mix - Mix to update\n\x09 * @param FadeTime - Fade time to user when interpolating between current value and new values.\n\x09 *\x09\x09\x09\x09\x09 If negative, falls back to last fade time set on stage. If fade time never set on stage,\n\x09 *\x09\x09\x09\x09\x09 uses attack time set on stage in mix asset.\n\x09 */" },
		{ "CPP_Default_FadeTime", "-1.000000" },
		{ "DisplayName", "Update Control Bus Mix" },
		{ "Keywords", "update set control bus mix modulation modulator" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Commits updates from a UObject definition of a bus mix to active instance in audio thread\n(ignored if mix has not been activated).\n@param Mix - Mix to update\n@param FadeTime - Fade time to user when interpolating between current value and new values.\n                                     If negative, falls back to last fade time set on stage. If fade time never set on stage,\n                                     uses attack time set on stage in mix asset." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "UpdateMixFromObject", nullptr, nullptr, sizeof(AudioModulationStatics_eventUpdateMixFromObject_Parms), Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics
	{
		struct AudioModulationStatics_eventUpdateModulator_Parms
		{
			const UObject* WorldContextObject;
			USoundModulatorBase* Modulator;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modulator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateModulator_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::NewProp_Modulator = { "Modulator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AudioModulationStatics_eventUpdateModulator_Parms, Modulator), Z_Construct_UClass_USoundModulatorBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::NewProp_Modulator,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::Function_MetaDataParams[] = {
		{ "Category", "Audio" },
		{ "Comment", "/** Commits updates from a UObject definition of a modulator (e.g. Bus, Bus Mix, LFO)\n\x09 *  to active instance in audio thread (ignored if modulator type has not been activated).\n\x09 * @param Modulator - Modulator to update\n\x09 */" },
		{ "DisplayName", "Update Modulator" },
		{ "Keywords", "update set control bus mix modulation modulator" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "Commits updates from a UObject definition of a modulator (e.g. Bus, Bus Mix, LFO)\nto active instance in audio thread (ignored if modulator type has not been activated).\n@param Modulator - Modulator to update" },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UAudioModulationStatics, nullptr, "UpdateModulator", nullptr, nullptr, sizeof(AudioModulationStatics_eventUpdateModulator_Parms), Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UAudioModulationStatics_NoRegister()
	{
		return UAudioModulationStatics::StaticClass();
	}
	struct Z_Construct_UClass_UAudioModulationStatics_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAudioModulationStatics_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UAudioModulationStatics_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UAudioModulationStatics_ActivateBus, "ActivateBus" }, // 3097195185
		{ &Z_Construct_UFunction_UAudioModulationStatics_ActivateBusMix, "ActivateBusMix" }, // 2798676428
		{ &Z_Construct_UFunction_UAudioModulationStatics_ActivateGenerator, "ActivateGenerator" }, // 2814951706
		{ &Z_Construct_UFunction_UAudioModulationStatics_CreateBus, "CreateBus" }, // 259271921
		{ &Z_Construct_UFunction_UAudioModulationStatics_CreateBusMix, "CreateBusMix" }, // 4278043943
		{ &Z_Construct_UFunction_UAudioModulationStatics_CreateBusMixStage, "CreateBusMixStage" }, // 3602752642
		{ &Z_Construct_UFunction_UAudioModulationStatics_DeactivateBus, "DeactivateBus" }, // 954054892
		{ &Z_Construct_UFunction_UAudioModulationStatics_DeactivateBusMix, "DeactivateBusMix" }, // 2798033017
		{ &Z_Construct_UFunction_UAudioModulationStatics_DeactivateGenerator, "DeactivateGenerator" }, // 2482907165
		{ &Z_Construct_UFunction_UAudioModulationStatics_LoadMixFromProfile, "LoadMixFromProfile" }, // 3754293150
		{ &Z_Construct_UFunction_UAudioModulationStatics_SaveMixToProfile, "SaveMixToProfile" }, // 658332458
		{ &Z_Construct_UFunction_UAudioModulationStatics_UpdateMix, "UpdateMix" }, // 2594441488
		{ &Z_Construct_UFunction_UAudioModulationStatics_UpdateMixByFilter, "UpdateMixByFilter" }, // 3840449430
		{ &Z_Construct_UFunction_UAudioModulationStatics_UpdateMixFromObject, "UpdateMixFromObject" }, // 3146762821
		{ &Z_Construct_UFunction_UAudioModulationStatics_UpdateModulator, "UpdateModulator" }, // 731951854
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAudioModulationStatics_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// namespace AudioModulation\n" },
		{ "IncludePath", "AudioModulationStatics.h" },
		{ "ModuleRelativePath", "Public/AudioModulationStatics.h" },
		{ "ToolTip", "namespace AudioModulation" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAudioModulationStatics_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAudioModulationStatics>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAudioModulationStatics_Statics::ClassParams = {
		&UAudioModulationStatics::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAudioModulationStatics_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAudioModulationStatics_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAudioModulationStatics()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAudioModulationStatics_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAudioModulationStatics, 4042623511);
	template<> AUDIOMODULATION_API UClass* StaticClass<UAudioModulationStatics>()
	{
		return UAudioModulationStatics::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAudioModulationStatics(Z_Construct_UClass_UAudioModulationStatics, &UAudioModulationStatics::StaticClass, TEXT("/Script/AudioModulation"), TEXT("UAudioModulationStatics"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAudioModulationStatics);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
