// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATIONEDITOR_ModulationPatchCurveEditorViewStacked_generated_h
#error "ModulationPatchCurveEditorViewStacked.generated.h already included, missing '#pragma once' in ModulationPatchCurveEditorViewStacked.h"
#endif
#define AUDIOMODULATIONEDITOR_ModulationPatchCurveEditorViewStacked_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Editors_ModulationPatchCurveEditorViewStacked_h


#define FOREACH_ENUM_EMODPATCHOUTPUTEDITORCURVESOURCE(op) \
	op(EModPatchOutputEditorCurveSource::Custom) \
	op(EModPatchOutputEditorCurveSource::Expression) \
	op(EModPatchOutputEditorCurveSource::Shared) \
	op(EModPatchOutputEditorCurveSource::Unset) 

enum class EModPatchOutputEditorCurveSource : uint8;
template<> AUDIOMODULATIONEDITOR_API UEnum* StaticEnum<EModPatchOutputEditorCurveSource>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
