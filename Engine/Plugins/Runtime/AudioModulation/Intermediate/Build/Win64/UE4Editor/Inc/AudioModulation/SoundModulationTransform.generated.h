// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATION_SoundModulationTransform_generated_h
#error "SoundModulationTransform.generated.h already included, missing '#pragma once' in SoundModulationTransform.h"
#endif
#define AUDIOMODULATION_SoundModulationTransform_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationTransform_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSoundModulationTransform_Statics; \
	static class UScriptStruct* StaticStruct();


template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<struct FSoundModulationTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationTransform_h


#define FOREACH_ENUM_ESOUNDMODULATORCURVE(op) \
	op(ESoundModulatorCurve::Linear) \
	op(ESoundModulatorCurve::Exp) \
	op(ESoundModulatorCurve::Exp_Inverse) \
	op(ESoundModulatorCurve::Log) \
	op(ESoundModulatorCurve::Sin) \
	op(ESoundModulatorCurve::SCurve) \
	op(ESoundModulatorCurve::Shared) \
	op(ESoundModulatorCurve::Custom) \
	op(ESoundModulatorCurve::Count) 

enum class ESoundModulatorCurve : uint8;
template<> AUDIOMODULATION_API UEnum* StaticEnum<ESoundModulatorCurve>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
