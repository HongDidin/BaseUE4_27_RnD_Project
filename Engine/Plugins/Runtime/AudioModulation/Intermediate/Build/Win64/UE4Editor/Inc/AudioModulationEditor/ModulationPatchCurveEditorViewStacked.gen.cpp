// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulationEditor/Private/Editors/ModulationPatchCurveEditorViewStacked.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeModulationPatchCurveEditorViewStacked() {}
// Cross Module References
	AUDIOMODULATIONEDITOR_API UEnum* Z_Construct_UEnum_AudioModulationEditor_EModPatchOutputEditorCurveSource();
	UPackage* Z_Construct_UPackage__Script_AudioModulationEditor();
// End Cross Module References
	static UEnum* EModPatchOutputEditorCurveSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AudioModulationEditor_EModPatchOutputEditorCurveSource, Z_Construct_UPackage__Script_AudioModulationEditor(), TEXT("EModPatchOutputEditorCurveSource"));
		}
		return Singleton;
	}
	template<> AUDIOMODULATIONEDITOR_API UEnum* StaticEnum<EModPatchOutputEditorCurveSource>()
	{
		return EModPatchOutputEditorCurveSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EModPatchOutputEditorCurveSource(EModPatchOutputEditorCurveSource_StaticEnum, TEXT("/Script/AudioModulationEditor"), TEXT("EModPatchOutputEditorCurveSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AudioModulationEditor_EModPatchOutputEditorCurveSource_Hash() { return 402081028U; }
	UEnum* Z_Construct_UEnum_AudioModulationEditor_EModPatchOutputEditorCurveSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulationEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EModPatchOutputEditorCurveSource"), 0, Get_Z_Construct_UEnum_AudioModulationEditor_EModPatchOutputEditorCurveSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EModPatchOutputEditorCurveSource::Custom", (int64)EModPatchOutputEditorCurveSource::Custom },
				{ "EModPatchOutputEditorCurveSource::Expression", (int64)EModPatchOutputEditorCurveSource::Expression },
				{ "EModPatchOutputEditorCurveSource::Shared", (int64)EModPatchOutputEditorCurveSource::Shared },
				{ "EModPatchOutputEditorCurveSource::Unset", (int64)EModPatchOutputEditorCurveSource::Unset },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Custom.Name", "EModPatchOutputEditorCurveSource::Custom" },
				{ "Expression.Name", "EModPatchOutputEditorCurveSource::Expression" },
				{ "ModuleRelativePath", "Private/Editors/ModulationPatchCurveEditorViewStacked.h" },
				{ "Shared.Name", "EModPatchOutputEditorCurveSource::Shared" },
				{ "Unset.Name", "EModPatchOutputEditorCurveSource::Unset" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AudioModulationEditor,
				nullptr,
				"EModPatchOutputEditorCurveSource",
				"EModPatchOutputEditorCurveSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
