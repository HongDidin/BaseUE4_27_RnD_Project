// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/SoundModulationParameter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationParameter() {}
// Cross Module References
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationParameterSettings();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameter_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameter();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterScaled_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterScaled();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterFrequencyBase_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterFrequencyBase();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterFrequency_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterFrequency();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterFilterFrequency_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterFilterFrequency();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterLPFFrequency_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterLPFFrequency();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterHPFFrequency_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterHPFFrequency();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterBipolar_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterBipolar();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterVolume_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameterVolume();
// End Cross Module References
class UScriptStruct* FSoundModulationParameterSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AUDIOMODULATION_API uint32 Get_Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundModulationParameterSettings, Z_Construct_UPackage__Script_AudioModulation(), TEXT("SoundModulationParameterSettings"), sizeof(FSoundModulationParameterSettings), Get_Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Hash());
	}
	return Singleton;
}
template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<FSoundModulationParameterSettings>()
{
	return FSoundModulationParameterSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundModulationParameterSettings(FSoundModulationParameterSettings::StaticStruct, TEXT("/Script/AudioModulation"), TEXT("SoundModulationParameterSettings"), false, nullptr, nullptr);
static struct FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationParameterSettings
{
	FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationParameterSettings()
	{
		UScriptStruct::DeferCppStructOps<FSoundModulationParameterSettings>(FName(TEXT("SoundModulationParameterSettings")));
	}
} ScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationParameterSettings;
	struct Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueNormalized_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ValueNormalized;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnitDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_UnitDisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueUnit_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ValueUnit;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundModulationParameterSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueNormalized_MetaData[] = {
		{ "Category", "General" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** \n\x09  * Normalized, unitless default value of modulator. To ensure bypass functionality of mixing, patching, and modulating \n\x09  * functions as anticipated, value should be selected such that GetMixFunction (see USoundModulationParameter)\n\x09  * reduces to an identity function (i.e. function acts as a \"pass-through\" for all values in the range [0.0, 1.0]).\n\x09  * If GetMixFunction performs the mathematical operation f(x1, x2), then the default ValueNormalized should result in\n\x09  * f(x1, d) = x1 where d is ValueNormalized.\n\x09  */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Normalized, unitless default value of modulator. To ensure bypass functionality of mixing, patching, and modulating\nfunctions as anticipated, value should be selected such that GetMixFunction (see USoundModulationParameter)\nreduces to an identity function (i.e. function acts as a \"pass-through\" for all values in the range [0.0, 1.0]).\nIf GetMixFunction performs the mathematical operation f(x1, x2), then the default ValueNormalized should result in\nf(x1, d) = x1 where d is ValueNormalized." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueNormalized = { "ValueNormalized", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationParameterSettings, ValueNormalized), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueNormalized_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueNormalized_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_UnitDisplayName_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** (Optional) Text name of parameter's unit */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "(Optional) Text name of parameter's unit" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_UnitDisplayName = { "UnitDisplayName", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationParameterSettings, UnitDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_UnitDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_UnitDisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueUnit_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Default value of modulator in units (editor only) */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Default value of modulator in units (editor only)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueUnit = { "ValueUnit", nullptr, (EPropertyFlags)0x0010000800002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationParameterSettings, ValueUnit), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueUnit_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueUnit_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueNormalized,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_UnitDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::NewProp_ValueUnit,
#endif // WITH_EDITORONLY_DATA
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
		nullptr,
		&NewStructOps,
		"SoundModulationParameterSettings",
		sizeof(FSoundModulationParameterSettings),
		alignof(FSoundModulationParameterSettings),
		Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationParameterSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundModulationParameterSettings"), sizeof(FSoundModulationParameterSettings), Get_Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundModulationParameterSettings_Hash() { return 31049698U; }
	void USoundModulationParameter::StaticRegisterNativesUSoundModulationParameter()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameter_NoRegister()
	{
		return USoundModulationParameter::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameter_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "General" },
		{ "DisplayName", "Parameter" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundModulationParameter_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationParameter, Settings), Z_Construct_UScriptStruct_FSoundModulationParameterSettings, METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameter_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameter_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationParameter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationParameter_Statics::NewProp_Settings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameter_Statics::ClassParams = {
		&USoundModulationParameter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationParameter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameter_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameter, 459809674);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameter>()
	{
		return USoundModulationParameter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameter(Z_Construct_UClass_USoundModulationParameter, &USoundModulationParameter::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameter);
	void USoundModulationParameterScaled::StaticRegisterNativesUSoundModulationParameterScaled()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterScaled_NoRegister()
	{
		return USoundModulationParameterScaled::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterScaled_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnitMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UnitMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnitMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UnitMax;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterScaled_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameter,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterScaled_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Linearly scaled value between unit minimum and maximum.\n" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Linearly scaled value between unit minimum and maximum." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMin_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Unit minimum of modulator. Minimum is only enforced at modulation destination. */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Unit minimum of modulator. Minimum is only enforced at modulation destination." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMin = { "UnitMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationParameterScaled, UnitMin), METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMax_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Unit maximum of modulator. Maximum is only enforced at modulation destination. */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Unit maximum of modulator. Maximum is only enforced at modulation destination." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMax = { "UnitMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationParameterScaled, UnitMax), METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMax_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationParameterScaled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationParameterScaled_Statics::NewProp_UnitMax,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterScaled_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterScaled>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterScaled_Statics::ClassParams = {
		&USoundModulationParameterScaled::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationParameterScaled_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterScaled_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterScaled_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterScaled_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterScaled()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterScaled_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterScaled, 2248286381);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterScaled>()
	{
		return USoundModulationParameterScaled::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterScaled(Z_Construct_UClass_USoundModulationParameterScaled, &USoundModulationParameterScaled::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterScaled"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterScaled);
	void USoundModulationParameterFrequencyBase::StaticRegisterNativesUSoundModulationParameterFrequencyBase()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterFrequencyBase_NoRegister()
	{
		return USoundModulationParameterFrequencyBase::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameter,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space.\n" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterFrequencyBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics::ClassParams = {
		&USoundModulationParameterFrequencyBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A1u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterFrequencyBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterFrequencyBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterFrequencyBase, 428356093);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterFrequencyBase>()
	{
		return USoundModulationParameterFrequencyBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterFrequencyBase(Z_Construct_UClass_USoundModulationParameterFrequencyBase, &USoundModulationParameterFrequencyBase::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterFrequencyBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterFrequencyBase);
	void USoundModulationParameterFrequency::StaticRegisterNativesUSoundModulationParameterFrequency()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterFrequency_NoRegister()
	{
		return USoundModulationParameterFrequency::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterFrequency_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnitMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UnitMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnitMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UnitMax;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterFrequency_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameterFrequencyBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterFrequency_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with provided minimum and maximum.\n" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with provided minimum and maximum." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMin_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Unit minimum of modulator. Minimum is only enforced at modulation destination. */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Unit minimum of modulator. Minimum is only enforced at modulation destination." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMin = { "UnitMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationParameterFrequency, UnitMin), METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMax_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Unit maximum of modulator. Maximum is only enforced at modulation destination. */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Unit maximum of modulator. Maximum is only enforced at modulation destination." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMax = { "UnitMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationParameterFrequency, UnitMax), METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMax_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationParameterFrequency_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationParameterFrequency_Statics::NewProp_UnitMax,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterFrequency_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterFrequency>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterFrequency_Statics::ClassParams = {
		&USoundModulationParameterFrequency::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationParameterFrequency_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterFrequency_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterFrequency_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterFrequency_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterFrequency()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterFrequency_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterFrequency, 2408234155);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterFrequency>()
	{
		return USoundModulationParameterFrequency::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterFrequency(Z_Construct_UClass_USoundModulationParameterFrequency, &USoundModulationParameterFrequency::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterFrequency"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterFrequency);
	void USoundModulationParameterFilterFrequency::StaticRegisterNativesUSoundModulationParameterFilterFrequency()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterFilterFrequency_NoRegister()
	{
		return USoundModulationParameterFilterFrequency::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameterFrequencyBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with standard filter min and max frequency set.\n" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with standard filter min and max frequency set." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterFilterFrequency>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics::ClassParams = {
		&USoundModulationParameterFilterFrequency::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterFilterFrequency()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterFilterFrequency_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterFilterFrequency, 2628597536);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterFilterFrequency>()
	{
		return USoundModulationParameterFilterFrequency::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterFilterFrequency(Z_Construct_UClass_USoundModulationParameterFilterFrequency, &USoundModulationParameterFilterFrequency::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterFilterFrequency"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterFilterFrequency);
	void USoundModulationParameterLPFFrequency::StaticRegisterNativesUSoundModulationParameterLPFFrequency()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterLPFFrequency_NoRegister()
	{
		return USoundModulationParameterLPFFrequency::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameterFilterFrequency,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with standard filter min and max frequency set.\n// Mixes by taking the minimum (i.e. aggressive) filter frequency of all active modulators.\n" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with standard filter min and max frequency set.\nMixes by taking the minimum (i.e. aggressive) filter frequency of all active modulators." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterLPFFrequency>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics::ClassParams = {
		&USoundModulationParameterLPFFrequency::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterLPFFrequency()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterLPFFrequency_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterLPFFrequency, 2659988106);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterLPFFrequency>()
	{
		return USoundModulationParameterLPFFrequency::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterLPFFrequency(Z_Construct_UClass_USoundModulationParameterLPFFrequency, &USoundModulationParameterLPFFrequency::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterLPFFrequency"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterLPFFrequency);
	void USoundModulationParameterHPFFrequency::StaticRegisterNativesUSoundModulationParameterHPFFrequency()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterHPFFrequency_NoRegister()
	{
		return USoundModulationParameterHPFFrequency::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameterFilterFrequency,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with standard filter min and max frequency set.\n// Mixes by taking the maximum (i.e. aggressive) filter frequency of all active modulators.\n" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Modulation Parameter that scales normalized, unitless value to logarithmic frequency unit space with standard filter min and max frequency set.\nMixes by taking the maximum (i.e. aggressive) filter frequency of all active modulators." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterHPFFrequency>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics::ClassParams = {
		&USoundModulationParameterHPFFrequency::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterHPFFrequency()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterHPFFrequency_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterHPFFrequency, 183750316);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterHPFFrequency>()
	{
		return USoundModulationParameterHPFFrequency::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterHPFFrequency(Z_Construct_UClass_USoundModulationParameterHPFFrequency, &USoundModulationParameterHPFFrequency::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterHPFFrequency"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterHPFFrequency);
	void USoundModulationParameterBipolar::StaticRegisterNativesUSoundModulationParameterBipolar()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterBipolar_NoRegister()
	{
		return USoundModulationParameterBipolar::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterBipolar_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnitRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UnitRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterBipolar_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameter,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterBipolar_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Modulation Parameter that scales normalized, unitless value to bipolar range. Mixes multiplicatively.\n" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Modulation Parameter that scales normalized, unitless value to bipolar range. Mixes multiplicatively." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterBipolar_Statics::NewProp_UnitRange_MetaData[] = {
		{ "Category", "General" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** Unit range of modulator. Range is only enforced at modulation destination. */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Unit range of modulator. Range is only enforced at modulation destination." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USoundModulationParameterBipolar_Statics::NewProp_UnitRange = { "UnitRange", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationParameterBipolar, UnitRange), METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterBipolar_Statics::NewProp_UnitRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterBipolar_Statics::NewProp_UnitRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationParameterBipolar_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationParameterBipolar_Statics::NewProp_UnitRange,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterBipolar_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterBipolar>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterBipolar_Statics::ClassParams = {
		&USoundModulationParameterBipolar::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationParameterBipolar_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterBipolar_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterBipolar_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterBipolar_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterBipolar()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterBipolar_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterBipolar, 2331667349);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterBipolar>()
	{
		return USoundModulationParameterBipolar::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterBipolar(Z_Construct_UClass_USoundModulationParameterBipolar, &USoundModulationParameterBipolar::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterBipolar"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterBipolar);
	void USoundModulationParameterVolume::StaticRegisterNativesUSoundModulationParameterVolume()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterVolume_NoRegister()
	{
		return USoundModulationParameterVolume::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterVolume_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MinVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MinVolume;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterVolume_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationParameter,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterVolume_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SoundModulationParameter.h" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterVolume_Statics::NewProp_MinVolume_MetaData[] = {
		{ "Category", "General" },
		{ "ClampMax", "0.000000" },
		{ "Comment", "/** Minimum volume of parameter. Only enforced at modulation destination. */" },
		{ "ModuleRelativePath", "Public/SoundModulationParameter.h" },
		{ "ToolTip", "Minimum volume of parameter. Only enforced at modulation destination." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_USoundModulationParameterVolume_Statics::NewProp_MinVolume = { "MinVolume", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationParameterVolume, MinVolume), METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterVolume_Statics::NewProp_MinVolume_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterVolume_Statics::NewProp_MinVolume_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationParameterVolume_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationParameterVolume_Statics::NewProp_MinVolume,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterVolume_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterVolume>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterVolume_Statics::ClassParams = {
		&USoundModulationParameterVolume::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationParameterVolume_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterVolume_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterVolume_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterVolume_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterVolume()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterVolume_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterVolume, 3732312574);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationParameterVolume>()
	{
		return USoundModulationParameterVolume::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterVolume(Z_Construct_UClass_USoundModulationParameterVolume, &USoundModulationParameterVolume::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationParameterVolume"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterVolume);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
