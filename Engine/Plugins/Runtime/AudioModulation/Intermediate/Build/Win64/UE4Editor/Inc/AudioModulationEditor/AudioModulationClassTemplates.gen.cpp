// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulationEditor/Public/AudioModulationClassTemplates.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAudioModulationClassTemplates() {}
// Cross Module References
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationClassTemplate_NoRegister();
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationClassTemplate();
	GAMEPROJECTGENERATION_API UClass* Z_Construct_UClass_UPluginClassTemplate();
	UPackage* Z_Construct_UPackage__Script_AudioModulationEditor();
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationGeneratorClassTemplate_NoRegister();
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationGeneratorClassTemplate();
// End Cross Module References
	void USoundModulationClassTemplate::StaticRegisterNativesUSoundModulationClassTemplate()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationClassTemplate_NoRegister()
	{
		return USoundModulationClassTemplate::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationClassTemplate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationClassTemplate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPluginClassTemplate,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationClassTemplate_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AudioModulationClassTemplates.h" },
		{ "ModuleRelativePath", "Public/AudioModulationClassTemplates.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationClassTemplate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationClassTemplate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationClassTemplate_Statics::ClassParams = {
		&USoundModulationClassTemplate::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationClassTemplate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationClassTemplate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationClassTemplate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationClassTemplate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationClassTemplate, 1807709193);
	template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<USoundModulationClassTemplate>()
	{
		return USoundModulationClassTemplate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationClassTemplate(Z_Construct_UClass_USoundModulationClassTemplate, &USoundModulationClassTemplate::StaticClass, TEXT("/Script/AudioModulationEditor"), TEXT("USoundModulationClassTemplate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationClassTemplate);
	void USoundModulationGeneratorClassTemplate::StaticRegisterNativesUSoundModulationGeneratorClassTemplate()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationGeneratorClassTemplate_NoRegister()
	{
		return USoundModulationGeneratorClassTemplate::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationClassTemplate,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AudioModulationClassTemplates.h" },
		{ "ModuleRelativePath", "Public/AudioModulationClassTemplates.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationGeneratorClassTemplate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics::ClassParams = {
		&USoundModulationGeneratorClassTemplate::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationGeneratorClassTemplate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationGeneratorClassTemplate, 842684429);
	template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<USoundModulationGeneratorClassTemplate>()
	{
		return USoundModulationGeneratorClassTemplate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationGeneratorClassTemplate(Z_Construct_UClass_USoundModulationGeneratorClassTemplate, &USoundModulationGeneratorClassTemplate::StaticClass, TEXT("/Script/AudioModulationEditor"), TEXT("USoundModulationGeneratorClassTemplate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationGeneratorClassTemplate);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
