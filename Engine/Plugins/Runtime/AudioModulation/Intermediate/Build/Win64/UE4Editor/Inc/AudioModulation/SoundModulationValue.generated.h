// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATION_SoundModulationValue_generated_h
#error "SoundModulationValue.generated.h already included, missing '#pragma once' in SoundModulationValue.h"
#endif
#define AUDIOMODULATION_SoundModulationValue_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationValue_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics; \
	static class UScriptStruct* StaticStruct();


template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<struct FSoundModulationMixValue>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationValue_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
