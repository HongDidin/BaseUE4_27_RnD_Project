// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/SoundControlBus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundControlBus() {}
// Cross Module References
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundControlBus_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundControlBus();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USoundModulatorBase();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGenerator_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameter_NoRegister();
// End Cross Module References
	void USoundControlBus::StaticRegisterNativesUSoundControlBus()
	{
	}
	UClass* Z_Construct_UClass_USoundControlBus_NoRegister()
	{
		return USoundControlBus::StaticClass();
	}
	struct Z_Construct_UClass_USoundControlBus_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBypass_MetaData[];
#endif
		static void NewProp_bBypass_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBypass;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideAddress_MetaData[];
#endif
		static void NewProp_bOverrideAddress_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideAddress;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Address_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Address;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Generators_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Generators_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Generators;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Parameter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundControlBus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulatorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundControlBus_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "SoundControlBus.h" },
		{ "ModuleRelativePath", "Public/SoundControlBus.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundControlBus_Statics::NewProp_bBypass_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** If true, bypasses control bus from being modulated by parameters, patches, or mixed (control bus remains active and computed). */" },
		{ "ModuleRelativePath", "Public/SoundControlBus.h" },
		{ "ToolTip", "If true, bypasses control bus from being modulated by parameters, patches, or mixed (control bus remains active and computed)." },
	};
#endif
	void Z_Construct_UClass_USoundControlBus_Statics::NewProp_bBypass_SetBit(void* Obj)
	{
		((USoundControlBus*)Obj)->bBypass = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USoundControlBus_Statics::NewProp_bBypass = { "bBypass", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USoundControlBus), &Z_Construct_UClass_USoundControlBus_Statics::NewProp_bBypass_SetBit, METADATA_PARAMS(Z_Construct_UClass_USoundControlBus_Statics::NewProp_bBypass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBus_Statics::NewProp_bBypass_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundControlBus_Statics::NewProp_bOverrideAddress_MetaData[] = {
		{ "Category", "Mix" },
		{ "Comment", "/** If true, Address field is used in place of object name for address used when applying mix changes using filtering. */" },
		{ "ModuleRelativePath", "Public/SoundControlBus.h" },
		{ "ToolTip", "If true, Address field is used in place of object name for address used when applying mix changes using filtering." },
	};
#endif
	void Z_Construct_UClass_USoundControlBus_Statics::NewProp_bOverrideAddress_SetBit(void* Obj)
	{
		((USoundControlBus*)Obj)->bOverrideAddress = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USoundControlBus_Statics::NewProp_bOverrideAddress = { "bOverrideAddress", nullptr, (EPropertyFlags)0x0010000800000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USoundControlBus), &Z_Construct_UClass_USoundControlBus_Statics::NewProp_bOverrideAddress_SetBit, METADATA_PARAMS(Z_Construct_UClass_USoundControlBus_Statics::NewProp_bOverrideAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBus_Statics::NewProp_bOverrideAddress_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundControlBus_Statics::NewProp_Address_MetaData[] = {
		{ "Category", "Mix" },
		{ "Comment", "/** Address to use when applying mix changes. */" },
		{ "EditCondition", "bOverrideAddress" },
		{ "ModuleRelativePath", "Public/SoundControlBus.h" },
		{ "ToolTip", "Address to use when applying mix changes." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USoundControlBus_Statics::NewProp_Address = { "Address", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundControlBus, Address), METADATA_PARAMS(Z_Construct_UClass_USoundControlBus_Statics::NewProp_Address_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBus_Statics::NewProp_Address_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USoundControlBus_Statics::NewProp_Generators_Inner = { "Generators", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USoundModulationGenerator_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundControlBus_Statics::NewProp_Generators_MetaData[] = {
		{ "Category", "Generators" },
		{ "ModuleRelativePath", "Public/SoundControlBus.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USoundControlBus_Statics::NewProp_Generators = { "Generators", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundControlBus, Generators), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USoundControlBus_Statics::NewProp_Generators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBus_Statics::NewProp_Generators_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundControlBus_Statics::NewProp_Parameter_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Public/SoundControlBus.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USoundControlBus_Statics::NewProp_Parameter = { "Parameter", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundControlBus, Parameter), Z_Construct_UClass_USoundModulationParameter_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USoundControlBus_Statics::NewProp_Parameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBus_Statics::NewProp_Parameter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundControlBus_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundControlBus_Statics::NewProp_bBypass,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundControlBus_Statics::NewProp_bOverrideAddress,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundControlBus_Statics::NewProp_Address,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundControlBus_Statics::NewProp_Generators_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundControlBus_Statics::NewProp_Generators,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundControlBus_Statics::NewProp_Parameter,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundControlBus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundControlBus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundControlBus_Statics::ClassParams = {
		&USoundControlBus::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundControlBus_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBus_Statics::PropPointers),
		0,
		0x000810A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundControlBus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundControlBus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundControlBus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundControlBus, 1194697812);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundControlBus>()
	{
		return USoundControlBus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundControlBus(Z_Construct_UClass_USoundControlBus, &USoundControlBus::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundControlBus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundControlBus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
