// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
class USoundModulatorBase;
class USoundControlBusMix;
class USoundModulationParameter;
struct FSoundControlBusMixStage;
class USoundModulationGenerator;
class USoundControlBus;
#ifdef AUDIOMODULATION_AudioModulationStatics_generated_h
#error "AudioModulationStatics.generated.h already included, missing '#pragma once' in AudioModulationStatics.h"
#endif
#define AUDIOMODULATION_AudioModulationStatics_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateModulator); \
	DECLARE_FUNCTION(execUpdateMixFromObject); \
	DECLARE_FUNCTION(execUpdateMixByFilter); \
	DECLARE_FUNCTION(execUpdateMix); \
	DECLARE_FUNCTION(execLoadMixFromProfile); \
	DECLARE_FUNCTION(execSaveMixToProfile); \
	DECLARE_FUNCTION(execDeactivateGenerator); \
	DECLARE_FUNCTION(execDeactivateBusMix); \
	DECLARE_FUNCTION(execDeactivateBus); \
	DECLARE_FUNCTION(execCreateBusMix); \
	DECLARE_FUNCTION(execCreateBusMixStage); \
	DECLARE_FUNCTION(execCreateBus); \
	DECLARE_FUNCTION(execActivateGenerator); \
	DECLARE_FUNCTION(execActivateBusMix); \
	DECLARE_FUNCTION(execActivateBus);


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateModulator); \
	DECLARE_FUNCTION(execUpdateMixFromObject); \
	DECLARE_FUNCTION(execUpdateMixByFilter); \
	DECLARE_FUNCTION(execUpdateMix); \
	DECLARE_FUNCTION(execLoadMixFromProfile); \
	DECLARE_FUNCTION(execSaveMixToProfile); \
	DECLARE_FUNCTION(execDeactivateGenerator); \
	DECLARE_FUNCTION(execDeactivateBusMix); \
	DECLARE_FUNCTION(execDeactivateBus); \
	DECLARE_FUNCTION(execCreateBusMix); \
	DECLARE_FUNCTION(execCreateBusMixStage); \
	DECLARE_FUNCTION(execCreateBus); \
	DECLARE_FUNCTION(execActivateGenerator); \
	DECLARE_FUNCTION(execActivateBusMix); \
	DECLARE_FUNCTION(execActivateBus);


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAudioModulationStatics(); \
	friend struct Z_Construct_UClass_UAudioModulationStatics_Statics; \
public: \
	DECLARE_CLASS(UAudioModulationStatics, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(UAudioModulationStatics)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUAudioModulationStatics(); \
	friend struct Z_Construct_UClass_UAudioModulationStatics_Statics; \
public: \
	DECLARE_CLASS(UAudioModulationStatics, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(UAudioModulationStatics)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAudioModulationStatics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioModulationStatics) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAudioModulationStatics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioModulationStatics); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAudioModulationStatics(UAudioModulationStatics&&); \
	NO_API UAudioModulationStatics(const UAudioModulationStatics&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAudioModulationStatics(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAudioModulationStatics(UAudioModulationStatics&&); \
	NO_API UAudioModulationStatics(const UAudioModulationStatics&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAudioModulationStatics); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioModulationStatics); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioModulationStatics)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_22_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AudioModulationStatics."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATION_API UClass* StaticClass<class UAudioModulationStatics>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationStatics_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
