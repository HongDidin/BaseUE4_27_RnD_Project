// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATION_SoundModulationPatch_generated_h
#error "SoundModulationPatch.generated.h already included, missing '#pragma once' in SoundModulationPatch.h"
#endif
#define AUDIOMODULATION_SoundModulationPatch_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_43_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics; \
	static class UScriptStruct* StaticStruct();


template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<struct FSoundControlModulationPatch>();

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics; \
	static class UScriptStruct* StaticStruct();


template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<struct FSoundControlModulationInput>();

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationPatch(); \
	friend struct Z_Construct_UClass_USoundModulationPatch_Statics; \
public: \
	DECLARE_CLASS(USoundModulationPatch, USoundModulatorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationPatch)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationPatch(); \
	friend struct Z_Construct_UClass_USoundModulationPatch_Statics; \
public: \
	DECLARE_CLASS(USoundModulationPatch, USoundModulatorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationPatch)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationPatch(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationPatch) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationPatch); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationPatch); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationPatch(USoundModulationPatch&&); \
	NO_API USoundModulationPatch(const USoundModulationPatch&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationPatch(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationPatch(USoundModulationPatch&&); \
	NO_API USoundModulationPatch(const USoundModulationPatch&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationPatch); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationPatch); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationPatch)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_58_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h_61_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundModulationPatch."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATION_API UClass* StaticClass<class USoundModulationPatch>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationPatch_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
