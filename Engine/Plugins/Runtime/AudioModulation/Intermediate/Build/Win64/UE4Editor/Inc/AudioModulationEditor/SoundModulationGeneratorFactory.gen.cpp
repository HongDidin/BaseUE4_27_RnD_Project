// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulationEditor/Private/Factories/SoundModulationGeneratorFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationGeneratorFactory() {}
// Cross Module References
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationGeneratorFactory_NoRegister();
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationGeneratorFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AudioModulationEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGenerator_NoRegister();
// End Cross Module References
	void USoundModulationGeneratorFactory::StaticRegisterNativesUSoundModulationGeneratorFactory()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationGeneratorFactory_NoRegister()
	{
		return USoundModulationGeneratorFactory::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationGeneratorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeneratorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_GeneratorClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/SoundModulationGeneratorFactory.h" },
		{ "ModuleRelativePath", "Private/Factories/SoundModulationGeneratorFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::NewProp_GeneratorClass_MetaData[] = {
		{ "ModuleRelativePath", "Private/Factories/SoundModulationGeneratorFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::NewProp_GeneratorClass = { "GeneratorClass", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationGeneratorFactory, GeneratorClass), Z_Construct_UClass_USoundModulationGenerator_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::NewProp_GeneratorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::NewProp_GeneratorClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::NewProp_GeneratorClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationGeneratorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::ClassParams = {
		&USoundModulationGeneratorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::PropPointers),
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationGeneratorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationGeneratorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationGeneratorFactory, 4116175678);
	template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<USoundModulationGeneratorFactory>()
	{
		return USoundModulationGeneratorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationGeneratorFactory(Z_Construct_UClass_USoundModulationGeneratorFactory, &USoundModulationGeneratorFactory::StaticClass, TEXT("/Script/AudioModulationEditor"), TEXT("USoundModulationGeneratorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationGeneratorFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
