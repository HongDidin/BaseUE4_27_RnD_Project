// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/SoundModulationValue.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationValue() {}
// Cross Module References
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationMixValue();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
// End Cross Module References
class UScriptStruct* FSoundModulationMixValue::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AUDIOMODULATION_API uint32 Get_Z_Construct_UScriptStruct_FSoundModulationMixValue_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundModulationMixValue, Z_Construct_UPackage__Script_AudioModulation(), TEXT("SoundModulationMixValue"), sizeof(FSoundModulationMixValue), Get_Z_Construct_UScriptStruct_FSoundModulationMixValue_Hash());
	}
	return Singleton;
}
template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<FSoundModulationMixValue>()
{
	return FSoundModulationMixValue::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundModulationMixValue(FSoundModulationMixValue::StaticStruct, TEXT("/Script/AudioModulation"), TEXT("SoundModulationMixValue"), false, nullptr, nullptr);
static struct FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationMixValue
{
	FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationMixValue()
	{
		UScriptStruct::DeferCppStructOps<FSoundModulationMixValue>(FName(TEXT("SoundModulationMixValue")));
	}
} ScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationMixValue;
	struct Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetValue;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetUnitValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetUnitValue;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttackTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AttackTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReleaseTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReleaseTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SoundModulationValue.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundModulationMixValue>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetValue_MetaData[] = {
		{ "Category", "Modulation" },
		{ "Comment", "/** Target value of the modulator. */" },
		{ "DisplayName", "Value" },
		{ "ModuleRelativePath", "Public/SoundModulationValue.h" },
		{ "ToolTip", "Target value of the modulator." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetValue = { "TargetValue", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationMixValue, TargetValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetValue_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetUnitValue_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Target value of the modulator (in units if provided). */" },
		{ "ModuleRelativePath", "Public/SoundModulationValue.h" },
		{ "ToolTip", "Target value of the modulator (in units if provided)." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetUnitValue = { "TargetUnitValue", nullptr, (EPropertyFlags)0x0010000800002001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationMixValue, TargetUnitValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetUnitValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetUnitValue_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_AttackTime_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Time it takes (in sec) to interpolate from the parameter's default value to the mix value. */" },
		{ "DisplayName", "Attack Time (sec)" },
		{ "ModuleRelativePath", "Public/SoundModulationValue.h" },
		{ "ToolTip", "Time it takes (in sec) to interpolate from the parameter's default value to the mix value." },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_AttackTime = { "AttackTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationMixValue, AttackTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_AttackTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_AttackTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_ReleaseTime_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Time it takes (in sec) to interpolate from the current mix value to the parameter's default value. */" },
		{ "DisplayName", "Release Time (sec)" },
		{ "ModuleRelativePath", "Public/SoundModulationValue.h" },
		{ "ToolTip", "Time it takes (in sec) to interpolate from the current mix value to the parameter's default value." },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_ReleaseTime = { "ReleaseTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationMixValue, ReleaseTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_ReleaseTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_ReleaseTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetValue,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_TargetUnitValue,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_AttackTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::NewProp_ReleaseTime,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
		nullptr,
		&NewStructOps,
		"SoundModulationMixValue",
		sizeof(FSoundModulationMixValue),
		alignof(FSoundModulationMixValue),
		Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationMixValue()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundModulationMixValue_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundModulationMixValue"), sizeof(FSoundModulationMixValue), Get_Z_Construct_UScriptStruct_FSoundModulationMixValue_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundModulationMixValue_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundModulationMixValue_Hash() { return 3894160247U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
