// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATION_SoundModulationGenerator_generated_h
#error "SoundModulationGenerator.generated.h already included, missing '#pragma once' in SoundModulationGenerator.h"
#endif
#define AUDIOMODULATION_SoundModulationGenerator_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationGenerator(); \
	friend struct Z_Construct_UClass_USoundModulationGenerator_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGenerator, USoundModulatorBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationGenerator)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationGenerator(); \
	friend struct Z_Construct_UClass_USoundModulationGenerator_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGenerator, USoundModulatorBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationGenerator)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationGenerator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGenerator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationGenerator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGenerator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationGenerator(USoundModulationGenerator&&); \
	NO_API USoundModulationGenerator(const USoundModulationGenerator&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationGenerator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationGenerator(USoundModulationGenerator&&); \
	NO_API USoundModulationGenerator(const USoundModulationGenerator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationGenerator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGenerator); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGenerator)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_58_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATION_API UClass* StaticClass<class USoundModulationGenerator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_SoundModulationGenerator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
