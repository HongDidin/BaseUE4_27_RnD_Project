// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATIONEDITOR_SoundModulationParameterFactory_generated_h
#error "SoundModulationParameterFactory.generated.h already included, missing '#pragma once' in SoundModulationParameterFactory.h"
#endif
#define AUDIOMODULATIONEDITOR_SoundModulationParameterFactory_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationParameterFactory(); \
	friend struct Z_Construct_UClass_USoundModulationParameterFactory_Statics; \
public: \
	DECLARE_CLASS(USoundModulationParameterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), AUDIOMODULATIONEDITOR_API) \
	DECLARE_SERIALIZER(USoundModulationParameterFactory)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationParameterFactory(); \
	friend struct Z_Construct_UClass_USoundModulationParameterFactory_Statics; \
public: \
	DECLARE_CLASS(USoundModulationParameterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), AUDIOMODULATIONEDITOR_API) \
	DECLARE_SERIALIZER(USoundModulationParameterFactory)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOMODULATIONEDITOR_API USoundModulationParameterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationParameterFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOMODULATIONEDITOR_API, USoundModulationParameterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationParameterFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOMODULATIONEDITOR_API USoundModulationParameterFactory(USoundModulationParameterFactory&&); \
	AUDIOMODULATIONEDITOR_API USoundModulationParameterFactory(const USoundModulationParameterFactory&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	AUDIOMODULATIONEDITOR_API USoundModulationParameterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	AUDIOMODULATIONEDITOR_API USoundModulationParameterFactory(USoundModulationParameterFactory&&); \
	AUDIOMODULATIONEDITOR_API USoundModulationParameterFactory(const USoundModulationParameterFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(AUDIOMODULATIONEDITOR_API, USoundModulationParameterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationParameterFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationParameterFactory)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_10_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundModulationParameterFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<class USoundModulationParameterFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Private_Factories_SoundModulationParameterFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
