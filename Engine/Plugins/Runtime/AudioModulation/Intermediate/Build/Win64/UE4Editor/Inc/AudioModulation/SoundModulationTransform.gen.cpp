// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/SoundModulationTransform.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationTransform() {}
// Cross Module References
	AUDIOMODULATION_API UEnum* Z_Construct_UEnum_AudioModulation_ESoundModulatorCurve();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationTransform();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRichCurve();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
// End Cross Module References
	static UEnum* ESoundModulatorCurve_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AudioModulation_ESoundModulatorCurve, Z_Construct_UPackage__Script_AudioModulation(), TEXT("ESoundModulatorCurve"));
		}
		return Singleton;
	}
	template<> AUDIOMODULATION_API UEnum* StaticEnum<ESoundModulatorCurve>()
	{
		return ESoundModulatorCurve_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESoundModulatorCurve(ESoundModulatorCurve_StaticEnum, TEXT("/Script/AudioModulation"), TEXT("ESoundModulatorCurve"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AudioModulation_ESoundModulatorCurve_Hash() { return 1592824552U; }
	UEnum* Z_Construct_UEnum_AudioModulation_ESoundModulatorCurve()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESoundModulatorCurve"), 0, Get_Z_Construct_UEnum_AudioModulation_ESoundModulatorCurve_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESoundModulatorCurve::Linear", (int64)ESoundModulatorCurve::Linear },
				{ "ESoundModulatorCurve::Exp", (int64)ESoundModulatorCurve::Exp },
				{ "ESoundModulatorCurve::Exp_Inverse", (int64)ESoundModulatorCurve::Exp_Inverse },
				{ "ESoundModulatorCurve::Log", (int64)ESoundModulatorCurve::Log },
				{ "ESoundModulatorCurve::Sin", (int64)ESoundModulatorCurve::Sin },
				{ "ESoundModulatorCurve::SCurve", (int64)ESoundModulatorCurve::SCurve },
				{ "ESoundModulatorCurve::Shared", (int64)ESoundModulatorCurve::Shared },
				{ "ESoundModulatorCurve::Custom", (int64)ESoundModulatorCurve::Custom },
				{ "ESoundModulatorCurve::Count", (int64)ESoundModulatorCurve::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Count.Hidden", "" },
				{ "Count.Name", "ESoundModulatorCurve::Count" },
				{ "Custom.Comment", "// Design a custom curve unique to the owning transform\n" },
				{ "Custom.DisplayName", "Custom" },
				{ "Custom.Name", "ESoundModulatorCurve::Custom" },
				{ "Custom.ToolTip", "Design a custom curve unique to the owning transform" },
				{ "Exp.DisplayName", "Exponential" },
				{ "Exp.Name", "ESoundModulatorCurve::Exp" },
				{ "Exp_Inverse.DisplayName", "Exponential (Inverse)" },
				{ "Exp_Inverse.Name", "ESoundModulatorCurve::Exp_Inverse" },
				{ "Linear.Comment", "// Expressions\n" },
				{ "Linear.DisplayName", "Linear" },
				{ "Linear.Name", "ESoundModulatorCurve::Linear" },
				{ "Linear.ToolTip", "Expressions" },
				{ "Log.DisplayName", "Log" },
				{ "Log.Name", "ESoundModulatorCurve::Log" },
				{ "ModuleRelativePath", "Public/SoundModulationTransform.h" },
				{ "SCurve.DisplayName", "Sin (S-Curve)" },
				{ "SCurve.Name", "ESoundModulatorCurve::SCurve" },
				{ "Shared.Comment", "// Reference a shared curve asset\n" },
				{ "Shared.DisplayName", "Shared" },
				{ "Shared.Name", "ESoundModulatorCurve::Shared" },
				{ "Shared.ToolTip", "Reference a shared curve asset" },
				{ "Sin.DisplayName", "Sin (Quarter)" },
				{ "Sin.Name", "ESoundModulatorCurve::Sin" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AudioModulation,
				nullptr,
				"ESoundModulatorCurve",
				"ESoundModulatorCurve",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FSoundModulationTransform::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AUDIOMODULATION_API uint32 Get_Z_Construct_UScriptStruct_FSoundModulationTransform_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundModulationTransform, Z_Construct_UPackage__Script_AudioModulation(), TEXT("SoundModulationTransform"), sizeof(FSoundModulationTransform), Get_Z_Construct_UScriptStruct_FSoundModulationTransform_Hash());
	}
	return Singleton;
}
template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<FSoundModulationTransform>()
{
	return FSoundModulationTransform::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundModulationTransform(FSoundModulationTransform::StaticStruct, TEXT("/Script/AudioModulation"), TEXT("SoundModulationTransform"), false, nullptr, nullptr);
static struct FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationTransform
{
	FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationTransform()
	{
		UScriptStruct::DeferCppStructOps<FSoundModulationTransform>(FName(TEXT("SoundModulationTransform")));
	}
} ScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationTransform;
	struct Z_Construct_UScriptStruct_FSoundModulationTransform_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Curve_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curve_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Curve;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scalar_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scalar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveCustom_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveCustom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveShared_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurveShared;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SoundModulationTransform.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundModulationTransform>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Curve_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Curve_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** The curve to apply when transforming the output. */" },
		{ "DisplayName", "Curve Type" },
		{ "ModuleRelativePath", "Public/SoundModulationTransform.h" },
		{ "ToolTip", "The curve to apply when transforming the output." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Curve = { "Curve", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationTransform, Curve), Z_Construct_UEnum_AudioModulation_ESoundModulatorCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Curve_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Curve_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Scalar_MetaData[] = {
		{ "Category", "Input" },
		{ "ClampMax", "10.0" },
		{ "ClampMin", "0.1" },
		{ "Comment", "/** When curve set to log, exponential or exponential inverse, value is factor 'b' in following equations with output 'y' and input 'x':\n\x09 *  Exponential: y = x * 10^-b(1-x)\n\x09 *  Exponential (Inverse): y = ((x - 1) * 10^(-bx)) + 1\n\x09 *  Logarithmic: y = b * log(x) + 1\n\x09 */" },
		{ "DisplayName", "Exponential Scalar" },
		{ "ModuleRelativePath", "Public/SoundModulationTransform.h" },
		{ "ToolTip", "When curve set to log, exponential or exponential inverse, value is factor 'b' in following equations with output 'y' and input 'x':\nExponential: y = x * 10^-b(1-x)\nExponential (Inverse): y = ((x - 1) * 10^(-bx)) + 1\nLogarithmic: y = b * log(x) + 1" },
		{ "UIMax", "10.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Scalar = { "Scalar", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationTransform, Scalar), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Scalar_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Scalar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveCustom_MetaData[] = {
		{ "Comment", "/** Custom curve to apply if output curve type is set to 'Custom.' */" },
		{ "ModuleRelativePath", "Public/SoundModulationTransform.h" },
		{ "ToolTip", "Custom curve to apply if output curve type is set to 'Custom.'" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveCustom = { "CurveCustom", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationTransform, CurveCustom), Z_Construct_UScriptStruct_FRichCurve, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveCustom_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveCustom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveShared_MetaData[] = {
		{ "Category", "Curve" },
		{ "Comment", "/** Asset curve reference to apply if output curve type is set to 'Shared.' */" },
		{ "DisplayName", "Asset" },
		{ "ModuleRelativePath", "Public/SoundModulationTransform.h" },
		{ "ToolTip", "Asset curve reference to apply if output curve type is set to 'Shared.'" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveShared = { "CurveShared", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationTransform, CurveShared), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveShared_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveShared_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Curve_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Curve,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_Scalar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveCustom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::NewProp_CurveShared,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
		nullptr,
		&NewStructOps,
		"SoundModulationTransform",
		sizeof(FSoundModulationTransform),
		alignof(FSoundModulationTransform),
		Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationTransform()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundModulationTransform_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundModulationTransform"), sizeof(FSoundModulationTransform), Get_Z_Construct_UScriptStruct_FSoundModulationTransform_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundModulationTransform_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundModulationTransform_Hash() { return 1338238712U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
