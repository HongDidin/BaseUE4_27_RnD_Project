// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATION_AudioModulationSettings_generated_h
#error "AudioModulationSettings.generated.h already included, missing '#pragma once' in AudioModulationSettings.h"
#endif
#define AUDIOMODULATION_AudioModulationSettings_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAudioModulationSettings(); \
	friend struct Z_Construct_UClass_UAudioModulationSettings_Statics; \
public: \
	DECLARE_CLASS(UAudioModulationSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(UAudioModulationSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("AudioModulation");} \



#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUAudioModulationSettings(); \
	friend struct Z_Construct_UClass_UAudioModulationSettings_Statics; \
public: \
	DECLARE_CLASS(UAudioModulationSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/AudioModulation"), NO_API) \
	DECLARE_SERIALIZER(UAudioModulationSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("AudioModulation");} \



#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAudioModulationSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioModulationSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAudioModulationSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioModulationSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAudioModulationSettings(UAudioModulationSettings&&); \
	NO_API UAudioModulationSettings(const UAudioModulationSettings&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAudioModulationSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAudioModulationSettings(UAudioModulationSettings&&); \
	NO_API UAudioModulationSettings(const UAudioModulationSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAudioModulationSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAudioModulationSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAudioModulationSettings)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_13_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATION_API UClass* StaticClass<class UAudioModulationSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulation_Public_AudioModulationSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
