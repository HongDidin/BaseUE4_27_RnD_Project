// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef AUDIOMODULATIONEDITOR_AudioModulationClassTemplates_generated_h
#error "AudioModulationClassTemplates.generated.h already included, missing '#pragma once' in AudioModulationClassTemplates.h"
#endif
#define AUDIOMODULATIONEDITOR_AudioModulationClassTemplates_generated_h

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationClassTemplate(); \
	friend struct Z_Construct_UClass_USoundModulationClassTemplate_Statics; \
public: \
	DECLARE_CLASS(USoundModulationClassTemplate, UPluginClassTemplate, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationClassTemplate)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationClassTemplate(); \
	friend struct Z_Construct_UClass_USoundModulationClassTemplate_Statics; \
public: \
	DECLARE_CLASS(USoundModulationClassTemplate, UPluginClassTemplate, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationClassTemplate)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationClassTemplate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationClassTemplate) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationClassTemplate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationClassTemplate); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationClassTemplate(USoundModulationClassTemplate&&); \
	NO_API USoundModulationClassTemplate(const USoundModulationClassTemplate&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationClassTemplate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationClassTemplate(USoundModulationClassTemplate&&); \
	NO_API USoundModulationClassTemplate(const USoundModulationClassTemplate&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationClassTemplate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationClassTemplate); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationClassTemplate)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_12_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundModulationClassTemplate."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<class USoundModulationClassTemplate>();

#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_SPARSE_DATA
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_RPC_WRAPPERS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSoundModulationGeneratorClassTemplate(); \
	friend struct Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGeneratorClassTemplate, USoundModulationClassTemplate, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationGeneratorClassTemplate)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUSoundModulationGeneratorClassTemplate(); \
	friend struct Z_Construct_UClass_USoundModulationGeneratorClassTemplate_Statics; \
public: \
	DECLARE_CLASS(USoundModulationGeneratorClassTemplate, USoundModulationClassTemplate, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AudioModulationEditor"), NO_API) \
	DECLARE_SERIALIZER(USoundModulationGeneratorClassTemplate)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationGeneratorClassTemplate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGeneratorClassTemplate) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationGeneratorClassTemplate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGeneratorClassTemplate); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationGeneratorClassTemplate(USoundModulationGeneratorClassTemplate&&); \
	NO_API USoundModulationGeneratorClassTemplate(const USoundModulationGeneratorClassTemplate&); \
public:


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USoundModulationGeneratorClassTemplate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USoundModulationGeneratorClassTemplate(USoundModulationGeneratorClassTemplate&&); \
	NO_API USoundModulationGeneratorClassTemplate(const USoundModulationGeneratorClassTemplate&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USoundModulationGeneratorClassTemplate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USoundModulationGeneratorClassTemplate); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USoundModulationGeneratorClassTemplate)


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_18_PROLOG
#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_RPC_WRAPPERS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_INCLASS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_SPARSE_DATA \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h_21_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class SoundModulationGeneratorClassTemplate."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<class USoundModulationGeneratorClassTemplate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_AudioModulation_Source_AudioModulationEditor_Public_AudioModulationClassTemplates_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
