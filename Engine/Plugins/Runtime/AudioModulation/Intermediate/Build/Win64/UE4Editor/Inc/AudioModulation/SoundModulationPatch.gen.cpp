// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/SoundModulationPatch.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationPatch() {}
// Cross Module References
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundControlModulationPatch();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationParameter_NoRegister();
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundControlModulationInput();
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationTransform();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundControlBus_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationPatch_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationPatch();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USoundModulatorBase();
// End Cross Module References
class UScriptStruct* FSoundControlModulationPatch::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AUDIOMODULATION_API uint32 Get_Z_Construct_UScriptStruct_FSoundControlModulationPatch_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundControlModulationPatch, Z_Construct_UPackage__Script_AudioModulation(), TEXT("SoundControlModulationPatch"), sizeof(FSoundControlModulationPatch), Get_Z_Construct_UScriptStruct_FSoundControlModulationPatch_Hash());
	}
	return Singleton;
}
template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<FSoundControlModulationPatch>()
{
	return FSoundControlModulationPatch::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundControlModulationPatch(FSoundControlModulationPatch::StaticStruct, TEXT("/Script/AudioModulation"), TEXT("SoundControlModulationPatch"), false, nullptr, nullptr);
static struct FScriptStruct_AudioModulation_StaticRegisterNativesFSoundControlModulationPatch
{
	FScriptStruct_AudioModulation_StaticRegisterNativesFSoundControlModulationPatch()
	{
		UScriptStruct::DeferCppStructOps<FSoundControlModulationPatch>(FName(TEXT("SoundControlModulationPatch")));
	}
} ScriptStruct_AudioModulation_StaticRegisterNativesFSoundControlModulationPatch;
	struct Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBypass_MetaData[];
#endif
		static void NewProp_bBypass_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBypass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputParameter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutputParameter;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Inputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Inputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Inputs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundControlModulationPatch>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_bBypass_MetaData[] = {
		{ "Category", "Inputs" },
		{ "Comment", "/** Whether or not patch is bypassed (patch is still active, but always returns output parameter default value when modulated) */" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
		{ "ToolTip", "Whether or not patch is bypassed (patch is still active, but always returns output parameter default value when modulated)" },
	};
#endif
	void Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_bBypass_SetBit(void* Obj)
	{
		((FSoundControlModulationPatch*)Obj)->bBypass = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_bBypass = { "bBypass", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FSoundControlModulationPatch), &Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_bBypass_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_bBypass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_bBypass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_OutputParameter_MetaData[] = {
		{ "Category", "Output" },
		{ "DisplayName", "Parameter" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_OutputParameter = { "OutputParameter", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundControlModulationPatch, OutputParameter), Z_Construct_UClass_USoundModulationParameter_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_OutputParameter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_OutputParameter_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_Inputs_Inner = { "Inputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoundControlModulationInput, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_Inputs_MetaData[] = {
		{ "Category", "Inputs" },
		{ "Comment", "/** Modulation inputs */" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
		{ "ToolTip", "Modulation inputs" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_Inputs = { "Inputs", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundControlModulationPatch, Inputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_Inputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_Inputs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_bBypass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_OutputParameter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_Inputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::NewProp_Inputs,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
		nullptr,
		&NewStructOps,
		"SoundControlModulationPatch",
		sizeof(FSoundControlModulationPatch),
		alignof(FSoundControlModulationPatch),
		Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundControlModulationPatch()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundControlModulationPatch_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundControlModulationPatch"), sizeof(FSoundControlModulationPatch), Get_Z_Construct_UScriptStruct_FSoundControlModulationPatch_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundControlModulationPatch_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundControlModulationPatch_Hash() { return 2726241951U; }
class UScriptStruct* FSoundControlModulationInput::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AUDIOMODULATION_API uint32 Get_Z_Construct_UScriptStruct_FSoundControlModulationInput_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundControlModulationInput, Z_Construct_UPackage__Script_AudioModulation(), TEXT("SoundControlModulationInput"), sizeof(FSoundControlModulationInput), Get_Z_Construct_UScriptStruct_FSoundControlModulationInput_Hash());
	}
	return Singleton;
}
template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<FSoundControlModulationInput>()
{
	return FSoundControlModulationInput::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundControlModulationInput(FSoundControlModulationInput::StaticStruct, TEXT("/Script/AudioModulation"), TEXT("SoundControlModulationInput"), false, nullptr, nullptr);
static struct FScriptStruct_AudioModulation_StaticRegisterNativesFSoundControlModulationInput
{
	FScriptStruct_AudioModulation_StaticRegisterNativesFSoundControlModulationInput()
	{
		UScriptStruct::DeferCppStructOps<FSoundControlModulationInput>(FName(TEXT("SoundControlModulationInput")));
	}
} ScriptStruct_AudioModulation_StaticRegisterNativesFSoundControlModulationInput;
	struct Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSampleAndHold_MetaData[];
#endif
		static void NewProp_bSampleAndHold_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSampleAndHold;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transform_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bus_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Bus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundControlModulationInput>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_bSampleAndHold_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Get the modulated input value on parent patch initialization and hold that value for its lifetime */" },
		{ "DisplayName", "Sample-And-Hold" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
		{ "ToolTip", "Get the modulated input value on parent patch initialization and hold that value for its lifetime" },
	};
#endif
	void Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_bSampleAndHold_SetBit(void* Obj)
	{
		((FSoundControlModulationInput*)Obj)->bSampleAndHold = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_bSampleAndHold = { "bSampleAndHold", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FSoundControlModulationInput), &Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_bSampleAndHold_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_bSampleAndHold_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_bSampleAndHold_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Transform_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** Transform to apply to the input prior to mix phase */" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
		{ "ToolTip", "Transform to apply to the input prior to mix phase" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Transform = { "Transform", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundControlModulationInput, Transform), Z_Construct_UScriptStruct_FSoundModulationTransform, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Transform_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Transform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Bus_MetaData[] = {
		{ "Category", "Input" },
		{ "Comment", "/** The input bus */" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
		{ "ToolTip", "The input bus" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Bus = { "Bus", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundControlModulationInput, Bus), Z_Construct_UClass_USoundControlBus_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Bus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Bus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_bSampleAndHold,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Transform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::NewProp_Bus,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
		nullptr,
		&NewStructOps,
		"SoundControlModulationInput",
		sizeof(FSoundControlModulationInput),
		alignof(FSoundControlModulationInput),
		Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundControlModulationInput()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundControlModulationInput_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundControlModulationInput"), sizeof(FSoundControlModulationInput), Get_Z_Construct_UScriptStruct_FSoundControlModulationInput_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundControlModulationInput_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundControlModulationInput_Hash() { return 821495152U; }
	void USoundModulationPatch::StaticRegisterNativesUSoundModulationPatch()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationPatch_NoRegister()
	{
		return USoundModulationPatch::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationPatch_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PatchSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PatchSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationPatch_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulatorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationPatch_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "SoundModulationPatch.h" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationPatch_Statics::NewProp_PatchSettings_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ModuleRelativePath", "Public/SoundModulationPatch.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundModulationPatch_Statics::NewProp_PatchSettings = { "PatchSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationPatch, PatchSettings), Z_Construct_UScriptStruct_FSoundControlModulationPatch, METADATA_PARAMS(Z_Construct_UClass_USoundModulationPatch_Statics::NewProp_PatchSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationPatch_Statics::NewProp_PatchSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationPatch_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationPatch_Statics::NewProp_PatchSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationPatch_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationPatch>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationPatch_Statics::ClassParams = {
		&USoundModulationPatch::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationPatch_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationPatch_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationPatch_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationPatch_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationPatch()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationPatch_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationPatch, 1387683004);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationPatch>()
	{
		return USoundModulationPatch::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationPatch(Z_Construct_UClass_USoundModulationPatch, &USoundModulationPatch::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationPatch"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationPatch);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
