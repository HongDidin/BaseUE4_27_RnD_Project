// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulationEditor/Private/Factories/SoundControlBusMixFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundControlBusMixFactory() {}
// Cross Module References
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundControlBusMixFactory_NoRegister();
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundControlBusMixFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AudioModulationEditor();
// End Cross Module References
	void USoundControlBusMixFactory::StaticRegisterNativesUSoundControlBusMixFactory()
	{
	}
	UClass* Z_Construct_UClass_USoundControlBusMixFactory_NoRegister()
	{
		return USoundControlBusMixFactory::StaticClass();
	}
	struct Z_Construct_UClass_USoundControlBusMixFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundControlBusMixFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundControlBusMixFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/SoundControlBusMixFactory.h" },
		{ "ModuleRelativePath", "Private/Factories/SoundControlBusMixFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundControlBusMixFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundControlBusMixFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundControlBusMixFactory_Statics::ClassParams = {
		&USoundControlBusMixFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundControlBusMixFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundControlBusMixFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundControlBusMixFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundControlBusMixFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundControlBusMixFactory, 33931588);
	template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<USoundControlBusMixFactory>()
	{
		return USoundControlBusMixFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundControlBusMixFactory(Z_Construct_UClass_USoundControlBusMixFactory, &USoundControlBusMixFactory::StaticClass, TEXT("/Script/AudioModulationEditor"), TEXT("USoundControlBusMixFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundControlBusMixFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
