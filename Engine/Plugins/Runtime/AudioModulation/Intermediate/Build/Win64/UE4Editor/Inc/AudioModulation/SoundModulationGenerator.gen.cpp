// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/SoundModulationGenerator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationGenerator() {}
// Cross Module References
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGenerator_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGenerator();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USoundModulatorBase();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
// End Cross Module References
	void USoundModulationGenerator::StaticRegisterNativesUSoundModulationGenerator()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationGenerator_NoRegister()
	{
		return USoundModulationGenerator::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulatorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGenerator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for modulators that algorithmically generate values that can effect\n * various endpoints (ex. Control Buses & Parameter Destinations)\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "SoundModulationGenerator.h" },
		{ "ModuleRelativePath", "Public/SoundModulationGenerator.h" },
		{ "ToolTip", "Base class for modulators that algorithmically generate values that can effect\nvarious endpoints (ex. Control Buses & Parameter Destinations)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationGenerator_Statics::ClassParams = {
		&USoundModulationGenerator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationGenerator, 2897056736);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationGenerator>()
	{
		return USoundModulationGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationGenerator(Z_Construct_UClass_USoundModulationGenerator, &USoundModulationGenerator::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationGenerator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
