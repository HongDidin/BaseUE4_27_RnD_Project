// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/Generators/SoundModulationLFO.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationLFO() {}
// Cross Module References
	AUDIOMODULATION_API UEnum* Z_Construct_UEnum_AudioModulation_FSoundModulationLFOShape();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationLFOParams();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGeneratorLFO_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGeneratorLFO();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGenerator();
// End Cross Module References
	static UEnum* FSoundModulationLFOShape_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_AudioModulation_FSoundModulationLFOShape, Z_Construct_UPackage__Script_AudioModulation(), TEXT("FSoundModulationLFOShape"));
		}
		return Singleton;
	}
	template<> AUDIOMODULATION_API UEnum* StaticEnum<FSoundModulationLFOShape>()
	{
		return FSoundModulationLFOShape_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_FSoundModulationLFOShape(FSoundModulationLFOShape_StaticEnum, TEXT("/Script/AudioModulation"), TEXT("FSoundModulationLFOShape"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_AudioModulation_FSoundModulationLFOShape_Hash() { return 645252780U; }
	UEnum* Z_Construct_UEnum_AudioModulation_FSoundModulationLFOShape()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("FSoundModulationLFOShape"), 0, Get_Z_Construct_UEnum_AudioModulation_FSoundModulationLFOShape_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "FSoundModulationLFOShape::Sine", (int64)FSoundModulationLFOShape::Sine },
				{ "FSoundModulationLFOShape::UpSaw", (int64)FSoundModulationLFOShape::UpSaw },
				{ "FSoundModulationLFOShape::DownSaw", (int64)FSoundModulationLFOShape::DownSaw },
				{ "FSoundModulationLFOShape::Square", (int64)FSoundModulationLFOShape::Square },
				{ "FSoundModulationLFOShape::Triangle", (int64)FSoundModulationLFOShape::Triangle },
				{ "FSoundModulationLFOShape::Exponential", (int64)FSoundModulationLFOShape::Exponential },
				{ "FSoundModulationLFOShape::RandomSampleHold", (int64)FSoundModulationLFOShape::RandomSampleHold },
				{ "FSoundModulationLFOShape::COUNT", (int64)FSoundModulationLFOShape::COUNT },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "COUNT.Hidden", "" },
				{ "COUNT.Name", "FSoundModulationLFOShape::COUNT" },
				{ "DownSaw.DisplayName", "Saw (Down)" },
				{ "DownSaw.Name", "FSoundModulationLFOShape::DownSaw" },
				{ "Exponential.DisplayName", "Exponential" },
				{ "Exponential.Name", "FSoundModulationLFOShape::Exponential" },
				{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
				{ "RandomSampleHold.DisplayName", "Random" },
				{ "RandomSampleHold.Name", "FSoundModulationLFOShape::RandomSampleHold" },
				{ "Sine.DisplayName", "Sine" },
				{ "Sine.Name", "FSoundModulationLFOShape::Sine" },
				{ "Square.DisplayName", "Square" },
				{ "Square.Name", "FSoundModulationLFOShape::Square" },
				{ "Triangle.DisplayName", "Triangle" },
				{ "Triangle.Name", "FSoundModulationLFOShape::Triangle" },
				{ "UpSaw.DisplayName", "Saw (Up)" },
				{ "UpSaw.Name", "FSoundModulationLFOShape::UpSaw" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_AudioModulation,
				nullptr,
				"FSoundModulationLFOShape",
				"FSoundModulationLFOShape",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FSoundModulationLFOParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AUDIOMODULATION_API uint32 Get_Z_Construct_UScriptStruct_FSoundModulationLFOParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSoundModulationLFOParams, Z_Construct_UPackage__Script_AudioModulation(), TEXT("SoundModulationLFOParams"), sizeof(FSoundModulationLFOParams), Get_Z_Construct_UScriptStruct_FSoundModulationLFOParams_Hash());
	}
	return Singleton;
}
template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<FSoundModulationLFOParams>()
{
	return FSoundModulationLFOParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSoundModulationLFOParams(FSoundModulationLFOParams::StaticStruct, TEXT("/Script/AudioModulation"), TEXT("SoundModulationLFOParams"), false, nullptr, nullptr);
static struct FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationLFOParams
{
	FScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationLFOParams()
	{
		UScriptStruct::DeferCppStructOps<FSoundModulationLFOParams>(FName(TEXT("SoundModulationLFOParams")));
	}
} ScriptStruct_AudioModulation_StaticRegisterNativesFSoundModulationLFOParams;
	struct Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Shape_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shape_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Shape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Amplitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Amplitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Offset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Offset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLooping_MetaData[];
#endif
		static void NewProp_bLooping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLooping;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBypass_MetaData[];
#endif
		static void NewProp_bBypass_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBypass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSoundModulationLFOParams>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Shape_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Shape_MetaData[] = {
		{ "Category", "Modulation" },
		{ "Comment", "/** Shape of oscillating waveform */" },
		{ "DisplayPriority", "20" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ToolTip", "Shape of oscillating waveform" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Shape = { "Shape", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationLFOParams, Shape), Z_Construct_UEnum_AudioModulation_FSoundModulationLFOShape, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Shape_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Shape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Amplitude_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMax", "1" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Amplitude of oscillator */" },
		{ "DisplayPriority", "30" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ToolTip", "Amplitude of oscillator" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Amplitude = { "Amplitude", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationLFOParams, Amplitude), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Amplitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Amplitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Frequency_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMax", "20" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Frequency of oscillator */" },
		{ "DisplayPriority", "40" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ToolTip", "Frequency of oscillator" },
		{ "UIMax", "20" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Frequency = { "Frequency", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationLFOParams, Frequency), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Frequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Frequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Offset_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMax", "1" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Offset of oscillator */" },
		{ "DisplayPriority", "50" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ToolTip", "Offset of oscillator" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Offset = { "Offset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSoundModulationLFOParams, Offset), METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Offset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Offset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bLooping_MetaData[] = {
		{ "Category", "Modulation" },
		{ "Comment", "/** Whether or not to loop the oscillation more than once */" },
		{ "DisplayPriority", "60" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ToolTip", "Whether or not to loop the oscillation more than once" },
	};
#endif
	void Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bLooping_SetBit(void* Obj)
	{
		((FSoundModulationLFOParams*)Obj)->bLooping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bLooping = { "bLooping", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FSoundModulationLFOParams), &Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bLooping_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bLooping_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bLooping_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bBypass_MetaData[] = {
		{ "Category", "Modulation" },
		{ "Comment", "/** If true, bypasses LFO bus from being modulated by parameters, patches, or mixed (LFO remains active and computed). */" },
		{ "DisplayPriority", "10" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ToolTip", "If true, bypasses LFO bus from being modulated by parameters, patches, or mixed (LFO remains active and computed)." },
	};
#endif
	void Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bBypass_SetBit(void* Obj)
	{
		((FSoundModulationLFOParams*)Obj)->bBypass = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bBypass = { "bBypass", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FSoundModulationLFOParams), &Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bBypass_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bBypass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bBypass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Shape_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Shape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Amplitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Frequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_Offset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bLooping,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::NewProp_bBypass,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
		nullptr,
		&NewStructOps,
		"SoundModulationLFOParams",
		sizeof(FSoundModulationLFOParams),
		alignof(FSoundModulationLFOParams),
		Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSoundModulationLFOParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSoundModulationLFOParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SoundModulationLFOParams"), sizeof(FSoundModulationLFOParams), Get_Z_Construct_UScriptStruct_FSoundModulationLFOParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSoundModulationLFOParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSoundModulationLFOParams_Hash() { return 1599070927U; }
	void USoundModulationGeneratorLFO::StaticRegisterNativesUSoundModulationGeneratorLFO()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationGeneratorLFO_NoRegister()
	{
		return USoundModulationGeneratorLFO::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationGeneratorLFO_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// namespace AudioModulation\n" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "Generators/SoundModulationLFO.h" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ToolTip", "namespace AudioModulation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::NewProp_Params_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationLFO.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationGeneratorLFO, Params), Z_Construct_UScriptStruct_FSoundModulationLFOParams, METADATA_PARAMS(Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::NewProp_Params,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationGeneratorLFO>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::ClassParams = {
		&USoundModulationGeneratorLFO::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::PropPointers),
		0,
		0x000810A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationGeneratorLFO()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationGeneratorLFO_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationGeneratorLFO, 2011039496);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationGeneratorLFO>()
	{
		return USoundModulationGeneratorLFO::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationGeneratorLFO(Z_Construct_UClass_USoundModulationGeneratorLFO, &USoundModulationGeneratorLFO::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationGeneratorLFO"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationGeneratorLFO);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
