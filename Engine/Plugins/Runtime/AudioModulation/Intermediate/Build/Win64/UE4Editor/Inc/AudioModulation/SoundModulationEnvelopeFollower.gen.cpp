// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulation/Public/Generators/SoundModulationEnvelopeFollower.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationEnvelopeFollower() {}
// Cross Module References
	AUDIOMODULATION_API UScriptStruct* Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams();
	UPackage* Z_Construct_UPackage__Script_AudioModulation();
	ENGINE_API UClass* Z_Construct_UClass_UAudioBus_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_NoRegister();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower();
	AUDIOMODULATION_API UClass* Z_Construct_UClass_USoundModulationGenerator();
// End Cross Module References
class UScriptStruct* FEnvelopeFollowerGeneratorParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern AUDIOMODULATION_API uint32 Get_Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams, Z_Construct_UPackage__Script_AudioModulation(), TEXT("EnvelopeFollowerGeneratorParams"), sizeof(FEnvelopeFollowerGeneratorParams), Get_Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Hash());
	}
	return Singleton;
}
template<> AUDIOMODULATION_API UScriptStruct* StaticStruct<FEnvelopeFollowerGeneratorParams>()
{
	return FEnvelopeFollowerGeneratorParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FEnvelopeFollowerGeneratorParams(FEnvelopeFollowerGeneratorParams::StaticStruct, TEXT("/Script/AudioModulation"), TEXT("EnvelopeFollowerGeneratorParams"), false, nullptr, nullptr);
static struct FScriptStruct_AudioModulation_StaticRegisterNativesFEnvelopeFollowerGeneratorParams
{
	FScriptStruct_AudioModulation_StaticRegisterNativesFEnvelopeFollowerGeneratorParams()
	{
		UScriptStruct::DeferCppStructOps<FEnvelopeFollowerGeneratorParams>(FName(TEXT("EnvelopeFollowerGeneratorParams")));
	}
} ScriptStruct_AudioModulation_StaticRegisterNativesFEnvelopeFollowerGeneratorParams;
	struct Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBypass_MetaData[];
#endif
		static void NewProp_bBypass_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBypass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvert_MetaData[];
#endif
		static void NewProp_bInvert_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvert;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioBus_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AudioBus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Gain_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Gain;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttackTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AttackTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReleaseTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReleaseTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FEnvelopeFollowerGeneratorParams>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bBypass_MetaData[] = {
		{ "Category", "Modulation" },
		{ "Comment", "/** If true, bypasses generator from being modulated by parameters, patches, or mixed (remains active and computed). */" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ToolTip", "If true, bypasses generator from being modulated by parameters, patches, or mixed (remains active and computed)." },
	};
#endif
	void Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bBypass_SetBit(void* Obj)
	{
		((FEnvelopeFollowerGeneratorParams*)Obj)->bBypass = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bBypass = { "bBypass", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEnvelopeFollowerGeneratorParams), &Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bBypass_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bBypass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bBypass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bInvert_MetaData[] = {
		{ "Category", "Modulation" },
		{ "Comment", "/** If true, inverts output */" },
		{ "DisplayAfter", "ReleaseTime" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ToolTip", "If true, inverts output" },
	};
#endif
	void Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bInvert_SetBit(void* Obj)
	{
		((FEnvelopeFollowerGeneratorParams*)Obj)->bInvert = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bInvert = { "bInvert", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FEnvelopeFollowerGeneratorParams), &Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bInvert_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bInvert_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bInvert_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AudioBus_MetaData[] = {
		{ "Category", "Modulation" },
		{ "Comment", "/** AudioBus to follow amplitude of and generate modulation control signal from. */" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ToolTip", "AudioBus to follow amplitude of and generate modulation control signal from." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AudioBus = { "AudioBus", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnvelopeFollowerGeneratorParams, AudioBus), Z_Construct_UClass_UAudioBus_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AudioBus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AudioBus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_Gain_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** Gain to apply to amplitude signal. */" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ToolTip", "Gain to apply to amplitude signal." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_Gain = { "Gain", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnvelopeFollowerGeneratorParams, Gain), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_Gain_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_Gain_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AttackTime_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** Attack time of envelope response (in sec) */" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ToolTip", "Attack time of envelope response (in sec)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AttackTime = { "AttackTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnvelopeFollowerGeneratorParams, AttackTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AttackTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AttackTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_ReleaseTime_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ClampMin", "0.000000" },
		{ "Comment", "/** Release time of envelope response (in sec) */" },
		{ "EditCondition", "!bBypass" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ToolTip", "Release time of envelope response (in sec)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_ReleaseTime = { "ReleaseTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FEnvelopeFollowerGeneratorParams, ReleaseTime), METADATA_PARAMS(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_ReleaseTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_ReleaseTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bBypass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_bInvert,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AudioBus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_Gain,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_AttackTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::NewProp_ReleaseTime,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
		nullptr,
		&NewStructOps,
		"EnvelopeFollowerGeneratorParams",
		sizeof(FEnvelopeFollowerGeneratorParams),
		alignof(FEnvelopeFollowerGeneratorParams),
		Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_AudioModulation();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("EnvelopeFollowerGeneratorParams"), sizeof(FEnvelopeFollowerGeneratorParams), Get_Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams_Hash() { return 1946205819U; }
	void USoundModulationGeneratorEnvelopeFollower::StaticRegisterNativesUSoundModulationGeneratorEnvelopeFollower()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_NoRegister()
	{
		return USoundModulationGeneratorEnvelopeFollower::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundModulationGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// namespace AudioModulation\n" },
		{ "DisplayName", "Envelope Follower Generator" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "Generators/SoundModulationEnvelopeFollower.h" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ToolTip", "namespace AudioModulation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::NewProp_Params_MetaData[] = {
		{ "Category", "Modulation" },
		{ "ModuleRelativePath", "Public/Generators/SoundModulationEnvelopeFollower.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USoundModulationGeneratorEnvelopeFollower, Params), Z_Construct_UScriptStruct_FEnvelopeFollowerGeneratorParams, METADATA_PARAMS(Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::NewProp_Params,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationGeneratorEnvelopeFollower>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::ClassParams = {
		&USoundModulationGeneratorEnvelopeFollower::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationGeneratorEnvelopeFollower, 3370455564);
	template<> AUDIOMODULATION_API UClass* StaticClass<USoundModulationGeneratorEnvelopeFollower>()
	{
		return USoundModulationGeneratorEnvelopeFollower::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationGeneratorEnvelopeFollower(Z_Construct_UClass_USoundModulationGeneratorEnvelopeFollower, &USoundModulationGeneratorEnvelopeFollower::StaticClass, TEXT("/Script/AudioModulation"), TEXT("USoundModulationGeneratorEnvelopeFollower"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationGeneratorEnvelopeFollower);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
