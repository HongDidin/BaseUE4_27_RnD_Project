// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AudioModulationEditor/Private/Factories/SoundModulationParameterFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoundModulationParameterFactory() {}
// Cross Module References
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationParameterFactory_NoRegister();
	AUDIOMODULATIONEDITOR_API UClass* Z_Construct_UClass_USoundModulationParameterFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_AudioModulationEditor();
// End Cross Module References
	void USoundModulationParameterFactory::StaticRegisterNativesUSoundModulationParameterFactory()
	{
	}
	UClass* Z_Construct_UClass_USoundModulationParameterFactory_NoRegister()
	{
		return USoundModulationParameterFactory::StaticClass();
	}
	struct Z_Construct_UClass_USoundModulationParameterFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USoundModulationParameterFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_AudioModulationEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USoundModulationParameterFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/SoundModulationParameterFactory.h" },
		{ "ModuleRelativePath", "Private/Factories/SoundModulationParameterFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USoundModulationParameterFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USoundModulationParameterFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USoundModulationParameterFactory_Statics::ClassParams = {
		&USoundModulationParameterFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_USoundModulationParameterFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USoundModulationParameterFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USoundModulationParameterFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USoundModulationParameterFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USoundModulationParameterFactory, 869818377);
	template<> AUDIOMODULATIONEDITOR_API UClass* StaticClass<USoundModulationParameterFactory>()
	{
		return USoundModulationParameterFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USoundModulationParameterFactory(Z_Construct_UClass_USoundModulationParameterFactory, &USoundModulationParameterFactory::StaticClass, TEXT("/Script/AudioModulationEditor"), TEXT("USoundModulationParameterFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USoundModulationParameterFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
