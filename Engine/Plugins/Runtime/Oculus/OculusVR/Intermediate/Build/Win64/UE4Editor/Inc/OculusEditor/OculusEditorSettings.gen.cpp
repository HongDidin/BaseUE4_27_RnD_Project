// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusEditor/Public/OculusEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusEditorSettings() {}
// Cross Module References
	OCULUSEDITOR_API UEnum* Z_Construct_UEnum_OculusEditor_EOculusPlatform();
	UPackage* Z_Construct_UPackage__Script_OculusEditor();
	OCULUSEDITOR_API UClass* Z_Construct_UClass_UOculusEditorSettings_NoRegister();
	OCULUSEDITOR_API UClass* Z_Construct_UClass_UOculusEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* EOculusPlatform_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OculusEditor_EOculusPlatform, Z_Construct_UPackage__Script_OculusEditor(), TEXT("EOculusPlatform"));
		}
		return Singleton;
	}
	template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusPlatform>()
	{
		return EOculusPlatform_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOculusPlatform(EOculusPlatform_StaticEnum, TEXT("/Script/OculusEditor"), TEXT("EOculusPlatform"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OculusEditor_EOculusPlatform_Hash() { return 3510347006U; }
	UEnum* Z_Construct_UEnum_OculusEditor_EOculusPlatform()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OculusEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOculusPlatform"), 0, Get_Z_Construct_UEnum_OculusEditor_EOculusPlatform_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOculusPlatform::PC", (int64)EOculusPlatform::PC },
				{ "EOculusPlatform::Mobile", (int64)EOculusPlatform::Mobile },
				{ "EOculusPlatform::Length", (int64)EOculusPlatform::Length },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Length.DisplayName", "Invalid" },
				{ "Length.Name", "EOculusPlatform::Length" },
				{ "Mobile.DisplayName", "Mobile" },
				{ "Mobile.Name", "EOculusPlatform::Mobile" },
				{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
				{ "PC.DisplayName", "PC" },
				{ "PC.Name", "EOculusPlatform::PC" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OculusEditor,
				nullptr,
				"EOculusPlatform",
				"EOculusPlatform",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UOculusEditorSettings::StaticRegisterNativesUOculusEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UOculusEditorSettings_NoRegister()
	{
		return UOculusEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOculusEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_PerfToolIgnoreList_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PerfToolIgnoreList_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerfToolIgnoreList_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PerfToolIgnoreList;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PerfToolTargetPlatform_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerfToolTargetPlatform_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PerfToolTargetPlatform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAddMenuOption_MetaData[];
#endif
		static void NewProp_bAddMenuOption_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAddMenuOption;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "OculusEditorSettings.h" },
		{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_ValueProp = { "PerfToolIgnoreList", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_Key_KeyProp = { "PerfToolIgnoreList_Key", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList = { "PerfToolIgnoreList", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusEditorSettings, PerfToolIgnoreList), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform = { "PerfToolTargetPlatform", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusEditorSettings, PerfToolTargetPlatform), Z_Construct_UEnum_OculusEditor_EOculusPlatform, METADATA_PARAMS(Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_bAddMenuOption_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusEditorSettings.h" },
	};
#endif
	void Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_bAddMenuOption_SetBit(void* Obj)
	{
		((UOculusEditorSettings*)Obj)->bAddMenuOption = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_bAddMenuOption = { "bAddMenuOption", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOculusEditorSettings), &Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_bAddMenuOption_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_bAddMenuOption_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_bAddMenuOption_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolIgnoreList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_PerfToolTargetPlatform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusEditorSettings_Statics::NewProp_bAddMenuOption,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusEditorSettings_Statics::ClassParams = {
		&UOculusEditorSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOculusEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEditorSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusEditorSettings, 3122891158);
	template<> OCULUSEDITOR_API UClass* StaticClass<UOculusEditorSettings>()
	{
		return UOculusEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusEditorSettings(Z_Construct_UClass_UOculusEditorSettings, &UOculusEditorSettings::StaticClass, TEXT("/Script/OculusEditor"), TEXT("UOculusEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
