// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSEDITOR_OculusEditorSettings_generated_h
#error "OculusEditorSettings.generated.h already included, missing '#pragma once' in OculusEditorSettings.h"
#endif
#define OCULUSEDITOR_OculusEditorSettings_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusEditorSettings(); \
	friend struct Z_Construct_UClass_UOculusEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UOculusEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusEditor"), NO_API) \
	DECLARE_SERIALIZER(UOculusEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUOculusEditorSettings(); \
	friend struct Z_Construct_UClass_UOculusEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UOculusEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusEditor"), NO_API) \
	DECLARE_SERIALIZER(UOculusEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusEditorSettings(UOculusEditorSettings&&); \
	NO_API UOculusEditorSettings(const UOculusEditorSettings&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusEditorSettings(UOculusEditorSettings&&); \
	NO_API UOculusEditorSettings(const UOculusEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOculusEditorSettings)


#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_20_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSEDITOR_API UClass* StaticClass<class UOculusEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusEditorSettings_h


#define FOREACH_ENUM_EOCULUSPLATFORM(op) \
	op(EOculusPlatform::PC) \
	op(EOculusPlatform::Mobile) \
	op(EOculusPlatform::Length) 

enum class EOculusPlatform : uint8;
template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusPlatform>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
