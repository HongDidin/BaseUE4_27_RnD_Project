// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSEDITOR_OculusPlatformToolSettings_generated_h
#error "OculusPlatformToolSettings.generated.h already included, missing '#pragma once' in OculusPlatformToolSettings.h"
#endif
#define OCULUSEDITOR_OculusPlatformToolSettings_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_72_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAssetConfigArray_Statics; \
	OCULUSEDITOR_API static class UScriptStruct* StaticStruct();


template<> OCULUSEDITOR_API UScriptStruct* StaticStruct<struct FAssetConfigArray>();

#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_54_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAssetConfig_Statics; \
	OCULUSEDITOR_API static class UScriptStruct* StaticStruct();


template<> OCULUSEDITOR_API UScriptStruct* StaticStruct<struct FAssetConfig>();

#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_39_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRedistPackage_Statics; \
	OCULUSEDITOR_API static class UScriptStruct* StaticStruct();


template<> OCULUSEDITOR_API UScriptStruct* StaticStruct<struct FRedistPackage>();

#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusPlatformToolSettings(); \
	friend struct Z_Construct_UClass_UOculusPlatformToolSettings_Statics; \
public: \
	DECLARE_CLASS(UOculusPlatformToolSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusEditor"), NO_API) \
	DECLARE_SERIALIZER(UOculusPlatformToolSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_INCLASS \
private: \
	static void StaticRegisterNativesUOculusPlatformToolSettings(); \
	friend struct Z_Construct_UClass_UOculusPlatformToolSettings_Statics; \
public: \
	DECLARE_CLASS(UOculusPlatformToolSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusEditor"), NO_API) \
	DECLARE_SERIALIZER(UOculusPlatformToolSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusPlatformToolSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusPlatformToolSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusPlatformToolSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusPlatformToolSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusPlatformToolSettings(UOculusPlatformToolSettings&&); \
	NO_API UOculusPlatformToolSettings(const UOculusPlatformToolSettings&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusPlatformToolSettings(UOculusPlatformToolSettings&&); \
	NO_API UOculusPlatformToolSettings(const UOculusPlatformToolSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusPlatformToolSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusPlatformToolSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOculusPlatformToolSettings)


#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OculusTargetPlatform() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusTargetPlatform); } \
	FORCEINLINE static uint32 __PPO__OculusApplicationID() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusApplicationID); } \
	FORCEINLINE static uint32 __PPO__OculusApplicationToken() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusApplicationToken); } \
	FORCEINLINE static uint32 __PPO__OculusReleaseChannel() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusReleaseChannel); } \
	FORCEINLINE static uint32 __PPO__OculusReleaseNote() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusReleaseNote); } \
	FORCEINLINE static uint32 __PPO__OculusLaunchFilePath() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusLaunchFilePath); } \
	FORCEINLINE static uint32 __PPO__OculusRiftGamepadEmulation() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRiftGamepadEmulation); } \
	FORCEINLINE static uint32 __PPO__OculusLanguagePacksPath() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusLanguagePacksPath); } \
	FORCEINLINE static uint32 __PPO__OculusExpansionFilesPath() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusExpansionFilesPath); } \
	FORCEINLINE static uint32 __PPO__OculusSymbolDirPath() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusSymbolDirPath); } \
	FORCEINLINE static uint32 __PPO__OculusAssetConfigs() { return STRUCT_OFFSET(UOculusPlatformToolSettings, OculusAssetConfigs); }


#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_81_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h_84_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSEDITOR_API UClass* StaticClass<class UOculusPlatformToolSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusVR_Source_OculusEditor_Public_OculusPlatformToolSettings_h


#define FOREACH_ENUM_EOCULUSASSETTYPE(op) \
	op(EOculusAssetType::Default) \
	op(EOculusAssetType::Store) \
	op(EOculusAssetType::Language_Pack) \
	op(EOculusAssetType::Length) 

enum class EOculusAssetType : uint8;
template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusAssetType>();

#define FOREACH_ENUM_EOCULUSGAMEPADEMULATION(op) \
	op(EOculusGamepadEmulation::Off) \
	op(EOculusGamepadEmulation::Twinstick) \
	op(EOculusGamepadEmulation::RightDPad) \
	op(EOculusGamepadEmulation::LeftDPad) \
	op(EOculusGamepadEmulation::Length) 

enum class EOculusGamepadEmulation : uint8;
template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusGamepadEmulation>();

#define FOREACH_ENUM_EOCULUSPLATFORMTARGET(op) \
	op(EOculusPlatformTarget::Rift) \
	op(EOculusPlatformTarget::Quest) \
	op(EOculusPlatformTarget::Length) 

enum class EOculusPlatformTarget : uint8;
template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusPlatformTarget>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
