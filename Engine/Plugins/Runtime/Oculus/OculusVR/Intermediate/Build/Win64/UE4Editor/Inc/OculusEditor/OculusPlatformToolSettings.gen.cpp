// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusEditor/Public/OculusPlatformToolSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusPlatformToolSettings() {}
// Cross Module References
	OCULUSEDITOR_API UEnum* Z_Construct_UEnum_OculusEditor_EOculusAssetType();
	UPackage* Z_Construct_UPackage__Script_OculusEditor();
	OCULUSEDITOR_API UEnum* Z_Construct_UEnum_OculusEditor_EOculusGamepadEmulation();
	OCULUSEDITOR_API UEnum* Z_Construct_UEnum_OculusEditor_EOculusPlatformTarget();
	OCULUSEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FAssetConfigArray();
	OCULUSEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FAssetConfig();
	OCULUSEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FRedistPackage();
	OCULUSEDITOR_API UClass* Z_Construct_UClass_UOculusPlatformToolSettings_NoRegister();
	OCULUSEDITOR_API UClass* Z_Construct_UClass_UOculusPlatformToolSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* EOculusAssetType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OculusEditor_EOculusAssetType, Z_Construct_UPackage__Script_OculusEditor(), TEXT("EOculusAssetType"));
		}
		return Singleton;
	}
	template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusAssetType>()
	{
		return EOculusAssetType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOculusAssetType(EOculusAssetType_StaticEnum, TEXT("/Script/OculusEditor"), TEXT("EOculusAssetType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OculusEditor_EOculusAssetType_Hash() { return 840663925U; }
	UEnum* Z_Construct_UEnum_OculusEditor_EOculusAssetType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OculusEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOculusAssetType"), 0, Get_Z_Construct_UEnum_OculusEditor_EOculusAssetType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOculusAssetType::Default", (int64)EOculusAssetType::Default },
				{ "EOculusAssetType::Store", (int64)EOculusAssetType::Store },
				{ "EOculusAssetType::Language_Pack", (int64)EOculusAssetType::Language_Pack },
				{ "EOculusAssetType::Length", (int64)EOculusAssetType::Length },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Default.DisplayName", "Default" },
				{ "Default.Name", "EOculusAssetType::Default" },
				{ "Language_Pack.DisplayName", "Language Pack" },
				{ "Language_Pack.Name", "EOculusAssetType::Language_Pack" },
				{ "Length.DisplayName", "Invlaid" },
				{ "Length.Name", "EOculusAssetType::Length" },
				{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
				{ "Store.DisplayName", "Store" },
				{ "Store.Name", "EOculusAssetType::Store" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OculusEditor,
				nullptr,
				"EOculusAssetType",
				"EOculusAssetType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOculusGamepadEmulation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OculusEditor_EOculusGamepadEmulation, Z_Construct_UPackage__Script_OculusEditor(), TEXT("EOculusGamepadEmulation"));
		}
		return Singleton;
	}
	template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusGamepadEmulation>()
	{
		return EOculusGamepadEmulation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOculusGamepadEmulation(EOculusGamepadEmulation_StaticEnum, TEXT("/Script/OculusEditor"), TEXT("EOculusGamepadEmulation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OculusEditor_EOculusGamepadEmulation_Hash() { return 1591575565U; }
	UEnum* Z_Construct_UEnum_OculusEditor_EOculusGamepadEmulation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OculusEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOculusGamepadEmulation"), 0, Get_Z_Construct_UEnum_OculusEditor_EOculusGamepadEmulation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOculusGamepadEmulation::Off", (int64)EOculusGamepadEmulation::Off },
				{ "EOculusGamepadEmulation::Twinstick", (int64)EOculusGamepadEmulation::Twinstick },
				{ "EOculusGamepadEmulation::RightDPad", (int64)EOculusGamepadEmulation::RightDPad },
				{ "EOculusGamepadEmulation::LeftDPad", (int64)EOculusGamepadEmulation::LeftDPad },
				{ "EOculusGamepadEmulation::Length", (int64)EOculusGamepadEmulation::Length },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "LeftDPad.DisplayName", "Left D Pad" },
				{ "LeftDPad.Name", "EOculusGamepadEmulation::LeftDPad" },
				{ "Length.DisplayName", "Invalid" },
				{ "Length.Name", "EOculusGamepadEmulation::Length" },
				{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
				{ "Off.DisplayName", "Off" },
				{ "Off.Name", "EOculusGamepadEmulation::Off" },
				{ "RightDPad.DisplayName", "Right D Pad" },
				{ "RightDPad.Name", "EOculusGamepadEmulation::RightDPad" },
				{ "Twinstick.DisplayName", "Twinstick" },
				{ "Twinstick.Name", "EOculusGamepadEmulation::Twinstick" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OculusEditor,
				nullptr,
				"EOculusGamepadEmulation",
				"EOculusGamepadEmulation",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOculusPlatformTarget_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OculusEditor_EOculusPlatformTarget, Z_Construct_UPackage__Script_OculusEditor(), TEXT("EOculusPlatformTarget"));
		}
		return Singleton;
	}
	template<> OCULUSEDITOR_API UEnum* StaticEnum<EOculusPlatformTarget>()
	{
		return EOculusPlatformTarget_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOculusPlatformTarget(EOculusPlatformTarget_StaticEnum, TEXT("/Script/OculusEditor"), TEXT("EOculusPlatformTarget"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OculusEditor_EOculusPlatformTarget_Hash() { return 2333686934U; }
	UEnum* Z_Construct_UEnum_OculusEditor_EOculusPlatformTarget()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OculusEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOculusPlatformTarget"), 0, Get_Z_Construct_UEnum_OculusEditor_EOculusPlatformTarget_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOculusPlatformTarget::Rift", (int64)EOculusPlatformTarget::Rift },
				{ "EOculusPlatformTarget::Quest", (int64)EOculusPlatformTarget::Quest },
				{ "EOculusPlatformTarget::Length", (int64)EOculusPlatformTarget::Length },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Length.DisplayName", "Invalid" },
				{ "Length.Name", "EOculusPlatformTarget::Length" },
				{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
				{ "Quest.DisplayName", "Quest" },
				{ "Quest.Name", "EOculusPlatformTarget::Quest" },
				{ "Rift.DisplayName", "Rift" },
				{ "Rift.Name", "EOculusPlatformTarget::Rift" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OculusEditor,
				nullptr,
				"EOculusPlatformTarget",
				"EOculusPlatformTarget",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FAssetConfigArray::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OCULUSEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FAssetConfigArray_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAssetConfigArray, Z_Construct_UPackage__Script_OculusEditor(), TEXT("AssetConfigArray"), sizeof(FAssetConfigArray), Get_Z_Construct_UScriptStruct_FAssetConfigArray_Hash());
	}
	return Singleton;
}
template<> OCULUSEDITOR_API UScriptStruct* StaticStruct<FAssetConfigArray>()
{
	return FAssetConfigArray::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAssetConfigArray(FAssetConfigArray::StaticStruct, TEXT("/Script/OculusEditor"), TEXT("AssetConfigArray"), false, nullptr, nullptr);
static struct FScriptStruct_OculusEditor_StaticRegisterNativesFAssetConfigArray
{
	FScriptStruct_OculusEditor_StaticRegisterNativesFAssetConfigArray()
	{
		UScriptStruct::DeferCppStructOps<FAssetConfigArray>(FName(TEXT("AssetConfigArray")));
	}
} ScriptStruct_OculusEditor_StaticRegisterNativesFAssetConfigArray;
	struct Z_Construct_UScriptStruct_FAssetConfigArray_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConfigArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ConfigArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetConfigArray_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAssetConfigArray>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewProp_ConfigArray_Inner = { "ConfigArray", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAssetConfig, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewProp_ConfigArray_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewProp_ConfigArray = { "ConfigArray", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAssetConfigArray, ConfigArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewProp_ConfigArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewProp_ConfigArray_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAssetConfigArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewProp_ConfigArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetConfigArray_Statics::NewProp_ConfigArray,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAssetConfigArray_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusEditor,
		nullptr,
		&NewStructOps,
		"AssetConfigArray",
		sizeof(FAssetConfigArray),
		alignof(FAssetConfigArray),
		Z_Construct_UScriptStruct_FAssetConfigArray_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfigArray_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetConfigArray_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfigArray_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAssetConfigArray()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAssetConfigArray_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OculusEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AssetConfigArray"), sizeof(FAssetConfigArray), Get_Z_Construct_UScriptStruct_FAssetConfigArray_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAssetConfigArray_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAssetConfigArray_Hash() { return 2531778341U; }
class UScriptStruct* FAssetConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OCULUSEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FAssetConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAssetConfig, Z_Construct_UPackage__Script_OculusEditor(), TEXT("AssetConfig"), sizeof(FAssetConfig), Get_Z_Construct_UScriptStruct_FAssetConfig_Hash());
	}
	return Singleton;
}
template<> OCULUSEDITOR_API UScriptStruct* StaticStruct<FAssetConfig>()
{
	return FAssetConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAssetConfig(FAssetConfig::StaticStruct, TEXT("/Script/OculusEditor"), TEXT("AssetConfig"), false, nullptr, nullptr);
static struct FScriptStruct_OculusEditor_StaticRegisterNativesFAssetConfig
{
	FScriptStruct_OculusEditor_StaticRegisterNativesFAssetConfig()
	{
		UScriptStruct::DeferCppStructOps<FAssetConfig>(FName(TEXT("AssetConfig")));
	}
} ScriptStruct_OculusEditor_StaticRegisterNativesFAssetConfig;
	struct Z_Construct_UScriptStruct_FAssetConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AssetType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AssetType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Required_MetaData[];
#endif
		static void NewProp_Required_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Required;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sku_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Sku;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetConfig_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAssetConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAssetConfig>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_AssetType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_AssetType_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_AssetType = { "AssetType", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAssetConfig, AssetType), Z_Construct_UEnum_OculusEditor_EOculusAssetType, METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_AssetType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_AssetType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Required_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Required_SetBit(void* Obj)
	{
		((FAssetConfig*)Obj)->Required = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Required = { "Required", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FAssetConfig), &Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Required_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Required_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Required_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAssetConfig, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Sku_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Sku = { "Sku", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAssetConfig, Sku), METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Sku_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Sku_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAssetConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_AssetType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_AssetType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Required,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAssetConfig_Statics::NewProp_Sku,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAssetConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusEditor,
		nullptr,
		&NewStructOps,
		"AssetConfig",
		sizeof(FAssetConfig),
		alignof(FAssetConfig),
		Z_Construct_UScriptStruct_FAssetConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAssetConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAssetConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAssetConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAssetConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OculusEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AssetConfig"), sizeof(FAssetConfig), Get_Z_Construct_UScriptStruct_FAssetConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAssetConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAssetConfig_Hash() { return 2064051902U; }
class UScriptStruct* FRedistPackage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OCULUSEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FRedistPackage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FRedistPackage, Z_Construct_UPackage__Script_OculusEditor(), TEXT("RedistPackage"), sizeof(FRedistPackage), Get_Z_Construct_UScriptStruct_FRedistPackage_Hash());
	}
	return Singleton;
}
template<> OCULUSEDITOR_API UScriptStruct* StaticStruct<FRedistPackage>()
{
	return FRedistPackage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FRedistPackage(FRedistPackage::StaticStruct, TEXT("/Script/OculusEditor"), TEXT("RedistPackage"), false, nullptr, nullptr);
static struct FScriptStruct_OculusEditor_StaticRegisterNativesFRedistPackage
{
	FScriptStruct_OculusEditor_StaticRegisterNativesFRedistPackage()
	{
		UScriptStruct::DeferCppStructOps<FRedistPackage>(FName(TEXT("RedistPackage")));
	}
} ScriptStruct_OculusEditor_StaticRegisterNativesFRedistPackage;
	struct Z_Construct_UScriptStruct_FRedistPackage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Included_MetaData[];
#endif
		static void NewProp_Included_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Included;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Id;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRedistPackage_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FRedistPackage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FRedistPackage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Included_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Included_SetBit(void* Obj)
	{
		((FRedistPackage*)Obj)->Included = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Included = { "Included", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FRedistPackage), &Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Included_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Included_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Included_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRedistPackage, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Id_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Id = { "Id", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FRedistPackage, Id), METADATA_PARAMS(Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Id_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FRedistPackage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Included,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FRedistPackage_Statics::NewProp_Id,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FRedistPackage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusEditor,
		nullptr,
		&NewStructOps,
		"RedistPackage",
		sizeof(FRedistPackage),
		alignof(FRedistPackage),
		Z_Construct_UScriptStruct_FRedistPackage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRedistPackage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FRedistPackage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FRedistPackage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FRedistPackage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FRedistPackage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OculusEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("RedistPackage"), sizeof(FRedistPackage), Get_Z_Construct_UScriptStruct_FRedistPackage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FRedistPackage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FRedistPackage_Hash() { return 2173675024U; }
	void UOculusPlatformToolSettings::StaticRegisterNativesUOculusPlatformToolSettings()
	{
	}
	UClass* Z_Construct_UClass_UOculusPlatformToolSettings_NoRegister()
	{
		return UOculusPlatformToolSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOculusPlatformToolSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRiftBuildDirectory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusRiftBuildDirectory;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRiftBuildVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusRiftBuildVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRiftLaunchParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusRiftLaunchParams;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRiftFireWallException_MetaData[];
#endif
		static void NewProp_OculusRiftFireWallException_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_OculusRiftFireWallException;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRift2DLaunchPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusRift2DLaunchPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRift2DLaunchParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusRift2DLaunchParams;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OculusRedistPackages_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRedistPackages_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusRedistPackages;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UploadDebugSymbols_MetaData[];
#endif
		static void NewProp_UploadDebugSymbols_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_UploadDebugSymbols;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugSymbolsOnly_MetaData[];
#endif
		static void NewProp_DebugSymbolsOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_DebugSymbolsOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuildID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BuildID;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OculusTargetPlatform_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusTargetPlatform_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OculusTargetPlatform;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusApplicationID_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusApplicationID_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusApplicationID;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusApplicationToken_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusApplicationToken_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusApplicationToken;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusReleaseChannel_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusReleaseChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusReleaseChannel;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusReleaseNote_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusReleaseNote_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusReleaseNote;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusLaunchFilePath_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusLaunchFilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusLaunchFilePath;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OculusRiftGamepadEmulation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusRiftGamepadEmulation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OculusRiftGamepadEmulation;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusLanguagePacksPath_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusLanguagePacksPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusLanguagePacksPath;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusExpansionFilesPath_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusExpansionFilesPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusExpansionFilesPath;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OculusSymbolDirPath_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusSymbolDirPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusSymbolDirPath;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OculusAssetConfigs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusAssetConfigs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OculusAssetConfigs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusPlatformToolSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "OculusPlatformToolSettings.h" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildDirectory_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildDirectory = { "OculusRiftBuildDirectory", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRiftBuildDirectory), METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildDirectory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildDirectory_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildVersion_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildVersion = { "OculusRiftBuildVersion", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRiftBuildVersion), METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftLaunchParams_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftLaunchParams = { "OculusRiftLaunchParams", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRiftLaunchParams), METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftLaunchParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftLaunchParams_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftFireWallException_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftFireWallException_SetBit(void* Obj)
	{
		((UOculusPlatformToolSettings*)Obj)->OculusRiftFireWallException = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftFireWallException = { "OculusRiftFireWallException", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOculusPlatformToolSettings), &Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftFireWallException_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftFireWallException_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftFireWallException_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchPath_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchPath = { "OculusRift2DLaunchPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRift2DLaunchPath), METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchParams_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchParams = { "OculusRift2DLaunchParams", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRift2DLaunchParams), METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchParams_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRedistPackages_Inner = { "OculusRedistPackages", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FRedistPackage, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRedistPackages_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRedistPackages = { "OculusRedistPackages", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRedistPackages), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRedistPackages_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRedistPackages_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_UploadDebugSymbols_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_UploadDebugSymbols_SetBit(void* Obj)
	{
		((UOculusPlatformToolSettings*)Obj)->UploadDebugSymbols = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_UploadDebugSymbols = { "UploadDebugSymbols", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOculusPlatformToolSettings), &Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_UploadDebugSymbols_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_UploadDebugSymbols_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_UploadDebugSymbols_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_DebugSymbolsOnly_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	void Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_DebugSymbolsOnly_SetBit(void* Obj)
	{
		((UOculusPlatformToolSettings*)Obj)->DebugSymbolsOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_DebugSymbolsOnly = { "DebugSymbolsOnly", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOculusPlatformToolSettings), &Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_DebugSymbolsOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_DebugSymbolsOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_DebugSymbolsOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_BuildID_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_BuildID = { "BuildID", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, BuildID), METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_BuildID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_BuildID_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusTargetPlatform_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusTargetPlatform_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusTargetPlatform = { "OculusTargetPlatform", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusTargetPlatform), Z_Construct_UEnum_OculusEditor_EOculusPlatformTarget, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusTargetPlatform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusTargetPlatform_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationID_Inner = { "OculusApplicationID", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationID_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationID = { "OculusApplicationID", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusApplicationID), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationID_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationToken_Inner = { "OculusApplicationToken", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationToken_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationToken = { "OculusApplicationToken", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusApplicationToken), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationToken_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationToken_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseChannel_Inner = { "OculusReleaseChannel", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseChannel_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseChannel = { "OculusReleaseChannel", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusReleaseChannel), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseChannel_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseNote_Inner = { "OculusReleaseNote", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseNote_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseNote = { "OculusReleaseNote", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusReleaseNote), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseNote_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseNote_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLaunchFilePath_Inner = { "OculusLaunchFilePath", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLaunchFilePath_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLaunchFilePath = { "OculusLaunchFilePath", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusLaunchFilePath), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLaunchFilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLaunchFilePath_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftGamepadEmulation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftGamepadEmulation_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftGamepadEmulation = { "OculusRiftGamepadEmulation", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusRiftGamepadEmulation), Z_Construct_UEnum_OculusEditor_EOculusGamepadEmulation, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftGamepadEmulation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftGamepadEmulation_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLanguagePacksPath_Inner = { "OculusLanguagePacksPath", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLanguagePacksPath_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLanguagePacksPath = { "OculusLanguagePacksPath", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusLanguagePacksPath), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLanguagePacksPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLanguagePacksPath_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusExpansionFilesPath_Inner = { "OculusExpansionFilesPath", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusExpansionFilesPath_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusExpansionFilesPath = { "OculusExpansionFilesPath", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusExpansionFilesPath), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusExpansionFilesPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusExpansionFilesPath_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusSymbolDirPath_Inner = { "OculusSymbolDirPath", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusSymbolDirPath_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusSymbolDirPath = { "OculusSymbolDirPath", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusSymbolDirPath), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusSymbolDirPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusSymbolDirPath_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusAssetConfigs_Inner = { "OculusAssetConfigs", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAssetConfigArray, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusAssetConfigs_MetaData[] = {
		{ "Category", "Oculus" },
		{ "ModuleRelativePath", "Public/OculusPlatformToolSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusAssetConfigs = { "OculusAssetConfigs", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusPlatformToolSettings, OculusAssetConfigs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusAssetConfigs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusAssetConfigs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusPlatformToolSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildDirectory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftBuildVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftLaunchParams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftFireWallException,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRift2DLaunchParams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRedistPackages_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRedistPackages,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_UploadDebugSymbols,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_DebugSymbolsOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_BuildID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusTargetPlatform_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusTargetPlatform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationID_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationToken_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusApplicationToken,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseChannel_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseNote_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusReleaseNote,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLaunchFilePath_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLaunchFilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftGamepadEmulation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusRiftGamepadEmulation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLanguagePacksPath_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusLanguagePacksPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusExpansionFilesPath_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusExpansionFilesPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusSymbolDirPath_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusSymbolDirPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusAssetConfigs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusPlatformToolSettings_Statics::NewProp_OculusAssetConfigs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusPlatformToolSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusPlatformToolSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusPlatformToolSettings_Statics::ClassParams = {
		&UOculusPlatformToolSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOculusPlatformToolSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusPlatformToolSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusPlatformToolSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusPlatformToolSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusPlatformToolSettings, 2705957558);
	template<> OCULUSEDITOR_API UClass* StaticClass<UOculusPlatformToolSettings>()
	{
		return UOculusPlatformToolSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusPlatformToolSettings(Z_Construct_UClass_UOculusPlatformToolSettings, &UOculusPlatformToolSettings::StaticClass, TEXT("/Script/OculusEditor"), TEXT("UOculusPlatformToolSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusPlatformToolSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
