// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusAudio/Public/OculusAudioSourceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusAudioSourceSettings() {}
// Cross Module References
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioSourceSettings_NoRegister();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioSourceSettings();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USpatializationPluginSourceSettingsBase();
	UPackage* Z_Construct_UPackage__Script_OculusAudio();
// End Cross Module References
	void UOculusAudioSourceSettings::StaticRegisterNativesUOculusAudioSourceSettings()
	{
	}
	UClass* Z_Construct_UClass_UOculusAudioSourceSettings_NoRegister()
	{
		return UOculusAudioSourceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOculusAudioSourceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EarlyReflectionsEnabled_MetaData[];
#endif
		static void NewProp_EarlyReflectionsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_EarlyReflectionsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttenuationEnabled_MetaData[];
#endif
		static void NewProp_AttenuationEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AttenuationEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttenuationRangeMinimum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AttenuationRangeMinimum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttenuationRangeMaximum_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AttenuationRangeMaximum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumetricRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VolumetricRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReverbSendLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReverbSendLevel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusAudioSourceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USpatializationPluginSourceSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OculusAudioSourceSettings.h" },
		{ "ModuleRelativePath", "Public/OculusAudioSourceSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_EarlyReflectionsEnabled_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "ModuleRelativePath", "Public/OculusAudioSourceSettings.h" },
	};
#endif
	void Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_EarlyReflectionsEnabled_SetBit(void* Obj)
	{
		((UOculusAudioSourceSettings*)Obj)->EarlyReflectionsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_EarlyReflectionsEnabled = { "EarlyReflectionsEnabled", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOculusAudioSourceSettings), &Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_EarlyReflectionsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_EarlyReflectionsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_EarlyReflectionsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationEnabled_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "ModuleRelativePath", "Public/OculusAudioSourceSettings.h" },
	};
#endif
	void Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationEnabled_SetBit(void* Obj)
	{
		((UOculusAudioSourceSettings*)Obj)->AttenuationEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationEnabled = { "AttenuationEnabled", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOculusAudioSourceSettings), &Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMinimum_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "ModuleRelativePath", "Public/OculusAudioSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMinimum = { "AttenuationRangeMinimum", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusAudioSourceSettings, AttenuationRangeMinimum), METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMinimum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMinimum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMaximum_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "ModuleRelativePath", "Public/OculusAudioSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMaximum = { "AttenuationRangeMaximum", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusAudioSourceSettings, AttenuationRangeMaximum), METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMaximum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMaximum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_VolumetricRadius_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "ModuleRelativePath", "Public/OculusAudioSourceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_VolumetricRadius = { "VolumetricRadius", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusAudioSourceSettings, VolumetricRadius), METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_VolumetricRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_VolumetricRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_ReverbSendLevel_MetaData[] = {
		{ "Category", "SpatializationSettings" },
		{ "ClampMax", "20.0" },
		{ "ClampMin", "-60.0" },
		{ "ModuleRelativePath", "Public/OculusAudioSourceSettings.h" },
		{ "UIMax", "20.0" },
		{ "UIMin", "-60.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_ReverbSendLevel = { "ReverbSendLevel", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusAudioSourceSettings, ReverbSendLevel), METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_ReverbSendLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_ReverbSendLevel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusAudioSourceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_EarlyReflectionsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMinimum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_AttenuationRangeMaximum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_VolumetricRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSourceSettings_Statics::NewProp_ReverbSendLevel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusAudioSourceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusAudioSourceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusAudioSourceSettings_Statics::ClassParams = {
		&UOculusAudioSourceSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOculusAudioSourceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::PropPointers),
		0,
		0x001010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusAudioSourceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusAudioSourceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusAudioSourceSettings, 322085908);
	template<> OCULUSAUDIO_API UClass* StaticClass<UOculusAudioSourceSettings>()
	{
		return UOculusAudioSourceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusAudioSourceSettings(Z_Construct_UClass_UOculusAudioSourceSettings, &UOculusAudioSourceSettings::StaticClass, TEXT("/Script/OculusAudio"), TEXT("UOculusAudioSourceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusAudioSourceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
