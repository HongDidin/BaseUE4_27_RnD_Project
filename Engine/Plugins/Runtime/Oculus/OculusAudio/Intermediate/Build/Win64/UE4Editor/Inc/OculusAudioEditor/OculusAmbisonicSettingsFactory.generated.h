// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSAUDIOEDITOR_OculusAmbisonicSettingsFactory_generated_h
#error "OculusAmbisonicSettingsFactory.generated.h already included, missing '#pragma once' in OculusAmbisonicSettingsFactory.h"
#endif
#define OCULUSAUDIOEDITOR_OculusAmbisonicSettingsFactory_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusAmbisonicsSettingsFactory(); \
	friend struct Z_Construct_UClass_UOculusAmbisonicsSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UOculusAmbisonicsSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusAudioEditor"), OCULUSAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UOculusAmbisonicsSettingsFactory)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUOculusAmbisonicsSettingsFactory(); \
	friend struct Z_Construct_UClass_UOculusAmbisonicsSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UOculusAmbisonicsSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusAudioEditor"), OCULUSAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UOculusAmbisonicsSettingsFactory)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OCULUSAUDIOEDITOR_API UOculusAmbisonicsSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAmbisonicsSettingsFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCULUSAUDIOEDITOR_API, UOculusAmbisonicsSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAmbisonicsSettingsFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCULUSAUDIOEDITOR_API UOculusAmbisonicsSettingsFactory(UOculusAmbisonicsSettingsFactory&&); \
	OCULUSAUDIOEDITOR_API UOculusAmbisonicsSettingsFactory(const UOculusAmbisonicsSettingsFactory&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OCULUSAUDIOEDITOR_API UOculusAmbisonicsSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCULUSAUDIOEDITOR_API UOculusAmbisonicsSettingsFactory(UOculusAmbisonicsSettingsFactory&&); \
	OCULUSAUDIOEDITOR_API UOculusAmbisonicsSettingsFactory(const UOculusAmbisonicsSettingsFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCULUSAUDIOEDITOR_API, UOculusAmbisonicsSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAmbisonicsSettingsFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAmbisonicsSettingsFactory)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_21_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h_24_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OculusAmbisonicsSettingsFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSAUDIOEDITOR_API UClass* StaticClass<class UOculusAmbisonicsSettingsFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAmbisonicSettingsFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
