// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusAudio/Private/OculusAudioGeometryLandscape.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusAudioGeometryLandscape() {}
// Cross Module References
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioGeometryLandscape_NoRegister();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioGeometryLandscape();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioMaterialComponent();
	UPackage* Z_Construct_UPackage__Script_OculusAudio();
// End Cross Module References
	void UOculusAudioGeometryLandscape::StaticRegisterNativesUOculusAudioGeometryLandscape()
	{
	}
	UClass* Z_Construct_UClass_UOculusAudioGeometryLandscape_NoRegister()
	{
		return UOculusAudioGeometryLandscape::StaticClass();
	}
	struct Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOculusAudioMaterialComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Audio" },
		{ "HideCategories", "Activation Collision Cooking Activation Collision Cooking" },
		{ "IncludePath", "OculusAudioGeometryLandscape.h" },
		{ "ModuleRelativePath", "Private/OculusAudioGeometryLandscape.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusAudioGeometryLandscape>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics::ClassParams = {
		&UOculusAudioGeometryLandscape::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusAudioGeometryLandscape()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusAudioGeometryLandscape_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusAudioGeometryLandscape, 4165063278);
	template<> OCULUSAUDIO_API UClass* StaticClass<UOculusAudioGeometryLandscape>()
	{
		return UOculusAudioGeometryLandscape::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusAudioGeometryLandscape(Z_Construct_UClass_UOculusAudioGeometryLandscape, &UOculusAudioGeometryLandscape::StaticClass, TEXT("/Script/OculusAudio"), TEXT("UOculusAudioGeometryLandscape"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusAudioGeometryLandscape);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UOculusAudioGeometryLandscape)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
