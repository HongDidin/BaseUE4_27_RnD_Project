// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSAUDIO_OculusAudioGeometryComponent_generated_h
#error "OculusAudioGeometryComponent.generated.h already included, missing '#pragma once' in OculusAudioGeometryComponent.h"
#endif
#define OCULUSAUDIO_OculusAudioGeometryComponent_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UOculusAudioGeometryComponent, NO_API)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusAudioGeometryComponent(); \
	friend struct Z_Construct_UClass_UOculusAudioGeometryComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioGeometryComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioGeometryComponent) \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUOculusAudioGeometryComponent(); \
	friend struct Z_Construct_UClass_UOculusAudioGeometryComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioGeometryComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioGeometryComponent) \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusAudioGeometryComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAudioGeometryComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioGeometryComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioGeometryComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioGeometryComponent(UOculusAudioGeometryComponent&&); \
	NO_API UOculusAudioGeometryComponent(const UOculusAudioGeometryComponent&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioGeometryComponent(UOculusAudioGeometryComponent&&); \
	NO_API UOculusAudioGeometryComponent(const UOculusAudioGeometryComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioGeometryComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioGeometryComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOculusAudioGeometryComponent)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__IncludeChildren() { return STRUCT_OFFSET(UOculusAudioGeometryComponent, IncludeChildren); }


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_11_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSAUDIO_API UClass* StaticClass<class UOculusAudioGeometryComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
