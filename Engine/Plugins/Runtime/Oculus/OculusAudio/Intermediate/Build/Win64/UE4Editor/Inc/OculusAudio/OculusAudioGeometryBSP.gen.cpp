// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusAudio/Private/OculusAudioGeometryBSP.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusAudioGeometryBSP() {}
// Cross Module References
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioGeometryBSP_NoRegister();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioGeometryBSP();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioMaterialComponent();
	UPackage* Z_Construct_UPackage__Script_OculusAudio();
// End Cross Module References
	void UOculusAudioGeometryBSP::StaticRegisterNativesUOculusAudioGeometryBSP()
	{
	}
	UClass* Z_Construct_UClass_UOculusAudioGeometryBSP_NoRegister()
	{
		return UOculusAudioGeometryBSP::StaticClass();
	}
	struct Z_Construct_UClass_UOculusAudioGeometryBSP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusAudioGeometryBSP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UOculusAudioMaterialComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioGeometryBSP_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Audio" },
		{ "HideCategories", "Activation Collision Cooking Activation Collision Cooking" },
		{ "IncludePath", "OculusAudioGeometryBSP.h" },
		{ "ModuleRelativePath", "Private/OculusAudioGeometryBSP.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusAudioGeometryBSP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusAudioGeometryBSP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusAudioGeometryBSP_Statics::ClassParams = {
		&UOculusAudioGeometryBSP::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusAudioGeometryBSP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioGeometryBSP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusAudioGeometryBSP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusAudioGeometryBSP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusAudioGeometryBSP, 1766543705);
	template<> OCULUSAUDIO_API UClass* StaticClass<UOculusAudioGeometryBSP>()
	{
		return UOculusAudioGeometryBSP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusAudioGeometryBSP(Z_Construct_UClass_UOculusAudioGeometryBSP, &UOculusAudioGeometryBSP::StaticClass, TEXT("/Script/OculusAudio"), TEXT("UOculusAudioGeometryBSP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusAudioGeometryBSP);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UOculusAudioGeometryBSP)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
