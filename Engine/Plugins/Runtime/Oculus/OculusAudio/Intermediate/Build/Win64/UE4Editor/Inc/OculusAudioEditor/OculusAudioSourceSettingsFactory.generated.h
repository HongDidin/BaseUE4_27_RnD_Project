// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSAUDIOEDITOR_OculusAudioSourceSettingsFactory_generated_h
#error "OculusAudioSourceSettingsFactory.generated.h already included, missing '#pragma once' in OculusAudioSourceSettingsFactory.h"
#endif
#define OCULUSAUDIOEDITOR_OculusAudioSourceSettingsFactory_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusAudioSourceSettingsFactory(); \
	friend struct Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioSourceSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusAudioEditor"), OCULUSAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UOculusAudioSourceSettingsFactory)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUOculusAudioSourceSettingsFactory(); \
	friend struct Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioSourceSettingsFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusAudioEditor"), OCULUSAUDIOEDITOR_API) \
	DECLARE_SERIALIZER(UOculusAudioSourceSettingsFactory)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OCULUSAUDIOEDITOR_API UOculusAudioSourceSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAudioSourceSettingsFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCULUSAUDIOEDITOR_API, UOculusAudioSourceSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioSourceSettingsFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCULUSAUDIOEDITOR_API UOculusAudioSourceSettingsFactory(UOculusAudioSourceSettingsFactory&&); \
	OCULUSAUDIOEDITOR_API UOculusAudioSourceSettingsFactory(const UOculusAudioSourceSettingsFactory&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	OCULUSAUDIOEDITOR_API UOculusAudioSourceSettingsFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	OCULUSAUDIOEDITOR_API UOculusAudioSourceSettingsFactory(UOculusAudioSourceSettingsFactory&&); \
	OCULUSAUDIOEDITOR_API UOculusAudioSourceSettingsFactory(const UOculusAudioSourceSettingsFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(OCULUSAUDIOEDITOR_API, UOculusAudioSourceSettingsFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioSourceSettingsFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAudioSourceSettingsFactory)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_21_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h_24_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OculusAudioSourceSettingsFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSAUDIOEDITOR_API UClass* StaticClass<class UOculusAudioSourceSettingsFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudioEditor_Private_OculusAudioSourceSettingsFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
