// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSAUDIO_OculusAudioGeometryBSP_generated_h
#error "OculusAudioGeometryBSP.generated.h already included, missing '#pragma once' in OculusAudioGeometryBSP.h"
#endif
#define OCULUSAUDIO_OculusAudioGeometryBSP_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UOculusAudioGeometryBSP, NO_API)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusAudioGeometryBSP(); \
	friend struct Z_Construct_UClass_UOculusAudioGeometryBSP_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioGeometryBSP, UOculusAudioMaterialComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioGeometryBSP) \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUOculusAudioGeometryBSP(); \
	friend struct Z_Construct_UClass_UOculusAudioGeometryBSP_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioGeometryBSP, UOculusAudioMaterialComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioGeometryBSP) \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_ARCHIVESERIALIZER


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusAudioGeometryBSP(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAudioGeometryBSP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioGeometryBSP); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioGeometryBSP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioGeometryBSP(UOculusAudioGeometryBSP&&); \
	NO_API UOculusAudioGeometryBSP(const UOculusAudioGeometryBSP&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusAudioGeometryBSP() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioGeometryBSP(UOculusAudioGeometryBSP&&); \
	NO_API UOculusAudioGeometryBSP(const UOculusAudioGeometryBSP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioGeometryBSP); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioGeometryBSP); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOculusAudioGeometryBSP)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_14_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSAUDIO_API UClass* StaticClass<class UOculusAudioGeometryBSP>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioGeometryBSP_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
