// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSAUDIO_OculusAmbisonicsSettings_generated_h
#error "OculusAmbisonicsSettings.generated.h already included, missing '#pragma once' in OculusAmbisonicsSettings.h"
#endif
#define OCULUSAUDIO_OculusAmbisonicsSettings_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> OCULUSAUDIO_API UScriptStruct* StaticStruct<struct FSubmixEffectOculusAmbisonicSpatializerSettings>();

#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusAudioSoundfieldSettings(); \
	friend struct Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioSoundfieldSettings, USoundfieldEncodingSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioSoundfieldSettings)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_INCLASS \
private: \
	static void StaticRegisterNativesUOculusAudioSoundfieldSettings(); \
	friend struct Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioSoundfieldSettings, USoundfieldEncodingSettingsBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioSoundfieldSettings)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusAudioSoundfieldSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAudioSoundfieldSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioSoundfieldSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioSoundfieldSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioSoundfieldSettings(UOculusAudioSoundfieldSettings&&); \
	NO_API UOculusAudioSoundfieldSettings(const UOculusAudioSoundfieldSettings&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusAudioSoundfieldSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioSoundfieldSettings(UOculusAudioSoundfieldSettings&&); \
	NO_API UOculusAudioSoundfieldSettings(const UOculusAudioSoundfieldSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioSoundfieldSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioSoundfieldSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAudioSoundfieldSettings)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_58_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h_61_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSAUDIO_API UClass* StaticClass<class UOculusAudioSoundfieldSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Public_OculusAmbisonicsSettings_h


#define FOREACH_ENUM_EOCULUSAUDIOAMBISONICSMODE(op) \
	op(EOculusAudioAmbisonicsMode::SphericalHarmonics) \
	op(EOculusAudioAmbisonicsMode::VirtualSpeakers) 

enum class EOculusAudioAmbisonicsMode : uint8;
template<> OCULUSAUDIO_API UEnum* StaticEnum<EOculusAudioAmbisonicsMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
