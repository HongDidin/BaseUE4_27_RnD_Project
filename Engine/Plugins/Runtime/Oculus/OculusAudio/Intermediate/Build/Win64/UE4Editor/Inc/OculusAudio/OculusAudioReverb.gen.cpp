// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusAudio/Private/OculusAudioReverb.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusAudioReverb() {}
// Cross Module References
	OCULUSAUDIO_API UScriptStruct* Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings();
	UPackage* Z_Construct_UPackage__Script_OculusAudio();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_NoRegister();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset();
	ENGINE_API UClass* Z_Construct_UClass_USoundEffectSubmixPreset();
// End Cross Module References
class UScriptStruct* FSubmixEffectOculusReverbPluginSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OCULUSAUDIO_API uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings, Z_Construct_UPackage__Script_OculusAudio(), TEXT("SubmixEffectOculusReverbPluginSettings"), sizeof(FSubmixEffectOculusReverbPluginSettings), Get_Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Hash());
	}
	return Singleton;
}
template<> OCULUSAUDIO_API UScriptStruct* StaticStruct<FSubmixEffectOculusReverbPluginSettings>()
{
	return FSubmixEffectOculusReverbPluginSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings(FSubmixEffectOculusReverbPluginSettings::StaticStruct, TEXT("/Script/OculusAudio"), TEXT("SubmixEffectOculusReverbPluginSettings"), false, nullptr, nullptr);
static struct FScriptStruct_OculusAudio_StaticRegisterNativesFSubmixEffectOculusReverbPluginSettings
{
	FScriptStruct_OculusAudio_StaticRegisterNativesFSubmixEffectOculusReverbPluginSettings()
	{
		UScriptStruct::DeferCppStructOps<FSubmixEffectOculusReverbPluginSettings>(FName(TEXT("SubmixEffectOculusReverbPluginSettings")));
	}
} ScriptStruct_OculusAudio_StaticRegisterNativesFSubmixEffectOculusReverbPluginSettings;
	struct Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OculusAudioReverb.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSubmixEffectOculusReverbPluginSettings>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
		nullptr,
		&NewStructOps,
		"SubmixEffectOculusReverbPluginSettings",
		sizeof(FSubmixEffectOculusReverbPluginSettings),
		alignof(FSubmixEffectOculusReverbPluginSettings),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OculusAudio();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SubmixEffectOculusReverbPluginSettings"), sizeof(FSubmixEffectOculusReverbPluginSettings), Get_Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings_Hash() { return 2493680864U; }
	DEFINE_FUNCTION(USubmixEffectOculusReverbPluginPreset::execSetSettings)
	{
		P_GET_STRUCT_REF(FSubmixEffectOculusReverbPluginSettings,Z_Param_Out_InSettings);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSettings(Z_Param_Out_InSettings);
		P_NATIVE_END;
	}
	void USubmixEffectOculusReverbPluginPreset::StaticRegisterNativesUSubmixEffectOculusReverbPluginPreset()
	{
		UClass* Class = USubmixEffectOculusReverbPluginPreset::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetSettings", &USubmixEffectOculusReverbPluginPreset::execSetSettings },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics
	{
		struct SubmixEffectOculusReverbPluginPreset_eventSetSettings_Parms
		{
			FSubmixEffectOculusReverbPluginSettings InSettings;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::NewProp_InSettings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::NewProp_InSettings = { "InSettings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SubmixEffectOculusReverbPluginPreset_eventSetSettings_Parms, InSettings), Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings, METADATA_PARAMS(Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::NewProp_InSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::NewProp_InSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::NewProp_InSettings,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OculusAudioReverb.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset, nullptr, "SetSettings", nullptr, nullptr, sizeof(SubmixEffectOculusReverbPluginPreset_eventSetSettings_Parms), Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_NoRegister()
	{
		return USubmixEffectOculusReverbPluginPreset::StaticClass();
	}
	struct Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundEffectSubmixPreset,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USubmixEffectOculusReverbPluginPreset_SetSettings, "SetSettings" }, // 2605284122
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "OculusAudioReverb.h" },
		{ "ModuleRelativePath", "Private/OculusAudioReverb.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Private/OculusAudioReverb.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USubmixEffectOculusReverbPluginPreset, Settings), Z_Construct_UScriptStruct_FSubmixEffectOculusReverbPluginSettings, METADATA_PARAMS(Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::NewProp_Settings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USubmixEffectOculusReverbPluginPreset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::ClassParams = {
		&USubmixEffectOculusReverbPluginPreset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USubmixEffectOculusReverbPluginPreset, 1069239646);
	template<> OCULUSAUDIO_API UClass* StaticClass<USubmixEffectOculusReverbPluginPreset>()
	{
		return USubmixEffectOculusReverbPluginPreset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USubmixEffectOculusReverbPluginPreset(Z_Construct_UClass_USubmixEffectOculusReverbPluginPreset, &USubmixEffectOculusReverbPluginPreset::StaticClass, TEXT("/Script/OculusAudio"), TEXT("USubmixEffectOculusReverbPluginPreset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USubmixEffectOculusReverbPluginPreset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
