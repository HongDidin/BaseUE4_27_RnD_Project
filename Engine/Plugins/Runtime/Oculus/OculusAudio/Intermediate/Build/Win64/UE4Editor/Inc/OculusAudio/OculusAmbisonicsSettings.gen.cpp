// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusAudio/Public/OculusAmbisonicsSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusAmbisonicsSettings() {}
// Cross Module References
	OCULUSAUDIO_API UEnum* Z_Construct_UEnum_OculusAudio_EOculusAudioAmbisonicsMode();
	UPackage* Z_Construct_UPackage__Script_OculusAudio();
	OCULUSAUDIO_API UScriptStruct* Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioSoundfieldSettings_NoRegister();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioSoundfieldSettings();
	AUDIOEXTENSIONS_API UClass* Z_Construct_UClass_USoundfieldEncodingSettingsBase();
// End Cross Module References
	static UEnum* EOculusAudioAmbisonicsMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_OculusAudio_EOculusAudioAmbisonicsMode, Z_Construct_UPackage__Script_OculusAudio(), TEXT("EOculusAudioAmbisonicsMode"));
		}
		return Singleton;
	}
	template<> OCULUSAUDIO_API UEnum* StaticEnum<EOculusAudioAmbisonicsMode>()
	{
		return EOculusAudioAmbisonicsMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOculusAudioAmbisonicsMode(EOculusAudioAmbisonicsMode_StaticEnum, TEXT("/Script/OculusAudio"), TEXT("EOculusAudioAmbisonicsMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_OculusAudio_EOculusAudioAmbisonicsMode_Hash() { return 411782615U; }
	UEnum* Z_Construct_UEnum_OculusAudio_EOculusAudioAmbisonicsMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_OculusAudio();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOculusAudioAmbisonicsMode"), 0, Get_Z_Construct_UEnum_OculusAudio_EOculusAudioAmbisonicsMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOculusAudioAmbisonicsMode::SphericalHarmonics", (int64)EOculusAudioAmbisonicsMode::SphericalHarmonics },
				{ "EOculusAudioAmbisonicsMode::VirtualSpeakers", (int64)EOculusAudioAmbisonicsMode::VirtualSpeakers },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/OculusAmbisonicsSettings.h" },
				{ "SphericalHarmonics.Comment", "// High quality ambisonic spatialization method\n" },
				{ "SphericalHarmonics.Name", "EOculusAudioAmbisonicsMode::SphericalHarmonics" },
				{ "SphericalHarmonics.ToolTip", "High quality ambisonic spatialization method" },
				{ "VirtualSpeakers.Comment", "// Alternative ambisonic spatialization method\n" },
				{ "VirtualSpeakers.Name", "EOculusAudioAmbisonicsMode::VirtualSpeakers" },
				{ "VirtualSpeakers.ToolTip", "Alternative ambisonic spatialization method" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_OculusAudio,
				nullptr,
				"EOculusAudioAmbisonicsMode",
				"EOculusAudioAmbisonicsMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FSubmixEffectOculusAmbisonicSpatializerSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OCULUSAUDIO_API uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings, Z_Construct_UPackage__Script_OculusAudio(), TEXT("SubmixEffectOculusAmbisonicSpatializerSettings"), sizeof(FSubmixEffectOculusAmbisonicSpatializerSettings), Get_Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Hash());
	}
	return Singleton;
}
template<> OCULUSAUDIO_API UScriptStruct* StaticStruct<FSubmixEffectOculusAmbisonicSpatializerSettings>()
{
	return FSubmixEffectOculusAmbisonicSpatializerSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings(FSubmixEffectOculusAmbisonicSpatializerSettings::StaticStruct, TEXT("/Script/OculusAudio"), TEXT("SubmixEffectOculusAmbisonicSpatializerSettings"), false, nullptr, nullptr);
static struct FScriptStruct_OculusAudio_StaticRegisterNativesFSubmixEffectOculusAmbisonicSpatializerSettings
{
	FScriptStruct_OculusAudio_StaticRegisterNativesFSubmixEffectOculusAmbisonicSpatializerSettings()
	{
		UScriptStruct::DeferCppStructOps<FSubmixEffectOculusAmbisonicSpatializerSettings>(FName(TEXT("SubmixEffectOculusAmbisonicSpatializerSettings")));
	}
} ScriptStruct_OculusAudio_StaticRegisterNativesFSubmixEffectOculusAmbisonicSpatializerSettings;
	struct Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AmbisonicMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AmbisonicMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AmbisonicMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OculusAmbisonicsSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSubmixEffectOculusAmbisonicSpatializerSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewProp_AmbisonicMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewProp_AmbisonicMode_MetaData[] = {
		{ "Category", "Realtime" },
		{ "Comment", "// Ambisonic spatialization mode\n" },
		{ "ModuleRelativePath", "Public/OculusAmbisonicsSettings.h" },
		{ "ToolTip", "Ambisonic spatialization mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewProp_AmbisonicMode = { "AmbisonicMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSubmixEffectOculusAmbisonicSpatializerSettings, AmbisonicMode), Z_Construct_UEnum_OculusAudio_EOculusAudioAmbisonicsMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewProp_AmbisonicMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewProp_AmbisonicMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewProp_AmbisonicMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::NewProp_AmbisonicMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
		nullptr,
		&NewStructOps,
		"SubmixEffectOculusAmbisonicSpatializerSettings",
		sizeof(FSubmixEffectOculusAmbisonicSpatializerSettings),
		alignof(FSubmixEffectOculusAmbisonicSpatializerSettings),
		Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OculusAudio();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SubmixEffectOculusAmbisonicSpatializerSettings"), sizeof(FSubmixEffectOculusAmbisonicSpatializerSettings), Get_Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSubmixEffectOculusAmbisonicSpatializerSettings_Hash() { return 1055616697U; }
	void UOculusAudioSoundfieldSettings::StaticRegisterNativesUOculusAudioSoundfieldSettings()
	{
	}
	UClass* Z_Construct_UClass_UOculusAudioSoundfieldSettings_NoRegister()
	{
		return UOculusAudioSoundfieldSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SpatializationMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpatializationMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SpatializationMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USoundfieldEncodingSettingsBase,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OculusAmbisonicsSettings.h" },
		{ "ModuleRelativePath", "Public/OculusAmbisonicsSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::NewProp_SpatializationMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::NewProp_SpatializationMode_MetaData[] = {
		{ "Category", "Ambisonics" },
		{ "ModuleRelativePath", "Public/OculusAmbisonicsSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::NewProp_SpatializationMode = { "SpatializationMode", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOculusAudioSoundfieldSettings, SpatializationMode), Z_Construct_UEnum_OculusAudio_EOculusAudioAmbisonicsMode, METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::NewProp_SpatializationMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::NewProp_SpatializationMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::NewProp_SpatializationMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::NewProp_SpatializationMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusAudioSoundfieldSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::ClassParams = {
		&UOculusAudioSoundfieldSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusAudioSoundfieldSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusAudioSoundfieldSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusAudioSoundfieldSettings, 1749815499);
	template<> OCULUSAUDIO_API UClass* StaticClass<UOculusAudioSoundfieldSettings>()
	{
		return UOculusAudioSoundfieldSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusAudioSoundfieldSettings(Z_Construct_UClass_UOculusAudioSoundfieldSettings, &UOculusAudioSoundfieldSettings::StaticClass, TEXT("/Script/OculusAudio"), TEXT("UOculusAudioSoundfieldSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusAudioSoundfieldSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
