// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusAudioEditor/Private/OculusAudioSourceSettingsFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusAudioSourceSettingsFactory() {}
// Cross Module References
	OCULUSAUDIOEDITOR_API UClass* Z_Construct_UClass_UOculusAudioSourceSettingsFactory_NoRegister();
	OCULUSAUDIOEDITOR_API UClass* Z_Construct_UClass_UOculusAudioSourceSettingsFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_OculusAudioEditor();
// End Cross Module References
	void UOculusAudioSourceSettingsFactory::StaticRegisterNativesUOculusAudioSourceSettingsFactory()
	{
	}
	UClass* Z_Construct_UClass_UOculusAudioSourceSettingsFactory_NoRegister()
	{
		return UOculusAudioSourceSettingsFactory::StaticClass();
	}
	struct Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudioEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "OculusAudioSourceSettingsFactory.h" },
		{ "ModuleRelativePath", "Private/OculusAudioSourceSettingsFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusAudioSourceSettingsFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics::ClassParams = {
		&UOculusAudioSourceSettingsFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusAudioSourceSettingsFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusAudioSourceSettingsFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusAudioSourceSettingsFactory, 3422551535);
	template<> OCULUSAUDIOEDITOR_API UClass* StaticClass<UOculusAudioSourceSettingsFactory>()
	{
		return UOculusAudioSourceSettingsFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusAudioSourceSettingsFactory(Z_Construct_UClass_UOculusAudioSourceSettingsFactory, &UOculusAudioSourceSettingsFactory::StaticClass, TEXT("/Script/OculusAudioEditor"), TEXT("UOculusAudioSourceSettingsFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusAudioSourceSettingsFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
