// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OCULUSAUDIO_OculusAudioMaterialComponent_generated_h
#error "OculusAudioMaterialComponent.generated.h already included, missing '#pragma once' in OculusAudioMaterialComponent.h"
#endif
#define OCULUSAUDIO_OculusAudioMaterialComponent_generated_h

#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_SPARSE_DATA
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_RPC_WRAPPERS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOculusAudioMaterialComponent(); \
	friend struct Z_Construct_UClass_UOculusAudioMaterialComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioMaterialComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioMaterialComponent)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUOculusAudioMaterialComponent(); \
	friend struct Z_Construct_UClass_UOculusAudioMaterialComponent_Statics; \
public: \
	DECLARE_CLASS(UOculusAudioMaterialComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OculusAudio"), NO_API) \
	DECLARE_SERIALIZER(UOculusAudioMaterialComponent)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOculusAudioMaterialComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOculusAudioMaterialComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioMaterialComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioMaterialComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioMaterialComponent(UOculusAudioMaterialComponent&&); \
	NO_API UOculusAudioMaterialComponent(const UOculusAudioMaterialComponent&); \
public:


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOculusAudioMaterialComponent(UOculusAudioMaterialComponent&&); \
	NO_API UOculusAudioMaterialComponent(const UOculusAudioMaterialComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOculusAudioMaterialComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOculusAudioMaterialComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UOculusAudioMaterialComponent)


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaterialPreset() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, MaterialPreset); } \
	FORCEINLINE static uint32 __PPO__Absorption63Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption63Hz); } \
	FORCEINLINE static uint32 __PPO__Absorption125Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption125Hz); } \
	FORCEINLINE static uint32 __PPO__Absorption250Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption250Hz); } \
	FORCEINLINE static uint32 __PPO__Absorption500Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption500Hz); } \
	FORCEINLINE static uint32 __PPO__Absorption1000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption1000Hz); } \
	FORCEINLINE static uint32 __PPO__Absorption2000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption2000Hz); } \
	FORCEINLINE static uint32 __PPO__Absorption4000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption4000Hz); } \
	FORCEINLINE static uint32 __PPO__Absorption8000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Absorption8000Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission63Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission63Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission125Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission125Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission250Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission250Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission500Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission500Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission1000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission1000Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission2000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission2000Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission4000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission4000Hz); } \
	FORCEINLINE static uint32 __PPO__Transmission8000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Transmission8000Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering63Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering63Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering125Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering125Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering250Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering250Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering500Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering500Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering1000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering1000Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering2000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering2000Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering4000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering4000Hz); } \
	FORCEINLINE static uint32 __PPO__Scattering8000Hz() { return STRUCT_OFFSET(UOculusAudioMaterialComponent, Scattering8000Hz); }


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_48_PROLOG
#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_RPC_WRAPPERS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_INCLASS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_SPARSE_DATA \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OCULUSAUDIO_API UClass* StaticClass<class UOculusAudioMaterialComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Runtime_Oculus_OculusAudio_Source_OculusAudio_Private_OculusAudioMaterialComponent_h


#define FOREACH_ENUM_EOCULUSAUDIOMATERIAL(op) \
	op(EOculusAudioMaterial::ACOUSTICTILE) \
	op(EOculusAudioMaterial::BRICK) \
	op(EOculusAudioMaterial::BRICKPAINTED) \
	op(EOculusAudioMaterial::CARPET) \
	op(EOculusAudioMaterial::CARPETHEAVY) \
	op(EOculusAudioMaterial::CARPETHEAVYPADDED) \
	op(EOculusAudioMaterial::CERAMICTILE) \
	op(EOculusAudioMaterial::CONCRETE) \
	op(EOculusAudioMaterial::CONCRETEROUGH) \
	op(EOculusAudioMaterial::CONCRETEBLOCK) \
	op(EOculusAudioMaterial::CONCRETEBLOCKPAINTED) \
	op(EOculusAudioMaterial::CURTAIN) \
	op(EOculusAudioMaterial::FOLIAGE) \
	op(EOculusAudioMaterial::GLASS) \
	op(EOculusAudioMaterial::GLASSHEAVY) \
	op(EOculusAudioMaterial::GRASS) \
	op(EOculusAudioMaterial::GRAVEL) \
	op(EOculusAudioMaterial::GYPSUMBOARD) \
	op(EOculusAudioMaterial::PLASTERONBRICK) \
	op(EOculusAudioMaterial::PLASTERONCONCRETEBLOCK) \
	op(EOculusAudioMaterial::SOIL) \
	op(EOculusAudioMaterial::SOUNDPROOF) \
	op(EOculusAudioMaterial::SNOW) \
	op(EOculusAudioMaterial::STEEL) \
	op(EOculusAudioMaterial::WATER) \
	op(EOculusAudioMaterial::WOODTHIN) \
	op(EOculusAudioMaterial::WOODTHICK) \
	op(EOculusAudioMaterial::WOODFLOOR) \
	op(EOculusAudioMaterial::WOODONCONCRETE) \
	op(EOculusAudioMaterial::MATERIAL_MAX) \
	op(EOculusAudioMaterial::NOMATERIAL) 

enum class EOculusAudioMaterial : uint8;
template<> OCULUSAUDIO_API UEnum* StaticEnum<EOculusAudioMaterial>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
