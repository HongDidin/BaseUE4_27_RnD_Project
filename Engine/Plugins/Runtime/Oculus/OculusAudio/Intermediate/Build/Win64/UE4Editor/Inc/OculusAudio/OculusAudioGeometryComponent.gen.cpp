// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OculusAudio/Private/OculusAudioGeometryComponent.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOculusAudioGeometryComponent() {}
// Cross Module References
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioGeometryComponent_NoRegister();
	OCULUSAUDIO_API UClass* Z_Construct_UClass_UOculusAudioGeometryComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_OculusAudio();
// End Cross Module References
	void UOculusAudioGeometryComponent::StaticRegisterNativesUOculusAudioGeometryComponent()
	{
	}
	UClass* Z_Construct_UClass_UOculusAudioGeometryComponent_NoRegister()
	{
		return UOculusAudioGeometryComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOculusAudioGeometryComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IncludeChildren_MetaData[];
#endif
		static void NewProp_IncludeChildren_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IncludeChildren;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_OculusAudio,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Audio" },
		{ "Comment", "/*\n * OculusAudio geometry components are used to customize an a static mesh actor's acoustic properties.\n */" },
		{ "HideCategories", "Activation Collision Cooking" },
		{ "IncludePath", "OculusAudioGeometryComponent.h" },
		{ "ModuleRelativePath", "Private/OculusAudioGeometryComponent.h" },
		{ "ToolTip", "* OculusAudio geometry components are used to customize an a static mesh actor's acoustic properties." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::NewProp_IncludeChildren_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Mesh hierarchy optimization for both content editing and runtime performance\n// if IncludeChildren is true, children (attached) meshes will be merged\n" },
		{ "ModuleRelativePath", "Private/OculusAudioGeometryComponent.h" },
		{ "ToolTip", "Mesh hierarchy optimization for both content editing and runtime performance\nif IncludeChildren is true, children (attached) meshes will be merged" },
	};
#endif
	void Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::NewProp_IncludeChildren_SetBit(void* Obj)
	{
		((UOculusAudioGeometryComponent*)Obj)->IncludeChildren = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::NewProp_IncludeChildren = { "IncludeChildren", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UOculusAudioGeometryComponent), &Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::NewProp_IncludeChildren_SetBit, METADATA_PARAMS(Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::NewProp_IncludeChildren_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::NewProp_IncludeChildren_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::NewProp_IncludeChildren,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOculusAudioGeometryComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::ClassParams = {
		&UOculusAudioGeometryComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::PropPointers),
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOculusAudioGeometryComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOculusAudioGeometryComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOculusAudioGeometryComponent, 1154683540);
	template<> OCULUSAUDIO_API UClass* StaticClass<UOculusAudioGeometryComponent>()
	{
		return UOculusAudioGeometryComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOculusAudioGeometryComponent(Z_Construct_UClass_UOculusAudioGeometryComponent, &UOculusAudioGeometryComponent::StaticClass, TEXT("/Script/OculusAudio"), TEXT("UOculusAudioGeometryComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOculusAudioGeometryComponent);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UOculusAudioGeometryComponent)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
