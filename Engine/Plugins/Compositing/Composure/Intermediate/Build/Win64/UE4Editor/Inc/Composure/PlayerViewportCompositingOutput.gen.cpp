// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/CompositingElements/PlayerViewportCompositingOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePlayerViewportCompositingOutput() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UPlayerViewportCompositingOutput_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UPlayerViewportCompositingOutput();
	COMPOSURE_API UClass* Z_Construct_UClass_UColorConverterOutputPass();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UClass* Z_Construct_UClass_UPlayerCompOutputCameraModifier_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBlendableInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UPlayerCompOutputCameraModifier();
	ENGINE_API UClass* Z_Construct_UClass_UCameraModifier();
// End Cross Module References
	void UPlayerViewportCompositingOutput::StaticRegisterNativesUPlayerViewportCompositingOutput()
	{
	}
	UClass* Z_Construct_UClass_UPlayerViewportCompositingOutput_NoRegister()
	{
		return UPlayerViewportCompositingOutput::StaticClass();
	}
	struct Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PlayerIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveCamModifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActiveCamModifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TonemapperBaseMat_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TonemapperBaseMat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreTonemapBaseMat_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreTonemapBaseMat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportOverrideMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ViewportOverrideMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UColorConverterOutputPass,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* UPlayerViewportCompositingOutput\n *****************************************************************************/" },
		{ "IncludePath", "CompositingElements/PlayerViewportCompositingOutput.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
		{ "ToolTip", "UPlayerViewportCompositingOutput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PlayerIndex_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PlayerIndex = { "PlayerIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlayerViewportCompositingOutput, PlayerIndex), METADATA_PARAMS(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PlayerIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PlayerIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ActiveCamModifier_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ActiveCamModifier = { "ActiveCamModifier", nullptr, (EPropertyFlags)0x00c0000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlayerViewportCompositingOutput, ActiveCamModifier), Z_Construct_UClass_UPlayerCompOutputCameraModifier_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ActiveCamModifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ActiveCamModifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_TonemapperBaseMat_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_TonemapperBaseMat = { "TonemapperBaseMat", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlayerViewportCompositingOutput, TonemapperBaseMat), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_TonemapperBaseMat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_TonemapperBaseMat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PreTonemapBaseMat_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PreTonemapBaseMat = { "PreTonemapBaseMat", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlayerViewportCompositingOutput, PreTonemapBaseMat), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PreTonemapBaseMat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PreTonemapBaseMat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ViewportOverrideMID_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ViewportOverrideMID = { "ViewportOverrideMID", nullptr, (EPropertyFlags)0x00c0000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlayerViewportCompositingOutput, ViewportOverrideMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ViewportOverrideMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ViewportOverrideMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PlayerIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ActiveCamModifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_TonemapperBaseMat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_PreTonemapBaseMat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::NewProp_ViewportOverrideMID,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UBlendableInterface_NoRegister, (int32)VTABLE_OFFSET(UPlayerViewportCompositingOutput, IBlendableInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlayerViewportCompositingOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::ClassParams = {
		&UPlayerViewportCompositingOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlayerViewportCompositingOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlayerViewportCompositingOutput, 3209121055);
	template<> COMPOSURE_API UClass* StaticClass<UPlayerViewportCompositingOutput>()
	{
		return UPlayerViewportCompositingOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlayerViewportCompositingOutput(Z_Construct_UClass_UPlayerViewportCompositingOutput, &UPlayerViewportCompositingOutput::StaticClass, TEXT("/Script/Composure"), TEXT("UPlayerViewportCompositingOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlayerViewportCompositingOutput);
	void UPlayerCompOutputCameraModifier::StaticRegisterNativesUPlayerCompOutputCameraModifier()
	{
	}
	UClass* Z_Construct_UClass_UPlayerCompOutputCameraModifier_NoRegister()
	{
		return UPlayerCompOutputCameraModifier::StaticClass();
	}
	struct Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Owner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Owner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/* UPlayerCompOutputCameraModifier\n *****************************************************************************/" },
		{ "IncludePath", "CompositingElements/PlayerViewportCompositingOutput.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
		{ "NotBlueprintType", "true" },
		{ "ToolTip", "UPlayerCompOutputCameraModifier" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::NewProp_Owner_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/PlayerViewportCompositingOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::NewProp_Owner = { "Owner", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlayerCompOutputCameraModifier, Owner), Z_Construct_UClass_UPlayerViewportCompositingOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::NewProp_Owner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::NewProp_Owner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::NewProp_Owner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlayerCompOutputCameraModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::ClassParams = {
		&UPlayerCompOutputCameraModifier::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlayerCompOutputCameraModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlayerCompOutputCameraModifier, 1129611);
	template<> COMPOSURE_API UClass* StaticClass<UPlayerCompOutputCameraModifier>()
	{
		return UPlayerCompOutputCameraModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlayerCompOutputCameraModifier(Z_Construct_UClass_UPlayerCompOutputCameraModifier, &UPlayerCompOutputCameraModifier::StaticClass, TEXT("/Script/Composure"), TEXT("UPlayerCompOutputCameraModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlayerCompOutputCameraModifier);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
