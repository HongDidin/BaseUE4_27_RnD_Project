// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_PlayerViewportCompositingOutput_generated_h
#error "PlayerViewportCompositingOutput.generated.h already included, missing '#pragma once' in PlayerViewportCompositingOutput.h"
#endif
#define COMPOSURE_PlayerViewportCompositingOutput_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerViewportCompositingOutput(); \
	friend struct Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics; \
public: \
	DECLARE_CLASS(UPlayerViewportCompositingOutput, UColorConverterOutputPass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UPlayerViewportCompositingOutput) \
	virtual UObject* _getUObject() const override { return const_cast<UPlayerViewportCompositingOutput*>(this); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerViewportCompositingOutput(); \
	friend struct Z_Construct_UClass_UPlayerViewportCompositingOutput_Statics; \
public: \
	DECLARE_CLASS(UPlayerViewportCompositingOutput, UColorConverterOutputPass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UPlayerViewportCompositingOutput) \
	virtual UObject* _getUObject() const override { return const_cast<UPlayerViewportCompositingOutput*>(this); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerViewportCompositingOutput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerViewportCompositingOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerViewportCompositingOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerViewportCompositingOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerViewportCompositingOutput(UPlayerViewportCompositingOutput&&); \
	NO_API UPlayerViewportCompositingOutput(const UPlayerViewportCompositingOutput&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerViewportCompositingOutput(UPlayerViewportCompositingOutput&&); \
	NO_API UPlayerViewportCompositingOutput(const UPlayerViewportCompositingOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerViewportCompositingOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerViewportCompositingOutput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPlayerViewportCompositingOutput)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActiveCamModifier() { return STRUCT_OFFSET(UPlayerViewportCompositingOutput, ActiveCamModifier); } \
	FORCEINLINE static uint32 __PPO__TonemapperBaseMat() { return STRUCT_OFFSET(UPlayerViewportCompositingOutput, TonemapperBaseMat); } \
	FORCEINLINE static uint32 __PPO__PreTonemapBaseMat() { return STRUCT_OFFSET(UPlayerViewportCompositingOutput, PreTonemapBaseMat); } \
	FORCEINLINE static uint32 __PPO__ViewportOverrideMID() { return STRUCT_OFFSET(UPlayerViewportCompositingOutput, ViewportOverrideMID); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_20_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UPlayerViewportCompositingOutput>();

#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPlayerCompOutputCameraModifier(); \
	friend struct Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics; \
public: \
	DECLARE_CLASS(UPlayerCompOutputCameraModifier, UCameraModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UPlayerCompOutputCameraModifier)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_INCLASS \
private: \
	static void StaticRegisterNativesUPlayerCompOutputCameraModifier(); \
	friend struct Z_Construct_UClass_UPlayerCompOutputCameraModifier_Statics; \
public: \
	DECLARE_CLASS(UPlayerCompOutputCameraModifier, UCameraModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UPlayerCompOutputCameraModifier)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerCompOutputCameraModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerCompOutputCameraModifier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerCompOutputCameraModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerCompOutputCameraModifier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerCompOutputCameraModifier(UPlayerCompOutputCameraModifier&&); \
	NO_API UPlayerCompOutputCameraModifier(const UPlayerCompOutputCameraModifier&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPlayerCompOutputCameraModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPlayerCompOutputCameraModifier(UPlayerCompOutputCameraModifier&&); \
	NO_API UPlayerCompOutputCameraModifier(const UPlayerCompOutputCameraModifier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPlayerCompOutputCameraModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPlayerCompOutputCameraModifier); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPlayerCompOutputCameraModifier)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Owner() { return STRUCT_OFFSET(UPlayerCompOutputCameraModifier, Owner); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_71_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h_74_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UPlayerCompOutputCameraModifier>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_PlayerViewportCompositingOutput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
