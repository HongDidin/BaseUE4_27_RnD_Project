// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURELAYERSEDITOR_EditorCompElementContainer_generated_h
#error "EditorCompElementContainer.generated.h already included, missing '#pragma once' in EditorCompElementContainer.h"
#endif
#define COMPOSURELAYERSEDITOR_EditorCompElementContainer_generated_h

#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEditorCompElementContainer(); \
	friend struct Z_Construct_UClass_UEditorCompElementContainer_Statics; \
public: \
	DECLARE_CLASS(UEditorCompElementContainer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ComposureLayersEditor"), NO_API) \
	DECLARE_SERIALIZER(UEditorCompElementContainer)


#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUEditorCompElementContainer(); \
	friend struct Z_Construct_UClass_UEditorCompElementContainer_Statics; \
public: \
	DECLARE_CLASS(UEditorCompElementContainer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ComposureLayersEditor"), NO_API) \
	DECLARE_SERIALIZER(UEditorCompElementContainer)


#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEditorCompElementContainer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEditorCompElementContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorCompElementContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorCompElementContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorCompElementContainer(UEditorCompElementContainer&&); \
	NO_API UEditorCompElementContainer(const UEditorCompElementContainer&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEditorCompElementContainer(UEditorCompElementContainer&&); \
	NO_API UEditorCompElementContainer(const UEditorCompElementContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEditorCompElementContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEditorCompElementContainer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEditorCompElementContainer)


#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CompElements() { return STRUCT_OFFSET(UEditorCompElementContainer, CompElements); }


#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_16_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURELAYERSEDITOR_API UClass* StaticClass<class UEditorCompElementContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_ComposureLayersEditor_Private_EditorCompElementContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
