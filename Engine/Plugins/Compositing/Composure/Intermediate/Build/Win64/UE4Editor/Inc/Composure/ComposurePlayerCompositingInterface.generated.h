// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_ComposurePlayerCompositingInterface_generated_h
#error "ComposurePlayerCompositingInterface.generated.h already included, missing '#pragma once' in ComposurePlayerCompositingInterface.h"
#endif
#define COMPOSURE_ComposurePlayerCompositingInterface_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UComposurePlayerCompositingInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UComposurePlayerCompositingInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UComposurePlayerCompositingInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UComposurePlayerCompositingInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UComposurePlayerCompositingInterface(UComposurePlayerCompositingInterface&&); \
	COMPOSURE_API UComposurePlayerCompositingInterface(const UComposurePlayerCompositingInterface&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UComposurePlayerCompositingInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UComposurePlayerCompositingInterface(UComposurePlayerCompositingInterface&&); \
	COMPOSURE_API UComposurePlayerCompositingInterface(const UComposurePlayerCompositingInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UComposurePlayerCompositingInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UComposurePlayerCompositingInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UComposurePlayerCompositingInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUComposurePlayerCompositingInterface(); \
	friend struct Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics; \
public: \
	DECLARE_CLASS(UComposurePlayerCompositingInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UComposurePlayerCompositingInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IComposurePlayerCompositingInterface() {} \
public: \
	typedef UComposurePlayerCompositingInterface UClassType; \
	typedef IComposurePlayerCompositingInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IComposurePlayerCompositingInterface() {} \
public: \
	typedef UComposurePlayerCompositingInterface UClassType; \
	typedef IComposurePlayerCompositingInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_10_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UComposurePlayerCompositingInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Public_ComposurePlayerCompositingInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
