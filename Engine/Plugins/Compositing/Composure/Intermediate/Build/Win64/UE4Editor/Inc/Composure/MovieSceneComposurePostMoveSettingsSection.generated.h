// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_MovieSceneComposurePostMoveSettingsSection_generated_h
#error "MovieSceneComposurePostMoveSettingsSection.generated.h already included, missing '#pragma once' in MovieSceneComposurePostMoveSettingsSection.h"
#endif
#define COMPOSURE_MovieSceneComposurePostMoveSettingsSection_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneComposurePostMoveSettingsSection(); \
	friend struct Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposurePostMoveSettingsSection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposurePostMoveSettingsSection)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneComposurePostMoveSettingsSection(); \
	friend struct Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposurePostMoveSettingsSection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposurePostMoveSettingsSection)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsSection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposurePostMoveSettingsSection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposurePostMoveSettingsSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposurePostMoveSettingsSection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsSection(UMovieSceneComposurePostMoveSettingsSection&&); \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsSection(const UMovieSceneComposurePostMoveSettingsSection&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsSection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsSection(UMovieSceneComposurePostMoveSettingsSection&&); \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsSection(const UMovieSceneComposurePostMoveSettingsSection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposurePostMoveSettingsSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposurePostMoveSettingsSection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposurePostMoveSettingsSection)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_12_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class MovieSceneComposurePostMoveSettingsSection."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UMovieSceneComposurePostMoveSettingsSection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsSection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
