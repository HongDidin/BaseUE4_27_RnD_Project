// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/ComposureUVMap.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposureUVMap() {}
// Cross Module References
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FComposureUVMapSettings();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FMatrix();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
// End Cross Module References
class UScriptStruct* FComposureUVMapSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FComposureUVMapSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FComposureUVMapSettings, Z_Construct_UPackage__Script_Composure(), TEXT("ComposureUVMapSettings"), sizeof(FComposureUVMapSettings), Get_Z_Construct_UScriptStruct_FComposureUVMapSettings_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FComposureUVMapSettings>()
{
	return FComposureUVMapSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FComposureUVMapSettings(FComposureUVMapSettings::StaticStruct, TEXT("/Script/Composure"), TEXT("ComposureUVMapSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFComposureUVMapSettings
{
	FScriptStruct_Composure_StaticRegisterNativesFComposureUVMapSettings()
	{
		UScriptStruct::DeferCppStructOps<FComposureUVMapSettings>(FName(TEXT("ComposureUVMapSettings")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFComposureUVMapSettings;
	struct Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreUVDisplacementMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreUVDisplacementMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostUVDisplacementMatrix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PostUVDisplacementMatrix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementDecodeParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DisplacementDecodeParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplacementTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseDisplacementBlueAndAlphaChannels_MetaData[];
#endif
		static void NewProp_bUseDisplacementBlueAndAlphaChannels_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseDisplacementBlueAndAlphaChannels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/ComposureUVMap.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FComposureUVMapSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PreUVDisplacementMatrix_MetaData[] = {
		{ "Category", "UV Mapping" },
		{ "Comment", "/** UV Matrix to apply before sampling DisplacementTexture. */" },
		{ "ModuleRelativePath", "Public/ComposureUVMap.h" },
		{ "ToolTip", "UV Matrix to apply before sampling DisplacementTexture." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PreUVDisplacementMatrix = { "PreUVDisplacementMatrix", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposureUVMapSettings, PreUVDisplacementMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PreUVDisplacementMatrix_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PreUVDisplacementMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PostUVDisplacementMatrix_MetaData[] = {
		{ "Category", "UV Mapping" },
		{ "Comment", "/** UV Matrix to apply after displacing UV using DisplacementTexture. */" },
		{ "ModuleRelativePath", "Public/ComposureUVMap.h" },
		{ "ToolTip", "UV Matrix to apply after displacing UV using DisplacementTexture." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PostUVDisplacementMatrix = { "PostUVDisplacementMatrix", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposureUVMapSettings, PostUVDisplacementMatrix), Z_Construct_UScriptStruct_FMatrix, METADATA_PARAMS(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PostUVDisplacementMatrix_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PostUVDisplacementMatrix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementDecodeParameters_MetaData[] = {
		{ "Category", "UV Mapping" },
		{ "Comment", "/** Decoding parameters for DisplacementTexture. DeltaUV = ((RedChannel, GreenChannel) - Y) * X. */" },
		{ "ModuleRelativePath", "Public/ComposureUVMap.h" },
		{ "ToolTip", "Decoding parameters for DisplacementTexture. DeltaUV = ((RedChannel, GreenChannel) - Y) * X." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementDecodeParameters = { "DisplacementDecodeParameters", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposureUVMapSettings, DisplacementDecodeParameters), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementDecodeParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementDecodeParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementTexture_MetaData[] = {
		{ "Category", "UV Mapping" },
		{ "Comment", "/** Displacement texture to use. */" },
		{ "ModuleRelativePath", "Public/ComposureUVMap.h" },
		{ "ToolTip", "Displacement texture to use." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementTexture = { "DisplacementTexture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposureUVMapSettings, DisplacementTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_bUseDisplacementBlueAndAlphaChannels_MetaData[] = {
		{ "Category", "UV Mapping" },
		{ "Comment", "/** Whether to use blue and alpha channel instead of red and green channel in computation of DeltaUV. */" },
		{ "ModuleRelativePath", "Public/ComposureUVMap.h" },
		{ "ToolTip", "Whether to use blue and alpha channel instead of red and green channel in computation of DeltaUV." },
	};
#endif
	void Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_bUseDisplacementBlueAndAlphaChannels_SetBit(void* Obj)
	{
		((FComposureUVMapSettings*)Obj)->bUseDisplacementBlueAndAlphaChannels = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_bUseDisplacementBlueAndAlphaChannels = { "bUseDisplacementBlueAndAlphaChannels", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FComposureUVMapSettings), &Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_bUseDisplacementBlueAndAlphaChannels_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_bUseDisplacementBlueAndAlphaChannels_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_bUseDisplacementBlueAndAlphaChannels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PreUVDisplacementMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_PostUVDisplacementMatrix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementDecodeParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_DisplacementTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::NewProp_bUseDisplacementBlueAndAlphaChannels,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		nullptr,
		&NewStructOps,
		"ComposureUVMapSettings",
		sizeof(FComposureUVMapSettings),
		alignof(FComposureUVMapSettings),
		Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FComposureUVMapSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FComposureUVMapSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ComposureUVMapSettings"), sizeof(FComposureUVMapSettings), Get_Z_Construct_UScriptStruct_FComposureUVMapSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FComposureUVMapSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FComposureUVMapSettings_Hash() { return 3532875155U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
