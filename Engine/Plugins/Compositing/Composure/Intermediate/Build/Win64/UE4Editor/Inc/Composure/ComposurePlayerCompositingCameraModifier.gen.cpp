// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Private/ComposurePlayerCompositingCameraModifier.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposurePlayerCompositingCameraModifier() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingCameraModifier();
	ENGINE_API UClass* Z_Construct_UClass_UCameraModifier();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBlendableInterface_NoRegister();
// End Cross Module References
	void UComposurePlayerCompositingCameraModifier::StaticRegisterNativesUComposurePlayerCompositingCameraModifier()
	{
	}
	UClass* Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_NoRegister()
	{
		return UComposurePlayerCompositingCameraModifier::StaticClass();
	}
	struct Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_Target;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Private camera manager for  UComposurePlayerCompositingTarget.\n */" },
		{ "IncludePath", "ComposurePlayerCompositingCameraModifier.h" },
		{ "ModuleRelativePath", "Private/ComposurePlayerCompositingCameraModifier.h" },
		{ "NotBlueprintType", "true" },
		{ "ToolTip", "Private camera manager for  UComposurePlayerCompositingTarget." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::NewProp_Target_MetaData[] = {
		{ "Comment", "// Current player camera manager the target is bind on.\n" },
		{ "ModuleRelativePath", "Private/ComposurePlayerCompositingCameraModifier.h" },
		{ "ToolTip", "Current player camera manager the target is bind on." },
	};
#endif
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePlayerCompositingCameraModifier, Target), Z_Construct_UClass_UComposurePlayerCompositingInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::NewProp_Target_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::NewProp_Target,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UBlendableInterface_NoRegister, (int32)VTABLE_OFFSET(UComposurePlayerCompositingCameraModifier, IBlendableInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposurePlayerCompositingCameraModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::ClassParams = {
		&UComposurePlayerCompositingCameraModifier::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposurePlayerCompositingCameraModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposurePlayerCompositingCameraModifier, 189811688);
	template<> COMPOSURE_API UClass* StaticClass<UComposurePlayerCompositingCameraModifier>()
	{
		return UComposurePlayerCompositingCameraModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposurePlayerCompositingCameraModifier(Z_Construct_UClass_UComposurePlayerCompositingCameraModifier, &UComposurePlayerCompositingCameraModifier::StaticClass, TEXT("/Script/Composure"), TEXT("UComposurePlayerCompositingCameraModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposurePlayerCompositingCameraModifier);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
