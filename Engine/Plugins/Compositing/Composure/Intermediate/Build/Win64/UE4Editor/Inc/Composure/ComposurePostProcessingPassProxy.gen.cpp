// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/ComposurePostProcessingPassProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposurePostProcessingPassProxy() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPassPolicy_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPassPolicy();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Composure();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessingPassProxy_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessingPassProxy();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPass();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UComposurePostProcessPassPolicy::execSetupPostProcess)
	{
		P_GET_OBJECT(USceneCaptureComponent2D,Z_Param_SceneCapture);
		P_GET_OBJECT_REF(UMaterialInterface,Z_Param_Out_TonemapperOverride);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetupPostProcess_Implementation(Z_Param_SceneCapture,Z_Param_Out_TonemapperOverride);
		P_NATIVE_END;
	}
	static FName NAME_UComposurePostProcessPassPolicy_SetupPostProcess = FName(TEXT("SetupPostProcess"));
	void UComposurePostProcessPassPolicy::SetupPostProcess(USceneCaptureComponent2D* SceneCapture, UMaterialInterface*& TonemapperOverride)
	{
		ComposurePostProcessPassPolicy_eventSetupPostProcess_Parms Parms;
		Parms.SceneCapture=SceneCapture;
		Parms.TonemapperOverride=TonemapperOverride;
		ProcessEvent(FindFunctionChecked(NAME_UComposurePostProcessPassPolicy_SetupPostProcess),&Parms);
		TonemapperOverride=Parms.TonemapperOverride;
	}
	void UComposurePostProcessPassPolicy::StaticRegisterNativesUComposurePostProcessPassPolicy()
	{
		UClass* Class = UComposurePostProcessPassPolicy::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetupPostProcess", &UComposurePostProcessPassPolicy::execSetupPostProcess },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapture;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TonemapperOverride;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::NewProp_SceneCapture_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::NewProp_SceneCapture = { "SceneCapture", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessPassPolicy_eventSetupPostProcess_Parms, SceneCapture), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::NewProp_SceneCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::NewProp_SceneCapture_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::NewProp_TonemapperOverride = { "TonemapperOverride", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessPassPolicy_eventSetupPostProcess_Parms, TonemapperOverride), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::NewProp_SceneCapture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::NewProp_TonemapperOverride,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::Function_MetaDataParams[] = {
		{ "Category", "Composure" },
		{ "Comment", "/** */" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessingPassProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePostProcessPassPolicy, nullptr, "SetupPostProcess", nullptr, nullptr, sizeof(ComposurePostProcessPassPolicy_eventSetupPostProcess_Parms), Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UComposurePostProcessPassPolicy_NoRegister()
	{
		return UComposurePostProcessPassPolicy::StaticClass();
	}
	struct Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UComposurePostProcessPassPolicy_SetupPostProcess, "SetupPostProcess" }, // 323905949
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Abstract base class for setting up post passes. Used in conjuntion with UComposurePostProcessingPassProxy.\n */" },
		{ "IncludePath", "ComposurePostProcessingPassProxy.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessingPassProxy.h" },
		{ "ToolTip", "Abstract base class for setting up post passes. Used in conjuntion with UComposurePostProcessingPassProxy." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposurePostProcessPassPolicy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::ClassParams = {
		&UComposurePostProcessPassPolicy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposurePostProcessPassPolicy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposurePostProcessPassPolicy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposurePostProcessPassPolicy, 1091214343);
	template<> COMPOSURE_API UClass* StaticClass<UComposurePostProcessPassPolicy>()
	{
		return UComposurePostProcessPassPolicy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposurePostProcessPassPolicy(Z_Construct_UClass_UComposurePostProcessPassPolicy, &UComposurePostProcessPassPolicy::StaticClass, TEXT("/Script/Composure"), TEXT("UComposurePostProcessPassPolicy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposurePostProcessPassPolicy);
	DEFINE_FUNCTION(UComposurePostProcessingPassProxy::execExecute)
	{
		P_GET_OBJECT(UTexture,Z_Param_PrePassInput);
		P_GET_OBJECT(UComposurePostProcessPassPolicy,Z_Param_PostProcessPass);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Execute(Z_Param_PrePassInput,Z_Param_PostProcessPass);
		P_NATIVE_END;
	}
	void UComposurePostProcessingPassProxy::StaticRegisterNativesUComposurePostProcessingPassProxy()
	{
		UClass* Class = UComposurePostProcessingPassProxy::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "Execute", &UComposurePostProcessingPassProxy::execExecute },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics
	{
		struct ComposurePostProcessingPassProxy_eventExecute_Parms
		{
			UTexture* PrePassInput;
			UComposurePostProcessPassPolicy* PostProcessPass;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PrePassInput;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PostProcessPass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::NewProp_PrePassInput = { "PrePassInput", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessingPassProxy_eventExecute_Parms, PrePassInput), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::NewProp_PostProcessPass = { "PostProcessPass", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessingPassProxy_eventExecute_Parms, PostProcessPass), Z_Construct_UClass_UComposurePostProcessPassPolicy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::NewProp_PrePassInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::NewProp_PostProcessPass,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::Function_MetaDataParams[] = {
		{ "Category", "Composure" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessingPassProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePostProcessingPassProxy, nullptr, "Execute", nullptr, nullptr, sizeof(ComposurePostProcessingPassProxy_eventExecute_Parms), Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UComposurePostProcessingPassProxy_NoRegister()
	{
		return UComposurePostProcessingPassProxy::StaticClass();
	}
	struct Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SetupMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SetupMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UComposurePostProcessPass,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UComposurePostProcessingPassProxy_Execute, "Execute" }, // 1718834175
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Composure" },
		{ "Comment", "/**\n * Generic component class which takes a UComposurePostProcessPassPolicy and \n * executes it, enqueuing a post-process pass for the render frame.\n */" },
		{ "HideCategories", "Collision Object Physics SceneComponent Transform Trigger PhysicsVolume" },
		{ "IncludePath", "ComposurePostProcessingPassProxy.h" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessingPassProxy.h" },
		{ "ToolTip", "Generic component class which takes a UComposurePostProcessPassPolicy and\nexecutes it, enqueuing a post-process pass for the render frame." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::NewProp_SetupMID_MetaData[] = {
		{ "ModuleRelativePath", "Classes/ComposurePostProcessingPassProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::NewProp_SetupMID = { "SetupMID", nullptr, (EPropertyFlags)0x00c0000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePostProcessingPassProxy, SetupMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::NewProp_SetupMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::NewProp_SetupMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::NewProp_SetupMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposurePostProcessingPassProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::ClassParams = {
		&UComposurePostProcessingPassProxy::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposurePostProcessingPassProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposurePostProcessingPassProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposurePostProcessingPassProxy, 1415123878);
	template<> COMPOSURE_API UClass* StaticClass<UComposurePostProcessingPassProxy>()
	{
		return UComposurePostProcessingPassProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposurePostProcessingPassProxy(Z_Construct_UClass_UComposurePostProcessingPassProxy, &UComposurePostProcessingPassProxy::StaticClass, TEXT("/Script/Composure"), TEXT("UComposurePostProcessingPassProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposurePostProcessingPassProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
