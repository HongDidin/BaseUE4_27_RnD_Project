// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/ComposurePostMoves.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposurePostMoves() {}
// Cross Module References
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FComposurePostMoveSettings();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
class UScriptStruct* FComposurePostMoveSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FComposurePostMoveSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FComposurePostMoveSettings, Z_Construct_UPackage__Script_Composure(), TEXT("ComposurePostMoveSettings"), sizeof(FComposurePostMoveSettings), Get_Z_Construct_UScriptStruct_FComposurePostMoveSettings_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FComposurePostMoveSettings>()
{
	return FComposurePostMoveSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FComposurePostMoveSettings(FComposurePostMoveSettings::StaticStruct, TEXT("/Script/Composure"), TEXT("ComposurePostMoveSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFComposurePostMoveSettings
{
	FScriptStruct_Composure_StaticRegisterNativesFComposurePostMoveSettings()
	{
		UScriptStruct::DeferCppStructOps<FComposurePostMoveSettings>(FName(TEXT("ComposurePostMoveSettings")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFComposurePostMoveSettings;
	struct Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pivot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pivot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Translation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Translation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RotationAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/ComposurePostMoves.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FComposurePostMoveSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Pivot_MetaData[] = {
		{ "Category", "Post Moves" },
		{ "Comment", "/** The normalized pivot point for applying rotation and scale to the image. The x and y values are normalized to the range 0-1 where 1 represents the full width and height of the image. */" },
		{ "ModuleRelativePath", "Public/ComposurePostMoves.h" },
		{ "ToolTip", "The normalized pivot point for applying rotation and scale to the image. The x and y values are normalized to the range 0-1 where 1 represents the full width and height of the image." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Pivot = { "Pivot", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposurePostMoveSettings, Pivot), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Pivot_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Pivot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Translation_MetaData[] = {
		{ "Category", "Post Moves" },
		{ "Comment", "/** The translation to apply to the image.  The x and y values are normalized to the range 0-1 where 1 represents the full width and height of the image. */" },
		{ "ModuleRelativePath", "Public/ComposurePostMoves.h" },
		{ "ToolTip", "The translation to apply to the image.  The x and y values are normalized to the range 0-1 where 1 represents the full width and height of the image." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Translation = { "Translation", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposurePostMoveSettings, Translation), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Translation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Translation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_RotationAngle_MetaData[] = {
		{ "Category", "Post Moves" },
		{ "Comment", "/** The anti clockwise rotation to apply to the image in degrees. */" },
		{ "ModuleRelativePath", "Public/ComposurePostMoves.h" },
		{ "ToolTip", "The anti clockwise rotation to apply to the image in degrees." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_RotationAngle = { "RotationAngle", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposurePostMoveSettings, RotationAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_RotationAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_RotationAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Scale_MetaData[] = {
		{ "Category", "Post Moves" },
		{ "Comment", "/** The scale to apply to the image. */" },
		{ "ModuleRelativePath", "Public/ComposurePostMoves.h" },
		{ "ToolTip", "The scale to apply to the image." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FComposurePostMoveSettings, Scale), METADATA_PARAMS(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Scale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Pivot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Translation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_RotationAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::NewProp_Scale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		nullptr,
		&NewStructOps,
		"ComposurePostMoveSettings",
		sizeof(FComposurePostMoveSettings),
		alignof(FComposurePostMoveSettings),
		Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FComposurePostMoveSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FComposurePostMoveSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ComposurePostMoveSettings"), sizeof(FComposurePostMoveSettings), Get_Z_Construct_UScriptStruct_FComposurePostMoveSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FComposurePostMoveSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FComposurePostMoveSettings_Hash() { return 3321757174U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
