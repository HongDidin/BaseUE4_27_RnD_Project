// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Private/ComposureConfigSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposureConfigSettings() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureGameSettings_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureGameSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
// End Cross Module References
	void UComposureGameSettings::StaticRegisterNativesUComposureGameSettings()
	{
	}
	UClass* Z_Construct_UClass_UComposureGameSettings_NoRegister()
	{
		return UComposureGameSettings::StaticClass();
	}
	struct Z_Construct_UClass_UComposureGameSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticVideoPlateDebugImage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StaticVideoPlateDebugImage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSceneCapWarnOfMissingCam_MetaData[];
#endif
		static void NewProp_bSceneCapWarnOfMissingCam_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSceneCapWarnOfMissingCam;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FallbackCompositingTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FallbackCompositingTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FallbackCompositingTextureObj_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FallbackCompositingTextureObj;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposureGameSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureGameSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ComposureConfigSettings.h" },
		{ "ModuleRelativePath", "Private/ComposureConfigSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_StaticVideoPlateDebugImage_MetaData[] = {
		{ "Category", "Composure|Media" },
		{ "ModuleRelativePath", "Private/ComposureConfigSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_StaticVideoPlateDebugImage = { "StaticVideoPlateDebugImage", nullptr, (EPropertyFlags)0x0010000000004014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureGameSettings, StaticVideoPlateDebugImage), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_StaticVideoPlateDebugImage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_StaticVideoPlateDebugImage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_bSceneCapWarnOfMissingCam_MetaData[] = {
		{ "Category", "Composure|Editor" },
		{ "ModuleRelativePath", "Private/ComposureConfigSettings.h" },
	};
#endif
	void Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_bSceneCapWarnOfMissingCam_SetBit(void* Obj)
	{
		((UComposureGameSettings*)Obj)->bSceneCapWarnOfMissingCam = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_bSceneCapWarnOfMissingCam = { "bSceneCapWarnOfMissingCam", nullptr, (EPropertyFlags)0x0010000000004014, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UComposureGameSettings), &Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_bSceneCapWarnOfMissingCam_SetBit, METADATA_PARAMS(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_bSceneCapWarnOfMissingCam_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_bSceneCapWarnOfMissingCam_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTexture_MetaData[] = {
		{ "Category", "Composure" },
		{ "ModuleRelativePath", "Private/ComposureConfigSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTexture = { "FallbackCompositingTexture", nullptr, (EPropertyFlags)0x0010000000004014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureGameSettings, FallbackCompositingTexture), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTextureObj_MetaData[] = {
		{ "ModuleRelativePath", "Private/ComposureConfigSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTextureObj = { "FallbackCompositingTextureObj", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureGameSettings, FallbackCompositingTextureObj), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTextureObj_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTextureObj_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposureGameSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_StaticVideoPlateDebugImage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_bSceneCapWarnOfMissingCam,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureGameSettings_Statics::NewProp_FallbackCompositingTextureObj,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposureGameSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposureGameSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposureGameSettings_Statics::ClassParams = {
		&UComposureGameSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UComposureGameSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposureGameSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UComposureGameSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureGameSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposureGameSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposureGameSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposureGameSettings, 2126836964);
	template<> COMPOSURE_API UClass* StaticClass<UComposureGameSettings>()
	{
		return UComposureGameSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposureGameSettings(Z_Construct_UClass_UComposureGameSettings, &UComposureGameSettings::StaticClass, TEXT("/Script/Composure"), TEXT("UComposureGameSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposureGameSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
