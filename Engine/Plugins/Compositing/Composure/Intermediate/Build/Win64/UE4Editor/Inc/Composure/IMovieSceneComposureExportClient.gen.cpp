// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/MovieScene/IMovieSceneComposureExportClient.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIMovieSceneComposureExportClient() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportClient_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportClient();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportInitializer_NoRegister();
// End Cross Module References
	void IMovieSceneComposureExportClient::InitializeForExport(UMovieSceneComposureExportInitializer* ExportInitializer)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_InitializeForExport instead.");
	}
	void UMovieSceneComposureExportClient::StaticRegisterNativesUMovieSceneComposureExportClient()
	{
	}
	struct Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExportInitializer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::NewProp_ExportInitializer = { "ExportInitializer", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneComposureExportClient_eventInitializeForExport_Parms, ExportInitializer), Z_Construct_UClass_UMovieSceneComposureExportInitializer_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::NewProp_ExportInitializer,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::Function_MetaDataParams[] = {
		{ "Category", "Compsure|Export" },
		{ "Comment", "/**\n\x09 * Initialize this object for export by setting up any of the necessary scene view extensions with the specified initializer.\n\x09 */" },
		{ "ModuleRelativePath", "Public/MovieScene/IMovieSceneComposureExportClient.h" },
		{ "ToolTip", "Initialize this object for export by setting up any of the necessary scene view extensions with the specified initializer." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneComposureExportClient, nullptr, "InitializeForExport", nullptr, nullptr, sizeof(MovieSceneComposureExportClient_eventInitializeForExport_Parms), Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMovieSceneComposureExportClient_NoRegister()
	{
		return UMovieSceneComposureExportClient::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneComposureExportClient_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMovieSceneComposureExportClient_InitializeForExport, "InitializeForExport" }, // 2506136258
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "Sequencer" },
		{ "DisplayName", "Composure Export Client" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MovieScene/IMovieSceneComposureExportClient.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IMovieSceneComposureExportClient>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::ClassParams = {
		&UMovieSceneComposureExportClient::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneComposureExportClient()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneComposureExportClient_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneComposureExportClient, 321424564);
	template<> COMPOSURE_API UClass* StaticClass<UMovieSceneComposureExportClient>()
	{
		return UMovieSceneComposureExportClient::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneComposureExportClient(Z_Construct_UClass_UMovieSceneComposureExportClient, &UMovieSceneComposureExportClient::StaticClass, TEXT("/Script/Composure"), TEXT("UMovieSceneComposureExportClient"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneComposureExportClient);
	static FName NAME_UMovieSceneComposureExportClient_InitializeForExport = FName(TEXT("InitializeForExport"));
	void IMovieSceneComposureExportClient::Execute_InitializeForExport(UObject* O, UMovieSceneComposureExportInitializer* ExportInitializer)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UMovieSceneComposureExportClient::StaticClass()));
		MovieSceneComposureExportClient_eventInitializeForExport_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UMovieSceneComposureExportClient_InitializeForExport);
		if (Func)
		{
			Parms.ExportInitializer=ExportInitializer;
			O->ProcessEvent(Func, &Parms);
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
