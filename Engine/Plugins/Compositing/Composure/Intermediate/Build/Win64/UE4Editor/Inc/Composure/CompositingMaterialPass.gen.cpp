// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/CompositingElements/CompositingMaterialPass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompositingMaterialPass() {}
// Cross Module References
	COMPOSURE_API UEnum* Z_Construct_UEnum_Composure_EParamType();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FCompositingMaterial();
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FCompositingParamPayload();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FNamedCompMaterialParam();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	static UEnum* EParamType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Composure_EParamType, Z_Construct_UPackage__Script_Composure(), TEXT("EParamType"));
		}
		return Singleton;
	}
	template<> COMPOSURE_API UEnum* StaticEnum<EParamType>()
	{
		return EParamType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EParamType(EParamType_StaticEnum, TEXT("/Script/Composure"), TEXT("EParamType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Composure_EParamType_Hash() { return 3258973345U; }
	UEnum* Z_Construct_UEnum_Composure_EParamType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EParamType"), 0, Get_Z_Construct_UEnum_Composure_EParamType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EParamType::UnknownParamType", (int64)EParamType::UnknownParamType },
				{ "EParamType::ScalarParam", (int64)EParamType::ScalarParam },
				{ "EParamType::VectorParam", (int64)EParamType::VectorParam },
				{ "EParamType::TextureParam", (int64)EParamType::TextureParam },
				{ "EParamType::MediaTextureParam", (int64)EParamType::MediaTextureParam },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/* FNamedCompMaterialParam\n *****************************************************************************/" },
				{ "MediaTextureParam.Name", "EParamType::MediaTextureParam" },
				{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
				{ "ScalarParam.Name", "EParamType::ScalarParam" },
				{ "TextureParam.Name", "EParamType::TextureParam" },
				{ "ToolTip", "FNamedCompMaterialParam" },
				{ "UnknownParamType.Name", "EParamType::UnknownParamType" },
				{ "VectorParam.Name", "EParamType::VectorParam" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Composure,
				nullptr,
				"EParamType",
				"EParamType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FCompositingMaterial>() == std::is_polymorphic<FCompositingParamPayload>(), "USTRUCT FCompositingMaterial cannot be polymorphic unless super FCompositingParamPayload is polymorphic");

class UScriptStruct* FCompositingMaterial::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FCompositingMaterial_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCompositingMaterial, Z_Construct_UPackage__Script_Composure(), TEXT("CompositingMaterial"), sizeof(FCompositingMaterial), Get_Z_Construct_UScriptStruct_FCompositingMaterial_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FCompositingMaterial>()
{
	return FCompositingMaterial::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCompositingMaterial(FCompositingMaterial::StaticStruct, TEXT("/Script/Composure"), TEXT("CompositingMaterial"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFCompositingMaterial
{
	FScriptStruct_Composure_StaticRegisterNativesFCompositingMaterial()
	{
		UScriptStruct::DeferCppStructOps<FCompositingMaterial>(FName(TEXT("CompositingMaterial")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFCompositingMaterial;
	struct Z_Construct_UScriptStruct_FCompositingMaterial_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParamPassMappings_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParamPassMappings_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParamPassMappings_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ParamPassMappings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RequiredMaterialParams_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RequiredMaterialParams_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequiredMaterialParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RequiredMaterialParams;
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_EditorHiddenParams_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EditorHiddenParams_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EditorHiddenParams;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VectorOverrideProxies_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_VectorOverrideProxies_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VectorOverrideProxies_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_VectorOverrideProxies;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingMaterial_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCompositingMaterial>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "CompositingMaterial" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingMaterial, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_Material_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings_ValueProp = { "ParamPassMappings", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings_Key_KeyProp = { "ParamPassMappings_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings_MetaData[] = {
		{ "Category", "CompositingMaterial" },
		{ "Comment", "/** Maps material texture param names to prior passes/elements. Overrides the element's param mapping list above. */" },
		{ "DisplayName", "Input Elements" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
		{ "ReadOnlyKeys", "" },
		{ "ToolTip", "Maps material texture param names to prior passes/elements. Overrides the element's param mapping list above." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings = { "ParamPassMappings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingMaterial, ParamPassMappings), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams_ValueProp = { "RequiredMaterialParams", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FNamedCompMaterialParam, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams_Key_KeyProp = { "RequiredMaterialParams_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams_MetaData[] = {
		{ "Category", "CompositingMaterial" },
		{ "DisplayName", "Expected Param Mappings" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams = { "RequiredMaterialParams", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingMaterial, RequiredMaterialParams), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams_MetaData)) };
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_EditorHiddenParams_Inner = { "EditorHiddenParams", nullptr, (EPropertyFlags)0x0000000800000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_EditorHiddenParams_MetaData[] = {
		{ "Category", "CompositingMaterial|Editor" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_EditorHiddenParams = { "EditorHiddenParams", nullptr, (EPropertyFlags)0x0010000800010001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingMaterial, EditorHiddenParams), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_EditorHiddenParams_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_EditorHiddenParams_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies_ValueProp = { "VectorOverrideProxies", nullptr, (EPropertyFlags)0x0000000800000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies_Key_KeyProp = { "VectorOverrideProxies_Key", nullptr, (EPropertyFlags)0x0000000800000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies_MetaData[] = {
		{ "Category", "CompositingMaterial|Editor" },
		{ "Comment", "/** Required for customizing the color picker widget - need a property to wrap (one for each material param). */" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
		{ "ReadOnlyKeys", "" },
		{ "ToolTip", "Required for customizing the color picker widget - need a property to wrap (one for each material param)." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies = { "VectorOverrideProxies", nullptr, (EPropertyFlags)0x0040000800000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingMaterial, VectorOverrideProxies), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_CachedMID_MetaData[] = {
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_CachedMID = { "CachedMID", nullptr, (EPropertyFlags)0x00c0000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingMaterial, CachedMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_CachedMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_CachedMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCompositingMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_ParamPassMappings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_RequiredMaterialParams,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_EditorHiddenParams_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_EditorHiddenParams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_VectorOverrideProxies,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingMaterial_Statics::NewProp_CachedMID,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCompositingMaterial_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		Z_Construct_UScriptStruct_FCompositingParamPayload,
		&NewStructOps,
		"CompositingMaterial",
		sizeof(FCompositingMaterial),
		alignof(FCompositingMaterial),
		Z_Construct_UScriptStruct_FCompositingMaterial_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingMaterial_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCompositingMaterial()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCompositingMaterial_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CompositingMaterial"), sizeof(FCompositingMaterial), Get_Z_Construct_UScriptStruct_FCompositingMaterial_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCompositingMaterial_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCompositingMaterial_Hash() { return 2242614960U; }
class UScriptStruct* FNamedCompMaterialParam::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FNamedCompMaterialParam_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNamedCompMaterialParam, Z_Construct_UPackage__Script_Composure(), TEXT("NamedCompMaterialParam"), sizeof(FNamedCompMaterialParam), Get_Z_Construct_UScriptStruct_FNamedCompMaterialParam_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FNamedCompMaterialParam>()
{
	return FNamedCompMaterialParam::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNamedCompMaterialParam(FNamedCompMaterialParam::StaticStruct, TEXT("/Script/Composure"), TEXT("NamedCompMaterialParam"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFNamedCompMaterialParam
{
	FScriptStruct_Composure_StaticRegisterNativesFNamedCompMaterialParam()
	{
		UScriptStruct::DeferCppStructOps<FNamedCompMaterialParam>(FName(TEXT("NamedCompMaterialParam")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFNamedCompMaterialParam;
	struct Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ParamType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParamType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ParamType;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParamName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ParamName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNamedCompMaterialParam>();
	}
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamType_MetaData[] = {
		{ "Category", "CompositingMaterial" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamType = { "ParamType", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNamedCompMaterialParam, ParamType), Z_Construct_UEnum_Composure_EParamType, METADATA_PARAMS(Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamType_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamName_MetaData[] = {
		{ "Category", "CompositingMaterial" },
		{ "DisplayName", "Default Parameter Name" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamName = { "ParamName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNamedCompMaterialParam, ParamName), METADATA_PARAMS(Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamType,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::NewProp_ParamName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		nullptr,
		&NewStructOps,
		"NamedCompMaterialParam",
		sizeof(FNamedCompMaterialParam),
		alignof(FNamedCompMaterialParam),
		Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNamedCompMaterialParam()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNamedCompMaterialParam_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NamedCompMaterialParam"), sizeof(FNamedCompMaterialParam), Get_Z_Construct_UScriptStruct_FNamedCompMaterialParam_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNamedCompMaterialParam_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNamedCompMaterialParam_Hash() { return 3724283912U; }
class UScriptStruct* FCompositingParamPayload::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FCompositingParamPayload_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCompositingParamPayload, Z_Construct_UPackage__Script_Composure(), TEXT("CompositingParamPayload"), sizeof(FCompositingParamPayload), Get_Z_Construct_UScriptStruct_FCompositingParamPayload_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FCompositingParamPayload>()
{
	return FCompositingParamPayload::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCompositingParamPayload(FCompositingParamPayload::StaticStruct, TEXT("/Script/Composure"), TEXT("CompositingParamPayload"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFCompositingParamPayload
{
	FScriptStruct_Composure_StaticRegisterNativesFCompositingParamPayload()
	{
		UScriptStruct::DeferCppStructOps<FCompositingParamPayload>(FName(TEXT("CompositingParamPayload")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFCompositingParamPayload;
	struct Z_Construct_UScriptStruct_FCompositingParamPayload_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ScalarParamOverrides_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ScalarParamOverrides_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScalarParamOverrides_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ScalarParamOverrides;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VectorParamOverrides_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_VectorParamOverrides_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VectorParamOverrides_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_VectorParamOverrides;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* FCompositingParamPayload\n *****************************************************************************/" },
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
		{ "ToolTip", "FCompositingParamPayload" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCompositingParamPayload>();
	}
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides_ValueProp = { "ScalarParamOverrides", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides_Key_KeyProp = { "ScalarParamOverrides_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides_MetaData[] = {
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides = { "ScalarParamOverrides", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingParamPayload, ScalarParamOverrides), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides_ValueProp = { "VectorParamOverrides", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides_Key_KeyProp = { "VectorParamOverrides_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides_MetaData[] = {
		{ "ModuleRelativePath", "Public/CompositingElements/CompositingMaterialPass.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides = { "VectorParamOverrides", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCompositingParamPayload, VectorParamOverrides), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_ScalarParamOverrides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::NewProp_VectorParamOverrides,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		nullptr,
		&NewStructOps,
		"CompositingParamPayload",
		sizeof(FCompositingParamPayload),
		alignof(FCompositingParamPayload),
		Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCompositingParamPayload()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCompositingParamPayload_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CompositingParamPayload"), sizeof(FCompositingParamPayload), Get_Z_Construct_UScriptStruct_FCompositingParamPayload_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCompositingParamPayload_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCompositingParamPayload_Hash() { return 868679879U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
