// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ACompositingElement;
class USceneCaptureComponent2D;
#ifdef COMPOSURE_MovieSceneComposureExportSectionTemplate_generated_h
#error "MovieSceneComposureExportSectionTemplate.generated.h already included, missing '#pragma once' in MovieSceneComposureExportSectionTemplate.h"
#endif
#define COMPOSURE_MovieSceneComposureExportSectionTemplate_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics; \
	COMPOSURE_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Pass() { return STRUCT_OFFSET(FMovieSceneComposureExportSectionTemplate, Pass); } \
	typedef FMovieSceneEvalTemplate Super;


template<> COMPOSURE_API UScriptStruct* StaticStruct<struct FMovieSceneComposureExportSectionTemplate>();

#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExportSceneCaptureBuffers);


#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExportSceneCaptureBuffers);


#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneComposureExportInitializer(); \
	friend struct Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposureExportInitializer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneComposureExportInitializer)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneComposureExportInitializer(); \
	friend struct Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposureExportInitializer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneComposureExportInitializer)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneComposureExportInitializer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposureExportInitializer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneComposureExportInitializer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposureExportInitializer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneComposureExportInitializer(UMovieSceneComposureExportInitializer&&); \
	NO_API UMovieSceneComposureExportInitializer(const UMovieSceneComposureExportInitializer&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneComposureExportInitializer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneComposureExportInitializer(UMovieSceneComposureExportInitializer&&); \
	NO_API UMovieSceneComposureExportInitializer(const UMovieSceneComposureExportInitializer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneComposureExportInitializer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposureExportInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposureExportInitializer)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_21_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h_26_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UMovieSceneComposureExportInitializer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposureExportSectionTemplate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
