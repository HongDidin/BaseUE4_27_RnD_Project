// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/CompositingElements/CompositingElementOutputs.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompositingElementOutputs() {}
// Cross Module References
	COMPOSURE_API UEnum* Z_Construct_UEnum_Composure_EExrCompressionOptions();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UClass* Z_Construct_UClass_UColorConverterOutputPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UColorConverterOutputPass();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingElementOutput();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingElementTransform_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompEditorImagePreviewInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingMediaCaptureOutput_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingMediaCaptureOutput();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_URenderTargetCompositingOutput_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_URenderTargetCompositingOutput();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UEXRFileCompositingOutput_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UEXRFileCompositingOutput();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
// End Cross Module References
	static UEnum* EExrCompressionOptions_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Composure_EExrCompressionOptions, Z_Construct_UPackage__Script_Composure(), TEXT("EExrCompressionOptions"));
		}
		return Singleton;
	}
	template<> COMPOSURE_API UEnum* StaticEnum<EExrCompressionOptions>()
	{
		return EExrCompressionOptions_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExrCompressionOptions(EExrCompressionOptions_StaticEnum, TEXT("/Script/Composure"), TEXT("EExrCompressionOptions"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Composure_EExrCompressionOptions_Hash() { return 1765453171U; }
	UEnum* Z_Construct_UEnum_Composure_EExrCompressionOptions()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExrCompressionOptions"), 0, Get_Z_Construct_UEnum_Composure_EExrCompressionOptions_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EExrCompressionOptions::Compressed", (int64)EExrCompressionOptions::Compressed },
				{ "EExrCompressionOptions::Uncompressed", (int64)EExrCompressionOptions::Uncompressed },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/* UEXRFileCompositingOutput\n *****************************************************************************/" },
				{ "Compressed.Name", "EExrCompressionOptions::Compressed" },
				{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
				{ "ToolTip", "UEXRFileCompositingOutput" },
				{ "Uncompressed.Name", "EExrCompressionOptions::Uncompressed" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Composure,
				nullptr,
				"EExrCompressionOptions",
				"EExrCompressionOptions",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UColorConverterOutputPass::StaticRegisterNativesUColorConverterOutputPass()
	{
	}
	UClass* Z_Construct_UClass_UColorConverterOutputPass_NoRegister()
	{
		return UColorConverterOutputPass::StaticClass();
	}
	struct Z_Construct_UClass_UColorConverterOutputPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorConverter_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ColorConverter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultConverterClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultConverterClass;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewResult;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UColorConverterOutputPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementOutput,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UColorConverterOutputPass_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CompositingElements/CompositingElementOutputs.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_ColorConverter_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "DisplayName", "Color Conversion" },
		{ "EditCondition", "bEnabled" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_ColorConverter = { "ColorConverter", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UColorConverterOutputPass, ColorConverter), Z_Construct_UClass_UCompositingElementTransform_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_ColorConverter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_ColorConverter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_DefaultConverterClass_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_DefaultConverterClass = { "DefaultConverterClass", nullptr, (EPropertyFlags)0x0024080000002000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UColorConverterOutputPass, DefaultConverterClass), Z_Construct_UClass_UCompositingElementTransform_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_DefaultConverterClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_DefaultConverterClass_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_PreviewResult_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_PreviewResult = { "PreviewResult", nullptr, (EPropertyFlags)0x0020080800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UColorConverterOutputPass, PreviewResult), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_PreviewResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_PreviewResult_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UColorConverterOutputPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_ColorConverter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_DefaultConverterClass,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UColorConverterOutputPass_Statics::NewProp_PreviewResult,
#endif // WITH_EDITORONLY_DATA
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UColorConverterOutputPass_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UCompEditorImagePreviewInterface_NoRegister, (int32)VTABLE_OFFSET(UColorConverterOutputPass, ICompEditorImagePreviewInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UColorConverterOutputPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UColorConverterOutputPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UColorConverterOutputPass_Statics::ClassParams = {
		&UColorConverterOutputPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UColorConverterOutputPass_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UColorConverterOutputPass_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x049010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UColorConverterOutputPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UColorConverterOutputPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UColorConverterOutputPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UColorConverterOutputPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UColorConverterOutputPass, 1548287233);
	template<> COMPOSURE_API UClass* StaticClass<UColorConverterOutputPass>()
	{
		return UColorConverterOutputPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UColorConverterOutputPass(Z_Construct_UClass_UColorConverterOutputPass, &UColorConverterOutputPass::StaticClass, TEXT("/Script/Composure"), TEXT("UColorConverterOutputPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UColorConverterOutputPass);
	void UCompositingMediaCaptureOutput::StaticRegisterNativesUCompositingMediaCaptureOutput()
	{
	}
	UClass* Z_Construct_UClass_UCompositingMediaCaptureOutput_NoRegister()
	{
		return UCompositingMediaCaptureOutput::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CaptureOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActiveCapture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UColorConverterOutputPass,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CompositingElements/CompositingElementOutputs.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_CaptureOutput_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_CaptureOutput = { "CaptureOutput", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingMediaCaptureOutput, CaptureOutput), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_CaptureOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_CaptureOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_ActiveCapture_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_ActiveCapture = { "ActiveCapture", nullptr, (EPropertyFlags)0x00c0000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingMediaCaptureOutput, ActiveCapture), Z_Construct_UClass_UMediaCapture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_ActiveCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_ActiveCapture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_CaptureOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::NewProp_ActiveCapture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingMediaCaptureOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::ClassParams = {
		&UCompositingMediaCaptureOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingMediaCaptureOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingMediaCaptureOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingMediaCaptureOutput, 4035220138);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingMediaCaptureOutput>()
	{
		return UCompositingMediaCaptureOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingMediaCaptureOutput(Z_Construct_UClass_UCompositingMediaCaptureOutput, &UCompositingMediaCaptureOutput::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingMediaCaptureOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingMediaCaptureOutput);
	void URenderTargetCompositingOutput::StaticRegisterNativesURenderTargetCompositingOutput()
	{
	}
	UClass* Z_Construct_UClass_URenderTargetCompositingOutput_NoRegister()
	{
		return URenderTargetCompositingOutput::StaticClass();
	}
	struct Z_Construct_UClass_URenderTargetCompositingOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URenderTargetCompositingOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementOutput,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderTargetCompositingOutput_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CompositingElements/CompositingElementOutputs.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URenderTargetCompositingOutput_Statics::NewProp_RenderTarget_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URenderTargetCompositingOutput_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URenderTargetCompositingOutput, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URenderTargetCompositingOutput_Statics::NewProp_RenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URenderTargetCompositingOutput_Statics::NewProp_RenderTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URenderTargetCompositingOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URenderTargetCompositingOutput_Statics::NewProp_RenderTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URenderTargetCompositingOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URenderTargetCompositingOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URenderTargetCompositingOutput_Statics::ClassParams = {
		&URenderTargetCompositingOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URenderTargetCompositingOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URenderTargetCompositingOutput_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_URenderTargetCompositingOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URenderTargetCompositingOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URenderTargetCompositingOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URenderTargetCompositingOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URenderTargetCompositingOutput, 3422809744);
	template<> COMPOSURE_API UClass* StaticClass<URenderTargetCompositingOutput>()
	{
		return URenderTargetCompositingOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URenderTargetCompositingOutput(Z_Construct_UClass_URenderTargetCompositingOutput, &URenderTargetCompositingOutput::StaticClass, TEXT("/Script/Composure"), TEXT("URenderTargetCompositingOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URenderTargetCompositingOutput);
	void UEXRFileCompositingOutput::StaticRegisterNativesUEXRFileCompositingOutput()
	{
	}
	UClass* Z_Construct_UClass_UEXRFileCompositingOutput_NoRegister()
	{
		return UEXRFileCompositingOutput::StaticClass();
	}
	struct Z_Construct_UClass_UEXRFileCompositingOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputDirectiory_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputDirectiory;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilenameFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilenameFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputFrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutputFrameRate;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Compression_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Compression_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Compression;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEXRFileCompositingOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementOutput,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEXRFileCompositingOutput_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CompositingElements/CompositingElementOutputs.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputDirectiory_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputDirectiory = { "OutputDirectiory", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEXRFileCompositingOutput, OutputDirectiory), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputDirectiory_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputDirectiory_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_FilenameFormat_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "Comment", "/**\n\x09 * The format to use for the resulting filename. Extension will be added automatically. Any tokens of the form {token} will be replaced with the corresponding value:\n\x09 * {frame} - The current frame number\n\x09 */" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
		{ "ToolTip", "The format to use for the resulting filename. Extension will be added automatically. Any tokens of the form {token} will be replaced with the corresponding value:\n{frame} - The current frame number" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_FilenameFormat = { "FilenameFormat", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEXRFileCompositingOutput, FilenameFormat), METADATA_PARAMS(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_FilenameFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_FilenameFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputFrameRate_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "ClampMax", "200" },
		{ "ClampMin", "1" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
		{ "UIMax", "200" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputFrameRate = { "OutputFrameRate", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEXRFileCompositingOutput, OutputFrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputFrameRate_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_Compression_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_Compression_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementOutputs.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_Compression = { "Compression", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEXRFileCompositingOutput, Compression), Z_Construct_UEnum_Composure_EExrCompressionOptions, METADATA_PARAMS(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_Compression_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_Compression_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEXRFileCompositingOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputDirectiory,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_FilenameFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_OutputFrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_Compression_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEXRFileCompositingOutput_Statics::NewProp_Compression,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEXRFileCompositingOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEXRFileCompositingOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEXRFileCompositingOutput_Statics::ClassParams = {
		&UEXRFileCompositingOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEXRFileCompositingOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEXRFileCompositingOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEXRFileCompositingOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEXRFileCompositingOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEXRFileCompositingOutput, 456268071);
	template<> COMPOSURE_API UClass* StaticClass<UEXRFileCompositingOutput>()
	{
		return UEXRFileCompositingOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEXRFileCompositingOutput(Z_Construct_UClass_UEXRFileCompositingOutput, &UEXRFileCompositingOutput::StaticClass, TEXT("/Script/Composure"), TEXT("UEXRFileCompositingOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEXRFileCompositingOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
