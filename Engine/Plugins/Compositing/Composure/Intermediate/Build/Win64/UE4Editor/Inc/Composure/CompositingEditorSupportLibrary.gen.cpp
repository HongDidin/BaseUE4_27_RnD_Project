// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Private/EditorSupport/CompositingEditorSupportLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompositingEditorSupportLibrary() {}
// Cross Module References
	COMPOSURE_API UFunction* Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingPickerAsyncTask_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingPickerAsyncTask();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintAsyncActionBase();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompImageColorPickerInterface_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics
	{
		struct _Script_Composure_eventOnPixelPicked_Parms
		{
			FVector2D PickedUV;
			FLinearColor SampledColor;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PickedUV_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PickedUV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SampledColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SampledColor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_PickedUV_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_PickedUV = { "PickedUV", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Composure_eventOnPixelPicked_Parms, PickedUV), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_PickedUV_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_PickedUV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_SampledColor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_SampledColor = { "SampledColor", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Composure_eventOnPixelPicked_Parms, SampledColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_SampledColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_SampledColor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_PickedUV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::NewProp_SampledColor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/* UCompositingPickerAsyncTask\n *****************************************************************************/" },
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
		{ "ToolTip", "UCompositingPickerAsyncTask" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Composure, nullptr, "OnPixelPicked__DelegateSignature", nullptr, nullptr, sizeof(_Script_Composure_eventOnPixelPicked_Parms), Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UCompositingPickerAsyncTask::execOpenCompositingPicker)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_PickerTarget);
		P_GET_OBJECT(UTexture,Z_Param_DisplayImage);
		P_GET_PROPERTY(FTextProperty,Z_Param_WindowTitle);
		P_GET_UBOOL(Z_Param_bAverageColorOnDrag);
		P_GET_UBOOL(Z_Param_bUseImplicitGamma);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCompositingPickerAsyncTask**)Z_Param__Result=UCompositingPickerAsyncTask::OpenCompositingPicker(Z_Param_PickerTarget,Z_Param_DisplayImage,Z_Param_WindowTitle,Z_Param_bAverageColorOnDrag,Z_Param_bUseImplicitGamma);
		P_NATIVE_END;
	}
	void UCompositingPickerAsyncTask::StaticRegisterNativesUCompositingPickerAsyncTask()
	{
		UClass* Class = UCompositingPickerAsyncTask::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OpenCompositingPicker", &UCompositingPickerAsyncTask::execOpenCompositingPicker },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics
	{
		struct CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms
		{
			UTextureRenderTarget2D* PickerTarget;
			UTexture* DisplayImage;
			FText WindowTitle;
			bool bAverageColorOnDrag;
			bool bUseImplicitGamma;
			UCompositingPickerAsyncTask* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PickerTarget;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplayImage;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_WindowTitle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAverageColorOnDrag_MetaData[];
#endif
		static void NewProp_bAverageColorOnDrag_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAverageColorOnDrag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseImplicitGamma_MetaData[];
#endif
		static void NewProp_bUseImplicitGamma_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseImplicitGamma;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_PickerTarget = { "PickerTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms, PickerTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_DisplayImage = { "DisplayImage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms, DisplayImage), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_WindowTitle = { "WindowTitle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms, WindowTitle), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bAverageColorOnDrag_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bAverageColorOnDrag_SetBit(void* Obj)
	{
		((CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms*)Obj)->bAverageColorOnDrag = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bAverageColorOnDrag = { "bAverageColorOnDrag", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms), &Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bAverageColorOnDrag_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bAverageColorOnDrag_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bAverageColorOnDrag_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bUseImplicitGamma_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bUseImplicitGamma_SetBit(void* Obj)
	{
		((CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms*)Obj)->bUseImplicitGamma = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bUseImplicitGamma = { "bUseImplicitGamma", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms), &Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bUseImplicitGamma_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bUseImplicitGamma_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bUseImplicitGamma_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms, ReturnValue), Z_Construct_UClass_UCompositingPickerAsyncTask_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_PickerTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_DisplayImage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_WindowTitle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bAverageColorOnDrag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_bUseImplicitGamma,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::Function_MetaDataParams[] = {
		{ "BlueprintInternalUseOnly", "true" },
		{ "Category", "Composure|Editor" },
		{ "CPP_Default_bAverageColorOnDrag", "true" },
		{ "CPP_Default_bUseImplicitGamma", "true" },
		{ "DevelopmentOnly", "" },
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingPickerAsyncTask, nullptr, "OpenCompositingPicker", nullptr, nullptr, sizeof(CompositingPickerAsyncTask_eventOpenCompositingPicker_Parms), Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCompositingPickerAsyncTask_NoRegister()
	{
		return UCompositingPickerAsyncTask::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingPickerAsyncTask_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPick_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnPick;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnCancel_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnCancel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnAccept_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnAccept;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PickerTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PickerTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PickerDisplayImage_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PickerDisplayImage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintAsyncActionBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCompositingPickerAsyncTask_OpenCompositingPicker, "OpenCompositingPicker" }, // 116276409
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EditorSupport/CompositingEditorSupportLibrary.h" },
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnPick_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnPick = { "OnPick", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingPickerAsyncTask, OnPick), Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnPick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnPick_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnCancel_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnCancel = { "OnCancel", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingPickerAsyncTask, OnCancel), Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnCancel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnCancel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnAccept_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnAccept = { "OnAccept", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingPickerAsyncTask, OnAccept), Z_Construct_UDelegateFunction_Composure_OnPixelPicked__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnAccept_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnAccept_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerTarget_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerTarget = { "PickerTarget", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingPickerAsyncTask, PickerTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerDisplayImage_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditorSupport/CompositingEditorSupportLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerDisplayImage = { "PickerDisplayImage", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingPickerAsyncTask, PickerDisplayImage), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerDisplayImage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerDisplayImage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnPick,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnCancel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_OnAccept,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::NewProp_PickerDisplayImage,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UCompImageColorPickerInterface_NoRegister, (int32)VTABLE_OFFSET(UCompositingPickerAsyncTask, ICompImageColorPickerInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingPickerAsyncTask>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::ClassParams = {
		&UCompositingPickerAsyncTask::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingPickerAsyncTask()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingPickerAsyncTask_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingPickerAsyncTask, 2446324738);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingPickerAsyncTask>()
	{
		return UCompositingPickerAsyncTask::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingPickerAsyncTask(Z_Construct_UClass_UCompositingPickerAsyncTask, &UCompositingPickerAsyncTask::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingPickerAsyncTask"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingPickerAsyncTask);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
