// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_MovieSceneComposurePostMoveSettingsTrack_generated_h
#error "MovieSceneComposurePostMoveSettingsTrack.generated.h already included, missing '#pragma once' in MovieSceneComposurePostMoveSettingsTrack.h"
#endif
#define COMPOSURE_MovieSceneComposurePostMoveSettingsTrack_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneComposurePostMoveSettingsTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposurePostMoveSettingsTrack, UMovieScenePropertyTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposurePostMoveSettingsTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneComposurePostMoveSettingsTrack*>(this); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneComposurePostMoveSettingsTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposurePostMoveSettingsTrack, UMovieScenePropertyTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposurePostMoveSettingsTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneComposurePostMoveSettingsTrack*>(this); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposurePostMoveSettingsTrack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposurePostMoveSettingsTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposurePostMoveSettingsTrack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsTrack(UMovieSceneComposurePostMoveSettingsTrack&&); \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsTrack(const UMovieSceneComposurePostMoveSettingsTrack&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsTrack(UMovieSceneComposurePostMoveSettingsTrack&&); \
	COMPOSURE_API UMovieSceneComposurePostMoveSettingsTrack(const UMovieSceneComposurePostMoveSettingsTrack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposurePostMoveSettingsTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposurePostMoveSettingsTrack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposurePostMoveSettingsTrack)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_12_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UMovieSceneComposurePostMoveSettingsTrack>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposurePostMoveSettingsTrack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
