// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ComposureLayersEditor/Private/EditorCompElementContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditorCompElementContainer() {}
// Cross Module References
	COMPOSURELAYERSEDITOR_API UClass* Z_Construct_UClass_UEditorCompElementContainer_NoRegister();
	COMPOSURELAYERSEDITOR_API UClass* Z_Construct_UClass_UEditorCompElementContainer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ComposureLayersEditor();
	COMPOSURE_API UClass* Z_Construct_UClass_ACompositingElement_NoRegister();
// End Cross Module References
	void UEditorCompElementContainer::StaticRegisterNativesUEditorCompElementContainer()
	{
	}
	UClass* Z_Construct_UClass_UEditorCompElementContainer_NoRegister()
	{
		return UEditorCompElementContainer::StaticClass();
	}
	struct Z_Construct_UClass_UEditorCompElementContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_CompElements_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompElements_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CompElements;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEditorCompElementContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ComposureLayersEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorCompElementContainer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UObject for tracking our list of in-level composure actors - wrapped by a\n * UObject to mimic the UWorld::Layers property (hooks into undo/redo easily, etc.)\n */" },
		{ "IncludePath", "EditorCompElementContainer.h" },
		{ "ModuleRelativePath", "Private/EditorCompElementContainer.h" },
		{ "ToolTip", "UObject for tracking our list of in-level composure actors - wrapped by a\nUObject to mimic the UWorld::Layers property (hooks into undo/redo easily, etc.)" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UEditorCompElementContainer_Statics::NewProp_CompElements_Inner = { "CompElements", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ACompositingElement_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEditorCompElementContainer_Statics::NewProp_CompElements_MetaData[] = {
		{ "ModuleRelativePath", "Private/EditorCompElementContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEditorCompElementContainer_Statics::NewProp_CompElements = { "CompElements", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEditorCompElementContainer, CompElements), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEditorCompElementContainer_Statics::NewProp_CompElements_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorCompElementContainer_Statics::NewProp_CompElements_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEditorCompElementContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorCompElementContainer_Statics::NewProp_CompElements_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEditorCompElementContainer_Statics::NewProp_CompElements,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEditorCompElementContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEditorCompElementContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEditorCompElementContainer_Statics::ClassParams = {
		&UEditorCompElementContainer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEditorCompElementContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEditorCompElementContainer_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEditorCompElementContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEditorCompElementContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEditorCompElementContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEditorCompElementContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEditorCompElementContainer, 3972731280);
	template<> COMPOSURELAYERSEDITOR_API UClass* StaticClass<UEditorCompElementContainer>()
	{
		return UEditorCompElementContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEditorCompElementContainer(Z_Construct_UClass_UEditorCompElementContainer, &UEditorCompElementContainer::StaticClass, TEXT("/Script/ComposureLayersEditor"), TEXT("UEditorCompElementContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEditorCompElementContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
