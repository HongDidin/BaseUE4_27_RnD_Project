// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Private/MovieScene/MovieSceneComposurePostMoveSettingsSectionTemplate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneComposurePostMoveSettingsSectionTemplate() {}
// Cross Module References
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate();
	UPackage* Z_Construct_UPackage__Script_Composure();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieScenePropertySectionTemplate();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneFloatChannel();
	MOVIESCENE_API UEnum* Z_Construct_UEnum_MovieScene_EMovieSceneBlendType();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneComposurePostMoveSettingsSectionTemplate>() == std::is_polymorphic<FMovieScenePropertySectionTemplate>(), "USTRUCT FMovieSceneComposurePostMoveSettingsSectionTemplate cannot be polymorphic unless super FMovieScenePropertySectionTemplate is polymorphic");

class UScriptStruct* FMovieSceneComposurePostMoveSettingsSectionTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate, Z_Construct_UPackage__Script_Composure(), TEXT("MovieSceneComposurePostMoveSettingsSectionTemplate"), sizeof(FMovieSceneComposurePostMoveSettingsSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FMovieSceneComposurePostMoveSettingsSectionTemplate>()
{
	return FMovieSceneComposurePostMoveSettingsSectionTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate(FMovieSceneComposurePostMoveSettingsSectionTemplate::StaticStruct, TEXT("/Script/Composure"), TEXT("MovieSceneComposurePostMoveSettingsSectionTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposurePostMoveSettingsSectionTemplate
{
	FScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposurePostMoveSettingsSectionTemplate()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneComposurePostMoveSettingsSectionTemplate>(FName(TEXT("MovieSceneComposurePostMoveSettingsSectionTemplate")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposurePostMoveSettingsSectionTemplate;
	struct Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pivot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pivot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Translation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Translation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Scale;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BlendType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BlendType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** A movie scene evaluation template for post move settings sections. */" },
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposurePostMoveSettingsSectionTemplate.h" },
		{ "ToolTip", "A movie scene evaluation template for post move settings sections." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneComposurePostMoveSettingsSectionTemplate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Pivot_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposurePostMoveSettingsSectionTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Pivot = { "Pivot", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(Pivot, FMovieSceneComposurePostMoveSettingsSectionTemplate), STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, Pivot), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Pivot_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Pivot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Translation_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposurePostMoveSettingsSectionTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Translation = { "Translation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(Translation, FMovieSceneComposurePostMoveSettingsSectionTemplate), STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, Translation), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Translation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Translation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_RotationAngle_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposurePostMoveSettingsSectionTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_RotationAngle = { "RotationAngle", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, RotationAngle), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_RotationAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_RotationAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Scale_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposurePostMoveSettingsSectionTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, Scale), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Scale_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_BlendType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_BlendType_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposurePostMoveSettingsSectionTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_BlendType = { "BlendType", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, BlendType), Z_Construct_UEnum_MovieScene_EMovieSceneBlendType, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_BlendType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_BlendType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Pivot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Translation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_RotationAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_Scale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_BlendType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::NewProp_BlendType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		Z_Construct_UScriptStruct_FMovieScenePropertySectionTemplate,
		&NewStructOps,
		"MovieSceneComposurePostMoveSettingsSectionTemplate",
		sizeof(FMovieSceneComposurePostMoveSettingsSectionTemplate),
		alignof(FMovieSceneComposurePostMoveSettingsSectionTemplate),
		Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneComposurePostMoveSettingsSectionTemplate"), sizeof(FMovieSceneComposurePostMoveSettingsSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Hash() { return 4201636019U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
