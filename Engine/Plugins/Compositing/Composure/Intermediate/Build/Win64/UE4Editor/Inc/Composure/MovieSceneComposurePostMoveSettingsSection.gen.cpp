// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/MovieScene/MovieSceneComposurePostMoveSettingsSection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneComposurePostMoveSettingsSection() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection();
	UPackage* Z_Construct_UPackage__Script_Composure();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneFloatChannel();
// End Cross Module References
	void UMovieSceneComposurePostMoveSettingsSection::StaticRegisterNativesUMovieSceneComposurePostMoveSettingsSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_NoRegister()
	{
		return UMovieSceneComposurePostMoveSettingsSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pivot_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pivot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Translation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Translation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RotationAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RotationAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Scale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Scale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneSection,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* A movie scene section for animating FComposurePostMoveSettings properties.\n*/" },
		{ "IncludePath", "MovieScene/MovieSceneComposurePostMoveSettingsSection.h" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposurePostMoveSettingsSection.h" },
		{ "ToolTip", "A movie scene section for animating FComposurePostMoveSettings properties." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Pivot_MetaData[] = {
		{ "Comment", "/** The curves for animating the pivot property. */" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposurePostMoveSettingsSection.h" },
		{ "ToolTip", "The curves for animating the pivot property." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Pivot = { "Pivot", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(Pivot, UMovieSceneComposurePostMoveSettingsSection), STRUCT_OFFSET(UMovieSceneComposurePostMoveSettingsSection, Pivot), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Pivot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Pivot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Translation_MetaData[] = {
		{ "Comment", "/** The curves for animating the translation property. */" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposurePostMoveSettingsSection.h" },
		{ "ToolTip", "The curves for animating the translation property." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Translation = { "Translation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(Translation, UMovieSceneComposurePostMoveSettingsSection), STRUCT_OFFSET(UMovieSceneComposurePostMoveSettingsSection, Translation), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Translation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Translation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_RotationAngle_MetaData[] = {
		{ "Comment", "/** The curve for animating the rotation angle property. */" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposurePostMoveSettingsSection.h" },
		{ "ToolTip", "The curve for animating the rotation angle property." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_RotationAngle = { "RotationAngle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneComposurePostMoveSettingsSection, RotationAngle), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_RotationAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_RotationAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Scale_MetaData[] = {
		{ "Comment", "/** The curve for animating the scale property. */" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposurePostMoveSettingsSection.h" },
		{ "ToolTip", "The curve for animating the scale property." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Scale = { "Scale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneComposurePostMoveSettingsSection, Scale), Z_Construct_UScriptStruct_FMovieSceneFloatChannel, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Scale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Scale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Pivot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Translation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_RotationAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::NewProp_Scale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneComposurePostMoveSettingsSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::ClassParams = {
		&UMovieSceneComposurePostMoveSettingsSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::PropPointers),
		0,
		0x002800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneComposurePostMoveSettingsSection, 3759674185);
	template<> COMPOSURE_API UClass* StaticClass<UMovieSceneComposurePostMoveSettingsSection>()
	{
		return UMovieSceneComposurePostMoveSettingsSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneComposurePostMoveSettingsSection(Z_Construct_UClass_UMovieSceneComposurePostMoveSettingsSection, &UMovieSceneComposurePostMoveSettingsSection::StaticClass, TEXT("/Script/Composure"), TEXT("UMovieSceneComposurePostMoveSettingsSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneComposurePostMoveSettingsSection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
