// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/ComposurePostProcessPass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposurePostProcessPass() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPass();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	UPackage* Z_Construct_UPackage__Script_Composure();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessBlendable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UComposurePostProcessPass::execSetOutputRenderTarget)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_RenderTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetOutputRenderTarget(Z_Param_RenderTarget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UComposurePostProcessPass::execGetOutputRenderTarget)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTextureRenderTarget2D**)Z_Param__Result=P_THIS->GetOutputRenderTarget();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UComposurePostProcessPass::execGetSetupMaterial)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMaterialInterface**)Z_Param__Result=P_THIS->GetSetupMaterial();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UComposurePostProcessPass::execSetSetupMaterial)
	{
		P_GET_OBJECT(UMaterialInterface,Z_Param_Material);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSetupMaterial(Z_Param_Material);
		P_NATIVE_END;
	}
	void UComposurePostProcessPass::StaticRegisterNativesUComposurePostProcessPass()
	{
		UClass* Class = UComposurePostProcessPass::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetOutputRenderTarget", &UComposurePostProcessPass::execGetOutputRenderTarget },
			{ "GetSetupMaterial", &UComposurePostProcessPass::execGetSetupMaterial },
			{ "SetOutputRenderTarget", &UComposurePostProcessPass::execSetOutputRenderTarget },
			{ "SetSetupMaterial", &UComposurePostProcessPass::execSetSetupMaterial },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics
	{
		struct ComposurePostProcessPass_eventGetOutputRenderTarget_Parms
		{
			UTextureRenderTarget2D* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessPass_eventGetOutputRenderTarget_Parms, ReturnValue), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Outputs" },
		{ "Comment", "/** \n\x09 * Gets current output render target.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Gets current output render target." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePostProcessPass, nullptr, "GetOutputRenderTarget", nullptr, nullptr, sizeof(ComposurePostProcessPass_eventGetOutputRenderTarget_Parms), Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics
	{
		struct ComposurePostProcessPass_eventGetSetupMaterial_Parms
		{
			UMaterialInterface* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessPass_eventGetSetupMaterial_Parms, ReturnValue), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::Function_MetaDataParams[] = {
		{ "Category", "Inputs" },
		{ "Comment", "/** Gets current setup material. */" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Gets current setup material." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePostProcessPass, nullptr, "GetSetupMaterial", nullptr, nullptr, sizeof(ComposurePostProcessPass_eventGetSetupMaterial_Parms), Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics
	{
		struct ComposurePostProcessPass_eventSetOutputRenderTarget_Parms
		{
			UTextureRenderTarget2D* RenderTarget;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessPass_eventSetOutputRenderTarget_Parms, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::NewProp_RenderTarget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Outputs" },
		{ "Comment", "/** \n\x09 * Sets current output render target.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Sets current output render target." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePostProcessPass, nullptr, "SetOutputRenderTarget", nullptr, nullptr, sizeof(ComposurePostProcessPass_eventSetOutputRenderTarget_Parms), Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics
	{
		struct ComposurePostProcessPass_eventSetSetupMaterial_Parms
		{
			UMaterialInterface* Material;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePostProcessPass_eventSetSetupMaterial_Parms, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::NewProp_Material,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::Function_MetaDataParams[] = {
		{ "Category", "Inputs" },
		{ "Comment", "/** \n\x09 * Sets a custom setup post process material. The material location must be set at BeforeTranslucency.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Sets a custom setup post process material. The material location must be set at BeforeTranslucency." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePostProcessPass, nullptr, "SetSetupMaterial", nullptr, nullptr, sizeof(ComposurePostProcessPass_eventSetSetupMaterial_Parms), Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UComposurePostProcessPass_NoRegister()
	{
		return UComposurePostProcessPass::StaticClass();
	}
	struct Z_Construct_UClass_UComposurePostProcessPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendableInterface_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BlendableInterface;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SetupMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SetupMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TonemapperReplacement_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TonemapperReplacement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposurePostProcessPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UComposurePostProcessPass_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UComposurePostProcessPass_GetOutputRenderTarget, "GetOutputRenderTarget" }, // 2486197601
		{ &Z_Construct_UFunction_UComposurePostProcessPass_GetSetupMaterial, "GetSetupMaterial" }, // 1720721196
		{ &Z_Construct_UFunction_UComposurePostProcessPass_SetOutputRenderTarget, "SetOutputRenderTarget" }, // 1241208423
		{ &Z_Construct_UFunction_UComposurePostProcessPass_SetSetupMaterial, "SetSetupMaterial" }, // 3115544650
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessPass_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * In engine post process based pass.\n */" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "ComposurePostProcessPass.h" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "In engine post process based pass." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SceneCapture_MetaData[] = {
		{ "Comment", "// Underlying scene capture.\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Underlying scene capture." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SceneCapture = { "SceneCapture", nullptr, (EPropertyFlags)0x0020080400082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePostProcessPass, SceneCapture), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SceneCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SceneCapture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_BlendableInterface_MetaData[] = {
		{ "Comment", "// Blendable interface to intercept the OverrideBlendableSettings.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Blendable interface to intercept the OverrideBlendableSettings." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_BlendableInterface = { "BlendableInterface", nullptr, (EPropertyFlags)0x0020080400002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePostProcessPass, BlendableInterface), Z_Construct_UClass_UComposurePostProcessBlendable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_BlendableInterface_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_BlendableInterface_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SetupMaterial_MetaData[] = {
		{ "Comment", "// Setup post process material.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Setup post process material." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SetupMaterial = { "SetupMaterial", nullptr, (EPropertyFlags)0x0020080400002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePostProcessPass, SetupMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SetupMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SetupMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_TonemapperReplacement_MetaData[] = {
		{ "Comment", "// Internal material that replace the tonemapper to output linear color space.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePostProcessPass.h" },
		{ "ToolTip", "Internal material that replace the tonemapper to output linear color space." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_TonemapperReplacement = { "TonemapperReplacement", nullptr, (EPropertyFlags)0x0020080400002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePostProcessPass, TonemapperReplacement), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_TonemapperReplacement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_TonemapperReplacement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposurePostProcessPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SceneCapture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_BlendableInterface,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_SetupMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePostProcessPass_Statics::NewProp_TonemapperReplacement,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposurePostProcessPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposurePostProcessPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposurePostProcessPass_Statics::ClassParams = {
		&UComposurePostProcessPass::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UComposurePostProcessPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessPass_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposurePostProcessPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposurePostProcessPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposurePostProcessPass, 1613049233);
	template<> COMPOSURE_API UClass* StaticClass<UComposurePostProcessPass>()
	{
		return UComposurePostProcessPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposurePostProcessPass(Z_Construct_UClass_UComposurePostProcessPass, &UComposurePostProcessPass::StaticClass, TEXT("/Script/Composure"), TEXT("UComposurePostProcessPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposurePostProcessPass);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
