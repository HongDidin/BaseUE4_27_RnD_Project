// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UCompositingInputInterfaceProxy;
class UTexture;
#ifdef COMPOSURE_CompositingElementInputs_generated_h
#error "CompositingElementInputs.generated.h already included, missing '#pragma once' in CompositingElementInputs.h"
#endif
#define COMPOSURE_CompositingElementInputs_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCompositingMediaInput(); \
	friend struct Z_Construct_UClass_UCompositingMediaInput_Statics; \
public: \
	DECLARE_CLASS(UCompositingMediaInput, UCompositingElementInput, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UCompositingMediaInput)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUCompositingMediaInput(); \
	friend struct Z_Construct_UClass_UCompositingMediaInput_Statics; \
public: \
	DECLARE_CLASS(UCompositingMediaInput, UCompositingElementInput, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UCompositingMediaInput)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCompositingMediaInput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompositingMediaInput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCompositingMediaInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingMediaInput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCompositingMediaInput(UCompositingMediaInput&&); \
	NO_API UCompositingMediaInput(const UCompositingMediaInput&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCompositingMediaInput(UCompositingMediaInput&&); \
	NO_API UCompositingMediaInput(const UCompositingMediaInput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCompositingMediaInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingMediaInput); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UCompositingMediaInput)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DefaultMaterial() { return STRUCT_OFFSET(UCompositingMediaInput, DefaultMaterial); } \
	FORCEINLINE static uint32 __PPO__DefaultTestPlateMaterial() { return STRUCT_OFFSET(UCompositingMediaInput, DefaultTestPlateMaterial); } \
	FORCEINLINE static uint32 __PPO__FallbackMID() { return STRUCT_OFFSET(UCompositingMediaInput, FallbackMID); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_15_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UCompositingMediaInput>();

#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMediaTextureCompositingInput(); \
	friend struct Z_Construct_UClass_UMediaTextureCompositingInput_Statics; \
public: \
	DECLARE_CLASS(UMediaTextureCompositingInput, UCompositingMediaInput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UMediaTextureCompositingInput)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_INCLASS \
private: \
	static void StaticRegisterNativesUMediaTextureCompositingInput(); \
	friend struct Z_Construct_UClass_UMediaTextureCompositingInput_Statics; \
public: \
	DECLARE_CLASS(UMediaTextureCompositingInput, UCompositingMediaInput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UMediaTextureCompositingInput)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMediaTextureCompositingInput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMediaTextureCompositingInput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaTextureCompositingInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaTextureCompositingInput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaTextureCompositingInput(UMediaTextureCompositingInput&&); \
	NO_API UMediaTextureCompositingInput(const UMediaTextureCompositingInput&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMediaTextureCompositingInput(UMediaTextureCompositingInput&&); \
	NO_API UMediaTextureCompositingInput(const UMediaTextureCompositingInput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMediaTextureCompositingInput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMediaTextureCompositingInput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMediaTextureCompositingInput)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_74_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_77_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UMediaTextureCompositingInput>();

#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_RPC_WRAPPERS \
	virtual void OnFrameEnd_Implementation(UCompositingInputInterfaceProxy* Proxy) {}; \
	virtual UTexture* GenerateInput_Implementation(UCompositingInputInterfaceProxy* Proxy) { return NULL; }; \
	virtual void OnFrameBegin_Implementation(UCompositingInputInterfaceProxy* Proxy, bool bCameraCutThisFrame) {}; \
 \
	DECLARE_FUNCTION(execOnFrameEnd); \
	DECLARE_FUNCTION(execGenerateInput); \
	DECLARE_FUNCTION(execOnFrameBegin);


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void OnFrameEnd_Implementation(UCompositingInputInterfaceProxy* Proxy) {}; \
	virtual UTexture* GenerateInput_Implementation(UCompositingInputInterfaceProxy* Proxy) { return NULL; }; \
	virtual void OnFrameBegin_Implementation(UCompositingInputInterfaceProxy* Proxy, bool bCameraCutThisFrame) {}; \
 \
	DECLARE_FUNCTION(execOnFrameEnd); \
	DECLARE_FUNCTION(execGenerateInput); \
	DECLARE_FUNCTION(execOnFrameBegin);


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_EVENT_PARMS \
	struct CompositingInputInterface_eventGenerateInput_Parms \
	{ \
		UCompositingInputInterfaceProxy* Proxy; \
		UTexture* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		CompositingInputInterface_eventGenerateInput_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	}; \
	struct CompositingInputInterface_eventOnFrameBegin_Parms \
	{ \
		UCompositingInputInterfaceProxy* Proxy; \
		bool bCameraCutThisFrame; \
	}; \
	struct CompositingInputInterface_eventOnFrameEnd_Parms \
	{ \
		UCompositingInputInterfaceProxy* Proxy; \
	};


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_CALLBACK_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompositingInputInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompositingInputInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompositingInputInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingInputInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompositingInputInterface(UCompositingInputInterface&&); \
	COMPOSURE_API UCompositingInputInterface(const UCompositingInputInterface&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompositingInputInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompositingInputInterface(UCompositingInputInterface&&); \
	COMPOSURE_API UCompositingInputInterface(const UCompositingInputInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompositingInputInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingInputInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompositingInputInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUCompositingInputInterface(); \
	friend struct Z_Construct_UClass_UCompositingInputInterface_Statics; \
public: \
	DECLARE_CLASS(UCompositingInputInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UCompositingInputInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ICompositingInputInterface() {} \
public: \
	typedef UCompositingInputInterface UClassType; \
	typedef ICompositingInputInterface ThisClass; \
	static UTexture* Execute_GenerateInput(UObject* O, UCompositingInputInterfaceProxy* Proxy); \
	static void Execute_OnFrameBegin(UObject* O, UCompositingInputInterfaceProxy* Proxy, bool bCameraCutThisFrame); \
	static void Execute_OnFrameEnd(UObject* O, UCompositingInputInterfaceProxy* Proxy); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_INCLASS_IINTERFACE \
protected: \
	virtual ~ICompositingInputInterface() {} \
public: \
	typedef UCompositingInputInterface UClassType; \
	typedef ICompositingInputInterface ThisClass; \
	static UTexture* Execute_GenerateInput(UObject* O, UCompositingInputInterfaceProxy* Proxy); \
	static void Execute_OnFrameBegin(UObject* O, UCompositingInputInterfaceProxy* Proxy, bool bCameraCutThisFrame); \
	static void Execute_OnFrameEnd(UObject* O, UCompositingInputInterfaceProxy* Proxy); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_93_PROLOG \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_EVENT_PARMS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_101_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_CALLBACK_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_101_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_CALLBACK_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_96_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UCompositingInputInterface>();

#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCompositingInputInterfaceProxy(); \
	friend struct Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics; \
public: \
	DECLARE_CLASS(UCompositingInputInterfaceProxy, UCompositingElementInput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UCompositingInputInterfaceProxy)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_INCLASS \
private: \
	static void StaticRegisterNativesUCompositingInputInterfaceProxy(); \
	friend struct Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics; \
public: \
	DECLARE_CLASS(UCompositingInputInterfaceProxy, UCompositingElementInput, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), NO_API) \
	DECLARE_SERIALIZER(UCompositingInputInterfaceProxy)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCompositingInputInterfaceProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompositingInputInterfaceProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCompositingInputInterfaceProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingInputInterfaceProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCompositingInputInterfaceProxy(UCompositingInputInterfaceProxy&&); \
	NO_API UCompositingInputInterfaceProxy(const UCompositingInputInterfaceProxy&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCompositingInputInterfaceProxy() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCompositingInputInterfaceProxy(UCompositingInputInterfaceProxy&&); \
	NO_API UCompositingInputInterfaceProxy(const UCompositingInputInterfaceProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCompositingInputInterfaceProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingInputInterfaceProxy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UCompositingInputInterfaceProxy)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_114_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h_117_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UCompositingInputInterfaceProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_CompositingElementInputs_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
