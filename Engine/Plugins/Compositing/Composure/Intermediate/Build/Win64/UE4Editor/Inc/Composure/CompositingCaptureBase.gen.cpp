// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/CompositingCaptureBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompositingCaptureBase() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_ACompositingCaptureBase_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_ACompositingCaptureBase();
	COMPOSURE_API UClass* Z_Construct_UClass_ACompositingElement();
	UPackage* Z_Construct_UPackage__Script_Composure();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	CAMERACALIBRATIONCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDistortionHandlerPicker();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ACompositingCaptureBase::execUpdateDistortion)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->UpdateDistortion();
		P_NATIVE_END;
	}
	void ACompositingCaptureBase::StaticRegisterNativesACompositingCaptureBase()
	{
		UClass* Class = ACompositingCaptureBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "UpdateDistortion", &ACompositingCaptureBase::execUpdateDistortion },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion_Statics::Function_MetaDataParams[] = {
		{ "Category", "Composure|LensDistortion" },
		{ "Comment", "/** Update the state of the Lens Distortion Data Handler, and updates or removes the Distortion MID from the SceneCaptureComponent's post process materials, depending on whether distortion should be applied*/" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Update the state of the Lens Distortion Data Handler, and updates or removes the Distortion MID from the SceneCaptureComponent's post process materials, depending on whether distortion should be applied" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ACompositingCaptureBase, nullptr, "UpdateDistortion", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ACompositingCaptureBase_NoRegister()
	{
		return ACompositingCaptureBase::StaticClass();
	}
	struct Z_Construct_UClass_ACompositingCaptureBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCaptureComponent2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCaptureComponent2D;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyDistortion_MetaData[];
#endif
		static void NewProp_bApplyDistortion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyDistortion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DistortionSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DistortionSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverscanFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OverscanFactor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalFocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OriginalFocalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastDistortionMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastDistortionMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACompositingCaptureBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACompositingElement,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ACompositingCaptureBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ACompositingCaptureBase_UpdateDistortion, "UpdateDistortion" }, // 1242140515
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompositingCaptureBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Base class for CG Compositing Elements\n */" },
		{ "IncludePath", "CompositingCaptureBase.h" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Base class for CG Compositing Elements" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_SceneCaptureComponent2D_MetaData[] = {
		{ "Category", "SceneCapture" },
		{ "Comment", "/** Component used to generate the scene capture for this CG Layer */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Component used to generate the scene capture for this CG Layer" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_SceneCaptureComponent2D = { "SceneCaptureComponent2D", nullptr, (EPropertyFlags)0x00100000000b001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACompositingCaptureBase, SceneCaptureComponent2D), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_SceneCaptureComponent2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_SceneCaptureComponent2D_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_bApplyDistortion_MetaData[] = {
		{ "Category", "Composure|LensDistortion" },
		{ "Comment", "/** Whether to apply distortion as a post-process effect on this CG Layer */" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Whether to apply distortion as a post-process effect on this CG Layer" },
	};
#endif
	void Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_bApplyDistortion_SetBit(void* Obj)
	{
		((ACompositingCaptureBase*)Obj)->bApplyDistortion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_bApplyDistortion = { "bApplyDistortion", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ACompositingCaptureBase), &Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_bApplyDistortion_SetBit, METADATA_PARAMS(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_bApplyDistortion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_bApplyDistortion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_DistortionSource_MetaData[] = {
		{ "Category", "Composure|LensDistortion" },
		{ "Comment", "/** Structure used to query the camera calibration subsystem for a lens distortion model handler */" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Structure used to query the camera calibration subsystem for a lens distortion model handler" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_DistortionSource = { "DistortionSource", nullptr, (EPropertyFlags)0x0020088000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACompositingCaptureBase, DistortionSource), Z_Construct_UScriptStruct_FDistortionHandlerPicker, METADATA_PARAMS(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_DistortionSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_DistortionSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OverscanFactor_MetaData[] = {
		{ "Category", "Composure|LensDistortion" },
		{ "Comment", "/** Value used to augment the FOV of the scene capture to produce a CG image with enough data to distort */" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Value used to augment the FOV of the scene capture to produce a CG image with enough data to distort" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OverscanFactor = { "OverscanFactor", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACompositingCaptureBase, OverscanFactor), METADATA_PARAMS(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OverscanFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OverscanFactor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OriginalFocalLength_MetaData[] = {
		{ "Category", "Composure|LensDistortion" },
		{ "Comment", "/** Focal length of the target camera before any overscan has been applied */" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Focal length of the target camera before any overscan has been applied" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OriginalFocalLength = { "OriginalFocalLength", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACompositingCaptureBase, OriginalFocalLength), METADATA_PARAMS(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OriginalFocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OriginalFocalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_LastDistortionMID_MetaData[] = {
		{ "Comment", "/** Cached distortion MID produced by the Lens Distortion Handler, used to clean up the post-process materials in the case that the the MID changes */" },
		{ "ModuleRelativePath", "Classes/CompositingCaptureBase.h" },
		{ "ToolTip", "Cached distortion MID produced by the Lens Distortion Handler, used to clean up the post-process materials in the case that the the MID changes" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_LastDistortionMID = { "LastDistortionMID", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ACompositingCaptureBase, LastDistortionMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_LastDistortionMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_LastDistortionMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ACompositingCaptureBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_SceneCaptureComponent2D,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_bApplyDistortion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_DistortionSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OverscanFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_OriginalFocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ACompositingCaptureBase_Statics::NewProp_LastDistortionMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACompositingCaptureBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACompositingCaptureBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACompositingCaptureBase_Statics::ClassParams = {
		&ACompositingCaptureBase::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ACompositingCaptureBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ACompositingCaptureBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACompositingCaptureBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACompositingCaptureBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACompositingCaptureBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACompositingCaptureBase, 2148261896);
	template<> COMPOSURE_API UClass* StaticClass<ACompositingCaptureBase>()
	{
		return ACompositingCaptureBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACompositingCaptureBase(Z_Construct_UClass_ACompositingCaptureBase, &ACompositingCaptureBase::StaticClass, TEXT("/Script/Composure"), TEXT("ACompositingCaptureBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACompositingCaptureBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
