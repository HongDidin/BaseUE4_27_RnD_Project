// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Private/ComposurePostProcessBlendable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposurePostProcessBlendable() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessBlendable_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessBlendable();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPass_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBlendableInterface_NoRegister();
// End Cross Module References
	void UComposurePostProcessBlendable::StaticRegisterNativesUComposurePostProcessBlendable()
	{
	}
	UClass* Z_Construct_UClass_UComposurePostProcessBlendable_NoRegister()
	{
		return UComposurePostProcessBlendable::StaticClass();
	}
	struct Z_Construct_UClass_UComposurePostProcessBlendable_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Target_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Target;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposurePostProcessBlendable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessBlendable_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Private blendable interface for  UComposurePostProcessPass.\n */" },
		{ "IncludePath", "ComposurePostProcessBlendable.h" },
		{ "ModuleRelativePath", "Private/ComposurePostProcessBlendable.h" },
		{ "NotBlueprintType", "true" },
		{ "ToolTip", "Private blendable interface for  UComposurePostProcessPass." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePostProcessBlendable_Statics::NewProp_Target_MetaData[] = {
		{ "Comment", "// Current player camera manager the target is bind on.\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/ComposurePostProcessBlendable.h" },
		{ "ToolTip", "Current player camera manager the target is bind on." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePostProcessBlendable_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0040000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePostProcessBlendable, Target), Z_Construct_UClass_UComposurePostProcessPass_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessBlendable_Statics::NewProp_Target_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessBlendable_Statics::NewProp_Target_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposurePostProcessBlendable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePostProcessBlendable_Statics::NewProp_Target,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UComposurePostProcessBlendable_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UBlendableInterface_NoRegister, (int32)VTABLE_OFFSET(UComposurePostProcessBlendable, IBlendableInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposurePostProcessBlendable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposurePostProcessBlendable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposurePostProcessBlendable_Statics::ClassParams = {
		&UComposurePostProcessBlendable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UComposurePostProcessBlendable_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessBlendable_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UComposurePostProcessBlendable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePostProcessBlendable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposurePostProcessBlendable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposurePostProcessBlendable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposurePostProcessBlendable, 3759151403);
	template<> COMPOSURE_API UClass* StaticClass<UComposurePostProcessBlendable>()
	{
		return UComposurePostProcessBlendable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposurePostProcessBlendable(Z_Construct_UClass_UComposurePostProcessBlendable, &UComposurePostProcessBlendable::StaticClass, TEXT("/Script/Composure"), TEXT("UComposurePostProcessBlendable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposurePostProcessBlendable);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
