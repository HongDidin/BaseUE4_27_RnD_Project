// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/ComposureLensBloomPass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposureLensBloomPass() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureLensBloomPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureLensBloomPass();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPass();
	UPackage* Z_Construct_UPackage__Script_Composure();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FLensBloomSettings();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureLensBloomPassPolicy_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureLensBloomPassPolicy();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPassPolicy();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UComposureLensBloomPass::execBloomToRenderTarget)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BloomToRenderTarget();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UComposureLensBloomPass::execSetTonemapperReplacingMaterial)
	{
		P_GET_OBJECT(UMaterialInstanceDynamic,Z_Param_Material);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTonemapperReplacingMaterial(Z_Param_Material);
		P_NATIVE_END;
	}
	void UComposureLensBloomPass::StaticRegisterNativesUComposureLensBloomPass()
	{
		UClass* Class = UComposureLensBloomPass::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BloomToRenderTarget", &UComposureLensBloomPass::execBloomToRenderTarget },
			{ "SetTonemapperReplacingMaterial", &UComposureLensBloomPass::execSetTonemapperReplacingMaterial },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Outputs" },
		{ "Comment", "/** \n\x09 * Blurs the input into the output.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
		{ "ToolTip", "Blurs the input into the output." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposureLensBloomPass, nullptr, "BloomToRenderTarget", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics
	{
		struct ComposureLensBloomPass_eventSetTonemapperReplacingMaterial_Parms
		{
			UMaterialInstanceDynamic* Material;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Material;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposureLensBloomPass_eventSetTonemapperReplacingMaterial_Parms, Material), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::NewProp_Material,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Bloom Settings" },
		{ "Comment", "/** Sets a custom tonemapper replacing material instance. */" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
		{ "ToolTip", "Sets a custom tonemapper replacing material instance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposureLensBloomPass, nullptr, "SetTonemapperReplacingMaterial", nullptr, nullptr, sizeof(ComposureLensBloomPass_eventSetTonemapperReplacingMaterial_Parms), Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UComposureLensBloomPass_NoRegister()
	{
		return UComposureLensBloomPass::StaticClass();
	}
	struct Z_Construct_UClass_UComposureLensBloomPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TonemapperReplacingMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TonemapperReplacingMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposureLensBloomPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UComposurePostProcessPass,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UComposureLensBloomPass_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UComposureLensBloomPass_BloomToRenderTarget, "BloomToRenderTarget" }, // 2168537628
		{ &Z_Construct_UFunction_UComposureLensBloomPass_SetTonemapperReplacingMaterial, "SetTonemapperReplacingMaterial" }, // 3873201345
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPass_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Composure" },
		{ "Comment", "/**\n * Bloom only pass implemented on top of the in-engine bloom.\n */" },
		{ "HideCategories", "Collision Object Physics SceneComponent Transform Trigger PhysicsVolume" },
		{ "IncludePath", "ComposureLensBloomPass.h" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
		{ "ToolTip", "Bloom only pass implemented on top of the in-engine bloom." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "Lens Bloom Settings" },
		{ "Comment", "/** Bloom settings. */" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
		{ "ToolTip", "Bloom settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureLensBloomPass, Settings), Z_Construct_UScriptStruct_FLensBloomSettings, METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_TonemapperReplacingMID_MetaData[] = {
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_TonemapperReplacingMID = { "TonemapperReplacingMID", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureLensBloomPass, TonemapperReplacingMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_TonemapperReplacingMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_TonemapperReplacingMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposureLensBloomPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureLensBloomPass_Statics::NewProp_TonemapperReplacingMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposureLensBloomPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposureLensBloomPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposureLensBloomPass_Statics::ClassParams = {
		&UComposureLensBloomPass::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UComposureLensBloomPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPass_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposureLensBloomPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposureLensBloomPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposureLensBloomPass, 457365301);
	template<> COMPOSURE_API UClass* StaticClass<UComposureLensBloomPass>()
	{
		return UComposureLensBloomPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposureLensBloomPass(Z_Construct_UClass_UComposureLensBloomPass, &UComposureLensBloomPass::StaticClass, TEXT("/Script/Composure"), TEXT("UComposureLensBloomPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposureLensBloomPass);
	void UComposureLensBloomPassPolicy::StaticRegisterNativesUComposureLensBloomPassPolicy()
	{
	}
	UClass* Z_Construct_UClass_UComposureLensBloomPassPolicy_NoRegister()
	{
		return UComposureLensBloomPassPolicy::StaticClass();
	}
	struct Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplacementMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReplacementMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BloomIntensityParamName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_BloomIntensityParamName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TonemapperReplacmentMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TonemapperReplacmentMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UComposurePostProcessPassPolicy,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Bloom only rules used for configuring how UComposurePostProcessingPassProxy executes\n */" },
		{ "DisplayName", "Lens Bloom Pass" },
		{ "IncludePath", "ComposureLensBloomPass.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
		{ "ToolTip", "Bloom only rules used for configuring how UComposurePostProcessingPassProxy executes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "Lens Bloom Settings" },
		{ "Comment", "/** Bloom settings. */" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
		{ "ToolTip", "Bloom settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureLensBloomPassPolicy, Settings), Z_Construct_UScriptStruct_FLensBloomSettings, METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_ReplacementMaterial_MetaData[] = {
		{ "Category", "Tonemapper Settings" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_ReplacementMaterial = { "ReplacementMaterial", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureLensBloomPassPolicy, ReplacementMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_ReplacementMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_ReplacementMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_BloomIntensityParamName_MetaData[] = {
		{ "Category", "Tonemapper Settings" },
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_BloomIntensityParamName = { "BloomIntensityParamName", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureLensBloomPassPolicy, BloomIntensityParamName), METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_BloomIntensityParamName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_BloomIntensityParamName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_TonemapperReplacmentMID_MetaData[] = {
		{ "ModuleRelativePath", "Classes/ComposureLensBloomPass.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_TonemapperReplacmentMID = { "TonemapperReplacmentMID", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureLensBloomPassPolicy, TonemapperReplacmentMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_TonemapperReplacmentMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_TonemapperReplacmentMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_ReplacementMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_BloomIntensityParamName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::NewProp_TonemapperReplacmentMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposureLensBloomPassPolicy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::ClassParams = {
		&UComposureLensBloomPassPolicy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposureLensBloomPassPolicy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposureLensBloomPassPolicy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposureLensBloomPassPolicy, 4265131277);
	template<> COMPOSURE_API UClass* StaticClass<UComposureLensBloomPassPolicy>()
	{
		return UComposureLensBloomPassPolicy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposureLensBloomPassPolicy(Z_Construct_UClass_UComposureLensBloomPassPolicy, &UComposureLensBloomPassPolicy::StaticClass, TEXT("/Script/Composure"), TEXT("UComposureLensBloomPassPolicy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposureLensBloomPassPolicy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
