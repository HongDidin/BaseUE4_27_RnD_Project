// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/MovieScene/MovieSceneComposureExportTrack.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneComposureExportTrack() {}
// Cross Module References
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneComposureExportPass();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportTrack_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportTrack();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneTrack();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection_NoRegister();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneTrackTemplateProducer_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportSection_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportSection();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection();
// End Cross Module References
class UScriptStruct* FMovieSceneComposureExportPass::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass, Z_Construct_UPackage__Script_Composure(), TEXT("MovieSceneComposureExportPass"), sizeof(FMovieSceneComposureExportPass), Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FMovieSceneComposureExportPass>()
{
	return FMovieSceneComposureExportPass::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneComposureExportPass(FMovieSceneComposureExportPass::StaticStruct, TEXT("/Script/Composure"), TEXT("MovieSceneComposureExportPass"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposureExportPass
{
	FScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposureExportPass()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneComposureExportPass>(FName(TEXT("MovieSceneComposureExportPass")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposureExportPass;
	struct Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransformPassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TransformPassName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRenamePass_MetaData[];
#endif
		static void NewProp_bRenamePass_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRenamePass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportedAs_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ExportedAs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Export configuration options for a single internal pass on an ACompositingElement, or its output pass (where TransformPassName is None)\n */" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ToolTip", "Export configuration options for a single internal pass on an ACompositingElement, or its output pass (where TransformPassName is None)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneComposureExportPass>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_TransformPassName_MetaData[] = {
		{ "Category", "Export" },
		{ "Comment", "/** The name of the transform pass in the comp to export. None signifies the element's output. */" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ToolTip", "The name of the transform pass in the comp to export. None signifies the element's output." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_TransformPassName = { "TransformPassName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneComposureExportPass, TransformPassName), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_TransformPassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_TransformPassName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_bRenamePass_MetaData[] = {
		{ "Category", "Export" },
		{ "Comment", "/** Whether to rename this pass when rendering out */" },
		{ "InlineEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ToolTip", "Whether to rename this pass when rendering out" },
	};
#endif
	void Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_bRenamePass_SetBit(void* Obj)
	{
		((FMovieSceneComposureExportPass*)Obj)->bRenamePass = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_bRenamePass = { "bRenamePass", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMovieSceneComposureExportPass), &Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_bRenamePass_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_bRenamePass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_bRenamePass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_ExportedAs_MetaData[] = {
		{ "Category", "Export" },
		{ "Comment", "/** The name to use for this pass when rendering out */" },
		{ "EditCondition", "bRenamePass" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ToolTip", "The name to use for this pass when rendering out" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_ExportedAs = { "ExportedAs", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneComposureExportPass, ExportedAs), METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_ExportedAs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_ExportedAs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_TransformPassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_bRenamePass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::NewProp_ExportedAs,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		nullptr,
		&NewStructOps,
		"MovieSceneComposureExportPass",
		sizeof(FMovieSceneComposureExportPass),
		alignof(FMovieSceneComposureExportPass),
		Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneComposureExportPass()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneComposureExportPass"), sizeof(FMovieSceneComposureExportPass), Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Hash() { return 1137125541U; }
	void UMovieSceneComposureExportTrack::StaticRegisterNativesUMovieSceneComposureExportTrack()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneComposureExportTrack_NoRegister()
	{
		return UMovieSceneComposureExportTrack::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sections;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneTrack,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Movie scene track that exports a single pass (either the element's output, or an internal transform pass) during burnouts\n */" },
		{ "IncludePath", "MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Movie scene track that exports a single pass (either the element's output, or an internal transform pass) during burnouts" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Pass_MetaData[] = {
		{ "Category", "Export" },
		{ "Comment", "/**\n\x09 * Configuration options for the pass to export\n\x09 */" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Configuration options for the pass to export" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Pass = { "Pass", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneComposureExportTrack, Pass), Z_Construct_UScriptStruct_FMovieSceneComposureExportPass, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Pass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Pass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Sections_Inner = { "Sections", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneSection_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Sections_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Sections = { "Sections", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneComposureExportTrack, Sections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Sections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Sections_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Pass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Sections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::NewProp_Sections,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UMovieSceneTrackTemplateProducer_NoRegister, (int32)VTABLE_OFFSET(UMovieSceneComposureExportTrack, IMovieSceneTrackTemplateProducer), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneComposureExportTrack>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::ClassParams = {
		&UMovieSceneComposureExportTrack::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x00A800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneComposureExportTrack()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneComposureExportTrack, 1995788940);
	template<> COMPOSURE_API UClass* StaticClass<UMovieSceneComposureExportTrack>()
	{
		return UMovieSceneComposureExportTrack::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneComposureExportTrack(Z_Construct_UClass_UMovieSceneComposureExportTrack, &UMovieSceneComposureExportTrack::StaticClass, TEXT("/Script/Composure"), TEXT("UMovieSceneComposureExportTrack"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneComposureExportTrack);
	void UMovieSceneComposureExportSection::StaticRegisterNativesUMovieSceneComposureExportSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneComposureExportSection_NoRegister()
	{
		return UMovieSceneComposureExportSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneComposureExportSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneComposureExportSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneSection,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposureExportSection_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ModuleRelativePath", "Public/MovieScene/MovieSceneComposureExportTrack.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneComposureExportSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneComposureExportSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneComposureExportSection_Statics::ClassParams = {
		&UMovieSceneComposureExportSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x002800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposureExportSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposureExportSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneComposureExportSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneComposureExportSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneComposureExportSection, 1837763408);
	template<> COMPOSURE_API UClass* StaticClass<UMovieSceneComposureExportSection>()
	{
		return UMovieSceneComposureExportSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneComposureExportSection(Z_Construct_UClass_UMovieSceneComposureExportSection, &UMovieSceneComposureExportSection::StaticClass, TEXT("/Script/Composure"), TEXT("UMovieSceneComposureExportSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneComposureExportSection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
