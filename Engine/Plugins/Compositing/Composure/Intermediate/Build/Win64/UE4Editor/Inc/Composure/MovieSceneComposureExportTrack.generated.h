// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_MovieSceneComposureExportTrack_generated_h
#error "MovieSceneComposureExportTrack.generated.h already included, missing '#pragma once' in MovieSceneComposureExportTrack.h"
#endif
#define COMPOSURE_MovieSceneComposureExportTrack_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneComposureExportPass_Statics; \
	COMPOSURE_API static class UScriptStruct* StaticStruct();


template<> COMPOSURE_API UScriptStruct* StaticStruct<struct FMovieSceneComposureExportPass>();

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneComposureExportTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposureExportTrack, UMovieSceneTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposureExportTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneComposureExportTrack*>(this); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneComposureExportTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneComposureExportTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposureExportTrack, UMovieSceneTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposureExportTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneComposureExportTrack*>(this); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UMovieSceneComposureExportTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposureExportTrack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposureExportTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposureExportTrack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposureExportTrack(UMovieSceneComposureExportTrack&&); \
	COMPOSURE_API UMovieSceneComposureExportTrack(const UMovieSceneComposureExportTrack&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposureExportTrack(UMovieSceneComposureExportTrack&&); \
	COMPOSURE_API UMovieSceneComposureExportTrack(const UMovieSceneComposureExportTrack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposureExportTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposureExportTrack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposureExportTrack)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Sections() { return STRUCT_OFFSET(UMovieSceneComposureExportTrack, Sections); }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_36_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_40_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UMovieSceneComposureExportTrack>();

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneComposureExportSection(); \
	friend struct Z_Construct_UClass_UMovieSceneComposureExportSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposureExportSection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposureExportSection)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneComposureExportSection(); \
	friend struct Z_Construct_UClass_UMovieSceneComposureExportSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneComposureExportSection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UMovieSceneComposureExportSection)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UMovieSceneComposureExportSection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposureExportSection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposureExportSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposureExportSection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposureExportSection(UMovieSceneComposureExportSection&&); \
	COMPOSURE_API UMovieSceneComposureExportSection(const UMovieSceneComposureExportSection&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UMovieSceneComposureExportSection(UMovieSceneComposureExportSection&&); \
	COMPOSURE_API UMovieSceneComposureExportSection(const UMovieSceneComposureExportSection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UMovieSceneComposureExportSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneComposureExportSection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneComposureExportSection)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_74_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_INCLASS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h_78_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UMovieSceneComposureExportSection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Public_MovieScene_MovieSceneComposureExportTrack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
