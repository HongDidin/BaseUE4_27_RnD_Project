// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/CompositingElements/CompositingElementTransforms.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompositingElementTransforms() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingPostProcessPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingPostProcessPass();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingElementTransform();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePostProcessPassPolicy_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingElementMaterialPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingElementMaterialPass();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FCompositingMaterial();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingTonemapPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingTonemapPass();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FColorGradingSettings();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilmStockSettings();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureTonemapperPassPolicy_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMultiPassChromaKeyer_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMultiPassChromaKeyer();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMultiPassDespill_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMultiPassDespill();
	COMPOSURE_API UClass* Z_Construct_UClass_UAlphaTransformPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UAlphaTransformPass();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingOpenColorIOPass_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingOpenColorIOPass();
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings();
// End Cross Module References
	void UCompositingPostProcessPass::StaticRegisterNativesUCompositingPostProcessPass()
	{
	}
	UClass* Z_Construct_UClass_UCompositingPostProcessPass_NoRegister()
	{
		return UCompositingPostProcessPass::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingPostProcessPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_RenderScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostProcessPasses_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PostProcessPasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostProcessPasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PostProcessPasses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingPostProcessPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPostProcessPass_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CompositingElements/CompositingElementTransforms.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_RenderScale_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_RenderScale = { "RenderScale", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingPostProcessPass, RenderScale), METADATA_PARAMS(Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_RenderScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_RenderScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_Inner_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "RenderScale" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_Inner = { "PostProcessPasses", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UComposurePostProcessPassPolicy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "RenderScale" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses = { "PostProcessPasses", nullptr, (EPropertyFlags)0x001000800000001d, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingPostProcessPass, PostProcessPasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingPostProcessPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_RenderScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingPostProcessPass_Statics::NewProp_PostProcessPasses,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingPostProcessPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingPostProcessPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingPostProcessPass_Statics::ClassParams = {
		&UCompositingPostProcessPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCompositingPostProcessPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPostProcessPass_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingPostProcessPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingPostProcessPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingPostProcessPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingPostProcessPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingPostProcessPass, 3776928004);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingPostProcessPass>()
	{
		return UCompositingPostProcessPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingPostProcessPass(Z_Construct_UClass_UCompositingPostProcessPass, &UCompositingPostProcessPass::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingPostProcessPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingPostProcessPass);
	DEFINE_FUNCTION(UCompositingElementMaterialPass::execSetParameterMapping)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_TextureParamName);
		P_GET_PROPERTY(FNameProperty,Z_Param_ComposureLayerName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetParameterMapping(Z_Param_TextureParamName,Z_Param_ComposureLayerName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UCompositingElementMaterialPass::execSetMaterialInterface)
	{
		P_GET_OBJECT(UMaterialInterface,Z_Param_NewMaterial);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetMaterialInterface(Z_Param_NewMaterial);
		P_NATIVE_END;
	}
	static FName NAME_UCompositingElementMaterialPass_ApplyMaterialParams = FName(TEXT("ApplyMaterialParams"));
	void UCompositingElementMaterialPass::ApplyMaterialParams(UMaterialInstanceDynamic* MID)
	{
		CompositingElementMaterialPass_eventApplyMaterialParams_Parms Parms;
		Parms.MID=MID;
		ProcessEvent(FindFunctionChecked(NAME_UCompositingElementMaterialPass_ApplyMaterialParams),&Parms);
	}
	void UCompositingElementMaterialPass::StaticRegisterNativesUCompositingElementMaterialPass()
	{
		UClass* Class = UCompositingElementMaterialPass::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetMaterialInterface", &UCompositingElementMaterialPass::execSetMaterialInterface },
			{ "SetParameterMapping", &UCompositingElementMaterialPass::execSetParameterMapping },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::NewProp_MID = { "MID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingElementMaterialPass_eventApplyMaterialParams_Parms, MID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::NewProp_MID,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingElementMaterialPass, nullptr, "ApplyMaterialParams", nullptr, nullptr, sizeof(CompositingElementMaterialPass_eventApplyMaterialParams_Parms), Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08080800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics
	{
		struct CompositingElementMaterialPass_eventSetMaterialInterface_Parms
		{
			UMaterialInterface* NewMaterial;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::NewProp_NewMaterial = { "NewMaterial", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingElementMaterialPass_eventSetMaterialInterface_Parms, NewMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::NewProp_NewMaterial,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::Function_MetaDataParams[] = {
		{ "Category", "Compositing Pass" },
		{ "Comment", "/**\n\x09 * Set the material interface used by current material pass. \n\x09 * @param NewMaterial            The new material interface users want to set.\n\x09 */" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ToolTip", "Set the material interface used by current material pass.\n@param NewMaterial            The new material interface users want to set." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingElementMaterialPass, nullptr, "SetMaterialInterface", nullptr, nullptr, sizeof(CompositingElementMaterialPass_eventSetMaterialInterface_Parms), Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics
	{
		struct CompositingElementMaterialPass_eventSetParameterMapping_Parms
		{
			FName TextureParamName;
			FName ComposureLayerName;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TextureParamName;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ComposureLayerName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_TextureParamName = { "TextureParamName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingElementMaterialPass_eventSetParameterMapping_Parms, TextureParamName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_ComposureLayerName = { "ComposureLayerName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingElementMaterialPass_eventSetParameterMapping_Parms, ComposureLayerName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CompositingElementMaterialPass_eventSetParameterMapping_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CompositingElementMaterialPass_eventSetParameterMapping_Parms), &Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_TextureParamName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_ComposureLayerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::Function_MetaDataParams[] = {
		{ "Category", "Compositing Pass" },
		{ "Comment", "/**\n\x09 * Set the parameter mappings between texture parameters and composure layers. Users can not create new entries into the map as the keys are read only.\n\x09 * Invalid Texture parameter names will result in a failed setting operation. \n\x09 * @param TextureParamName       The name of the texture parameter inside the material interface. Used as key.\n\x09 * @param ComposureLayerName     The name of the composure layer the texture parameter is mapped to. Used as value.\n\x09 * @return bool                  True if set operation is successful. \n\x09 */" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ToolTip", "Set the parameter mappings between texture parameters and composure layers. Users can not create new entries into the map as the keys are read only.\nInvalid Texture parameter names will result in a failed setting operation.\n@param TextureParamName       The name of the texture parameter inside the material interface. Used as key.\n@param ComposureLayerName     The name of the composure layer the texture parameter is mapped to. Used as value.\n@return bool                  True if set operation is successful." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingElementMaterialPass, nullptr, "SetParameterMapping", nullptr, nullptr, sizeof(CompositingElementMaterialPass_eventSetParameterMapping_Parms), Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCompositingElementMaterialPass_NoRegister()
	{
		return UCompositingElementMaterialPass::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingElementMaterialPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Material;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingElementMaterialPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingPostProcessPass,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCompositingElementMaterialPass_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCompositingElementMaterialPass_ApplyMaterialParams, "ApplyMaterialParams" }, // 3248709436
		{ &Z_Construct_UFunction_UCompositingElementMaterialPass_SetMaterialInterface, "SetMaterialInterface" }, // 4111057535
		{ &Z_Construct_UFunction_UCompositingElementMaterialPass_SetParameterMapping, "SetParameterMapping" }, // 3860194649
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingElementMaterialPass_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* UCompositingElementMaterialPass\n *****************************************************************************/" },
		{ "IncludePath", "CompositingElements/CompositingElementTransforms.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ToolTip", "UCompositingElementMaterialPass" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingElementMaterialPass_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCompositingElementMaterialPass_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingElementMaterialPass, Material), Z_Construct_UScriptStruct_FCompositingMaterial, METADATA_PARAMS(Z_Construct_UClass_UCompositingElementMaterialPass_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingElementMaterialPass_Statics::NewProp_Material_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingElementMaterialPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingElementMaterialPass_Statics::NewProp_Material,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingElementMaterialPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingElementMaterialPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingElementMaterialPass_Statics::ClassParams = {
		&UCompositingElementMaterialPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UCompositingElementMaterialPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingElementMaterialPass_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingElementMaterialPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingElementMaterialPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingElementMaterialPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingElementMaterialPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingElementMaterialPass, 836095592);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingElementMaterialPass>()
	{
		return UCompositingElementMaterialPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingElementMaterialPass(Z_Construct_UClass_UCompositingElementMaterialPass, &UCompositingElementMaterialPass::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingElementMaterialPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingElementMaterialPass);
	void UCompositingTonemapPass::StaticRegisterNativesUCompositingTonemapPass()
	{
	}
	UClass* Z_Construct_UClass_UCompositingTonemapPass_NoRegister()
	{
		return UCompositingTonemapPass::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingTonemapPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorGradingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorGradingSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilmStockSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilmStockSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChromaticAberration_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChromaticAberration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TonemapPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TonemapPolicy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingTonemapPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingTonemapPass_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CompositingElements/CompositingElementTransforms.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ColorGradingSettings_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "Comment", "/** Color grading settings. */" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Color grading settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ColorGradingSettings = { "ColorGradingSettings", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingTonemapPass, ColorGradingSettings), Z_Construct_UScriptStruct_FColorGradingSettings, METADATA_PARAMS(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ColorGradingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ColorGradingSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_FilmStockSettings_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "Comment", "/** Film stock settings. */" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Film stock settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_FilmStockSettings = { "FilmStockSettings", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingTonemapPass, FilmStockSettings), Z_Construct_UScriptStruct_FFilmStockSettings, METADATA_PARAMS(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_FilmStockSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_FilmStockSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ChromaticAberration_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "Comment", "/** in percent, Scene chromatic aberration / color fringe (camera imperfection) to simulate an artifact that happens in real-world lens, mostly visible in the image corners. */" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ToolTip", "in percent, Scene chromatic aberration / color fringe (camera imperfection) to simulate an artifact that happens in real-world lens, mostly visible in the image corners." },
		{ "UIMax", "5.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ChromaticAberration = { "ChromaticAberration", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingTonemapPass, ChromaticAberration), METADATA_PARAMS(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ChromaticAberration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ChromaticAberration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_TonemapPolicy_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_TonemapPolicy = { "TonemapPolicy", nullptr, (EPropertyFlags)0x00c0000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingTonemapPass, TonemapPolicy), Z_Construct_UClass_UComposureTonemapperPassPolicy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_TonemapPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_TonemapPolicy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingTonemapPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ColorGradingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_FilmStockSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_ChromaticAberration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingTonemapPass_Statics::NewProp_TonemapPolicy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingTonemapPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingTonemapPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingTonemapPass_Statics::ClassParams = {
		&UCompositingTonemapPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCompositingTonemapPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingTonemapPass_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingTonemapPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingTonemapPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingTonemapPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingTonemapPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingTonemapPass, 4172846492);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingTonemapPass>()
	{
		return UCompositingTonemapPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingTonemapPass(Z_Construct_UClass_UCompositingTonemapPass, &UCompositingTonemapPass::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingTonemapPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingTonemapPass);
	void UMultiPassChromaKeyer::StaticRegisterNativesUMultiPassChromaKeyer()
	{
	}
	UClass* Z_Construct_UClass_UMultiPassChromaKeyer_NoRegister()
	{
		return UMultiPassChromaKeyer::StaticClass();
	}
	struct Z_Construct_UClass_UMultiPassChromaKeyer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyColors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyColors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_KeyColors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyerMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyerMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultWhiteTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultWhiteTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMultiPassChromaKeyer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassChromaKeyer_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CompositingElements/CompositingElementTransforms.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyColors_Inner = { "KeyColors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyColors_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyColors = { "KeyColors", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiPassChromaKeyer, KeyColors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyColors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyerMaterial_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyerMaterial = { "KeyerMaterial", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiPassChromaKeyer, KeyerMaterial), Z_Construct_UScriptStruct_FCompositingMaterial, METADATA_PARAMS(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyerMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyerMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_DefaultWhiteTexture_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_DefaultWhiteTexture = { "DefaultWhiteTexture", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiPassChromaKeyer, DefaultWhiteTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_DefaultWhiteTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_DefaultWhiteTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMultiPassChromaKeyer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyColors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_KeyerMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassChromaKeyer_Statics::NewProp_DefaultWhiteTexture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMultiPassChromaKeyer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMultiPassChromaKeyer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMultiPassChromaKeyer_Statics::ClassParams = {
		&UMultiPassChromaKeyer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMultiPassChromaKeyer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::PropPointers),
		0,
		0x000010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassChromaKeyer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMultiPassChromaKeyer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMultiPassChromaKeyer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMultiPassChromaKeyer, 3568678731);
	template<> COMPOSURE_API UClass* StaticClass<UMultiPassChromaKeyer>()
	{
		return UMultiPassChromaKeyer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMultiPassChromaKeyer(Z_Construct_UClass_UMultiPassChromaKeyer, &UMultiPassChromaKeyer::StaticClass, TEXT("/Script/Composure"), TEXT("UMultiPassChromaKeyer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMultiPassChromaKeyer);
	void UMultiPassDespill::StaticRegisterNativesUMultiPassDespill()
	{
	}
	UClass* Z_Construct_UClass_UMultiPassDespill_NoRegister()
	{
		return UMultiPassDespill::StaticClass();
	}
	struct Z_Construct_UClass_UMultiPassDespill_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyColors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyColors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_KeyColors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeyerMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_KeyerMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultWhiteTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultWhiteTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMultiPassDespill_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassDespill_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CompositingElements/CompositingElementTransforms.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyColors_Inner = { "KeyColors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyColors_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyColors = { "KeyColors", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiPassDespill, KeyColors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyColors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyerMaterial_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyerMaterial = { "KeyerMaterial", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiPassDespill, KeyerMaterial), Z_Construct_UScriptStruct_FCompositingMaterial, METADATA_PARAMS(Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyerMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyerMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_DefaultWhiteTexture_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_DefaultWhiteTexture = { "DefaultWhiteTexture", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMultiPassDespill, DefaultWhiteTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_DefaultWhiteTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_DefaultWhiteTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMultiPassDespill_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyColors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_KeyerMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMultiPassDespill_Statics::NewProp_DefaultWhiteTexture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMultiPassDespill_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMultiPassDespill>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMultiPassDespill_Statics::ClassParams = {
		&UMultiPassDespill::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMultiPassDespill_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassDespill_Statics::PropPointers),
		0,
		0x000010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMultiPassDespill_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMultiPassDespill_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMultiPassDespill()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMultiPassDespill_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMultiPassDespill, 3799709016);
	template<> COMPOSURE_API UClass* StaticClass<UMultiPassDespill>()
	{
		return UMultiPassDespill::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMultiPassDespill(Z_Construct_UClass_UMultiPassDespill, &UMultiPassDespill::StaticClass, TEXT("/Script/Composure"), TEXT("UMultiPassDespill"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMultiPassDespill);
	void UAlphaTransformPass::StaticRegisterNativesUAlphaTransformPass()
	{
	}
	UClass* Z_Construct_UClass_UAlphaTransformPass_NoRegister()
	{
		return UAlphaTransformPass::StaticClass();
	}
	struct Z_Construct_UClass_UAlphaTransformPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlphaScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AlphaScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlphaTransformMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AlphaTransformMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAlphaTransformPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlphaTransformPass_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CompositingElements/CompositingElementTransforms.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaScale_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaScale = { "AlphaScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlphaTransformPass, AlphaScale), METADATA_PARAMS(Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_DefaultMaterial = { "DefaultMaterial", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlphaTransformPass, DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaTransformMID_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaTransformMID = { "AlphaTransformMID", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAlphaTransformPass, AlphaTransformMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaTransformMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaTransformMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAlphaTransformPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAlphaTransformPass_Statics::NewProp_AlphaTransformMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAlphaTransformPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAlphaTransformPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAlphaTransformPass_Statics::ClassParams = {
		&UAlphaTransformPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAlphaTransformPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAlphaTransformPass_Statics::PropPointers),
		0,
		0x040010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UAlphaTransformPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAlphaTransformPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAlphaTransformPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAlphaTransformPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAlphaTransformPass, 2768718812);
	template<> COMPOSURE_API UClass* StaticClass<UAlphaTransformPass>()
	{
		return UAlphaTransformPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAlphaTransformPass(Z_Construct_UClass_UAlphaTransformPass, &UAlphaTransformPass::StaticClass, TEXT("/Script/Composure"), TEXT("UAlphaTransformPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAlphaTransformPass);
	void UCompositingOpenColorIOPass::StaticRegisterNativesUCompositingOpenColorIOPass()
	{
	}
	UClass* Z_Construct_UClass_UCompositingOpenColorIOPass_NoRegister()
	{
		return UCompositingOpenColorIOPass::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingOpenColorIOPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorConversionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorConversionSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/* UCompositingOpenColorIOPass\n*****************************************************************************/" },
		{ "DisplayName", "OpenColorIO Pass" },
		{ "IncludePath", "CompositingElements/CompositingElementTransforms.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ToolTip", "UCompositingOpenColorIOPass" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::NewProp_ColorConversionSettings_MetaData[] = {
		{ "Category", "OpenColorIO Settings" },
		{ "Comment", "/** Color grading settings. */" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementTransforms.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Color grading settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::NewProp_ColorConversionSettings = { "ColorConversionSettings", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingOpenColorIOPass, ColorConversionSettings), Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings, METADATA_PARAMS(Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::NewProp_ColorConversionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::NewProp_ColorConversionSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::NewProp_ColorConversionSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingOpenColorIOPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::ClassParams = {
		&UCompositingOpenColorIOPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingOpenColorIOPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingOpenColorIOPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingOpenColorIOPass, 1576053623);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingOpenColorIOPass>()
	{
		return UCompositingOpenColorIOPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingOpenColorIOPass(Z_Construct_UClass_UCompositingOpenColorIOPass, &UCompositingOpenColorIOPass::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingOpenColorIOPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingOpenColorIOPass);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
