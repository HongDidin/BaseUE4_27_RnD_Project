// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/CompositingElements/ICompositingTextureLookupTable.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeICompositingTextureLookupTable() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingTextureLookupTable_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingTextureLookupTable();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Composure();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ICompositingTextureLookupTable::execFindNamedPassResult)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_LookupName);
		P_GET_OBJECT_REF(UTexture,Z_Param_Out_OutTexture);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->FindNamedPassResult(Z_Param_LookupName,Z_Param_Out_OutTexture);
		P_NATIVE_END;
	}
	void UCompositingTextureLookupTable::StaticRegisterNativesUCompositingTextureLookupTable()
	{
		UClass* Class = UCompositingTextureLookupTable::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FindNamedPassResult", &ICompositingTextureLookupTable::execFindNamedPassResult },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics
	{
		struct CompositingTextureLookupTable_eventFindNamedPassResult_Parms
		{
			FName LookupName;
			UTexture* OutTexture;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LookupName;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutTexture;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_LookupName = { "LookupName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingTextureLookupTable_eventFindNamedPassResult_Parms, LookupName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_OutTexture = { "OutTexture", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingTextureLookupTable_eventFindNamedPassResult_Parms, OutTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((CompositingTextureLookupTable_eventFindNamedPassResult_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CompositingTextureLookupTable_eventFindNamedPassResult_Parms), &Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_LookupName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_OutTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::Function_MetaDataParams[] = {
		{ "Category", "Composure" },
		{ "ModuleRelativePath", "Classes/CompositingElements/ICompositingTextureLookupTable.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingTextureLookupTable, nullptr, "FindNamedPassResult", nullptr, nullptr, sizeof(CompositingTextureLookupTable_eventFindNamedPassResult_Parms), Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCompositingTextureLookupTable_NoRegister()
	{
		return UCompositingTextureLookupTable::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingTextureLookupTable_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingTextureLookupTable_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCompositingTextureLookupTable_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCompositingTextureLookupTable_FindNamedPassResult, "FindNamedPassResult" }, // 1256923927
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingTextureLookupTable_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Classes/CompositingElements/ICompositingTextureLookupTable.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingTextureLookupTable_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ICompositingTextureLookupTable>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingTextureLookupTable_Statics::ClassParams = {
		&UCompositingTextureLookupTable::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingTextureLookupTable_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingTextureLookupTable_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingTextureLookupTable()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingTextureLookupTable_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingTextureLookupTable, 2366922032);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingTextureLookupTable>()
	{
		return UCompositingTextureLookupTable::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingTextureLookupTable(Z_Construct_UClass_UCompositingTextureLookupTable, &UCompositingTextureLookupTable::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingTextureLookupTable"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingTextureLookupTable);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
