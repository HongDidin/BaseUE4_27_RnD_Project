// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/CompositingElements/CompositingElementInputs.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompositingElementInputs() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingMediaInput_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingMediaInput();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingElementInput();
	UPackage* Z_Construct_UPackage__Script_Composure();
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FCompositingMaterial();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMediaTextureCompositingInput_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMediaTextureCompositingInput();
	MEDIAASSETS_API UClass* Z_Construct_UClass_UMediaTexture_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingInputInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingInputInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingInputInterfaceProxy_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompositingInputInterfaceProxy();
// End Cross Module References
	void UCompositingMediaInput::StaticRegisterNativesUCompositingMediaInput()
	{
	}
	UClass* Z_Construct_UClass_UCompositingMediaInput_NoRegister()
	{
		return UCompositingMediaInput::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingMediaInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaTransformMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MediaTransformMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTestPlateMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultTestPlateMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FallbackMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FallbackMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingMediaInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementInput,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaInput_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CompositingElements/CompositingElementInputs.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_MediaTransformMaterial_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "MediaSource" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_MediaTransformMaterial = { "MediaTransformMaterial", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingMediaInput, MediaTransformMaterial), Z_Construct_UScriptStruct_FCompositingMaterial, METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_MediaTransformMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_MediaTransformMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultMaterial = { "DefaultMaterial", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingMediaInput, DefaultMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultTestPlateMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultTestPlateMaterial = { "DefaultTestPlateMaterial", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingMediaInput, DefaultTestPlateMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultTestPlateMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultTestPlateMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_FallbackMID_MetaData[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_FallbackMID = { "FallbackMID", nullptr, (EPropertyFlags)0x00c0000000202000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingMediaInput, FallbackMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_FallbackMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_FallbackMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingMediaInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_MediaTransformMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_DefaultTestPlateMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingMediaInput_Statics::NewProp_FallbackMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingMediaInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingMediaInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingMediaInput_Statics::ClassParams = {
		&UCompositingMediaInput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCompositingMediaInput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaInput_Statics::PropPointers),
		0,
		0x041010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingMediaInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingMediaInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingMediaInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingMediaInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingMediaInput, 1547156540);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingMediaInput>()
	{
		return UCompositingMediaInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingMediaInput(Z_Construct_UClass_UCompositingMediaInput, &UCompositingMediaInput::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingMediaInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingMediaInput);
	void UMediaTextureCompositingInput::StaticRegisterNativesUMediaTextureCompositingInput()
	{
	}
	UClass* Z_Construct_UClass_UMediaTextureCompositingInput_NoRegister()
	{
		return UMediaTextureCompositingInput::StaticClass();
	}
	struct Z_Construct_UClass_UMediaTextureCompositingInput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaSource;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMediaTextureCompositingInput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingMediaInput,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaTextureCompositingInput_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "CompositingElements/CompositingElementInputs.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMediaTextureCompositingInput_Statics::NewProp_MediaSource_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "DisplayAfter", "PassName" },
		{ "EditCondition", "bEnabled" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMediaTextureCompositingInput_Statics::NewProp_MediaSource = { "MediaSource", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMediaTextureCompositingInput, MediaSource), Z_Construct_UClass_UMediaTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMediaTextureCompositingInput_Statics::NewProp_MediaSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaTextureCompositingInput_Statics::NewProp_MediaSource_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMediaTextureCompositingInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMediaTextureCompositingInput_Statics::NewProp_MediaSource,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMediaTextureCompositingInput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMediaTextureCompositingInput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMediaTextureCompositingInput_Statics::ClassParams = {
		&UMediaTextureCompositingInput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMediaTextureCompositingInput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMediaTextureCompositingInput_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMediaTextureCompositingInput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMediaTextureCompositingInput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMediaTextureCompositingInput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMediaTextureCompositingInput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMediaTextureCompositingInput, 3225915002);
	template<> COMPOSURE_API UClass* StaticClass<UMediaTextureCompositingInput>()
	{
		return UMediaTextureCompositingInput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMediaTextureCompositingInput(Z_Construct_UClass_UMediaTextureCompositingInput, &UMediaTextureCompositingInput::StaticClass, TEXT("/Script/Composure"), TEXT("UMediaTextureCompositingInput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMediaTextureCompositingInput);
	DEFINE_FUNCTION(ICompositingInputInterface::execOnFrameEnd)
	{
		P_GET_OBJECT(UCompositingInputInterfaceProxy,Z_Param_Proxy);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnFrameEnd_Implementation(Z_Param_Proxy);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ICompositingInputInterface::execGenerateInput)
	{
		P_GET_OBJECT(UCompositingInputInterfaceProxy,Z_Param_Proxy);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTexture**)Z_Param__Result=P_THIS->GenerateInput_Implementation(Z_Param_Proxy);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ICompositingInputInterface::execOnFrameBegin)
	{
		P_GET_OBJECT(UCompositingInputInterfaceProxy,Z_Param_Proxy);
		P_GET_UBOOL(Z_Param_bCameraCutThisFrame);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnFrameBegin_Implementation(Z_Param_Proxy,Z_Param_bCameraCutThisFrame);
		P_NATIVE_END;
	}
	UTexture* ICompositingInputInterface::GenerateInput(UCompositingInputInterfaceProxy* Proxy)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_GenerateInput instead.");
		CompositingInputInterface_eventGenerateInput_Parms Parms;
		return Parms.ReturnValue;
	}
	void ICompositingInputInterface::OnFrameBegin(UCompositingInputInterfaceProxy* Proxy, bool bCameraCutThisFrame)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnFrameBegin instead.");
	}
	void ICompositingInputInterface::OnFrameEnd(UCompositingInputInterfaceProxy* Proxy)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnFrameEnd instead.");
	}
	void UCompositingInputInterface::StaticRegisterNativesUCompositingInputInterface()
	{
		UClass* Class = UCompositingInputInterface::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GenerateInput", &ICompositingInputInterface::execGenerateInput },
			{ "OnFrameBegin", &ICompositingInputInterface::execOnFrameBegin },
			{ "OnFrameEnd", &ICompositingInputInterface::execOnFrameEnd },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Proxy;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::NewProp_Proxy = { "Proxy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingInputInterface_eventGenerateInput_Parms, Proxy), Z_Construct_UClass_UCompositingInputInterfaceProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingInputInterface_eventGenerateInput_Parms, ReturnValue), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::NewProp_Proxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Composure|Input" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingInputInterface, nullptr, "GenerateInput", nullptr, nullptr, sizeof(CompositingInputInterface_eventGenerateInput_Parms), Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingInputInterface_GenerateInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingInputInterface_GenerateInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Proxy;
		static void NewProp_bCameraCutThisFrame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCameraCutThisFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::NewProp_Proxy = { "Proxy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingInputInterface_eventOnFrameBegin_Parms, Proxy), Z_Construct_UClass_UCompositingInputInterfaceProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::NewProp_bCameraCutThisFrame_SetBit(void* Obj)
	{
		((CompositingInputInterface_eventOnFrameBegin_Parms*)Obj)->bCameraCutThisFrame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::NewProp_bCameraCutThisFrame = { "bCameraCutThisFrame", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(CompositingInputInterface_eventOnFrameBegin_Parms), &Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::NewProp_bCameraCutThisFrame_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::NewProp_Proxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::NewProp_bCameraCutThisFrame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Composure" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingInputInterface, nullptr, "OnFrameBegin", nullptr, nullptr, sizeof(CompositingInputInterface_eventOnFrameBegin_Parms), Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Proxy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::NewProp_Proxy = { "Proxy", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(CompositingInputInterface_eventOnFrameEnd_Parms, Proxy), Z_Construct_UClass_UCompositingInputInterfaceProxy_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::NewProp_Proxy,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Composure" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UCompositingInputInterface, nullptr, "OnFrameEnd", nullptr, nullptr, sizeof(CompositingInputInterface_eventOnFrameEnd_Parms), Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UCompositingInputInterface_NoRegister()
	{
		return UCompositingInputInterface::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingInputInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingInputInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UCompositingInputInterface_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UCompositingInputInterface_GenerateInput, "GenerateInput" }, // 874937190
		{ &Z_Construct_UFunction_UCompositingInputInterface_OnFrameBegin, "OnFrameBegin" }, // 735376414
		{ &Z_Construct_UFunction_UCompositingInputInterface_OnFrameEnd, "OnFrameEnd" }, // 1872991165
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingInputInterface_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingInputInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ICompositingInputInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingInputInterface_Statics::ClassParams = {
		&UCompositingInputInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingInputInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingInputInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingInputInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingInputInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingInputInterface, 3302118216);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingInputInterface>()
	{
		return UCompositingInputInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingInputInterface(Z_Construct_UClass_UCompositingInputInterface, &UCompositingInputInterface::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingInputInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingInputInterface);
	static FName NAME_UCompositingInputInterface_GenerateInput = FName(TEXT("GenerateInput"));
	UTexture* ICompositingInputInterface::Execute_GenerateInput(UObject* O, UCompositingInputInterfaceProxy* Proxy)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UCompositingInputInterface::StaticClass()));
		CompositingInputInterface_eventGenerateInput_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UCompositingInputInterface_GenerateInput);
		if (Func)
		{
			Parms.Proxy=Proxy;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (ICompositingInputInterface*)(O->GetNativeInterfaceAddress(UCompositingInputInterface::StaticClass())))
		{
			Parms.ReturnValue = I->GenerateInput_Implementation(Proxy);
		}
		return Parms.ReturnValue;
	}
	static FName NAME_UCompositingInputInterface_OnFrameBegin = FName(TEXT("OnFrameBegin"));
	void ICompositingInputInterface::Execute_OnFrameBegin(UObject* O, UCompositingInputInterfaceProxy* Proxy, bool bCameraCutThisFrame)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UCompositingInputInterface::StaticClass()));
		CompositingInputInterface_eventOnFrameBegin_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UCompositingInputInterface_OnFrameBegin);
		if (Func)
		{
			Parms.Proxy=Proxy;
			Parms.bCameraCutThisFrame=bCameraCutThisFrame;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (ICompositingInputInterface*)(O->GetNativeInterfaceAddress(UCompositingInputInterface::StaticClass())))
		{
			I->OnFrameBegin_Implementation(Proxy,bCameraCutThisFrame);
		}
	}
	static FName NAME_UCompositingInputInterface_OnFrameEnd = FName(TEXT("OnFrameEnd"));
	void ICompositingInputInterface::Execute_OnFrameEnd(UObject* O, UCompositingInputInterfaceProxy* Proxy)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UCompositingInputInterface::StaticClass()));
		CompositingInputInterface_eventOnFrameEnd_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UCompositingInputInterface_OnFrameEnd);
		if (Func)
		{
			Parms.Proxy=Proxy;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (ICompositingInputInterface*)(O->GetNativeInterfaceAddress(UCompositingInputInterface::StaticClass())))
		{
			I->OnFrameEnd_Implementation(Proxy);
		}
	}
	void UCompositingInputInterfaceProxy::StaticRegisterNativesUCompositingInputInterfaceProxy()
	{
	}
	UClass* Z_Construct_UClass_UCompositingInputInterfaceProxy_NoRegister()
	{
		return UCompositingInputInterfaceProxy::StaticClass();
	}
	struct Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompositingInput_MetaData[];
#endif
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_CompositingInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompositingElementInput,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CompositingElements/CompositingElementInputs.h" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::NewProp_CompositingInput_MetaData[] = {
		{ "Category", "Compositing Pass" },
		{ "ModuleRelativePath", "Classes/CompositingElements/CompositingElementInputs.h" },
	};
#endif
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::NewProp_CompositingInput = { "CompositingInput", nullptr, (EPropertyFlags)0x0014000000000004, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCompositingInputInterfaceProxy, CompositingInput), Z_Construct_UClass_UCompositingInputInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::NewProp_CompositingInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::NewProp_CompositingInput_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::NewProp_CompositingInput,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCompositingInputInterfaceProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::ClassParams = {
		&UCompositingInputInterfaceProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::PropPointers),
		0,
		0x041010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompositingInputInterfaceProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompositingInputInterfaceProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompositingInputInterfaceProxy, 1581020444);
	template<> COMPOSURE_API UClass* StaticClass<UCompositingInputInterfaceProxy>()
	{
		return UCompositingInputInterfaceProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompositingInputInterfaceProxy(Z_Construct_UClass_UCompositingInputInterfaceProxy, &UCompositingInputInterfaceProxy::StaticClass, TEXT("/Script/Composure"), TEXT("UCompositingInputInterfaceProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompositingInputInterfaceProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
