// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/ComposurePlayerCompositingInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposurePlayerCompositingInterface() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Composure();
// End Cross Module References
	void UComposurePlayerCompositingInterface::StaticRegisterNativesUComposurePlayerCompositingInterface()
	{
	}
	UClass* Z_Construct_UClass_UComposurePlayerCompositingInterface_NoRegister()
	{
		return UComposurePlayerCompositingInterface::StaticClass();
	}
	struct Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Public/ComposurePlayerCompositingInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IComposurePlayerCompositingInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics::ClassParams = {
		&UComposurePlayerCompositingInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposurePlayerCompositingInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposurePlayerCompositingInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposurePlayerCompositingInterface, 2361211871);
	template<> COMPOSURE_API UClass* StaticClass<UComposurePlayerCompositingInterface>()
	{
		return UComposurePlayerCompositingInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposurePlayerCompositingInterface(Z_Construct_UClass_UComposurePlayerCompositingInterface, &UComposurePlayerCompositingInterface::StaticClass, TEXT("/Script/Composure"), TEXT("UComposurePlayerCompositingInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposurePlayerCompositingInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
