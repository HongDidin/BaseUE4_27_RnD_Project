// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Private/MovieScene/MovieSceneComposureExportSectionTemplate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneComposureExportSectionTemplate() {}
// Cross Module References
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate();
	UPackage* Z_Construct_UPackage__Script_Composure();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneEvalTemplate();
	COMPOSURE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneComposureExportPass();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportInitializer_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UMovieSceneComposureExportInitializer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COMPOSURE_API UClass* Z_Construct_UClass_ACompositingElement_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneComposureExportSectionTemplate>() == std::is_polymorphic<FMovieSceneEvalTemplate>(), "USTRUCT FMovieSceneComposureExportSectionTemplate cannot be polymorphic unless super FMovieSceneEvalTemplate is polymorphic");

class UScriptStruct* FMovieSceneComposureExportSectionTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMPOSURE_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate, Z_Construct_UPackage__Script_Composure(), TEXT("MovieSceneComposureExportSectionTemplate"), sizeof(FMovieSceneComposureExportSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Hash());
	}
	return Singleton;
}
template<> COMPOSURE_API UScriptStruct* StaticStruct<FMovieSceneComposureExportSectionTemplate>()
{
	return FMovieSceneComposureExportSectionTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneComposureExportSectionTemplate(FMovieSceneComposureExportSectionTemplate::StaticStruct, TEXT("/Script/Composure"), TEXT("MovieSceneComposureExportSectionTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposureExportSectionTemplate
{
	FScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposureExportSectionTemplate()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneComposureExportSectionTemplate>(FName(TEXT("MovieSceneComposureExportSectionTemplate")));
	}
} ScriptStruct_Composure_StaticRegisterNativesFMovieSceneComposureExportSectionTemplate;
	struct Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposureExportSectionTemplate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneComposureExportSectionTemplate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::NewProp_Pass_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposureExportSectionTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::NewProp_Pass = { "Pass", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneComposureExportSectionTemplate, Pass), Z_Construct_UScriptStruct_FMovieSceneComposureExportPass, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::NewProp_Pass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::NewProp_Pass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::NewProp_Pass,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
		Z_Construct_UScriptStruct_FMovieSceneEvalTemplate,
		&NewStructOps,
		"MovieSceneComposureExportSectionTemplate",
		sizeof(FMovieSceneComposureExportSectionTemplate),
		alignof(FMovieSceneComposureExportSectionTemplate),
		Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Composure();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneComposureExportSectionTemplate"), sizeof(FMovieSceneComposureExportSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneComposureExportSectionTemplate_Hash() { return 2449600789U; }
	DEFINE_FUNCTION(UMovieSceneComposureExportInitializer::execExportSceneCaptureBuffers)
	{
		P_GET_OBJECT(ACompositingElement,Z_Param_CompShotElement);
		P_GET_OBJECT(USceneCaptureComponent2D,Z_Param_SceneCapture);
		P_GET_TARRAY_REF(FString,Z_Param_Out_BuffersToExport);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ExportSceneCaptureBuffers(Z_Param_CompShotElement,Z_Param_SceneCapture,Z_Param_Out_BuffersToExport);
		P_NATIVE_END;
	}
	void UMovieSceneComposureExportInitializer::StaticRegisterNativesUMovieSceneComposureExportInitializer()
	{
		UClass* Class = UMovieSceneComposureExportInitializer::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ExportSceneCaptureBuffers", &UMovieSceneComposureExportInitializer::execExportSceneCaptureBuffers },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics
	{
		struct MovieSceneComposureExportInitializer_eventExportSceneCaptureBuffers_Parms
		{
			ACompositingElement* CompShotElement;
			USceneCaptureComponent2D* SceneCapture;
			TArray<FString> BuffersToExport;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CompShotElement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCapture;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_BuffersToExport_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuffersToExport_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BuffersToExport;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_CompShotElement = { "CompShotElement", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneComposureExportInitializer_eventExportSceneCaptureBuffers_Parms, CompShotElement), Z_Construct_UClass_ACompositingElement_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_SceneCapture_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_SceneCapture = { "SceneCapture", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneComposureExportInitializer_eventExportSceneCaptureBuffers_Parms, SceneCapture), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_SceneCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_SceneCapture_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_BuffersToExport_Inner = { "BuffersToExport", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_BuffersToExport_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_BuffersToExport = { "BuffersToExport", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneComposureExportInitializer_eventExportSceneCaptureBuffers_Parms, BuffersToExport), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_BuffersToExport_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_BuffersToExport_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_CompShotElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_SceneCapture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_BuffersToExport_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::NewProp_BuffersToExport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::Function_MetaDataParams[] = {
		{ "Category", "Compsure|Export" },
		{ "Comment", "/**\n\x09 * Initialize the export to capture the specified named buffer visualization targets from a scene capture\n\x09 */" },
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposureExportSectionTemplate.h" },
		{ "ToolTip", "Initialize the export to capture the specified named buffer visualization targets from a scene capture" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneComposureExportInitializer, nullptr, "ExportSceneCaptureBuffers", nullptr, nullptr, sizeof(MovieSceneComposureExportInitializer_eventExportSceneCaptureBuffers_Parms), Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMovieSceneComposureExportInitializer_NoRegister()
	{
		return UMovieSceneComposureExportInitializer::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMovieSceneComposureExportInitializer_ExportSceneCaptureBuffers, "ExportSceneCaptureBuffers" }, // 3025373197
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Object passed to comp shot elements to initialize them for export.\n * Currenly only allows scene captures to initialize a new extension that can capture GBuffers and other buffer visualization targets\n */" },
		{ "IncludePath", "MovieScene/MovieSceneComposureExportSectionTemplate.h" },
		{ "ModuleRelativePath", "Private/MovieScene/MovieSceneComposureExportSectionTemplate.h" },
		{ "ToolTip", "Object passed to comp shot elements to initialize them for export.\nCurrenly only allows scene captures to initialize a new extension that can capture GBuffers and other buffer visualization targets" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneComposureExportInitializer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::ClassParams = {
		&UMovieSceneComposureExportInitializer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneComposureExportInitializer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneComposureExportInitializer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneComposureExportInitializer, 2469563079);
	template<> COMPOSURE_API UClass* StaticClass<UMovieSceneComposureExportInitializer>()
	{
		return UMovieSceneComposureExportInitializer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneComposureExportInitializer(Z_Construct_UClass_UMovieSceneComposureExportInitializer, &UMovieSceneComposureExportInitializer::StaticClass, TEXT("/Script/Composure"), TEXT("UMovieSceneComposureExportInitializer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneComposureExportInitializer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
