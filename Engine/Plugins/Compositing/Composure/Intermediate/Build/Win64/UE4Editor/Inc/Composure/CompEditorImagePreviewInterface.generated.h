// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_CompEditorImagePreviewInterface_generated_h
#error "CompEditorImagePreviewInterface.generated.h already included, missing '#pragma once' in CompEditorImagePreviewInterface.h"
#endif
#define COMPOSURE_CompEditorImagePreviewInterface_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompEditorImagePreviewInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompEditorImagePreviewInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompEditorImagePreviewInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompEditorImagePreviewInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompEditorImagePreviewInterface(UCompEditorImagePreviewInterface&&); \
	COMPOSURE_API UCompEditorImagePreviewInterface(const UCompEditorImagePreviewInterface&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompEditorImagePreviewInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompEditorImagePreviewInterface(UCompEditorImagePreviewInterface&&); \
	COMPOSURE_API UCompEditorImagePreviewInterface(const UCompEditorImagePreviewInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompEditorImagePreviewInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompEditorImagePreviewInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompEditorImagePreviewInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUCompEditorImagePreviewInterface(); \
	friend struct Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics; \
public: \
	DECLARE_CLASS(UCompEditorImagePreviewInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UCompEditorImagePreviewInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ICompEditorImagePreviewInterface() {} \
public: \
	typedef UCompEditorImagePreviewInterface UClassType; \
	typedef ICompEditorImagePreviewInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_INCLASS_IINTERFACE \
protected: \
	virtual ~ICompEditorImagePreviewInterface() {} \
public: \
	typedef UCompEditorImagePreviewInterface UClassType; \
	typedef ICompEditorImagePreviewInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_12_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UCompEditorImagePreviewInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompEditorImagePreviewInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
