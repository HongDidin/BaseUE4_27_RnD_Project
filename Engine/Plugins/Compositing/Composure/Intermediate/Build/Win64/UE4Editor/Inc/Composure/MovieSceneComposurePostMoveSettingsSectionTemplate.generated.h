// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_MovieSceneComposurePostMoveSettingsSectionTemplate_generated_h
#error "MovieSceneComposurePostMoveSettingsSectionTemplate.generated.h already included, missing '#pragma once' in MovieSceneComposurePostMoveSettingsSectionTemplate.h"
#endif
#define COMPOSURE_MovieSceneComposurePostMoveSettingsSectionTemplate_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposurePostMoveSettingsSectionTemplate_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneComposurePostMoveSettingsSectionTemplate_Statics; \
	COMPOSURE_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Pivot() { return STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, Pivot); } \
	FORCEINLINE static uint32 __PPO__Translation() { return STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, Translation); } \
	FORCEINLINE static uint32 __PPO__RotationAngle() { return STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, RotationAngle); } \
	FORCEINLINE static uint32 __PPO__Scale() { return STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, Scale); } \
	FORCEINLINE static uint32 __PPO__BlendType() { return STRUCT_OFFSET(FMovieSceneComposurePostMoveSettingsSectionTemplate, BlendType); } \
	typedef FMovieScenePropertySectionTemplate Super;


template<> COMPOSURE_API UScriptStruct* StaticStruct<struct FMovieSceneComposurePostMoveSettingsSectionTemplate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Private_MovieScene_MovieSceneComposurePostMoveSettingsSectionTemplate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
