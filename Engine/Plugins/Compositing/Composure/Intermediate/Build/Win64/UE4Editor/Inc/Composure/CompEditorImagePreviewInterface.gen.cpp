// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/EditorSupport/CompEditorImagePreviewInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompEditorImagePreviewInterface() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UCompEditorImagePreviewInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompEditorImagePreviewInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Composure();
// End Cross Module References
	void UCompEditorImagePreviewInterface::StaticRegisterNativesUCompEditorImagePreviewInterface()
	{
	}
	UClass* Z_Construct_UClass_UCompEditorImagePreviewInterface_NoRegister()
	{
		return UCompEditorImagePreviewInterface::StaticClass();
	}
	struct Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Public/EditorSupport/CompEditorImagePreviewInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ICompEditorImagePreviewInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics::ClassParams = {
		&UCompEditorImagePreviewInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompEditorImagePreviewInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompEditorImagePreviewInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompEditorImagePreviewInterface, 636199298);
	template<> COMPOSURE_API UClass* StaticClass<UCompEditorImagePreviewInterface>()
	{
		return UCompEditorImagePreviewInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompEditorImagePreviewInterface(Z_Construct_UClass_UCompEditorImagePreviewInterface, &UCompEditorImagePreviewInterface::StaticClass, TEXT("/Script/Composure"), TEXT("UCompEditorImagePreviewInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompEditorImagePreviewInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
