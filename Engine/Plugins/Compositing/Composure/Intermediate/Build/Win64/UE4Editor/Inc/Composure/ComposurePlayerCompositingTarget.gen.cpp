// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Classes/ComposurePlayerCompositingTarget.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposurePlayerCompositingTarget() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingTarget_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingTarget();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Composure();
	ENGINE_API UClass* Z_Construct_UClass_APlayerCameraManager_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposurePlayerCompositingInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureCompositingTargetComponent_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UComposureCompositingTargetComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompEditorImagePreviewInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UComposurePlayerCompositingTarget::execSetRenderTarget)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_RenderTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRenderTarget(Z_Param_RenderTarget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UComposurePlayerCompositingTarget::execSetPlayerCameraManager)
	{
		P_GET_OBJECT(APlayerCameraManager,Z_Param_PlayerCameraManager);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(APlayerCameraManager**)Z_Param__Result=P_THIS->SetPlayerCameraManager(Z_Param_PlayerCameraManager);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UComposurePlayerCompositingTarget::execGetPlayerCameraManager)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(APlayerCameraManager**)Z_Param__Result=P_THIS->GetPlayerCameraManager();
		P_NATIVE_END;
	}
	void UComposurePlayerCompositingTarget::StaticRegisterNativesUComposurePlayerCompositingTarget()
	{
		UClass* Class = UComposurePlayerCompositingTarget::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetPlayerCameraManager", &UComposurePlayerCompositingTarget::execGetPlayerCameraManager },
			{ "SetPlayerCameraManager", &UComposurePlayerCompositingTarget::execSetPlayerCameraManager },
			{ "SetRenderTarget", &UComposurePlayerCompositingTarget::execSetRenderTarget },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics
	{
		struct ComposurePlayerCompositingTarget_eventGetPlayerCameraManager_Parms
		{
			APlayerCameraManager* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePlayerCompositingTarget_eventGetPlayerCameraManager_Parms, ReturnValue), Z_Construct_UClass_APlayerCameraManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Compositing target" },
		{ "Comment", "// Current player camera manager the target is bind on.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Current player camera manager the target is bind on." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePlayerCompositingTarget, nullptr, "GetPlayerCameraManager", nullptr, nullptr, sizeof(ComposurePlayerCompositingTarget_eventGetPlayerCameraManager_Parms), Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics
	{
		struct ComposurePlayerCompositingTarget_eventSetPlayerCameraManager_Parms
		{
			APlayerCameraManager* PlayerCameraManager;
			APlayerCameraManager* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerCameraManager;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::NewProp_PlayerCameraManager = { "PlayerCameraManager", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePlayerCompositingTarget_eventSetPlayerCameraManager_Parms, PlayerCameraManager), Z_Construct_UClass_APlayerCameraManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePlayerCompositingTarget_eventSetPlayerCameraManager_Parms, ReturnValue), Z_Construct_UClass_APlayerCameraManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::NewProp_PlayerCameraManager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Compositing target" },
		{ "Comment", "// Set player camera manager to bind the render target to.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Set player camera manager to bind the render target to." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePlayerCompositingTarget, nullptr, "SetPlayerCameraManager", nullptr, nullptr, sizeof(ComposurePlayerCompositingTarget_eventSetPlayerCameraManager_Parms), Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics
	{
		struct ComposurePlayerCompositingTarget_eventSetRenderTarget_Parms
		{
			UTextureRenderTarget2D* RenderTarget;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::NewProp_RenderTarget = { "RenderTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposurePlayerCompositingTarget_eventSetRenderTarget_Parms, RenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::NewProp_RenderTarget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Player Compositing target" },
		{ "Comment", "// Set the render target of the player.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Set the render target of the player." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposurePlayerCompositingTarget, nullptr, "SetRenderTarget", nullptr, nullptr, sizeof(ComposurePlayerCompositingTarget_eventSetRenderTarget_Parms), Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UComposurePlayerCompositingTarget_NoRegister()
	{
		return UComposurePlayerCompositingTarget::StaticClass();
	}
	struct Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerCameraManager_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerCameraManager;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerCameraModifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlayerCameraModifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplaceTonemapperMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReplaceTonemapperMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UComposurePlayerCompositingTarget_GetPlayerCameraManager, "GetPlayerCameraManager" }, // 2807169193
		{ &Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetPlayerCameraManager, "SetPlayerCameraManager" }, // 1959545051
		{ &Z_Construct_UFunction_UComposurePlayerCompositingTarget_SetRenderTarget, "SetRenderTarget" }, // 1287492023
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Object to bind to a APlayerCameraManager with a UTextureRenderTarget2D to be used as a player's render target.\n * @TODO-BADGER: deprecate this (UComposurePlayerCompositingTarget) once we're comfortable using the new UComposureCompositingTargetComponent in its place\n */" },
		{ "IncludePath", "ComposurePlayerCompositingTarget.h" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Object to bind to a APlayerCameraManager with a UTextureRenderTarget2D to be used as a player's render target.\n@TODO-BADGER: deprecate this (UComposurePlayerCompositingTarget) once we're comfortable using the new UComposureCompositingTargetComponent in its place" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraManager_MetaData[] = {
		{ "Comment", "// Current player camera manager the target is bind on.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Current player camera manager the target is bind on." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraManager = { "PlayerCameraManager", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePlayerCompositingTarget, PlayerCameraManager), Z_Construct_UClass_APlayerCameraManager_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraManager_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraManager_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraModifier_MetaData[] = {
		{ "Comment", "// Underlying player camera modifier\n" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Underlying player camera modifier" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraModifier = { "PlayerCameraModifier", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePlayerCompositingTarget, PlayerCameraModifier), Z_Construct_UClass_UComposurePlayerCompositingCameraModifier_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraModifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraModifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_ReplaceTonemapperMID_MetaData[] = {
		{ "Comment", "// Post process material that replaces the tonemapper to dump the player's render target.\n" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Post process material that replaces the tonemapper to dump the player's render target." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_ReplaceTonemapperMID = { "ReplaceTonemapperMID", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposurePlayerCompositingTarget, ReplaceTonemapperMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_ReplaceTonemapperMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_ReplaceTonemapperMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraManager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_PlayerCameraModifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::NewProp_ReplaceTonemapperMID,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UComposurePlayerCompositingInterface_NoRegister, (int32)VTABLE_OFFSET(UComposurePlayerCompositingTarget, IComposurePlayerCompositingInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposurePlayerCompositingTarget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::ClassParams = {
		&UComposurePlayerCompositingTarget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposurePlayerCompositingTarget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposurePlayerCompositingTarget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposurePlayerCompositingTarget, 3526863543);
	template<> COMPOSURE_API UClass* StaticClass<UComposurePlayerCompositingTarget>()
	{
		return UComposurePlayerCompositingTarget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposurePlayerCompositingTarget(Z_Construct_UClass_UComposurePlayerCompositingTarget, &UComposurePlayerCompositingTarget::StaticClass, TEXT("/Script/Composure"), TEXT("UComposurePlayerCompositingTarget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposurePlayerCompositingTarget);
	DEFINE_FUNCTION(UComposureCompositingTargetComponent::execGetDisplayTexture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTexture**)Z_Param__Result=P_THIS->GetDisplayTexture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UComposureCompositingTargetComponent::execSetDisplayTexture)
	{
		P_GET_OBJECT(UTexture,Z_Param_DisplayTexture);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDisplayTexture(Z_Param_DisplayTexture);
		P_NATIVE_END;
	}
	void UComposureCompositingTargetComponent::StaticRegisterNativesUComposureCompositingTargetComponent()
	{
		UClass* Class = UComposureCompositingTargetComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDisplayTexture", &UComposureCompositingTargetComponent::execGetDisplayTexture },
			{ "SetDisplayTexture", &UComposureCompositingTargetComponent::execSetDisplayTexture },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics
	{
		struct ComposureCompositingTargetComponent_eventGetDisplayTexture_Parms
		{
			UTexture* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposureCompositingTargetComponent_eventGetDisplayTexture_Parms, ReturnValue), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Composure|CompositingTarget" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposureCompositingTargetComponent, nullptr, "GetDisplayTexture", nullptr, nullptr, sizeof(ComposureCompositingTargetComponent_eventGetDisplayTexture_Parms), Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics
	{
		struct ComposureCompositingTargetComponent_eventSetDisplayTexture_Parms
		{
			UTexture* DisplayTexture;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplayTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::NewProp_DisplayTexture = { "DisplayTexture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ComposureCompositingTargetComponent_eventSetDisplayTexture_Parms, DisplayTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::NewProp_DisplayTexture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Composure|CompositingTarget" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UComposureCompositingTargetComponent, nullptr, "SetDisplayTexture", nullptr, nullptr, sizeof(ComposureCompositingTargetComponent_eventSetDisplayTexture_Parms), Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UComposureCompositingTargetComponent_NoRegister()
	{
		return UComposureCompositingTargetComponent::StaticClass();
	}
	struct Z_Construct_UClass_UComposureCompositingTargetComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DisplayTexture;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CompilerErrImage_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CompilerErrImage;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UComposureCompositingTargetComponent_GetDisplayTexture, "GetDisplayTexture" }, // 1582814555
		{ &Z_Construct_UFunction_UComposureCompositingTargetComponent_SetDisplayTexture, "SetDisplayTexture" }, // 973572458
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "Rendering" },
		{ "Comment", "/**\n * Component intended to replace UComposurePlayerCompositingTarget - a object to bind to a APlayerCameraManager \n * with a UTextureRenderTarget2D to be used as a player's render target.\n * Made into a component so we can hook into preview rendering in editor.\n */" },
		{ "IncludePath", "ComposurePlayerCompositingTarget.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
		{ "ToolTip", "Component intended to replace UComposurePlayerCompositingTarget - a object to bind to a APlayerCameraManager\nwith a UTextureRenderTarget2D to be used as a player's render target.\nMade into a component so we can hook into preview rendering in editor." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_DisplayTexture_MetaData[] = {
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_DisplayTexture = { "DisplayTexture", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureCompositingTargetComponent, DisplayTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_DisplayTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_DisplayTexture_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_CompilerErrImage_MetaData[] = {
		{ "ModuleRelativePath", "Classes/ComposurePlayerCompositingTarget.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_CompilerErrImage = { "CompilerErrImage", nullptr, (EPropertyFlags)0x0040000800002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureCompositingTargetComponent, CompilerErrImage), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_CompilerErrImage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_CompilerErrImage_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_DisplayTexture,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::NewProp_CompilerErrImage,
#endif // WITH_EDITORONLY_DATA
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UCompEditorImagePreviewInterface_NoRegister, (int32)VTABLE_OFFSET(UComposureCompositingTargetComponent, ICompEditorImagePreviewInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposureCompositingTargetComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::ClassParams = {
		&UComposureCompositingTargetComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposureCompositingTargetComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposureCompositingTargetComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposureCompositingTargetComponent, 1223599714);
	template<> COMPOSURE_API UClass* StaticClass<UComposureCompositingTargetComponent>()
	{
		return UComposureCompositingTargetComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposureCompositingTargetComponent(Z_Construct_UClass_UComposureCompositingTargetComponent, &UComposureCompositingTargetComponent::StaticClass, TEXT("/Script/Composure"), TEXT("UComposureCompositingTargetComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposureCompositingTargetComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
