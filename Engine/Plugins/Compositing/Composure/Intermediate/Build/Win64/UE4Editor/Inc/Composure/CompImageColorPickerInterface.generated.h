// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMPOSURE_CompImageColorPickerInterface_generated_h
#error "CompImageColorPickerInterface.generated.h already included, missing '#pragma once' in CompImageColorPickerInterface.h"
#endif
#define COMPOSURE_CompImageColorPickerInterface_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_RPC_WRAPPERS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompImageColorPickerInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompImageColorPickerInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompImageColorPickerInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompImageColorPickerInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompImageColorPickerInterface(UCompImageColorPickerInterface&&); \
	COMPOSURE_API UCompImageColorPickerInterface(const UCompImageColorPickerInterface&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompImageColorPickerInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompImageColorPickerInterface(UCompImageColorPickerInterface&&); \
	COMPOSURE_API UCompImageColorPickerInterface(const UCompImageColorPickerInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompImageColorPickerInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompImageColorPickerInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompImageColorPickerInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUCompImageColorPickerInterface(); \
	friend struct Z_Construct_UClass_UCompImageColorPickerInterface_Statics; \
public: \
	DECLARE_CLASS(UCompImageColorPickerInterface, UCompEditorImagePreviewInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UCompImageColorPickerInterface)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ICompImageColorPickerInterface() {} \
public: \
	typedef UCompImageColorPickerInterface UClassType; \
	typedef ICompImageColorPickerInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_INCLASS_IINTERFACE \
protected: \
	virtual ~ICompImageColorPickerInterface() {} \
public: \
	typedef UCompImageColorPickerInterface UClassType; \
	typedef ICompImageColorPickerInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_14_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h_17_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UCompImageColorPickerInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Public_EditorSupport_CompImageColorPickerInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
