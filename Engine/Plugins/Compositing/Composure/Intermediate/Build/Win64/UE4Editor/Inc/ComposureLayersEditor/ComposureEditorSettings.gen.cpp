// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ComposureLayersEditor/Private/ComposureEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeComposureEditorSettings() {}
// Cross Module References
	COMPOSURELAYERSEDITOR_API UClass* Z_Construct_UClass_UDefaultComposureEditorSettings_NoRegister();
	COMPOSURELAYERSEDITOR_API UClass* Z_Construct_UClass_UDefaultComposureEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ComposureLayersEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	COMPOSURELAYERSEDITOR_API UClass* Z_Construct_UClass_UComposureEditorSettings_NoRegister();
	COMPOSURELAYERSEDITOR_API UClass* Z_Construct_UClass_UComposureEditorSettings();
// End Cross Module References
	void UDefaultComposureEditorSettings::StaticRegisterNativesUDefaultComposureEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UDefaultComposureEditorSettings_NoRegister()
	{
		return UDefaultComposureEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UDefaultComposureEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeaturedCompShotClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeaturedCompShotClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeaturedCompShotClasses;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeaturedElementClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeaturedElementClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeaturedElementClasses;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultElementNames_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DefaultElementNames_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultElementNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_DefaultElementNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ComposureLayersEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ComposureEditorSettings.h" },
		{ "ModuleRelativePath", "Private/ComposureEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedCompShotClasses_Inner = { "FeaturedCompShotClasses", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedCompShotClasses_MetaData[] = {
		{ "ModuleRelativePath", "Private/ComposureEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedCompShotClasses = { "FeaturedCompShotClasses", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDefaultComposureEditorSettings, FeaturedCompShotClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedCompShotClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedCompShotClasses_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedElementClasses_Inner = { "FeaturedElementClasses", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedElementClasses_MetaData[] = {
		{ "ModuleRelativePath", "Private/ComposureEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedElementClasses = { "FeaturedElementClasses", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDefaultComposureEditorSettings, FeaturedElementClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedElementClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedElementClasses_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames_ValueProp = { "DefaultElementNames", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames_Key_KeyProp = { "DefaultElementNames_Key", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames_MetaData[] = {
		{ "ModuleRelativePath", "Private/ComposureEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames = { "DefaultElementNames", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDefaultComposureEditorSettings, DefaultElementNames), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedCompShotClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedCompShotClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedElementClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_FeaturedElementClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::NewProp_DefaultElementNames,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDefaultComposureEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::ClassParams = {
		&UDefaultComposureEditorSettings::StaticClass,
		"Composure",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDefaultComposureEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDefaultComposureEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDefaultComposureEditorSettings, 1732758788);
	template<> COMPOSURELAYERSEDITOR_API UClass* StaticClass<UDefaultComposureEditorSettings>()
	{
		return UDefaultComposureEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDefaultComposureEditorSettings(Z_Construct_UClass_UDefaultComposureEditorSettings, &UDefaultComposureEditorSettings::StaticClass, TEXT("/Script/ComposureLayersEditor"), TEXT("UDefaultComposureEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDefaultComposureEditorSettings);
	void UComposureEditorSettings::StaticRegisterNativesUComposureEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UComposureEditorSettings_NoRegister()
	{
		return UComposureEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UComposureEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeaturedCompShotClassOverrides_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeaturedCompShotClassOverrides_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeaturedCompShotClassOverrides;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FeaturedElementClassOverrides_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FeaturedElementClassOverrides_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FeaturedElementClassOverrides;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UComposureEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ComposureLayersEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ComposureEditorSettings.h" },
		{ "ModuleRelativePath", "Private/ComposureEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedCompShotClassOverrides_Inner = { "FeaturedCompShotClassOverrides", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedCompShotClassOverrides_MetaData[] = {
		{ "ModuleRelativePath", "Private/ComposureEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedCompShotClassOverrides = { "FeaturedCompShotClassOverrides", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureEditorSettings, FeaturedCompShotClassOverrides), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedCompShotClassOverrides_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedCompShotClassOverrides_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedElementClassOverrides_Inner = { "FeaturedElementClassOverrides", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedElementClassOverrides_MetaData[] = {
		{ "ModuleRelativePath", "Private/ComposureEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedElementClassOverrides = { "FeaturedElementClassOverrides", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UComposureEditorSettings, FeaturedElementClassOverrides), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedElementClassOverrides_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedElementClassOverrides_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UComposureEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedCompShotClassOverrides_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedCompShotClassOverrides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedElementClassOverrides_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UComposureEditorSettings_Statics::NewProp_FeaturedElementClassOverrides,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UComposureEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UComposureEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UComposureEditorSettings_Statics::ClassParams = {
		&UComposureEditorSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UComposureEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UComposureEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UComposureEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UComposureEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UComposureEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UComposureEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UComposureEditorSettings, 1151141971);
	template<> COMPOSURELAYERSEDITOR_API UClass* StaticClass<UComposureEditorSettings>()
	{
		return UComposureEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UComposureEditorSettings(Z_Construct_UClass_UComposureEditorSettings, &UComposureEditorSettings::StaticClass, TEXT("/Script/ComposureLayersEditor"), TEXT("UComposureEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UComposureEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
