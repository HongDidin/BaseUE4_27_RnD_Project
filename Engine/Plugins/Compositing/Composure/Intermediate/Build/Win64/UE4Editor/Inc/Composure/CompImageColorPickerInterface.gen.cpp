// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Composure/Public/EditorSupport/CompImageColorPickerInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCompImageColorPickerInterface() {}
// Cross Module References
	COMPOSURE_API UClass* Z_Construct_UClass_UCompImageColorPickerInterface_NoRegister();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompImageColorPickerInterface();
	COMPOSURE_API UClass* Z_Construct_UClass_UCompEditorImagePreviewInterface();
	UPackage* Z_Construct_UPackage__Script_Composure();
// End Cross Module References
	void UCompImageColorPickerInterface::StaticRegisterNativesUCompImageColorPickerInterface()
	{
	}
	UClass* Z_Construct_UClass_UCompImageColorPickerInterface_NoRegister()
	{
		return UCompImageColorPickerInterface::StaticClass();
	}
	struct Z_Construct_UClass_UCompImageColorPickerInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCompImageColorPickerInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCompEditorImagePreviewInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Composure,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCompImageColorPickerInterface_Statics::Class_MetaDataParams[] = {
		{ "CannotImplementInterfaceInBlueprint", "" },
		{ "ModuleRelativePath", "Public/EditorSupport/CompImageColorPickerInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCompImageColorPickerInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ICompImageColorPickerInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCompImageColorPickerInterface_Statics::ClassParams = {
		&UCompImageColorPickerInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UCompImageColorPickerInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCompImageColorPickerInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCompImageColorPickerInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCompImageColorPickerInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCompImageColorPickerInterface, 3831016767);
	template<> COMPOSURE_API UClass* StaticClass<UCompImageColorPickerInterface>()
	{
		return UCompImageColorPickerInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCompImageColorPickerInterface(Z_Construct_UClass_UCompImageColorPickerInterface, &UCompImageColorPickerInterface::StaticClass, TEXT("/Script/Composure"), TEXT("UCompImageColorPickerInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCompImageColorPickerInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
