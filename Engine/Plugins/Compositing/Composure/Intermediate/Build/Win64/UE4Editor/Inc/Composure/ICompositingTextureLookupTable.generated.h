// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UTexture;
#ifdef COMPOSURE_ICompositingTextureLookupTable_generated_h
#error "ICompositingTextureLookupTable.generated.h already included, missing '#pragma once' in ICompositingTextureLookupTable.h"
#endif
#define COMPOSURE_ICompositingTextureLookupTable_generated_h

#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_SPARSE_DATA
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execFindNamedPassResult);


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFindNamedPassResult);


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompositingTextureLookupTable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompositingTextureLookupTable) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompositingTextureLookupTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingTextureLookupTable); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompositingTextureLookupTable(UCompositingTextureLookupTable&&); \
	COMPOSURE_API UCompositingTextureLookupTable(const UCompositingTextureLookupTable&); \
public:


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	COMPOSURE_API UCompositingTextureLookupTable(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	COMPOSURE_API UCompositingTextureLookupTable(UCompositingTextureLookupTable&&); \
	COMPOSURE_API UCompositingTextureLookupTable(const UCompositingTextureLookupTable&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(COMPOSURE_API, UCompositingTextureLookupTable); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCompositingTextureLookupTable); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCompositingTextureLookupTable)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUCompositingTextureLookupTable(); \
	friend struct Z_Construct_UClass_UCompositingTextureLookupTable_Statics; \
public: \
	DECLARE_CLASS(UCompositingTextureLookupTable, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Composure"), COMPOSURE_API) \
	DECLARE_SERIALIZER(UCompositingTextureLookupTable)


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~ICompositingTextureLookupTable() {} \
public: \
	typedef UCompositingTextureLookupTable UClassType; \
	typedef ICompositingTextureLookupTable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_INCLASS_IINTERFACE \
protected: \
	virtual ~ICompositingTextureLookupTable() {} \
public: \
	typedef UCompositingTextureLookupTable UClassType; \
	typedef ICompositingTextureLookupTable ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_12_PROLOG
#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_RPC_WRAPPERS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_SPARSE_DATA \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h_15_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMPOSURE_API UClass* StaticClass<class UCompositingTextureLookupTable>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_Composure_Source_Composure_Classes_CompositingElements_ICompositingTextureLookupTable_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
