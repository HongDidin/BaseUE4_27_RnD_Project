// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenCVLensDistortion/Public/OpenCVLensDistortionBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenCVLensDistortionBlueprintLibrary() {}
// Cross Module References
	OPENCVLENSDISTORTION_API UClass* Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_NoRegister();
	OPENCVLENSDISTORTION_API UClass* Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_OpenCVLensDistortion();
	OPENCVLENSDISTORTION_API UScriptStruct* Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	OPENCVLENSDISTORTION_API UScriptStruct* Z_Construct_UScriptStruct_FOpenCVCameraViewInfo();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UOpenCVLensDistortionBlueprintLibrary::execNotEqual_CompareLensDistortionModels)
	{
		P_GET_STRUCT_REF(FOpenCVLensDistortionParameters,Z_Param_Out_A);
		P_GET_STRUCT_REF(FOpenCVLensDistortionParameters,Z_Param_Out_B);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOpenCVLensDistortionBlueprintLibrary::NotEqual_CompareLensDistortionModels(Z_Param_Out_A,Z_Param_Out_B);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOpenCVLensDistortionBlueprintLibrary::execEqualEqual_CompareLensDistortionModels)
	{
		P_GET_STRUCT_REF(FOpenCVLensDistortionParameters,Z_Param_Out_A);
		P_GET_STRUCT_REF(FOpenCVLensDistortionParameters,Z_Param_Out_B);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UOpenCVLensDistortionBlueprintLibrary::EqualEqual_CompareLensDistortionModels(Z_Param_Out_A,Z_Param_Out_B);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOpenCVLensDistortionBlueprintLibrary::execCreateUndistortUVDisplacementMap)
	{
		P_GET_STRUCT_REF(FOpenCVLensDistortionParameters,Z_Param_Out_LensParameters);
		P_GET_STRUCT_REF(FIntPoint,Z_Param_Out_ImageSize);
		P_GET_PROPERTY(FFloatProperty,Z_Param_CroppingFactor);
		P_GET_STRUCT_REF(FOpenCVCameraViewInfo,Z_Param_Out_CameraViewInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTexture2D**)Z_Param__Result=UOpenCVLensDistortionBlueprintLibrary::CreateUndistortUVDisplacementMap(Z_Param_Out_LensParameters,Z_Param_Out_ImageSize,Z_Param_CroppingFactor,Z_Param_Out_CameraViewInfo);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOpenCVLensDistortionBlueprintLibrary::execDrawDisplacementMapToRenderTarget)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_OutputRenderTarget);
		P_GET_OBJECT(UTexture2D,Z_Param_PreComputedUndistortDisplacementMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		UOpenCVLensDistortionBlueprintLibrary::DrawDisplacementMapToRenderTarget(Z_Param_WorldContextObject,Z_Param_OutputRenderTarget,Z_Param_PreComputedUndistortDisplacementMap);
		P_NATIVE_END;
	}
	void UOpenCVLensDistortionBlueprintLibrary::StaticRegisterNativesUOpenCVLensDistortionBlueprintLibrary()
	{
		UClass* Class = UOpenCVLensDistortionBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateUndistortUVDisplacementMap", &UOpenCVLensDistortionBlueprintLibrary::execCreateUndistortUVDisplacementMap },
			{ "DrawDisplacementMapToRenderTarget", &UOpenCVLensDistortionBlueprintLibrary::execDrawDisplacementMapToRenderTarget },
			{ "EqualEqual_CompareLensDistortionModels", &UOpenCVLensDistortionBlueprintLibrary::execEqualEqual_CompareLensDistortionModels },
			{ "NotEqual_CompareLensDistortionModels", &UOpenCVLensDistortionBlueprintLibrary::execNotEqual_CompareLensDistortionModels },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics
	{
		struct OpenCVLensDistortionBlueprintLibrary_eventCreateUndistortUVDisplacementMap_Parms
		{
			FOpenCVLensDistortionParameters LensParameters;
			FIntPoint ImageSize;
			float CroppingFactor;
			FOpenCVCameraViewInfo CameraViewInfo;
			UTexture2D* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LensParameters;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImageSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImageSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CroppingFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CroppingFactor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraViewInfo;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_LensParameters_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_LensParameters = { "LensParameters", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventCreateUndistortUVDisplacementMap_Parms, LensParameters), Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_LensParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_LensParameters_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_ImageSize_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_ImageSize = { "ImageSize", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventCreateUndistortUVDisplacementMap_Parms, ImageSize), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_ImageSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_ImageSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_CroppingFactor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_CroppingFactor = { "CroppingFactor", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventCreateUndistortUVDisplacementMap_Parms, CroppingFactor), METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_CroppingFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_CroppingFactor_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_CameraViewInfo = { "CameraViewInfo", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventCreateUndistortUVDisplacementMap_Parms, CameraViewInfo), Z_Construct_UScriptStruct_FOpenCVCameraViewInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventCreateUndistortUVDisplacementMap_Parms, ReturnValue), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_LensParameters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_ImageSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_CroppingFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_CameraViewInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Distortion | OpenCV" },
		{ "Comment", "/**\n\x09 * Creates a texture containing a DisplacementMap in the Red and the Green channel for undistorting a camera image.\n\x09 * This call can take quite some time to process depending on the resolution.\n\x09 * @param LensParameters The Lens distortion parameters with which to compute the UV displacement map.\n\x09 * @param ImageSize The size of the camera image to be undistorted in pixels. Scaled down resolution will have an impact. \n\x09 * @param CroppingFactor One means OpenCV will attempt to crop out all empty pixels resulting from the process (essentially zooming the image). Zero will keep all pixels.\n\x09 * @param CameraViewInfo Information computed by OpenCV about the undistorted space. Can be used with SceneCapture to adjust FOV.\n\x09 * @return Texture2D containing the distort to undistort space displacement map.\n\x09 */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionBlueprintLibrary.h" },
		{ "ToolTip", "Creates a texture containing a DisplacementMap in the Red and the Green channel for undistorting a camera image.\nThis call can take quite some time to process depending on the resolution.\n@param LensParameters The Lens distortion parameters with which to compute the UV displacement map.\n@param ImageSize The size of the camera image to be undistorted in pixels. Scaled down resolution will have an impact.\n@param CroppingFactor One means OpenCV will attempt to crop out all empty pixels resulting from the process (essentially zooming the image). Zero will keep all pixels.\n@param CameraViewInfo Information computed by OpenCV about the undistorted space. Can be used with SceneCapture to adjust FOV.\n@return Texture2D containing the distort to undistort space displacement map." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary, nullptr, "CreateUndistortUVDisplacementMap", nullptr, nullptr, sizeof(OpenCVLensDistortionBlueprintLibrary_eventCreateUndistortUVDisplacementMap_Parms), Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C22401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics
	{
		struct OpenCVLensDistortionBlueprintLibrary_eventDrawDisplacementMapToRenderTarget_Parms
		{
			const UObject* WorldContextObject;
			UTextureRenderTarget2D* OutputRenderTarget;
			UTexture2D* PreComputedUndistortDisplacementMap;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutputRenderTarget;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreComputedUndistortDisplacementMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventDrawDisplacementMapToRenderTarget_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_OutputRenderTarget = { "OutputRenderTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventDrawDisplacementMapToRenderTarget_Parms, OutputRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_PreComputedUndistortDisplacementMap = { "PreComputedUndistortDisplacementMap", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventDrawDisplacementMapToRenderTarget_Parms, PreComputedUndistortDisplacementMap), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_OutputRenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::NewProp_PreComputedUndistortDisplacementMap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Distortion | OpenCV" },
		{ "Comment", "/** Draws UV displacement map within the output render target.\n\x09 * - Red & green channels hold the distort to undistort displacement;\n\x09 * - Blue & alpha channels hold the undistort to distort displacement.\n\x09 * @param WorldContextObject Current world to get the rendering settings from (such as feature level).\n\x09 * @param OutputRenderTarget The render target to draw to. Don't necessarily need to have same resolution or aspect ratio as distorted render.\n\x09 * @param PreComputedUndistortDisplacementMap Distort to undistort displacement pre computed using OpenCV in engine or externally.\n\x09 */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionBlueprintLibrary.h" },
		{ "ToolTip", "Draws UV displacement map within the output render target.\n- Red & green channels hold the distort to undistort displacement;\n- Blue & alpha channels hold the undistort to distort displacement.\n@param WorldContextObject Current world to get the rendering settings from (such as feature level).\n@param OutputRenderTarget The render target to draw to. Don't necessarily need to have same resolution or aspect ratio as distorted render.\n@param PreComputedUndistortDisplacementMap Distort to undistort displacement pre computed using OpenCV in engine or externally." },
		{ "WorldContext", "WorldContextObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary, nullptr, "DrawDisplacementMapToRenderTarget", nullptr, nullptr, sizeof(OpenCVLensDistortionBlueprintLibrary_eventDrawDisplacementMapToRenderTarget_Parms), Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics
	{
		struct OpenCVLensDistortionBlueprintLibrary_eventEqualEqual_CompareLensDistortionModels_Parms
		{
			FOpenCVLensDistortionParameters A;
			FOpenCVLensDistortionParameters B;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_A_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventEqualEqual_CompareLensDistortionModels_Parms, A), Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_B_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventEqualEqual_CompareLensDistortionModels_Parms, B), Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_B_MetaData)) };
	void Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OpenCVLensDistortionBlueprintLibrary_eventEqualEqual_CompareLensDistortionModels_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(OpenCVLensDistortionBlueprintLibrary_eventEqualEqual_CompareLensDistortionModels_Parms), &Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Distortion" },
		{ "Comment", "/* Returns true if A is equal to B (A == B) */" },
		{ "CompactNodeTitle", "==" },
		{ "DisplayName", "Equal (LensDistortionParameters)" },
		{ "Keywords", "== equal" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionBlueprintLibrary.h" },
		{ "ScriptOperator", "==" },
		{ "ToolTip", "Returns true if A is equal to B (A == B)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary, nullptr, "EqualEqual_CompareLensDistortionModels", nullptr, nullptr, sizeof(OpenCVLensDistortionBlueprintLibrary_eventEqualEqual_CompareLensDistortionModels_Parms), Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics
	{
		struct OpenCVLensDistortionBlueprintLibrary_eventNotEqual_CompareLensDistortionModels_Parms
		{
			FOpenCVLensDistortionParameters A;
			FOpenCVLensDistortionParameters B;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_A_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_A;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_B_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_B;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_A_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_A = { "A", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventNotEqual_CompareLensDistortionModels_Parms, A), Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_A_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_A_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_B_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_B = { "B", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenCVLensDistortionBlueprintLibrary_eventNotEqual_CompareLensDistortionModels_Parms, B), Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_B_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_B_MetaData)) };
	void Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((OpenCVLensDistortionBlueprintLibrary_eventNotEqual_CompareLensDistortionModels_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(OpenCVLensDistortionBlueprintLibrary_eventNotEqual_CompareLensDistortionModels_Parms), &Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_A,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_B,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::Function_MetaDataParams[] = {
		{ "Category", "Lens Distortion" },
		{ "Comment", "/* Returns true if A is not equal to B (A != B) */" },
		{ "CompactNodeTitle", "!=" },
		{ "DisplayName", "NotEqual (LensDistortionParameters)" },
		{ "Keywords", "!= not equal" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionBlueprintLibrary.h" },
		{ "ScriptOperator", "!=" },
		{ "ToolTip", "Returns true if A is not equal to B (A != B)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary, nullptr, "NotEqual_CompareLensDistortionModels", nullptr, nullptr, sizeof(OpenCVLensDistortionBlueprintLibrary_eventNotEqual_CompareLensDistortionModels_Parms), Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_NoRegister()
	{
		return UOpenCVLensDistortionBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenCVLensDistortion,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_CreateUndistortUVDisplacementMap, "CreateUndistortUVDisplacementMap" }, // 1699954799
		{ &Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_DrawDisplacementMapToRenderTarget, "DrawDisplacementMapToRenderTarget" }, // 3271341749
		{ &Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_EqualEqual_CompareLensDistortionModels, "EqualEqual_CompareLensDistortionModels" }, // 2830279283
		{ &Z_Construct_UFunction_UOpenCVLensDistortionBlueprintLibrary_NotEqual_CompareLensDistortionModels, "NotEqual_CompareLensDistortionModels" }, // 1390566806
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OpenCVLensDistortionBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionBlueprintLibrary.h" },
		{ "ScriptName", "OpenCVLensDistortionLibrary" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOpenCVLensDistortionBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::ClassParams = {
		&UOpenCVLensDistortionBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOpenCVLensDistortionBlueprintLibrary, 2275510039);
	template<> OPENCVLENSDISTORTION_API UClass* StaticClass<UOpenCVLensDistortionBlueprintLibrary>()
	{
		return UOpenCVLensDistortionBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOpenCVLensDistortionBlueprintLibrary(Z_Construct_UClass_UOpenCVLensDistortionBlueprintLibrary, &UOpenCVLensDistortionBlueprintLibrary::StaticClass, TEXT("/Script/OpenCVLensDistortion"), TEXT("UOpenCVLensDistortionBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOpenCVLensDistortionBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
