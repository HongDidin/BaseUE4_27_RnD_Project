// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenCVLensDistortion/Public/OpenCVLensDistortionParameters.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenCVLensDistortionParameters() {}
// Cross Module References
	OPENCVLENSDISTORTION_API UScriptStruct* Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters();
	UPackage* Z_Construct_UPackage__Script_OpenCVLensDistortion();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	OPENCVLENSDISTORTION_API UScriptStruct* Z_Construct_UScriptStruct_FOpenCVCameraViewInfo();
// End Cross Module References
class UScriptStruct* FOpenCVLensDistortionParameters::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPENCVLENSDISTORTION_API uint32 Get_Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters, Z_Construct_UPackage__Script_OpenCVLensDistortion(), TEXT("OpenCVLensDistortionParameters"), sizeof(FOpenCVLensDistortionParameters), Get_Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Hash());
	}
	return Singleton;
}
template<> OPENCVLENSDISTORTION_API UScriptStruct* StaticStruct<FOpenCVLensDistortionParameters>()
{
	return FOpenCVLensDistortionParameters::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOpenCVLensDistortionParameters(FOpenCVLensDistortionParameters::StaticStruct, TEXT("/Script/OpenCVLensDistortion"), TEXT("OpenCVLensDistortionParameters"), false, nullptr, nullptr);
static struct FScriptStruct_OpenCVLensDistortion_StaticRegisterNativesFOpenCVLensDistortionParameters
{
	FScriptStruct_OpenCVLensDistortion_StaticRegisterNativesFOpenCVLensDistortionParameters()
	{
		UScriptStruct::DeferCppStructOps<FOpenCVLensDistortionParameters>(FName(TEXT("OpenCVLensDistortionParameters")));
	}
} ScriptStruct_OpenCVLensDistortion_StaticRegisterNativesFOpenCVLensDistortionParameters;
	struct Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K1_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K2_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_P1_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_P1;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_P2_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_P2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K3_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K3;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K4_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K5_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K5;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_K6_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_K6;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_F_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_F;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_C_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_C;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseFisheyeModel_MetaData[];
#endif
		static void NewProp_bUseFisheyeModel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseFisheyeModel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Mathematic camera model for lens distortion/undistortion.\n * Camera matrix =\n *  | F.X  0  C.x |\n *  |  0  F.Y C.Y |\n *  |  0   0   1  |\n * where F and C are normalized.\n */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Mathematic camera model for lens distortion/undistortion.\nCamera matrix =\n | F.X  0  C.x |\n |  0  F.Y C.Y |\n |  0   0   1  |\nwhere F and C are normalized." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOpenCVLensDistortionParameters>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K1_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Radial parameter #1. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Radial parameter #1." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K1 = { "K1", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, K1), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K2_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Radial parameter #2. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Radial parameter #2." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K2 = { "K2", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, K2), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P1_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Tangential parameter #1. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Tangential parameter #1." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P1 = { "P1", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, P1), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P1_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P1_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P2_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Tangential parameter #2. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Tangential parameter #2." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P2 = { "P2", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, P2), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K3_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Radial parameter #3. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Radial parameter #3." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K3 = { "K3", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, K3), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K3_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K3_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K4_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Radial parameter #4. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Radial parameter #4." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K4 = { "K4", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, K4), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K4_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K5_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Radial parameter #5. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Radial parameter #5." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K5 = { "K5", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, K5), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K5_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K5_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K6_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Radial parameter #6. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Radial parameter #6." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K6 = { "K6", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, K6), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K6_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K6_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_F_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Camera matrix's normalized Fx and Fy. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Camera matrix's normalized Fx and Fy." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_F = { "F", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, F), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_F_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_F_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_C_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Camera matrix's normalized Cx and Cy. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Camera matrix's normalized Cx and Cy." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_C = { "C", nullptr, (EPropertyFlags)0x0010000200000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVLensDistortionParameters, C), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_C_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_C_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_bUseFisheyeModel_MetaData[] = {
		{ "Category", "Lens Distortion|Parameters" },
		{ "Comment", "/** Camera lens needs Fisheye camera model. */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Camera lens needs Fisheye camera model." },
	};
#endif
	void Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_bUseFisheyeModel_SetBit(void* Obj)
	{
		((FOpenCVLensDistortionParameters*)Obj)->bUseFisheyeModel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_bUseFisheyeModel = { "bUseFisheyeModel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FOpenCVLensDistortionParameters), &Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_bUseFisheyeModel_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_bUseFisheyeModel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_bUseFisheyeModel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P1,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_P2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K3,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K5,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_K6,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_F,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_C,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::NewProp_bUseFisheyeModel,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OpenCVLensDistortion,
		nullptr,
		&NewStructOps,
		"OpenCVLensDistortionParameters",
		sizeof(FOpenCVLensDistortionParameters),
		alignof(FOpenCVLensDistortionParameters),
		Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OpenCVLensDistortion();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OpenCVLensDistortionParameters"), sizeof(FOpenCVLensDistortionParameters), Get_Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOpenCVLensDistortionParameters_Hash() { return 1463286307U; }
class UScriptStruct* FOpenCVCameraViewInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPENCVLENSDISTORTION_API uint32 Get_Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo, Z_Construct_UPackage__Script_OpenCVLensDistortion(), TEXT("OpenCVCameraViewInfo"), sizeof(FOpenCVCameraViewInfo), Get_Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Hash());
	}
	return Singleton;
}
template<> OPENCVLENSDISTORTION_API UScriptStruct* StaticStruct<FOpenCVCameraViewInfo>()
{
	return FOpenCVCameraViewInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOpenCVCameraViewInfo(FOpenCVCameraViewInfo::StaticStruct, TEXT("/Script/OpenCVLensDistortion"), TEXT("OpenCVCameraViewInfo"), false, nullptr, nullptr);
static struct FScriptStruct_OpenCVLensDistortion_StaticRegisterNativesFOpenCVCameraViewInfo
{
	FScriptStruct_OpenCVLensDistortion_StaticRegisterNativesFOpenCVCameraViewInfo()
	{
		UScriptStruct::DeferCppStructOps<FOpenCVCameraViewInfo>(FName(TEXT("OpenCVCameraViewInfo")));
	}
} ScriptStruct_OpenCVLensDistortion_StaticRegisterNativesFOpenCVCameraViewInfo;
	struct Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HorizontalFOV_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HorizontalFOV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VerticalFOV_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VerticalFOV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLengthRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FocalLengthRatio;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOpenCVCameraViewInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_HorizontalFOV_MetaData[] = {
		{ "Category", "Camera Info" },
		{ "Comment", "/** Horizontal Field Of View in degrees */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Horizontal Field Of View in degrees" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_HorizontalFOV = { "HorizontalFOV", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVCameraViewInfo, HorizontalFOV), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_HorizontalFOV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_HorizontalFOV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_VerticalFOV_MetaData[] = {
		{ "Category", "Camera Info" },
		{ "Comment", "/** Vertical Field Of View in degrees */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Vertical Field Of View in degrees" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_VerticalFOV = { "VerticalFOV", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVCameraViewInfo, VerticalFOV), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_VerticalFOV_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_VerticalFOV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_FocalLengthRatio_MetaData[] = {
		{ "Category", "Camera Info" },
		{ "Comment", "/** Focal length aspect ratio -> Fy / Fx */" },
		{ "ModuleRelativePath", "Public/OpenCVLensDistortionParameters.h" },
		{ "ToolTip", "Focal length aspect ratio -> Fy / Fx" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_FocalLengthRatio = { "FocalLengthRatio", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenCVCameraViewInfo, FocalLengthRatio), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_FocalLengthRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_FocalLengthRatio_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_HorizontalFOV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_VerticalFOV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::NewProp_FocalLengthRatio,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OpenCVLensDistortion,
		nullptr,
		&NewStructOps,
		"OpenCVCameraViewInfo",
		sizeof(FOpenCVCameraViewInfo),
		alignof(FOpenCVCameraViewInfo),
		Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOpenCVCameraViewInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OpenCVLensDistortion();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OpenCVCameraViewInfo"), sizeof(FOpenCVCameraViewInfo), Get_Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOpenCVCameraViewInfo_Hash() { return 2790192004U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
