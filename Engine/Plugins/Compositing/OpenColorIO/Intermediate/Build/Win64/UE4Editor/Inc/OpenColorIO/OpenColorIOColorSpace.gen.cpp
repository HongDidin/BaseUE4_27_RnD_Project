// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenColorIO/Public/OpenColorIOColorSpace.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenColorIOColorSpace() {}
// Cross Module References
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration();
	UPackage* Z_Construct_UPackage__Script_OpenColorIO();
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings();
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIOConfiguration_NoRegister();
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIOColorSpace();
// End Cross Module References
class UScriptStruct* FOpenColorIODisplayConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPENCOLORIO_API uint32 Get_Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration, Z_Construct_UPackage__Script_OpenColorIO(), TEXT("OpenColorIODisplayConfiguration"), sizeof(FOpenColorIODisplayConfiguration), Get_Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Hash());
	}
	return Singleton;
}
template<> OPENCOLORIO_API UScriptStruct* StaticStruct<FOpenColorIODisplayConfiguration>()
{
	return FOpenColorIODisplayConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOpenColorIODisplayConfiguration(FOpenColorIODisplayConfiguration::StaticStruct, TEXT("/Script/OpenColorIO"), TEXT("OpenColorIODisplayConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIODisplayConfiguration
{
	FScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIODisplayConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FOpenColorIODisplayConfiguration>(FName(TEXT("OpenColorIODisplayConfiguration")));
	}
} ScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIODisplayConfiguration;
	struct Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ColorConfiguration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Identifies an OCIO Display look configuration \n */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "Identifies an OCIO Display look configuration" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOpenColorIODisplayConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** Whether or not this display configuration is enabled\n\x09 *  Since display look are applied on viewports, this will \n\x09 * dictate whether it's applied or not to it\n\x09 */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "Whether or not this display configuration is enabled\nSince display look are applied on viewports, this will\ndictate whether it's applied or not to it" },
	};
#endif
	void Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((FOpenColorIODisplayConfiguration*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FOpenColorIODisplayConfiguration), &Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_ColorConfiguration_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** Conversion to apply when this display is enabled */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "Conversion to apply when this display is enabled" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_ColorConfiguration = { "ColorConfiguration", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenColorIODisplayConfiguration, ColorConfiguration), Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_ColorConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_ColorConfiguration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::NewProp_ColorConfiguration,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIO,
		nullptr,
		&NewStructOps,
		"OpenColorIODisplayConfiguration",
		sizeof(FOpenColorIODisplayConfiguration),
		alignof(FOpenColorIODisplayConfiguration),
		Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OpenColorIO();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OpenColorIODisplayConfiguration"), sizeof(FOpenColorIODisplayConfiguration), Get_Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Hash() { return 4268526066U; }
class UScriptStruct* FOpenColorIOColorConversionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPENCOLORIO_API uint32 Get_Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings, Z_Construct_UPackage__Script_OpenColorIO(), TEXT("OpenColorIOColorConversionSettings"), sizeof(FOpenColorIOColorConversionSettings), Get_Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Hash());
	}
	return Singleton;
}
template<> OPENCOLORIO_API UScriptStruct* StaticStruct<FOpenColorIOColorConversionSettings>()
{
	return FOpenColorIOColorConversionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOpenColorIOColorConversionSettings(FOpenColorIOColorConversionSettings::StaticStruct, TEXT("/Script/OpenColorIO"), TEXT("OpenColorIOColorConversionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIOColorConversionSettings
{
	FScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIOColorConversionSettings()
	{
		UScriptStruct::DeferCppStructOps<FOpenColorIOColorConversionSettings>(FName(TEXT("OpenColorIOColorConversionSettings")));
	}
} ScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIOColorConversionSettings;
	struct Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigurationSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConfigurationSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceColorSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourceColorSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationColorSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DestinationColorSpace;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Identifies a OCIO ColorSpace conversion.\n */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "Identifies a OCIO ColorSpace conversion." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOpenColorIOColorConversionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_ConfigurationSource_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** The source color space name. */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "The source color space name." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_ConfigurationSource = { "ConfigurationSource", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenColorIOColorConversionSettings, ConfigurationSource), Z_Construct_UClass_UOpenColorIOConfiguration_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_ConfigurationSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_ConfigurationSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_SourceColorSpace_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** The source color space name. */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "The source color space name." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_SourceColorSpace = { "SourceColorSpace", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenColorIOColorConversionSettings, SourceColorSpace), Z_Construct_UScriptStruct_FOpenColorIOColorSpace, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_SourceColorSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_SourceColorSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_DestinationColorSpace_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** The destination color space name. */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "The destination color space name." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_DestinationColorSpace = { "DestinationColorSpace", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenColorIOColorConversionSettings, DestinationColorSpace), Z_Construct_UScriptStruct_FOpenColorIOColorSpace, METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_DestinationColorSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_DestinationColorSpace_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_ConfigurationSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_SourceColorSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::NewProp_DestinationColorSpace,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIO,
		nullptr,
		&NewStructOps,
		"OpenColorIOColorConversionSettings",
		sizeof(FOpenColorIOColorConversionSettings),
		alignof(FOpenColorIOColorConversionSettings),
		Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OpenColorIO();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OpenColorIOColorConversionSettings"), sizeof(FOpenColorIOColorConversionSettings), Get_Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Hash() { return 1752482614U; }
class UScriptStruct* FOpenColorIOColorSpace::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPENCOLORIO_API uint32 Get_Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOpenColorIOColorSpace, Z_Construct_UPackage__Script_OpenColorIO(), TEXT("OpenColorIOColorSpace"), sizeof(FOpenColorIOColorSpace), Get_Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Hash());
	}
	return Singleton;
}
template<> OPENCOLORIO_API UScriptStruct* StaticStruct<FOpenColorIOColorSpace>()
{
	return FOpenColorIOColorSpace::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOpenColorIOColorSpace(FOpenColorIOColorSpace::StaticStruct, TEXT("/Script/OpenColorIO"), TEXT("OpenColorIOColorSpace"), false, nullptr, nullptr);
static struct FScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIOColorSpace
{
	FScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIOColorSpace()
	{
		UScriptStruct::DeferCppStructOps<FOpenColorIOColorSpace>(FName(TEXT("OpenColorIOColorSpace")));
	}
} ScriptStruct_OpenColorIO_StaticRegisterNativesFOpenColorIOColorSpace;
	struct Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorSpaceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ColorSpaceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorSpaceIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ColorSpaceIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FamilyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FamilyName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Structure to identify a ColorSpace as described in an OCIO configuration file. \n * Members are populated by data coming from a config file.\n */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "Structure to identify a ColorSpace as described in an OCIO configuration file.\nMembers are populated by data coming from a config file." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOpenColorIOColorSpace>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceName_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** The ColorSpace name. */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "The ColorSpace name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceName = { "ColorSpaceName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenColorIOColorSpace, ColorSpaceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceIndex_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** The index of the ColorSpace in the config */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "The index of the ColorSpace in the config" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceIndex = { "ColorSpaceIndex", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenColorIOColorSpace, ColorSpaceIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_FamilyName_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "Comment", "/** \n\x09 * The family of this ColorSpace as specified in the configuration file. \n\x09 * When you have lots of colorspaces, you can regroup them by family to facilitate browsing them. \n\x09 */" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorSpace.h" },
		{ "ToolTip", "The family of this ColorSpace as specified in the configuration file.\nWhen you have lots of colorspaces, you can regroup them by family to facilitate browsing them." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_FamilyName = { "FamilyName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOpenColorIOColorSpace, FamilyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_FamilyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_FamilyName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_ColorSpaceIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::NewProp_FamilyName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIO,
		nullptr,
		&NewStructOps,
		"OpenColorIOColorSpace",
		sizeof(FOpenColorIOColorSpace),
		alignof(FOpenColorIOColorSpace),
		Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIOColorSpace()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OpenColorIO();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OpenColorIOColorSpace"), sizeof(FOpenColorIOColorSpace), Get_Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Hash() { return 48890777U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
