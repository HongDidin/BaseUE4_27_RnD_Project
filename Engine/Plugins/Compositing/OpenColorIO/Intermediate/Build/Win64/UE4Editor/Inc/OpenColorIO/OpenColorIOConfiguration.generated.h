// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCOLORIO_OpenColorIOConfiguration_generated_h
#error "OpenColorIOConfiguration.generated.h already included, missing '#pragma once' in OpenColorIOConfiguration.h"
#endif
#define OPENCOLORIO_OpenColorIOConfiguration_generated_h

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_SPARSE_DATA
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_RPC_WRAPPERS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOpenColorIOConfiguration(); \
	friend struct Z_Construct_UClass_UOpenColorIOConfiguration_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOConfiguration, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenColorIO"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOConfiguration)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUOpenColorIOConfiguration(); \
	friend struct Z_Construct_UClass_UOpenColorIOConfiguration_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOConfiguration, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenColorIO"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOConfiguration)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOpenColorIOConfiguration(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOConfiguration) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOConfiguration); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOConfiguration); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOConfiguration(UOpenColorIOConfiguration&&); \
	NO_API UOpenColorIOConfiguration(const UOpenColorIOConfiguration&); \
public:


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOConfiguration(UOpenColorIOConfiguration&&); \
	NO_API UOpenColorIOConfiguration(const UOpenColorIOConfiguration&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOConfiguration); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOConfiguration); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOConfiguration)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ColorTransforms() { return STRUCT_OFFSET(UOpenColorIOConfiguration, ColorTransforms); }


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_28_PROLOG
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_RPC_WRAPPERS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_INCLASS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h_31_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCOLORIO_API UClass* StaticClass<class UOpenColorIOConfiguration>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOConfiguration_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
