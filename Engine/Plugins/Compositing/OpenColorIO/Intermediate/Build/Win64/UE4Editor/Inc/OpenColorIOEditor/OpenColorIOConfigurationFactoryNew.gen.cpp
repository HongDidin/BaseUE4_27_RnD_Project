// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenColorIOEditor/Private/Factories/OpenColorIOConfigurationFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenColorIOConfigurationFactoryNew() {}
// Cross Module References
	OPENCOLORIOEDITOR_API UClass* Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_NoRegister();
	OPENCOLORIOEDITOR_API UClass* Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_OpenColorIOEditor();
// End Cross Module References
	void UOpenColorIOConfigurationFactoryNew::StaticRegisterNativesUOpenColorIOConfigurationFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_NoRegister()
	{
		return UOpenColorIOConfigurationFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIOEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UOpenColorIOConfiguration objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/OpenColorIOConfigurationFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/OpenColorIOConfigurationFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UOpenColorIOConfiguration objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOpenColorIOConfigurationFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics::ClassParams = {
		&UOpenColorIOConfigurationFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOpenColorIOConfigurationFactoryNew, 1364622695);
	template<> OPENCOLORIOEDITOR_API UClass* StaticClass<UOpenColorIOConfigurationFactoryNew>()
	{
		return UOpenColorIOConfigurationFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOpenColorIOConfigurationFactoryNew(Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew, &UOpenColorIOConfigurationFactoryNew::StaticClass, TEXT("/Script/OpenColorIOEditor"), TEXT("UOpenColorIOConfigurationFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOpenColorIOConfigurationFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
