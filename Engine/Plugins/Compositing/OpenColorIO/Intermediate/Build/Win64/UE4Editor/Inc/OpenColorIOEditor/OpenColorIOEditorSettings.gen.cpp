// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenColorIOEditor/Private/OpenColorIOEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenColorIOEditorSettings() {}
// Cross Module References
	OPENCOLORIOEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair();
	UPackage* Z_Construct_UPackage__Script_OpenColorIOEditor();
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration();
	OPENCOLORIOEDITOR_API UClass* Z_Construct_UClass_UOpenColorIOLevelViewportSettings_NoRegister();
	OPENCOLORIOEDITOR_API UClass* Z_Construct_UClass_UOpenColorIOLevelViewportSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FPerViewportDisplaySettingPair::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern OPENCOLORIOEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair, Z_Construct_UPackage__Script_OpenColorIOEditor(), TEXT("PerViewportDisplaySettingPair"), sizeof(FPerViewportDisplaySettingPair), Get_Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Hash());
	}
	return Singleton;
}
template<> OPENCOLORIOEDITOR_API UScriptStruct* StaticStruct<FPerViewportDisplaySettingPair>()
{
	return FPerViewportDisplaySettingPair::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPerViewportDisplaySettingPair(FPerViewportDisplaySettingPair::StaticStruct, TEXT("/Script/OpenColorIOEditor"), TEXT("PerViewportDisplaySettingPair"), false, nullptr, nullptr);
static struct FScriptStruct_OpenColorIOEditor_StaticRegisterNativesFPerViewportDisplaySettingPair
{
	FScriptStruct_OpenColorIOEditor_StaticRegisterNativesFPerViewportDisplaySettingPair()
	{
		UScriptStruct::DeferCppStructOps<FPerViewportDisplaySettingPair>(FName(TEXT("PerViewportDisplaySettingPair")));
	}
} ScriptStruct_OpenColorIOEditor_StaticRegisterNativesFPerViewportDisplaySettingPair;
	struct Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ViewportIdentifier;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayConfiguration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DisplayConfiguration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OpenColorIOEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPerViewportDisplaySettingPair>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_ViewportIdentifier_MetaData[] = {
		{ "Comment", "/*  Name associated with this viewport's layout to identify it. */" },
		{ "ModuleRelativePath", "Private/OpenColorIOEditorSettings.h" },
		{ "ToolTip", "Name associated with this viewport's layout to identify it." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_ViewportIdentifier = { "ViewportIdentifier", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPerViewportDisplaySettingPair, ViewportIdentifier), METADATA_PARAMS(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_ViewportIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_ViewportIdentifier_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_DisplayConfiguration_MetaData[] = {
		{ "Comment", "/* Display configuration for a given viewport */" },
		{ "ModuleRelativePath", "Private/OpenColorIOEditorSettings.h" },
		{ "ToolTip", "Display configuration for a given viewport" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_DisplayConfiguration = { "DisplayConfiguration", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPerViewportDisplaySettingPair, DisplayConfiguration), Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration, METADATA_PARAMS(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_DisplayConfiguration_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_DisplayConfiguration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_ViewportIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::NewProp_DisplayConfiguration,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIOEditor,
		nullptr,
		&NewStructOps,
		"PerViewportDisplaySettingPair",
		sizeof(FPerViewportDisplaySettingPair),
		alignof(FPerViewportDisplaySettingPair),
		Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OpenColorIOEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PerViewportDisplaySettingPair"), sizeof(FPerViewportDisplaySettingPair), Get_Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Hash() { return 1464819890U; }
	void UOpenColorIOLevelViewportSettings::StaticRegisterNativesUOpenColorIOLevelViewportSettings()
	{
	}
	UClass* Z_Construct_UClass_UOpenColorIOLevelViewportSettings_NoRegister()
	{
		return UOpenColorIOLevelViewportSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportsSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportsSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ViewportsSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIOEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * List of settings associated to level viewport instances linked with an identifier\n */" },
		{ "IncludePath", "OpenColorIOEditorSettings.h" },
		{ "ModuleRelativePath", "Private/OpenColorIOEditorSettings.h" },
		{ "ToolTip", "List of settings associated to level viewport instances linked with an identifier" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::NewProp_ViewportsSettings_Inner = { "ViewportsSettings", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::NewProp_ViewportsSettings_MetaData[] = {
		{ "Comment", "/** Settings associated to each viewport that was configured */" },
		{ "ModuleRelativePath", "Private/OpenColorIOEditorSettings.h" },
		{ "ToolTip", "Settings associated to each viewport that was configured" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::NewProp_ViewportsSettings = { "ViewportsSettings", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOpenColorIOLevelViewportSettings, ViewportsSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::NewProp_ViewportsSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::NewProp_ViewportsSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::NewProp_ViewportsSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::NewProp_ViewportsSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOpenColorIOLevelViewportSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::ClassParams = {
		&UOpenColorIOLevelViewportSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOpenColorIOLevelViewportSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOpenColorIOLevelViewportSettings, 3311731211);
	template<> OPENCOLORIOEDITOR_API UClass* StaticClass<UOpenColorIOLevelViewportSettings>()
	{
		return UOpenColorIOLevelViewportSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOpenColorIOLevelViewportSettings(Z_Construct_UClass_UOpenColorIOLevelViewportSettings, &UOpenColorIOLevelViewportSettings::StaticClass, TEXT("/Script/OpenColorIOEditor"), TEXT("UOpenColorIOLevelViewportSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOpenColorIOLevelViewportSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
