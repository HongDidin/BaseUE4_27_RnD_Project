// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCOLORIO_OpenColorIOColorTransform_generated_h
#error "OpenColorIOColorTransform.generated.h already included, missing '#pragma once' in OpenColorIOColorTransform.h"
#endif
#define OPENCOLORIO_OpenColorIOColorTransform_generated_h

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_SPARSE_DATA
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_RPC_WRAPPERS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UOpenColorIOColorTransform, NO_API)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOpenColorIOColorTransform(); \
	friend struct Z_Construct_UClass_UOpenColorIOColorTransform_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOColorTransform, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenColorIO"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOColorTransform) \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_ARCHIVESERIALIZER


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUOpenColorIOColorTransform(); \
	friend struct Z_Construct_UClass_UOpenColorIOColorTransform_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOColorTransform, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenColorIO"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOColorTransform) \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_ARCHIVESERIALIZER


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOpenColorIOColorTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOColorTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOColorTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOColorTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOColorTransform(UOpenColorIOColorTransform&&); \
	NO_API UOpenColorIOColorTransform(const UOpenColorIOColorTransform&); \
public:


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOColorTransform(UOpenColorIOColorTransform&&); \
	NO_API UOpenColorIOColorTransform(const UOpenColorIOColorTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOColorTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOColorTransform); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOColorTransform)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_25_PROLOG
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_RPC_WRAPPERS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_INCLASS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCOLORIO_API UClass* StaticClass<class UOpenColorIOColorTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorTransform_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
