// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenColorIO/Public/OpenColorIODisplayExtensionWrapper.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenColorIODisplayExtensionWrapper() {}
// Cross Module References
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_NoRegister();
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_OpenColorIO();
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor();
// End Cross Module References
	DEFINE_FUNCTION(UOpenColorIODisplayExtensionWrapper::execCreateOpenColorIODisplayExtension)
	{
		P_GET_STRUCT(FOpenColorIODisplayConfiguration,Z_Param_InDisplayConfiguration);
		P_GET_STRUCT_REF(FSceneViewExtensionIsActiveFunctor,Z_Param_Out_IsActiveFunction);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UOpenColorIODisplayExtensionWrapper**)Z_Param__Result=UOpenColorIODisplayExtensionWrapper::CreateOpenColorIODisplayExtension(Z_Param_InDisplayConfiguration,Z_Param_Out_IsActiveFunction);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOpenColorIODisplayExtensionWrapper::execRemoveSceneExtension)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveSceneExtension();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOpenColorIODisplayExtensionWrapper::execSetSceneExtensionIsActiveFunctions)
	{
		P_GET_TARRAY_REF(FSceneViewExtensionIsActiveFunctor,Z_Param_Out_IsActiveFunctions);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSceneExtensionIsActiveFunctions(Z_Param_Out_IsActiveFunctions);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOpenColorIODisplayExtensionWrapper::execSetSceneExtensionIsActiveFunction)
	{
		P_GET_STRUCT_REF(FSceneViewExtensionIsActiveFunctor,Z_Param_Out_IsActiveFunction);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSceneExtensionIsActiveFunction(Z_Param_Out_IsActiveFunction);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UOpenColorIODisplayExtensionWrapper::execSetOpenColorIOConfiguration)
	{
		P_GET_STRUCT(FOpenColorIODisplayConfiguration,Z_Param_InDisplayConfiguration);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetOpenColorIOConfiguration(Z_Param_InDisplayConfiguration);
		P_NATIVE_END;
	}
	void UOpenColorIODisplayExtensionWrapper::StaticRegisterNativesUOpenColorIODisplayExtensionWrapper()
	{
		UClass* Class = UOpenColorIODisplayExtensionWrapper::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CreateOpenColorIODisplayExtension", &UOpenColorIODisplayExtensionWrapper::execCreateOpenColorIODisplayExtension },
			{ "RemoveSceneExtension", &UOpenColorIODisplayExtensionWrapper::execRemoveSceneExtension },
			{ "SetOpenColorIOConfiguration", &UOpenColorIODisplayExtensionWrapper::execSetOpenColorIOConfiguration },
			{ "SetSceneExtensionIsActiveFunction", &UOpenColorIODisplayExtensionWrapper::execSetSceneExtensionIsActiveFunction },
			{ "SetSceneExtensionIsActiveFunctions", &UOpenColorIODisplayExtensionWrapper::execSetSceneExtensionIsActiveFunctions },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics
	{
		struct OpenColorIODisplayExtensionWrapper_eventCreateOpenColorIODisplayExtension_Parms
		{
			FOpenColorIODisplayConfiguration InDisplayConfiguration;
			FSceneViewExtensionIsActiveFunctor IsActiveFunction;
			UOpenColorIODisplayExtensionWrapper* ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InDisplayConfiguration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsActiveFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IsActiveFunction;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_InDisplayConfiguration = { "InDisplayConfiguration", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenColorIODisplayExtensionWrapper_eventCreateOpenColorIODisplayExtension_Parms, InDisplayConfiguration), Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_IsActiveFunction_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_IsActiveFunction = { "IsActiveFunction", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenColorIODisplayExtensionWrapper_eventCreateOpenColorIODisplayExtension_Parms, IsActiveFunction), Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_IsActiveFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_IsActiveFunction_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenColorIODisplayExtensionWrapper_eventCreateOpenColorIODisplayExtension_Parms, ReturnValue), Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_InDisplayConfiguration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_IsActiveFunction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::Function_MetaDataParams[] = {
		{ "Category", "OpenColorIO" },
		{ "Comment", "// Creates an instance of this object, configured with the given arguments (OCIO and activation function).\n" },
		{ "DisplayName", "Create OpenColorIO Display Extension" },
		{ "ModuleRelativePath", "Public/OpenColorIODisplayExtensionWrapper.h" },
		{ "ToolTip", "Creates an instance of this object, configured with the given arguments (OCIO and activation function)." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper, nullptr, "CreateOpenColorIODisplayExtension", nullptr, nullptr, sizeof(OpenColorIODisplayExtensionWrapper_eventCreateOpenColorIODisplayExtension_Parms), Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension_Statics::Function_MetaDataParams[] = {
		{ "Category", "OpenColorIO" },
		{ "Comment", "// Removes the extension.\n" },
		{ "ModuleRelativePath", "Public/OpenColorIODisplayExtensionWrapper.h" },
		{ "ToolTip", "Removes the extension." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper, nullptr, "RemoveSceneExtension", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics
	{
		struct OpenColorIODisplayExtensionWrapper_eventSetOpenColorIOConfiguration_Parms
		{
			FOpenColorIODisplayConfiguration InDisplayConfiguration;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InDisplayConfiguration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::NewProp_InDisplayConfiguration = { "InDisplayConfiguration", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenColorIODisplayExtensionWrapper_eventSetOpenColorIOConfiguration_Parms, InDisplayConfiguration), Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::NewProp_InDisplayConfiguration,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::Function_MetaDataParams[] = {
		{ "Category", "OpenColorIO" },
		{ "Comment", "// Sets a new OCIO configuration.\n" },
		{ "DisplayName", "Set OpenColorIO Configuration" },
		{ "ModuleRelativePath", "Public/OpenColorIODisplayExtensionWrapper.h" },
		{ "ToolTip", "Sets a new OCIO configuration." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper, nullptr, "SetOpenColorIOConfiguration", nullptr, nullptr, sizeof(OpenColorIODisplayExtensionWrapper_eventSetOpenColorIOConfiguration_Parms), Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics
	{
		struct OpenColorIODisplayExtensionWrapper_eventSetSceneExtensionIsActiveFunction_Parms
		{
			FSceneViewExtensionIsActiveFunctor IsActiveFunction;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsActiveFunction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IsActiveFunction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::NewProp_IsActiveFunction_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::NewProp_IsActiveFunction = { "IsActiveFunction", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenColorIODisplayExtensionWrapper_eventSetSceneExtensionIsActiveFunction_Parms, IsActiveFunction), Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::NewProp_IsActiveFunction_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::NewProp_IsActiveFunction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::NewProp_IsActiveFunction,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::Function_MetaDataParams[] = {
		{ "Category", "OpenColorIO" },
		{ "Comment", "// Sets a single activation function. Will remove any others.\n" },
		{ "ModuleRelativePath", "Public/OpenColorIODisplayExtensionWrapper.h" },
		{ "ToolTip", "Sets a single activation function. Will remove any others." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper, nullptr, "SetSceneExtensionIsActiveFunction", nullptr, nullptr, sizeof(OpenColorIODisplayExtensionWrapper_eventSetSceneExtensionIsActiveFunction_Parms), Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics
	{
		struct OpenColorIODisplayExtensionWrapper_eventSetSceneExtensionIsActiveFunctions_Parms
		{
			TArray<FSceneViewExtensionIsActiveFunctor> IsActiveFunctions;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IsActiveFunctions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsActiveFunctions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IsActiveFunctions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::NewProp_IsActiveFunctions_Inner = { "IsActiveFunctions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSceneViewExtensionIsActiveFunctor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::NewProp_IsActiveFunctions_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::NewProp_IsActiveFunctions = { "IsActiveFunctions", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(OpenColorIODisplayExtensionWrapper_eventSetSceneExtensionIsActiveFunctions_Parms, IsActiveFunctions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::NewProp_IsActiveFunctions_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::NewProp_IsActiveFunctions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::NewProp_IsActiveFunctions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::NewProp_IsActiveFunctions,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::Function_MetaDataParams[] = {
		{ "Category", "OpenColorIO" },
		{ "Comment", "// Sets an array of activation functions. Will remove any others.\n" },
		{ "ModuleRelativePath", "Public/OpenColorIODisplayExtensionWrapper.h" },
		{ "ToolTip", "Sets an array of activation functions. Will remove any others." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper, nullptr, "SetSceneExtensionIsActiveFunctions", nullptr, nullptr, sizeof(OpenColorIODisplayExtensionWrapper_eventSetSceneExtensionIsActiveFunctions_Parms), Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_NoRegister()
	{
		return UOpenColorIODisplayExtensionWrapper::StaticClass();
	}
	struct Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIO,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_CreateOpenColorIODisplayExtension, "CreateOpenColorIODisplayExtension" }, // 1741444465
		{ &Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_RemoveSceneExtension, "RemoveSceneExtension" }, // 590542980
		{ &Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetOpenColorIOConfiguration, "SetOpenColorIOConfiguration" }, // 3392014419
		{ &Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunction, "SetSceneExtensionIsActiveFunction" }, // 1701618025
		{ &Z_Construct_UFunction_UOpenColorIODisplayExtensionWrapper_SetSceneExtensionIsActiveFunctions, "SetSceneExtensionIsActiveFunctions" }, // 1417018240
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** \n * This Blueprintable object can hold an OCIO Scene View Extension. \n * You can change its OCIO config, and specify the context in which you want it to be active on.\n */" },
		{ "IncludePath", "OpenColorIODisplayExtensionWrapper.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/OpenColorIODisplayExtensionWrapper.h" },
		{ "ToolTip", "This Blueprintable object can hold an OCIO Scene View Extension.\nYou can change its OCIO config, and specify the context in which you want it to be active on." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOpenColorIODisplayExtensionWrapper>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::ClassParams = {
		&UOpenColorIODisplayExtensionWrapper::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOpenColorIODisplayExtensionWrapper, 3487257311);
	template<> OPENCOLORIO_API UClass* StaticClass<UOpenColorIODisplayExtensionWrapper>()
	{
		return UOpenColorIODisplayExtensionWrapper::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOpenColorIODisplayExtensionWrapper(Z_Construct_UClass_UOpenColorIODisplayExtensionWrapper, &UOpenColorIODisplayExtensionWrapper::StaticClass, TEXT("/Script/OpenColorIO"), TEXT("UOpenColorIODisplayExtensionWrapper"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOpenColorIODisplayExtensionWrapper);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
