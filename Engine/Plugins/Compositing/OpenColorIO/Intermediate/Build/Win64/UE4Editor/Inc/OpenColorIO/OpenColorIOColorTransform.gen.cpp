// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenColorIO/Public/OpenColorIOColorTransform.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenColorIOColorTransform() {}
// Cross Module References
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIOColorTransform_NoRegister();
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIOColorTransform();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_OpenColorIO();
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIOConfiguration_NoRegister();
// End Cross Module References
	void UOpenColorIOColorTransform::StaticRegisterNativesUOpenColorIOColorTransform()
	{
	}
	UClass* Z_Construct_UClass_UOpenColorIOColorTransform_NoRegister()
	{
		return UOpenColorIOColorTransform::StaticClass();
	}
	struct Z_Construct_UClass_UOpenColorIOColorTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigurationOwner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConfigurationOwner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceColorSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceColorSpace;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationColorSpace_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DestinationColorSpace;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOpenColorIOColorTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIO,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOColorTransform_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Object used to generate shader and LUTs from OCIO configuration file and contain required resource to make a color space transform.\n */" },
		{ "IncludePath", "OpenColorIOColorTransform.h" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorTransform.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Object used to generate shader and LUTs from OCIO configuration file and contain required resource to make a color space transform." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_ConfigurationOwner_MetaData[] = {
		{ "ModuleRelativePath", "Public/OpenColorIOColorTransform.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_ConfigurationOwner = { "ConfigurationOwner", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOpenColorIOColorTransform, ConfigurationOwner), Z_Construct_UClass_UOpenColorIOConfiguration_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_ConfigurationOwner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_ConfigurationOwner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_SourceColorSpace_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorTransform.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_SourceColorSpace = { "SourceColorSpace", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOpenColorIOColorTransform, SourceColorSpace), METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_SourceColorSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_SourceColorSpace_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_DestinationColorSpace_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "ModuleRelativePath", "Public/OpenColorIOColorTransform.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_DestinationColorSpace = { "DestinationColorSpace", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOpenColorIOColorTransform, DestinationColorSpace), METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_DestinationColorSpace_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_DestinationColorSpace_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOpenColorIOColorTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_ConfigurationOwner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_SourceColorSpace,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOColorTransform_Statics::NewProp_DestinationColorSpace,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOpenColorIOColorTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOpenColorIOColorTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOpenColorIOColorTransform_Statics::ClassParams = {
		&UOpenColorIOColorTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOpenColorIOColorTransform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOColorTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOpenColorIOColorTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOpenColorIOColorTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOpenColorIOColorTransform, 2530467614);
	template<> OPENCOLORIO_API UClass* StaticClass<UOpenColorIOColorTransform>()
	{
		return UOpenColorIOColorTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOpenColorIOColorTransform(Z_Construct_UClass_UOpenColorIOColorTransform, &UOpenColorIOColorTransform::StaticClass, TEXT("/Script/OpenColorIO"), TEXT("UOpenColorIOColorTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOpenColorIOColorTransform);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UOpenColorIOColorTransform)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
