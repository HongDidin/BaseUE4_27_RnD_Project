// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OpenColorIO/Public/OpenColorIOConfiguration.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOpenColorIOConfiguration() {}
// Cross Module References
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIOConfiguration_NoRegister();
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIOConfiguration();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_OpenColorIO();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilePath();
	OPENCOLORIO_API UScriptStruct* Z_Construct_UScriptStruct_FOpenColorIOColorSpace();
	OPENCOLORIO_API UClass* Z_Construct_UClass_UOpenColorIOColorTransform_NoRegister();
// End Cross Module References
	void UOpenColorIOConfiguration::StaticRegisterNativesUOpenColorIOConfiguration()
	{
	}
	UClass* Z_Construct_UClass_UOpenColorIOConfiguration_NoRegister()
	{
		return UOpenColorIOConfiguration::StaticClass();
	}
	struct Z_Construct_UClass_UOpenColorIOConfiguration_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConfigurationFile_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConfigurationFile;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DesiredColorSpaces_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesiredColorSpaces_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DesiredColorSpaces;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ColorTransforms_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorTransforms_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ColorTransforms;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOpenColorIOConfiguration_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OpenColorIO,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOConfiguration_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Asset to manage whitelisted OpenColorIO color spaces. This will create required transform objects.\n */" },
		{ "IncludePath", "OpenColorIOConfiguration.h" },
		{ "ModuleRelativePath", "Public/OpenColorIOConfiguration.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Asset to manage whitelisted OpenColorIO color spaces. This will create required transform objects." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ConfigurationFile_MetaData[] = {
		{ "Category", "Config" },
		{ "FilePathFilter", "ocio" },
		{ "ModuleRelativePath", "Public/OpenColorIOConfiguration.h" },
		{ "RelativeToGameDir", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ConfigurationFile = { "ConfigurationFile", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOpenColorIOConfiguration, ConfigurationFile), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ConfigurationFile_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ConfigurationFile_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_DesiredColorSpaces_Inner = { "DesiredColorSpaces", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOpenColorIOColorSpace, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_DesiredColorSpaces_MetaData[] = {
		{ "Category", "ColorSpace" },
		{ "ModuleRelativePath", "Public/OpenColorIOConfiguration.h" },
		{ "OCIOConfigFile", "ConfigurationFile" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_DesiredColorSpaces = { "DesiredColorSpaces", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOpenColorIOConfiguration, DesiredColorSpaces), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_DesiredColorSpaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_DesiredColorSpaces_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ColorTransforms_Inner = { "ColorTransforms", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UOpenColorIOColorTransform_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ColorTransforms_MetaData[] = {
		{ "ModuleRelativePath", "Public/OpenColorIOConfiguration.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ColorTransforms = { "ColorTransforms", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOpenColorIOConfiguration, ColorTransforms), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ColorTransforms_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ColorTransforms_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOpenColorIOConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ConfigurationFile,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_DesiredColorSpaces_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_DesiredColorSpaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ColorTransforms_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOpenColorIOConfiguration_Statics::NewProp_ColorTransforms,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOpenColorIOConfiguration_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOpenColorIOConfiguration>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOpenColorIOConfiguration_Statics::ClassParams = {
		&UOpenColorIOConfiguration::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOpenColorIOConfiguration_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOpenColorIOConfiguration_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOpenColorIOConfiguration()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOpenColorIOConfiguration_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOpenColorIOConfiguration, 266451913);
	template<> OPENCOLORIO_API UClass* StaticClass<UOpenColorIOConfiguration>()
	{
		return UOpenColorIOConfiguration::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOpenColorIOConfiguration(Z_Construct_UClass_UOpenColorIOConfiguration, &UOpenColorIOConfiguration::StaticClass, TEXT("/Script/OpenColorIO"), TEXT("UOpenColorIOConfiguration"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOpenColorIOConfiguration);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
