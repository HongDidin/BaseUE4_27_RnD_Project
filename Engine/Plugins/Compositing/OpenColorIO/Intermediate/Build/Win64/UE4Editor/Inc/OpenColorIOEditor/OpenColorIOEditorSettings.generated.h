// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCOLORIOEDITOR_OpenColorIOEditorSettings_generated_h
#error "OpenColorIOEditorSettings.generated.h already included, missing '#pragma once' in OpenColorIOEditorSettings.h"
#endif
#define OPENCOLORIOEDITOR_OpenColorIOEditorSettings_generated_h

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPerViewportDisplaySettingPair_Statics; \
	OPENCOLORIOEDITOR_API static class UScriptStruct* StaticStruct();


template<> OPENCOLORIOEDITOR_API UScriptStruct* StaticStruct<struct FPerViewportDisplaySettingPair>();

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_SPARSE_DATA
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_RPC_WRAPPERS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOpenColorIOLevelViewportSettings(); \
	friend struct Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOLevelViewportSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenColorIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOLevelViewportSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUOpenColorIOLevelViewportSettings(); \
	friend struct Z_Construct_UClass_UOpenColorIOLevelViewportSettings_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOLevelViewportSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OpenColorIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOLevelViewportSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOpenColorIOLevelViewportSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOLevelViewportSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOLevelViewportSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOLevelViewportSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOLevelViewportSettings(UOpenColorIOLevelViewportSettings&&); \
	NO_API UOpenColorIOLevelViewportSettings(const UOpenColorIOLevelViewportSettings&); \
public:


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOpenColorIOLevelViewportSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOLevelViewportSettings(UOpenColorIOLevelViewportSettings&&); \
	NO_API UOpenColorIOLevelViewportSettings(const UOpenColorIOLevelViewportSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOLevelViewportSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOLevelViewportSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOLevelViewportSettings)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ViewportsSettings() { return STRUCT_OFFSET(UOpenColorIOLevelViewportSettings, ViewportsSettings); }


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_35_PROLOG
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_RPC_WRAPPERS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_INCLASS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCOLORIOEDITOR_API UClass* StaticClass<class UOpenColorIOLevelViewportSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_OpenColorIOEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
