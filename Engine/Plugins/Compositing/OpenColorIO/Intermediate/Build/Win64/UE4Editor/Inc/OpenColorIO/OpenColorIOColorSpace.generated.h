// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCOLORIO_OpenColorIOColorSpace_generated_h
#error "OpenColorIOColorSpace.generated.h already included, missing '#pragma once' in OpenColorIOColorSpace.h"
#endif
#define OPENCOLORIO_OpenColorIOColorSpace_generated_h

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorSpace_h_114_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOpenColorIODisplayConfiguration_Statics; \
	static class UScriptStruct* StaticStruct();


template<> OPENCOLORIO_API UScriptStruct* StaticStruct<struct FOpenColorIODisplayConfiguration>();

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorSpace_h_75_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOpenColorIOColorConversionSettings_Statics; \
	static class UScriptStruct* StaticStruct();


template<> OPENCOLORIO_API UScriptStruct* StaticStruct<struct FOpenColorIOColorConversionSettings>();

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorSpace_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOpenColorIOColorSpace_Statics; \
	static class UScriptStruct* StaticStruct();


template<> OPENCOLORIO_API UScriptStruct* StaticStruct<struct FOpenColorIOColorSpace>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIO_Public_OpenColorIOColorSpace_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
