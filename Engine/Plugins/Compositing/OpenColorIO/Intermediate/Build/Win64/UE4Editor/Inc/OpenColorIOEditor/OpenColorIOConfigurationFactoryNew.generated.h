// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef OPENCOLORIOEDITOR_OpenColorIOConfigurationFactoryNew_generated_h
#error "OpenColorIOConfigurationFactoryNew.generated.h already included, missing '#pragma once' in OpenColorIOConfigurationFactoryNew.h"
#endif
#define OPENCOLORIOEDITOR_OpenColorIOConfigurationFactoryNew_generated_h

#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_SPARSE_DATA
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_RPC_WRAPPERS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOpenColorIOConfigurationFactoryNew(); \
	friend struct Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOConfigurationFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenColorIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOConfigurationFactoryNew)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUOpenColorIOConfigurationFactoryNew(); \
	friend struct Z_Construct_UClass_UOpenColorIOConfigurationFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UOpenColorIOConfigurationFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/OpenColorIOEditor"), NO_API) \
	DECLARE_SERIALIZER(UOpenColorIOConfigurationFactoryNew)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOpenColorIOConfigurationFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOConfigurationFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOConfigurationFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOConfigurationFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOConfigurationFactoryNew(UOpenColorIOConfigurationFactoryNew&&); \
	NO_API UOpenColorIOConfigurationFactoryNew(const UOpenColorIOConfigurationFactoryNew&); \
public:


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOpenColorIOConfigurationFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOpenColorIOConfigurationFactoryNew(UOpenColorIOConfigurationFactoryNew&&); \
	NO_API UOpenColorIOConfigurationFactoryNew(const UOpenColorIOConfigurationFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOpenColorIOConfigurationFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOpenColorIOConfigurationFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOpenColorIOConfigurationFactoryNew)


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_13_PROLOG
#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_RPC_WRAPPERS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_INCLASS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_SPARSE_DATA \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OpenColorIOConfigurationFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> OPENCOLORIOEDITOR_API UClass* StaticClass<class UOpenColorIOConfigurationFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Compositing_OpenColorIO_Source_OpenColorIOEditor_Private_Factories_OpenColorIOConfigurationFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
