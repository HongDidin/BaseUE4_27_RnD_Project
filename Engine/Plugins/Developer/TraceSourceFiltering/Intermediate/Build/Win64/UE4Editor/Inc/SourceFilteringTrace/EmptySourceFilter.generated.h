// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOURCEFILTERINGTRACE_EmptySourceFilter_generated_h
#error "EmptySourceFilter.generated.h already included, missing '#pragma once' in EmptySourceFilter.h"
#endif
#define SOURCEFILTERINGTRACE_EmptySourceFilter_generated_h

#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_SPARSE_DATA
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_RPC_WRAPPERS
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEmptySourceFilter(); \
	friend struct Z_Construct_UClass_UEmptySourceFilter_Statics; \
public: \
	DECLARE_CLASS(UEmptySourceFilter, UDataSourceFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SourceFilteringTrace"), NO_API) \
	DECLARE_SERIALIZER(UEmptySourceFilter)


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUEmptySourceFilter(); \
	friend struct Z_Construct_UClass_UEmptySourceFilter_Statics; \
public: \
	DECLARE_CLASS(UEmptySourceFilter, UDataSourceFilter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SourceFilteringTrace"), NO_API) \
	DECLARE_SERIALIZER(UEmptySourceFilter)


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEmptySourceFilter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEmptySourceFilter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEmptySourceFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEmptySourceFilter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEmptySourceFilter(UEmptySourceFilter&&); \
	NO_API UEmptySourceFilter(const UEmptySourceFilter&); \
public:


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEmptySourceFilter() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEmptySourceFilter(UEmptySourceFilter&&); \
	NO_API UEmptySourceFilter(const UEmptySourceFilter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEmptySourceFilter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEmptySourceFilter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UEmptySourceFilter)


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_10_PROLOG
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_SPARSE_DATA \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_RPC_WRAPPERS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_INCLASS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_SPARSE_DATA \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOURCEFILTERINGTRACE_API UClass* StaticClass<class UEmptySourceFilter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringTrace_Public_EmptySourceFilter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
