// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringCore/Public/DataSourceFiltering.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataSourceFiltering() {}
// Cross Module References
	SOURCEFILTERINGCORE_API UEnum* Z_Construct_UEnum_SourceFilteringCore_EFilterSetMode();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringCore();
	SOURCEFILTERINGCORE_API UScriptStruct* Z_Construct_UScriptStruct_FActorClassFilter();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
	static UEnum* EFilterSetMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SourceFilteringCore_EFilterSetMode, Z_Construct_UPackage__Script_SourceFilteringCore(), TEXT("EFilterSetMode"));
		}
		return Singleton;
	}
	template<> SOURCEFILTERINGCORE_API UEnum* StaticEnum<EFilterSetMode>()
	{
		return EFilterSetMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFilterSetMode(EFilterSetMode_StaticEnum, TEXT("/Script/SourceFilteringCore"), TEXT("EFilterSetMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SourceFilteringCore_EFilterSetMode_Hash() { return 2569007726U; }
	UEnum* Z_Construct_UEnum_SourceFilteringCore_EFilterSetMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SourceFilteringCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFilterSetMode"), 0, Get_Z_Construct_UEnum_SourceFilteringCore_EFilterSetMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFilterSetMode::AND", (int64)EFilterSetMode::AND },
				{ "EFilterSetMode::OR", (int64)EFilterSetMode::OR },
				{ "EFilterSetMode::NOT", (int64)EFilterSetMode::NOT },
				{ "EFilterSetMode::Count", (int64)EFilterSetMode::Count },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AND.Name", "EFilterSetMode::AND" },
				{ "Comment", "/** Enum representing the possible Filter Set operations */" },
				{ "Count.Hidden", "" },
				{ "Count.Name", "EFilterSetMode::Count" },
				{ "ModuleRelativePath", "Public/DataSourceFiltering.h" },
				{ "NOT.Name", "EFilterSetMode::NOT" },
				{ "OR.Name", "EFilterSetMode::OR" },
				{ "ToolTip", "Enum representing the possible Filter Set operations" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SourceFilteringCore,
				nullptr,
				"EFilterSetMode",
				"EFilterSetMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FActorClassFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SOURCEFILTERINGCORE_API uint32 Get_Z_Construct_UScriptStruct_FActorClassFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FActorClassFilter, Z_Construct_UPackage__Script_SourceFilteringCore(), TEXT("ActorClassFilter"), sizeof(FActorClassFilter), Get_Z_Construct_UScriptStruct_FActorClassFilter_Hash());
	}
	return Singleton;
}
template<> SOURCEFILTERINGCORE_API UScriptStruct* StaticStruct<FActorClassFilter>()
{
	return FActorClassFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FActorClassFilter(FActorClassFilter::StaticStruct, TEXT("/Script/SourceFilteringCore"), TEXT("ActorClassFilter"), false, nullptr, nullptr);
static struct FScriptStruct_SourceFilteringCore_StaticRegisterNativesFActorClassFilter
{
	FScriptStruct_SourceFilteringCore_StaticRegisterNativesFActorClassFilter()
	{
		UScriptStruct::DeferCppStructOps<FActorClassFilter>(FName(TEXT("ActorClassFilter")));
	}
} ScriptStruct_SourceFilteringCore_StaticRegisterNativesFActorClassFilter;
	struct Z_Construct_UScriptStruct_FActorClassFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeDerivedClasses_MetaData[];
#endif
		static void NewProp_bIncludeDerivedClasses_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeDerivedClasses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorClassFilter_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** High-level filter structure used when filtering AActor instances to apply user filters to inside of a UWorld */" },
		{ "ModuleRelativePath", "Public/DataSourceFiltering.h" },
		{ "ToolTip", "High-level filter structure used when filtering AActor instances to apply user filters to inside of a UWorld" },
	};
#endif
	void* Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FActorClassFilter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_ActorClass_MetaData[] = {
		{ "Category", "TraceSourceFiltering" },
		{ "Comment", "/** Target actor class used when applying this filter */" },
		{ "ModuleRelativePath", "Public/DataSourceFiltering.h" },
		{ "ToolTip", "Target actor class used when applying this filter" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_ActorClass = { "ActorClass", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorClassFilter, ActorClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_ActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_ActorClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_bIncludeDerivedClasses_MetaData[] = {
		{ "Category", "TraceSourceFiltering" },
		{ "Comment", "/** Flag as to whether or not any derived classes from ActorClass should also be considered when filtering */" },
		{ "ModuleRelativePath", "Public/DataSourceFiltering.h" },
		{ "ToolTip", "Flag as to whether or not any derived classes from ActorClass should also be considered when filtering" },
	};
#endif
	void Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_bIncludeDerivedClasses_SetBit(void* Obj)
	{
		((FActorClassFilter*)Obj)->bIncludeDerivedClasses = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_bIncludeDerivedClasses = { "bIncludeDerivedClasses", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FActorClassFilter), &Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_bIncludeDerivedClasses_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_bIncludeDerivedClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_bIncludeDerivedClasses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FActorClassFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_ActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorClassFilter_Statics::NewProp_bIncludeDerivedClasses,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FActorClassFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringCore,
		nullptr,
		&NewStructOps,
		"ActorClassFilter",
		sizeof(FActorClassFilter),
		alignof(FActorClassFilter),
		Z_Construct_UScriptStruct_FActorClassFilter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorClassFilter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FActorClassFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorClassFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FActorClassFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FActorClassFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SourceFilteringCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ActorClassFilter"), sizeof(FActorClassFilter), Get_Z_Construct_UScriptStruct_FActorClassFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FActorClassFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FActorClassFilter_Hash() { return 2631348335U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
