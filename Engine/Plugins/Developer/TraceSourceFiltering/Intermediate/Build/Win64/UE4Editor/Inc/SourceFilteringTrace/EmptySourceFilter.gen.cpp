// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringTrace/Public/EmptySourceFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEmptySourceFilter() {}
// Cross Module References
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UEmptySourceFilter_NoRegister();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UEmptySourceFilter();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilter();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringTrace();
// End Cross Module References
	void UEmptySourceFilter::StaticRegisterNativesUEmptySourceFilter()
	{
	}
	UClass* Z_Construct_UClass_UEmptySourceFilter_NoRegister()
	{
		return UEmptySourceFilter::StaticClass();
	}
	struct Z_Construct_UClass_UEmptySourceFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MissingClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MissingClassName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEmptySourceFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataSourceFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringTrace,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEmptySourceFilter_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Source filter implementation used to replace filter instance who's UClass is not loaded and or does not exist, primarily used by Filter Preset loading */" },
		{ "IncludePath", "EmptySourceFilter.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/EmptySourceFilter.h" },
		{ "ToolTip", "Source filter implementation used to replace filter instance who's UClass is not loaded and or does not exist, primarily used by Filter Preset loading" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEmptySourceFilter_Statics::NewProp_MissingClassName_MetaData[] = {
		{ "ModuleRelativePath", "Public/EmptySourceFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UEmptySourceFilter_Statics::NewProp_MissingClassName = { "MissingClassName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEmptySourceFilter, MissingClassName), METADATA_PARAMS(Z_Construct_UClass_UEmptySourceFilter_Statics::NewProp_MissingClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEmptySourceFilter_Statics::NewProp_MissingClassName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEmptySourceFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEmptySourceFilter_Statics::NewProp_MissingClassName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEmptySourceFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEmptySourceFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEmptySourceFilter_Statics::ClassParams = {
		&UEmptySourceFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEmptySourceFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEmptySourceFilter_Statics::PropPointers),
		0,
		0x041000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEmptySourceFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEmptySourceFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEmptySourceFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEmptySourceFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEmptySourceFilter, 3265304788);
	template<> SOURCEFILTERINGTRACE_API UClass* StaticClass<UEmptySourceFilter>()
	{
		return UEmptySourceFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEmptySourceFilter(Z_Construct_UClass_UEmptySourceFilter, &UEmptySourceFilter::StaticClass, TEXT("/Script/SourceFilteringTrace"), TEXT("UEmptySourceFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEmptySourceFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
