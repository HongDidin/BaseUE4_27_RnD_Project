// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOURCEFILTERINGCORE_TraceSourceFilteringSettings_generated_h
#error "TraceSourceFilteringSettings.generated.h already included, missing '#pragma once' in TraceSourceFilteringSettings.h"
#endif
#define SOURCEFILTERINGCORE_TraceSourceFilteringSettings_generated_h

#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_SPARSE_DATA
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_RPC_WRAPPERS
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTraceSourceFilteringSettings(); \
	friend struct Z_Construct_UClass_UTraceSourceFilteringSettings_Statics; \
public: \
	DECLARE_CLASS(UTraceSourceFilteringSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SourceFilteringCore"), NO_API) \
	DECLARE_SERIALIZER(UTraceSourceFilteringSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceSourceFilters");} \



#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUTraceSourceFilteringSettings(); \
	friend struct Z_Construct_UClass_UTraceSourceFilteringSettings_Statics; \
public: \
	DECLARE_CLASS(UTraceSourceFilteringSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SourceFilteringCore"), NO_API) \
	DECLARE_SERIALIZER(UTraceSourceFilteringSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceSourceFilters");} \



#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTraceSourceFilteringSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTraceSourceFilteringSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTraceSourceFilteringSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTraceSourceFilteringSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTraceSourceFilteringSettings(UTraceSourceFilteringSettings&&); \
	NO_API UTraceSourceFilteringSettings(const UTraceSourceFilteringSettings&); \
public:


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTraceSourceFilteringSettings(UTraceSourceFilteringSettings&&); \
	NO_API UTraceSourceFilteringSettings(const UTraceSourceFilteringSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTraceSourceFilteringSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTraceSourceFilteringSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTraceSourceFilteringSettings)


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_10_PROLOG
#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_SPARSE_DATA \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_RPC_WRAPPERS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_INCLASS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_SPARSE_DATA \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOURCEFILTERINGCORE_API UClass* StaticClass<class UTraceSourceFilteringSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_TraceSourceFiltering_Source_SourceFilteringCore_Public_TraceSourceFilteringSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
