// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringCore/Public/IDataSourceFilterInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIDataSourceFilterInterface() {}
// Cross Module References
	SOURCEFILTERINGCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataSourceFilterConfiguration();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringCore();
	SOURCEFILTERINGCORE_API UClass* Z_Construct_UClass_UDataSourceFilterInterface_NoRegister();
	SOURCEFILTERINGCORE_API UClass* Z_Construct_UClass_UDataSourceFilterInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
// End Cross Module References
class UScriptStruct* FDataSourceFilterConfiguration::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SOURCEFILTERINGCORE_API uint32 Get_Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration, Z_Construct_UPackage__Script_SourceFilteringCore(), TEXT("DataSourceFilterConfiguration"), sizeof(FDataSourceFilterConfiguration), Get_Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Hash());
	}
	return Singleton;
}
template<> SOURCEFILTERINGCORE_API UScriptStruct* StaticStruct<FDataSourceFilterConfiguration>()
{
	return FDataSourceFilterConfiguration::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDataSourceFilterConfiguration(FDataSourceFilterConfiguration::StaticStruct, TEXT("/Script/SourceFilteringCore"), TEXT("DataSourceFilterConfiguration"), false, nullptr, nullptr);
static struct FScriptStruct_SourceFilteringCore_StaticRegisterNativesFDataSourceFilterConfiguration
{
	FScriptStruct_SourceFilteringCore_StaticRegisterNativesFDataSourceFilterConfiguration()
	{
		UScriptStruct::DeferCppStructOps<FDataSourceFilterConfiguration>(FName(TEXT("DataSourceFilterConfiguration")));
	}
} ScriptStruct_SourceFilteringCore_StaticRegisterNativesFDataSourceFilterConfiguration;
	struct Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyApplyDuringActorSpawn_MetaData[];
#endif
		static void NewProp_bOnlyApplyDuringActorSpawn_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyApplyDuringActorSpawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCanRunAsynchronously_MetaData[];
#endif
		static void NewProp_bCanRunAsynchronously_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanRunAsynchronously;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterApplyingTickInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FilterApplyingTickInterval;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterInterface.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDataSourceFilterConfiguration>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bOnlyApplyDuringActorSpawn_MetaData[] = {
		{ "Category", "Filtering" },
		{ "Comment", "/** Flag whether or not this filter should only applied once, whenever an actor is spawned */" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterInterface.h" },
		{ "ToolTip", "Flag whether or not this filter should only applied once, whenever an actor is spawned" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bOnlyApplyDuringActorSpawn_SetBit(void* Obj)
	{
		((FDataSourceFilterConfiguration*)Obj)->bOnlyApplyDuringActorSpawn = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bOnlyApplyDuringActorSpawn = { "bOnlyApplyDuringActorSpawn", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FDataSourceFilterConfiguration), &Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bOnlyApplyDuringActorSpawn_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bOnlyApplyDuringActorSpawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bOnlyApplyDuringActorSpawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bCanRunAsynchronously_MetaData[] = {
		{ "Category", "Filtering" },
		{ "Comment", "/** Flag whether or not this filter does not rely on gamethread only data, and therefore can work on a differen thread */" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterInterface.h" },
		{ "ToolTip", "Flag whether or not this filter does not rely on gamethread only data, and therefore can work on a differen thread" },
	};
#endif
	void Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bCanRunAsynchronously_SetBit(void* Obj)
	{
		((FDataSourceFilterConfiguration*)Obj)->bCanRunAsynchronously = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bCanRunAsynchronously = { "bCanRunAsynchronously", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FDataSourceFilterConfiguration), &Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bCanRunAsynchronously_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bCanRunAsynchronously_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bCanRunAsynchronously_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_FilterApplyingTickInterval_MetaData[] = {
		{ "Category", "Filtering" },
		{ "ClampMax", "255" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Interval, in frames, between applying the filter. The resulting value is cached for intermediate frames. */" },
		{ "EditCondition", "!bOnlyApplyDuringActorSpawn" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterInterface.h" },
		{ "ToolTip", "Interval, in frames, between applying the filter. The resulting value is cached for intermediate frames." },
		{ "UIMax", "128" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_FilterApplyingTickInterval = { "FilterApplyingTickInterval", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDataSourceFilterConfiguration, FilterApplyingTickInterval), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_FilterApplyingTickInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_FilterApplyingTickInterval_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bOnlyApplyDuringActorSpawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_bCanRunAsynchronously,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::NewProp_FilterApplyingTickInterval,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringCore,
		nullptr,
		&NewStructOps,
		"DataSourceFilterConfiguration",
		sizeof(FDataSourceFilterConfiguration),
		alignof(FDataSourceFilterConfiguration),
		Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDataSourceFilterConfiguration()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SourceFilteringCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DataSourceFilterConfiguration"), sizeof(FDataSourceFilterConfiguration), Get_Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDataSourceFilterConfiguration_Hash() { return 2547801957U; }
	DEFINE_FUNCTION(IDataSourceFilterInterface::execGetToolTipText)
	{
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutDisplayText);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetToolTipText_Implementation(Z_Param_Out_OutDisplayText);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IDataSourceFilterInterface::execGetDisplayText)
	{
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutDisplayText);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetDisplayText_Implementation(Z_Param_Out_OutDisplayText);
		P_NATIVE_END;
	}
	void IDataSourceFilterInterface::GetDisplayText(FText& OutDisplayText) const
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_GetDisplayText instead.");
	}
	void IDataSourceFilterInterface::GetToolTipText(FText& OutDisplayText) const
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_GetToolTipText instead.");
	}
	void UDataSourceFilterInterface::StaticRegisterNativesUDataSourceFilterInterface()
	{
		UClass* Class = UDataSourceFilterInterface::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDisplayText", &IDataSourceFilterInterface::execGetDisplayText },
			{ "GetToolTipText", &IDataSourceFilterInterface::execGetToolTipText },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutDisplayText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::NewProp_OutDisplayText = { "OutDisplayText", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataSourceFilterInterface_eventGetDisplayText_Parms, OutDisplayText), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::NewProp_OutDisplayText,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::Function_MetaDataParams[] = {
		{ "Category", "TraceSourceFiltering" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterInterface.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataSourceFilterInterface, nullptr, "GetDisplayText", nullptr, nullptr, sizeof(DataSourceFilterInterface_eventGetDisplayText_Parms), Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics
	{
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutDisplayText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::NewProp_OutDisplayText = { "OutDisplayText", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataSourceFilterInterface_eventGetToolTipText_Parms, OutDisplayText), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::NewProp_OutDisplayText,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::Function_MetaDataParams[] = {
		{ "Category", "TraceSourceFiltering" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterInterface.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataSourceFilterInterface, nullptr, "GetToolTipText", nullptr, nullptr, sizeof(DataSourceFilterInterface_eventGetToolTipText_Parms), Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDataSourceFilterInterface_NoRegister()
	{
		return UDataSourceFilterInterface::StaticClass();
	}
	struct Z_Construct_UClass_UDataSourceFilterInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataSourceFilterInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDataSourceFilterInterface_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDataSourceFilterInterface_GetDisplayText, "GetDisplayText" }, // 3909042990
		{ &Z_Construct_UFunction_UDataSourceFilterInterface_GetToolTipText, "GetToolTipText" }, // 2125365859
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataSourceFilterInterface_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataSourceFilterInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IDataSourceFilterInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataSourceFilterInterface_Statics::ClassParams = {
		&UDataSourceFilterInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataSourceFilterInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilterInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataSourceFilterInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataSourceFilterInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataSourceFilterInterface, 1801208483);
	template<> SOURCEFILTERINGCORE_API UClass* StaticClass<UDataSourceFilterInterface>()
	{
		return UDataSourceFilterInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataSourceFilterInterface(Z_Construct_UClass_UDataSourceFilterInterface, &UDataSourceFilterInterface::StaticClass, TEXT("/Script/SourceFilteringCore"), TEXT("UDataSourceFilterInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataSourceFilterInterface);
	static FName NAME_UDataSourceFilterInterface_GetDisplayText = FName(TEXT("GetDisplayText"));
	void IDataSourceFilterInterface::Execute_GetDisplayText(const UObject* O, FText& OutDisplayText)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UDataSourceFilterInterface::StaticClass()));
		DataSourceFilterInterface_eventGetDisplayText_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UDataSourceFilterInterface_GetDisplayText);
		if (Func)
		{
			Parms.OutDisplayText=OutDisplayText;
			const_cast<UObject*>(O)->ProcessEvent(Func, &Parms);
			OutDisplayText=Parms.OutDisplayText;
		}
		else if (auto I = (const IDataSourceFilterInterface*)(O->GetNativeInterfaceAddress(UDataSourceFilterInterface::StaticClass())))
		{
			I->GetDisplayText_Implementation(OutDisplayText);
		}
	}
	static FName NAME_UDataSourceFilterInterface_GetToolTipText = FName(TEXT("GetToolTipText"));
	void IDataSourceFilterInterface::Execute_GetToolTipText(const UObject* O, FText& OutDisplayText)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UDataSourceFilterInterface::StaticClass()));
		DataSourceFilterInterface_eventGetToolTipText_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UDataSourceFilterInterface_GetToolTipText);
		if (Func)
		{
			Parms.OutDisplayText=OutDisplayText;
			const_cast<UObject*>(O)->ProcessEvent(Func, &Parms);
			OutDisplayText=Parms.OutDisplayText;
		}
		else if (auto I = (const IDataSourceFilterInterface*)(O->GetNativeInterfaceAddress(UDataSourceFilterInterface::StaticClass())))
		{
			I->GetToolTipText_Implementation(OutDisplayText);
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
