// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringTrace/Public/DataSourceFilterSet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataSourceFilterSet() {}
// Cross Module References
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilterSet_NoRegister();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilterSet();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilter();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringTrace();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilter_NoRegister();
	SOURCEFILTERINGCORE_API UEnum* Z_Construct_UEnum_SourceFilteringCore_EFilterSetMode();
	SOURCEFILTERINGCORE_API UClass* Z_Construct_UClass_UDataSourceFilterSetInterface_NoRegister();
// End Cross Module References
	void UDataSourceFilterSet::StaticRegisterNativesUDataSourceFilterSet()
	{
	}
	UClass* Z_Construct_UClass_UDataSourceFilterSet_NoRegister()
	{
		return UDataSourceFilterSet::StaticClass();
	}
	struct Z_Construct_UClass_UDataSourceFilterSet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Filters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Filters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Filters;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Mode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Mode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataSourceFilterSet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataSourceFilter,
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringTrace,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataSourceFilterSet_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Engine implementation of IDataSourceFilterSetInterface */" },
		{ "IncludePath", "DataSourceFilterSet.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/DataSourceFilterSet.h" },
		{ "ToolTip", "Engine implementation of IDataSourceFilterSetInterface" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Filters_Inner = { "Filters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataSourceFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Filters_MetaData[] = {
		{ "Comment", "/** Contained Filter instance */" },
		{ "ModuleRelativePath", "Public/DataSourceFilterSet.h" },
		{ "ToolTip", "Contained Filter instance" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Filters = { "Filters", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataSourceFilterSet, Filters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Filters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Filters_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Mode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Mode_MetaData[] = {
		{ "Comment", "/** Current Filter set operation */" },
		{ "ModuleRelativePath", "Public/DataSourceFilterSet.h" },
		{ "ToolTip", "Current Filter set operation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Mode = { "Mode", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataSourceFilterSet, Mode), Z_Construct_UEnum_SourceFilteringCore_EFilterSetMode, METADATA_PARAMS(Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Mode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Mode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataSourceFilterSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Filters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Filters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Mode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataSourceFilterSet_Statics::NewProp_Mode,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UDataSourceFilterSet_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UDataSourceFilterSetInterface_NoRegister, (int32)VTABLE_OFFSET(UDataSourceFilterSet, IDataSourceFilterSetInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataSourceFilterSet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataSourceFilterSet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataSourceFilterSet_Statics::ClassParams = {
		&UDataSourceFilterSet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataSourceFilterSet_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilterSet_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataSourceFilterSet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilterSet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataSourceFilterSet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataSourceFilterSet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataSourceFilterSet, 436636556);
	template<> SOURCEFILTERINGTRACE_API UClass* StaticClass<UDataSourceFilterSet>()
	{
		return UDataSourceFilterSet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataSourceFilterSet(Z_Construct_UClass_UDataSourceFilterSet, &UDataSourceFilterSet::StaticClass, TEXT("/Script/SourceFilteringTrace"), TEXT("UDataSourceFilterSet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataSourceFilterSet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
