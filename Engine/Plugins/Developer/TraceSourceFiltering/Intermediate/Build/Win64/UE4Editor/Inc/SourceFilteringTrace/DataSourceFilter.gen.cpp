// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringTrace/Public/DataSourceFilter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataSourceFilter() {}
// Cross Module References
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilter_NoRegister();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilter();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringTrace();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	SOURCEFILTERINGCORE_API UScriptStruct* Z_Construct_UScriptStruct_FDataSourceFilterConfiguration();
	SOURCEFILTERINGCORE_API UClass* Z_Construct_UClass_UDataSourceFilterInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UDataSourceFilter::execDoesActorPassFilter)
	{
		P_GET_OBJECT(AActor,Z_Param_InActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->DoesActorPassFilter_Implementation(Z_Param_InActor);
		P_NATIVE_END;
	}
	static FName NAME_UDataSourceFilter_DoesActorPassFilter = FName(TEXT("DoesActorPassFilter"));
	bool UDataSourceFilter::DoesActorPassFilter(const AActor* InActor) const
	{
		DataSourceFilter_eventDoesActorPassFilter_Parms Parms;
		Parms.InActor=InActor;
		const_cast<UDataSourceFilter*>(this)->ProcessEvent(FindFunctionChecked(NAME_UDataSourceFilter_DoesActorPassFilter),&Parms);
		return !!Parms.ReturnValue;
	}
	void UDataSourceFilter::StaticRegisterNativesUDataSourceFilter()
	{
		UClass* Class = UDataSourceFilter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "DoesActorPassFilter", &UDataSourceFilter::execDoesActorPassFilter },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InActor;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_InActor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_InActor = { "InActor", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(DataSourceFilter_eventDoesActorPassFilter_Parms, InActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_InActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_InActor_MetaData)) };
	void Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((DataSourceFilter_eventDoesActorPassFilter_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(DataSourceFilter_eventDoesActorPassFilter_Parms), &Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_InActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::Function_MetaDataParams[] = {
		{ "Category", "TraceSourceFiltering" },
		{ "ModuleRelativePath", "Public/DataSourceFilter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UDataSourceFilter, nullptr, "DoesActorPassFilter", nullptr, nullptr, sizeof(DataSourceFilter_eventDoesActorPassFilter_Parms), Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UDataSourceFilter_NoRegister()
	{
		return UDataSourceFilter::StaticClass();
	}
	struct Z_Construct_UClass_UDataSourceFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Configuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Configuration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataSourceFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringTrace,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UDataSourceFilter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UDataSourceFilter_DoesActorPassFilter, "DoesActorPassFilter" }, // 440222394
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataSourceFilter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "DataSourceFilter.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/DataSourceFilter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataSourceFilter_Statics::NewProp_Configuration_MetaData[] = {
		{ "Category", "Filtering" },
		{ "Comment", "/** Filter specific settings */" },
		{ "ModuleRelativePath", "Public/DataSourceFilter.h" },
		{ "ToolTip", "Filter specific settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataSourceFilter_Statics::NewProp_Configuration = { "Configuration", nullptr, (EPropertyFlags)0x0020080000010015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataSourceFilter, Configuration), Z_Construct_UScriptStruct_FDataSourceFilterConfiguration, METADATA_PARAMS(Z_Construct_UClass_UDataSourceFilter_Statics::NewProp_Configuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilter_Statics::NewProp_Configuration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataSourceFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataSourceFilter_Statics::NewProp_Configuration,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UDataSourceFilter_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UDataSourceFilterInterface_NoRegister, (int32)VTABLE_OFFSET(UDataSourceFilter, IDataSourceFilterInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataSourceFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataSourceFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataSourceFilter_Statics::ClassParams = {
		&UDataSourceFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UDataSourceFilter_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilter_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataSourceFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataSourceFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataSourceFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataSourceFilter, 3478569995);
	template<> SOURCEFILTERINGTRACE_API UClass* StaticClass<UDataSourceFilter>()
	{
		return UDataSourceFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataSourceFilter(Z_Construct_UClass_UDataSourceFilter, &UDataSourceFilter::StaticClass, TEXT("/Script/SourceFilteringTrace"), TEXT("UDataSourceFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataSourceFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
