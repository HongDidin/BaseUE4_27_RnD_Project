// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringTrace/Public/TraceSourceFilteringProjectSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTraceSourceFilteringProjectSettings() {}
// Cross Module References
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UTraceSourceFilteringProjectSettings_NoRegister();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UTraceSourceFilteringProjectSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringTrace();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilter_NoRegister();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_USourceFilterCollection_NoRegister();
// End Cross Module References
	void UTraceSourceFilteringProjectSettings::StaticRegisterNativesUTraceSourceFilteringProjectSettings()
	{
	}
	UClass* Z_Construct_UClass_UTraceSourceFilteringProjectSettings_NoRegister()
	{
		return UTraceSourceFilteringProjectSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FSoftClassPropertyParams NewProp_CookedSourceFilterClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CookedSourceFilterClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CookedSourceFilterClasses;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultFilterPreset_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultFilterPreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringTrace,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Trace Source Filtering" },
		{ "IncludePath", "TraceSourceFilteringProjectSettings.h" },
		{ "ModuleRelativePath", "Public/TraceSourceFilteringProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftClassPropertyParams Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_CookedSourceFilterClasses_Inner = { "CookedSourceFilterClasses", nullptr, (EPropertyFlags)0x0004000000004000, UE4CodeGen_Private::EPropertyGenFlags::SoftClass, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataSourceFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_CookedSourceFilterClasses_MetaData[] = {
		{ "Category", "TraceSourceFiltering" },
		{ "DisplayName", "Source Filter Classes, which should be incorporated into the cook" },
		{ "ModuleRelativePath", "Public/TraceSourceFilteringProjectSettings.h" },
		{ "RelativeToGameContentDir", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_CookedSourceFilterClasses = { "CookedSourceFilterClasses", nullptr, (EPropertyFlags)0x0014040000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTraceSourceFilteringProjectSettings, CookedSourceFilterClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_CookedSourceFilterClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_CookedSourceFilterClasses_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_DefaultFilterPreset_MetaData[] = {
		{ "Category", "TraceSourceFiltering" },
		{ "DisplayName", "Default Filter preset, which should be loaded during boot" },
		{ "ModuleRelativePath", "Public/TraceSourceFilteringProjectSettings.h" },
		{ "RelativeToGameContentDir", "" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_DefaultFilterPreset = { "DefaultFilterPreset", nullptr, (EPropertyFlags)0x0014040000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTraceSourceFilteringProjectSettings, DefaultFilterPreset), Z_Construct_UClass_USourceFilterCollection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_DefaultFilterPreset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_DefaultFilterPreset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_CookedSourceFilterClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_CookedSourceFilterClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::NewProp_DefaultFilterPreset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTraceSourceFilteringProjectSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::ClassParams = {
		&UTraceSourceFilteringProjectSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTraceSourceFilteringProjectSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTraceSourceFilteringProjectSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTraceSourceFilteringProjectSettings, 1668522679);
	template<> SOURCEFILTERINGTRACE_API UClass* StaticClass<UTraceSourceFilteringProjectSettings>()
	{
		return UTraceSourceFilteringProjectSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTraceSourceFilteringProjectSettings(Z_Construct_UClass_UTraceSourceFilteringProjectSettings, &UTraceSourceFilteringProjectSettings::StaticClass, TEXT("/Script/SourceFilteringTrace"), TEXT("UTraceSourceFilteringProjectSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTraceSourceFilteringProjectSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
