// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringTrace/Public/SourceFilterCollection.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSourceFilterCollection() {}
// Cross Module References
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_USourceFilterCollection_NoRegister();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_USourceFilterCollection();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringTrace();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilter_NoRegister();
	SOURCEFILTERINGCORE_API UScriptStruct* Z_Construct_UScriptStruct_FActorClassFilter();
	SOURCEFILTERINGTRACE_API UClass* Z_Construct_UClass_UDataSourceFilterSet_NoRegister();
// End Cross Module References
	void USourceFilterCollection::StaticRegisterNativesUSourceFilterCollection()
	{
	}
	UClass* Z_Construct_UClass_USourceFilterCollection_NoRegister()
	{
		return USourceFilterCollection::StaticClass();
	}
	struct Z_Construct_UClass_USourceFilterCollection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Filters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Filters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Filters;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClassFilters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClassFilters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ClassFilters;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilterClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilterClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FilterClasses;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChildToParent_ValueProp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ChildToParent_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChildToParent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ChildToParent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USourceFilterCollection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringTrace,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USourceFilterCollection_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SourceFilterCollection.h" },
		{ "ModuleRelativePath", "Public/SourceFilterCollection.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_Filters_Inner = { "Filters", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataSourceFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_Filters_MetaData[] = {
		{ "Category", "Filtering" },
		{ "Comment", "/** Root-level filter instances */" },
		{ "ModuleRelativePath", "Public/SourceFilterCollection.h" },
		{ "ToolTip", "Root-level filter instances" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_Filters = { "Filters", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USourceFilterCollection, Filters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_Filters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_Filters_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ClassFilters_Inner = { "ClassFilters", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FActorClassFilter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ClassFilters_MetaData[] = {
		{ "Category", "Filtering" },
		{ "Comment", "/** Class filters, used for high-level filtering of AActor instances inside of a UWorld */" },
		{ "ModuleRelativePath", "Public/SourceFilterCollection.h" },
		{ "ToolTip", "Class filters, used for high-level filtering of AActor instances inside of a UWorld" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ClassFilters = { "ClassFilters", nullptr, (EPropertyFlags)0x0020080000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USourceFilterCollection, ClassFilters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ClassFilters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ClassFilters_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_FilterClasses_Inner = { "FilterClasses", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_FilterClasses_MetaData[] = {
		{ "Comment", "/** Flat version of the Filter classes contained by this collection, stored according to Filters ordering, with child filters inline */" },
		{ "ModuleRelativePath", "Public/SourceFilterCollection.h" },
		{ "ToolTip", "Flat version of the Filter classes contained by this collection, stored according to Filters ordering, with child filters inline" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_FilterClasses = { "FilterClasses", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USourceFilterCollection, FilterClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_FilterClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_FilterClasses_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent_ValueProp = { "ChildToParent", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UDataSourceFilterSet_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent_Key_KeyProp = { "ChildToParent_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UDataSourceFilter_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent_MetaData[] = {
		{ "Comment", "/** Child / Parent mapping for Filter (sets) */" },
		{ "ModuleRelativePath", "Public/SourceFilterCollection.h" },
		{ "ToolTip", "Child / Parent mapping for Filter (sets)" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent = { "ChildToParent", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USourceFilterCollection, ChildToParent), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USourceFilterCollection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_Filters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_Filters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ClassFilters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ClassFilters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_FilterClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_FilterClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USourceFilterCollection_Statics::NewProp_ChildToParent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USourceFilterCollection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USourceFilterCollection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USourceFilterCollection_Statics::ClassParams = {
		&USourceFilterCollection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USourceFilterCollection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USourceFilterCollection_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_USourceFilterCollection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USourceFilterCollection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USourceFilterCollection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USourceFilterCollection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USourceFilterCollection, 2589916203);
	template<> SOURCEFILTERINGTRACE_API UClass* StaticClass<USourceFilterCollection>()
	{
		return USourceFilterCollection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USourceFilterCollection(Z_Construct_UClass_USourceFilterCollection, &USourceFilterCollection::StaticClass, TEXT("/Script/SourceFilteringTrace"), TEXT("USourceFilterCollection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USourceFilterCollection);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(USourceFilterCollection)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
