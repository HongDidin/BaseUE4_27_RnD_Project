// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SourceFilteringCore/Public/IDataSourceFilterSetInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIDataSourceFilterSetInterface() {}
// Cross Module References
	SOURCEFILTERINGCORE_API UClass* Z_Construct_UClass_UDataSourceFilterSetInterface_NoRegister();
	SOURCEFILTERINGCORE_API UClass* Z_Construct_UClass_UDataSourceFilterSetInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_SourceFilteringCore();
// End Cross Module References
	void UDataSourceFilterSetInterface::StaticRegisterNativesUDataSourceFilterSetInterface()
	{
	}
	UClass* Z_Construct_UClass_UDataSourceFilterSetInterface_NoRegister()
	{
		return UDataSourceFilterSetInterface::StaticClass();
	}
	struct Z_Construct_UClass_UDataSourceFilterSetInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataSourceFilterSetInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_SourceFilteringCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataSourceFilterSetInterface_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/IDataSourceFilterSetInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataSourceFilterSetInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IDataSourceFilterSetInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataSourceFilterSetInterface_Statics::ClassParams = {
		&UDataSourceFilterSetInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UDataSourceFilterSetInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataSourceFilterSetInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataSourceFilterSetInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataSourceFilterSetInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataSourceFilterSetInterface, 1365388227);
	template<> SOURCEFILTERINGCORE_API UClass* StaticClass<UDataSourceFilterSetInterface>()
	{
		return UDataSourceFilterSetInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataSourceFilterSetInterface(Z_Construct_UClass_UDataSourceFilterSetInterface, &UDataSourceFilterSetInterface::StaticClass, TEXT("/Script/SourceFilteringCore"), TEXT("UDataSourceFilterSetInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataSourceFilterSetInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
