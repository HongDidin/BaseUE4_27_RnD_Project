// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PROPERTYACCESSNODE_K2Node_PropertyAccess_generated_h
#error "K2Node_PropertyAccess.generated.h already included, missing '#pragma once' in K2Node_PropertyAccess.h"
#endif
#define PROPERTYACCESSNODE_K2Node_PropertyAccess_generated_h

#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_SPARSE_DATA
#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_RPC_WRAPPERS
#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_PropertyAccess(); \
	friend struct Z_Construct_UClass_UK2Node_PropertyAccess_Statics; \
public: \
	DECLARE_CLASS(UK2Node_PropertyAccess, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PropertyAccessNode"), PROPERTYACCESSNODE_API) \
	DECLARE_SERIALIZER(UK2Node_PropertyAccess) \
	virtual UObject* _getUObject() const override { return const_cast<UK2Node_PropertyAccess*>(this); }


#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_PropertyAccess(); \
	friend struct Z_Construct_UClass_UK2Node_PropertyAccess_Statics; \
public: \
	DECLARE_CLASS(UK2Node_PropertyAccess, UK2Node, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PropertyAccessNode"), PROPERTYACCESSNODE_API) \
	DECLARE_SERIALIZER(UK2Node_PropertyAccess) \
	virtual UObject* _getUObject() const override { return const_cast<UK2Node_PropertyAccess*>(this); }


#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PROPERTYACCESSNODE_API UK2Node_PropertyAccess(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_PropertyAccess) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PROPERTYACCESSNODE_API, UK2Node_PropertyAccess); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_PropertyAccess); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PROPERTYACCESSNODE_API UK2Node_PropertyAccess(UK2Node_PropertyAccess&&); \
	PROPERTYACCESSNODE_API UK2Node_PropertyAccess(const UK2Node_PropertyAccess&); \
public:


#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PROPERTYACCESSNODE_API UK2Node_PropertyAccess(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PROPERTYACCESSNODE_API UK2Node_PropertyAccess(UK2Node_PropertyAccess&&); \
	PROPERTYACCESSNODE_API UK2Node_PropertyAccess(const UK2Node_PropertyAccess&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PROPERTYACCESSNODE_API, UK2Node_PropertyAccess); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_PropertyAccess); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_PropertyAccess)


#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Path() { return STRUCT_OFFSET(UK2Node_PropertyAccess, Path); } \
	FORCEINLINE static uint32 __PPO__TextPath() { return STRUCT_OFFSET(UK2Node_PropertyAccess, TextPath); } \
	FORCEINLINE static uint32 __PPO__ResolvedPinType() { return STRUCT_OFFSET(UK2Node_PropertyAccess, ResolvedPinType); } \
	FORCEINLINE static uint32 __PPO__GeneratedPropertyName() { return STRUCT_OFFSET(UK2Node_PropertyAccess, GeneratedPropertyName); }


#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_12_PROLOG
#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_SPARSE_DATA \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_RPC_WRAPPERS \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_INCLASS \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_SPARSE_DATA \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PROPERTYACCESSNODE_API UClass* StaticClass<class UK2Node_PropertyAccess>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_PropertyAccessNode_Source_PropertyAccessNode_Private_K2Node_PropertyAccess_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
