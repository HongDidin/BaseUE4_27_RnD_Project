// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PropertyAccessNode/Private/K2Node_PropertyAccess.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_PropertyAccess() {}
// Cross Module References
	PROPERTYACCESSNODE_API UClass* Z_Construct_UClass_UK2Node_PropertyAccess_NoRegister();
	PROPERTYACCESSNODE_API UClass* Z_Construct_UClass_UK2Node_PropertyAccess();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node();
	UPackage* Z_Construct_UPackage__Script_PropertyAccessNode();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphPinType();
	ANIMGRAPH_API UClass* Z_Construct_UClass_UClassVariableCreator_NoRegister();
// End Cross Module References
	void UK2Node_PropertyAccess::StaticRegisterNativesUK2Node_PropertyAccess()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_PropertyAccess_NoRegister()
	{
		return UK2Node_PropertyAccess::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_PropertyAccess_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Path_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Path;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TextPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResolvedPinType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ResolvedPinType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeneratedPropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_GeneratedPropertyName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_PropertyAccess_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node,
		(UObject* (*)())Z_Construct_UPackage__Script_PropertyAccessNode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_PropertyAccess_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_PropertyAccess.h" },
		{ "ModuleRelativePath", "Private/K2Node_PropertyAccess.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_Path_Inner = { "Path", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_Path_MetaData[] = {
		{ "Comment", "/** Path that this access exposes */" },
		{ "ModuleRelativePath", "Private/K2Node_PropertyAccess.h" },
		{ "ToolTip", "Path that this access exposes" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_PropertyAccess, Path), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_Path_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_TextPath_MetaData[] = {
		{ "Comment", "/** Path as text, for display */" },
		{ "ModuleRelativePath", "Private/K2Node_PropertyAccess.h" },
		{ "ToolTip", "Path as text, for display" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_TextPath = { "TextPath", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_PropertyAccess, TextPath), METADATA_PARAMS(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_TextPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_TextPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_ResolvedPinType_MetaData[] = {
		{ "Comment", "/** Resolved pin type */" },
		{ "ModuleRelativePath", "Private/K2Node_PropertyAccess.h" },
		{ "ToolTip", "Resolved pin type" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_ResolvedPinType = { "ResolvedPinType", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_PropertyAccess, ResolvedPinType), Z_Construct_UScriptStruct_FEdGraphPinType, METADATA_PARAMS(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_ResolvedPinType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_ResolvedPinType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_GeneratedPropertyName_MetaData[] = {
		{ "Comment", "/** Generated property created during compilation */" },
		{ "ModuleRelativePath", "Private/K2Node_PropertyAccess.h" },
		{ "ToolTip", "Generated property created during compilation" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_GeneratedPropertyName = { "GeneratedPropertyName", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_PropertyAccess, GeneratedPropertyName), METADATA_PARAMS(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_GeneratedPropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_GeneratedPropertyName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UK2Node_PropertyAccess_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_Path_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_Path,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_TextPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_ResolvedPinType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_PropertyAccess_Statics::NewProp_GeneratedPropertyName,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UK2Node_PropertyAccess_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UClassVariableCreator_NoRegister, (int32)VTABLE_OFFSET(UK2Node_PropertyAccess, IClassVariableCreator), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_PropertyAccess_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_PropertyAccess>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_PropertyAccess_Statics::ClassParams = {
		&UK2Node_PropertyAccess::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UK2Node_PropertyAccess_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_PropertyAccess_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_PropertyAccess()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_PropertyAccess_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_PropertyAccess, 4102366536);
	template<> PROPERTYACCESSNODE_API UClass* StaticClass<UK2Node_PropertyAccess>()
	{
		return UK2Node_PropertyAccess::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_PropertyAccess(Z_Construct_UClass_UK2Node_PropertyAccess, &UK2Node_PropertyAccess::StaticClass, TEXT("/Script/PropertyAccessNode"), TEXT("UK2Node_PropertyAccess"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_PropertyAccess);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
