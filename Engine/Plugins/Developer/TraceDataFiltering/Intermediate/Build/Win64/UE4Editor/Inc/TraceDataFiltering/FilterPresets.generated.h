// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TRACEDATAFILTERING_FilterPresets_generated_h
#error "FilterPresets.generated.h already included, missing '#pragma once' in FilterPresets.h"
#endif
#define TRACEDATAFILTERING_FilterPresets_generated_h

#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FFilterData_Statics; \
	TRACEDATAFILTERING_API static class UScriptStruct* StaticStruct();


template<> TRACEDATAFILTERING_API UScriptStruct* StaticStruct<struct FFilterData>();

#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_SPARSE_DATA
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_RPC_WRAPPERS
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULocalFilterPresetContainer(); \
	friend struct Z_Construct_UClass_ULocalFilterPresetContainer_Statics; \
public: \
	DECLARE_CLASS(ULocalFilterPresetContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TraceDataFiltering"), NO_API) \
	DECLARE_SERIALIZER(ULocalFilterPresetContainer) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceDataFilters");} \



#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_INCLASS \
private: \
	static void StaticRegisterNativesULocalFilterPresetContainer(); \
	friend struct Z_Construct_UClass_ULocalFilterPresetContainer_Statics; \
public: \
	DECLARE_CLASS(ULocalFilterPresetContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TraceDataFiltering"), NO_API) \
	DECLARE_SERIALIZER(ULocalFilterPresetContainer) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceDataFilters");} \



#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULocalFilterPresetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULocalFilterPresetContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULocalFilterPresetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULocalFilterPresetContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULocalFilterPresetContainer(ULocalFilterPresetContainer&&); \
	NO_API ULocalFilterPresetContainer(const ULocalFilterPresetContainer&); \
public:


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULocalFilterPresetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULocalFilterPresetContainer(ULocalFilterPresetContainer&&); \
	NO_API ULocalFilterPresetContainer(const ULocalFilterPresetContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULocalFilterPresetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULocalFilterPresetContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULocalFilterPresetContainer)


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__UserPresets() { return STRUCT_OFFSET(ULocalFilterPresetContainer, UserPresets); }


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_89_PROLOG
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_SPARSE_DATA \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_RPC_WRAPPERS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_INCLASS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_SPARSE_DATA \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_92_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TRACEDATAFILTERING_API UClass* StaticClass<class ULocalFilterPresetContainer>();

#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_SPARSE_DATA
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_RPC_WRAPPERS
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSharedFilterPresetContainer(); \
	friend struct Z_Construct_UClass_USharedFilterPresetContainer_Statics; \
public: \
	DECLARE_CLASS(USharedFilterPresetContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/TraceDataFiltering"), NO_API) \
	DECLARE_SERIALIZER(USharedFilterPresetContainer) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceDataFilters");} \



#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_INCLASS \
private: \
	static void StaticRegisterNativesUSharedFilterPresetContainer(); \
	friend struct Z_Construct_UClass_USharedFilterPresetContainer_Statics; \
public: \
	DECLARE_CLASS(USharedFilterPresetContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/TraceDataFiltering"), NO_API) \
	DECLARE_SERIALIZER(USharedFilterPresetContainer) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceDataFilters");} \



#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USharedFilterPresetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USharedFilterPresetContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USharedFilterPresetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USharedFilterPresetContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USharedFilterPresetContainer(USharedFilterPresetContainer&&); \
	NO_API USharedFilterPresetContainer(const USharedFilterPresetContainer&); \
public:


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USharedFilterPresetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USharedFilterPresetContainer(USharedFilterPresetContainer&&); \
	NO_API USharedFilterPresetContainer(const USharedFilterPresetContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USharedFilterPresetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USharedFilterPresetContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USharedFilterPresetContainer)


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SharedPresets() { return STRUCT_OFFSET(USharedFilterPresetContainer, SharedPresets); }


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_106_PROLOG
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_SPARSE_DATA \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_RPC_WRAPPERS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_INCLASS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_SPARSE_DATA \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_109_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TRACEDATAFILTERING_API UClass* StaticClass<class USharedFilterPresetContainer>();

#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_SPARSE_DATA
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_RPC_WRAPPERS
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUEngineFilterPresetContainer(); \
	friend struct Z_Construct_UClass_UEngineFilterPresetContainer_Statics; \
public: \
	DECLARE_CLASS(UEngineFilterPresetContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/TraceDataFiltering"), NO_API) \
	DECLARE_SERIALIZER(UEngineFilterPresetContainer) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceDataFilters");} \



#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_INCLASS \
private: \
	static void StaticRegisterNativesUEngineFilterPresetContainer(); \
	friend struct Z_Construct_UClass_UEngineFilterPresetContainer_Statics; \
public: \
	DECLARE_CLASS(UEngineFilterPresetContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/TraceDataFiltering"), NO_API) \
	DECLARE_SERIALIZER(UEngineFilterPresetContainer) \
	static const TCHAR* StaticConfigName() {return TEXT("TraceDataFilters");} \



#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEngineFilterPresetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEngineFilterPresetContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEngineFilterPresetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEngineFilterPresetContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEngineFilterPresetContainer(UEngineFilterPresetContainer&&); \
	NO_API UEngineFilterPresetContainer(const UEngineFilterPresetContainer&); \
public:


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UEngineFilterPresetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UEngineFilterPresetContainer(UEngineFilterPresetContainer&&); \
	NO_API UEngineFilterPresetContainer(const UEngineFilterPresetContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UEngineFilterPresetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UEngineFilterPresetContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UEngineFilterPresetContainer)


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__EnginePresets() { return STRUCT_OFFSET(UEngineFilterPresetContainer, EnginePresets); }


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_123_PROLOG
#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_SPARSE_DATA \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_RPC_WRAPPERS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_INCLASS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_SPARSE_DATA \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h_126_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TRACEDATAFILTERING_API UClass* StaticClass<class UEngineFilterPresetContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_TraceDataFiltering_Source_TraceDataFiltering_Private_FilterPresets_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
