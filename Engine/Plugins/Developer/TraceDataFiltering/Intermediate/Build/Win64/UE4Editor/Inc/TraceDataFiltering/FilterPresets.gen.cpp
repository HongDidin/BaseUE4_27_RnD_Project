// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TraceDataFiltering/Private/FilterPresets.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFilterPresets() {}
// Cross Module References
	TRACEDATAFILTERING_API UScriptStruct* Z_Construct_UScriptStruct_FFilterData();
	UPackage* Z_Construct_UPackage__Script_TraceDataFiltering();
	TRACEDATAFILTERING_API UClass* Z_Construct_UClass_ULocalFilterPresetContainer_NoRegister();
	TRACEDATAFILTERING_API UClass* Z_Construct_UClass_ULocalFilterPresetContainer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	TRACEDATAFILTERING_API UClass* Z_Construct_UClass_USharedFilterPresetContainer_NoRegister();
	TRACEDATAFILTERING_API UClass* Z_Construct_UClass_USharedFilterPresetContainer();
	TRACEDATAFILTERING_API UClass* Z_Construct_UClass_UEngineFilterPresetContainer_NoRegister();
	TRACEDATAFILTERING_API UClass* Z_Construct_UClass_UEngineFilterPresetContainer();
// End Cross Module References
class UScriptStruct* FFilterData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern TRACEDATAFILTERING_API uint32 Get_Z_Construct_UScriptStruct_FFilterData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FFilterData, Z_Construct_UPackage__Script_TraceDataFiltering(), TEXT("FilterData"), sizeof(FFilterData), Get_Z_Construct_UScriptStruct_FFilterData_Hash());
	}
	return Singleton;
}
template<> TRACEDATAFILTERING_API UScriptStruct* StaticStruct<FFilterData>()
{
	return FFilterData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FFilterData(FFilterData::StaticStruct, TEXT("/Script/TraceDataFiltering"), TEXT("FilterData"), false, nullptr, nullptr);
static struct FScriptStruct_TraceDataFiltering_StaticRegisterNativesFFilterData
{
	FScriptStruct_TraceDataFiltering_StaticRegisterNativesFFilterData()
	{
		UScriptStruct::DeferCppStructOps<FFilterData>(FName(TEXT("FilterData")));
	}
} ScriptStruct_TraceDataFiltering_StaticRegisterNativesFFilterData;
	struct Z_Construct_UScriptStruct_FFilterData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_WhitelistedNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WhitelistedNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WhitelistedNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Structure representing an individual preset in configuration (ini) files */" },
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
		{ "ToolTip", "Structure representing an individual preset in configuration (ini) files" },
	};
#endif
	void* Z_Construct_UScriptStruct_FFilterData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FFilterData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterData, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_WhitelistedNames_Inner = { "WhitelistedNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_WhitelistedNames_MetaData[] = {
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_WhitelistedNames = { "WhitelistedNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FFilterData, WhitelistedNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_WhitelistedNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_WhitelistedNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FFilterData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_WhitelistedNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FFilterData_Statics::NewProp_WhitelistedNames,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FFilterData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_TraceDataFiltering,
		nullptr,
		&NewStructOps,
		"FilterData",
		sizeof(FFilterData),
		alignof(FFilterData),
		Z_Construct_UScriptStruct_FFilterData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FFilterData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FFilterData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FFilterData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FFilterData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_TraceDataFiltering();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("FilterData"), sizeof(FFilterData), Get_Z_Construct_UScriptStruct_FFilterData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FFilterData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FFilterData_Hash() { return 651877145U; }
	void ULocalFilterPresetContainer::StaticRegisterNativesULocalFilterPresetContainer()
	{
	}
	UClass* Z_Construct_UClass_ULocalFilterPresetContainer_NoRegister()
	{
		return ULocalFilterPresetContainer::StaticClass();
	}
	struct Z_Construct_UClass_ULocalFilterPresetContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UserPresets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserPresets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UserPresets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULocalFilterPresetContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TraceDataFiltering,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocalFilterPresetContainer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UObject containers for the preset data */" },
		{ "IncludePath", "FilterPresets.h" },
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
		{ "ToolTip", "UObject containers for the preset data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULocalFilterPresetContainer_Statics::NewProp_UserPresets_Inner = { "UserPresets", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFilterData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULocalFilterPresetContainer_Statics::NewProp_UserPresets_MetaData[] = {
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ULocalFilterPresetContainer_Statics::NewProp_UserPresets = { "UserPresets", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULocalFilterPresetContainer, UserPresets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULocalFilterPresetContainer_Statics::NewProp_UserPresets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULocalFilterPresetContainer_Statics::NewProp_UserPresets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULocalFilterPresetContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULocalFilterPresetContainer_Statics::NewProp_UserPresets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULocalFilterPresetContainer_Statics::NewProp_UserPresets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULocalFilterPresetContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULocalFilterPresetContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULocalFilterPresetContainer_Statics::ClassParams = {
		&ULocalFilterPresetContainer::StaticClass,
		"TraceDataFilters",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULocalFilterPresetContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULocalFilterPresetContainer_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULocalFilterPresetContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULocalFilterPresetContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULocalFilterPresetContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULocalFilterPresetContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULocalFilterPresetContainer, 4115988603);
	template<> TRACEDATAFILTERING_API UClass* StaticClass<ULocalFilterPresetContainer>()
	{
		return ULocalFilterPresetContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULocalFilterPresetContainer(Z_Construct_UClass_ULocalFilterPresetContainer, &ULocalFilterPresetContainer::StaticClass, TEXT("/Script/TraceDataFiltering"), TEXT("ULocalFilterPresetContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULocalFilterPresetContainer);
	void USharedFilterPresetContainer::StaticRegisterNativesUSharedFilterPresetContainer()
	{
	}
	UClass* Z_Construct_UClass_USharedFilterPresetContainer_NoRegister()
	{
		return USharedFilterPresetContainer::StaticClass();
	}
	struct Z_Construct_UClass_USharedFilterPresetContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SharedPresets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SharedPresets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SharedPresets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USharedFilterPresetContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TraceDataFiltering,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USharedFilterPresetContainer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FilterPresets.h" },
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USharedFilterPresetContainer_Statics::NewProp_SharedPresets_Inner = { "SharedPresets", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFilterData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USharedFilterPresetContainer_Statics::NewProp_SharedPresets_MetaData[] = {
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USharedFilterPresetContainer_Statics::NewProp_SharedPresets = { "SharedPresets", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USharedFilterPresetContainer, SharedPresets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USharedFilterPresetContainer_Statics::NewProp_SharedPresets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USharedFilterPresetContainer_Statics::NewProp_SharedPresets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USharedFilterPresetContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USharedFilterPresetContainer_Statics::NewProp_SharedPresets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USharedFilterPresetContainer_Statics::NewProp_SharedPresets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USharedFilterPresetContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USharedFilterPresetContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USharedFilterPresetContainer_Statics::ClassParams = {
		&USharedFilterPresetContainer::StaticClass,
		"TraceDataFilters",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USharedFilterPresetContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USharedFilterPresetContainer_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_USharedFilterPresetContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USharedFilterPresetContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USharedFilterPresetContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USharedFilterPresetContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USharedFilterPresetContainer, 1357246071);
	template<> TRACEDATAFILTERING_API UClass* StaticClass<USharedFilterPresetContainer>()
	{
		return USharedFilterPresetContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USharedFilterPresetContainer(Z_Construct_UClass_USharedFilterPresetContainer, &USharedFilterPresetContainer::StaticClass, TEXT("/Script/TraceDataFiltering"), TEXT("USharedFilterPresetContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USharedFilterPresetContainer);
	void UEngineFilterPresetContainer::StaticRegisterNativesUEngineFilterPresetContainer()
	{
	}
	UClass* Z_Construct_UClass_UEngineFilterPresetContainer_NoRegister()
	{
		return UEngineFilterPresetContainer::StaticClass();
	}
	struct Z_Construct_UClass_UEngineFilterPresetContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EnginePresets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EnginePresets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_EnginePresets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEngineFilterPresetContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TraceDataFiltering,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEngineFilterPresetContainer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FilterPresets.h" },
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UEngineFilterPresetContainer_Statics::NewProp_EnginePresets_Inner = { "EnginePresets", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFilterData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEngineFilterPresetContainer_Statics::NewProp_EnginePresets_MetaData[] = {
		{ "ModuleRelativePath", "Private/FilterPresets.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UEngineFilterPresetContainer_Statics::NewProp_EnginePresets = { "EnginePresets", nullptr, (EPropertyFlags)0x0020080000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UEngineFilterPresetContainer, EnginePresets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UEngineFilterPresetContainer_Statics::NewProp_EnginePresets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEngineFilterPresetContainer_Statics::NewProp_EnginePresets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEngineFilterPresetContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEngineFilterPresetContainer_Statics::NewProp_EnginePresets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEngineFilterPresetContainer_Statics::NewProp_EnginePresets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEngineFilterPresetContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEngineFilterPresetContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEngineFilterPresetContainer_Statics::ClassParams = {
		&UEngineFilterPresetContainer::StaticClass,
		"TraceDataFilters",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEngineFilterPresetContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEngineFilterPresetContainer_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UEngineFilterPresetContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEngineFilterPresetContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEngineFilterPresetContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEngineFilterPresetContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEngineFilterPresetContainer, 3470515876);
	template<> TRACEDATAFILTERING_API UClass* StaticClass<UEngineFilterPresetContainer>()
	{
		return UEngineFilterPresetContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEngineFilterPresetContainer(Z_Construct_UClass_UEngineFilterPresetContainer, &UEngineFilterPresetContainer::StaticClass, TEXT("/Script/TraceDataFiltering"), TEXT("UEngineFilterPresetContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEngineFilterPresetContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
