// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MultiUserClient/Private/Widgets/ConcertSessionBrowserSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertSessionBrowserSettings() {}
// Cross Module References
	MULTIUSERCLIENT_API UClass* Z_Construct_UClass_UConcertSessionBrowserSettings_NoRegister();
	MULTIUSERCLIENT_API UClass* Z_Construct_UClass_UConcertSessionBrowserSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MultiUserClient();
// End Cross Module References
	void UConcertSessionBrowserSettings::StaticRegisterNativesUConcertSessionBrowserSettings()
	{
	}
	UClass* Z_Construct_UClass_UConcertSessionBrowserSettings_NoRegister()
	{
		return UConcertSessionBrowserSettings::StaticClass();
	}
	struct Z_Construct_UClass_UConcertSessionBrowserSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowActiveSessions_MetaData[];
#endif
		static void NewProp_bShowActiveSessions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowActiveSessions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowArchivedSessions_MetaData[];
#endif
		static void NewProp_bShowArchivedSessions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowArchivedSessions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowDefaultServerSessionsOnly_MetaData[];
#endif
		static void NewProp_bShowDefaultServerSessionsOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowDefaultServerSessionsOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MultiUserClient,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Serializes the multi-user session browser settings like the active filters. */" },
		{ "IncludePath", "Widgets/ConcertSessionBrowserSettings.h" },
		{ "ModuleRelativePath", "Private/Widgets/ConcertSessionBrowserSettings.h" },
		{ "ToolTip", "Serializes the multi-user session browser settings like the active filters." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowActiveSessions_MetaData[] = {
		{ "Category", "Multi-User Session Browser" },
		{ "ModuleRelativePath", "Private/Widgets/ConcertSessionBrowserSettings.h" },
	};
#endif
	void Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowActiveSessions_SetBit(void* Obj)
	{
		((UConcertSessionBrowserSettings*)Obj)->bShowActiveSessions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowActiveSessions = { "bShowActiveSessions", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertSessionBrowserSettings), &Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowActiveSessions_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowActiveSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowActiveSessions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowArchivedSessions_MetaData[] = {
		{ "Category", "Multi-User Session Browser" },
		{ "ModuleRelativePath", "Private/Widgets/ConcertSessionBrowserSettings.h" },
	};
#endif
	void Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowArchivedSessions_SetBit(void* Obj)
	{
		((UConcertSessionBrowserSettings*)Obj)->bShowArchivedSessions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowArchivedSessions = { "bShowArchivedSessions", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertSessionBrowserSettings), &Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowArchivedSessions_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowArchivedSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowArchivedSessions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowDefaultServerSessionsOnly_MetaData[] = {
		{ "Category", "Multi-User Session Browser" },
		{ "ModuleRelativePath", "Private/Widgets/ConcertSessionBrowserSettings.h" },
	};
#endif
	void Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowDefaultServerSessionsOnly_SetBit(void* Obj)
	{
		((UConcertSessionBrowserSettings*)Obj)->bShowDefaultServerSessionsOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowDefaultServerSessionsOnly = { "bShowDefaultServerSessionsOnly", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertSessionBrowserSettings), &Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowDefaultServerSessionsOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowDefaultServerSessionsOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowDefaultServerSessionsOnly_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowActiveSessions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowArchivedSessions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::NewProp_bShowDefaultServerSessionsOnly,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertSessionBrowserSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::ClassParams = {
		&UConcertSessionBrowserSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertSessionBrowserSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertSessionBrowserSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertSessionBrowserSettings, 4181048309);
	template<> MULTIUSERCLIENT_API UClass* StaticClass<UConcertSessionBrowserSettings>()
	{
		return UConcertSessionBrowserSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertSessionBrowserSettings(Z_Construct_UClass_UConcertSessionBrowserSettings, &UConcertSessionBrowserSettings::StaticClass, TEXT("/Script/MultiUserClient"), TEXT("UConcertSessionBrowserSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertSessionBrowserSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
