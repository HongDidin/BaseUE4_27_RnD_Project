// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MULTIUSERCLIENT_ConcertSessionBrowserSettings_generated_h
#error "ConcertSessionBrowserSettings.generated.h already included, missing '#pragma once' in ConcertSessionBrowserSettings.h"
#endif
#define MULTIUSERCLIENT_ConcertSessionBrowserSettings_generated_h

#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertSessionBrowserSettings(); \
	friend struct Z_Construct_UClass_UConcertSessionBrowserSettings_Statics; \
public: \
	DECLARE_CLASS(UConcertSessionBrowserSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MultiUserClient"), NO_API) \
	DECLARE_SERIALIZER(UConcertSessionBrowserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUConcertSessionBrowserSettings(); \
	friend struct Z_Construct_UClass_UConcertSessionBrowserSettings_Statics; \
public: \
	DECLARE_CLASS(UConcertSessionBrowserSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MultiUserClient"), NO_API) \
	DECLARE_SERIALIZER(UConcertSessionBrowserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertSessionBrowserSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertSessionBrowserSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertSessionBrowserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertSessionBrowserSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertSessionBrowserSettings(UConcertSessionBrowserSettings&&); \
	NO_API UConcertSessionBrowserSettings(const UConcertSessionBrowserSettings&); \
public:


#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertSessionBrowserSettings(UConcertSessionBrowserSettings&&); \
	NO_API UConcertSessionBrowserSettings(const UConcertSessionBrowserSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertSessionBrowserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertSessionBrowserSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConcertSessionBrowserSettings)


#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_11_PROLOG
#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_INCLASS \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MULTIUSERCLIENT_API UClass* StaticClass<class UConcertSessionBrowserSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_MultiUserClient_Source_MultiUserClient_Private_Widgets_ConcertSessionBrowserSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
