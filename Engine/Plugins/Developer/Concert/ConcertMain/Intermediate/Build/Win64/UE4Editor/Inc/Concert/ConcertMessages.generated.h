// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERT_ConcertMessages_generated_h
#error "ConcertMessages.generated.h already included, missing '#pragma once' in ConcertMessages.h"
#endif
#define CONCERT_ConcertMessages_generated_h

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_598_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_CustomResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_577_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_CustomRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_556_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEventData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_CustomEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_547_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEventData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_SessionRenamedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_538_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEventData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_ClientListUpdatedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_529_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEventData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_UpdateClientInfoEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_520_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEventData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_LeaveSessionEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_505_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEndpointDiscoveryEvent Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_JoinSessionResultEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_493_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEndpointDiscoveryEvent Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSession_DiscoverAndJoinSessionEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_479_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetSessionActivitiesResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_461_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetSessionActivitiesRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_452_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetSessionClientsResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_443_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetSessionClientsRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_429_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_DeleteSessionResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_412_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_DeleteSessionRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_396_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_RenameSessionResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_375_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_RenameSessionRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_352_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_ArchiveSessionResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_326_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_ArchiveSessionRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_316_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_SessionInfoResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_283_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_CopySessionRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_264_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_FindSessionRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_246_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_CreateSessionRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_237_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetSessionsResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_231_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetArchivedSessionsRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_225_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetLiveSessionsRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_213_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetAllSessionsResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_207_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetAllSessionsRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_197_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_DropSessionRepositoriesResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_187_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_DropSessionRepositoriesRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_175_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetSessionRepositoriesResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_169_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_GetSessionRepositoriesRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_159_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertResponseData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_MountSessionRepositoryResponse>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_137_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertRequestData Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_MountSessionRepositoryRequest>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_120_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSessionRepositoryInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_101_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEndpointDiscoveryEvent Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_ServerDiscoveredEvent>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h_83_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertEndpointDiscoveryEvent Super;


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertAdmin_DiscoverServersEvent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessages_h


#define FOREACH_ENUM_ECONCERTSESSIONREPOSITORYMOUNTRESPONSECODE(op) \
	op(EConcertSessionRepositoryMountResponseCode::None) \
	op(EConcertSessionRepositoryMountResponseCode::Mounted) \
	op(EConcertSessionRepositoryMountResponseCode::AlreadyMounted) \
	op(EConcertSessionRepositoryMountResponseCode::NotFound) 

enum class EConcertSessionRepositoryMountResponseCode : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertSessionRepositoryMountResponseCode>();

#define FOREACH_ENUM_ECONCERTSESSIONRESPONSECODE(op) \
	op(EConcertSessionResponseCode::Success) \
	op(EConcertSessionResponseCode::Failed) \
	op(EConcertSessionResponseCode::InvalidRequest) 

enum class EConcertSessionResponseCode : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertSessionResponseCode>();

#define FOREACH_ENUM_ECONCERTCLIENTSTATUS(op) \
	op(EConcertClientStatus::Connected) \
	op(EConcertClientStatus::Disconnected) \
	op(EConcertClientStatus::Updated) 

enum class EConcertClientStatus : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertClientStatus>();

#define FOREACH_ENUM_ECONCERTCONNECTIONRESULT(op) \
	op(EConcertConnectionResult::None) \
	op(EConcertConnectionResult::ConnectionAccepted) \
	op(EConcertConnectionResult::ConnectionRefused) \
	op(EConcertConnectionResult::AlreadyConnected) 

enum class EConcertConnectionResult : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertConnectionResult>();

#define FOREACH_ENUM_ECONCERTCONNECTIONSTATUS(op) \
	op(EConcertConnectionStatus::Connecting) \
	op(EConcertConnectionStatus::Connected) \
	op(EConcertConnectionStatus::Disconnecting) \
	op(EConcertConnectionStatus::Disconnected) 

enum class EConcertConnectionStatus : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertConnectionStatus>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
