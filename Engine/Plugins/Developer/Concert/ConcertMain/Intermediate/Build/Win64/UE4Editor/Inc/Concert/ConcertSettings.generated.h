// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERT_ConcertSettings_generated_h
#error "ConcertSettings.generated.h already included, missing '#pragma once' in ConcertSettings.h"
#endif
#define CONCERT_ConcertSettings_generated_h

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_277_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSourceControlSettings>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_198_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientSettings_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertClientSettings>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_86_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertServerSettings_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertServerSettings>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_28_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSessionSettings_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSessionSettings>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertServerConfig(); \
	friend struct Z_Construct_UClass_UConcertServerConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertServerConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Concert"), NO_API) \
	DECLARE_SERIALIZER(UConcertServerConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_INCLASS \
private: \
	static void StaticRegisterNativesUConcertServerConfig(); \
	friend struct Z_Construct_UClass_UConcertServerConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertServerConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Concert"), NO_API) \
	DECLARE_SERIALIZER(UConcertServerConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertServerConfig(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertServerConfig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertServerConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertServerConfig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertServerConfig(UConcertServerConfig&&); \
	NO_API UConcertServerConfig(const UConcertServerConfig&); \
public:


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertServerConfig(UConcertServerConfig&&); \
	NO_API UConcertServerConfig(const UConcertServerConfig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertServerConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertServerConfig); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConcertServerConfig)


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_97_PROLOG
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_INCLASS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_100_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERT_API UClass* StaticClass<class UConcertServerConfig>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertClientConfig(); \
	friend struct Z_Construct_UClass_UConcertClientConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertClientConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Concert"), NO_API) \
	DECLARE_SERIALIZER(UConcertClientConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_INCLASS \
private: \
	static void StaticRegisterNativesUConcertClientConfig(); \
	friend struct Z_Construct_UClass_UConcertClientConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertClientConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Concert"), NO_API) \
	DECLARE_SERIALIZER(UConcertClientConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertClientConfig(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertClientConfig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertClientConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertClientConfig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertClientConfig(UConcertClientConfig&&); \
	NO_API UConcertClientConfig(const UConcertClientConfig&); \
public:


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertClientConfig(UConcertClientConfig&&); \
	NO_API UConcertClientConfig(const UConcertClientConfig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertClientConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertClientConfig); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConcertClientConfig)


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_287_PROLOG
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_INCLASS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h_290_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERT_API UClass* StaticClass<class UConcertClientConfig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertSettings_h


#define FOREACH_ENUM_ECONCERTSOURCEVALIDATIONMODE(op) \
	op(EConcertSourceValidationMode::Hard) \
	op(EConcertSourceValidationMode::Soft) \
	op(EConcertSourceValidationMode::SoftAutoProceed) 

enum class EConcertSourceValidationMode : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertSourceValidationMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
