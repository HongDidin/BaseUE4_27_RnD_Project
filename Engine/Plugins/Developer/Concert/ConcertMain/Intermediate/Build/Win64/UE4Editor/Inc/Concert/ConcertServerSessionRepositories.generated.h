// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERT_ConcertServerSessionRepositories_generated_h
#error "ConcertServerSessionRepositories.generated.h already included, missing '#pragma once' in ConcertServerSessionRepositories.h"
#endif
#define CONCERT_ConcertServerSessionRepositories_generated_h

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Private_ConcertServerSessionRepositories_h_73_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertServerSessionRepositoryDatabase>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Private_ConcertServerSessionRepositories_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertServerSessionRepository>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Private_ConcertServerSessionRepositories_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
