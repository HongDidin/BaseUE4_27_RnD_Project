// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertTransport/Public/ConcertTransportMessages.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertTransportMessages() {}
// Cross Module References
	CONCERTTRANSPORT_API UEnum* Z_Construct_UEnum_ConcertTransport_EConcertMessageVersion();
	UPackage* Z_Construct_UPackage__Script_ConcertTransport();
	CONCERTTRANSPORT_API UEnum* Z_Construct_UEnum_ConcertTransport_EConcertReliableHandshakeState();
	CONCERTTRANSPORT_API UEnum* Z_Construct_UEnum_ConcertTransport_EConcertResponseCode();
	CONCERTTRANSPORT_API UEnum* Z_Construct_UEnum_ConcertTransport_EConcertMessageFlags();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSendResendPending();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertKeepAlive();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertMessageData();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAckData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertReliableHandshakeData();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointClosedData();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEventData();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertResponseData();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertRequestData();
// End Cross Module References
	static UEnum* EConcertMessageVersion_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertTransport_EConcertMessageVersion, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("EConcertMessageVersion"));
		}
		return Singleton;
	}
	template<> CONCERTTRANSPORT_API UEnum* StaticEnum<EConcertMessageVersion>()
	{
		return EConcertMessageVersion_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertMessageVersion(EConcertMessageVersion_StaticEnum, TEXT("/Script/ConcertTransport"), TEXT("EConcertMessageVersion"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertTransport_EConcertMessageVersion_Hash() { return 2334780075U; }
	UEnum* Z_Construct_UEnum_ConcertTransport_EConcertMessageVersion()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertMessageVersion"), 0, Get_Z_Construct_UEnum_ConcertTransport_EConcertMessageVersion_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertMessageVersion::BeforeVersioning", (int64)EConcertMessageVersion::BeforeVersioning },
				{ "EConcertMessageVersion::Initial", (int64)EConcertMessageVersion::Initial },
				{ "EConcertMessageVersion::VersionPlusOne", (int64)EConcertMessageVersion::VersionPlusOne },
				{ "EConcertMessageVersion::LatestVersion", (int64)EConcertMessageVersion::LatestVersion },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BeforeVersioning.Name", "EConcertMessageVersion::BeforeVersioning" },
				{ "Comment", "/** Versioning for concert message protocol */" },
				{ "Initial.Name", "EConcertMessageVersion::Initial" },
				{ "LatestVersion.Name", "EConcertMessageVersion::LatestVersion" },
				{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
				{ "ToolTip", "Versioning for concert message protocol" },
				{ "VersionPlusOne.Comment", "// -----<new versions can be added above this line>-------------------------------------------------\n" },
				{ "VersionPlusOne.Name", "EConcertMessageVersion::VersionPlusOne" },
				{ "VersionPlusOne.ToolTip", "-----<new versions can be added above this line>-------------------------------------------------" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertTransport,
				nullptr,
				"EConcertMessageVersion",
				"EConcertMessageVersion",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertReliableHandshakeState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertTransport_EConcertReliableHandshakeState, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("EConcertReliableHandshakeState"));
		}
		return Singleton;
	}
	template<> CONCERTTRANSPORT_API UEnum* StaticEnum<EConcertReliableHandshakeState>()
	{
		return EConcertReliableHandshakeState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertReliableHandshakeState(EConcertReliableHandshakeState_StaticEnum, TEXT("/Script/ConcertTransport"), TEXT("EConcertReliableHandshakeState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertTransport_EConcertReliableHandshakeState_Hash() { return 2026356050U; }
	UEnum* Z_Construct_UEnum_ConcertTransport_EConcertReliableHandshakeState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertReliableHandshakeState"), 0, Get_Z_Construct_UEnum_ConcertTransport_EConcertReliableHandshakeState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertReliableHandshakeState::None", (int64)EConcertReliableHandshakeState::None },
				{ "EConcertReliableHandshakeState::Negotiate", (int64)EConcertReliableHandshakeState::Negotiate },
				{ "EConcertReliableHandshakeState::Success", (int64)EConcertReliableHandshakeState::Success },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Handshake codes used during reliable channel negotiation */" },
				{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
				{ "Negotiate.Comment", "/** Handshake is being negotiated */" },
				{ "Negotiate.Name", "EConcertReliableHandshakeState::Negotiate" },
				{ "Negotiate.ToolTip", "Handshake is being negotiated" },
				{ "None.Comment", "/** Handshake initialization state */" },
				{ "None.Name", "EConcertReliableHandshakeState::None" },
				{ "None.ToolTip", "Handshake initialization state" },
				{ "Success.Comment", "/** Handshake was successfully negotiated */" },
				{ "Success.Name", "EConcertReliableHandshakeState::Success" },
				{ "Success.ToolTip", "Handshake was successfully negotiated" },
				{ "ToolTip", "Handshake codes used during reliable channel negotiation" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertTransport,
				nullptr,
				"EConcertReliableHandshakeState",
				"EConcertReliableHandshakeState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertResponseCode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertTransport_EConcertResponseCode, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("EConcertResponseCode"));
		}
		return Singleton;
	}
	template<> CONCERTTRANSPORT_API UEnum* StaticEnum<EConcertResponseCode>()
	{
		return EConcertResponseCode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertResponseCode(EConcertResponseCode_StaticEnum, TEXT("/Script/ConcertTransport"), TEXT("EConcertResponseCode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertTransport_EConcertResponseCode_Hash() { return 2002486508U; }
	UEnum* Z_Construct_UEnum_ConcertTransport_EConcertResponseCode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertResponseCode"), 0, Get_Z_Construct_UEnum_ConcertTransport_EConcertResponseCode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertResponseCode::Pending", (int64)EConcertResponseCode::Pending },
				{ "EConcertResponseCode::Success", (int64)EConcertResponseCode::Success },
				{ "EConcertResponseCode::Failed", (int64)EConcertResponseCode::Failed },
				{ "EConcertResponseCode::InvalidRequest", (int64)EConcertResponseCode::InvalidRequest },
				{ "EConcertResponseCode::UnknownRequest", (int64)EConcertResponseCode::UnknownRequest },
				{ "EConcertResponseCode::TimedOut", (int64)EConcertResponseCode::TimedOut },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Response codes to a sent request */" },
				{ "Failed.Comment", "/** The request data was valid, but the request failed. A response was generated. */" },
				{ "Failed.Name", "EConcertResponseCode::Failed" },
				{ "Failed.ToolTip", "The request data was valid, but the request failed. A response was generated." },
				{ "InvalidRequest.Comment", "/** The request data was invalid. No response was generated. */" },
				{ "InvalidRequest.Name", "EConcertResponseCode::InvalidRequest" },
				{ "InvalidRequest.ToolTip", "The request data was invalid. No response was generated." },
				{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
				{ "Pending.Comment", "/** The response code is still pending. */" },
				{ "Pending.Name", "EConcertResponseCode::Pending" },
				{ "Pending.ToolTip", "The response code is still pending." },
				{ "Success.Comment", "/** The request data was valid. A response was generated. */" },
				{ "Success.Name", "EConcertResponseCode::Success" },
				{ "Success.ToolTip", "The request data was valid. A response was generated." },
				{ "TimedOut.Comment", "/** The request failed to reach the target instance. No response was generated. */" },
				{ "TimedOut.Name", "EConcertResponseCode::TimedOut" },
				{ "TimedOut.ToolTip", "The request failed to reach the target instance. No response was generated." },
				{ "ToolTip", "Response codes to a sent request" },
				{ "UnknownRequest.Comment", "/** The request type was unknown on the target instance. No response was generated. */" },
				{ "UnknownRequest.Name", "EConcertResponseCode::UnknownRequest" },
				{ "UnknownRequest.ToolTip", "The request type was unknown on the target instance. No response was generated." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertTransport,
				nullptr,
				"EConcertResponseCode",
				"EConcertResponseCode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertMessageFlags_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertTransport_EConcertMessageFlags, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("EConcertMessageFlags"));
		}
		return Singleton;
	}
	template<> CONCERTTRANSPORT_API UEnum* StaticEnum<EConcertMessageFlags>()
	{
		return EConcertMessageFlags_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertMessageFlags(EConcertMessageFlags_StaticEnum, TEXT("/Script/ConcertTransport"), TEXT("EConcertMessageFlags"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertTransport_EConcertMessageFlags_Hash() { return 1414013385U; }
	UEnum* Z_Construct_UEnum_ConcertTransport_EConcertMessageFlags()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertMessageFlags"), 0, Get_Z_Construct_UEnum_ConcertTransport_EConcertMessageFlags_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertMessageFlags::None", (int64)EConcertMessageFlags::None },
				{ "EConcertMessageFlags::ReliableOrdered", (int64)EConcertMessageFlags::ReliableOrdered },
				{ "EConcertMessageFlags::UniqueId", (int64)EConcertMessageFlags::UniqueId },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Message Flags when sent */" },
				{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
				{ "None.Comment", "/** No special flags */" },
				{ "None.Name", "EConcertMessageFlags::None" },
				{ "None.ToolTip", "No special flags" },
				{ "ReliableOrdered.Comment", "/** Guarantee that this message is received by the client(s) and processed in the order they were sent */" },
				{ "ReliableOrdered.Name", "EConcertMessageFlags::ReliableOrdered" },
				{ "ReliableOrdered.ToolTip", "Guarantee that this message is received by the client(s) and processed in the order they were sent" },
				{ "ToolTip", "Message Flags when sent" },
				{ "UniqueId.Comment", "/** Message sent with this flag should be uniquely identifiable across clients. */" },
				{ "UniqueId.Name", "EConcertMessageFlags::UniqueId" },
				{ "UniqueId.ToolTip", "Message sent with this flag should be uniquely identifiable across clients." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertTransport,
				nullptr,
				"EConcertMessageFlags",
				"EConcertMessageFlags",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertSendResendPending::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSendResendPending_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSendResendPending, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertSendResendPending"), sizeof(FConcertSendResendPending), Get_Z_Construct_UScriptStruct_FConcertSendResendPending_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertSendResendPending>()
{
	return FConcertSendResendPending::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSendResendPending(FConcertSendResendPending::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertSendResendPending"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertSendResendPending
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertSendResendPending()
	{
		UScriptStruct::DeferCppStructOps<FConcertSendResendPending>(FName(TEXT("ConcertSendResendPending")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertSendResendPending;
	struct Z_Construct_UScriptStruct_FConcertSendResendPending_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSendResendPending_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Force the endpoint to resend pending packages.*/" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Force the endpoint to resend pending packages." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSendResendPending_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSendResendPending>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSendResendPending_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		nullptr,
		&NewStructOps,
		"ConcertSendResendPending",
		sizeof(FConcertSendResendPending),
		alignof(FConcertSendResendPending),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSendResendPending_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSendResendPending_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSendResendPending()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSendResendPending_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSendResendPending"), sizeof(FConcertSendResendPending), Get_Z_Construct_UScriptStruct_FConcertSendResendPending_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSendResendPending_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSendResendPending_Hash() { return 2143297655U; }

static_assert(std::is_polymorphic<FConcertKeepAlive>() == std::is_polymorphic<FConcertMessageData>(), "USTRUCT FConcertKeepAlive cannot be polymorphic unless super FConcertMessageData is polymorphic");

class UScriptStruct* FConcertKeepAlive::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertKeepAlive_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertKeepAlive, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertKeepAlive"), sizeof(FConcertKeepAlive), Get_Z_Construct_UScriptStruct_FConcertKeepAlive_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertKeepAlive>()
{
	return FConcertKeepAlive::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertKeepAlive(FConcertKeepAlive::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertKeepAlive"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertKeepAlive
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertKeepAlive()
	{
		UScriptStruct::DeferCppStructOps<FConcertKeepAlive>(FName(TEXT("ConcertKeepAlive")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertKeepAlive;
	struct Z_Construct_UScriptStruct_FConcertKeepAlive_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertKeepAlive_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Keep alive message */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Keep alive message" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertKeepAlive_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertKeepAlive>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertKeepAlive_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertMessageData,
		&NewStructOps,
		"ConcertKeepAlive",
		sizeof(FConcertKeepAlive),
		alignof(FConcertKeepAlive),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertKeepAlive_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertKeepAlive_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertKeepAlive()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertKeepAlive_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertKeepAlive"), sizeof(FConcertKeepAlive), Get_Z_Construct_UScriptStruct_FConcertKeepAlive_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertKeepAlive_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertKeepAlive_Hash() { return 1019585569U; }

static_assert(std::is_polymorphic<FConcertAckData>() == std::is_polymorphic<FConcertMessageData>(), "USTRUCT FConcertAckData cannot be polymorphic unless super FConcertMessageData is polymorphic");

class UScriptStruct* FConcertAckData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAckData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAckData, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertAckData"), sizeof(FConcertAckData), Get_Z_Construct_UScriptStruct_FConcertAckData_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertAckData>()
{
	return FConcertAckData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAckData(FConcertAckData::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertAckData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertAckData
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertAckData()
	{
		UScriptStruct::DeferCppStructOps<FConcertAckData>(FName(TEXT("ConcertAckData")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertAckData;
	struct Z_Construct_UScriptStruct_FConcertAckData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AckSendTimeTicks_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_AckSendTimeTicks;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceMessageId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourceMessageId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAckData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Acknowledgment messages to reliable events */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Acknowledgment messages to reliable events" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAckData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAckData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_AckSendTimeTicks_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Time when this acknowledgment was sent (UTC) */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Time when this acknowledgment was sent (UTC)" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_AckSendTimeTicks = { "AckSendTimeTicks", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAckData, AckSendTimeTicks), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_AckSendTimeTicks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_AckSendTimeTicks_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_SourceMessageId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** ID of the source message we're acknowledging */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "ID of the source message we're acknowledging" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_SourceMessageId = { "SourceMessageId", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAckData, SourceMessageId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_SourceMessageId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_SourceMessageId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAckData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_AckSendTimeTicks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAckData_Statics::NewProp_SourceMessageId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAckData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertMessageData,
		&NewStructOps,
		"ConcertAckData",
		sizeof(FConcertAckData),
		alignof(FConcertAckData),
		Z_Construct_UScriptStruct_FConcertAckData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAckData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAckData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAckData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAckData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAckData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAckData"), sizeof(FConcertAckData), Get_Z_Construct_UScriptStruct_FConcertAckData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAckData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAckData_Hash() { return 3303235709U; }

static_assert(std::is_polymorphic<FConcertReliableHandshakeData>() == std::is_polymorphic<FConcertEndpointDiscoveryEvent>(), "USTRUCT FConcertReliableHandshakeData cannot be polymorphic unless super FConcertEndpointDiscoveryEvent is polymorphic");

class UScriptStruct* FConcertReliableHandshakeData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertReliableHandshakeData, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertReliableHandshakeData"), sizeof(FConcertReliableHandshakeData), Get_Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertReliableHandshakeData>()
{
	return FConcertReliableHandshakeData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertReliableHandshakeData(FConcertReliableHandshakeData::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertReliableHandshakeData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertReliableHandshakeData
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertReliableHandshakeData()
	{
		UScriptStruct::DeferCppStructOps<FConcertReliableHandshakeData>(FName(TEXT("ConcertReliableHandshakeData")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertReliableHandshakeData;
	struct Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_HandshakeState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandshakeState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_HandshakeState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReliableChannelId_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ReliableChannelId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NextMessageIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_NextMessageIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointTimeoutTick_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_EndpointTimeoutTick;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Handshake used to negotiate a reliable channel between endpoints (also uses the ReliableChannelId from the base message) */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Handshake used to negotiate a reliable channel between endpoints (also uses the ReliableChannelId from the base message)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertReliableHandshakeData>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_HandshakeState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_HandshakeState_MetaData[] = {
		{ "Comment", "/** State of the handshake */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "State of the handshake" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_HandshakeState = { "HandshakeState", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertReliableHandshakeData, HandshakeState), Z_Construct_UEnum_ConcertTransport_EConcertReliableHandshakeState, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_HandshakeState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_HandshakeState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_ReliableChannelId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Channel ID we're going to send reliable messages of */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Channel ID we're going to send reliable messages of" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_ReliableChannelId = { "ReliableChannelId", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertReliableHandshakeData, ReliableChannelId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_ReliableChannelId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_ReliableChannelId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_NextMessageIndex_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The next message index that the remote endpoint is going to send */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "The next message index that the remote endpoint is going to send" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_NextMessageIndex = { "NextMessageIndex", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertReliableHandshakeData, NextMessageIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_NextMessageIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_NextMessageIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_EndpointTimeoutTick_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** It's a timespan encoded in ticks. EndpointTimeoutTick represent the time it takes for the sending endpoint to consider another endpoint timed out */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "It's a timespan encoded in ticks. EndpointTimeoutTick represent the time it takes for the sending endpoint to consider another endpoint timed out" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_EndpointTimeoutTick = { "EndpointTimeoutTick", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertReliableHandshakeData, EndpointTimeoutTick), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_EndpointTimeoutTick_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_EndpointTimeoutTick_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_HandshakeState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_HandshakeState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_ReliableChannelId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_NextMessageIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::NewProp_EndpointTimeoutTick,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent,
		&NewStructOps,
		"ConcertReliableHandshakeData",
		sizeof(FConcertReliableHandshakeData),
		alignof(FConcertReliableHandshakeData),
		Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertReliableHandshakeData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertReliableHandshakeData"), sizeof(FConcertReliableHandshakeData), Get_Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertReliableHandshakeData_Hash() { return 752353030U; }

static_assert(std::is_polymorphic<FConcertEndpointClosedData>() == std::is_polymorphic<FConcertMessageData>(), "USTRUCT FConcertEndpointClosedData cannot be polymorphic unless super FConcertMessageData is polymorphic");

class UScriptStruct* FConcertEndpointClosedData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointClosedData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertEndpointClosedData, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertEndpointClosedData"), sizeof(FConcertEndpointClosedData), Get_Z_Construct_UScriptStruct_FConcertEndpointClosedData_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertEndpointClosedData>()
{
	return FConcertEndpointClosedData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertEndpointClosedData(FConcertEndpointClosedData::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertEndpointClosedData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointClosedData
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointClosedData()
	{
		UScriptStruct::DeferCppStructOps<FConcertEndpointClosedData>(FName(TEXT("ConcertEndpointClosedData")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointClosedData;
	struct Z_Construct_UScriptStruct_FConcertEndpointClosedData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEndpointClosedData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Message send when an endpoint is closed on a remote peer */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Message send when an endpoint is closed on a remote peer" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertEndpointClosedData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertEndpointClosedData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertEndpointClosedData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertMessageData,
		&NewStructOps,
		"ConcertEndpointClosedData",
		sizeof(FConcertEndpointClosedData),
		alignof(FConcertEndpointClosedData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEndpointClosedData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointClosedData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointClosedData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointClosedData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertEndpointClosedData"), sizeof(FConcertEndpointClosedData), Get_Z_Construct_UScriptStruct_FConcertEndpointClosedData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertEndpointClosedData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointClosedData_Hash() { return 2314694905U; }

static_assert(std::is_polymorphic<FConcertEndpointDiscoveryEvent>() == std::is_polymorphic<FConcertEventData>(), "USTRUCT FConcertEndpointDiscoveryEvent cannot be polymorphic unless super FConcertEventData is polymorphic");

class UScriptStruct* FConcertEndpointDiscoveryEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertEndpointDiscoveryEvent"), sizeof(FConcertEndpointDiscoveryEvent), Get_Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertEndpointDiscoveryEvent>()
{
	return FConcertEndpointDiscoveryEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertEndpointDiscoveryEvent(FConcertEndpointDiscoveryEvent::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertEndpointDiscoveryEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointDiscoveryEvent
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointDiscoveryEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertEndpointDiscoveryEvent>(FName(TEXT("ConcertEndpointDiscoveryEvent")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointDiscoveryEvent;
	struct Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ConcertProtocolVersion_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConcertProtocolVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConcertProtocolVersion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Special event message base struct that is also caught by the endpoint to discover remote endpoint before passing it to handlers */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Special event message base struct that is also caught by the endpoint to discover remote endpoint before passing it to handlers" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertEndpointDiscoveryEvent>();
	}
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewProp_ConcertProtocolVersion_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewProp_ConcertProtocolVersion_MetaData[] = {
		{ "Comment", "/** Holds the concert messages protocol version, default initialize to `BeforeVersioning` to handle message sent from older protocol. */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Holds the concert messages protocol version, default initialize to `BeforeVersioning` to handle message sent from older protocol." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewProp_ConcertProtocolVersion = { "ConcertProtocolVersion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertEndpointDiscoveryEvent, ConcertProtocolVersion), Z_Construct_UEnum_ConcertTransport_EConcertMessageVersion, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewProp_ConcertProtocolVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewProp_ConcertProtocolVersion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewProp_ConcertProtocolVersion_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::NewProp_ConcertProtocolVersion,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertEventData,
		&NewStructOps,
		"ConcertEndpointDiscoveryEvent",
		sizeof(FConcertEndpointDiscoveryEvent),
		alignof(FConcertEndpointDiscoveryEvent),
		Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertEndpointDiscoveryEvent"), sizeof(FConcertEndpointDiscoveryEvent), Get_Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent_Hash() { return 2924804830U; }

static_assert(std::is_polymorphic<FConcertResponseData>() == std::is_polymorphic<FConcertMessageData>(), "USTRUCT FConcertResponseData cannot be polymorphic unless super FConcertMessageData is polymorphic");

class UScriptStruct* FConcertResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertResponseData, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertResponseData"), sizeof(FConcertResponseData), Get_Z_Construct_UScriptStruct_FConcertResponseData_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertResponseData>()
{
	return FConcertResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertResponseData(FConcertResponseData::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertResponseData
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertResponseData()
	{
		UScriptStruct::DeferCppStructOps<FConcertResponseData>(FName(TEXT("ConcertResponseData")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertResponseData;
	struct Z_Construct_UScriptStruct_FConcertResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequestMessageId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RequestMessageId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ResponseCode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResponseCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ResponseCode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Reason_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Reason;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResponseData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base struct for all concert request response messages */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Base struct for all concert request response messages" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_RequestMessageId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** ID of the request message we're responding to */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "ID of the request message we're responding to" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_RequestMessageId = { "RequestMessageId", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResponseData, RequestMessageId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_RequestMessageId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_RequestMessageId_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_ResponseCode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_ResponseCode_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Response code for the response */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Response code for the response" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_ResponseCode = { "ResponseCode", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResponseData, ResponseCode), Z_Construct_UEnum_ConcertTransport_EConcertResponseCode, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_ResponseCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_ResponseCode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_Reason_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** If the code isn't successful, a reason for it */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "If the code isn't successful, a reason for it" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_Reason = { "Reason", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResponseData, Reason), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_Reason_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_Reason_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_RequestMessageId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_ResponseCode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_ResponseCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResponseData_Statics::NewProp_Reason,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertMessageData,
		&NewStructOps,
		"ConcertResponseData",
		sizeof(FConcertResponseData),
		alignof(FConcertResponseData),
		Z_Construct_UScriptStruct_FConcertResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertResponseData"), sizeof(FConcertResponseData), Get_Z_Construct_UScriptStruct_FConcertResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertResponseData_Hash() { return 2660843342U; }

static_assert(std::is_polymorphic<FConcertRequestData>() == std::is_polymorphic<FConcertMessageData>(), "USTRUCT FConcertRequestData cannot be polymorphic unless super FConcertMessageData is polymorphic");

class UScriptStruct* FConcertRequestData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertRequestData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertRequestData, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertRequestData"), sizeof(FConcertRequestData), Get_Z_Construct_UScriptStruct_FConcertRequestData_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertRequestData>()
{
	return FConcertRequestData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertRequestData(FConcertRequestData::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertRequestData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertRequestData
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertRequestData()
	{
		UScriptStruct::DeferCppStructOps<FConcertRequestData>(FName(TEXT("ConcertRequestData")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertRequestData;
	struct Z_Construct_UScriptStruct_FConcertRequestData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertRequestData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base struct for all concert request messages */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Base struct for all concert request messages" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertRequestData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertRequestData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertRequestData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertMessageData,
		&NewStructOps,
		"ConcertRequestData",
		sizeof(FConcertRequestData),
		alignof(FConcertRequestData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertRequestData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertRequestData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertRequestData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertRequestData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertRequestData"), sizeof(FConcertRequestData), Get_Z_Construct_UScriptStruct_FConcertRequestData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertRequestData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertRequestData_Hash() { return 1764879991U; }

static_assert(std::is_polymorphic<FConcertEventData>() == std::is_polymorphic<FConcertMessageData>(), "USTRUCT FConcertEventData cannot be polymorphic unless super FConcertMessageData is polymorphic");

class UScriptStruct* FConcertEventData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertEventData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertEventData, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertEventData"), sizeof(FConcertEventData), Get_Z_Construct_UScriptStruct_FConcertEventData_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertEventData>()
{
	return FConcertEventData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertEventData(FConcertEventData::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertEventData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEventData
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEventData()
	{
		UScriptStruct::DeferCppStructOps<FConcertEventData>(FName(TEXT("ConcertEventData")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEventData;
	struct Z_Construct_UScriptStruct_FConcertEventData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEventData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base struct for all concert event messages */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Base struct for all concert event messages" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertEventData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertEventData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertEventData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		Z_Construct_UScriptStruct_FConcertMessageData,
		&NewStructOps,
		"ConcertEventData",
		sizeof(FConcertEventData),
		alignof(FConcertEventData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEventData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEventData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertEventData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertEventData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertEventData"), sizeof(FConcertEventData), Get_Z_Construct_UScriptStruct_FConcertEventData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertEventData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertEventData_Hash() { return 1831229383U; }
class UScriptStruct* FConcertMessageData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertMessageData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertMessageData, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertMessageData"), sizeof(FConcertMessageData), Get_Z_Construct_UScriptStruct_FConcertMessageData_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertMessageData>()
{
	return FConcertMessageData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertMessageData(FConcertMessageData::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertMessageData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertMessageData
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertMessageData()
	{
		UScriptStruct::DeferCppStructOps<FConcertMessageData>(FName(TEXT("ConcertMessageData")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertMessageData;
	struct Z_Construct_UScriptStruct_FConcertMessageData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConcertEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConcertEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MessageId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageOrderIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_MessageOrderIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelId_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ChannelId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMessageData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base class for all message data sent through concert */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Base class for all message data sent through concert" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertMessageData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ConcertEndpointId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** ID of the Endpoint this was sent from */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "ID of the Endpoint this was sent from" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ConcertEndpointId = { "ConcertEndpointId", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertMessageData, ConcertEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ConcertEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ConcertEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** ID of the message */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "ID of the message" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageId = { "MessageId", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertMessageData, MessageId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageOrderIndex_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Order index of the message (for ordering reliable messages, used when ChannelId != UnreliableChannelId) */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "Order index of the message (for ordering reliable messages, used when ChannelId != UnreliableChannelId)" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageOrderIndex = { "MessageOrderIndex", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertMessageData, MessageOrderIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageOrderIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageOrderIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ChannelId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** ID of the channel this message was sent from */" },
		{ "ModuleRelativePath", "Public/ConcertTransportMessages.h" },
		{ "ToolTip", "ID of the channel this message was sent from" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ChannelId = { "ChannelId", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertMessageData, ChannelId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ChannelId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ChannelId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertMessageData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ConcertEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_MessageOrderIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertMessageData_Statics::NewProp_ChannelId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertMessageData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		nullptr,
		&NewStructOps,
		"ConcertMessageData",
		sizeof(FConcertMessageData),
		alignof(FConcertMessageData),
		Z_Construct_UScriptStruct_FConcertMessageData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMessageData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertMessageData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertMessageData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertMessageData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertMessageData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertMessageData"), sizeof(FConcertMessageData), Get_Z_Construct_UScriptStruct_FConcertMessageData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertMessageData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertMessageData_Hash() { return 1896791216U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
