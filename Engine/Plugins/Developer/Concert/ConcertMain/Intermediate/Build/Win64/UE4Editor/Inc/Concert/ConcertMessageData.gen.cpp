// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Concert/Public/ConcertMessageData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertMessageData() {}
// Cross Module References
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertPayloadSerializationMethod();
	UPackage* Z_Construct_UPackage__Script_Concert();
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertPayloadCompressionType();
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertServerFlags();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSerializedPayload();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertByteArray();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionFilter();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionInfo();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSettings();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionVersionInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionClientInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertInstanceInfo();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertServerInfo();
// End Cross Module References
	static UEnum* EConcertPayloadSerializationMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertPayloadSerializationMethod, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertPayloadSerializationMethod"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertPayloadSerializationMethod>()
	{
		return EConcertPayloadSerializationMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertPayloadSerializationMethod(EConcertPayloadSerializationMethod_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertPayloadSerializationMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertPayloadSerializationMethod_Hash() { return 1978278735U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertPayloadSerializationMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertPayloadSerializationMethod"), 0, Get_Z_Construct_UEnum_Concert_EConcertPayloadSerializationMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertPayloadSerializationMethod::Standard", (int64)EConcertPayloadSerializationMethod::Standard },
				{ "EConcertPayloadSerializationMethod::Cbor", (int64)EConcertPayloadSerializationMethod::Cbor },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Cbor.Comment", "// The data will be serialized using Cbor method.\n" },
				{ "Cbor.Name", "EConcertPayloadSerializationMethod::Cbor" },
				{ "Cbor.ToolTip", "The data will be serialized using Cbor method." },
				{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
				{ "Standard.Comment", "// The data will be serialized using standard platform method.\n" },
				{ "Standard.Name", "EConcertPayloadSerializationMethod::Standard" },
				{ "Standard.ToolTip", "The data will be serialized using standard platform method." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertPayloadSerializationMethod",
				"EConcertPayloadSerializationMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertPayloadCompressionType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertPayloadCompressionType, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertPayloadCompressionType"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertPayloadCompressionType>()
	{
		return EConcertPayloadCompressionType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertPayloadCompressionType(EConcertPayloadCompressionType_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertPayloadCompressionType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertPayloadCompressionType_Hash() { return 1444063481U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertPayloadCompressionType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertPayloadCompressionType"), 0, Get_Z_Construct_UEnum_Concert_EConcertPayloadCompressionType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertPayloadCompressionType::None", (int64)EConcertPayloadCompressionType::None },
				{ "EConcertPayloadCompressionType::Heuristic", (int64)EConcertPayloadCompressionType::Heuristic },
				{ "EConcertPayloadCompressionType::Always", (int64)EConcertPayloadCompressionType::Always },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Always.Comment", "// The serialized data will always be compressed.\n" },
				{ "Always.Name", "EConcertPayloadCompressionType::Always" },
				{ "Always.ToolTip", "The serialized data will always be compressed." },
				{ "Heuristic.Comment", "// The serialized data will be compressed based on struct size.\n" },
				{ "Heuristic.Name", "EConcertPayloadCompressionType::Heuristic" },
				{ "Heuristic.ToolTip", "The serialized data will be compressed based on struct size." },
				{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
				{ "None.Comment", "// The serialized data will not be compressed.\n" },
				{ "None.Name", "EConcertPayloadCompressionType::None" },
				{ "None.ToolTip", "The serialized data will not be compressed." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertPayloadCompressionType",
				"EConcertPayloadCompressionType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertServerFlags_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertServerFlags, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertServerFlags"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertServerFlags>()
	{
		return EConcertServerFlags_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertServerFlags(EConcertServerFlags_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertServerFlags"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertServerFlags_Hash() { return 3718054934U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertServerFlags()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertServerFlags"), 0, Get_Z_Construct_UEnum_Concert_EConcertServerFlags_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertServerFlags::None", (int64)EConcertServerFlags::None },
				{ "EConcertServerFlags::IgnoreSessionRequirement", (int64)EConcertServerFlags::IgnoreSessionRequirement },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "IgnoreSessionRequirement.Comment", "//The server will ignore the session requirement when someone try to join a session\n" },
				{ "IgnoreSessionRequirement.Name", "EConcertServerFlags::IgnoreSessionRequirement" },
				{ "IgnoreSessionRequirement.ToolTip", "The server will ignore the session requirement when someone try to join a session" },
				{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
				{ "None.Name", "EConcertServerFlags::None" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertServerFlags",
				"EConcertServerFlags",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertSessionSerializedPayload::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSessionSerializedPayload"), sizeof(FConcertSessionSerializedPayload), Get_Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSessionSerializedPayload>()
{
	return FConcertSessionSerializedPayload::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSessionSerializedPayload(FConcertSessionSerializedPayload::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSessionSerializedPayload"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSessionSerializedPayload
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSessionSerializedPayload()
	{
		UScriptStruct::DeferCppStructOps<FConcertSessionSerializedPayload>(FName(TEXT("ConcertSessionSerializedPayload")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSessionSerializedPayload;
	struct Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PayloadTypeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PayloadTypeName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SerializationMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializationMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SerializationMethod;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPayloadIsCompressed_MetaData[];
#endif
		static void NewProp_bPayloadIsCompressed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPayloadIsCompressed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PayloadSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PayloadSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PayloadBytes_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PayloadBytes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSessionSerializedPayload>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadTypeName_MetaData[] = {
		{ "Category", "Payload" },
		{ "Comment", "/** The typename of the user-defined payload. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "The typename of the user-defined payload." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadTypeName = { "PayloadTypeName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionSerializedPayload, PayloadTypeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadTypeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadTypeName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_SerializationMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_SerializationMethod_MetaData[] = {
		{ "Category", "Payload" },
		{ "Comment", "/** Specifies the serialization method used to pack the data */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Specifies the serialization method used to pack the data" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_SerializationMethod = { "SerializationMethod", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionSerializedPayload, SerializationMethod), Z_Construct_UEnum_Concert_EConcertPayloadSerializationMethod, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_SerializationMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_SerializationMethod_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_bPayloadIsCompressed_MetaData[] = {
		{ "Category", "Payload" },
		{ "Comment", "/** Indicates if the serialized payload has been compressed. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Indicates if the serialized payload has been compressed." },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_bPayloadIsCompressed_SetBit(void* Obj)
	{
		((FConcertSessionSerializedPayload*)Obj)->bPayloadIsCompressed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_bPayloadIsCompressed = { "bPayloadIsCompressed", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSessionSerializedPayload), &Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_bPayloadIsCompressed_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_bPayloadIsCompressed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_bPayloadIsCompressed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadSize_MetaData[] = {
		{ "Category", "Payload" },
		{ "Comment", "/** The uncompressed size of the user-defined payload data. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "The uncompressed size of the user-defined payload data." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadSize = { "PayloadSize", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionSerializedPayload, PayloadSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadBytes_MetaData[] = {
		{ "Comment", "/** The data of the user-defined payload (potentially stored as compressed binary for compact transfer). */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "The data of the user-defined payload (potentially stored as compressed binary for compact transfer)." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadBytes = { "PayloadBytes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionSerializedPayload, PayloadBytes), Z_Construct_UScriptStruct_FConcertByteArray, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadBytes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadBytes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadTypeName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_SerializationMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_SerializationMethod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_bPayloadIsCompressed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::NewProp_PayloadBytes,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSessionSerializedPayload",
		sizeof(FConcertSessionSerializedPayload),
		alignof(FConcertSessionSerializedPayload),
		Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSerializedPayload()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSessionSerializedPayload"), sizeof(FConcertSessionSerializedPayload), Get_Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Hash() { return 3033157727U; }
class UScriptStruct* FConcertByteArray::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertByteArray_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertByteArray, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertByteArray"), sizeof(FConcertByteArray), Get_Z_Construct_UScriptStruct_FConcertByteArray_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertByteArray>()
{
	return FConcertByteArray::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertByteArray(FConcertByteArray::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertByteArray"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertByteArray
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertByteArray()
	{
		UScriptStruct::DeferCppStructOps<FConcertByteArray>(FName(TEXT("ConcertByteArray")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertByteArray;
	struct Z_Construct_UScriptStruct_FConcertByteArray_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Bytes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bytes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Bytes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertByteArray_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertByteArray>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewProp_Bytes_Inner = { "Bytes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewProp_Bytes_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewProp_Bytes = { "Bytes", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertByteArray, Bytes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewProp_Bytes_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewProp_Bytes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertByteArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewProp_Bytes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertByteArray_Statics::NewProp_Bytes,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertByteArray_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertByteArray",
		sizeof(FConcertByteArray),
		alignof(FConcertByteArray),
		Z_Construct_UScriptStruct_FConcertByteArray_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertByteArray_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertByteArray_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertByteArray_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertByteArray()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertByteArray_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertByteArray"), sizeof(FConcertByteArray), Get_Z_Construct_UScriptStruct_FConcertByteArray_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertByteArray_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertByteArray_Hash() { return 1817587954U; }
class UScriptStruct* FConcertSessionFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSessionFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSessionFilter, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSessionFilter"), sizeof(FConcertSessionFilter), Get_Z_Construct_UScriptStruct_FConcertSessionFilter_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSessionFilter>()
{
	return FConcertSessionFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSessionFilter(FConcertSessionFilter::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSessionFilter"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSessionFilter
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSessionFilter()
	{
		UScriptStruct::DeferCppStructOps<FConcertSessionFilter>(FName(TEXT("ConcertSessionFilter")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSessionFilter;
	struct Z_Construct_UScriptStruct_FConcertSessionFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityIdLowerBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_ActivityIdLowerBound;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityIdUpperBound_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_ActivityIdUpperBound;
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_ActivityIdsToExclude_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityIdsToExclude_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActivityIdsToExclude;
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_ActivityIdsToInclude_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityIdsToInclude_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActivityIdsToInclude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyLiveData_MetaData[];
#endif
		static void NewProp_bOnlyLiveData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyLiveData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMetaDataOnly_MetaData[];
#endif
		static void NewProp_bMetaDataOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMetaDataOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeIgnoredActivities_MetaData[];
#endif
		static void NewProp_bIncludeIgnoredActivities_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeIgnoredActivities;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds filter rules used when migrating session data */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds filter rules used when migrating session data" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSessionFilter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdLowerBound_MetaData[] = {
		{ "Comment", "/** The lower-bound (inclusive) of activity IDs to include (unless explicitly excluded via ActivityIdsToExclude) */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "The lower-bound (inclusive) of activity IDs to include (unless explicitly excluded via ActivityIdsToExclude)" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdLowerBound = { "ActivityIdLowerBound", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionFilter, ActivityIdLowerBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdLowerBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdLowerBound_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdUpperBound_MetaData[] = {
		{ "Comment", "/** The upper-bound (inclusive) of activity IDs to include (unless explicitly excluded via ActivityIdsToExclude) */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "The upper-bound (inclusive) of activity IDs to include (unless explicitly excluded via ActivityIdsToExclude)" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdUpperBound = { "ActivityIdUpperBound", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionFilter, ActivityIdUpperBound), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdUpperBound_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdUpperBound_MetaData)) };
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToExclude_Inner = { "ActivityIdsToExclude", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToExclude_MetaData[] = {
		{ "Comment", "/** Activity IDs to explicitly exclude, even if inside of the bounded-range specified above */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Activity IDs to explicitly exclude, even if inside of the bounded-range specified above" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToExclude = { "ActivityIdsToExclude", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionFilter, ActivityIdsToExclude), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToExclude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToExclude_MetaData)) };
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToInclude_Inner = { "ActivityIdsToInclude", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToInclude_MetaData[] = {
		{ "Comment", "/** Activity IDs to explicitly include, even if outside of the bounded-range specified above (takes precedence over ActivityIdsToExclude) */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Activity IDs to explicitly include, even if outside of the bounded-range specified above (takes precedence over ActivityIdsToExclude)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToInclude = { "ActivityIdsToInclude", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionFilter, ActivityIdsToInclude), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToInclude_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToInclude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bOnlyLiveData_MetaData[] = {
		{ "Comment", "/** True if only live data should be included (live transactions and head package revisions) */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "True if only live data should be included (live transactions and head package revisions)" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bOnlyLiveData_SetBit(void* Obj)
	{
		((FConcertSessionFilter*)Obj)->bOnlyLiveData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bOnlyLiveData = { "bOnlyLiveData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSessionFilter), &Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bOnlyLiveData_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bOnlyLiveData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bOnlyLiveData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bMetaDataOnly_MetaData[] = {
		{ "Comment", "/** True to export the activity summaries without the package/transaction data to look at the log rather than replaying the activities. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "True to export the activity summaries without the package/transaction data to look at the log rather than replaying the activities." },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bMetaDataOnly_SetBit(void* Obj)
	{
		((FConcertSessionFilter*)Obj)->bMetaDataOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bMetaDataOnly = { "bMetaDataOnly", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSessionFilter), &Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bMetaDataOnly_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bMetaDataOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bMetaDataOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bIncludeIgnoredActivities_MetaData[] = {
		{ "Comment", "/** True to include ignored activities */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "True to include ignored activities" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bIncludeIgnoredActivities_SetBit(void* Obj)
	{
		((FConcertSessionFilter*)Obj)->bIncludeIgnoredActivities = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bIncludeIgnoredActivities = { "bIncludeIgnoredActivities", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSessionFilter), &Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bIncludeIgnoredActivities_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bIncludeIgnoredActivities_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bIncludeIgnoredActivities_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdLowerBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdUpperBound,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToExclude_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToExclude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToInclude_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_ActivityIdsToInclude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bOnlyLiveData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bMetaDataOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::NewProp_bIncludeIgnoredActivities,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSessionFilter",
		sizeof(FConcertSessionFilter),
		alignof(FConcertSessionFilter),
		Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSessionFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSessionFilter"), sizeof(FConcertSessionFilter), Get_Z_Construct_UScriptStruct_FConcertSessionFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSessionFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSessionFilter_Hash() { return 3856281397U; }
class UScriptStruct* FConcertSessionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSessionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSessionInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSessionInfo"), sizeof(FConcertSessionInfo), Get_Z_Construct_UScriptStruct_FConcertSessionInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSessionInfo>()
{
	return FConcertSessionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSessionInfo(FConcertSessionInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSessionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSessionInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSessionInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertSessionInfo>(FName(TEXT("ConcertSessionInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSessionInfo;
	struct Z_Construct_UScriptStruct_FConcertSessionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerInstanceId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ServerInstanceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ServerEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerInstanceId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerInstanceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerUserName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OwnerUserName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerDeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OwnerDeviceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Settings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VersionInfos_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VersionInfos_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VersionInfos;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds info on a session */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds info on a session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSessionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerInstanceId_MetaData[] = {
		{ "Category", "Session Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerInstanceId = { "ServerInstanceId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, ServerInstanceId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerInstanceId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerInstanceId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerEndpointId_MetaData[] = {
		{ "Category", "Session Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerEndpointId = { "ServerEndpointId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, ServerEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerInstanceId_MetaData[] = {
		{ "Category", "Session Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerInstanceId = { "OwnerInstanceId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, OwnerInstanceId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerInstanceId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerInstanceId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Session Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionName_MetaData[] = {
		{ "Category", "Session Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionName = { "SessionName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, SessionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerUserName_MetaData[] = {
		{ "Category", "Session Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerUserName = { "OwnerUserName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, OwnerUserName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerUserName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerUserName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerDeviceName_MetaData[] = {
		{ "Category", "Session Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerDeviceName = { "OwnerDeviceName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, OwnerDeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerDeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerDeviceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_Settings_MetaData[] = {
		{ "Category", "Session Info" },
		{ "Comment", "/** Settings pertaining to project, change list number etc */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Settings pertaining to project, change list number etc" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, Settings), Z_Construct_UScriptStruct_FConcertSessionSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_Settings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_VersionInfos_Inner = { "VersionInfos", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionVersionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_VersionInfos_MetaData[] = {
		{ "Comment", "/** Version information for this session. This is set during creation, and updated each time the session is restored */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Version information for this session. This is set during creation, and updated each time the session is restored" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_VersionInfos = { "VersionInfos", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionInfo, VersionInfos), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_VersionInfos_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_VersionInfos_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerInstanceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_ServerEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerInstanceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_SessionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerUserName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_OwnerDeviceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_VersionInfos_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::NewProp_VersionInfos,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSessionInfo",
		sizeof(FConcertSessionInfo),
		alignof(FConcertSessionInfo),
		Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSessionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSessionInfo"), sizeof(FConcertSessionInfo), Get_Z_Construct_UScriptStruct_FConcertSessionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSessionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSessionInfo_Hash() { return 4055765697U; }
class UScriptStruct* FConcertSessionClientInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSessionClientInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSessionClientInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSessionClientInfo"), sizeof(FConcertSessionClientInfo), Get_Z_Construct_UScriptStruct_FConcertSessionClientInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSessionClientInfo>()
{
	return FConcertSessionClientInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSessionClientInfo(FConcertSessionClientInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSessionClientInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSessionClientInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSessionClientInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertSessionClientInfo>(FName(TEXT("ConcertSessionClientInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSessionClientInfo;
	struct Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds information on session client */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds information on session client" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSessionClientInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientEndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientEndpointId = { "ClientEndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionClientInfo, ClientEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientInfo_MetaData[] = {
		{ "Category", "Client Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientInfo = { "ClientInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionClientInfo, ClientInfo), Z_Construct_UScriptStruct_FConcertClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::NewProp_ClientInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSessionClientInfo",
		sizeof(FConcertSessionClientInfo),
		alignof(FConcertSessionClientInfo),
		Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionClientInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSessionClientInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSessionClientInfo"), sizeof(FConcertSessionClientInfo), Get_Z_Construct_UScriptStruct_FConcertSessionClientInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSessionClientInfo_Hash() { return 2444943749U; }
class UScriptStruct* FConcertClientInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertClientInfo"), sizeof(FConcertClientInfo), Get_Z_Construct_UScriptStruct_FConcertClientInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertClientInfo>()
{
	return FConcertClientInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientInfo(FConcertClientInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertClientInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertClientInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertClientInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientInfo>(FName(TEXT("ConcertClientInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertClientInfo;
	struct Z_Construct_UScriptStruct_FConcertClientInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstanceInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InstanceInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlatformName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PlatformName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AvatarColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AvatarColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesktopAvatarActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DesktopAvatarActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRAvatarActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_VRAvatarActorClass;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Tags_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tags_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Tags;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasEditorData_MetaData[];
#endif
		static void NewProp_bHasEditorData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasEditorData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRequiresCookedData_MetaData[];
#endif
		static void NewProp_bRequiresCookedData_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRequiresCookedData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds info on a client connected through concert */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds info on a client connected through concert" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_InstanceInfo_MetaData[] = {
		{ "Category", "Client Info" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_InstanceInfo = { "InstanceInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, InstanceInfo), Z_Construct_UScriptStruct_FConcertInstanceInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_InstanceInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_InstanceInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds the name of the device that the instance is running on. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the name of the device that the instance is running on." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, DeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DeviceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_PlatformName_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds the name of the platform that the instance is running on. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the name of the platform that the instance is running on." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_PlatformName = { "PlatformName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, PlatformName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_PlatformName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_PlatformName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_UserName_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds the name of the user that owns this instance. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the name of the user that owns this instance." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_UserName = { "UserName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, UserName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_UserName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_UserName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds the display name of the user that owns this instance. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the display name of the user that owns this instance." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_AvatarColor_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds the color of the user avatar in a session. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the color of the user avatar in a session." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_AvatarColor = { "AvatarColor", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, AvatarColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_AvatarColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_AvatarColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DesktopAvatarActorClass_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds the string representation of the desktop actor class to be used as the avatar for a representation of a client */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the string representation of the desktop actor class to be used as the avatar for a representation of a client" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DesktopAvatarActorClass = { "DesktopAvatarActorClass", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, DesktopAvatarActorClass), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DesktopAvatarActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DesktopAvatarActorClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_VRAvatarActorClass_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds the string representation of the VR actor class to be used as the avatar for a representation of a client */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the string representation of the VR actor class to be used as the avatar for a representation of a client" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_VRAvatarActorClass = { "VRAvatarActorClass", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, VRAvatarActorClass), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_VRAvatarActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_VRAvatarActorClass_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_Tags_Inner = { "Tags", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_Tags_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** Holds an array of tags that can be used for grouping and categorizing. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds an array of tags that can be used for grouping and categorizing." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_Tags = { "Tags", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientInfo, Tags), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_Tags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_Tags_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bHasEditorData_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** True if this instance was built with editor-data */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "True if this instance was built with editor-data" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bHasEditorData_SetBit(void* Obj)
	{
		((FConcertClientInfo*)Obj)->bHasEditorData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bHasEditorData = { "bHasEditorData", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertClientInfo), &Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bHasEditorData_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bHasEditorData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bHasEditorData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bRequiresCookedData_MetaData[] = {
		{ "Category", "Client Info" },
		{ "Comment", "/** True if this platform requires cooked data */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "True if this platform requires cooked data" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bRequiresCookedData_SetBit(void* Obj)
	{
		((FConcertClientInfo*)Obj)->bRequiresCookedData = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bRequiresCookedData = { "bRequiresCookedData", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertClientInfo), &Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bRequiresCookedData_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bRequiresCookedData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bRequiresCookedData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_InstanceInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DeviceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_PlatformName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_UserName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_AvatarColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_DesktopAvatarActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_VRAvatarActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_Tags_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_Tags,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bHasEditorData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientInfo_Statics::NewProp_bRequiresCookedData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertClientInfo",
		sizeof(FConcertClientInfo),
		alignof(FConcertClientInfo),
		Z_Construct_UScriptStruct_FConcertClientInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientInfo"), sizeof(FConcertClientInfo), Get_Z_Construct_UScriptStruct_FConcertClientInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientInfo_Hash() { return 1008895721U; }
class UScriptStruct* FConcertServerInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertServerInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertServerInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertServerInfo"), sizeof(FConcertServerInfo), Get_Z_Construct_UScriptStruct_FConcertServerInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertServerInfo>()
{
	return FConcertServerInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertServerInfo(FConcertServerInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertServerInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertServerInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertServerInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertServerInfo>(FName(TEXT("ConcertServerInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertServerInfo;
	struct Z_Construct_UScriptStruct_FConcertServerInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdminEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdminEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ServerName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstanceInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InstanceInfo;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ServerFlags_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerFlags_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ServerFlags;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds info on a Concert server */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds info on a Concert server" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertServerInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_AdminEndpointId_MetaData[] = {
		{ "Comment", "/** Server endpoint for performing administration tasks (FConcertAdmin_X messages) */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Server endpoint for performing administration tasks (FConcertAdmin_X messages)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_AdminEndpointId = { "AdminEndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerInfo, AdminEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_AdminEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_AdminEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerName = { "ServerName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerInfo, ServerName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_InstanceInfo_MetaData[] = {
		{ "Category", "Server Info" },
		{ "Comment", "/** Basic server information */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Basic server information" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_InstanceInfo = { "InstanceInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerInfo, InstanceInfo), Z_Construct_UScriptStruct_FConcertInstanceInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_InstanceInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_InstanceInfo_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerFlags_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerFlags_MetaData[] = {
		{ "Category", "Server Info" },
		{ "Comment", "/** Contains information on the server settings */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Contains information on the server settings" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerFlags = { "ServerFlags", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerInfo, ServerFlags), Z_Construct_UEnum_Concert_EConcertServerFlags, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerFlags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerFlags_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertServerInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_AdminEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_InstanceInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerFlags_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerInfo_Statics::NewProp_ServerFlags,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertServerInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertServerInfo",
		sizeof(FConcertServerInfo),
		alignof(FConcertServerInfo),
		Z_Construct_UScriptStruct_FConcertServerInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertServerInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertServerInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertServerInfo"), sizeof(FConcertServerInfo), Get_Z_Construct_UScriptStruct_FConcertServerInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertServerInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertServerInfo_Hash() { return 615286595U; }
class UScriptStruct* FConcertInstanceInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertInstanceInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertInstanceInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertInstanceInfo"), sizeof(FConcertInstanceInfo), Get_Z_Construct_UScriptStruct_FConcertInstanceInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertInstanceInfo>()
{
	return FConcertInstanceInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertInstanceInfo(FConcertInstanceInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertInstanceInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertInstanceInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertInstanceInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertInstanceInfo>(FName(TEXT("ConcertInstanceInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertInstanceInfo;
	struct Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstanceId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InstanceId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstanceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InstanceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstanceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InstanceType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds info on an instance communicating through concert */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds info on an instance communicating through concert" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertInstanceInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceId_MetaData[] = {
		{ "Category", "Instance Info" },
		{ "Comment", "/** Holds the instance identifier. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the instance identifier." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceId = { "InstanceId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertInstanceInfo, InstanceId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceName_MetaData[] = {
		{ "Category", "Instance Info" },
		{ "Comment", "/** Holds the instance name. */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the instance name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceName = { "InstanceName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertInstanceInfo, InstanceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceType_MetaData[] = {
		{ "Category", "Instance Info" },
		{ "Comment", "/** Holds the instance type (Editor, Game, Server, etc). */" },
		{ "ModuleRelativePath", "Public/ConcertMessageData.h" },
		{ "ToolTip", "Holds the instance type (Editor, Game, Server, etc)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceType = { "InstanceType", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertInstanceInfo, InstanceType), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::NewProp_InstanceType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertInstanceInfo",
		sizeof(FConcertInstanceInfo),
		alignof(FConcertInstanceInfo),
		Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertInstanceInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertInstanceInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertInstanceInfo"), sizeof(FConcertInstanceInfo), Get_Z_Construct_UScriptStruct_FConcertInstanceInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertInstanceInfo_Hash() { return 2346175387U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
