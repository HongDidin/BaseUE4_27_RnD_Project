// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertTransport/Public/IdentifierTable/ConcertIdentifierTableData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertIdentifierTableData() {}
// Cross Module References
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertLocalIdentifierState();
	UPackage* Z_Construct_UPackage__Script_ConcertTransport();
// End Cross Module References
class UScriptStruct* FConcertLocalIdentifierState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertLocalIdentifierState, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertLocalIdentifierState"), sizeof(FConcertLocalIdentifierState), Get_Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertLocalIdentifierState>()
{
	return FConcertLocalIdentifierState::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertLocalIdentifierState(FConcertLocalIdentifierState::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertLocalIdentifierState"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertLocalIdentifierState
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertLocalIdentifierState()
	{
		UScriptStruct::DeferCppStructOps<FConcertLocalIdentifierState>(FName(TEXT("ConcertLocalIdentifierState")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertLocalIdentifierState;
	struct Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MappedNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MappedNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MappedNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/IdentifierTable/ConcertIdentifierTableData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertLocalIdentifierState>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewProp_MappedNames_Inner = { "MappedNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewProp_MappedNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/IdentifierTable/ConcertIdentifierTableData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewProp_MappedNames = { "MappedNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLocalIdentifierState, MappedNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewProp_MappedNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewProp_MappedNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewProp_MappedNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::NewProp_MappedNames,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		nullptr,
		&NewStructOps,
		"ConcertLocalIdentifierState",
		sizeof(FConcertLocalIdentifierState),
		alignof(FConcertLocalIdentifierState),
		Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertLocalIdentifierState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertLocalIdentifierState"), sizeof(FConcertLocalIdentifierState), Get_Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertLocalIdentifierState_Hash() { return 4132474181U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
