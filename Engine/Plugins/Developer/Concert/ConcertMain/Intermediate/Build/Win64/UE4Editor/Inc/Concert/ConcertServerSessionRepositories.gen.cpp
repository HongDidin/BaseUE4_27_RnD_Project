// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Concert/Private/ConcertServerSessionRepositories.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertServerSessionRepositories() {}
// Cross Module References
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase();
	UPackage* Z_Construct_UPackage__Script_Concert();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertServerSessionRepository();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FConcertServerSessionRepositoryDatabase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertServerSessionRepositoryDatabase"), sizeof(FConcertServerSessionRepositoryDatabase), Get_Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertServerSessionRepositoryDatabase>()
{
	return FConcertServerSessionRepositoryDatabase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertServerSessionRepositoryDatabase(FConcertServerSessionRepositoryDatabase::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertServerSessionRepositoryDatabase"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertServerSessionRepositoryDatabase
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertServerSessionRepositoryDatabase()
	{
		UScriptStruct::DeferCppStructOps<FConcertServerSessionRepositoryDatabase>(FName(TEXT("ConcertServerSessionRepositoryDatabase")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertServerSessionRepositoryDatabase;
	struct Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Repositories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Repositories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Repositories;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Tracks the session repositories across the server instances. */" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "Tracks the session repositories across the server instances." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertServerSessionRepositoryDatabase>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewProp_Repositories_Inner = { "Repositories", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertServerSessionRepository, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewProp_Repositories_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewProp_Repositories = { "Repositories", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerSessionRepositoryDatabase, Repositories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewProp_Repositories_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewProp_Repositories_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewProp_Repositories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::NewProp_Repositories,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertServerSessionRepositoryDatabase",
		sizeof(FConcertServerSessionRepositoryDatabase),
		alignof(FConcertServerSessionRepositoryDatabase),
		Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertServerSessionRepositoryDatabase"), sizeof(FConcertServerSessionRepositoryDatabase), Get_Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertServerSessionRepositoryDatabase_Hash() { return 1199623261U; }
class UScriptStruct* FConcertServerSessionRepository::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertServerSessionRepository_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertServerSessionRepository, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertServerSessionRepository"), sizeof(FConcertServerSessionRepository), Get_Z_Construct_UScriptStruct_FConcertServerSessionRepository_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertServerSessionRepository>()
{
	return FConcertServerSessionRepository::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertServerSessionRepository(FConcertServerSessionRepository::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertServerSessionRepository"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertServerSessionRepository
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertServerSessionRepository()
	{
		UScriptStruct::DeferCppStructOps<FConcertServerSessionRepository>(FName(TEXT("ConcertServerSessionRepository")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertServerSessionRepository;
	struct Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RepositoryId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryRootDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RepositoryRootDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkingDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_WorkingDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SavedDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SavedDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProcessId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ProcessId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMounted_MetaData[];
#endif
		static void NewProp_bMounted_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMounted;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Keep the information about a the session repository. */" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "Keep the information about a the session repository." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertServerSessionRepository>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryId_MetaData[] = {
		{ "Comment", "/** The repository id.*/" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "The repository id." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryId = { "RepositoryId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerSessionRepository, RepositoryId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryRootDir_MetaData[] = {
		{ "Comment", "/** The repository root directory. Can be empty is the working/archive dir are not stored in a standard repository. */" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "The repository root directory. Can be empty is the working/archive dir are not stored in a standard repository." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryRootDir = { "RepositoryRootDir", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerSessionRepository, RepositoryRootDir), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryRootDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryRootDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_WorkingDir_MetaData[] = {
		{ "Comment", "/** This repository working directory. */" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "This repository working directory." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_WorkingDir = { "WorkingDir", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerSessionRepository, WorkingDir), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_WorkingDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_WorkingDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_SavedDir_MetaData[] = {
		{ "Comment", "/** This repository saving directory. */" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "This repository saving directory." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_SavedDir = { "SavedDir", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerSessionRepository, SavedDir), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_SavedDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_SavedDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_ProcessId_MetaData[] = {
		{ "Comment", "/** The server process ID that mounted the workspace if bMounted is true. */" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "The server process ID that mounted the workspace if bMounted is true." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_ProcessId = { "ProcessId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerSessionRepository, ProcessId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_ProcessId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_ProcessId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_bMounted_MetaData[] = {
		{ "Comment", "/** Whether the workspace is mounted on a server.*/" },
		{ "ModuleRelativePath", "Private/ConcertServerSessionRepositories.h" },
		{ "ToolTip", "Whether the workspace is mounted on a server." },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_bMounted_SetBit(void* Obj)
	{
		((FConcertServerSessionRepository*)Obj)->bMounted = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_bMounted = { "bMounted", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertServerSessionRepository), &Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_bMounted_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_bMounted_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_bMounted_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_RepositoryRootDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_WorkingDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_SavedDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_ProcessId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::NewProp_bMounted,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertServerSessionRepository",
		sizeof(FConcertServerSessionRepository),
		alignof(FConcertServerSessionRepository),
		Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertServerSessionRepository()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertServerSessionRepository_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertServerSessionRepository"), sizeof(FConcertServerSessionRepository), Get_Z_Construct_UScriptStruct_FConcertServerSessionRepository_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertServerSessionRepository_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertServerSessionRepository_Hash() { return 971614851U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
