// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Concert/Public/ConcertMessages.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertMessages() {}
// Cross Module References
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertSessionRepositoryMountResponseCode();
	UPackage* Z_Construct_UPackage__Script_Concert();
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertSessionResponseCode();
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertClientStatus();
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertConnectionResult();
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertConnectionStatus();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_CustomResponse();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertResponseData();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSerializedPayload();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_CustomRequest();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertRequestData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_CustomEvent();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEventData();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionClientInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionFilter();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSettings();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionVersionInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertInstanceInfo();
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertServerFlags();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent();
// End Cross Module References
	static UEnum* EConcertSessionRepositoryMountResponseCode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertSessionRepositoryMountResponseCode, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertSessionRepositoryMountResponseCode"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertSessionRepositoryMountResponseCode>()
	{
		return EConcertSessionRepositoryMountResponseCode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertSessionRepositoryMountResponseCode(EConcertSessionRepositoryMountResponseCode_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertSessionRepositoryMountResponseCode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertSessionRepositoryMountResponseCode_Hash() { return 616605821U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertSessionRepositoryMountResponseCode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertSessionRepositoryMountResponseCode"), 0, Get_Z_Construct_UEnum_Concert_EConcertSessionRepositoryMountResponseCode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertSessionRepositoryMountResponseCode::None", (int64)EConcertSessionRepositoryMountResponseCode::None },
				{ "EConcertSessionRepositoryMountResponseCode::Mounted", (int64)EConcertSessionRepositoryMountResponseCode::Mounted },
				{ "EConcertSessionRepositoryMountResponseCode::AlreadyMounted", (int64)EConcertSessionRepositoryMountResponseCode::AlreadyMounted },
				{ "EConcertSessionRepositoryMountResponseCode::NotFound", (int64)EConcertSessionRepositoryMountResponseCode::NotFound },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlreadyMounted.Comment", "/** The repository is already mounted by another server instance. */" },
				{ "AlreadyMounted.Name", "EConcertSessionRepositoryMountResponseCode::AlreadyMounted" },
				{ "AlreadyMounted.ToolTip", "The repository is already mounted by another server instance." },
				{ "Comment", "/** Response code returned when trying to mount a session repository on the server. */" },
				{ "ModuleRelativePath", "Public/ConcertMessages.h" },
				{ "Mounted.Comment", "/** The repository was mounted on the invoked server. */" },
				{ "Mounted.Name", "EConcertSessionRepositoryMountResponseCode::Mounted" },
				{ "Mounted.ToolTip", "The repository was mounted on the invoked server." },
				{ "None.Comment", "/** No response code yet. */" },
				{ "None.Name", "EConcertSessionRepositoryMountResponseCode::None" },
				{ "None.ToolTip", "No response code yet." },
				{ "NotFound.Comment", "/** The repository ID could not be found in the server list. */" },
				{ "NotFound.Name", "EConcertSessionRepositoryMountResponseCode::NotFound" },
				{ "NotFound.ToolTip", "The repository ID could not be found in the server list." },
				{ "ToolTip", "Response code returned when trying to mount a session repository on the server." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertSessionRepositoryMountResponseCode",
				"EConcertSessionRepositoryMountResponseCode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertSessionResponseCode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertSessionResponseCode, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertSessionResponseCode"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertSessionResponseCode>()
	{
		return EConcertSessionResponseCode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertSessionResponseCode(EConcertSessionResponseCode_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertSessionResponseCode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertSessionResponseCode_Hash() { return 1894233470U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertSessionResponseCode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertSessionResponseCode"), 0, Get_Z_Construct_UEnum_Concert_EConcertSessionResponseCode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertSessionResponseCode::Success", (int64)EConcertSessionResponseCode::Success },
				{ "EConcertSessionResponseCode::Failed", (int64)EConcertSessionResponseCode::Failed },
				{ "EConcertSessionResponseCode::InvalidRequest", (int64)EConcertSessionResponseCode::InvalidRequest },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Response codes for a session custom request */" },
				{ "Failed.Comment", "/** The request data was valid, but the request failed. A response was generated. */" },
				{ "Failed.Name", "EConcertSessionResponseCode::Failed" },
				{ "Failed.ToolTip", "The request data was valid, but the request failed. A response was generated." },
				{ "InvalidRequest.Comment", "/** The request data was invalid. No response was generated. */" },
				{ "InvalidRequest.Name", "EConcertSessionResponseCode::InvalidRequest" },
				{ "InvalidRequest.ToolTip", "The request data was invalid. No response was generated." },
				{ "ModuleRelativePath", "Public/ConcertMessages.h" },
				{ "Success.Comment", "/** The request data was valid. A response was generated. */" },
				{ "Success.Name", "EConcertSessionResponseCode::Success" },
				{ "Success.ToolTip", "The request data was valid. A response was generated." },
				{ "ToolTip", "Response codes for a session custom request" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertSessionResponseCode",
				"EConcertSessionResponseCode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertClientStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertClientStatus, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertClientStatus"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertClientStatus>()
	{
		return EConcertClientStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertClientStatus(EConcertClientStatus_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertClientStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertClientStatus_Hash() { return 220624129U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertClientStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertClientStatus"), 0, Get_Z_Construct_UEnum_Concert_EConcertClientStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertClientStatus::Connected", (int64)EConcertClientStatus::Connected },
				{ "EConcertClientStatus::Disconnected", (int64)EConcertClientStatus::Disconnected },
				{ "EConcertClientStatus::Updated", (int64)EConcertClientStatus::Updated },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Status for Concert session clients */" },
				{ "Connected.Comment", "/** Client connected */" },
				{ "Connected.Name", "EConcertClientStatus::Connected" },
				{ "Connected.ToolTip", "Client connected" },
				{ "Disconnected.Comment", "/** Client disconnected */" },
				{ "Disconnected.Name", "EConcertClientStatus::Disconnected" },
				{ "Disconnected.ToolTip", "Client disconnected" },
				{ "ModuleRelativePath", "Public/ConcertMessages.h" },
				{ "ToolTip", "Status for Concert session clients" },
				{ "Updated.Comment", "/** Client state updated */" },
				{ "Updated.Name", "EConcertClientStatus::Updated" },
				{ "Updated.ToolTip", "Client state updated" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertClientStatus",
				"EConcertClientStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertConnectionResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertConnectionResult, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertConnectionResult"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertConnectionResult>()
	{
		return EConcertConnectionResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertConnectionResult(EConcertConnectionResult_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertConnectionResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertConnectionResult_Hash() { return 2044336077U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertConnectionResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertConnectionResult"), 0, Get_Z_Construct_UEnum_Concert_EConcertConnectionResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertConnectionResult::None", (int64)EConcertConnectionResult::None },
				{ "EConcertConnectionResult::ConnectionAccepted", (int64)EConcertConnectionResult::ConnectionAccepted },
				{ "EConcertConnectionResult::ConnectionRefused", (int64)EConcertConnectionResult::ConnectionRefused },
				{ "EConcertConnectionResult::AlreadyConnected", (int64)EConcertConnectionResult::AlreadyConnected },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlreadyConnected.Comment", "/** Server already accepted connection */" },
				{ "AlreadyConnected.Name", "EConcertConnectionResult::AlreadyConnected" },
				{ "AlreadyConnected.ToolTip", "Server already accepted connection" },
				{ "Comment", "/** Connection Result for Concert client session */" },
				{ "ConnectionAccepted.Comment", "/** Server has accepted connection */" },
				{ "ConnectionAccepted.Name", "EConcertConnectionResult::ConnectionAccepted" },
				{ "ConnectionAccepted.ToolTip", "Server has accepted connection" },
				{ "ConnectionRefused.Comment", "/** Server has refused the connection session messages beside other connection request are ignored */" },
				{ "ConnectionRefused.Name", "EConcertConnectionResult::ConnectionRefused" },
				{ "ConnectionRefused.ToolTip", "Server has refused the connection session messages beside other connection request are ignored" },
				{ "ModuleRelativePath", "Public/ConcertMessages.h" },
				{ "None.Comment", "/** No result yet */" },
				{ "None.Name", "EConcertConnectionResult::None" },
				{ "None.ToolTip", "No result yet" },
				{ "ToolTip", "Connection Result for Concert client session" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertConnectionResult",
				"EConcertConnectionResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertConnectionStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertConnectionStatus, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertConnectionStatus"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertConnectionStatus>()
	{
		return EConcertConnectionStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertConnectionStatus(EConcertConnectionStatus_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertConnectionStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertConnectionStatus_Hash() { return 2053855848U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertConnectionStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertConnectionStatus"), 0, Get_Z_Construct_UEnum_Concert_EConcertConnectionStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertConnectionStatus::Connecting", (int64)EConcertConnectionStatus::Connecting },
				{ "EConcertConnectionStatus::Connected", (int64)EConcertConnectionStatus::Connected },
				{ "EConcertConnectionStatus::Disconnecting", (int64)EConcertConnectionStatus::Disconnecting },
				{ "EConcertConnectionStatus::Disconnected", (int64)EConcertConnectionStatus::Disconnected },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Connection status for Concert client sessions */" },
				{ "Connected.Comment", "/** Connection established and alive */" },
				{ "Connected.Name", "EConcertConnectionStatus::Connected" },
				{ "Connected.ToolTip", "Connection established and alive" },
				{ "Connecting.Comment", "/** Currently establishing connection to the server session */" },
				{ "Connecting.Name", "EConcertConnectionStatus::Connecting" },
				{ "Connecting.ToolTip", "Currently establishing connection to the server session" },
				{ "Disconnected.Comment", "/** Disconnected */" },
				{ "Disconnected.Name", "EConcertConnectionStatus::Disconnected" },
				{ "Disconnected.ToolTip", "Disconnected" },
				{ "Disconnecting.Comment", "/** Currently severing connection to the server session gracefully */" },
				{ "Disconnecting.Name", "EConcertConnectionStatus::Disconnecting" },
				{ "Disconnecting.ToolTip", "Currently severing connection to the server session gracefully" },
				{ "ModuleRelativePath", "Public/ConcertMessages.h" },
				{ "ToolTip", "Connection status for Concert client sessions" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertConnectionStatus",
				"EConcertConnectionStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FConcertSession_CustomResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertSession_CustomResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertSession_CustomResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_CustomResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_CustomResponse"), sizeof(FConcertSession_CustomResponse), Get_Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_CustomResponse>()
{
	return FConcertSession_CustomResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_CustomResponse(FConcertSession_CustomResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_CustomResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_CustomResponse>(FName(TEXT("ConcertSession_CustomResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomResponse;
	struct Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedPayload_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SerializedPayload;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_CustomResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::NewProp_SerializedPayload_MetaData[] = {
		{ "Category", "Concert Custom Message" },
		{ "Comment", "/** The serialized payload that we're hosting. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The serialized payload that we're hosting." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::NewProp_SerializedPayload = { "SerializedPayload", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_CustomResponse, SerializedPayload), Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::NewProp_SerializedPayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::NewProp_SerializedPayload_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::NewProp_SerializedPayload,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertSession_CustomResponse",
		sizeof(FConcertSession_CustomResponse),
		alignof(FConcertSession_CustomResponse),
		Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_CustomResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_CustomResponse"), sizeof(FConcertSession_CustomResponse), Get_Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomResponse_Hash() { return 381832559U; }

static_assert(std::is_polymorphic<FConcertSession_CustomRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertSession_CustomRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertSession_CustomRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_CustomRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_CustomRequest"), sizeof(FConcertSession_CustomRequest), Get_Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_CustomRequest>()
{
	return FConcertSession_CustomRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_CustomRequest(FConcertSession_CustomRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_CustomRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_CustomRequest>(FName(TEXT("ConcertSession_CustomRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomRequest;
	struct Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourceEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DestinationEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedPayload_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SerializedPayload;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_CustomRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SourceEndpointId_MetaData[] = {
		{ "Category", "Concert Custom Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SourceEndpointId = { "SourceEndpointId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_CustomRequest, SourceEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SourceEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SourceEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_DestinationEndpointId_MetaData[] = {
		{ "Category", "Concert Custom Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_DestinationEndpointId = { "DestinationEndpointId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_CustomRequest, DestinationEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_DestinationEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_DestinationEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SerializedPayload_MetaData[] = {
		{ "Category", "Concert Custom Message" },
		{ "Comment", "/** The serialized payload that we're hosting. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The serialized payload that we're hosting." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SerializedPayload = { "SerializedPayload", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_CustomRequest, SerializedPayload), Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SerializedPayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SerializedPayload_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SourceEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_DestinationEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::NewProp_SerializedPayload,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertSession_CustomRequest",
		sizeof(FConcertSession_CustomRequest),
		alignof(FConcertSession_CustomRequest),
		Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_CustomRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_CustomRequest"), sizeof(FConcertSession_CustomRequest), Get_Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomRequest_Hash() { return 876165942U; }

static_assert(std::is_polymorphic<FConcertSession_CustomEvent>() == std::is_polymorphic<FConcertEventData>(), "USTRUCT FConcertSession_CustomEvent cannot be polymorphic unless super FConcertEventData is polymorphic");

class UScriptStruct* FConcertSession_CustomEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_CustomEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_CustomEvent"), sizeof(FConcertSession_CustomEvent), Get_Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_CustomEvent>()
{
	return FConcertSession_CustomEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_CustomEvent(FConcertSession_CustomEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_CustomEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_CustomEvent>(FName(TEXT("ConcertSession_CustomEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_CustomEvent;
	struct Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourceEndpointId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DestinationEndpointIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationEndpointIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DestinationEndpointIds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedPayload_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SerializedPayload;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_CustomEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SourceEndpointId_MetaData[] = {
		{ "Category", "Concert Custom Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SourceEndpointId = { "SourceEndpointId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_CustomEvent, SourceEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SourceEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SourceEndpointId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_DestinationEndpointIds_Inner = { "DestinationEndpointIds", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_DestinationEndpointIds_MetaData[] = {
		{ "Category", "Concert Custom Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_DestinationEndpointIds = { "DestinationEndpointIds", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_CustomEvent, DestinationEndpointIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_DestinationEndpointIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_DestinationEndpointIds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SerializedPayload_MetaData[] = {
		{ "Category", "Concert Custom Message" },
		{ "Comment", "/** The serialized payload that we're hosting. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The serialized payload that we're hosting." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SerializedPayload = { "SerializedPayload", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_CustomEvent, SerializedPayload), Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SerializedPayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SerializedPayload_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SourceEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_DestinationEndpointIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_DestinationEndpointIds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::NewProp_SerializedPayload,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEventData,
		&NewStructOps,
		"ConcertSession_CustomEvent",
		sizeof(FConcertSession_CustomEvent),
		alignof(FConcertSession_CustomEvent),
		Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_CustomEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_CustomEvent"), sizeof(FConcertSession_CustomEvent), Get_Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_CustomEvent_Hash() { return 405697272U; }

static_assert(std::is_polymorphic<FConcertSession_SessionRenamedEvent>() == std::is_polymorphic<FConcertEventData>(), "USTRUCT FConcertSession_SessionRenamedEvent cannot be polymorphic unless super FConcertEventData is polymorphic");

class UScriptStruct* FConcertSession_SessionRenamedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_SessionRenamedEvent"), sizeof(FConcertSession_SessionRenamedEvent), Get_Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_SessionRenamedEvent>()
{
	return FConcertSession_SessionRenamedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_SessionRenamedEvent(FConcertSession_SessionRenamedEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_SessionRenamedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_SessionRenamedEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_SessionRenamedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_SessionRenamedEvent>(FName(TEXT("ConcertSession_SessionRenamedEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_SessionRenamedEvent;
	struct Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_SessionRenamedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::NewProp_NewName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::NewProp_NewName = { "NewName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_SessionRenamedEvent, NewName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::NewProp_NewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::NewProp_NewName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::NewProp_NewName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEventData,
		&NewStructOps,
		"ConcertSession_SessionRenamedEvent",
		sizeof(FConcertSession_SessionRenamedEvent),
		alignof(FConcertSession_SessionRenamedEvent),
		Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_SessionRenamedEvent"), sizeof(FConcertSession_SessionRenamedEvent), Get_Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_SessionRenamedEvent_Hash() { return 3271823026U; }

static_assert(std::is_polymorphic<FConcertSession_ClientListUpdatedEvent>() == std::is_polymorphic<FConcertEventData>(), "USTRUCT FConcertSession_ClientListUpdatedEvent cannot be polymorphic unless super FConcertEventData is polymorphic");

class UScriptStruct* FConcertSession_ClientListUpdatedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_ClientListUpdatedEvent"), sizeof(FConcertSession_ClientListUpdatedEvent), Get_Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_ClientListUpdatedEvent>()
{
	return FConcertSession_ClientListUpdatedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_ClientListUpdatedEvent(FConcertSession_ClientListUpdatedEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_ClientListUpdatedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_ClientListUpdatedEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_ClientListUpdatedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_ClientListUpdatedEvent>(FName(TEXT("ConcertSession_ClientListUpdatedEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_ClientListUpdatedEvent;
	struct Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionClients_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionClients_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SessionClients;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_ClientListUpdatedEvent>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewProp_SessionClients_Inner = { "SessionClients", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionClientInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewProp_SessionClients_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewProp_SessionClients = { "SessionClients", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_ClientListUpdatedEvent, SessionClients), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewProp_SessionClients_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewProp_SessionClients_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewProp_SessionClients_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::NewProp_SessionClients,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEventData,
		&NewStructOps,
		"ConcertSession_ClientListUpdatedEvent",
		sizeof(FConcertSession_ClientListUpdatedEvent),
		alignof(FConcertSession_ClientListUpdatedEvent),
		Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_ClientListUpdatedEvent"), sizeof(FConcertSession_ClientListUpdatedEvent), Get_Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_ClientListUpdatedEvent_Hash() { return 72119363U; }

static_assert(std::is_polymorphic<FConcertSession_UpdateClientInfoEvent>() == std::is_polymorphic<FConcertEventData>(), "USTRUCT FConcertSession_UpdateClientInfoEvent cannot be polymorphic unless super FConcertEventData is polymorphic");

class UScriptStruct* FConcertSession_UpdateClientInfoEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_UpdateClientInfoEvent"), sizeof(FConcertSession_UpdateClientInfoEvent), Get_Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_UpdateClientInfoEvent>()
{
	return FConcertSession_UpdateClientInfoEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_UpdateClientInfoEvent(FConcertSession_UpdateClientInfoEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_UpdateClientInfoEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_UpdateClientInfoEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_UpdateClientInfoEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_UpdateClientInfoEvent>(FName(TEXT("ConcertSession_UpdateClientInfoEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_UpdateClientInfoEvent;
	struct Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionClient_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionClient;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_UpdateClientInfoEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::NewProp_SessionClient_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::NewProp_SessionClient = { "SessionClient", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_UpdateClientInfoEvent, SessionClient), Z_Construct_UScriptStruct_FConcertSessionClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::NewProp_SessionClient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::NewProp_SessionClient_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::NewProp_SessionClient,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEventData,
		&NewStructOps,
		"ConcertSession_UpdateClientInfoEvent",
		sizeof(FConcertSession_UpdateClientInfoEvent),
		alignof(FConcertSession_UpdateClientInfoEvent),
		Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_UpdateClientInfoEvent"), sizeof(FConcertSession_UpdateClientInfoEvent), Get_Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_UpdateClientInfoEvent_Hash() { return 731584523U; }

static_assert(std::is_polymorphic<FConcertSession_LeaveSessionEvent>() == std::is_polymorphic<FConcertEventData>(), "USTRUCT FConcertSession_LeaveSessionEvent cannot be polymorphic unless super FConcertEventData is polymorphic");

class UScriptStruct* FConcertSession_LeaveSessionEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_LeaveSessionEvent"), sizeof(FConcertSession_LeaveSessionEvent), Get_Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_LeaveSessionEvent>()
{
	return FConcertSession_LeaveSessionEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_LeaveSessionEvent(FConcertSession_LeaveSessionEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_LeaveSessionEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_LeaveSessionEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_LeaveSessionEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_LeaveSessionEvent>(FName(TEXT("ConcertSession_LeaveSessionEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_LeaveSessionEvent;
	struct Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionServerEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionServerEndpointId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_LeaveSessionEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::NewProp_SessionServerEndpointId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::NewProp_SessionServerEndpointId = { "SessionServerEndpointId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_LeaveSessionEvent, SessionServerEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::NewProp_SessionServerEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::NewProp_SessionServerEndpointId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::NewProp_SessionServerEndpointId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEventData,
		&NewStructOps,
		"ConcertSession_LeaveSessionEvent",
		sizeof(FConcertSession_LeaveSessionEvent),
		alignof(FConcertSession_LeaveSessionEvent),
		Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_LeaveSessionEvent"), sizeof(FConcertSession_LeaveSessionEvent), Get_Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_LeaveSessionEvent_Hash() { return 1642703009U; }

static_assert(std::is_polymorphic<FConcertSession_JoinSessionResultEvent>() == std::is_polymorphic<FConcertEndpointDiscoveryEvent>(), "USTRUCT FConcertSession_JoinSessionResultEvent cannot be polymorphic unless super FConcertEndpointDiscoveryEvent is polymorphic");

class UScriptStruct* FConcertSession_JoinSessionResultEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_JoinSessionResultEvent"), sizeof(FConcertSession_JoinSessionResultEvent), Get_Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_JoinSessionResultEvent>()
{
	return FConcertSession_JoinSessionResultEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_JoinSessionResultEvent(FConcertSession_JoinSessionResultEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_JoinSessionResultEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_JoinSessionResultEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_JoinSessionResultEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_JoinSessionResultEvent>(FName(TEXT("ConcertSession_JoinSessionResultEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_JoinSessionResultEvent;
	struct Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionServerEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionServerEndpointId;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ConnectionResult_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionResult_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConnectionResult;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionClients_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionClients_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SessionClients;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_JoinSessionResultEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionServerEndpointId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionServerEndpointId = { "SessionServerEndpointId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_JoinSessionResultEvent, SessionServerEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionServerEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionServerEndpointId_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_ConnectionResult_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_ConnectionResult_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_ConnectionResult = { "ConnectionResult", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_JoinSessionResultEvent, ConnectionResult), Z_Construct_UEnum_Concert_EConcertConnectionResult, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_ConnectionResult_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_ConnectionResult_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionClients_Inner = { "SessionClients", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionClientInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionClients_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionClients = { "SessionClients", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_JoinSessionResultEvent, SessionClients), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionClients_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionClients_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionServerEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_ConnectionResult_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_ConnectionResult,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionClients_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::NewProp_SessionClients,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent,
		&NewStructOps,
		"ConcertSession_JoinSessionResultEvent",
		sizeof(FConcertSession_JoinSessionResultEvent),
		alignof(FConcertSession_JoinSessionResultEvent),
		Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_JoinSessionResultEvent"), sizeof(FConcertSession_JoinSessionResultEvent), Get_Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_JoinSessionResultEvent_Hash() { return 3511815572U; }

static_assert(std::is_polymorphic<FConcertSession_DiscoverAndJoinSessionEvent>() == std::is_polymorphic<FConcertEndpointDiscoveryEvent>(), "USTRUCT FConcertSession_DiscoverAndJoinSessionEvent cannot be polymorphic unless super FConcertEndpointDiscoveryEvent is polymorphic");

class UScriptStruct* FConcertSession_DiscoverAndJoinSessionEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSession_DiscoverAndJoinSessionEvent"), sizeof(FConcertSession_DiscoverAndJoinSessionEvent), Get_Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSession_DiscoverAndJoinSessionEvent>()
{
	return FConcertSession_DiscoverAndJoinSessionEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent(FConcertSession_DiscoverAndJoinSessionEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSession_DiscoverAndJoinSessionEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSession_DiscoverAndJoinSessionEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSession_DiscoverAndJoinSessionEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSession_DiscoverAndJoinSessionEvent>(FName(TEXT("ConcertSession_DiscoverAndJoinSessionEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSession_DiscoverAndJoinSessionEvent;
	struct Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionServerEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionServerEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSession_DiscoverAndJoinSessionEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_SessionServerEndpointId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_SessionServerEndpointId = { "SessionServerEndpointId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_DiscoverAndJoinSessionEvent, SessionServerEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_SessionServerEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_SessionServerEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_ClientInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_ClientInfo = { "ClientInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSession_DiscoverAndJoinSessionEvent, ClientInfo), Z_Construct_UScriptStruct_FConcertClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_ClientInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_ClientInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_SessionServerEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::NewProp_ClientInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent,
		&NewStructOps,
		"ConcertSession_DiscoverAndJoinSessionEvent",
		sizeof(FConcertSession_DiscoverAndJoinSessionEvent),
		alignof(FConcertSession_DiscoverAndJoinSessionEvent),
		Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSession_DiscoverAndJoinSessionEvent"), sizeof(FConcertSession_DiscoverAndJoinSessionEvent), Get_Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSession_DiscoverAndJoinSessionEvent_Hash() { return 2791359676U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetSessionActivitiesResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_GetSessionActivitiesResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_GetSessionActivitiesResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetSessionActivitiesResponse"), sizeof(FConcertAdmin_GetSessionActivitiesResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetSessionActivitiesResponse>()
{
	return FConcertAdmin_GetSessionActivitiesResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse(FConcertAdmin_GetSessionActivitiesResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetSessionActivitiesResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionActivitiesResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionActivitiesResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetSessionActivitiesResponse>(FName(TEXT("ConcertAdmin_GetSessionActivitiesResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionActivitiesResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Activities_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Activities_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Activities;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointClientInfoMap_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointClientInfoMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointClientInfoMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_EndpointClientInfoMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetSessionActivitiesResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_Activities_Inner = { "Activities", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_Activities_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The list of activities. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The list of activities." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_Activities = { "Activities", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionActivitiesResponse, Activities), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_Activities_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_Activities_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap_ValueProp = { "EndpointClientInfoMap", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FConcertClientInfo, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap_Key_KeyProp = { "EndpointClientInfoMap_Key", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Maps each activity endpoint to its client info. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Maps each activity endpoint to its client info." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap = { "EndpointClientInfoMap", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionActivitiesResponse, EndpointClientInfoMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_Activities_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_Activities,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::NewProp_EndpointClientInfoMap,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_GetSessionActivitiesResponse",
		sizeof(FConcertAdmin_GetSessionActivitiesResponse),
		alignof(FConcertAdmin_GetSessionActivitiesResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetSessionActivitiesResponse"), sizeof(FConcertAdmin_GetSessionActivitiesResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesResponse_Hash() { return 4122479413U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetSessionActivitiesRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_GetSessionActivitiesRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_GetSessionActivitiesRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetSessionActivitiesRequest"), sizeof(FConcertAdmin_GetSessionActivitiesRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetSessionActivitiesRequest>()
{
	return FConcertAdmin_GetSessionActivitiesRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest(FConcertAdmin_GetSessionActivitiesRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetSessionActivitiesRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionActivitiesRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionActivitiesRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetSessionActivitiesRequest>(FName(TEXT("ConcertAdmin_GetSessionActivitiesRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionActivitiesRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FromActivityId_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_FromActivityId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_ActivityCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeDetails_MetaData[];
#endif
		static void NewProp_bIncludeDetails_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeDetails;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetSessionActivitiesRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionActivitiesRequest, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_FromActivityId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_FromActivityId = { "FromActivityId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionActivitiesRequest, FromActivityId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_FromActivityId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_FromActivityId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_ActivityCount_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_ActivityCount = { "ActivityCount", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionActivitiesRequest, ActivityCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_ActivityCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_ActivityCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_bIncludeDetails_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_bIncludeDetails_SetBit(void* Obj)
	{
		((FConcertAdmin_GetSessionActivitiesRequest*)Obj)->bIncludeDetails = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_bIncludeDetails = { "bIncludeDetails", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertAdmin_GetSessionActivitiesRequest), &Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_bIncludeDetails_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_bIncludeDetails_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_bIncludeDetails_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_FromActivityId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_ActivityCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::NewProp_bIncludeDetails,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_GetSessionActivitiesRequest",
		sizeof(FConcertAdmin_GetSessionActivitiesRequest),
		alignof(FConcertAdmin_GetSessionActivitiesRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetSessionActivitiesRequest"), sizeof(FConcertAdmin_GetSessionActivitiesRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionActivitiesRequest_Hash() { return 3381771840U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetSessionClientsResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_GetSessionClientsResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_GetSessionClientsResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetSessionClientsResponse"), sizeof(FConcertAdmin_GetSessionClientsResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetSessionClientsResponse>()
{
	return FConcertAdmin_GetSessionClientsResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse(FConcertAdmin_GetSessionClientsResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetSessionClientsResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionClientsResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionClientsResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetSessionClientsResponse>(FName(TEXT("ConcertAdmin_GetSessionClientsResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionClientsResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionClients_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionClients_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SessionClients;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetSessionClientsResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewProp_SessionClients_Inner = { "SessionClients", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionClientInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewProp_SessionClients_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewProp_SessionClients = { "SessionClients", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionClientsResponse, SessionClients), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewProp_SessionClients_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewProp_SessionClients_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewProp_SessionClients_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::NewProp_SessionClients,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_GetSessionClientsResponse",
		sizeof(FConcertAdmin_GetSessionClientsResponse),
		alignof(FConcertAdmin_GetSessionClientsResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetSessionClientsResponse"), sizeof(FConcertAdmin_GetSessionClientsResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsResponse_Hash() { return 2406339292U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetSessionClientsRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_GetSessionClientsRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_GetSessionClientsRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetSessionClientsRequest"), sizeof(FConcertAdmin_GetSessionClientsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetSessionClientsRequest>()
{
	return FConcertAdmin_GetSessionClientsRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest(FConcertAdmin_GetSessionClientsRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetSessionClientsRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionClientsRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionClientsRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetSessionClientsRequest>(FName(TEXT("ConcertAdmin_GetSessionClientsRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionClientsRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetSessionClientsRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionClientsRequest, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::NewProp_SessionId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::NewProp_SessionId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_GetSessionClientsRequest",
		sizeof(FConcertAdmin_GetSessionClientsRequest),
		alignof(FConcertAdmin_GetSessionClientsRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetSessionClientsRequest"), sizeof(FConcertAdmin_GetSessionClientsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionClientsRequest_Hash() { return 3073087830U; }

static_assert(std::is_polymorphic<FConcertAdmin_DeleteSessionResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_DeleteSessionResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_DeleteSessionResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_DeleteSessionResponse"), sizeof(FConcertAdmin_DeleteSessionResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_DeleteSessionResponse>()
{
	return FConcertAdmin_DeleteSessionResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_DeleteSessionResponse(FConcertAdmin_DeleteSessionResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_DeleteSessionResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DeleteSessionResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DeleteSessionResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_DeleteSessionResponse>(FName(TEXT("ConcertAdmin_DeleteSessionResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DeleteSessionResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_DeleteSessionResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the session that was requested to be deleted. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the session that was requested to be deleted." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DeleteSessionResponse, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The name of the session that was was requested to be deleted. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The name of the session that was was requested to be deleted." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionName = { "SessionName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DeleteSessionResponse, SessionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::NewProp_SessionName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_DeleteSessionResponse",
		sizeof(FConcertAdmin_DeleteSessionResponse),
		alignof(FConcertAdmin_DeleteSessionResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_DeleteSessionResponse"), sizeof(FConcertAdmin_DeleteSessionResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionResponse_Hash() { return 2905449918U; }

static_assert(std::is_polymorphic<FConcertAdmin_DeleteSessionRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_DeleteSessionRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_DeleteSessionRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_DeleteSessionRequest"), sizeof(FConcertAdmin_DeleteSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_DeleteSessionRequest>()
{
	return FConcertAdmin_DeleteSessionRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_DeleteSessionRequest(FConcertAdmin_DeleteSessionRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_DeleteSessionRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DeleteSessionRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DeleteSessionRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_DeleteSessionRequest>(FName(TEXT("ConcertAdmin_DeleteSessionRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DeleteSessionRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Delete a live session. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Delete a live session." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_DeleteSessionRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the session to delete. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the session to delete." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DeleteSessionRequest, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_UserName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "//For now only the user name and device name of the client is used to id him as the owner of a session\n" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "For now only the user name and device name of the client is used to id him as the owner of a session" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_UserName = { "UserName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DeleteSessionRequest, UserName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_UserName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_UserName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DeleteSessionRequest, DeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_DeviceName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_UserName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::NewProp_DeviceName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_DeleteSessionRequest",
		sizeof(FConcertAdmin_DeleteSessionRequest),
		alignof(FConcertAdmin_DeleteSessionRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_DeleteSessionRequest"), sizeof(FConcertAdmin_DeleteSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DeleteSessionRequest_Hash() { return 2886464820U; }

static_assert(std::is_polymorphic<FConcertAdmin_RenameSessionResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_RenameSessionResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_RenameSessionResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_RenameSessionResponse"), sizeof(FConcertAdmin_RenameSessionResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_RenameSessionResponse>()
{
	return FConcertAdmin_RenameSessionResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_RenameSessionResponse(FConcertAdmin_RenameSessionResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_RenameSessionResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_RenameSessionResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_RenameSessionResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_RenameSessionResponse>(FName(TEXT("ConcertAdmin_RenameSessionResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_RenameSessionResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OldName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OldName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_RenameSessionResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the session that was requested to be renamed. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the session that was requested to be renamed." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_RenameSessionResponse, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_OldName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The old session name (if the session exist). */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The old session name (if the session exist)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_OldName = { "OldName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_RenameSessionResponse, OldName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_OldName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_OldName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::NewProp_OldName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_RenameSessionResponse",
		sizeof(FConcertAdmin_RenameSessionResponse),
		alignof(FConcertAdmin_RenameSessionResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_RenameSessionResponse"), sizeof(FConcertAdmin_RenameSessionResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionResponse_Hash() { return 1381903134U; }

static_assert(std::is_polymorphic<FConcertAdmin_RenameSessionRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_RenameSessionRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_RenameSessionRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_RenameSessionRequest"), sizeof(FConcertAdmin_RenameSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_RenameSessionRequest>()
{
	return FConcertAdmin_RenameSessionRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_RenameSessionRequest(FConcertAdmin_RenameSessionRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_RenameSessionRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_RenameSessionRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_RenameSessionRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_RenameSessionRequest>(FName(TEXT("ConcertAdmin_RenameSessionRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_RenameSessionRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NewName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Rename a session. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Rename a session." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_RenameSessionRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the session to rename. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the session to rename." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_RenameSessionRequest, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_NewName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The new session name. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The new session name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_NewName = { "NewName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_RenameSessionRequest, NewName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_NewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_NewName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_UserName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "// For now only the user name and device name of the client is used to id him as the owner of a session\n" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "For now only the user name and device name of the client is used to id him as the owner of a session" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_UserName = { "UserName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_RenameSessionRequest, UserName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_UserName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_UserName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_RenameSessionRequest, DeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_DeviceName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_NewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_UserName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::NewProp_DeviceName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_RenameSessionRequest",
		sizeof(FConcertAdmin_RenameSessionRequest),
		alignof(FConcertAdmin_RenameSessionRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_RenameSessionRequest"), sizeof(FConcertAdmin_RenameSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_RenameSessionRequest_Hash() { return 1420844406U; }

static_assert(std::is_polymorphic<FConcertAdmin_ArchiveSessionResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_ArchiveSessionResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_ArchiveSessionResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_ArchiveSessionResponse"), sizeof(FConcertAdmin_ArchiveSessionResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_ArchiveSessionResponse>()
{
	return FConcertAdmin_ArchiveSessionResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse(FConcertAdmin_ArchiveSessionResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_ArchiveSessionResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ArchiveSessionResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ArchiveSessionResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_ArchiveSessionResponse>(FName(TEXT("ConcertAdmin_ArchiveSessionResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ArchiveSessionResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArchiveId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ArchiveId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArchiveName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ArchiveName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_ArchiveSessionResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the session that was requested to be archived. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the session that was requested to be archived." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionResponse, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The name of the session that was requested to be archived. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The name of the session that was requested to be archived." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionName = { "SessionName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionResponse, SessionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the new archived session (on success). */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the new archived session (on success)." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveId = { "ArchiveId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionResponse, ArchiveId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The name of the new archived session (on success). */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The name of the new archived session (on success)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveName = { "ArchiveName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionResponse, ArchiveName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_SessionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::NewProp_ArchiveName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_ArchiveSessionResponse",
		sizeof(FConcertAdmin_ArchiveSessionResponse),
		alignof(FConcertAdmin_ArchiveSessionResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_ArchiveSessionResponse"), sizeof(FConcertAdmin_ArchiveSessionResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionResponse_Hash() { return 1751430282U; }

static_assert(std::is_polymorphic<FConcertAdmin_ArchiveSessionRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_ArchiveSessionRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_ArchiveSessionRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_ArchiveSessionRequest"), sizeof(FConcertAdmin_ArchiveSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_ArchiveSessionRequest>()
{
	return FConcertAdmin_ArchiveSessionRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest(FConcertAdmin_ArchiveSessionRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_ArchiveSessionRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ArchiveSessionRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ArchiveSessionRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_ArchiveSessionRequest>(FName(TEXT("ConcertAdmin_ArchiveSessionRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ArchiveSessionRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArchiveNameOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ArchiveNameOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeviceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DeviceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionFilter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Create an archived copy of a live session. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Create an archived copy of a live session." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_ArchiveSessionRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the session to archive (must be a live session). */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the session to archive (must be a live session)." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionRequest, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_ArchiveNameOverride_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The override for the archive. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The override for the archive." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_ArchiveNameOverride = { "ArchiveNameOverride", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionRequest, ArchiveNameOverride), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_ArchiveNameOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_ArchiveNameOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_UserName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The caller user name. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The caller user name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_UserName = { "UserName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionRequest, UserName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_UserName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_UserName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_DeviceName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The caller device name. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The caller device name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_DeviceName = { "DeviceName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionRequest, DeviceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_DeviceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_DeviceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionFilter_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The filter controlling which activities from the session should be archived. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The filter controlling which activities from the session should be archived." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionFilter = { "SessionFilter", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ArchiveSessionRequest, SessionFilter), Z_Construct_UScriptStruct_FConcertSessionFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionFilter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_ArchiveNameOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_UserName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_DeviceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::NewProp_SessionFilter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_ArchiveSessionRequest",
		sizeof(FConcertAdmin_ArchiveSessionRequest),
		alignof(FConcertAdmin_ArchiveSessionRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_ArchiveSessionRequest"), sizeof(FConcertAdmin_ArchiveSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ArchiveSessionRequest_Hash() { return 2869811256U; }

static_assert(std::is_polymorphic<FConcertAdmin_SessionInfoResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_SessionInfoResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_SessionInfoResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_SessionInfoResponse"), sizeof(FConcertAdmin_SessionInfoResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_SessionInfoResponse>()
{
	return FConcertAdmin_SessionInfoResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_SessionInfoResponse(FConcertAdmin_SessionInfoResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_SessionInfoResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_SessionInfoResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_SessionInfoResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_SessionInfoResponse>(FName(TEXT("ConcertAdmin_SessionInfoResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_SessionInfoResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_SessionInfoResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::NewProp_SessionInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::NewProp_SessionInfo = { "SessionInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_SessionInfoResponse, SessionInfo), Z_Construct_UScriptStruct_FConcertSessionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::NewProp_SessionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::NewProp_SessionInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::NewProp_SessionInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_SessionInfoResponse",
		sizeof(FConcertAdmin_SessionInfoResponse),
		alignof(FConcertAdmin_SessionInfoResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_SessionInfoResponse"), sizeof(FConcertAdmin_SessionInfoResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_SessionInfoResponse_Hash() { return 3934095368U; }

static_assert(std::is_polymorphic<FConcertAdmin_CopySessionRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_CopySessionRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_CopySessionRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_CopySessionRequest"), sizeof(FConcertAdmin_CopySessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_CopySessionRequest>()
{
	return FConcertAdmin_CopySessionRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_CopySessionRequest(FConcertAdmin_CopySessionRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_CopySessionRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_CopySessionRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_CopySessionRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_CopySessionRequest>(FName(TEXT("ConcertAdmin_CopySessionRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_CopySessionRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerClientInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerClientInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VersionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VersionInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionFilter_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionFilter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Used to copy a live session or restore an archived one. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Used to copy a live session or restore an archived one." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_CopySessionRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The ID of the session to copy or restore. It can be a live or an archived session unless bRestoreOnly is true, in which case, the session to copy must be an archived one. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The ID of the session to copy or restore. It can be a live or an archived session unless bRestoreOnly is true, in which case, the session to copy must be an archived one." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CopySessionRequest, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The name of the session to create. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The name of the session to create." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionName = { "SessionName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CopySessionRequest, SessionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_OwnerClientInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Information about the owner of the copied session. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Information about the owner of the copied session." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_OwnerClientInfo = { "OwnerClientInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CopySessionRequest, OwnerClientInfo), Z_Construct_UScriptStruct_FConcertClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_OwnerClientInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_OwnerClientInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionSettings_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Settings to apply to the copied session. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Settings to apply to the copied session." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionSettings = { "SessionSettings", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CopySessionRequest, SessionSettings), Z_Construct_UScriptStruct_FConcertSessionSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_VersionInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Version information of the client requesting the copy. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Version information of the client requesting the copy." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_VersionInfo = { "VersionInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CopySessionRequest, VersionInfo), Z_Construct_UScriptStruct_FConcertSessionVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_VersionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_VersionInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionFilter_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The filter controlling which activities from the session should be copied over. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The filter controlling which activities from the session should be copied over." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionFilter = { "SessionFilter", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CopySessionRequest, SessionFilter), Z_Construct_UScriptStruct_FConcertSessionFilter, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionFilter_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionFilter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_OwnerClientInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_VersionInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::NewProp_SessionFilter,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_CopySessionRequest",
		sizeof(FConcertAdmin_CopySessionRequest),
		alignof(FConcertAdmin_CopySessionRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_CopySessionRequest"), sizeof(FConcertAdmin_CopySessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_CopySessionRequest_Hash() { return 2779391518U; }

static_assert(std::is_polymorphic<FConcertAdmin_FindSessionRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_FindSessionRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_FindSessionRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_FindSessionRequest"), sizeof(FConcertAdmin_FindSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_FindSessionRequest>()
{
	return FConcertAdmin_FindSessionRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_FindSessionRequest(FConcertAdmin_FindSessionRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_FindSessionRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_FindSessionRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_FindSessionRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_FindSessionRequest>(FName(TEXT("ConcertAdmin_FindSessionRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_FindSessionRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerClientInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerClientInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VersionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VersionInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_FindSessionRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionId = { "SessionId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_FindSessionRequest, SessionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_OwnerClientInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_OwnerClientInfo = { "OwnerClientInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_FindSessionRequest, OwnerClientInfo), Z_Construct_UScriptStruct_FConcertClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_OwnerClientInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_OwnerClientInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionSettings_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionSettings = { "SessionSettings", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_FindSessionRequest, SessionSettings), Z_Construct_UScriptStruct_FConcertSessionSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_VersionInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_VersionInfo = { "VersionInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_FindSessionRequest, VersionInfo), Z_Construct_UScriptStruct_FConcertSessionVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_VersionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_VersionInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_OwnerClientInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_SessionSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::NewProp_VersionInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_FindSessionRequest",
		sizeof(FConcertAdmin_FindSessionRequest),
		alignof(FConcertAdmin_FindSessionRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_FindSessionRequest"), sizeof(FConcertAdmin_FindSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_FindSessionRequest_Hash() { return 2647431679U; }

static_assert(std::is_polymorphic<FConcertAdmin_CreateSessionRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_CreateSessionRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_CreateSessionRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_CreateSessionRequest"), sizeof(FConcertAdmin_CreateSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_CreateSessionRequest>()
{
	return FConcertAdmin_CreateSessionRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_CreateSessionRequest(FConcertAdmin_CreateSessionRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_CreateSessionRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_CreateSessionRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_CreateSessionRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_CreateSessionRequest>(FName(TEXT("ConcertAdmin_CreateSessionRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_CreateSessionRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerClientInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OwnerClientInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VersionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VersionInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_CreateSessionRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionName = { "SessionName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CreateSessionRequest, SessionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_OwnerClientInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_OwnerClientInfo = { "OwnerClientInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CreateSessionRequest, OwnerClientInfo), Z_Construct_UScriptStruct_FConcertClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_OwnerClientInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_OwnerClientInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionSettings_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionSettings = { "SessionSettings", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CreateSessionRequest, SessionSettings), Z_Construct_UScriptStruct_FConcertSessionSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_VersionInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_VersionInfo = { "VersionInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_CreateSessionRequest, VersionInfo), Z_Construct_UScriptStruct_FConcertSessionVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_VersionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_VersionInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_OwnerClientInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_SessionSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::NewProp_VersionInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_CreateSessionRequest",
		sizeof(FConcertAdmin_CreateSessionRequest),
		alignof(FConcertAdmin_CreateSessionRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_CreateSessionRequest"), sizeof(FConcertAdmin_CreateSessionRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_CreateSessionRequest_Hash() { return 3406981024U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetSessionsResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_GetSessionsResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_GetSessionsResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetSessionsResponse"), sizeof(FConcertAdmin_GetSessionsResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetSessionsResponse>()
{
	return FConcertAdmin_GetSessionsResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetSessionsResponse(FConcertAdmin_GetSessionsResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetSessionsResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionsResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionsResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetSessionsResponse>(FName(TEXT("ConcertAdmin_GetSessionsResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionsResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Sessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sessions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetSessionsResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewProp_Sessions_Inner = { "Sessions", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewProp_Sessions_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewProp_Sessions = { "Sessions", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionsResponse, Sessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewProp_Sessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewProp_Sessions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewProp_Sessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::NewProp_Sessions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_GetSessionsResponse",
		sizeof(FConcertAdmin_GetSessionsResponse),
		alignof(FConcertAdmin_GetSessionsResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetSessionsResponse"), sizeof(FConcertAdmin_GetSessionsResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionsResponse_Hash() { return 3355967903U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetArchivedSessionsRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_GetArchivedSessionsRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_GetArchivedSessionsRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetArchivedSessionsRequest"), sizeof(FConcertAdmin_GetArchivedSessionsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetArchivedSessionsRequest>()
{
	return FConcertAdmin_GetArchivedSessionsRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest(FConcertAdmin_GetArchivedSessionsRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetArchivedSessionsRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetArchivedSessionsRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetArchivedSessionsRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetArchivedSessionsRequest>(FName(TEXT("ConcertAdmin_GetArchivedSessionsRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetArchivedSessionsRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetArchivedSessionsRequest>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_GetArchivedSessionsRequest",
		sizeof(FConcertAdmin_GetArchivedSessionsRequest),
		alignof(FConcertAdmin_GetArchivedSessionsRequest),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetArchivedSessionsRequest"), sizeof(FConcertAdmin_GetArchivedSessionsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetArchivedSessionsRequest_Hash() { return 146085004U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetLiveSessionsRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_GetLiveSessionsRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_GetLiveSessionsRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetLiveSessionsRequest"), sizeof(FConcertAdmin_GetLiveSessionsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetLiveSessionsRequest>()
{
	return FConcertAdmin_GetLiveSessionsRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest(FConcertAdmin_GetLiveSessionsRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetLiveSessionsRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetLiveSessionsRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetLiveSessionsRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetLiveSessionsRequest>(FName(TEXT("ConcertAdmin_GetLiveSessionsRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetLiveSessionsRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetLiveSessionsRequest>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_GetLiveSessionsRequest",
		sizeof(FConcertAdmin_GetLiveSessionsRequest),
		alignof(FConcertAdmin_GetLiveSessionsRequest),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetLiveSessionsRequest"), sizeof(FConcertAdmin_GetLiveSessionsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetLiveSessionsRequest_Hash() { return 3609518716U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetAllSessionsResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_GetAllSessionsResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_GetAllSessionsResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetAllSessionsResponse"), sizeof(FConcertAdmin_GetAllSessionsResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetAllSessionsResponse>()
{
	return FConcertAdmin_GetAllSessionsResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse(FConcertAdmin_GetAllSessionsResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetAllSessionsResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetAllSessionsResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetAllSessionsResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetAllSessionsResponse>(FName(TEXT("ConcertAdmin_GetAllSessionsResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetAllSessionsResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LiveSessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LiveSessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LiveSessions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ArchivedSessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArchivedSessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ArchivedSessions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetAllSessionsResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_LiveSessions_Inner = { "LiveSessions", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_LiveSessions_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_LiveSessions = { "LiveSessions", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetAllSessionsResponse, LiveSessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_LiveSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_LiveSessions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_ArchivedSessions_Inner = { "ArchivedSessions", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_ArchivedSessions_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_ArchivedSessions = { "ArchivedSessions", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetAllSessionsResponse, ArchivedSessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_ArchivedSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_ArchivedSessions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_LiveSessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_LiveSessions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_ArchivedSessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::NewProp_ArchivedSessions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_GetAllSessionsResponse",
		sizeof(FConcertAdmin_GetAllSessionsResponse),
		alignof(FConcertAdmin_GetAllSessionsResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetAllSessionsResponse"), sizeof(FConcertAdmin_GetAllSessionsResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsResponse_Hash() { return 2837141964U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetAllSessionsRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_GetAllSessionsRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_GetAllSessionsRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetAllSessionsRequest"), sizeof(FConcertAdmin_GetAllSessionsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetAllSessionsRequest>()
{
	return FConcertAdmin_GetAllSessionsRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest(FConcertAdmin_GetAllSessionsRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetAllSessionsRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetAllSessionsRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetAllSessionsRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetAllSessionsRequest>(FName(TEXT("ConcertAdmin_GetAllSessionsRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetAllSessionsRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetAllSessionsRequest>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_GetAllSessionsRequest",
		sizeof(FConcertAdmin_GetAllSessionsRequest),
		alignof(FConcertAdmin_GetAllSessionsRequest),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetAllSessionsRequest"), sizeof(FConcertAdmin_GetAllSessionsRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetAllSessionsRequest_Hash() { return 508532630U; }

static_assert(std::is_polymorphic<FConcertAdmin_DropSessionRepositoriesResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_DropSessionRepositoriesResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_DropSessionRepositoriesResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_DropSessionRepositoriesResponse"), sizeof(FConcertAdmin_DropSessionRepositoriesResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_DropSessionRepositoriesResponse>()
{
	return FConcertAdmin_DropSessionRepositoriesResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse(FConcertAdmin_DropSessionRepositoriesResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_DropSessionRepositoriesResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DropSessionRepositoriesResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DropSessionRepositoriesResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_DropSessionRepositoriesResponse>(FName(TEXT("ConcertAdmin_DropSessionRepositoriesResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DropSessionRepositoriesResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DroppedRepositoryIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DroppedRepositoryIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DroppedRepositoryIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_DropSessionRepositoriesResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewProp_DroppedRepositoryIds_Inner = { "DroppedRepositoryIds", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewProp_DroppedRepositoryIds_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The list of repository IDs successfully dropped (not found == dropped). */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The list of repository IDs successfully dropped (not found == dropped)." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewProp_DroppedRepositoryIds = { "DroppedRepositoryIds", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DropSessionRepositoriesResponse, DroppedRepositoryIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewProp_DroppedRepositoryIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewProp_DroppedRepositoryIds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewProp_DroppedRepositoryIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::NewProp_DroppedRepositoryIds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_DropSessionRepositoriesResponse",
		sizeof(FConcertAdmin_DropSessionRepositoriesResponse),
		alignof(FConcertAdmin_DropSessionRepositoriesResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_DropSessionRepositoriesResponse"), sizeof(FConcertAdmin_DropSessionRepositoriesResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesResponse_Hash() { return 2827366702U; }

static_assert(std::is_polymorphic<FConcertAdmin_DropSessionRepositoriesRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_DropSessionRepositoriesRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_DropSessionRepositoriesRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_DropSessionRepositoriesRequest"), sizeof(FConcertAdmin_DropSessionRepositoriesRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_DropSessionRepositoriesRequest>()
{
	return FConcertAdmin_DropSessionRepositoriesRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest(FConcertAdmin_DropSessionRepositoriesRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_DropSessionRepositoriesRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DropSessionRepositoriesRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DropSessionRepositoriesRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_DropSessionRepositoriesRequest>(FName(TEXT("ConcertAdmin_DropSessionRepositoriesRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DropSessionRepositoriesRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RepositoryIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RepositoryIds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Drop one or more session repositories from the server, deleting all the contained files.\n */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Drop one or more session repositories from the server, deleting all the contained files." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_DropSessionRepositoriesRequest>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewProp_RepositoryIds_Inner = { "RepositoryIds", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewProp_RepositoryIds_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The list of repository IDs to drop. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The list of repository IDs to drop." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewProp_RepositoryIds = { "RepositoryIds", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DropSessionRepositoriesRequest, RepositoryIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewProp_RepositoryIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewProp_RepositoryIds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewProp_RepositoryIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::NewProp_RepositoryIds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_DropSessionRepositoriesRequest",
		sizeof(FConcertAdmin_DropSessionRepositoriesRequest),
		alignof(FConcertAdmin_DropSessionRepositoriesRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_DropSessionRepositoriesRequest"), sizeof(FConcertAdmin_DropSessionRepositoriesRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DropSessionRepositoriesRequest_Hash() { return 1884077734U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetSessionRepositoriesResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_GetSessionRepositoriesResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_GetSessionRepositoriesResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetSessionRepositoriesResponse"), sizeof(FConcertAdmin_GetSessionRepositoriesResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetSessionRepositoriesResponse>()
{
	return FConcertAdmin_GetSessionRepositoriesResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse(FConcertAdmin_GetSessionRepositoriesResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetSessionRepositoriesResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionRepositoriesResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionRepositoriesResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetSessionRepositoriesResponse>(FName(TEXT("ConcertAdmin_GetSessionRepositoriesResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionRepositoriesResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionRepositories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionRepositories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SessionRepositories;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetSessionRepositoriesResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewProp_SessionRepositories_Inner = { "SessionRepositories", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewProp_SessionRepositories_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewProp_SessionRepositories = { "SessionRepositories", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_GetSessionRepositoriesResponse, SessionRepositories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewProp_SessionRepositories_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewProp_SessionRepositories_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewProp_SessionRepositories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::NewProp_SessionRepositories,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_GetSessionRepositoriesResponse",
		sizeof(FConcertAdmin_GetSessionRepositoriesResponse),
		alignof(FConcertAdmin_GetSessionRepositoriesResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetSessionRepositoriesResponse"), sizeof(FConcertAdmin_GetSessionRepositoriesResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesResponse_Hash() { return 2709623862U; }

static_assert(std::is_polymorphic<FConcertAdmin_GetSessionRepositoriesRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_GetSessionRepositoriesRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_GetSessionRepositoriesRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_GetSessionRepositoriesRequest"), sizeof(FConcertAdmin_GetSessionRepositoriesRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_GetSessionRepositoriesRequest>()
{
	return FConcertAdmin_GetSessionRepositoriesRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest(FConcertAdmin_GetSessionRepositoriesRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_GetSessionRepositoriesRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionRepositoriesRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionRepositoriesRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_GetSessionRepositoriesRequest>(FName(TEXT("ConcertAdmin_GetSessionRepositoriesRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_GetSessionRepositoriesRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Returns the list of repositories known of the server. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Returns the list of repositories known of the server." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_GetSessionRepositoriesRequest>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_GetSessionRepositoriesRequest",
		sizeof(FConcertAdmin_GetSessionRepositoriesRequest),
		alignof(FConcertAdmin_GetSessionRepositoriesRequest),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_GetSessionRepositoriesRequest"), sizeof(FConcertAdmin_GetSessionRepositoriesRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_GetSessionRepositoriesRequest_Hash() { return 3007228905U; }

static_assert(std::is_polymorphic<FConcertAdmin_MountSessionRepositoryResponse>() == std::is_polymorphic<FConcertResponseData>(), "USTRUCT FConcertAdmin_MountSessionRepositoryResponse cannot be polymorphic unless super FConcertResponseData is polymorphic");

class UScriptStruct* FConcertAdmin_MountSessionRepositoryResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_MountSessionRepositoryResponse"), sizeof(FConcertAdmin_MountSessionRepositoryResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_MountSessionRepositoryResponse>()
{
	return FConcertAdmin_MountSessionRepositoryResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse(FConcertAdmin_MountSessionRepositoryResponse::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_MountSessionRepositoryResponse"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_MountSessionRepositoryResponse
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_MountSessionRepositoryResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_MountSessionRepositoryResponse>(FName(TEXT("ConcertAdmin_MountSessionRepositoryResponse")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_MountSessionRepositoryResponse;
	struct Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MountStatus_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MountStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MountStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_MountSessionRepositoryResponse>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewProp_MountStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewProp_MountStatus_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewProp_MountStatus = { "MountStatus", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_MountSessionRepositoryResponse, MountStatus), Z_Construct_UEnum_Concert_EConcertSessionRepositoryMountResponseCode, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewProp_MountStatus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewProp_MountStatus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewProp_MountStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::NewProp_MountStatus,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertResponseData,
		&NewStructOps,
		"ConcertAdmin_MountSessionRepositoryResponse",
		sizeof(FConcertAdmin_MountSessionRepositoryResponse),
		alignof(FConcertAdmin_MountSessionRepositoryResponse),
		Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_MountSessionRepositoryResponse"), sizeof(FConcertAdmin_MountSessionRepositoryResponse), Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryResponse_Hash() { return 1550039258U; }

static_assert(std::is_polymorphic<FConcertAdmin_MountSessionRepositoryRequest>() == std::is_polymorphic<FConcertRequestData>(), "USTRUCT FConcertAdmin_MountSessionRepositoryRequest cannot be polymorphic unless super FConcertRequestData is polymorphic");

class UScriptStruct* FConcertAdmin_MountSessionRepositoryRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_MountSessionRepositoryRequest"), sizeof(FConcertAdmin_MountSessionRepositoryRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_MountSessionRepositoryRequest>()
{
	return FConcertAdmin_MountSessionRepositoryRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest(FConcertAdmin_MountSessionRepositoryRequest::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_MountSessionRepositoryRequest"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_MountSessionRepositoryRequest
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_MountSessionRepositoryRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_MountSessionRepositoryRequest>(FName(TEXT("ConcertAdmin_MountSessionRepositoryRequest")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_MountSessionRepositoryRequest;
	struct Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RepositoryId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryRootDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RepositoryRootDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAsServerDefault_MetaData[];
#endif
		static void NewProp_bAsServerDefault_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAsServerDefault;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCreateIfNotExist_MetaData[];
#endif
		static void NewProp_bCreateIfNotExist_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCreateIfNotExist;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Mount a session repository used to store session files.\n */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Mount a session repository used to store session files." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_MountSessionRepositoryRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The repository unique Id. Must be valid. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The repository unique Id. Must be valid." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryId = { "RepositoryId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_MountSessionRepositoryRequest, RepositoryId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryRootDir_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The repository root dir (absolute path) where the repository should be found/created. If empty, the server will use its default one. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The repository root dir (absolute path) where the repository should be found/created. If empty, the server will use its default one." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryRootDir = { "RepositoryRootDir", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_MountSessionRepositoryRequest, RepositoryRootDir), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryRootDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryRootDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bAsServerDefault_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Whether this repository is set as the default one used by server to store new sessions. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Whether this repository is set as the default one used by server to store new sessions." },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bAsServerDefault_SetBit(void* Obj)
	{
		((FConcertAdmin_MountSessionRepositoryRequest*)Obj)->bAsServerDefault = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bAsServerDefault = { "bAsServerDefault", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertAdmin_MountSessionRepositoryRequest), &Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bAsServerDefault_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bAsServerDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bAsServerDefault_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bCreateIfNotExist_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Whether the repository should be created if it was not found. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Whether the repository should be created if it was not found." },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bCreateIfNotExist_SetBit(void* Obj)
	{
		((FConcertAdmin_MountSessionRepositoryRequest*)Obj)->bCreateIfNotExist = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bCreateIfNotExist = { "bCreateIfNotExist", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertAdmin_MountSessionRepositoryRequest), &Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bCreateIfNotExist_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bCreateIfNotExist_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bCreateIfNotExist_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_RepositoryRootDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bAsServerDefault,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::NewProp_bCreateIfNotExist,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertRequestData,
		&NewStructOps,
		"ConcertAdmin_MountSessionRepositoryRequest",
		sizeof(FConcertAdmin_MountSessionRepositoryRequest),
		alignof(FConcertAdmin_MountSessionRepositoryRequest),
		Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_MountSessionRepositoryRequest"), sizeof(FConcertAdmin_MountSessionRepositoryRequest), Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_MountSessionRepositoryRequest_Hash() { return 1187571716U; }
class UScriptStruct* FConcertSessionRepositoryInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSessionRepositoryInfo"), sizeof(FConcertSessionRepositoryInfo), Get_Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSessionRepositoryInfo>()
{
	return FConcertSessionRepositoryInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSessionRepositoryInfo(FConcertSessionRepositoryInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSessionRepositoryInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSessionRepositoryInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSessionRepositoryInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertSessionRepositoryInfo>(FName(TEXT("ConcertSessionRepositoryInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSessionRepositoryInfo;
	struct Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RepositoryId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMounted_MetaData[];
#endif
		static void NewProp_bMounted_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMounted;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Contains information about a session repository. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Contains information about a session repository." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSessionRepositoryInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_RepositoryId_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The repository ID.*/" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The repository ID." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_RepositoryId = { "RepositoryId", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionRepositoryInfo, RepositoryId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_RepositoryId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_RepositoryId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_bMounted_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The mounted state of this repository. */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The mounted state of this repository." },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_bMounted_SetBit(void* Obj)
	{
		((FConcertSessionRepositoryInfo*)Obj)->bMounted = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_bMounted = { "bMounted", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSessionRepositoryInfo), &Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_bMounted_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_bMounted_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_bMounted_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_RepositoryId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::NewProp_bMounted,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSessionRepositoryInfo",
		sizeof(FConcertSessionRepositoryInfo),
		alignof(FConcertSessionRepositoryInfo),
		Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSessionRepositoryInfo"), sizeof(FConcertSessionRepositoryInfo), Get_Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSessionRepositoryInfo_Hash() { return 640724U; }

static_assert(std::is_polymorphic<FConcertAdmin_ServerDiscoveredEvent>() == std::is_polymorphic<FConcertEndpointDiscoveryEvent>(), "USTRUCT FConcertAdmin_ServerDiscoveredEvent cannot be polymorphic unless super FConcertEndpointDiscoveryEvent is polymorphic");

class UScriptStruct* FConcertAdmin_ServerDiscoveredEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_ServerDiscoveredEvent"), sizeof(FConcertAdmin_ServerDiscoveredEvent), Get_Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_ServerDiscoveredEvent>()
{
	return FConcertAdmin_ServerDiscoveredEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent(FConcertAdmin_ServerDiscoveredEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_ServerDiscoveredEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ServerDiscoveredEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ServerDiscoveredEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_ServerDiscoveredEvent>(FName(TEXT("ConcertAdmin_ServerDiscoveredEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_ServerDiscoveredEvent;
	struct Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ServerName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InstanceInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InstanceInfo;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ServerFlags_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerFlags_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ServerFlags;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_ServerDiscoveredEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerName_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Server designated name */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Server designated name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerName = { "ServerName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ServerDiscoveredEvent, ServerName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_InstanceInfo_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Basic information about the server instance */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Basic information about the server instance" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_InstanceInfo = { "InstanceInfo", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ServerDiscoveredEvent, InstanceInfo), Z_Construct_UScriptStruct_FConcertInstanceInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_InstanceInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_InstanceInfo_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerFlags_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerFlags_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** Contains information on the server settings */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "Contains information on the server settings" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerFlags = { "ServerFlags", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_ServerDiscoveredEvent, ServerFlags), Z_Construct_UEnum_Concert_EConcertServerFlags, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerFlags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerFlags_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_InstanceInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerFlags_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::NewProp_ServerFlags,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent,
		&NewStructOps,
		"ConcertAdmin_ServerDiscoveredEvent",
		sizeof(FConcertAdmin_ServerDiscoveredEvent),
		alignof(FConcertAdmin_ServerDiscoveredEvent),
		Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_ServerDiscoveredEvent"), sizeof(FConcertAdmin_ServerDiscoveredEvent), Get_Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_ServerDiscoveredEvent_Hash() { return 1501381052U; }

static_assert(std::is_polymorphic<FConcertAdmin_DiscoverServersEvent>() == std::is_polymorphic<FConcertEndpointDiscoveryEvent>(), "USTRUCT FConcertAdmin_DiscoverServersEvent cannot be polymorphic unless super FConcertEndpointDiscoveryEvent is polymorphic");

class UScriptStruct* FConcertAdmin_DiscoverServersEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertAdmin_DiscoverServersEvent"), sizeof(FConcertAdmin_DiscoverServersEvent), Get_Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertAdmin_DiscoverServersEvent>()
{
	return FConcertAdmin_DiscoverServersEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertAdmin_DiscoverServersEvent(FConcertAdmin_DiscoverServersEvent::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertAdmin_DiscoverServersEvent"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DiscoverServersEvent
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DiscoverServersEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertAdmin_DiscoverServersEvent>(FName(TEXT("ConcertAdmin_DiscoverServersEvent")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertAdmin_DiscoverServersEvent;
	struct Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequiredRole_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RequiredRole;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequiredVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RequiredVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientAuthenticationKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ClientAuthenticationKey;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertAdmin_DiscoverServersEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredRole_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The required role of the server (eg, MultiUser, DisasterRecovery, etc) */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The required role of the server (eg, MultiUser, DisasterRecovery, etc)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredRole = { "RequiredRole", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DiscoverServersEvent, RequiredRole), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredRole_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredRole_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredVersion_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** The required version of the server (eg, 4.22, 4.23, etc) */" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "The required version of the server (eg, 4.22, 4.23, etc)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredVersion = { "RequiredVersion", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DiscoverServersEvent, RequiredVersion), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_ClientAuthenticationKey_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "Comment", "/** If a server was configured to restrict access to specific client(s), it will search for this key in its list of authorized keys.*/" },
		{ "ModuleRelativePath", "Public/ConcertMessages.h" },
		{ "ToolTip", "If a server was configured to restrict access to specific client(s), it will search for this key in its list of authorized keys." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_ClientAuthenticationKey = { "ClientAuthenticationKey", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertAdmin_DiscoverServersEvent, ClientAuthenticationKey), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_ClientAuthenticationKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_ClientAuthenticationKey_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredRole,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_RequiredVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::NewProp_ClientAuthenticationKey,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		Z_Construct_UScriptStruct_FConcertEndpointDiscoveryEvent,
		&NewStructOps,
		"ConcertAdmin_DiscoverServersEvent",
		sizeof(FConcertAdmin_DiscoverServersEvent),
		alignof(FConcertAdmin_DiscoverServersEvent),
		Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertAdmin_DiscoverServersEvent"), sizeof(FConcertAdmin_DiscoverServersEvent), Get_Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertAdmin_DiscoverServersEvent_Hash() { return 173630453U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
