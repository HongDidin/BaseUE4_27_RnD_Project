// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertTransport/Public/ConcertTransportSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertTransportSettings() {}
// Cross Module References
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointSettings();
	UPackage* Z_Construct_UPackage__Script_ConcertTransport();
	CONCERTTRANSPORT_API UClass* Z_Construct_UClass_UConcertEndpointConfig_NoRegister();
	CONCERTTRANSPORT_API UClass* Z_Construct_UClass_UConcertEndpointConfig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FConcertEndpointSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTTRANSPORT_API uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertEndpointSettings, Z_Construct_UPackage__Script_ConcertTransport(), TEXT("ConcertEndpointSettings"), sizeof(FConcertEndpointSettings), Get_Z_Construct_UScriptStruct_FConcertEndpointSettings_Hash());
	}
	return Singleton;
}
template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<FConcertEndpointSettings>()
{
	return FConcertEndpointSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertEndpointSettings(FConcertEndpointSettings::StaticStruct, TEXT("/Script/ConcertTransport"), TEXT("ConcertEndpointSettings"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointSettings
{
	FScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointSettings()
	{
		UScriptStruct::DeferCppStructOps<FConcertEndpointSettings>(FName(TEXT("ConcertEndpointSettings")));
	}
} ScriptStruct_ConcertTransport_StaticRegisterNativesFConcertEndpointSettings;
	struct Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableLogging_MetaData[];
#endif
		static void NewProp_bEnableLogging_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableLogging;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PurgeProcessedMessageDelaySeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PurgeProcessedMessageDelaySeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteEndpointTimeoutSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RemoteEndpointTimeoutSeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransportSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertEndpointSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_bEnableLogging_MetaData[] = {
		{ "Category", "Endpoint Settings" },
		{ "Comment", "/** Enable detailed message logging for Concert endpoints */" },
		{ "ModuleRelativePath", "Public/ConcertTransportSettings.h" },
		{ "ToolTip", "Enable detailed message logging for Concert endpoints" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_bEnableLogging_SetBit(void* Obj)
	{
		((FConcertEndpointSettings*)Obj)->bEnableLogging = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_bEnableLogging = { "bEnableLogging", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertEndpointSettings), &Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_bEnableLogging_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_bEnableLogging_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_bEnableLogging_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_PurgeProcessedMessageDelaySeconds_MetaData[] = {
		{ "Category", "Endpoint Settings" },
		{ "Comment", "/** The timespan at which retained processed messages are purged on Concert endpoints */" },
		{ "DisplayName", "Purge Processed Message Delay" },
		{ "ForceUnits", "s" },
		{ "ModuleRelativePath", "Public/ConcertTransportSettings.h" },
		{ "ToolTip", "The timespan at which retained processed messages are purged on Concert endpoints" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_PurgeProcessedMessageDelaySeconds = { "PurgeProcessedMessageDelaySeconds", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertEndpointSettings, PurgeProcessedMessageDelaySeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_PurgeProcessedMessageDelaySeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_PurgeProcessedMessageDelaySeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_RemoteEndpointTimeoutSeconds_MetaData[] = {
		{ "Category", "Endpoint Settings" },
		{ "ClampMin", "4" },
		{ "Comment", "/** The timespan at which remote endpoints that haven't sent a message are considered stale */" },
		{ "DisplayName", "Remote Endpoint Timeout" },
		{ "ForceUnits", "s" },
		{ "ModuleRelativePath", "Public/ConcertTransportSettings.h" },
		{ "ToolTip", "The timespan at which remote endpoints that haven't sent a message are considered stale" },
		{ "UIMin", "4" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_RemoteEndpointTimeoutSeconds = { "RemoteEndpointTimeoutSeconds", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertEndpointSettings, RemoteEndpointTimeoutSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_RemoteEndpointTimeoutSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_RemoteEndpointTimeoutSeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_bEnableLogging,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_PurgeProcessedMessageDelaySeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::NewProp_RemoteEndpointTimeoutSeconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
		nullptr,
		&NewStructOps,
		"ConcertEndpointSettings",
		sizeof(FConcertEndpointSettings),
		alignof(FConcertEndpointSettings),
		Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertTransport();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertEndpointSettings"), sizeof(FConcertEndpointSettings), Get_Z_Construct_UScriptStruct_FConcertEndpointSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertEndpointSettings_Hash() { return 2903514304U; }
	void UConcertEndpointConfig::StaticRegisterNativesUConcertEndpointConfig()
	{
	}
	UClass* Z_Construct_UClass_UConcertEndpointConfig_NoRegister()
	{
		return UConcertEndpointConfig::StaticClass();
	}
	struct Z_Construct_UClass_UConcertEndpointConfig_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertEndpointConfig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertTransport,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertEndpointConfig_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConcertTransportSettings.h" },
		{ "ModuleRelativePath", "Public/ConcertTransportSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertEndpointConfig_Statics::NewProp_EndpointSettings_MetaData[] = {
		{ "Category", "Endpoint Settings" },
		{ "ModuleRelativePath", "Public/ConcertTransportSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertEndpointConfig_Statics::NewProp_EndpointSettings = { "EndpointSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertEndpointConfig, EndpointSettings), Z_Construct_UScriptStruct_FConcertEndpointSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertEndpointConfig_Statics::NewProp_EndpointSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertEndpointConfig_Statics::NewProp_EndpointSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertEndpointConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertEndpointConfig_Statics::NewProp_EndpointSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertEndpointConfig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertEndpointConfig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertEndpointConfig_Statics::ClassParams = {
		&UConcertEndpointConfig::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertEndpointConfig_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertEndpointConfig_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertEndpointConfig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertEndpointConfig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertEndpointConfig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertEndpointConfig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertEndpointConfig, 695363258);
	template<> CONCERTTRANSPORT_API UClass* StaticClass<UConcertEndpointConfig>()
	{
		return UConcertEndpointConfig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertEndpointConfig(Z_Construct_UClass_UConcertEndpointConfig, &UConcertEndpointConfig::StaticClass, TEXT("/Script/ConcertTransport"), TEXT("UConcertEndpointConfig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertEndpointConfig);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
