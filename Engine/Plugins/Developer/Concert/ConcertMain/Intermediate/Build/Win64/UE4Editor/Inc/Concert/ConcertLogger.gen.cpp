// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Concert/Private/ConcertLogger.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertLogger() {}
// Cross Module References
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertLogMessageAction();
	UPackage* Z_Construct_UPackage__Script_Concert();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertLog();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSerializedPayload();
// End Cross Module References
	static UEnum* EConcertLogMessageAction_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertLogMessageAction, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertLogMessageAction"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertLogMessageAction>()
	{
		return EConcertLogMessageAction_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertLogMessageAction(EConcertLogMessageAction_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertLogMessageAction"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertLogMessageAction_Hash() { return 4256288013U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertLogMessageAction()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertLogMessageAction"), 0, Get_Z_Construct_UEnum_Concert_EConcertLogMessageAction_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertLogMessageAction::None", (int64)EConcertLogMessageAction::None },
				{ "EConcertLogMessageAction::Send", (int64)EConcertLogMessageAction::Send },
				{ "EConcertLogMessageAction::Publish", (int64)EConcertLogMessageAction::Publish },
				{ "EConcertLogMessageAction::Receive", (int64)EConcertLogMessageAction::Receive },
				{ "EConcertLogMessageAction::Queue", (int64)EConcertLogMessageAction::Queue },
				{ "EConcertLogMessageAction::Discard", (int64)EConcertLogMessageAction::Discard },
				{ "EConcertLogMessageAction::Duplicate", (int64)EConcertLogMessageAction::Duplicate },
				{ "EConcertLogMessageAction::TimeOut", (int64)EConcertLogMessageAction::TimeOut },
				{ "EConcertLogMessageAction::Process", (int64)EConcertLogMessageAction::Process },
				{ "EConcertLogMessageAction::EndpointDiscovery", (int64)EConcertLogMessageAction::EndpointDiscovery },
				{ "EConcertLogMessageAction::EndpointTimeOut", (int64)EConcertLogMessageAction::EndpointTimeOut },
				{ "EConcertLogMessageAction::EndpointClosure", (int64)EConcertLogMessageAction::EndpointClosure },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Discard.Name", "EConcertLogMessageAction::Discard" },
				{ "Duplicate.Name", "EConcertLogMessageAction::Duplicate" },
				{ "EndpointClosure.Name", "EConcertLogMessageAction::EndpointClosure" },
				{ "EndpointDiscovery.Name", "EConcertLogMessageAction::EndpointDiscovery" },
				{ "EndpointTimeOut.Name", "EConcertLogMessageAction::EndpointTimeOut" },
				{ "ModuleRelativePath", "Private/ConcertLogger.h" },
				{ "None.Name", "EConcertLogMessageAction::None" },
				{ "Process.Name", "EConcertLogMessageAction::Process" },
				{ "Publish.Name", "EConcertLogMessageAction::Publish" },
				{ "Queue.Name", "EConcertLogMessageAction::Queue" },
				{ "Receive.Name", "EConcertLogMessageAction::Receive" },
				{ "Send.Name", "EConcertLogMessageAction::Send" },
				{ "TimeOut.Name", "EConcertLogMessageAction::TimeOut" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertLogMessageAction",
				"EConcertLogMessageAction",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertLog::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertLog_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertLog, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertLog"), sizeof(FConcertLog), Get_Z_Construct_UScriptStruct_FConcertLog_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertLog>()
{
	return FConcertLog::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertLog(FConcertLog::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertLog"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertLog
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertLog()
	{
		UScriptStruct::DeferCppStructOps<FConcertLog>(FName(TEXT("ConcertLog")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertLog;
	struct Z_Construct_UScriptStruct_FConcertLog_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frame_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt64PropertyParams NewProp_Frame;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MessageId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageOrderIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_MessageOrderIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelId_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ChannelId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Timestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Timestamp;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MessageAction_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageAction_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MessageAction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MessageTypeName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MessageTypeName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OriginEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DestinationEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DestinationEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomPayloadTypename_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CustomPayloadTypename;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomPayloadUncompressedByteSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CustomPayloadUncompressedByteSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringPayload_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringPayload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedPayload_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SerializedPayload;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertLog_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertLog>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Frame_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt64PropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Frame = { "Frame", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, Frame), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Frame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Frame_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageId_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageId = { "MessageId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, MessageId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageOrderIndex_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageOrderIndex = { "MessageOrderIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, MessageOrderIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageOrderIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageOrderIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_ChannelId_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_ChannelId = { "ChannelId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, ChannelId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_ChannelId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_ChannelId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Timestamp_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Timestamp = { "Timestamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, Timestamp), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Timestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Timestamp_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageAction_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageAction_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageAction = { "MessageAction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, MessageAction), Z_Construct_UEnum_Concert_EConcertLogMessageAction, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageAction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageAction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageTypeName_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageTypeName = { "MessageTypeName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, MessageTypeName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageTypeName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageTypeName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_OriginEndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_OriginEndpointId = { "OriginEndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, OriginEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_OriginEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_OriginEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_DestinationEndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_DestinationEndpointId = { "DestinationEndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, DestinationEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_DestinationEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_DestinationEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadTypename_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadTypename = { "CustomPayloadTypename", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, CustomPayloadTypename), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadTypename_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadTypename_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadUncompressedByteSize_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadUncompressedByteSize = { "CustomPayloadUncompressedByteSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, CustomPayloadUncompressedByteSize), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadUncompressedByteSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadUncompressedByteSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_StringPayload_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_StringPayload = { "StringPayload", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, StringPayload), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_StringPayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_StringPayload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_SerializedPayload_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertLogger.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_SerializedPayload = { "SerializedPayload", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLog, SerializedPayload), Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_SerializedPayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_SerializedPayload_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertLog_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Frame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageOrderIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_ChannelId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_Timestamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageAction_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageAction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_MessageTypeName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_OriginEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_DestinationEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadTypename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_CustomPayloadUncompressedByteSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_StringPayload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLog_Statics::NewProp_SerializedPayload,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertLog_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertLog",
		sizeof(FConcertLog),
		alignof(FConcertLog),
		Z_Construct_UScriptStruct_FConcertLog_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLog_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLog_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertLog()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertLog_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertLog"), sizeof(FConcertLog), Get_Z_Construct_UScriptStruct_FConcertLog_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertLog_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertLog_Hash() { return 1332349280U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
