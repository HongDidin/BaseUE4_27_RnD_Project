// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Concert/Public/ConcertSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertSettings() {}
// Cross Module References
	CONCERT_API UEnum* Z_Construct_UEnum_Concert_EConcertSourceValidationMode();
	UPackage* Z_Construct_UPackage__Script_Concert();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSourceControlSettings();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertServerSettings();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSettings();
	CONCERT_API UClass* Z_Construct_UClass_UConcertServerConfig_NoRegister();
	CONCERT_API UClass* Z_Construct_UClass_UConcertServerConfig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionVersionInfo();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEndpointSettings();
	CONCERT_API UClass* Z_Construct_UClass_UConcertClientConfig_NoRegister();
	CONCERT_API UClass* Z_Construct_UClass_UConcertClientConfig();
// End Cross Module References
	static UEnum* EConcertSourceValidationMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Concert_EConcertSourceValidationMode, Z_Construct_UPackage__Script_Concert(), TEXT("EConcertSourceValidationMode"));
		}
		return Singleton;
	}
	template<> CONCERT_API UEnum* StaticEnum<EConcertSourceValidationMode>()
	{
		return EConcertSourceValidationMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertSourceValidationMode(EConcertSourceValidationMode_StaticEnum, TEXT("/Script/Concert"), TEXT("EConcertSourceValidationMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Concert_EConcertSourceValidationMode_Hash() { return 4024748452U; }
	UEnum* Z_Construct_UEnum_Concert_EConcertSourceValidationMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertSourceValidationMode"), 0, Get_Z_Construct_UEnum_Concert_EConcertSourceValidationMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertSourceValidationMode::Hard", (int64)EConcertSourceValidationMode::Hard },
				{ "EConcertSourceValidationMode::Soft", (int64)EConcertSourceValidationMode::Soft },
				{ "EConcertSourceValidationMode::SoftAutoProceed", (int64)EConcertSourceValidationMode::SoftAutoProceed },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Hard.Comment", "/** Source control validation will fail on any changes when connecting to a Multi-User Session. */" },
				{ "Hard.Name", "EConcertSourceValidationMode::Hard" },
				{ "Hard.ToolTip", "Source control validation will fail on any changes when connecting to a Multi-User Session." },
				{ "ModuleRelativePath", "Public/ConcertSettings.h" },
				{ "Soft.Comment", "/** \n\x09 * Source control validation will warn and prompt on any changes when connecting to a Multi-User session. \n\x09 * In Memory changes will be hot-reloaded.\n\x09 * Source control changes aren't affected but will be stashed/shelved in the future.\n\x09 */" },
				{ "Soft.Name", "EConcertSourceValidationMode::Soft" },
				{ "Soft.ToolTip", "Source control validation will warn and prompt on any changes when connecting to a Multi-User session.\nIn Memory changes will be hot-reloaded.\nSource control changes aren't affected but will be stashed/shelved in the future." },
				{ "SoftAutoProceed.Comment", "/** Soft validation mode with auto proceed on prompts. */" },
				{ "SoftAutoProceed.Name", "EConcertSourceValidationMode::SoftAutoProceed" },
				{ "SoftAutoProceed.ToolTip", "Soft validation mode with auto proceed on prompts." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Concert,
				nullptr,
				"EConcertSourceValidationMode",
				"EConcertSourceValidationMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertSourceControlSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSourceControlSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSourceControlSettings, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSourceControlSettings"), sizeof(FConcertSourceControlSettings), Get_Z_Construct_UScriptStruct_FConcertSourceControlSettings_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSourceControlSettings>()
{
	return FConcertSourceControlSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSourceControlSettings(FConcertSourceControlSettings::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSourceControlSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSourceControlSettings
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSourceControlSettings()
	{
		UScriptStruct::DeferCppStructOps<FConcertSourceControlSettings>(FName(TEXT("ConcertSourceControlSettings")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSourceControlSettings;
	struct Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ValidationMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValidationMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ValidationMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSourceControlSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewProp_ValidationMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewProp_ValidationMode_MetaData[] = {
		{ "Category", "Source Control Settings" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewProp_ValidationMode = { "ValidationMode", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSourceControlSettings, ValidationMode), Z_Construct_UEnum_Concert_EConcertSourceValidationMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewProp_ValidationMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewProp_ValidationMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewProp_ValidationMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::NewProp_ValidationMode,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSourceControlSettings",
		sizeof(FConcertSourceControlSettings),
		alignof(FConcertSourceControlSettings),
		Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSourceControlSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSourceControlSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSourceControlSettings"), sizeof(FConcertSourceControlSettings), Get_Z_Construct_UScriptStruct_FConcertSourceControlSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSourceControlSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSourceControlSettings_Hash() { return 1909864805U; }
class UScriptStruct* FConcertClientSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientSettings, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertClientSettings"), sizeof(FConcertClientSettings), Get_Z_Construct_UScriptStruct_FConcertClientSettings_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertClientSettings>()
{
	return FConcertClientSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientSettings(FConcertClientSettings::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertClientSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertClientSettings
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertClientSettings()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientSettings>(FName(TEXT("ConcertClientSettings")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertClientSettings;
	struct Z_Construct_UScriptStruct_FConcertClientSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AvatarColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AvatarColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesktopAvatarActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DesktopAvatarActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRAvatarActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_VRAvatarActorClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_ServerPort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DiscoveryTimeoutSeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DiscoveryTimeoutSeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionTickFrequencySeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SessionTickFrequencySeconds;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LatencyCompensationMs_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LatencyCompensationMs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReflectLevelEditorInGame_MetaData[];
#endif
		static void NewProp_bReflectLevelEditorInGame_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReflectLevelEditorInGame;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Tags_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tags_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Tags;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientAuthenticationKey_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ClientAuthenticationKey;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DisplayName_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** \n\x09 * The display name to use when in a session. \n\x09 * Can be specified on the editor cmd with `-CONCERTDISPLAYNAME=`.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The display name to use when in a session.\nCan be specified on the editor cmd with `-CONCERTDISPLAYNAME=`." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DisplayName = { "DisplayName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, DisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_AvatarColor_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** The color used for the presence avatar in a session. */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The color used for the presence avatar in a session." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_AvatarColor = { "AvatarColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, AvatarColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_AvatarColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_AvatarColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DesktopAvatarActorClass_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** The desktop representation of this editor's user to other connected users */" },
		{ "MetaClass", "ConcertClientDesktopPresenceActor" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The desktop representation of this editor's user to other connected users" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DesktopAvatarActorClass = { "DesktopAvatarActorClass", nullptr, (EPropertyFlags)0x0010000002004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, DesktopAvatarActorClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DesktopAvatarActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DesktopAvatarActorClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_VRAvatarActorClass_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** The VR representation of this editor's user to other connected users */" },
		{ "DisplayName", "VR Avatar Actor Class" },
		{ "MetaClass", "ConcertClientVRPresenceActor" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The VR representation of this editor's user to other connected users" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_VRAvatarActorClass = { "VRAvatarActorClass", nullptr, (EPropertyFlags)0x0010000002004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, VRAvatarActorClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_VRAvatarActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_VRAvatarActorClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ServerPort_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** The port to use to reach the server with static endpoints when launched through the editor. This port will be used over the unicast endpoint port in the UDP Messagging settings if non 0 when transferring the editor settings to the launched server. */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The port to use to reach the server with static endpoints when launched through the editor. This port will be used over the unicast endpoint port in the UDP Messagging settings if non 0 when transferring the editor settings to the launched server." },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ServerPort = { "ServerPort", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, ServerPort), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ServerPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ServerPort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DiscoveryTimeoutSeconds_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** The timespan at which discovered Multi-User server are considered stale if they haven't answered back */" },
		{ "DisplayName", "Discovery Timeout" },
		{ "ForceUnits", "s" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The timespan at which discovered Multi-User server are considered stale if they haven't answered back" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DiscoveryTimeoutSeconds = { "DiscoveryTimeoutSeconds", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, DiscoveryTimeoutSeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DiscoveryTimeoutSeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DiscoveryTimeoutSeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_SessionTickFrequencySeconds_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** The timespan at which session updates are processed. */" },
		{ "DisplayName", "Session Tick Frequency" },
		{ "ForceUnits", "s" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The timespan at which session updates are processed." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_SessionTickFrequencySeconds = { "SessionTickFrequencySeconds", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, SessionTickFrequencySeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_SessionTickFrequencySeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_SessionTickFrequencySeconds_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_LatencyCompensationMs_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** Amount of latency compensation to apply to time-synchronization sensitive interactions */" },
		{ "DisplayName", "Latency Compensation" },
		{ "ForceUnits", "ms" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Amount of latency compensation to apply to time-synchronization sensitive interactions" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_LatencyCompensationMs = { "LatencyCompensationMs", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, LatencyCompensationMs), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_LatencyCompensationMs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_LatencyCompensationMs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_bReflectLevelEditorInGame_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** When level editor changes are made, reflect those changes to the game equivalent property */" },
		{ "DisplayName", "Reflect Level Visibility to Game" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "When level editor changes are made, reflect those changes to the game equivalent property" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_bReflectLevelEditorInGame_SetBit(void* Obj)
	{
		((FConcertClientSettings*)Obj)->bReflectLevelEditorInGame = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_bReflectLevelEditorInGame = { "bReflectLevelEditorInGame", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertClientSettings), &Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_bReflectLevelEditorInGame_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_bReflectLevelEditorInGame_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_bReflectLevelEditorInGame_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_Tags_Inner = { "Tags", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_Tags_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** Array of tags that can be used for grouping and categorizing. */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Array of tags that can be used for grouping and categorizing." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_Tags = { "Tags", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, Tags), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_Tags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_Tags_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ClientAuthenticationKey_MetaData[] = {
		{ "Comment", "/** A key used to identify the clients during server discovery. If the server was configured to restrict access, the client key must be know of the server. Can be left empty. */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "A key used to identify the clients during server discovery. If the server was configured to restrict access, the client key must be know of the server. Can be left empty." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ClientAuthenticationKey = { "ClientAuthenticationKey", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientSettings, ClientAuthenticationKey), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ClientAuthenticationKey_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ClientAuthenticationKey_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_AvatarColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DesktopAvatarActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_VRAvatarActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ServerPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_DiscoveryTimeoutSeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_SessionTickFrequencySeconds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_LatencyCompensationMs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_bReflectLevelEditorInGame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_Tags_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_Tags,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientSettings_Statics::NewProp_ClientAuthenticationKey,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertClientSettings",
		sizeof(FConcertClientSettings),
		alignof(FConcertClientSettings),
		Z_Construct_UScriptStruct_FConcertClientSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientSettings"), sizeof(FConcertClientSettings), Get_Z_Construct_UScriptStruct_FConcertClientSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientSettings_Hash() { return 733560007U; }
class UScriptStruct* FConcertServerSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertServerSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertServerSettings, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertServerSettings"), sizeof(FConcertServerSettings), Get_Z_Construct_UScriptStruct_FConcertServerSettings_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertServerSettings>()
{
	return FConcertServerSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertServerSettings(FConcertServerSettings::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertServerSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertServerSettings
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertServerSettings()
	{
		UScriptStruct::DeferCppStructOps<FConcertServerSettings>(FName(TEXT("ConcertServerSettings")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertServerSettings;
	struct Z_Construct_UScriptStruct_FConcertServerSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreSessionSettingsRestriction_MetaData[];
#endif
		static void NewProp_bIgnoreSessionSettingsRestriction_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreSessionSettingsRestriction;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionTickFrequencySeconds_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SessionTickFrequencySeconds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertServerSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_bIgnoreSessionSettingsRestriction_MetaData[] = {
		{ "Category", "Server Settings" },
		{ "Comment", "/** The server will allow client to join potentially incompatible sessions */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The server will allow client to join potentially incompatible sessions" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_bIgnoreSessionSettingsRestriction_SetBit(void* Obj)
	{
		((FConcertServerSettings*)Obj)->bIgnoreSessionSettingsRestriction = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_bIgnoreSessionSettingsRestriction = { "bIgnoreSessionSettingsRestriction", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertServerSettings), &Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_bIgnoreSessionSettingsRestriction_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_bIgnoreSessionSettingsRestriction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_bIgnoreSessionSettingsRestriction_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_SessionTickFrequencySeconds_MetaData[] = {
		{ "Category", "Server Settings" },
		{ "Comment", "/** The timespan at which session updates are processed. */" },
		{ "DisplayName", "Session Tick Frequency" },
		{ "ForceUnits", "s" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The timespan at which session updates are processed." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_SessionTickFrequencySeconds = { "SessionTickFrequencySeconds", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertServerSettings, SessionTickFrequencySeconds), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_SessionTickFrequencySeconds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_SessionTickFrequencySeconds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertServerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_bIgnoreSessionSettingsRestriction,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerSettings_Statics::NewProp_SessionTickFrequencySeconds,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertServerSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertServerSettings",
		sizeof(FConcertServerSettings),
		alignof(FConcertServerSettings),
		Z_Construct_UScriptStruct_FConcertServerSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertServerSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertServerSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertServerSettings"), sizeof(FConcertServerSettings), Get_Z_Construct_UScriptStruct_FConcertServerSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertServerSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertServerSettings_Hash() { return 80502761U; }
class UScriptStruct* FConcertSessionSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSessionSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSessionSettings, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSessionSettings"), sizeof(FConcertSessionSettings), Get_Z_Construct_UScriptStruct_FConcertSessionSettings_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSessionSettings>()
{
	return FConcertSessionSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSessionSettings(FConcertSessionSettings::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSessionSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSessionSettings
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSessionSettings()
	{
		UScriptStruct::DeferCppStructOps<FConcertSessionSettings>(FName(TEXT("ConcertSessionSettings")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSessionSettings;
	struct Z_Construct_UScriptStruct_FConcertSessionSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BaseRevision_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_BaseRevision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArchiveNameOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ArchiveNameOverride;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSessionSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ProjectName_MetaData[] = {
		{ "Category", "Session Settings" },
		{ "Comment", "/**\n\x09 * Name of the project of the session.\n\x09 * Can be specified on the server cmd with `-CONCERTPROJECT=`\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Name of the project of the session.\nCan be specified on the server cmd with `-CONCERTPROJECT=`" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ProjectName = { "ProjectName", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionSettings, ProjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ProjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ProjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_BaseRevision_MetaData[] = {
		{ "Category", "Session Settings" },
		{ "Comment", "/**\n\x09 * Base Revision the session was created at.\n\x09 * Can be specified on the server cmd with `-CONCERTREVISION=`\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Base Revision the session was created at.\nCan be specified on the server cmd with `-CONCERTREVISION=`" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_BaseRevision = { "BaseRevision", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionSettings, BaseRevision), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_BaseRevision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_BaseRevision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ArchiveNameOverride_MetaData[] = {
		{ "Category", "Session Settings" },
		{ "Comment", "/**\n\x09 * Override the default name chosen when archiving this session.\n\x09 * Can be specified on the server cmd with `-CONCERTSAVESESSIONAS=`\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Override the default name chosen when archiving this session.\nCan be specified on the server cmd with `-CONCERTSAVESESSIONAS=`" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ArchiveNameOverride = { "ArchiveNameOverride", nullptr, (EPropertyFlags)0x0010000000024001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionSettings, ArchiveNameOverride), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ArchiveNameOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ArchiveNameOverride_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ProjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_BaseRevision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::NewProp_ArchiveNameOverride,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSessionSettings",
		sizeof(FConcertSessionSettings),
		alignof(FConcertSessionSettings),
		Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSessionSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSessionSettings"), sizeof(FConcertSessionSettings), Get_Z_Construct_UScriptStruct_FConcertSessionSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSessionSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSessionSettings_Hash() { return 2929107164U; }
	void UConcertServerConfig::StaticRegisterNativesUConcertServerConfig()
	{
	}
	UClass* Z_Construct_UClass_UConcertServerConfig_NoRegister()
	{
		return UConcertServerConfig::StaticClass();
	}
	struct Z_Construct_UClass_UConcertServerConfig_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoArchiveOnReboot_MetaData[];
#endif
		static void NewProp_bAutoArchiveOnReboot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoArchiveOnReboot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoArchiveOnShutdown_MetaData[];
#endif
		static void NewProp_bAutoArchiveOnShutdown_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoArchiveOnShutdown;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCleanWorkingDir_MetaData[];
#endif
		static void NewProp_bCleanWorkingDir_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCleanWorkingDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumSessionsToKeep_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumSessionsToKeep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ServerName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultSessionName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AuthorizedClientKeys_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AuthorizedClientKeys_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_AuthorizedClientKeys;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSessionToRestore_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultSessionToRestore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultVersionInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultVersionInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSessionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultSessionSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ServerSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ServerSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorkingDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_WorkingDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArchiveDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ArchiveDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionRepositoryRootDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionRepositoryRootDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMountDefaultSessionRepository_MetaData[];
#endif
		static void NewProp_bMountDefaultSessionRepository_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMountDefaultSessionRepository;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertServerConfig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConcertSettings.h" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnReboot_MetaData[] = {
		{ "Comment", "/**\n\x09 * If true, instruct the server to auto-archive sessions that were left in the working directory because the server did not exit properly rather than\n\x09 * restoring them as 'live' (the default).\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "If true, instruct the server to auto-archive sessions that were left in the working directory because the server did not exit properly rather than\nrestoring them as 'live' (the default)." },
	};
#endif
	void Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnReboot_SetBit(void* Obj)
	{
		((UConcertServerConfig*)Obj)->bAutoArchiveOnReboot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnReboot = { "bAutoArchiveOnReboot", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertServerConfig), &Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnReboot_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnReboot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnReboot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnShutdown_MetaData[] = {
		{ "Comment", "/**\n\x09 * If true, instruct the server to auto-archive live sessions on shutdown.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "If true, instruct the server to auto-archive live sessions on shutdown." },
	};
#endif
	void Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnShutdown_SetBit(void* Obj)
	{
		((UConcertServerConfig*)Obj)->bAutoArchiveOnShutdown = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnShutdown = { "bAutoArchiveOnShutdown", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertServerConfig), &Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnShutdown_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnShutdown_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnShutdown_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bCleanWorkingDir_MetaData[] = {
		{ "Category", "Server Settings" },
		{ "Comment", "/**\n\x09 * Clean server sessions working directory when booting\n\x09 * Can be specified on the server cmd with `-CONCERTCLEAN`\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Clean server sessions working directory when booting\nCan be specified on the server cmd with `-CONCERTCLEAN`" },
	};
#endif
	void Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bCleanWorkingDir_SetBit(void* Obj)
	{
		((UConcertServerConfig*)Obj)->bCleanWorkingDir = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bCleanWorkingDir = { "bCleanWorkingDir", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertServerConfig), &Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bCleanWorkingDir_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bCleanWorkingDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bCleanWorkingDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_NumSessionsToKeep_MetaData[] = {
		{ "Category", "Server Settings" },
		{ "Comment", "/**\n\x09 * Number of archived sessions to keep when booting, or <0 to keep all archived sessions\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Number of archived sessions to keep when booting, or <0 to keep all archived sessions" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_NumSessionsToKeep = { "NumSessionsToKeep", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, NumSessionsToKeep), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_NumSessionsToKeep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_NumSessionsToKeep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerName_MetaData[] = {
		{ "Category", "Server Settings" },
		{ "Comment", "/** \n\x09 * Name of the server, or empty to use the default name.\n\x09 * Can be specified on the server cmd with `-CONCERTSERVER=`\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Name of the server, or empty to use the default name.\nCan be specified on the server cmd with `-CONCERTSERVER=`" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerName = { "ServerName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, ServerName), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionName_MetaData[] = {
		{ "Category", "Session Settings" },
		{ "Comment", "/** \n\x09 * Name of the default session created on the server.\n\x09 * Can be specified on the server cmd with `-CONCERTSESSION=`\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Name of the default session created on the server.\nCan be specified on the server cmd with `-CONCERTSESSION=`" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionName = { "DefaultSessionName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, DefaultSessionName), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionName_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_AuthorizedClientKeys_ElementProp = { "AuthorizedClientKeys", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_AuthorizedClientKeys_MetaData[] = {
		{ "Comment", "/** \n\x09 * A set of keys identifying the clients that can discover and access the server. If empty, the server can be discovered and used by any clients.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "A set of keys identifying the clients that can discover and access the server. If empty, the server can be discovered and used by any clients." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_AuthorizedClientKeys = { "AuthorizedClientKeys", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, AuthorizedClientKeys), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_AuthorizedClientKeys_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_AuthorizedClientKeys_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionToRestore_MetaData[] = {
		{ "Category", "Session Settings" },
		{ "Comment", "/**\n\x09 * Name of the default session to restore on the server.\n\x09 * Set the name of the desired save to restore its content in your session.\n\x09 * Leave this blank if you want to create an empty session.\n\x09 * Can be specified on the editor cmd with `-CONCERTSESSIONTORESTORE=`.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Name of the default session to restore on the server.\nSet the name of the desired save to restore its content in your session.\nLeave this blank if you want to create an empty session.\nCan be specified on the editor cmd with `-CONCERTSESSIONTORESTORE=`." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionToRestore = { "DefaultSessionToRestore", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, DefaultSessionToRestore), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionToRestore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionToRestore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultVersionInfo_MetaData[] = {
		{ "Comment", "/** \n\x09 * The version string for the default server created.\n\x09 * Can be specified on the server cmd with `-CONCERTVERSION=`\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The version string for the default server created.\nCan be specified on the server cmd with `-CONCERTVERSION=`" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultVersionInfo = { "DefaultVersionInfo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, DefaultVersionInfo), Z_Construct_UScriptStruct_FConcertSessionVersionInfo, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultVersionInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultVersionInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionSettings_MetaData[] = {
		{ "Category", "Session Settings" },
		{ "Comment", "/** Default server session settings */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Default server session settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionSettings = { "DefaultSessionSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, DefaultSessionSettings), Z_Construct_UScriptStruct_FConcertSessionSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerSettings_MetaData[] = {
		{ "Category", "Server Settings" },
		{ "Comment", "/** Server & server session settings */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Server & server session settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerSettings = { "ServerSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, ServerSettings), Z_Construct_UScriptStruct_FConcertServerSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_EndpointSettings_MetaData[] = {
		{ "Category", "Endpoint Settings" },
		{ "Comment", "/** Endpoint settings passed down to endpoints on creation */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Endpoint settings passed down to endpoints on creation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_EndpointSettings = { "EndpointSettings", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, EndpointSettings), Z_Construct_UScriptStruct_FConcertEndpointSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_EndpointSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_EndpointSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_WorkingDir_MetaData[] = {
		{ "Comment", "/** The default directory where the server keeps the live session files. Can be specified on the server command line with `-CONCERTWORKINGDIR=`*/" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The default directory where the server keeps the live session files. Can be specified on the server command line with `-CONCERTWORKINGDIR=`" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_WorkingDir = { "WorkingDir", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, WorkingDir), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_WorkingDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_WorkingDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ArchiveDir_MetaData[] = {
		{ "Comment", "/** The default directory where the server keeps the archived session files. Can be specified on the server command line with `-CONCERTSAVEDDIR=`*/" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The default directory where the server keeps the archived session files. Can be specified on the server command line with `-CONCERTSAVEDDIR=`" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ArchiveDir = { "ArchiveDir", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, ArchiveDir), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ArchiveDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ArchiveDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_SessionRepositoryRootDir_MetaData[] = {
		{ "Comment", "/** The root directory where the server creates new session repositories (unless the client request specifies its own root). If empty or invalid, the server will use a default. */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "The root directory where the server creates new session repositories (unless the client request specifies its own root). If empty or invalid, the server will use a default." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_SessionRepositoryRootDir = { "SessionRepositoryRootDir", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertServerConfig, SessionRepositoryRootDir), METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_SessionRepositoryRootDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_SessionRepositoryRootDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bMountDefaultSessionRepository_MetaData[] = {
		{ "Comment", "/** If neither of WorkingDir and ArchiveDir are set, determine whether the server should mount a standard default session repository where new session will be created. */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "If neither of WorkingDir and ArchiveDir are set, determine whether the server should mount a standard default session repository where new session will be created." },
	};
#endif
	void Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bMountDefaultSessionRepository_SetBit(void* Obj)
	{
		((UConcertServerConfig*)Obj)->bMountDefaultSessionRepository = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bMountDefaultSessionRepository = { "bMountDefaultSessionRepository", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertServerConfig), &Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bMountDefaultSessionRepository_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bMountDefaultSessionRepository_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bMountDefaultSessionRepository_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertServerConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnReboot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bAutoArchiveOnShutdown,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bCleanWorkingDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_NumSessionsToKeep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_AuthorizedClientKeys_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_AuthorizedClientKeys,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionToRestore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultVersionInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_DefaultSessionSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ServerSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_EndpointSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_WorkingDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_ArchiveDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_SessionRepositoryRootDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertServerConfig_Statics::NewProp_bMountDefaultSessionRepository,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertServerConfig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertServerConfig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertServerConfig_Statics::ClassParams = {
		&UConcertServerConfig::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertServerConfig_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertServerConfig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertServerConfig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertServerConfig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertServerConfig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertServerConfig, 3243560825);
	template<> CONCERT_API UClass* StaticClass<UConcertServerConfig>()
	{
		return UConcertServerConfig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertServerConfig(Z_Construct_UClass_UConcertServerConfig, &UConcertServerConfig::StaticClass, TEXT("/Script/Concert"), TEXT("UConcertServerConfig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertServerConfig);
	void UConcertClientConfig::StaticRegisterNativesUConcertClientConfig()
	{
	}
	UClass* Z_Construct_UClass_UConcertClientConfig_NoRegister()
	{
		return UConcertClientConfig::StaticClass();
	}
	struct Z_Construct_UClass_UConcertClientConfig_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsHeadless_MetaData[];
#endif
		static void NewProp_bIsHeadless_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsHeadless;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInstallEditorToolbarButton_MetaData[];
#endif
		static void NewProp_bInstallEditorToolbarButton_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInstallEditorToolbarButton;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoConnect_MetaData[];
#endif
		static void NewProp_bAutoConnect_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoConnect;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRetryAutoConnectOnError_MetaData[];
#endif
		static void NewProp_bRetryAutoConnectOnError_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRetryAutoConnectOnError;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultServerURL_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultServerURL;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultSessionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSessionToRestore_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultSessionToRestore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSaveSessionAs_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_DefaultSaveSessionAs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceControlSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SourceControlSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertClientConfig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConcertSettings.h" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bIsHeadless_MetaData[] = {
		{ "Comment", "/**\n\x09 * True if this client should be \"headless\"? (ie, not display any UI).\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "True if this client should be \"headless\"? (ie, not display any UI)." },
	};
#endif
	void Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bIsHeadless_SetBit(void* Obj)
	{
		((UConcertClientConfig*)Obj)->bIsHeadless = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bIsHeadless = { "bIsHeadless", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertClientConfig), &Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bIsHeadless_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bIsHeadless_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bIsHeadless_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bInstallEditorToolbarButton_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/**\n\x09 * True if the Multi-User module should install shortcut button and its drop-down menu in the level editor toolbar.\n\x09 */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "DisplayName", "Enable Multi-User Toolbar Button" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "True if the Multi-User module should install shortcut button and its drop-down menu in the level editor toolbar." },
	};
#endif
	void Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bInstallEditorToolbarButton_SetBit(void* Obj)
	{
		((UConcertClientConfig*)Obj)->bInstallEditorToolbarButton = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bInstallEditorToolbarButton = { "bInstallEditorToolbarButton", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertClientConfig), &Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bInstallEditorToolbarButton_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bInstallEditorToolbarButton_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bInstallEditorToolbarButton_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bAutoConnect_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** \n\x09 * Automatically connect or create default session on default server.\n\x09 * Can be specified on the editor cmd with `-CONCERTAUTOCONNECT` or `-CONCERTAUTOCONNECT=<true/false>`.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Automatically connect or create default session on default server.\nCan be specified on the editor cmd with `-CONCERTAUTOCONNECT` or `-CONCERTAUTOCONNECT=<true/false>`." },
	};
#endif
	void Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bAutoConnect_SetBit(void* Obj)
	{
		((UConcertClientConfig*)Obj)->bAutoConnect = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bAutoConnect = { "bAutoConnect", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertClientConfig), &Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bAutoConnect_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bAutoConnect_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bAutoConnect_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bRetryAutoConnectOnError_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** \n\x09 * If auto-connect is on, retry connecting to the default server/session until it succeeds or the user cancels.\n\x09 * Can be specified on the editor cmd with `-CONCERTRETRYAUTOCONNECTONERROR` or `-CONCERTRETRYAUTOCONNECTONERROR=<true/false>`.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "If auto-connect is on, retry connecting to the default server/session until it succeeds or the user cancels.\nCan be specified on the editor cmd with `-CONCERTRETRYAUTOCONNECTONERROR` or `-CONCERTRETRYAUTOCONNECTONERROR=<true/false>`." },
	};
#endif
	void Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bRetryAutoConnectOnError_SetBit(void* Obj)
	{
		((UConcertClientConfig*)Obj)->bRetryAutoConnectOnError = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bRetryAutoConnectOnError = { "bRetryAutoConnectOnError", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertClientConfig), &Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bRetryAutoConnectOnError_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bRetryAutoConnectOnError_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bRetryAutoConnectOnError_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultServerURL_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** \n\x09 * Default server url (just a name for now) to look for on auto or default connect. \n \x09 * Can be specified on the editor cmd with `-CONCERTSERVER=`.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Default server url (just a name for now) to look for on auto or default connect.\nCan be specified on the editor cmd with `-CONCERTSERVER=`." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultServerURL = { "DefaultServerURL", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertClientConfig, DefaultServerURL), METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultServerURL_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultServerURL_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionName_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** \n\x09 * Default session name to look for on auto connect or default connect.\n\x09 * Can be specified on the editor cmd with `-CONCERTSESSION=`.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "Default session name to look for on auto connect or default connect.\nCan be specified on the editor cmd with `-CONCERTSESSION=`." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionName = { "DefaultSessionName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertClientConfig, DefaultSessionName), METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionToRestore_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/**\n\x09 * If this client create the default session, should the session restore a saved session.\n\x09 * Set the name of the desired save to restore its content in your session.\n\x09 * Leave this blank if you want to create an empty session.\n\x09 * Can be specified on the editor cmd with `-CONCERTSESSIONTORESTORE=`.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "If this client create the default session, should the session restore a saved session.\nSet the name of the desired save to restore its content in your session.\nLeave this blank if you want to create an empty session.\nCan be specified on the editor cmd with `-CONCERTSESSIONTORESTORE=`." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionToRestore = { "DefaultSessionToRestore", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertClientConfig, DefaultSessionToRestore), METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionToRestore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionToRestore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSaveSessionAs_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/**\n\x09 * If this client create the default session, should the session data be saved when it's deleted.\n\x09 * Set the name desired for the save and the session data will be moved in that save when the session is deleted\n\x09 * Leave this blank if you don't want to save the session data.\n\x09 * Can be specified on the editor cmd with `-CONCERTSAVESESSIONAS=`.\n\x09*/" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ToolTip", "If this client create the default session, should the session data be saved when it's deleted.\nSet the name desired for the save and the session data will be moved in that save when the session is deleted\nLeave this blank if you don't want to save the session data.\nCan be specified on the editor cmd with `-CONCERTSAVESESSIONAS=`." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSaveSessionAs = { "DefaultSaveSessionAs", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertClientConfig, DefaultSaveSessionAs), METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSaveSessionAs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSaveSessionAs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_ClientSettings_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/** Client & client session settings */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Client & client session settings" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_ClientSettings = { "ClientSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertClientConfig, ClientSettings), Z_Construct_UScriptStruct_FConcertClientSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_ClientSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_ClientSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_SourceControlSettings_MetaData[] = {
		{ "Category", "Source Control Settings" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_SourceControlSettings = { "SourceControlSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertClientConfig, SourceControlSettings), Z_Construct_UScriptStruct_FConcertSourceControlSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_SourceControlSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_SourceControlSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_EndpointSettings_MetaData[] = {
		{ "Category", "Endpoint Settings" },
		{ "Comment", "/** Endpoint settings passed down to endpoints on creation */" },
		{ "ModuleRelativePath", "Public/ConcertSettings.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Endpoint settings passed down to endpoints on creation" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_EndpointSettings = { "EndpointSettings", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertClientConfig, EndpointSettings), Z_Construct_UScriptStruct_FConcertEndpointSettings, METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_EndpointSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_EndpointSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertClientConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bIsHeadless,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bInstallEditorToolbarButton,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bAutoConnect,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_bRetryAutoConnectOnError,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultServerURL,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSessionToRestore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_DefaultSaveSessionAs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_ClientSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_SourceControlSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertClientConfig_Statics::NewProp_EndpointSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertClientConfig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertClientConfig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertClientConfig_Statics::ClassParams = {
		&UConcertClientConfig::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertClientConfig_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertClientConfig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertClientConfig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertClientConfig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertClientConfig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertClientConfig, 4191348255);
	template<> CONCERT_API UClass* StaticClass<UConcertClientConfig>()
	{
		return UConcertClientConfig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertClientConfig(Z_Construct_UClass_UConcertClientConfig, &UConcertClientConfig::StaticClass, TEXT("/Script/Concert"), TEXT("UConcertClientConfig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertClientConfig);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
