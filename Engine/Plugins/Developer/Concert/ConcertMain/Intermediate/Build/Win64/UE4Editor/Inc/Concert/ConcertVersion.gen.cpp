// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Concert/Public/ConcertVersion.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertVersion() {}
// Cross Module References
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionVersionInfo();
	UPackage* Z_Construct_UPackage__Script_Concert();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertFileVersionInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertEngineVersionInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertCustomVersionInfo();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FConcertSessionVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSessionVersionInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertSessionVersionInfo"), sizeof(FConcertSessionVersionInfo), Get_Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertSessionVersionInfo>()
{
	return FConcertSessionVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSessionVersionInfo(FConcertSessionVersionInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertSessionVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertSessionVersionInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertSessionVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertSessionVersionInfo>(FName(TEXT("ConcertSessionVersionInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertSessionVersionInfo;
	struct Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FileVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EngineVersion;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CustomVersions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomVersions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CustomVersions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds version information for a session */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Holds version information for a session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSessionVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_FileVersion_MetaData[] = {
		{ "Comment", "/** File version info */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "File version info" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_FileVersion = { "FileVersion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionVersionInfo, FileVersion), Z_Construct_UScriptStruct_FConcertFileVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_FileVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_FileVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_EngineVersion_MetaData[] = {
		{ "Comment", "/** Engine version info */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Engine version info" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_EngineVersion = { "EngineVersion", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionVersionInfo, EngineVersion), Z_Construct_UScriptStruct_FConcertEngineVersionInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_EngineVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_EngineVersion_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_CustomVersions_Inner = { "CustomVersions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertCustomVersionInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_CustomVersions_MetaData[] = {
		{ "Comment", "/** Custom version info */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Custom version info" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_CustomVersions = { "CustomVersions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSessionVersionInfo, CustomVersions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_CustomVersions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_CustomVersions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_FileVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_EngineVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_CustomVersions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::NewProp_CustomVersions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertSessionVersionInfo",
		sizeof(FConcertSessionVersionInfo),
		alignof(FConcertSessionVersionInfo),
		Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSessionVersionInfo"), sizeof(FConcertSessionVersionInfo), Get_Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Hash() { return 1528920291U; }
class UScriptStruct* FConcertCustomVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertCustomVersionInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertCustomVersionInfo"), sizeof(FConcertCustomVersionInfo), Get_Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertCustomVersionInfo>()
{
	return FConcertCustomVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertCustomVersionInfo(FConcertCustomVersionInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertCustomVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertCustomVersionInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertCustomVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertCustomVersionInfo>(FName(TEXT("ConcertCustomVersionInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertCustomVersionInfo;
	struct Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FriendlyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FriendlyName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Version_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Version;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds custom version information */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Holds custom version information" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertCustomVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_FriendlyName_MetaData[] = {
		{ "Comment", "/** Friendly name of the version */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Friendly name of the version" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_FriendlyName = { "FriendlyName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertCustomVersionInfo, FriendlyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_FriendlyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_FriendlyName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Key_MetaData[] = {
		{ "Comment", "/** Unique custom key */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Unique custom key" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertCustomVersionInfo, Key), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Key_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Version_MetaData[] = {
		{ "Comment", "/** Custom version */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Custom version" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Version = { "Version", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertCustomVersionInfo, Version), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Version_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Version_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_FriendlyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::NewProp_Version,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertCustomVersionInfo",
		sizeof(FConcertCustomVersionInfo),
		alignof(FConcertCustomVersionInfo),
		Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertCustomVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertCustomVersionInfo"), sizeof(FConcertCustomVersionInfo), Get_Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Hash() { return 3057431805U; }
class UScriptStruct* FConcertEngineVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertEngineVersionInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertEngineVersionInfo"), sizeof(FConcertEngineVersionInfo), Get_Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertEngineVersionInfo>()
{
	return FConcertEngineVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertEngineVersionInfo(FConcertEngineVersionInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertEngineVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertEngineVersionInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertEngineVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertEngineVersionInfo>(FName(TEXT("ConcertEngineVersionInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertEngineVersionInfo;
	struct Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Major_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Major;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Minor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Minor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Patch_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_Patch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Changelist_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Changelist;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds engine version information */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Holds engine version information" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertEngineVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Major_MetaData[] = {
		{ "Comment", "/** Major version number */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Major version number" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Major = { "Major", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertEngineVersionInfo, Major), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Major_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Major_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Minor_MetaData[] = {
		{ "Comment", "/** Minor version number */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Minor version number" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Minor = { "Minor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertEngineVersionInfo, Minor), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Minor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Minor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Patch_MetaData[] = {
		{ "Comment", "/** Patch version number */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Patch version number" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Patch = { "Patch", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertEngineVersionInfo, Patch), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Patch_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Patch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Changelist_MetaData[] = {
		{ "Comment", "/** Changelist number. This is used to arbitrate when Major/Minor/Patch version numbers match */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Changelist number. This is used to arbitrate when Major/Minor/Patch version numbers match" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Changelist = { "Changelist", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertEngineVersionInfo, Changelist), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Changelist_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Changelist_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Major,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Minor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Patch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::NewProp_Changelist,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertEngineVersionInfo",
		sizeof(FConcertEngineVersionInfo),
		alignof(FConcertEngineVersionInfo),
		Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertEngineVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertEngineVersionInfo"), sizeof(FConcertEngineVersionInfo), Get_Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Hash() { return 2665546537U; }
class UScriptStruct* FConcertFileVersionInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERT_API uint32 Get_Z_Construct_UScriptStruct_FConcertFileVersionInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertFileVersionInfo, Z_Construct_UPackage__Script_Concert(), TEXT("ConcertFileVersionInfo"), sizeof(FConcertFileVersionInfo), Get_Z_Construct_UScriptStruct_FConcertFileVersionInfo_Hash());
	}
	return Singleton;
}
template<> CONCERT_API UScriptStruct* StaticStruct<FConcertFileVersionInfo>()
{
	return FConcertFileVersionInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertFileVersionInfo(FConcertFileVersionInfo::StaticStruct, TEXT("/Script/Concert"), TEXT("ConcertFileVersionInfo"), false, nullptr, nullptr);
static struct FScriptStruct_Concert_StaticRegisterNativesFConcertFileVersionInfo
{
	FScriptStruct_Concert_StaticRegisterNativesFConcertFileVersionInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertFileVersionInfo>(FName(TEXT("ConcertFileVersionInfo")));
	}
} ScriptStruct_Concert_StaticRegisterNativesFConcertFileVersionInfo;
	struct Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileVersionUE4_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FileVersionUE4;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileVersionLicenseeUE4_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FileVersionLicenseeUE4;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Holds file version information */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Holds file version information" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertFileVersionInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionUE4_MetaData[] = {
		{ "Comment", "/* UE4 file version */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "UE4 file version" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionUE4 = { "FileVersionUE4", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertFileVersionInfo, FileVersionUE4), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionUE4_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionUE4_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4_MetaData[] = {
		{ "Comment", "/* Licensee file version */" },
		{ "ModuleRelativePath", "Public/ConcertVersion.h" },
		{ "ToolTip", "Licensee file version" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4 = { "FileVersionLicenseeUE4", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertFileVersionInfo, FileVersionLicenseeUE4), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionUE4,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::NewProp_FileVersionLicenseeUE4,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Concert,
		nullptr,
		&NewStructOps,
		"ConcertFileVersionInfo",
		sizeof(FConcertFileVersionInfo),
		alignof(FConcertFileVersionInfo),
		Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertFileVersionInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertFileVersionInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Concert();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertFileVersionInfo"), sizeof(FConcertFileVersionInfo), Get_Z_Construct_UScriptStruct_FConcertFileVersionInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertFileVersionInfo_Hash() { return 2728109751U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
