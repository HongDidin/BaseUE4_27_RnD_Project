// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERT_ConcertMessageData_generated_h
#error "ConcertMessageData.generated.h already included, missing '#pragma once' in ConcertMessageData.h"
#endif
#define CONCERT_ConcertMessageData_generated_h

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_288_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSessionSerializedPayload_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSessionSerializedPayload>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_257_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertByteArray_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertByteArray>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_217_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSessionFilter_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSessionFilter>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_178_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSessionInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSessionInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_162_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSessionClientInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSessionClientInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_103_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertClientInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_75_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertServerInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertServerInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h_47_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertInstanceInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertInstanceInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertMessageData_h


#define FOREACH_ENUM_ECONCERTPAYLOADSERIALIZATIONMETHOD(op) \
	op(EConcertPayloadSerializationMethod::Standard) \
	op(EConcertPayloadSerializationMethod::Cbor) 

enum class EConcertPayloadSerializationMethod : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertPayloadSerializationMethod>();

#define FOREACH_ENUM_ECONCERTPAYLOADCOMPRESSIONTYPE(op) \
	op(EConcertPayloadCompressionType::None) \
	op(EConcertPayloadCompressionType::Heuristic) \
	op(EConcertPayloadCompressionType::Always) 

enum class EConcertPayloadCompressionType : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertPayloadCompressionType>();

#define FOREACH_ENUM_ECONCERTSERVERFLAGS(op) \
	op(EConcertServerFlags::None) \
	op(EConcertServerFlags::IgnoreSessionRequirement) 

enum class EConcertServerFlags : uint8;
template<> CONCERT_API UEnum* StaticEnum<EConcertServerFlags>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
