// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERT_ConcertVersion_generated_h
#error "ConcertVersion.generated.h already included, missing '#pragma once' in ConcertVersion.h"
#endif
#define CONCERT_ConcertVersion_generated_h

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertVersion_h_99_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSessionVersionInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertSessionVersionInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertVersion_h_74_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertCustomVersionInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertCustomVersionInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertVersion_h_45_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertEngineVersionInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertEngineVersionInfo>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertVersion_h_24_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertFileVersionInfo_Statics; \
	CONCERT_API static class UScriptStruct* StaticStruct();


template<> CONCERT_API UScriptStruct* StaticStruct<struct FConcertFileVersionInfo>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertMain_Source_Concert_Public_ConcertVersion_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
