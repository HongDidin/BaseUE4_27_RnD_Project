// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTTRANSPORT_ConcertTransportSettings_generated_h
#error "ConcertTransportSettings.generated.h already included, missing '#pragma once' in ConcertTransportSettings.h"
#endif
#define CONCERTTRANSPORT_ConcertTransportSettings_generated_h

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertEndpointSettings_Statics; \
	CONCERTTRANSPORT_API static class UScriptStruct* StaticStruct();


template<> CONCERTTRANSPORT_API UScriptStruct* StaticStruct<struct FConcertEndpointSettings>();

#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertEndpointConfig(); \
	friend struct Z_Construct_UClass_UConcertEndpointConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertEndpointConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertTransport"), NO_API) \
	DECLARE_SERIALIZER(UConcertEndpointConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUConcertEndpointConfig(); \
	friend struct Z_Construct_UClass_UConcertEndpointConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertEndpointConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertTransport"), NO_API) \
	DECLARE_SERIALIZER(UConcertEndpointConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertEndpointConfig(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertEndpointConfig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertEndpointConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertEndpointConfig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertEndpointConfig(UConcertEndpointConfig&&); \
	NO_API UConcertEndpointConfig(const UConcertEndpointConfig&); \
public:


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertEndpointConfig(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertEndpointConfig(UConcertEndpointConfig&&); \
	NO_API UConcertEndpointConfig(const UConcertEndpointConfig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertEndpointConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertEndpointConfig); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertEndpointConfig)


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_34_PROLOG
#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_INCLASS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERTTRANSPORT_API UClass* StaticClass<class UConcertEndpointConfig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertMain_Source_ConcertTransport_Public_ConcertTransportSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
