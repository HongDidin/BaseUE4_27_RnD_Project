// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncCore/Public/ConcertPresenceEvents.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertPresenceEvents() {}
// Cross Module References
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncCore();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceEventBase();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertLaserData();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References

static_assert(std::is_polymorphic<FConcertClientVRPresenceUpdateEvent>() == std::is_polymorphic<FConcertClientPresenceEventBase>(), "USTRUCT FConcertClientVRPresenceUpdateEvent cannot be polymorphic unless super FConcertClientPresenceEventBase is polymorphic");

class UScriptStruct* FConcertClientVRPresenceUpdateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertClientVRPresenceUpdateEvent"), sizeof(FConcertClientVRPresenceUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertClientVRPresenceUpdateEvent>()
{
	return FConcertClientVRPresenceUpdateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientVRPresenceUpdateEvent(FConcertClientVRPresenceUpdateEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertClientVRPresenceUpdateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientVRPresenceUpdateEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientVRPresenceUpdateEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientVRPresenceUpdateEvent>(FName(TEXT("ConcertClientVRPresenceUpdateEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientVRPresenceUpdateEvent;
	struct Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftMotionControllerPosition_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftMotionControllerPosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LeftMotionControllerOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LeftMotionControllerOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightMotionControllerPosition_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightMotionControllerPosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RightMotionControllerOrientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RightMotionControllerOrientation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Lasers_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Lasers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientVRPresenceUpdateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerPosition_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerPosition = { "LeftMotionControllerPosition", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientVRPresenceUpdateEvent, LeftMotionControllerPosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerPosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerPosition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerOrientation_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerOrientation = { "LeftMotionControllerOrientation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientVRPresenceUpdateEvent, LeftMotionControllerOrientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerPosition_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerPosition = { "RightMotionControllerPosition", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientVRPresenceUpdateEvent, RightMotionControllerPosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerPosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerPosition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerOrientation_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerOrientation = { "RightMotionControllerOrientation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientVRPresenceUpdateEvent, RightMotionControllerOrientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerOrientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerOrientation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_Lasers_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_Lasers = { "Lasers", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, CPP_ARRAY_DIM(Lasers, FConcertClientVRPresenceUpdateEvent), STRUCT_OFFSET(FConcertClientVRPresenceUpdateEvent, Lasers), Z_Construct_UScriptStruct_FConcertLaserData, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_Lasers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_Lasers_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_LeftMotionControllerOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerPosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_RightMotionControllerOrientation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::NewProp_Lasers,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertClientPresenceEventBase,
		&NewStructOps,
		"ConcertClientVRPresenceUpdateEvent",
		sizeof(FConcertClientVRPresenceUpdateEvent),
		alignof(FConcertClientVRPresenceUpdateEvent),
		Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientVRPresenceUpdateEvent"), sizeof(FConcertClientVRPresenceUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Hash() { return 1847488673U; }
class UScriptStruct* FConcertLaserData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertLaserData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertLaserData, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertLaserData"), sizeof(FConcertLaserData), Get_Z_Construct_UScriptStruct_FConcertLaserData_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertLaserData>()
{
	return FConcertLaserData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertLaserData(FConcertLaserData::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertLaserData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertLaserData
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertLaserData()
	{
		UScriptStruct::DeferCppStructOps<FConcertLaserData>(FName(TEXT("ConcertLaserData")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertLaserData;
	struct Z_Construct_UScriptStruct_FConcertLaserData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LaserStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LaserEnd;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLaserData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertLaserData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserStart_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserStart = { "LaserStart", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLaserData, LaserStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserEnd_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserEnd = { "LaserEnd", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertLaserData, LaserEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserEnd_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertLaserData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertLaserData_Statics::NewProp_LaserEnd,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertLaserData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertLaserData",
		sizeof(FConcertLaserData),
		alignof(FConcertLaserData),
		Z_Construct_UScriptStruct_FConcertLaserData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLaserData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertLaserData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertLaserData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertLaserData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertLaserData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertLaserData"), sizeof(FConcertLaserData), Get_Z_Construct_UScriptStruct_FConcertLaserData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertLaserData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertLaserData_Hash() { return 815819158U; }

static_assert(std::is_polymorphic<FConcertClientDesktopPresenceUpdateEvent>() == std::is_polymorphic<FConcertClientPresenceEventBase>(), "USTRUCT FConcertClientDesktopPresenceUpdateEvent cannot be polymorphic unless super FConcertClientPresenceEventBase is polymorphic");

class UScriptStruct* FConcertClientDesktopPresenceUpdateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertClientDesktopPresenceUpdateEvent"), sizeof(FConcertClientDesktopPresenceUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertClientDesktopPresenceUpdateEvent>()
{
	return FConcertClientDesktopPresenceUpdateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent(FConcertClientDesktopPresenceUpdateEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertClientDesktopPresenceUpdateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientDesktopPresenceUpdateEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientDesktopPresenceUpdateEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientDesktopPresenceUpdateEvent>(FName(TEXT("ConcertClientDesktopPresenceUpdateEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientDesktopPresenceUpdateEvent;
	struct Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TraceStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TraceEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TraceEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMovingCamera_MetaData[];
#endif
		static void NewProp_bMovingCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMovingCamera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientDesktopPresenceUpdateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceStart_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceStart = { "TraceStart", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientDesktopPresenceUpdateEvent, TraceStart), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceEnd_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceEnd = { "TraceEnd", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientDesktopPresenceUpdateEvent, TraceEnd), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_bMovingCamera_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_bMovingCamera_SetBit(void* Obj)
	{
		((FConcertClientDesktopPresenceUpdateEvent*)Obj)->bMovingCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_bMovingCamera = { "bMovingCamera", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertClientDesktopPresenceUpdateEvent), &Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_bMovingCamera_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_bMovingCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_bMovingCamera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_TraceEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::NewProp_bMovingCamera,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertClientPresenceEventBase,
		&NewStructOps,
		"ConcertClientDesktopPresenceUpdateEvent",
		sizeof(FConcertClientDesktopPresenceUpdateEvent),
		alignof(FConcertClientDesktopPresenceUpdateEvent),
		Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientDesktopPresenceUpdateEvent"), sizeof(FConcertClientDesktopPresenceUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Hash() { return 1771821461U; }

static_assert(std::is_polymorphic<FConcertClientPresenceDataUpdateEvent>() == std::is_polymorphic<FConcertClientPresenceEventBase>(), "USTRUCT FConcertClientPresenceDataUpdateEvent cannot be polymorphic unless super FConcertClientPresenceEventBase is polymorphic");

class UScriptStruct* FConcertClientPresenceDataUpdateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertClientPresenceDataUpdateEvent"), sizeof(FConcertClientPresenceDataUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertClientPresenceDataUpdateEvent>()
{
	return FConcertClientPresenceDataUpdateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientPresenceDataUpdateEvent(FConcertClientPresenceDataUpdateEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertClientPresenceDataUpdateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceDataUpdateEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceDataUpdateEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientPresenceDataUpdateEvent>(FName(TEXT("ConcertClientPresenceDataUpdateEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceDataUpdateEvent;
	struct Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_WorldPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Position_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Position;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Orientation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Orientation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientPresenceDataUpdateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_WorldPath_MetaData[] = {
		{ "Comment", "/** The non-PIE/SIE world path. In PIE/SIE, the world context and its path is different than the non-PIE/SIE context.\n\x09    For presence management, we are interested in the Non-PIE/SIE world path and this is what is emitted even if the user is in PIE/SIE. */" },
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
		{ "ToolTip", "The non-PIE/SIE world path. In PIE/SIE, the world context and its path is different than the non-PIE/SIE context.\n          For presence management, we are interested in the Non-PIE/SIE world path and this is what is emitted even if the user is in PIE/SIE." },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_WorldPath = { "WorldPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientPresenceDataUpdateEvent, WorldPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_WorldPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_WorldPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Position_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Position = { "Position", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientPresenceDataUpdateEvent, Position), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Position_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Position_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Orientation_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Orientation = { "Orientation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientPresenceDataUpdateEvent, Orientation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Orientation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Orientation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_WorldPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Position,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::NewProp_Orientation,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertClientPresenceEventBase,
		&NewStructOps,
		"ConcertClientPresenceDataUpdateEvent",
		sizeof(FConcertClientPresenceDataUpdateEvent),
		alignof(FConcertClientPresenceDataUpdateEvent),
		Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientPresenceDataUpdateEvent"), sizeof(FConcertClientPresenceDataUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Hash() { return 78892801U; }
class UScriptStruct* FConcertClientPresenceInVREvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertClientPresenceInVREvent"), sizeof(FConcertClientPresenceInVREvent), Get_Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertClientPresenceInVREvent>()
{
	return FConcertClientPresenceInVREvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientPresenceInVREvent(FConcertClientPresenceInVREvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertClientPresenceInVREvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceInVREvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceInVREvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientPresenceInVREvent>(FName(TEXT("ConcertClientPresenceInVREvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceInVREvent;
	struct Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VRDevice_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_VRDevice;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientPresenceInVREvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::NewProp_VRDevice_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::NewProp_VRDevice = { "VRDevice", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientPresenceInVREvent, VRDevice), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::NewProp_VRDevice_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::NewProp_VRDevice_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::NewProp_VRDevice,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertClientPresenceInVREvent",
		sizeof(FConcertClientPresenceInVREvent),
		alignof(FConcertClientPresenceInVREvent),
		Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientPresenceInVREvent"), sizeof(FConcertClientPresenceInVREvent), Get_Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Hash() { return 1926688260U; }
class UScriptStruct* FConcertClientPresenceVisibilityUpdateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertClientPresenceVisibilityUpdateEvent"), sizeof(FConcertClientPresenceVisibilityUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertClientPresenceVisibilityUpdateEvent>()
{
	return FConcertClientPresenceVisibilityUpdateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent(FConcertClientPresenceVisibilityUpdateEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertClientPresenceVisibilityUpdateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceVisibilityUpdateEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceVisibilityUpdateEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientPresenceVisibilityUpdateEvent>(FName(TEXT("ConcertClientPresenceVisibilityUpdateEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceVisibilityUpdateEvent;
	struct Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifiedEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bVisibility_MetaData[];
#endif
		static void NewProp_bVisibility_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bVisibility;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientPresenceVisibilityUpdateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_ModifiedEndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_ModifiedEndpointId = { "ModifiedEndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientPresenceVisibilityUpdateEvent, ModifiedEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_ModifiedEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_ModifiedEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_bVisibility_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_bVisibility_SetBit(void* Obj)
	{
		((FConcertClientPresenceVisibilityUpdateEvent*)Obj)->bVisibility = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_bVisibility = { "bVisibility", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertClientPresenceVisibilityUpdateEvent), &Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_bVisibility_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_bVisibility_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_bVisibility_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_ModifiedEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::NewProp_bVisibility,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertClientPresenceVisibilityUpdateEvent",
		sizeof(FConcertClientPresenceVisibilityUpdateEvent),
		alignof(FConcertClientPresenceVisibilityUpdateEvent),
		Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientPresenceVisibilityUpdateEvent"), sizeof(FConcertClientPresenceVisibilityUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Hash() { return 1260193153U; }
class UScriptStruct* FConcertClientPresenceEventBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientPresenceEventBase, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertClientPresenceEventBase"), sizeof(FConcertClientPresenceEventBase), Get_Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertClientPresenceEventBase>()
{
	return FConcertClientPresenceEventBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientPresenceEventBase(FConcertClientPresenceEventBase::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertClientPresenceEventBase"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceEventBase
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceEventBase()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientPresenceEventBase>(FName(TEXT("ConcertClientPresenceEventBase")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertClientPresenceEventBase;
	struct Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionUpdateIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_TransactionUpdateIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientPresenceEventBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::NewProp_TransactionUpdateIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertPresenceEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::NewProp_TransactionUpdateIndex = { "TransactionUpdateIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientPresenceEventBase, TransactionUpdateIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::NewProp_TransactionUpdateIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::NewProp_TransactionUpdateIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::NewProp_TransactionUpdateIndex,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertClientPresenceEventBase",
		sizeof(FConcertClientPresenceEventBase),
		alignof(FConcertClientPresenceEventBase),
		Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientPresenceEventBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientPresenceEventBase"), sizeof(FConcertClientPresenceEventBase), Get_Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Hash() { return 2310140021U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
