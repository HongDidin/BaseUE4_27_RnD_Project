// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCORE_ConcertSyncSettings_generated_h
#error "ConcertSyncSettings.generated.h already included, missing '#pragma once' in ConcertSyncSettings.h"
#endif
#define CONCERTSYNCCORE_ConcertSyncSettings_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_30_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPackageClassFilter_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FPackageClassFilter>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTransactionClassFilter_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FTransactionClassFilter>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertSyncConfig(); \
	friend struct Z_Construct_UClass_UConcertSyncConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertSyncConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertSyncCore"), NO_API) \
	DECLARE_SERIALIZER(UConcertSyncConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("ConcertSyncCore");} \



#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUConcertSyncConfig(); \
	friend struct Z_Construct_UClass_UConcertSyncConfig_Statics; \
public: \
	DECLARE_CLASS(UConcertSyncConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertSyncCore"), NO_API) \
	DECLARE_SERIALIZER(UConcertSyncConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("ConcertSyncCore");} \



#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertSyncConfig(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertSyncConfig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertSyncConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertSyncConfig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertSyncConfig(UConcertSyncConfig&&); \
	NO_API UConcertSyncConfig(const UConcertSyncConfig&); \
public:


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertSyncConfig(UConcertSyncConfig&&); \
	NO_API UConcertSyncConfig(const UConcertSyncConfig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertSyncConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertSyncConfig); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConcertSyncConfig)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_46_PROLOG
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_INCLASS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h_49_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERTSYNCCORE_API UClass* StaticClass<class UConcertSyncConfig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
