// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCORE_ConcertWorkspaceMessages_generated_h
#error "ConcertWorkspaceMessages.generated.h already included, missing '#pragma once' in ConcertWorkspaceMessages.h"
#endif
#define CONCERTSYNCCORE_ConcertWorkspaceMessages_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_207_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertIgnoreActivityStateChangedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_184_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertPlaySessionEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_163_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertResourceLockResponse>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_148_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertResourceLockRequest>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_133_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertResourceLockEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_116_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertPackageRejectedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_107_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertPackageUpdateEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_98_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertServerLogging_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertServerLogging>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_88_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncEventResponse>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_73_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncEventRequest>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_66_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertWorkspaceSyncCompletedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertWorkspaceSyncRequestedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_39_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertWorkspaceSyncEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertWorkspaceSyncLockEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_30_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertWorkspaceSyncEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertWorkspaceSyncActivityEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertWorkspaceSyncEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertWorkspaceSyncEndpointEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h_12_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertWorkspaceSyncEventBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertWorkspaceMessages_h


#define FOREACH_ENUM_ECONCERTPLAYSESSIONEVENTTYPE(op) \
	op(EConcertPlaySessionEventType::None) \
	op(EConcertPlaySessionEventType::BeginPlay) \
	op(EConcertPlaySessionEventType::SwitchPlay) \
	op(EConcertPlaySessionEventType::EndPlay) 

enum class EConcertPlaySessionEventType : uint8;
template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertPlaySessionEventType>();

#define FOREACH_ENUM_ECONCERTRESOURCELOCKTYPE(op) \
	op(EConcertResourceLockType::None) \
	op(EConcertResourceLockType::Lock) \
	op(EConcertResourceLockType::Unlock) 

enum class EConcertResourceLockType : uint8;
template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertResourceLockType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
