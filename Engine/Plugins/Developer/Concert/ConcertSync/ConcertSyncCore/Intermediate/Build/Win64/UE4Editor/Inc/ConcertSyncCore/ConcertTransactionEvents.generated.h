// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCORE_ConcertTransactionEvents_generated_h
#error "ConcertTransactionEvents.generated.h already included, missing '#pragma once' in ConcertTransactionEvents.h"
#endif
#define CONCERTSYNCCORE_ConcertTransactionEvents_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_185_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertTransactionRejectedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_179_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertTransactionEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertTransactionSnapshotEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_167_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertTransactionEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertTransactionFinalizedEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_137_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertTransactionEventBase>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_116_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertExportedObject_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertExportedObject>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_104_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSerializedPropertyData>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_77_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSerializedObjectData>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h_28_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertObjectId_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertObjectId>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertTransactionEvents_h


#define FOREACH_ENUM_ETRANSACTIONFILTERRESULT(op) \
	op(ETransactionFilterResult::IncludeObject) \
	op(ETransactionFilterResult::ExcludeObject) \
	op(ETransactionFilterResult::ExcludeTransaction) \
	op(ETransactionFilterResult::UseDefault) 

enum class ETransactionFilterResult : uint8;
template<> CONCERTSYNCCORE_API UEnum* StaticEnum<ETransactionFilterResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
