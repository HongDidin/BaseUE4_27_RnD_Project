// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCORE_ConcertSyncSessionTypes_generated_h
#error "ConcertSyncSessionTypes.generated.h already included, missing '#pragma once' in ConcertSyncSessionTypes.h"
#endif
#define CONCERTSYNCCORE_ConcertSyncSessionTypes_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_378_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivitySummary Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncPackageActivitySummary>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_339_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivitySummary Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncTransactionActivitySummary>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_308_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivitySummary Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncLockActivitySummary>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_289_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivitySummary Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncConnectionActivitySummary>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_267_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics; \
	static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncActivitySummary>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_251_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivity Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncPackageActivity>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_235_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivity Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncTransactionActivity>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_219_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivity Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncLockActivity>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_203_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertSyncActivity Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncConnectionActivity>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_168_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncActivity_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncActivity>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_130_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncPackageEventMetaData>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_115_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncPackageEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_104_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncTransactionEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_89_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncLockEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_78_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncConnectionEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_63_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncEndpointIdAndData>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h_52_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertSyncEndpointData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertSyncSessionTypes_h


#define FOREACH_ENUM_ECONCERTSYNCTRANSACTIONACTIVITYSUMMARYTYPE(op) \
	op(EConcertSyncTransactionActivitySummaryType::Added) \
	op(EConcertSyncTransactionActivitySummaryType::Updated) \
	op(EConcertSyncTransactionActivitySummaryType::Renamed) \
	op(EConcertSyncTransactionActivitySummaryType::Deleted) 

enum class EConcertSyncTransactionActivitySummaryType : uint8;
template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncTransactionActivitySummaryType>();

#define FOREACH_ENUM_ECONCERTSYNCACTIVITYEVENTTYPE(op) \
	op(EConcertSyncActivityEventType::None) \
	op(EConcertSyncActivityEventType::Connection) \
	op(EConcertSyncActivityEventType::Lock) \
	op(EConcertSyncActivityEventType::Transaction) \
	op(EConcertSyncActivityEventType::Package) 

enum class EConcertSyncActivityEventType : uint8;
template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncActivityEventType>();

#define FOREACH_ENUM_ECONCERTSYNCLOCKEVENTTYPE(op) \
	op(EConcertSyncLockEventType::Locked) \
	op(EConcertSyncLockEventType::Unlocked) 

enum class EConcertSyncLockEventType : uint8;
template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncLockEventType>();

#define FOREACH_ENUM_ECONCERTSYNCCONNECTIONEVENTTYPE(op) \
	op(EConcertSyncConnectionEventType::Connected) \
	op(EConcertSyncConnectionEventType::Disconnected) 

enum class EConcertSyncConnectionEventType : uint8;
template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncConnectionEventType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
