// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncCore/Public/ConcertSequencerMessages.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertSequencerMessages() {}
// Cross Module References
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertMovieScenePlayerStatus();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncCore();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerState();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerStateEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerCloseEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerOpenEvent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQualifiedFrameTime();
// End Cross Module References
	static UEnum* EConcertMovieScenePlayerStatus_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertMovieScenePlayerStatus, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertMovieScenePlayerStatus"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertMovieScenePlayerStatus>()
	{
		return EConcertMovieScenePlayerStatus_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertMovieScenePlayerStatus(EConcertMovieScenePlayerStatus_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertMovieScenePlayerStatus"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertMovieScenePlayerStatus_Hash() { return 4113685948U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertMovieScenePlayerStatus()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertMovieScenePlayerStatus"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertMovieScenePlayerStatus_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertMovieScenePlayerStatus::Stopped", (int64)EConcertMovieScenePlayerStatus::Stopped },
				{ "EConcertMovieScenePlayerStatus::Playing", (int64)EConcertMovieScenePlayerStatus::Playing },
				{ "EConcertMovieScenePlayerStatus::Scrubbing", (int64)EConcertMovieScenePlayerStatus::Scrubbing },
				{ "EConcertMovieScenePlayerStatus::Jumping", (int64)EConcertMovieScenePlayerStatus::Jumping },
				{ "EConcertMovieScenePlayerStatus::Stepping", (int64)EConcertMovieScenePlayerStatus::Stepping },
				{ "EConcertMovieScenePlayerStatus::Paused", (int64)EConcertMovieScenePlayerStatus::Paused },
				{ "EConcertMovieScenePlayerStatus::MAX", (int64)EConcertMovieScenePlayerStatus::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n * Enum for the current Sequencer player status, should match EMovieScenePlayerStatus::Type\n * Defined here to not have a dependency on the MovieScene module.\n */" },
				{ "Jumping.Name", "EConcertMovieScenePlayerStatus::Jumping" },
				{ "MAX.Name", "EConcertMovieScenePlayerStatus::MAX" },
				{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
				{ "Paused.Name", "EConcertMovieScenePlayerStatus::Paused" },
				{ "Playing.Name", "EConcertMovieScenePlayerStatus::Playing" },
				{ "Scrubbing.Name", "EConcertMovieScenePlayerStatus::Scrubbing" },
				{ "Stepping.Name", "EConcertMovieScenePlayerStatus::Stepping" },
				{ "Stopped.Name", "EConcertMovieScenePlayerStatus::Stopped" },
				{ "ToolTip", "Enum for the current Sequencer player status, should match EMovieScenePlayerStatus::Type\nDefined here to not have a dependency on the MovieScene module." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertMovieScenePlayerStatus",
				"EConcertMovieScenePlayerStatus",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertSequencerStateSyncEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSequencerStateSyncEvent"), sizeof(FConcertSequencerStateSyncEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSequencerStateSyncEvent>()
{
	return FConcertSequencerStateSyncEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSequencerStateSyncEvent(FConcertSequencerStateSyncEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSequencerStateSyncEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerStateSyncEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerStateSyncEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSequencerStateSyncEvent>(FName(TEXT("ConcertSequencerStateSyncEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerStateSyncEvent;
	struct Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SequencerStates_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SequencerStates_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SequencerStates;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Event that represent the current open sequencer states to a newly connected client\n */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "Event that represent the current open sequencer states to a newly connected client" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSequencerStateSyncEvent>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewProp_SequencerStates_Inner = { "SequencerStates", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSequencerState, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewProp_SequencerStates_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewProp_SequencerStates = { "SequencerStates", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerStateSyncEvent, SequencerStates), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewProp_SequencerStates_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewProp_SequencerStates_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewProp_SequencerStates_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::NewProp_SequencerStates,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSequencerStateSyncEvent",
		sizeof(FConcertSequencerStateSyncEvent),
		alignof(FConcertSequencerStateSyncEvent),
		Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSequencerStateSyncEvent"), sizeof(FConcertSequencerStateSyncEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerStateSyncEvent_Hash() { return 174823971U; }
class UScriptStruct* FConcertSequencerStateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSequencerStateEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSequencerStateEvent"), sizeof(FConcertSequencerStateEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSequencerStateEvent>()
{
	return FConcertSequencerStateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSequencerStateEvent(FConcertSequencerStateEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSequencerStateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerStateEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerStateEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSequencerStateEvent>(FName(TEXT("ConcertSequencerStateEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerStateEvent;
	struct Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_State_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_State;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Event that signals a sequencer UI has changed the current state\n */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "Event that signals a sequencer UI has changed the current state" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSequencerStateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::NewProp_State_MetaData[] = {
		{ "Comment", "/** The new state that the sequence is at */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "The new state that the sequence is at" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::NewProp_State = { "State", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerStateEvent, State), Z_Construct_UScriptStruct_FConcertSequencerState, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::NewProp_State_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::NewProp_State_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::NewProp_State,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSequencerStateEvent",
		sizeof(FConcertSequencerStateEvent),
		alignof(FConcertSequencerStateEvent),
		Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerStateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSequencerStateEvent"), sizeof(FConcertSequencerStateEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerStateEvent_Hash() { return 406091133U; }
class UScriptStruct* FConcertSequencerCloseEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSequencerCloseEvent"), sizeof(FConcertSequencerCloseEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSequencerCloseEvent>()
{
	return FConcertSequencerCloseEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSequencerCloseEvent(FConcertSequencerCloseEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSequencerCloseEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerCloseEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerCloseEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSequencerCloseEvent>(FName(TEXT("ConcertSequencerCloseEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerCloseEvent;
	struct Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SequenceObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SequenceObjectPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMasterClose_MetaData[];
#endif
		static void NewProp_bMasterClose_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMasterClose;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Event that signals a Sequencer just been closed.\n */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "Event that signals a Sequencer just been closed." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSequencerCloseEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_SequenceObjectPath_MetaData[] = {
		{ "Comment", "/** The full path name to the root sequence of the sequencer that just closed. */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "The full path name to the root sequence of the sequencer that just closed." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_SequenceObjectPath = { "SequenceObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerCloseEvent, SequenceObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_SequenceObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_SequenceObjectPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_bMasterClose_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_bMasterClose_SetBit(void* Obj)
	{
		((FConcertSequencerCloseEvent*)Obj)->bMasterClose = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_bMasterClose = { "bMasterClose", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSequencerCloseEvent), &Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_bMasterClose_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_bMasterClose_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_bMasterClose_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_SequenceObjectPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::NewProp_bMasterClose,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSequencerCloseEvent",
		sizeof(FConcertSequencerCloseEvent),
		alignof(FConcertSequencerCloseEvent),
		Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerCloseEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSequencerCloseEvent"), sizeof(FConcertSequencerCloseEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerCloseEvent_Hash() { return 2230340184U; }
class UScriptStruct* FConcertSequencerOpenEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSequencerOpenEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSequencerOpenEvent"), sizeof(FConcertSequencerOpenEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSequencerOpenEvent>()
{
	return FConcertSequencerOpenEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSequencerOpenEvent(FConcertSequencerOpenEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSequencerOpenEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerOpenEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerOpenEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSequencerOpenEvent>(FName(TEXT("ConcertSequencerOpenEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerOpenEvent;
	struct Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SequenceObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SequenceObjectPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Event that signals a Sequencer just been opened.\n */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "Event that signals a Sequencer just been opened." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSequencerOpenEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::NewProp_SequenceObjectPath_MetaData[] = {
		{ "Comment", "/** The full path name to the root sequence of the sequencer that just opened. */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "The full path name to the root sequence of the sequencer that just opened." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::NewProp_SequenceObjectPath = { "SequenceObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerOpenEvent, SequenceObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::NewProp_SequenceObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::NewProp_SequenceObjectPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::NewProp_SequenceObjectPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSequencerOpenEvent",
		sizeof(FConcertSequencerOpenEvent),
		alignof(FConcertSequencerOpenEvent),
		Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerOpenEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSequencerOpenEvent"), sizeof(FConcertSequencerOpenEvent), Get_Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerOpenEvent_Hash() { return 3853661338U; }
class UScriptStruct* FConcertSequencerState::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerState_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSequencerState, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSequencerState"), sizeof(FConcertSequencerState), Get_Z_Construct_UScriptStruct_FConcertSequencerState_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSequencerState>()
{
	return FConcertSequencerState::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSequencerState(FConcertSequencerState::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSequencerState"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerState
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerState()
	{
		UScriptStruct::DeferCppStructOps<FConcertSequencerState>(FName(TEXT("ConcertSequencerState")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSequencerState;
	struct Z_Construct_UScriptStruct_FConcertSequencerState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SequenceObjectPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SequenceObjectPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Time_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Time;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PlayerStatus_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayerStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PlayerStatus;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaybackSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PlaybackSpeed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerState_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSequencerState>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_SequenceObjectPath_MetaData[] = {
		{ "Comment", "/** The full path name to the root sequence that is open on the sequencer */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "The full path name to the root sequence that is open on the sequencer" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_SequenceObjectPath = { "SequenceObjectPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerState, SequenceObjectPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_SequenceObjectPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_SequenceObjectPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_Time_MetaData[] = {
		{ "Comment", "/** The time that the sequence is at */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "The time that the sequence is at" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_Time = { "Time", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerState, Time), Z_Construct_UScriptStruct_FQualifiedFrameTime, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_Time_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_Time_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlayerStatus_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlayerStatus_MetaData[] = {
		{ "Comment", "/** The current status of the sequencer player */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "The current status of the sequencer player" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlayerStatus = { "PlayerStatus", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerState, PlayerStatus), Z_Construct_UEnum_ConcertSyncCore_EConcertMovieScenePlayerStatus, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlayerStatus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlayerStatus_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlaybackSpeed_MetaData[] = {
		{ "Comment", "/** The current playback speed */" },
		{ "ModuleRelativePath", "Public/ConcertSequencerMessages.h" },
		{ "ToolTip", "The current playback speed" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlaybackSpeed = { "PlaybackSpeed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSequencerState, PlaybackSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlaybackSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlaybackSpeed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSequencerState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_SequenceObjectPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_Time,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlayerStatus_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlayerStatus,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSequencerState_Statics::NewProp_PlaybackSpeed,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSequencerState_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSequencerState",
		sizeof(FConcertSequencerState),
		alignof(FConcertSequencerState),
		Z_Construct_UScriptStruct_FConcertSequencerState_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSequencerState_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSequencerState()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerState_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSequencerState"), sizeof(FConcertSequencerState), Get_Z_Construct_UScriptStruct_FConcertSequencerState_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSequencerState_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSequencerState_Hash() { return 3493776977U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
