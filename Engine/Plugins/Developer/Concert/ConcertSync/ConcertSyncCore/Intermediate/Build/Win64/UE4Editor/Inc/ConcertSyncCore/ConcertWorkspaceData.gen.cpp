// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncCore/Public/ConcertWorkspaceData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertWorkspaceData() {}
// Cross Module References
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncCore();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPackage();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPackageInfo();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertByteArray();
// End Cross Module References
	static UEnum* EConcertPackageUpdateType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertPackageUpdateType"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertPackageUpdateType>()
	{
		return EConcertPackageUpdateType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertPackageUpdateType(EConcertPackageUpdateType_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertPackageUpdateType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType_Hash() { return 2107106590U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertPackageUpdateType"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertPackageUpdateType::Dummy", (int64)EConcertPackageUpdateType::Dummy },
				{ "EConcertPackageUpdateType::Added", (int64)EConcertPackageUpdateType::Added },
				{ "EConcertPackageUpdateType::Saved", (int64)EConcertPackageUpdateType::Saved },
				{ "EConcertPackageUpdateType::Renamed", (int64)EConcertPackageUpdateType::Renamed },
				{ "EConcertPackageUpdateType::Deleted", (int64)EConcertPackageUpdateType::Deleted },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Added.Comment", "/** This package has been added, but not yet saved */" },
				{ "Added.Name", "EConcertPackageUpdateType::Added" },
				{ "Added.ToolTip", "This package has been added, but not yet saved" },
				{ "Deleted.Comment", "/** This package has been deleted */" },
				{ "Deleted.Name", "EConcertPackageUpdateType::Deleted" },
				{ "Deleted.ToolTip", "This package has been deleted" },
				{ "Dummy.Comment", "/** A dummy update, typically used to fence some transactions as no longer relevant */" },
				{ "Dummy.Name", "EConcertPackageUpdateType::Dummy" },
				{ "Dummy.ToolTip", "A dummy update, typically used to fence some transactions as no longer relevant" },
				{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
				{ "Renamed.Comment", "/** This package has been renamed (leaving a redirector) */" },
				{ "Renamed.Name", "EConcertPackageUpdateType::Renamed" },
				{ "Renamed.ToolTip", "This package has been renamed (leaving a redirector)" },
				{ "Saved.Comment", "/** This package has been saved */" },
				{ "Saved.Name", "EConcertPackageUpdateType::Saved" },
				{ "Saved.ToolTip", "This package has been saved" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertPackageUpdateType",
				"EConcertPackageUpdateType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertPackage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertPackage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertPackage, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertPackage"), sizeof(FConcertPackage), Get_Z_Construct_UScriptStruct_FConcertPackage_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertPackage>()
{
	return FConcertPackage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertPackage(FConcertPackage::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertPackage"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackage
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackage()
	{
		UScriptStruct::DeferCppStructOps<FConcertPackage>(FName(TEXT("ConcertPackage")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackage;
	struct Z_Construct_UScriptStruct_FConcertPackage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Info_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Info;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PackageData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackage_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertPackage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertPackage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_Info_MetaData[] = {
		{ "Comment", "/** Contains information about the package event such as the package name, the event type, if this was triggered by an auto-save, etc. */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "Contains information about the package event such as the package name, the event type, if this was triggered by an auto-save, etc." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_Info = { "Info", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackage, Info), Z_Construct_UScriptStruct_FConcertPackageInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_Info_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_Info_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_PackageData_MetaData[] = {
		{ "Comment", "/** Contains the package data, unless the package size was too large (according to the hard limit or a configuration). */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "Contains the package data, unless the package size was too large (according to the hard limit or a configuration)." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_PackageData = { "PackageData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackage, PackageData), Z_Construct_UScriptStruct_FConcertByteArray, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_PackageData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_PackageData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_FileId_MetaData[] = {
		{ "Comment", "/** A link to a file containing the data if the package size was too large to be directly embedded. The package data needs to be retrieved using IConcertFileSharingService interface.*/" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "A link to a file containing the data if the package size was too large to be directly embedded. The package data needs to be retrieved using IConcertFileSharingService interface." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_FileId = { "FileId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackage, FileId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_FileId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_FileId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertPackage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_Info,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_PackageData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackage_Statics::NewProp_FileId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertPackage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertPackage",
		sizeof(FConcertPackage),
		alignof(FConcertPackage),
		Z_Construct_UScriptStruct_FConcertPackage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertPackage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertPackage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertPackage"), sizeof(FConcertPackage), Get_Z_Construct_UScriptStruct_FConcertPackage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertPackage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertPackage_Hash() { return 3212302790U; }
class UScriptStruct* FConcertPackageInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertPackageInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertPackageInfo, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertPackageInfo"), sizeof(FConcertPackageInfo), Get_Z_Construct_UScriptStruct_FConcertPackageInfo_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertPackageInfo>()
{
	return FConcertPackageInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertPackageInfo(FConcertPackageInfo::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertPackageInfo"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageInfo
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageInfo()
	{
		UScriptStruct::DeferCppStructOps<FConcertPackageInfo>(FName(TEXT("ConcertPackageInfo")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageInfo;
	struct Z_Construct_UScriptStruct_FConcertPackageInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewPackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageFileExtension_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PackageFileExtension;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PackageUpdateType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageUpdateType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PackageUpdateType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionEventIdAtSave_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_TransactionEventIdAtSave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreSave_MetaData[];
#endif
		static void NewProp_bPreSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreSave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoSave_MetaData[];
#endif
		static void NewProp_bAutoSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoSave;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertPackageInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageName_MetaData[] = {
		{ "Comment", "/** The name of the package */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "The name of the package" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageName = { "PackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageInfo, PackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_NewPackageName_MetaData[] = {
		{ "Comment", "/** The new name of the package (if PackageUpdateType == EConcertPackageUpdateType::Renamed) */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "The new name of the package (if PackageUpdateType == EConcertPackageUpdateType::Renamed)" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_NewPackageName = { "NewPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageInfo, NewPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_NewPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_NewPackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_AssetClass_MetaData[] = {
		{ "Comment", "/** The class of the asset contained in this package. */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "The class of the asset contained in this package." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_AssetClass = { "AssetClass", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageInfo, AssetClass), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_AssetClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_AssetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageFileExtension_MetaData[] = {
		{ "Comment", "/** The extension of the package file on disk (eg, .umap or .uasset) */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "The extension of the package file on disk (eg, .umap or .uasset)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageFileExtension = { "PackageFileExtension", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageInfo, PackageFileExtension), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageFileExtension_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageFileExtension_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageUpdateType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageUpdateType_MetaData[] = {
		{ "Comment", "/** What kind of package update is this? */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "What kind of package update is this?" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageUpdateType = { "PackageUpdateType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageInfo, PackageUpdateType), Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageUpdateType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageUpdateType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_TransactionEventIdAtSave_MetaData[] = {
		{ "Comment", "/** What was the max transaction event ID when this update was made? (to discard older transactions that applied to this package) */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "What was the max transaction event ID when this update was made? (to discard older transactions that applied to this package)" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_TransactionEventIdAtSave = { "TransactionEventIdAtSave", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageInfo, TransactionEventIdAtSave), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_TransactionEventIdAtSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_TransactionEventIdAtSave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bPreSave_MetaData[] = {
		{ "Comment", "/** Was this update caused by a pre-save? */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "Was this update caused by a pre-save?" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bPreSave_SetBit(void* Obj)
	{
		((FConcertPackageInfo*)Obj)->bPreSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bPreSave = { "bPreSave", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertPackageInfo), &Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bPreSave_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bPreSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bPreSave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bAutoSave_MetaData[] = {
		{ "Comment", "/** Was this update caused by an auto-save? */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceData.h" },
		{ "ToolTip", "Was this update caused by an auto-save?" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bAutoSave_SetBit(void* Obj)
	{
		((FConcertPackageInfo*)Obj)->bAutoSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bAutoSave = { "bAutoSave", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertPackageInfo), &Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bAutoSave_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bAutoSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bAutoSave_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_NewPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_AssetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageFileExtension,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageUpdateType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_PackageUpdateType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_TransactionEventIdAtSave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bPreSave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::NewProp_bAutoSave,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertPackageInfo",
		sizeof(FConcertPackageInfo),
		alignof(FConcertPackageInfo),
		Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertPackageInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertPackageInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertPackageInfo"), sizeof(FConcertPackageInfo), Get_Z_Construct_UScriptStruct_FConcertPackageInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertPackageInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertPackageInfo_Hash() { return 1820099195U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
