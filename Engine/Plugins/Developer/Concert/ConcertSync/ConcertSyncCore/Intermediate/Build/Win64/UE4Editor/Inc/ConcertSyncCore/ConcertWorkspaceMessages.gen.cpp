// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncCore/Public/ConcertWorkspaceMessages.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertWorkspaceMessages() {}
// Cross Module References
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertPlaySessionEventType();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncCore();
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPlaySessionEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertResourceLockResponse();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertResourceLockRequest();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertResourceLockEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPackageRejectedEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPackageUpdateEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPackage();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertServerLogging();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEventResponse();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSerializedPayload();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEventRequest();
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData();
// End Cross Module References
	static UEnum* EConcertPlaySessionEventType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertPlaySessionEventType, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertPlaySessionEventType"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertPlaySessionEventType>()
	{
		return EConcertPlaySessionEventType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertPlaySessionEventType(EConcertPlaySessionEventType_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertPlaySessionEventType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertPlaySessionEventType_Hash() { return 3998956006U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertPlaySessionEventType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertPlaySessionEventType"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertPlaySessionEventType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertPlaySessionEventType::None", (int64)EConcertPlaySessionEventType::None },
				{ "EConcertPlaySessionEventType::BeginPlay", (int64)EConcertPlaySessionEventType::BeginPlay },
				{ "EConcertPlaySessionEventType::SwitchPlay", (int64)EConcertPlaySessionEventType::SwitchPlay },
				{ "EConcertPlaySessionEventType::EndPlay", (int64)EConcertPlaySessionEventType::EndPlay },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BeginPlay.Name", "EConcertPlaySessionEventType::BeginPlay" },
				{ "EndPlay.Name", "EConcertPlaySessionEventType::EndPlay" },
				{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
				{ "None.Name", "EConcertPlaySessionEventType::None" },
				{ "SwitchPlay.Name", "EConcertPlaySessionEventType::SwitchPlay" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertPlaySessionEventType",
				"EConcertPlaySessionEventType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertResourceLockType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertResourceLockType"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertResourceLockType>()
	{
		return EConcertResourceLockType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertResourceLockType(EConcertResourceLockType_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertResourceLockType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType_Hash() { return 1205230753U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertResourceLockType"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertResourceLockType::None", (int64)EConcertResourceLockType::None },
				{ "EConcertResourceLockType::Lock", (int64)EConcertResourceLockType::Lock },
				{ "EConcertResourceLockType::Unlock", (int64)EConcertResourceLockType::Unlock },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Lock.Name", "EConcertResourceLockType::Lock" },
				{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
				{ "None.Name", "EConcertResourceLockType::None" },
				{ "Unlock.Name", "EConcertResourceLockType::Unlock" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertResourceLockType",
				"EConcertResourceLockType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertIgnoreActivityStateChangedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertIgnoreActivityStateChangedEvent"), sizeof(FConcertIgnoreActivityStateChangedEvent), Get_Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertIgnoreActivityStateChangedEvent>()
{
	return FConcertIgnoreActivityStateChangedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent(FConcertIgnoreActivityStateChangedEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertIgnoreActivityStateChangedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertIgnoreActivityStateChangedEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertIgnoreActivityStateChangedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertIgnoreActivityStateChangedEvent>(FName(TEXT("ConcertIgnoreActivityStateChangedEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertIgnoreActivityStateChangedEvent;
	struct Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnore_MetaData[];
#endif
		static void NewProp_bIgnore_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnore;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Sets the specified client 'ignore on restore' state for further activities. The 'ignored' flag can be raised to mark a series of activities as 'should not be restored'.\n * @note This can be used to record and monitor session activities for inspection purpose, for example allowing disaster recovery to record what\n *       happens in a multi-user session without restoring such activities in case of crash (because they occurred in a transient sandbox).\n */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "Sets the specified client 'ignore on restore' state for further activities. The 'ignored' flag can be raised to mark a series of activities as 'should not be restored'.\n@note This can be used to record and monitor session activities for inspection purpose, for example allowing disaster recovery to record what\n      happens in a multi-user session without restoring such activities in case of crash (because they occurred in a transient sandbox)." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertIgnoreActivityStateChangedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_EndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_EndpointId = { "EndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertIgnoreActivityStateChangedEvent, EndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_EndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_EndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_bIgnore_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_bIgnore_SetBit(void* Obj)
	{
		((FConcertIgnoreActivityStateChangedEvent*)Obj)->bIgnore = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_bIgnore = { "bIgnore", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertIgnoreActivityStateChangedEvent), &Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_bIgnore_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_bIgnore_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_bIgnore_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_EndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::NewProp_bIgnore,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertIgnoreActivityStateChangedEvent",
		sizeof(FConcertIgnoreActivityStateChangedEvent),
		alignof(FConcertIgnoreActivityStateChangedEvent),
		Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertIgnoreActivityStateChangedEvent"), sizeof(FConcertIgnoreActivityStateChangedEvent), Get_Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertIgnoreActivityStateChangedEvent_Hash() { return 736493631U; }
class UScriptStruct* FConcertPlaySessionEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertPlaySessionEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertPlaySessionEvent"), sizeof(FConcertPlaySessionEvent), Get_Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertPlaySessionEvent>()
{
	return FConcertPlaySessionEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertPlaySessionEvent(FConcertPlaySessionEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertPlaySessionEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPlaySessionEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPlaySessionEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertPlaySessionEvent>(FName(TEXT("ConcertPlaySessionEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPlaySessionEvent;
	struct Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PlayEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlayPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PlayPackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsSimulating_MetaData[];
#endif
		static void NewProp_bIsSimulating_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSimulating;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertPlaySessionEvent>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_EventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_EventType_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_EventType = { "EventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPlaySessionEvent, EventType), Z_Construct_UEnum_ConcertSyncCore_EConcertPlaySessionEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_EventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_EventType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayEndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayEndpointId = { "PlayEndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPlaySessionEvent, PlayEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayPackageName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayPackageName = { "PlayPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPlaySessionEvent, PlayPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayPackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_bIsSimulating_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_bIsSimulating_SetBit(void* Obj)
	{
		((FConcertPlaySessionEvent*)Obj)->bIsSimulating = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_bIsSimulating = { "bIsSimulating", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertPlaySessionEvent), &Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_bIsSimulating_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_bIsSimulating_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_bIsSimulating_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_EventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_EventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_PlayPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::NewProp_bIsSimulating,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertPlaySessionEvent",
		sizeof(FConcertPlaySessionEvent),
		alignof(FConcertPlaySessionEvent),
		Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertPlaySessionEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertPlaySessionEvent"), sizeof(FConcertPlaySessionEvent), Get_Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertPlaySessionEvent_Hash() { return 3112390971U; }
class UScriptStruct* FConcertResourceLockResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertResourceLockResponse, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertResourceLockResponse"), sizeof(FConcertResourceLockResponse), Get_Z_Construct_UScriptStruct_FConcertResourceLockResponse_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertResourceLockResponse>()
{
	return FConcertResourceLockResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertResourceLockResponse(FConcertResourceLockResponse::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertResourceLockResponse"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockResponse
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertResourceLockResponse>(FName(TEXT("ConcertResourceLockResponse")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockResponse;
	struct Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FailedResources_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FailedResources_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FailedResources_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FailedResources;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LockType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LockType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertResourceLockResponse>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources_ValueProp = { "FailedResources", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources_Key_KeyProp = { "FailedResources_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources = { "FailedResources", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockResponse, FailedResources), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_LockType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_LockType_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_LockType = { "LockType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockResponse, LockType), Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_LockType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_LockType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_FailedResources,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_LockType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::NewProp_LockType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertResourceLockResponse",
		sizeof(FConcertResourceLockResponse),
		alignof(FConcertResourceLockResponse),
		Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertResourceLockResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertResourceLockResponse"), sizeof(FConcertResourceLockResponse), Get_Z_Construct_UScriptStruct_FConcertResourceLockResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertResourceLockResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockResponse_Hash() { return 774002571U; }
class UScriptStruct* FConcertResourceLockRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertResourceLockRequest, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertResourceLockRequest"), sizeof(FConcertResourceLockRequest), Get_Z_Construct_UScriptStruct_FConcertResourceLockRequest_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertResourceLockRequest>()
{
	return FConcertResourceLockRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertResourceLockRequest(FConcertResourceLockRequest::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertResourceLockRequest"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockRequest
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertResourceLockRequest>(FName(TEXT("ConcertResourceLockRequest")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockRequest;
	struct Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientId;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ResourceNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResourceNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ResourceNames;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LockType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LockType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertResourceLockRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ClientId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ClientId = { "ClientId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockRequest, ClientId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ClientId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ClientId_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ResourceNames_Inner = { "ResourceNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ResourceNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ResourceNames = { "ResourceNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockRequest, ResourceNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ResourceNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ResourceNames_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_LockType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_LockType_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_LockType = { "LockType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockRequest, LockType), Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_LockType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_LockType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ClientId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ResourceNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_ResourceNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_LockType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::NewProp_LockType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertResourceLockRequest",
		sizeof(FConcertResourceLockRequest),
		alignof(FConcertResourceLockRequest),
		Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertResourceLockRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertResourceLockRequest"), sizeof(FConcertResourceLockRequest), Get_Z_Construct_UScriptStruct_FConcertResourceLockRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertResourceLockRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockRequest_Hash() { return 3293007815U; }
class UScriptStruct* FConcertResourceLockEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertResourceLockEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertResourceLockEvent"), sizeof(FConcertResourceLockEvent), Get_Z_Construct_UScriptStruct_FConcertResourceLockEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertResourceLockEvent>()
{
	return FConcertResourceLockEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertResourceLockEvent(FConcertResourceLockEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertResourceLockEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertResourceLockEvent>(FName(TEXT("ConcertResourceLockEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertResourceLockEvent;
	struct Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientId;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ResourceNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResourceNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ResourceNames;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LockType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LockType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertResourceLockEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ClientId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ClientId = { "ClientId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockEvent, ClientId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ClientId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ClientId_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ResourceNames_Inner = { "ResourceNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ResourceNames_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ResourceNames = { "ResourceNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockEvent, ResourceNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ResourceNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ResourceNames_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_LockType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_LockType_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_LockType = { "LockType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertResourceLockEvent, LockType), Z_Construct_UEnum_ConcertSyncCore_EConcertResourceLockType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_LockType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_LockType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ClientId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ResourceNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_ResourceNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_LockType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::NewProp_LockType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertResourceLockEvent",
		sizeof(FConcertResourceLockEvent),
		alignof(FConcertResourceLockEvent),
		Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertResourceLockEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertResourceLockEvent"), sizeof(FConcertResourceLockEvent), Get_Z_Construct_UScriptStruct_FConcertResourceLockEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertResourceLockEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertResourceLockEvent_Hash() { return 4234562251U; }
class UScriptStruct* FConcertPackageRejectedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertPackageRejectedEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertPackageRejectedEvent"), sizeof(FConcertPackageRejectedEvent), Get_Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertPackageRejectedEvent>()
{
	return FConcertPackageRejectedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertPackageRejectedEvent(FConcertPackageRejectedEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertPackageRejectedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageRejectedEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageRejectedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertPackageRejectedEvent>(FName(TEXT("ConcertPackageRejectedEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageRejectedEvent;
	struct Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PackageName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertPackageRejectedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::NewProp_PackageName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::NewProp_PackageName = { "PackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageRejectedEvent, PackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::NewProp_PackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::NewProp_PackageName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::NewProp_PackageName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertPackageRejectedEvent",
		sizeof(FConcertPackageRejectedEvent),
		alignof(FConcertPackageRejectedEvent),
		Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertPackageRejectedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertPackageRejectedEvent"), sizeof(FConcertPackageRejectedEvent), Get_Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertPackageRejectedEvent_Hash() { return 1466006321U; }
class UScriptStruct* FConcertPackageUpdateEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertPackageUpdateEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertPackageUpdateEvent"), sizeof(FConcertPackageUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertPackageUpdateEvent>()
{
	return FConcertPackageUpdateEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertPackageUpdateEvent(FConcertPackageUpdateEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertPackageUpdateEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageUpdateEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageUpdateEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertPackageUpdateEvent>(FName(TEXT("ConcertPackageUpdateEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertPackageUpdateEvent;
	struct Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Package_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Package;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertPackageUpdateEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::NewProp_Package_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::NewProp_Package = { "Package", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertPackageUpdateEvent, Package), Z_Construct_UScriptStruct_FConcertPackage, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::NewProp_Package_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::NewProp_Package_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::NewProp_Package,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertPackageUpdateEvent",
		sizeof(FConcertPackageUpdateEvent),
		alignof(FConcertPackageUpdateEvent),
		Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertPackageUpdateEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertPackageUpdateEvent"), sizeof(FConcertPackageUpdateEvent), Get_Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertPackageUpdateEvent_Hash() { return 3705049797U; }
class UScriptStruct* FConcertServerLogging::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertServerLogging_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertServerLogging, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertServerLogging"), sizeof(FConcertServerLogging), Get_Z_Construct_UScriptStruct_FConcertServerLogging_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertServerLogging>()
{
	return FConcertServerLogging::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertServerLogging(FConcertServerLogging::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertServerLogging"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertServerLogging
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertServerLogging()
	{
		UScriptStruct::DeferCppStructOps<FConcertServerLogging>(FName(TEXT("ConcertServerLogging")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertServerLogging;
	struct Z_Construct_UScriptStruct_FConcertServerLogging_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLoggingEnabled_MetaData[];
#endif
		static void NewProp_bLoggingEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLoggingEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerLogging_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertServerLogging>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewProp_bLoggingEnabled_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewProp_bLoggingEnabled_SetBit(void* Obj)
	{
		((FConcertServerLogging*)Obj)->bLoggingEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewProp_bLoggingEnabled = { "bLoggingEnabled", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertServerLogging), &Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewProp_bLoggingEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewProp_bLoggingEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewProp_bLoggingEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertServerLogging_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertServerLogging_Statics::NewProp_bLoggingEnabled,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertServerLogging_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertServerLogging",
		sizeof(FConcertServerLogging),
		alignof(FConcertServerLogging),
		Z_Construct_UScriptStruct_FConcertServerLogging_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerLogging_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertServerLogging_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertServerLogging_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertServerLogging()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertServerLogging_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertServerLogging"), sizeof(FConcertServerLogging), Get_Z_Construct_UScriptStruct_FConcertServerLogging_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertServerLogging_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertServerLogging_Hash() { return 3435676720U; }
class UScriptStruct* FConcertSyncEventResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEventResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncEventResponse, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncEventResponse"), sizeof(FConcertSyncEventResponse), Get_Z_Construct_UScriptStruct_FConcertSyncEventResponse_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncEventResponse>()
{
	return FConcertSyncEventResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncEventResponse(FConcertSyncEventResponse::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncEventResponse"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEventResponse
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEventResponse()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncEventResponse>(FName(TEXT("ConcertSyncEventResponse")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEventResponse;
	struct Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Event_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Event;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Response to a FConcertSyncEventRequest request. */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "Response to a FConcertSyncEventRequest request." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncEventResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::NewProp_Event_MetaData[] = {
		{ "Comment", "/** The payload contains the event corresponding to the requested event type like FConcertSyncTransactionEvent/FConcertSyncPackageEvent or an empty payload if the request failed. */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "The payload contains the event corresponding to the requested event type like FConcertSyncTransactionEvent/FConcertSyncPackageEvent or an empty payload if the request failed." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::NewProp_Event = { "Event", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncEventResponse, Event), Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::NewProp_Event_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::NewProp_Event_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::NewProp_Event,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncEventResponse",
		sizeof(FConcertSyncEventResponse),
		alignof(FConcertSyncEventResponse),
		Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEventResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEventResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncEventResponse"), sizeof(FConcertSyncEventResponse), Get_Z_Construct_UScriptStruct_FConcertSyncEventResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncEventResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEventResponse_Hash() { return 2411357678U; }
class UScriptStruct* FConcertSyncEventRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEventRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncEventRequest, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncEventRequest"), sizeof(FConcertSyncEventRequest), Get_Z_Construct_UScriptStruct_FConcertSyncEventRequest_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncEventRequest>()
{
	return FConcertSyncEventRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncEventRequest(FConcertSyncEventRequest::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncEventRequest"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEventRequest
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEventRequest()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncEventRequest>(FName(TEXT("ConcertSyncEventRequest")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEventRequest;
	struct Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventId_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_EventId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Request to sync an event that was partially synced on the client but for which the full data is required for inspection. FConcertSyncEventResponse is the corresponding response. */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "Request to sync an event that was partially synced on the client but for which the full data is required for inspection. FConcertSyncEventResponse is the corresponding response." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncEventRequest>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventType_MetaData[] = {
		{ "Comment", "/** The type of event to sync. Only Package and Transaction event types are supported. */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "The type of event to sync. Only Package and Transaction event types are supported." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventType = { "EventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncEventRequest, EventType), Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventId_MetaData[] = {
		{ "Comment", "/** The ID of the event to sync. */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "The ID of the event to sync." },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventId = { "EventId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncEventRequest, EventId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::NewProp_EventId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncEventRequest",
		sizeof(FConcertSyncEventRequest),
		alignof(FConcertSyncEventRequest),
		Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEventRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEventRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncEventRequest"), sizeof(FConcertSyncEventRequest), Get_Z_Construct_UScriptStruct_FConcertSyncEventRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncEventRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEventRequest_Hash() { return 2007167998U; }
class UScriptStruct* FConcertWorkspaceSyncCompletedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertWorkspaceSyncCompletedEvent"), sizeof(FConcertWorkspaceSyncCompletedEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertWorkspaceSyncCompletedEvent>()
{
	return FConcertWorkspaceSyncCompletedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent(FConcertWorkspaceSyncCompletedEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertWorkspaceSyncCompletedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncCompletedEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncCompletedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertWorkspaceSyncCompletedEvent>(FName(TEXT("ConcertWorkspaceSyncCompletedEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncCompletedEvent;
	struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertWorkspaceSyncCompletedEvent>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertWorkspaceSyncCompletedEvent",
		sizeof(FConcertWorkspaceSyncCompletedEvent),
		alignof(FConcertWorkspaceSyncCompletedEvent),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertWorkspaceSyncCompletedEvent"), sizeof(FConcertWorkspaceSyncCompletedEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncCompletedEvent_Hash() { return 2841759732U; }
class UScriptStruct* FConcertWorkspaceSyncRequestedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertWorkspaceSyncRequestedEvent"), sizeof(FConcertWorkspaceSyncRequestedEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertWorkspaceSyncRequestedEvent>()
{
	return FConcertWorkspaceSyncRequestedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent(FConcertWorkspaceSyncRequestedEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertWorkspaceSyncRequestedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncRequestedEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncRequestedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertWorkspaceSyncRequestedEvent>(FName(TEXT("ConcertWorkspaceSyncRequestedEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncRequestedEvent;
	struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FirstActivityIdToSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_FirstActivityIdToSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastActivityIdToSync_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_LastActivityIdToSync;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableLiveSync_MetaData[];
#endif
		static void NewProp_bEnableLiveSync_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableLiveSync;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertWorkspaceSyncRequestedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_FirstActivityIdToSync_MetaData[] = {
		{ "Comment", "/** The ID of the first activity to sync */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "The ID of the first activity to sync" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_FirstActivityIdToSync = { "FirstActivityIdToSync", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertWorkspaceSyncRequestedEvent, FirstActivityIdToSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_FirstActivityIdToSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_FirstActivityIdToSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_LastActivityIdToSync_MetaData[] = {
		{ "Comment", "/** The ID of the last activity to sync (ignored if bEnableLiveSync is true) */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "The ID of the last activity to sync (ignored if bEnableLiveSync is true)" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_LastActivityIdToSync = { "LastActivityIdToSync", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertWorkspaceSyncRequestedEvent, LastActivityIdToSync), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_LastActivityIdToSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_LastActivityIdToSync_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_bEnableLiveSync_MetaData[] = {
		{ "Comment", "/** True if the server workspace should be live-synced to this client as new activity is added, or false if syncing should only happen in response to these sync request events */" },
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
		{ "ToolTip", "True if the server workspace should be live-synced to this client as new activity is added, or false if syncing should only happen in response to these sync request events" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_bEnableLiveSync_SetBit(void* Obj)
	{
		((FConcertWorkspaceSyncRequestedEvent*)Obj)->bEnableLiveSync = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_bEnableLiveSync = { "bEnableLiveSync", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertWorkspaceSyncRequestedEvent), &Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_bEnableLiveSync_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_bEnableLiveSync_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_bEnableLiveSync_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_FirstActivityIdToSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_LastActivityIdToSync,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::NewProp_bEnableLiveSync,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertWorkspaceSyncRequestedEvent",
		sizeof(FConcertWorkspaceSyncRequestedEvent),
		alignof(FConcertWorkspaceSyncRequestedEvent),
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertWorkspaceSyncRequestedEvent"), sizeof(FConcertWorkspaceSyncRequestedEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncRequestedEvent_Hash() { return 1548506193U; }

static_assert(std::is_polymorphic<FConcertWorkspaceSyncLockEvent>() == std::is_polymorphic<FConcertWorkspaceSyncEventBase>(), "USTRUCT FConcertWorkspaceSyncLockEvent cannot be polymorphic unless super FConcertWorkspaceSyncEventBase is polymorphic");

class UScriptStruct* FConcertWorkspaceSyncLockEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertWorkspaceSyncLockEvent"), sizeof(FConcertWorkspaceSyncLockEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertWorkspaceSyncLockEvent>()
{
	return FConcertWorkspaceSyncLockEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertWorkspaceSyncLockEvent(FConcertWorkspaceSyncLockEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertWorkspaceSyncLockEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncLockEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncLockEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertWorkspaceSyncLockEvent>(FName(TEXT("ConcertWorkspaceSyncLockEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncLockEvent;
	struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LockedResources_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LockedResources_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockedResources_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LockedResources;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertWorkspaceSyncLockEvent>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources_ValueProp = { "LockedResources", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources_Key_KeyProp = { "LockedResources_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources = { "LockedResources", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertWorkspaceSyncLockEvent, LockedResources), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::NewProp_LockedResources,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase,
		&NewStructOps,
		"ConcertWorkspaceSyncLockEvent",
		sizeof(FConcertWorkspaceSyncLockEvent),
		alignof(FConcertWorkspaceSyncLockEvent),
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertWorkspaceSyncLockEvent"), sizeof(FConcertWorkspaceSyncLockEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncLockEvent_Hash() { return 415378728U; }

static_assert(std::is_polymorphic<FConcertWorkspaceSyncActivityEvent>() == std::is_polymorphic<FConcertWorkspaceSyncEventBase>(), "USTRUCT FConcertWorkspaceSyncActivityEvent cannot be polymorphic unless super FConcertWorkspaceSyncEventBase is polymorphic");

class UScriptStruct* FConcertWorkspaceSyncActivityEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertWorkspaceSyncActivityEvent"), sizeof(FConcertWorkspaceSyncActivityEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertWorkspaceSyncActivityEvent>()
{
	return FConcertWorkspaceSyncActivityEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertWorkspaceSyncActivityEvent(FConcertWorkspaceSyncActivityEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertWorkspaceSyncActivityEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncActivityEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncActivityEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertWorkspaceSyncActivityEvent>(FName(TEXT("ConcertWorkspaceSyncActivityEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncActivityEvent;
	struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Activity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Activity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertWorkspaceSyncActivityEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::NewProp_Activity_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::NewProp_Activity = { "Activity", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertWorkspaceSyncActivityEvent, Activity), Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::NewProp_Activity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::NewProp_Activity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::NewProp_Activity,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase,
		&NewStructOps,
		"ConcertWorkspaceSyncActivityEvent",
		sizeof(FConcertWorkspaceSyncActivityEvent),
		alignof(FConcertWorkspaceSyncActivityEvent),
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertWorkspaceSyncActivityEvent"), sizeof(FConcertWorkspaceSyncActivityEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncActivityEvent_Hash() { return 1882816230U; }

static_assert(std::is_polymorphic<FConcertWorkspaceSyncEndpointEvent>() == std::is_polymorphic<FConcertWorkspaceSyncEventBase>(), "USTRUCT FConcertWorkspaceSyncEndpointEvent cannot be polymorphic unless super FConcertWorkspaceSyncEventBase is polymorphic");

class UScriptStruct* FConcertWorkspaceSyncEndpointEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertWorkspaceSyncEndpointEvent"), sizeof(FConcertWorkspaceSyncEndpointEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertWorkspaceSyncEndpointEvent>()
{
	return FConcertWorkspaceSyncEndpointEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent(FConcertWorkspaceSyncEndpointEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertWorkspaceSyncEndpointEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncEndpointEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncEndpointEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertWorkspaceSyncEndpointEvent>(FName(TEXT("ConcertWorkspaceSyncEndpointEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncEndpointEvent;
	struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Endpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Endpoint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertWorkspaceSyncEndpointEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::NewProp_Endpoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::NewProp_Endpoint = { "Endpoint", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertWorkspaceSyncEndpointEvent, Endpoint), Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::NewProp_Endpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::NewProp_Endpoint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::NewProp_Endpoint,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase,
		&NewStructOps,
		"ConcertWorkspaceSyncEndpointEvent",
		sizeof(FConcertWorkspaceSyncEndpointEvent),
		alignof(FConcertWorkspaceSyncEndpointEvent),
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertWorkspaceSyncEndpointEvent"), sizeof(FConcertWorkspaceSyncEndpointEvent), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEndpointEvent_Hash() { return 2830951069U; }
class UScriptStruct* FConcertWorkspaceSyncEventBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertWorkspaceSyncEventBase"), sizeof(FConcertWorkspaceSyncEventBase), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertWorkspaceSyncEventBase>()
{
	return FConcertWorkspaceSyncEventBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertWorkspaceSyncEventBase(FConcertWorkspaceSyncEventBase::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertWorkspaceSyncEventBase"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncEventBase
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncEventBase()
	{
		UScriptStruct::DeferCppStructOps<FConcertWorkspaceSyncEventBase>(FName(TEXT("ConcertWorkspaceSyncEventBase")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertWorkspaceSyncEventBase;
	struct Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumRemainingSyncEvents_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumRemainingSyncEvents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertWorkspaceSyncEventBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::NewProp_NumRemainingSyncEvents_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertWorkspaceMessages.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::NewProp_NumRemainingSyncEvents = { "NumRemainingSyncEvents", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertWorkspaceSyncEventBase, NumRemainingSyncEvents), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::NewProp_NumRemainingSyncEvents_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::NewProp_NumRemainingSyncEvents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::NewProp_NumRemainingSyncEvents,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertWorkspaceSyncEventBase",
		sizeof(FConcertWorkspaceSyncEventBase),
		alignof(FConcertWorkspaceSyncEventBase),
		Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertWorkspaceSyncEventBase"), sizeof(FConcertWorkspaceSyncEventBase), Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertWorkspaceSyncEventBase_Hash() { return 373659378U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
