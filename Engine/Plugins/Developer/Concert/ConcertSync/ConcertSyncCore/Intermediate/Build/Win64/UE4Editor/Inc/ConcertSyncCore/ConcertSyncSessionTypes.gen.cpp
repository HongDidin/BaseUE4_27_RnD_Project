// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncCore/Public/ConcertSyncSessionTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertSyncSessionTypes() {}
// Cross Module References
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncTransactionActivitySummaryType();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncCore();
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType();
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncLockEventType();
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncConnectionEventType();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncActivitySummary();
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageActivity();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncActivity();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncTransactionActivity();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncTransactionEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncLockActivity();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncLockEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncConnectionActivity();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncConnectionEvent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSessionSerializedPayload();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPackageInfo();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertPackage();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEndpointData();
	CONCERT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientInfo();
// End Cross Module References
	static UEnum* EConcertSyncTransactionActivitySummaryType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertSyncTransactionActivitySummaryType, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertSyncTransactionActivitySummaryType"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncTransactionActivitySummaryType>()
	{
		return EConcertSyncTransactionActivitySummaryType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertSyncTransactionActivitySummaryType(EConcertSyncTransactionActivitySummaryType_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertSyncTransactionActivitySummaryType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncTransactionActivitySummaryType_Hash() { return 63343552U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncTransactionActivitySummaryType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertSyncTransactionActivitySummaryType"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncTransactionActivitySummaryType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertSyncTransactionActivitySummaryType::Added", (int64)EConcertSyncTransactionActivitySummaryType::Added },
				{ "EConcertSyncTransactionActivitySummaryType::Updated", (int64)EConcertSyncTransactionActivitySummaryType::Updated },
				{ "EConcertSyncTransactionActivitySummaryType::Renamed", (int64)EConcertSyncTransactionActivitySummaryType::Renamed },
				{ "EConcertSyncTransactionActivitySummaryType::Deleted", (int64)EConcertSyncTransactionActivitySummaryType::Deleted },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Added.Name", "EConcertSyncTransactionActivitySummaryType::Added" },
				{ "Comment", "/** Type of transaction summaries */" },
				{ "Deleted.Name", "EConcertSyncTransactionActivitySummaryType::Deleted" },
				{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
				{ "Renamed.Name", "EConcertSyncTransactionActivitySummaryType::Renamed" },
				{ "ToolTip", "Type of transaction summaries" },
				{ "Updated.Name", "EConcertSyncTransactionActivitySummaryType::Updated" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertSyncTransactionActivitySummaryType",
				"EConcertSyncTransactionActivitySummaryType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertSyncActivityEventType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertSyncActivityEventType"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncActivityEventType>()
	{
		return EConcertSyncActivityEventType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertSyncActivityEventType(EConcertSyncActivityEventType_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertSyncActivityEventType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType_Hash() { return 2990676827U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertSyncActivityEventType"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertSyncActivityEventType::None", (int64)EConcertSyncActivityEventType::None },
				{ "EConcertSyncActivityEventType::Connection", (int64)EConcertSyncActivityEventType::Connection },
				{ "EConcertSyncActivityEventType::Lock", (int64)EConcertSyncActivityEventType::Lock },
				{ "EConcertSyncActivityEventType::Transaction", (int64)EConcertSyncActivityEventType::Transaction },
				{ "EConcertSyncActivityEventType::Package", (int64)EConcertSyncActivityEventType::Package },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Types of activity events */" },
				{ "Connection.Name", "EConcertSyncActivityEventType::Connection" },
				{ "Lock.Name", "EConcertSyncActivityEventType::Lock" },
				{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
				{ "None.Name", "EConcertSyncActivityEventType::None" },
				{ "Package.Name", "EConcertSyncActivityEventType::Package" },
				{ "ToolTip", "Types of activity events" },
				{ "Transaction.Name", "EConcertSyncActivityEventType::Transaction" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertSyncActivityEventType",
				"EConcertSyncActivityEventType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertSyncLockEventType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertSyncLockEventType, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertSyncLockEventType"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncLockEventType>()
	{
		return EConcertSyncLockEventType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertSyncLockEventType(EConcertSyncLockEventType_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertSyncLockEventType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncLockEventType_Hash() { return 2280036213U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncLockEventType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertSyncLockEventType"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncLockEventType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertSyncLockEventType::Locked", (int64)EConcertSyncLockEventType::Locked },
				{ "EConcertSyncLockEventType::Unlocked", (int64)EConcertSyncLockEventType::Unlocked },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Types of lock events */" },
				{ "Locked.Name", "EConcertSyncLockEventType::Locked" },
				{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
				{ "ToolTip", "Types of lock events" },
				{ "Unlocked.Name", "EConcertSyncLockEventType::Unlocked" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertSyncLockEventType",
				"EConcertSyncLockEventType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EConcertSyncConnectionEventType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_EConcertSyncConnectionEventType, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("EConcertSyncConnectionEventType"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<EConcertSyncConnectionEventType>()
	{
		return EConcertSyncConnectionEventType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConcertSyncConnectionEventType(EConcertSyncConnectionEventType_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("EConcertSyncConnectionEventType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncConnectionEventType_Hash() { return 1321588947U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_EConcertSyncConnectionEventType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConcertSyncConnectionEventType"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_EConcertSyncConnectionEventType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConcertSyncConnectionEventType::Connected", (int64)EConcertSyncConnectionEventType::Connected },
				{ "EConcertSyncConnectionEventType::Disconnected", (int64)EConcertSyncConnectionEventType::Disconnected },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Types of connection events */" },
				{ "Connected.Name", "EConcertSyncConnectionEventType::Connected" },
				{ "Disconnected.Name", "EConcertSyncConnectionEventType::Disconnected" },
				{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
				{ "ToolTip", "Types of connection events" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"EConcertSyncConnectionEventType",
				"EConcertSyncConnectionEventType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FConcertSyncPackageActivitySummary>() == std::is_polymorphic<FConcertSyncActivitySummary>(), "USTRUCT FConcertSyncPackageActivitySummary cannot be polymorphic unless super FConcertSyncActivitySummary is polymorphic");

class UScriptStruct* FConcertSyncPackageActivitySummary::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncPackageActivitySummary"), sizeof(FConcertSyncPackageActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncPackageActivitySummary>()
{
	return FConcertSyncPackageActivitySummary::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncPackageActivitySummary(FConcertSyncPackageActivitySummary::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncPackageActivitySummary"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageActivitySummary
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageActivitySummary()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncPackageActivitySummary>(FName(TEXT("ConcertSyncPackageActivitySummary")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageActivitySummary;
	struct Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewPackageName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PackageUpdateType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageUpdateType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PackageUpdateType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoSave_MetaData[];
#endif
		static void NewProp_bAutoSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoSave;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreSave_MetaData[];
#endif
		static void NewProp_bPreSave_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreSave;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Summary for a package activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Summary for a package activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncPackageActivitySummary>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageName_MetaData[] = {
		{ "Comment", "/** The package affected by the package event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The package affected by the package event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageName = { "PackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageActivitySummary, PackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_NewPackageName_MetaData[] = {
		{ "Comment", "/** The new package name for the event we summarize (if PackageUpdateType == EConcertPackageUpdateType::Renamed) */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The new package name for the event we summarize (if PackageUpdateType == EConcertPackageUpdateType::Renamed)" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_NewPackageName = { "NewPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageActivitySummary, NewPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_NewPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_NewPackageName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageUpdateType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageUpdateType_MetaData[] = {
		{ "Comment", "/** The type of package update we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The type of package update we summarize" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageUpdateType = { "PackageUpdateType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageActivitySummary, PackageUpdateType), Z_Construct_UEnum_ConcertSyncCore_EConcertPackageUpdateType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageUpdateType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageUpdateType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bAutoSave_MetaData[] = {
		{ "Comment", "/** Are we summarizing an auto-save update? */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Are we summarizing an auto-save update?" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bAutoSave_SetBit(void* Obj)
	{
		((FConcertSyncPackageActivitySummary*)Obj)->bAutoSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bAutoSave = { "bAutoSave", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSyncPackageActivitySummary), &Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bAutoSave_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bAutoSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bAutoSave_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bPreSave_MetaData[] = {
		{ "Comment", "/** Are we summarizing a pre-save update? */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Are we summarizing a pre-save update?" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bPreSave_SetBit(void* Obj)
	{
		((FConcertSyncPackageActivitySummary*)Obj)->bPreSave = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bPreSave = { "bPreSave", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSyncPackageActivitySummary), &Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bPreSave_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bPreSave_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bPreSave_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_NewPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageUpdateType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_PackageUpdateType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bAutoSave,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::NewProp_bPreSave,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivitySummary,
		&NewStructOps,
		"ConcertSyncPackageActivitySummary",
		sizeof(FConcertSyncPackageActivitySummary),
		alignof(FConcertSyncPackageActivitySummary),
		Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncPackageActivitySummary"), sizeof(FConcertSyncPackageActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivitySummary_Hash() { return 125097346U; }

static_assert(std::is_polymorphic<FConcertSyncTransactionActivitySummary>() == std::is_polymorphic<FConcertSyncActivitySummary>(), "USTRUCT FConcertSyncTransactionActivitySummary cannot be polymorphic unless super FConcertSyncActivitySummary is polymorphic");

class UScriptStruct* FConcertSyncTransactionActivitySummary::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncTransactionActivitySummary"), sizeof(FConcertSyncTransactionActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncTransactionActivitySummary>()
{
	return FConcertSyncTransactionActivitySummary::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncTransactionActivitySummary(FConcertSyncTransactionActivitySummary::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncTransactionActivitySummary"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionActivitySummary
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionActivitySummary()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncTransactionActivitySummary>(FName(TEXT("ConcertSyncTransactionActivitySummary")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionActivitySummary;
	struct Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransactionSummaryType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionSummaryType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TransactionSummaryType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionTitle_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_TransactionTitle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryObjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PrimaryObjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PrimaryPackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewObjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewObjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumActions_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumActions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Summary for a transaction activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Summary for a transaction activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncTransactionActivitySummary>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionSummaryType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionSummaryType_MetaData[] = {
		{ "Comment", "/** The type of summary that the transaction event we summarize produced */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The type of summary that the transaction event we summarize produced" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionSummaryType = { "TransactionSummaryType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionActivitySummary, TransactionSummaryType), Z_Construct_UEnum_ConcertSyncCore_EConcertSyncTransactionActivitySummaryType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionSummaryType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionSummaryType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionTitle_MetaData[] = {
		{ "Comment", "/** The title of transaction in the transaction event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The title of transaction in the transaction event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionTitle = { "TransactionTitle", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionActivitySummary, TransactionTitle), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionTitle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionTitle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryObjectName_MetaData[] = {
		{ "Comment", "/** The primary object affected by the transaction event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The primary object affected by the transaction event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryObjectName = { "PrimaryObjectName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionActivitySummary, PrimaryObjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryObjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryObjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryPackageName_MetaData[] = {
		{ "Comment", "/** The primary package affected by the transaction event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The primary package affected by the transaction event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryPackageName = { "PrimaryPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionActivitySummary, PrimaryPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryPackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NewObjectName_MetaData[] = {
		{ "Comment", "/** The new object name for the event we summarize (if TransactionSummaryType == EConcertSyncTransactionActivitySummaryType::Renamed) */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The new object name for the event we summarize (if TransactionSummaryType == EConcertSyncTransactionActivitySummaryType::Renamed)" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NewObjectName = { "NewObjectName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionActivitySummary, NewObjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NewObjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NewObjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NumActions_MetaData[] = {
		{ "Comment", "/** The total number of actions created by the transaction event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The total number of actions created by the transaction event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NumActions = { "NumActions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionActivitySummary, NumActions), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NumActions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NumActions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionSummaryType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionSummaryType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_TransactionTitle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryObjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_PrimaryPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NewObjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::NewProp_NumActions,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivitySummary,
		&NewStructOps,
		"ConcertSyncTransactionActivitySummary",
		sizeof(FConcertSyncTransactionActivitySummary),
		alignof(FConcertSyncTransactionActivitySummary),
		Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncTransactionActivitySummary"), sizeof(FConcertSyncTransactionActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivitySummary_Hash() { return 119134876U; }

static_assert(std::is_polymorphic<FConcertSyncLockActivitySummary>() == std::is_polymorphic<FConcertSyncActivitySummary>(), "USTRUCT FConcertSyncLockActivitySummary cannot be polymorphic unless super FConcertSyncActivitySummary is polymorphic");

class UScriptStruct* FConcertSyncLockActivitySummary::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncLockActivitySummary"), sizeof(FConcertSyncLockActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncLockActivitySummary>()
{
	return FConcertSyncLockActivitySummary::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncLockActivitySummary(FConcertSyncLockActivitySummary::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncLockActivitySummary"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockActivitySummary
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockActivitySummary()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncLockActivitySummary>(FName(TEXT("ConcertSyncLockActivitySummary")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockActivitySummary;
	struct Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LockEventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockEventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LockEventType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryResourceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PrimaryResourceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PrimaryPackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumResources_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumResources;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Summary for a lock activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Summary for a lock activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncLockActivitySummary>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_LockEventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_LockEventType_MetaData[] = {
		{ "Comment", "/** The type of lock event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The type of lock event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_LockEventType = { "LockEventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncLockActivitySummary, LockEventType), Z_Construct_UEnum_ConcertSyncCore_EConcertSyncLockEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_LockEventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_LockEventType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryResourceName_MetaData[] = {
		{ "Comment", "/** The primary resource affected by the lock event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The primary resource affected by the lock event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryResourceName = { "PrimaryResourceName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncLockActivitySummary, PrimaryResourceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryResourceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryResourceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryPackageName_MetaData[] = {
		{ "Comment", "/** The primary package affected by the lock event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The primary package affected by the lock event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryPackageName = { "PrimaryPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncLockActivitySummary, PrimaryPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryPackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_NumResources_MetaData[] = {
		{ "Comment", "/** The total number of resources affected by the lock event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The total number of resources affected by the lock event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_NumResources = { "NumResources", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncLockActivitySummary, NumResources), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_NumResources_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_NumResources_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_LockEventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_LockEventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryResourceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_PrimaryPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::NewProp_NumResources,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivitySummary,
		&NewStructOps,
		"ConcertSyncLockActivitySummary",
		sizeof(FConcertSyncLockActivitySummary),
		alignof(FConcertSyncLockActivitySummary),
		Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncLockActivitySummary"), sizeof(FConcertSyncLockActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockActivitySummary_Hash() { return 3611593909U; }

static_assert(std::is_polymorphic<FConcertSyncConnectionActivitySummary>() == std::is_polymorphic<FConcertSyncActivitySummary>(), "USTRUCT FConcertSyncConnectionActivitySummary cannot be polymorphic unless super FConcertSyncActivitySummary is polymorphic");

class UScriptStruct* FConcertSyncConnectionActivitySummary::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncConnectionActivitySummary"), sizeof(FConcertSyncConnectionActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncConnectionActivitySummary>()
{
	return FConcertSyncConnectionActivitySummary::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncConnectionActivitySummary(FConcertSyncConnectionActivitySummary::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncConnectionActivitySummary"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionActivitySummary
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionActivitySummary()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncConnectionActivitySummary>(FName(TEXT("ConcertSyncConnectionActivitySummary")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionActivitySummary;
	struct Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ConnectionEventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionEventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConnectionEventType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Summary for a connection activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Summary for a connection activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncConnectionActivitySummary>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewProp_ConnectionEventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewProp_ConnectionEventType_MetaData[] = {
		{ "Comment", "/** The type of connection event we summarize */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The type of connection event we summarize" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewProp_ConnectionEventType = { "ConnectionEventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncConnectionActivitySummary, ConnectionEventType), Z_Construct_UEnum_ConcertSyncCore_EConcertSyncConnectionEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewProp_ConnectionEventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewProp_ConnectionEventType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewProp_ConnectionEventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::NewProp_ConnectionEventType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivitySummary,
		&NewStructOps,
		"ConcertSyncConnectionActivitySummary",
		sizeof(FConcertSyncConnectionActivitySummary),
		alignof(FConcertSyncConnectionActivitySummary),
		Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncConnectionActivitySummary"), sizeof(FConcertSyncConnectionActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivitySummary_Hash() { return 2623125111U; }
class UScriptStruct* FConcertSyncActivitySummary::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncActivitySummary, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncActivitySummary"), sizeof(FConcertSyncActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncActivitySummary>()
{
	return FConcertSyncActivitySummary::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncActivitySummary(FConcertSyncActivitySummary::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncActivitySummary"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncActivitySummary
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncActivitySummary()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncActivitySummary>(FName(TEXT("ConcertSyncActivitySummary")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncActivitySummary;
	struct Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Base summary for an activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Base summary for an activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncActivitySummary>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncActivitySummary",
		sizeof(FConcertSyncActivitySummary),
		alignof(FConcertSyncActivitySummary),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncActivitySummary()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncActivitySummary"), sizeof(FConcertSyncActivitySummary), Get_Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncActivitySummary_Hash() { return 1119756739U; }

static_assert(std::is_polymorphic<FConcertSyncPackageActivity>() == std::is_polymorphic<FConcertSyncActivity>(), "USTRUCT FConcertSyncPackageActivity cannot be polymorphic unless super FConcertSyncActivity is polymorphic");

class UScriptStruct* FConcertSyncPackageActivity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncPackageActivity, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncPackageActivity"), sizeof(FConcertSyncPackageActivity), Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncPackageActivity>()
{
	return FConcertSyncPackageActivity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncPackageActivity(FConcertSyncPackageActivity::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncPackageActivity"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageActivity
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageActivity()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncPackageActivity>(FName(TEXT("ConcertSyncPackageActivity")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageActivity;
	struct Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a package activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a package activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncPackageActivity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::NewProp_EventData_MetaData[] = {
		{ "Comment", "/** The package event data associated with this activity */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The package event data associated with this activity" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::NewProp_EventData = { "EventData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageActivity, EventData), Z_Construct_UScriptStruct_FConcertSyncPackageEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::NewProp_EventData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::NewProp_EventData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::NewProp_EventData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivity,
		&NewStructOps,
		"ConcertSyncPackageActivity",
		sizeof(FConcertSyncPackageActivity),
		alignof(FConcertSyncPackageActivity),
		Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageActivity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncPackageActivity"), sizeof(FConcertSyncPackageActivity), Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageActivity_Hash() { return 4260035283U; }

static_assert(std::is_polymorphic<FConcertSyncTransactionActivity>() == std::is_polymorphic<FConcertSyncActivity>(), "USTRUCT FConcertSyncTransactionActivity cannot be polymorphic unless super FConcertSyncActivity is polymorphic");

class UScriptStruct* FConcertSyncTransactionActivity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncTransactionActivity, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncTransactionActivity"), sizeof(FConcertSyncTransactionActivity), Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncTransactionActivity>()
{
	return FConcertSyncTransactionActivity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncTransactionActivity(FConcertSyncTransactionActivity::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncTransactionActivity"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionActivity
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionActivity()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncTransactionActivity>(FName(TEXT("ConcertSyncTransactionActivity")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionActivity;
	struct Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a transaction activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a transaction activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncTransactionActivity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::NewProp_EventData_MetaData[] = {
		{ "Comment", "/** The transaction event data associated with this activity */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The transaction event data associated with this activity" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::NewProp_EventData = { "EventData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionActivity, EventData), Z_Construct_UScriptStruct_FConcertSyncTransactionEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::NewProp_EventData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::NewProp_EventData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::NewProp_EventData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivity,
		&NewStructOps,
		"ConcertSyncTransactionActivity",
		sizeof(FConcertSyncTransactionActivity),
		alignof(FConcertSyncTransactionActivity),
		Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncTransactionActivity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncTransactionActivity"), sizeof(FConcertSyncTransactionActivity), Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionActivity_Hash() { return 1965346046U; }

static_assert(std::is_polymorphic<FConcertSyncLockActivity>() == std::is_polymorphic<FConcertSyncActivity>(), "USTRUCT FConcertSyncLockActivity cannot be polymorphic unless super FConcertSyncActivity is polymorphic");

class UScriptStruct* FConcertSyncLockActivity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockActivity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncLockActivity, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncLockActivity"), sizeof(FConcertSyncLockActivity), Get_Z_Construct_UScriptStruct_FConcertSyncLockActivity_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncLockActivity>()
{
	return FConcertSyncLockActivity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncLockActivity(FConcertSyncLockActivity::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncLockActivity"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockActivity
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockActivity()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncLockActivity>(FName(TEXT("ConcertSyncLockActivity")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockActivity;
	struct Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a lock activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a lock activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncLockActivity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::NewProp_EventData_MetaData[] = {
		{ "Comment", "/** The lock event data associated with this activity */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The lock event data associated with this activity" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::NewProp_EventData = { "EventData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncLockActivity, EventData), Z_Construct_UScriptStruct_FConcertSyncLockEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::NewProp_EventData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::NewProp_EventData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::NewProp_EventData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivity,
		&NewStructOps,
		"ConcertSyncLockActivity",
		sizeof(FConcertSyncLockActivity),
		alignof(FConcertSyncLockActivity),
		Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncLockActivity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockActivity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncLockActivity"), sizeof(FConcertSyncLockActivity), Get_Z_Construct_UScriptStruct_FConcertSyncLockActivity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncLockActivity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockActivity_Hash() { return 1751974765U; }

static_assert(std::is_polymorphic<FConcertSyncConnectionActivity>() == std::is_polymorphic<FConcertSyncActivity>(), "USTRUCT FConcertSyncConnectionActivity cannot be polymorphic unless super FConcertSyncActivity is polymorphic");

class UScriptStruct* FConcertSyncConnectionActivity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncConnectionActivity, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncConnectionActivity"), sizeof(FConcertSyncConnectionActivity), Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncConnectionActivity>()
{
	return FConcertSyncConnectionActivity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncConnectionActivity(FConcertSyncConnectionActivity::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncConnectionActivity"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionActivity
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionActivity()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncConnectionActivity>(FName(TEXT("ConcertSyncConnectionActivity")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionActivity;
	struct Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a connection activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a connection activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncConnectionActivity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::NewProp_EventData_MetaData[] = {
		{ "Comment", "/** The connection event data associated with this activity */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The connection event data associated with this activity" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::NewProp_EventData = { "EventData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncConnectionActivity, EventData), Z_Construct_UScriptStruct_FConcertSyncConnectionEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::NewProp_EventData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::NewProp_EventData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::NewProp_EventData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertSyncActivity,
		&NewStructOps,
		"ConcertSyncConnectionActivity",
		sizeof(FConcertSyncConnectionActivity),
		alignof(FConcertSyncConnectionActivity),
		Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncConnectionActivity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncConnectionActivity"), sizeof(FConcertSyncConnectionActivity), Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionActivity_Hash() { return 4122318778U; }
class UScriptStruct* FConcertSyncActivity::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncActivity_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncActivity, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncActivity"), sizeof(FConcertSyncActivity), Get_Z_Construct_UScriptStruct_FConcertSyncActivity_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncActivity>()
{
	return FConcertSyncActivity::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncActivity(FConcertSyncActivity::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncActivity"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncActivity
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncActivity()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncActivity>(FName(TEXT("ConcertSyncActivity")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncActivity;
	struct Z_Construct_UScriptStruct_FConcertSyncActivity_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActivityId_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_ActivityId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnored_MetaData[];
#endif
		static void NewProp_bIgnored_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnored;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventTime;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EventType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventId_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_EventId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventSummary_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EventSummary;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for an activity entry in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for an activity entry in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncActivity>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_ActivityId_MetaData[] = {
		{ "Comment", "/** The ID of the activity */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The ID of the activity" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_ActivityId = { "ActivityId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncActivity, ActivityId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_ActivityId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_ActivityId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_bIgnored_MetaData[] = {
		{ "Comment", "/** True if this activity is included for tracking purposes only, and can be ignored when migrating a database */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "True if this activity is included for tracking purposes only, and can be ignored when migrating a database" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_bIgnored_SetBit(void* Obj)
	{
		((FConcertSyncActivity*)Obj)->bIgnored = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_bIgnored = { "bIgnored", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSyncActivity), &Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_bIgnored_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_bIgnored_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_bIgnored_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EndpointId_MetaData[] = {
		{ "Comment", "/** The ID of the endpoint that produced the activity */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The ID of the endpoint that produced the activity" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EndpointId = { "EndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncActivity, EndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventTime_MetaData[] = {
		{ "Comment", "/** The time at which the activity was produced (UTC) */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The time at which the activity was produced (UTC)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventTime = { "EventTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncActivity, EventTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventTime_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventType_MetaData[] = {
		{ "Comment", "/** The type of this activity */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The type of this activity" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventType = { "EventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncActivity, EventType), Z_Construct_UEnum_ConcertSyncCore_EConcertSyncActivityEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventId_MetaData[] = {
		{ "Comment", "/** The ID of the event associated with this activity (@see EventType to work out how to resolve this) */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The ID of the event associated with this activity (@see EventType to work out how to resolve this)" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventId = { "EventId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncActivity, EventId), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventSummary_MetaData[] = {
		{ "Comment", "/** The minimal summary of the event associated with this activity (@see FConcertSyncActivitySummary) */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The minimal summary of the event associated with this activity (@see FConcertSyncActivitySummary)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventSummary = { "EventSummary", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncActivity, EventSummary), Z_Construct_UScriptStruct_FConcertSessionSerializedPayload, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventSummary_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventSummary_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_ActivityId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_bIgnored,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::NewProp_EventSummary,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncActivity",
		sizeof(FConcertSyncActivity),
		alignof(FConcertSyncActivity),
		Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncActivity()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncActivity_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncActivity"), sizeof(FConcertSyncActivity), Get_Z_Construct_UScriptStruct_FConcertSyncActivity_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncActivity_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncActivity_Hash() { return 939397940U; }
class UScriptStruct* FConcertSyncPackageEventMetaData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncPackageEventMetaData"), sizeof(FConcertSyncPackageEventMetaData), Get_Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncPackageEventMetaData>()
{
	return FConcertSyncPackageEventMetaData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncPackageEventMetaData(FConcertSyncPackageEventMetaData::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncPackageEventMetaData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageEventMetaData
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageEventMetaData()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncPackageEventMetaData>(FName(TEXT("ConcertSyncPackageEventMetaData")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageEventMetaData;
	struct Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageRevision_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_PackageRevision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PackageInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Meta data for a package event in a Concert Sync Session. */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Meta data for a package event in a Concert Sync Session." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncPackageEventMetaData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageRevision_MetaData[] = {
		{ "Comment", "/** The revision of this package within the session. */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The revision of this package within the session." },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageRevision = { "PackageRevision", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageEventMetaData, PackageRevision), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageRevision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageRevision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageInfo_MetaData[] = {
		{ "Comment", "/** Contains information about the package event such as the package name, the event type, if this was triggered by an auto-save, etc. */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Contains information about the package event such as the package name, the event type, if this was triggered by an auto-save, etc." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageInfo = { "PackageInfo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageEventMetaData, PackageInfo), Z_Construct_UScriptStruct_FConcertPackageInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageRevision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::NewProp_PackageInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncPackageEventMetaData",
		sizeof(FConcertSyncPackageEventMetaData),
		alignof(FConcertSyncPackageEventMetaData),
		Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncPackageEventMetaData"), sizeof(FConcertSyncPackageEventMetaData), Get_Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageEventMetaData_Hash() { return 459782200U; }
class UScriptStruct* FConcertSyncPackageEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncPackageEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncPackageEvent"), sizeof(FConcertSyncPackageEvent), Get_Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncPackageEvent>()
{
	return FConcertSyncPackageEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncPackageEvent(FConcertSyncPackageEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncPackageEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncPackageEvent>(FName(TEXT("ConcertSyncPackageEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncPackageEvent;
	struct Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackageRevision_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_PackageRevision;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Package_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Package;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a package event in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a package event in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncPackageEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_PackageRevision_MetaData[] = {
		{ "Comment", "/** The revision of this package within the session? */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The revision of this package within the session?" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_PackageRevision = { "PackageRevision", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageEvent, PackageRevision), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_PackageRevision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_PackageRevision_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_Package_MetaData[] = {
		{ "Comment", "/** The package data for this event */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The package data for this event" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_Package = { "Package", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncPackageEvent, Package), Z_Construct_UScriptStruct_FConcertPackage, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_Package_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_Package_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_PackageRevision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::NewProp_Package,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncPackageEvent",
		sizeof(FConcertSyncPackageEvent),
		alignof(FConcertSyncPackageEvent),
		Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncPackageEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncPackageEvent"), sizeof(FConcertSyncPackageEvent), Get_Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncPackageEvent_Hash() { return 1961775652U; }
class UScriptStruct* FConcertSyncTransactionEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncTransactionEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncTransactionEvent"), sizeof(FConcertSyncTransactionEvent), Get_Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncTransactionEvent>()
{
	return FConcertSyncTransactionEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncTransactionEvent(FConcertSyncTransactionEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncTransactionEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncTransactionEvent>(FName(TEXT("ConcertSyncTransactionEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncTransactionEvent;
	struct Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transaction_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Transaction;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a transaction event in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a transaction event in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncTransactionEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::NewProp_Transaction_MetaData[] = {
		{ "Comment", "/** The transaction data for this event */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The transaction data for this event" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::NewProp_Transaction = { "Transaction", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncTransactionEvent, Transaction), Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::NewProp_Transaction_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::NewProp_Transaction_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::NewProp_Transaction,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncTransactionEvent",
		sizeof(FConcertSyncTransactionEvent),
		alignof(FConcertSyncTransactionEvent),
		Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncTransactionEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncTransactionEvent"), sizeof(FConcertSyncTransactionEvent), Get_Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncTransactionEvent_Hash() { return 3949689494U; }
class UScriptStruct* FConcertSyncLockEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncLockEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncLockEvent"), sizeof(FConcertSyncLockEvent), Get_Z_Construct_UScriptStruct_FConcertSyncLockEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncLockEvent>()
{
	return FConcertSyncLockEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncLockEvent(FConcertSyncLockEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncLockEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncLockEvent>(FName(TEXT("ConcertSyncLockEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncLockEvent;
	struct Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_LockEventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LockEventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LockEventType;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ResourceNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ResourceNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ResourceNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a lock event in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a lock event in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncLockEvent>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_LockEventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_LockEventType_MetaData[] = {
		{ "Comment", "/** The type of this lock event */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The type of this lock event" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_LockEventType = { "LockEventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncLockEvent, LockEventType), Z_Construct_UEnum_ConcertSyncCore_EConcertSyncLockEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_LockEventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_LockEventType_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_ResourceNames_Inner = { "ResourceNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_ResourceNames_MetaData[] = {
		{ "Comment", "/** The resources affected by this lock event */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The resources affected by this lock event" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_ResourceNames = { "ResourceNames", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncLockEvent, ResourceNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_ResourceNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_ResourceNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_LockEventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_LockEventType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_ResourceNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::NewProp_ResourceNames,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncLockEvent",
		sizeof(FConcertSyncLockEvent),
		alignof(FConcertSyncLockEvent),
		Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncLockEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncLockEvent"), sizeof(FConcertSyncLockEvent), Get_Z_Construct_UScriptStruct_FConcertSyncLockEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncLockEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncLockEvent_Hash() { return 956295604U; }
class UScriptStruct* FConcertSyncConnectionEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncConnectionEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncConnectionEvent"), sizeof(FConcertSyncConnectionEvent), Get_Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncConnectionEvent>()
{
	return FConcertSyncConnectionEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncConnectionEvent(FConcertSyncConnectionEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncConnectionEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncConnectionEvent>(FName(TEXT("ConcertSyncConnectionEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncConnectionEvent;
	struct Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ConnectionEventType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConnectionEventType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ConnectionEventType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for a connection event in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for a connection event in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncConnectionEvent>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewProp_ConnectionEventType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewProp_ConnectionEventType_MetaData[] = {
		{ "Comment", "/** The type of this connection event */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The type of this connection event" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewProp_ConnectionEventType = { "ConnectionEventType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncConnectionEvent, ConnectionEventType), Z_Construct_UEnum_ConcertSyncCore_EConcertSyncConnectionEventType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewProp_ConnectionEventType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewProp_ConnectionEventType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewProp_ConnectionEventType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::NewProp_ConnectionEventType,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncConnectionEvent",
		sizeof(FConcertSyncConnectionEvent),
		alignof(FConcertSyncConnectionEvent),
		Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncConnectionEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncConnectionEvent"), sizeof(FConcertSyncConnectionEvent), Get_Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncConnectionEvent_Hash() { return 3145638541U; }
class UScriptStruct* FConcertSyncEndpointIdAndData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncEndpointIdAndData"), sizeof(FConcertSyncEndpointIdAndData), Get_Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncEndpointIdAndData>()
{
	return FConcertSyncEndpointIdAndData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncEndpointIdAndData(FConcertSyncEndpointIdAndData::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncEndpointIdAndData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEndpointIdAndData
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEndpointIdAndData()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncEndpointIdAndData>(FName(TEXT("ConcertSyncEndpointIdAndData")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEndpointIdAndData;
	struct Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndpointData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EndpointData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** ID and data pair for an endpoint in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "ID and data pair for an endpoint in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncEndpointIdAndData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointId_MetaData[] = {
		{ "Comment", "/** The ID of the endpoint */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The ID of the endpoint" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointId = { "EndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncEndpointIdAndData, EndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointData_MetaData[] = {
		{ "Comment", "/** The data for the endpoint */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The data for the endpoint" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointData = { "EndpointData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncEndpointIdAndData, EndpointData), Z_Construct_UScriptStruct_FConcertSyncEndpointData, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::NewProp_EndpointData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncEndpointIdAndData",
		sizeof(FConcertSyncEndpointIdAndData),
		alignof(FConcertSyncEndpointIdAndData),
		Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncEndpointIdAndData"), sizeof(FConcertSyncEndpointIdAndData), Get_Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEndpointIdAndData_Hash() { return 1029532346U; }
class UScriptStruct* FConcertSyncEndpointData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEndpointData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSyncEndpointData, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSyncEndpointData"), sizeof(FConcertSyncEndpointData), Get_Z_Construct_UScriptStruct_FConcertSyncEndpointData_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSyncEndpointData>()
{
	return FConcertSyncEndpointData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSyncEndpointData(FConcertSyncEndpointData::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSyncEndpointData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEndpointData
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEndpointData()
	{
		UScriptStruct::DeferCppStructOps<FConcertSyncEndpointData>(FName(TEXT("ConcertSyncEndpointData")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSyncEndpointData;
	struct Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientInfo;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Data for an endpoint in a Concert Sync Session */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "Data for an endpoint in a Concert Sync Session" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSyncEndpointData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::NewProp_ClientInfo_MetaData[] = {
		{ "Comment", "/** The information about the Concert client connected through this endpoint */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSessionTypes.h" },
		{ "ToolTip", "The information about the Concert client connected through this endpoint" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::NewProp_ClientInfo = { "ClientInfo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSyncEndpointData, ClientInfo), Z_Construct_UScriptStruct_FConcertClientInfo, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::NewProp_ClientInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::NewProp_ClientInfo_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::NewProp_ClientInfo,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSyncEndpointData",
		sizeof(FConcertSyncEndpointData),
		alignof(FConcertSyncEndpointData),
		Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSyncEndpointData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEndpointData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSyncEndpointData"), sizeof(FConcertSyncEndpointData), Get_Z_Construct_UScriptStruct_FConcertSyncEndpointData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSyncEndpointData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSyncEndpointData_Hash() { return 3468920740U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
