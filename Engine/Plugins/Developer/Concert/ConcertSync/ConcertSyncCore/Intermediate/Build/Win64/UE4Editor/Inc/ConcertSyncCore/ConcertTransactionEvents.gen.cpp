// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncCore/Public/ConcertTransactionEvents.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertTransactionEvents() {}
// Cross Module References
	CONCERTSYNCCORE_API UEnum* Z_Construct_UEnum_ConcertSyncCore_ETransactionFilterResult();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncCore();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionEventBase();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent();
	CONCERTTRANSPORT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertLocalIdentifierState();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertObjectId();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertExportedObject();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSerializedObjectData();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FConcertSerializedPropertyData();
// End Cross Module References
	static UEnum* ETransactionFilterResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ConcertSyncCore_ETransactionFilterResult, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ETransactionFilterResult"));
		}
		return Singleton;
	}
	template<> CONCERTSYNCCORE_API UEnum* StaticEnum<ETransactionFilterResult>()
	{
		return ETransactionFilterResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETransactionFilterResult(ETransactionFilterResult_StaticEnum, TEXT("/Script/ConcertSyncCore"), TEXT("ETransactionFilterResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ConcertSyncCore_ETransactionFilterResult_Hash() { return 2764987583U; }
	UEnum* Z_Construct_UEnum_ConcertSyncCore_ETransactionFilterResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETransactionFilterResult"), 0, Get_Z_Construct_UEnum_ConcertSyncCore_ETransactionFilterResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETransactionFilterResult::IncludeObject", (int64)ETransactionFilterResult::IncludeObject },
				{ "ETransactionFilterResult::ExcludeObject", (int64)ETransactionFilterResult::ExcludeObject },
				{ "ETransactionFilterResult::ExcludeTransaction", (int64)ETransactionFilterResult::ExcludeTransaction },
				{ "ETransactionFilterResult::UseDefault", (int64)ETransactionFilterResult::UseDefault },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ExcludeObject.Comment", "/** Filter the object from the Concert Transaction */" },
				{ "ExcludeObject.Name", "ETransactionFilterResult::ExcludeObject" },
				{ "ExcludeObject.ToolTip", "Filter the object from the Concert Transaction" },
				{ "ExcludeTransaction.Comment", "/** Filter the entire transaction and prevent propagation */" },
				{ "ExcludeTransaction.Name", "ETransactionFilterResult::ExcludeTransaction" },
				{ "ExcludeTransaction.ToolTip", "Filter the entire transaction and prevent propagation" },
				{ "IncludeObject.Comment", "/** Include the object in the Concert Transaction */" },
				{ "IncludeObject.Name", "ETransactionFilterResult::IncludeObject" },
				{ "IncludeObject.ToolTip", "Include the object in the Concert Transaction" },
				{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
				{ "UseDefault.Comment", "/** Delegate the filtering decision to the default handlers. */" },
				{ "UseDefault.Name", "ETransactionFilterResult::UseDefault" },
				{ "UseDefault.ToolTip", "Delegate the filtering decision to the default handlers." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ConcertSyncCore,
				nullptr,
				"ETransactionFilterResult",
				"ETransactionFilterResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConcertTransactionRejectedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertTransactionRejectedEvent"), sizeof(FConcertTransactionRejectedEvent), Get_Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertTransactionRejectedEvent>()
{
	return FConcertTransactionRejectedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertTransactionRejectedEvent(FConcertTransactionRejectedEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertTransactionRejectedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionRejectedEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionRejectedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertTransactionRejectedEvent>(FName(TEXT("ConcertTransactionRejectedEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionRejectedEvent;
	struct Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TransactionId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertTransactionRejectedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::NewProp_TransactionId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::NewProp_TransactionId = { "TransactionId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionRejectedEvent, TransactionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::NewProp_TransactionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::NewProp_TransactionId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::NewProp_TransactionId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertTransactionRejectedEvent",
		sizeof(FConcertTransactionRejectedEvent),
		alignof(FConcertTransactionRejectedEvent),
		Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertTransactionRejectedEvent"), sizeof(FConcertTransactionRejectedEvent), Get_Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionRejectedEvent_Hash() { return 2194443062U; }

static_assert(std::is_polymorphic<FConcertTransactionSnapshotEvent>() == std::is_polymorphic<FConcertTransactionEventBase>(), "USTRUCT FConcertTransactionSnapshotEvent cannot be polymorphic unless super FConcertTransactionEventBase is polymorphic");

class UScriptStruct* FConcertTransactionSnapshotEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertTransactionSnapshotEvent"), sizeof(FConcertTransactionSnapshotEvent), Get_Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertTransactionSnapshotEvent>()
{
	return FConcertTransactionSnapshotEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertTransactionSnapshotEvent(FConcertTransactionSnapshotEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertTransactionSnapshotEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionSnapshotEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionSnapshotEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertTransactionSnapshotEvent>(FName(TEXT("ConcertTransactionSnapshotEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionSnapshotEvent;
	struct Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertTransactionSnapshotEvent>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertTransactionEventBase,
		&NewStructOps,
		"ConcertTransactionSnapshotEvent",
		sizeof(FConcertTransactionSnapshotEvent),
		alignof(FConcertTransactionSnapshotEvent),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertTransactionSnapshotEvent"), sizeof(FConcertTransactionSnapshotEvent), Get_Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionSnapshotEvent_Hash() { return 1750882603U; }

static_assert(std::is_polymorphic<FConcertTransactionFinalizedEvent>() == std::is_polymorphic<FConcertTransactionEventBase>(), "USTRUCT FConcertTransactionFinalizedEvent cannot be polymorphic unless super FConcertTransactionEventBase is polymorphic");

class UScriptStruct* FConcertTransactionFinalizedEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertTransactionFinalizedEvent"), sizeof(FConcertTransactionFinalizedEvent), Get_Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertTransactionFinalizedEvent>()
{
	return FConcertTransactionFinalizedEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertTransactionFinalizedEvent(FConcertTransactionFinalizedEvent::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertTransactionFinalizedEvent"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionFinalizedEvent
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionFinalizedEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertTransactionFinalizedEvent>(FName(TEXT("ConcertTransactionFinalizedEvent")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionFinalizedEvent;
	struct Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalIdentifierState_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalIdentifierState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Title_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Title;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertTransactionFinalizedEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_LocalIdentifierState_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_LocalIdentifierState = { "LocalIdentifierState", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionFinalizedEvent, LocalIdentifierState), Z_Construct_UScriptStruct_FConcertLocalIdentifierState, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_LocalIdentifierState_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_LocalIdentifierState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_Title_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_Title = { "Title", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionFinalizedEvent, Title), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_Title_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_Title_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_LocalIdentifierState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::NewProp_Title,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		Z_Construct_UScriptStruct_FConcertTransactionEventBase,
		&NewStructOps,
		"ConcertTransactionFinalizedEvent",
		sizeof(FConcertTransactionFinalizedEvent),
		alignof(FConcertTransactionFinalizedEvent),
		Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertTransactionFinalizedEvent"), sizeof(FConcertTransactionFinalizedEvent), Get_Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionFinalizedEvent_Hash() { return 2977325462U; }
class UScriptStruct* FConcertTransactionEventBase::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionEventBase_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertTransactionEventBase, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertTransactionEventBase"), sizeof(FConcertTransactionEventBase), Get_Z_Construct_UScriptStruct_FConcertTransactionEventBase_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertTransactionEventBase>()
{
	return FConcertTransactionEventBase::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertTransactionEventBase(FConcertTransactionEventBase::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertTransactionEventBase"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionEventBase
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionEventBase()
	{
		UScriptStruct::DeferCppStructOps<FConcertTransactionEventBase>(FName(TEXT("ConcertTransactionEventBase")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertTransactionEventBase;
	struct Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TransactionId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OperationId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OperationId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionEndpointId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TransactionEndpointId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TransactionUpdateIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TransactionUpdateIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VersionIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_VersionIndex;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ModifiedPackages_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifiedPackages_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ModifiedPackages;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimaryObjectId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PrimaryObjectId;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExportedObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExportedObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExportedObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertTransactionEventBase>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionId = { "TransactionId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, TransactionId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_OperationId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_OperationId = { "OperationId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, OperationId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_OperationId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_OperationId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionEndpointId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionEndpointId = { "TransactionEndpointId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, TransactionEndpointId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionEndpointId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionEndpointId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionUpdateIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionUpdateIndex = { "TransactionUpdateIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, TransactionUpdateIndex), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionUpdateIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionUpdateIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_VersionIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_VersionIndex = { "VersionIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, VersionIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_VersionIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_VersionIndex_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ModifiedPackages_Inner = { "ModifiedPackages", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ModifiedPackages_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ModifiedPackages = { "ModifiedPackages", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, ModifiedPackages), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ModifiedPackages_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ModifiedPackages_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_PrimaryObjectId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_PrimaryObjectId = { "PrimaryObjectId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, PrimaryObjectId), Z_Construct_UScriptStruct_FConcertObjectId, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_PrimaryObjectId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_PrimaryObjectId_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ExportedObjects_Inner = { "ExportedObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertExportedObject, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ExportedObjects_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ExportedObjects = { "ExportedObjects", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertTransactionEventBase, ExportedObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ExportedObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ExportedObjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_OperationId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionEndpointId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_TransactionUpdateIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_VersionIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ModifiedPackages_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ModifiedPackages,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_PrimaryObjectId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ExportedObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::NewProp_ExportedObjects,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertTransactionEventBase",
		sizeof(FConcertTransactionEventBase),
		alignof(FConcertTransactionEventBase),
		Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertTransactionEventBase()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionEventBase_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertTransactionEventBase"), sizeof(FConcertTransactionEventBase), Get_Z_Construct_UScriptStruct_FConcertTransactionEventBase_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertTransactionEventBase_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertTransactionEventBase_Hash() { return 404819516U; }
class UScriptStruct* FConcertExportedObject::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertExportedObject_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertExportedObject, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertExportedObject"), sizeof(FConcertExportedObject), Get_Z_Construct_UScriptStruct_FConcertExportedObject_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertExportedObject>()
{
	return FConcertExportedObject::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertExportedObject(FConcertExportedObject::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertExportedObject"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertExportedObject
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertExportedObject()
	{
		UScriptStruct::DeferCppStructOps<FConcertExportedObject>(FName(TEXT("ConcertExportedObject")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertExportedObject;
	struct Z_Construct_UScriptStruct_FConcertExportedObject_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPathDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ObjectPathDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectData;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PropertyDatas_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyDatas_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PropertyDatas;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SerializedAnnotationData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedAnnotationData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SerializedAnnotationData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertExportedObject_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertExportedObject>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectId_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectId = { "ObjectId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertExportedObject, ObjectId), Z_Construct_UScriptStruct_FConcertObjectId, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectPathDepth_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectPathDepth = { "ObjectPathDepth", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertExportedObject, ObjectPathDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectPathDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectPathDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectData_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectData = { "ObjectData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertExportedObject, ObjectData), Z_Construct_UScriptStruct_FConcertSerializedObjectData, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectData_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_PropertyDatas_Inner = { "PropertyDatas", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConcertSerializedPropertyData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_PropertyDatas_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_PropertyDatas = { "PropertyDatas", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertExportedObject, PropertyDatas), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_PropertyDatas_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_PropertyDatas_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_SerializedAnnotationData_Inner = { "SerializedAnnotationData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_SerializedAnnotationData_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_SerializedAnnotationData = { "SerializedAnnotationData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertExportedObject, SerializedAnnotationData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_SerializedAnnotationData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_SerializedAnnotationData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertExportedObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectPathDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_ObjectData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_PropertyDatas_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_PropertyDatas,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_SerializedAnnotationData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertExportedObject_Statics::NewProp_SerializedAnnotationData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertExportedObject_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertExportedObject",
		sizeof(FConcertExportedObject),
		alignof(FConcertExportedObject),
		Z_Construct_UScriptStruct_FConcertExportedObject_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertExportedObject_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertExportedObject()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertExportedObject_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertExportedObject"), sizeof(FConcertExportedObject), Get_Z_Construct_UScriptStruct_FConcertExportedObject_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertExportedObject_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertExportedObject_Hash() { return 3929564121U; }
class UScriptStruct* FConcertSerializedPropertyData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSerializedPropertyData, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSerializedPropertyData"), sizeof(FConcertSerializedPropertyData), Get_Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSerializedPropertyData>()
{
	return FConcertSerializedPropertyData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSerializedPropertyData(FConcertSerializedPropertyData::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSerializedPropertyData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSerializedPropertyData
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSerializedPropertyData()
	{
		UScriptStruct::DeferCppStructOps<FConcertSerializedPropertyData>(FName(TEXT("ConcertSerializedPropertyData")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSerializedPropertyData;
	struct Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PropertyName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SerializedData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SerializedData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSerializedPropertyData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_PropertyName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_PropertyName = { "PropertyName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSerializedPropertyData, PropertyName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_PropertyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_PropertyName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_SerializedData_Inner = { "SerializedData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_SerializedData_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_SerializedData = { "SerializedData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSerializedPropertyData, SerializedData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_SerializedData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_SerializedData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_PropertyName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_SerializedData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::NewProp_SerializedData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSerializedPropertyData",
		sizeof(FConcertSerializedPropertyData),
		alignof(FConcertSerializedPropertyData),
		Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSerializedPropertyData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSerializedPropertyData"), sizeof(FConcertSerializedPropertyData), Get_Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSerializedPropertyData_Hash() { return 2623622652U; }
class UScriptStruct* FConcertSerializedObjectData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertSerializedObjectData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertSerializedObjectData, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertSerializedObjectData"), sizeof(FConcertSerializedObjectData), Get_Z_Construct_UScriptStruct_FConcertSerializedObjectData_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertSerializedObjectData>()
{
	return FConcertSerializedObjectData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertSerializedObjectData(FConcertSerializedObjectData::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertSerializedObjectData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSerializedObjectData
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSerializedObjectData()
	{
		UScriptStruct::DeferCppStructOps<FConcertSerializedObjectData>(FName(TEXT("ConcertSerializedObjectData")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertSerializedObjectData;
	struct Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowCreate_MetaData[];
#endif
		static void NewProp_bAllowCreate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowCreate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPendingKill_MetaData[];
#endif
		static void NewProp_bIsPendingKill_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPendingKill;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewPackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewOuterPathName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewOuterPathName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewExternalPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewExternalPackageName;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SerializedData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SerializedData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SerializedData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertSerializedObjectData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bAllowCreate_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bAllowCreate_SetBit(void* Obj)
	{
		((FConcertSerializedObjectData*)Obj)->bAllowCreate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bAllowCreate = { "bAllowCreate", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSerializedObjectData), &Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bAllowCreate_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bAllowCreate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bAllowCreate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bIsPendingKill_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bIsPendingKill_SetBit(void* Obj)
	{
		((FConcertSerializedObjectData*)Obj)->bIsPendingKill = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bIsPendingKill = { "bIsPendingKill", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConcertSerializedObjectData), &Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bIsPendingKill_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bIsPendingKill_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bIsPendingKill_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewPackageName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewPackageName = { "NewPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSerializedObjectData, NewPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewPackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewName = { "NewName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSerializedObjectData, NewName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewOuterPathName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewOuterPathName = { "NewOuterPathName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSerializedObjectData, NewOuterPathName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewOuterPathName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewOuterPathName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewExternalPackageName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewExternalPackageName = { "NewExternalPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSerializedObjectData, NewExternalPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewExternalPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewExternalPackageName_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_SerializedData_Inner = { "SerializedData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_SerializedData_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_SerializedData = { "SerializedData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertSerializedObjectData, SerializedData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_SerializedData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_SerializedData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bAllowCreate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_bIsPendingKill,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewOuterPathName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_NewExternalPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_SerializedData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::NewProp_SerializedData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertSerializedObjectData",
		sizeof(FConcertSerializedObjectData),
		alignof(FConcertSerializedObjectData),
		Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertSerializedObjectData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertSerializedObjectData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertSerializedObjectData"), sizeof(FConcertSerializedObjectData), Get_Z_Construct_UScriptStruct_FConcertSerializedObjectData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertSerializedObjectData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertSerializedObjectData_Hash() { return 1805187040U; }
class UScriptStruct* FConcertObjectId::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FConcertObjectId_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertObjectId, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("ConcertObjectId"), sizeof(FConcertObjectId), Get_Z_Construct_UScriptStruct_FConcertObjectId_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FConcertObjectId>()
{
	return FConcertObjectId::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertObjectId(FConcertObjectId::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("ConcertObjectId"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertObjectId
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertObjectId()
	{
		UScriptStruct::DeferCppStructOps<FConcertObjectId>(FName(TEXT("ConcertObjectId")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFConcertObjectId;
	struct Z_Construct_UScriptStruct_FConcertObjectId_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectClassPathName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ObjectClassPathName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ObjectPackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ObjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectOuterPathName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ObjectOuterPathName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectExternalPackageName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ObjectExternalPackageName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectPersistentFlags_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ObjectPersistentFlags;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertObjectId_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertObjectId>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectClassPathName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectClassPathName = { "ObjectClassPathName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertObjectId, ObjectClassPathName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectClassPathName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectClassPathName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPackageName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPackageName = { "ObjectPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertObjectId, ObjectPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectName = { "ObjectName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertObjectId, ObjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectOuterPathName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectOuterPathName = { "ObjectOuterPathName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertObjectId, ObjectOuterPathName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectOuterPathName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectOuterPathName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectExternalPackageName_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectExternalPackageName = { "ObjectExternalPackageName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertObjectId, ObjectExternalPackageName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectExternalPackageName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectExternalPackageName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPersistentFlags_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertTransactionEvents.h" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPersistentFlags = { "ObjectPersistentFlags", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertObjectId, ObjectPersistentFlags), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPersistentFlags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPersistentFlags_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertObjectId_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectClassPathName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectOuterPathName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectExternalPackageName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertObjectId_Statics::NewProp_ObjectPersistentFlags,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertObjectId_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"ConcertObjectId",
		sizeof(FConcertObjectId),
		alignof(FConcertObjectId),
		Z_Construct_UScriptStruct_FConcertObjectId_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertObjectId_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertObjectId_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertObjectId()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertObjectId_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertObjectId"), sizeof(FConcertObjectId), Get_Z_Construct_UScriptStruct_FConcertObjectId_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertObjectId_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertObjectId_Hash() { return 1583033419U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
