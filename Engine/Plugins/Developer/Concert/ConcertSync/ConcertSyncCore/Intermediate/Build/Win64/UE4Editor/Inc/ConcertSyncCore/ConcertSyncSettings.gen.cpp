// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncCore/Public/ConcertSyncSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertSyncSettings() {}
// Cross Module References
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FPackageClassFilter();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncCore();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
	CONCERTSYNCCORE_API UScriptStruct* Z_Construct_UScriptStruct_FTransactionClassFilter();
	CONCERTSYNCCORE_API UClass* Z_Construct_UClass_UConcertSyncConfig_NoRegister();
	CONCERTSYNCCORE_API UClass* Z_Construct_UClass_UConcertSyncConfig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FPackageClassFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FPackageClassFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPackageClassFilter, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("PackageClassFilter"), sizeof(FPackageClassFilter), Get_Z_Construct_UScriptStruct_FPackageClassFilter_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FPackageClassFilter>()
{
	return FPackageClassFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPackageClassFilter(FPackageClassFilter::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("PackageClassFilter"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFPackageClassFilter
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFPackageClassFilter()
	{
		UScriptStruct::DeferCppStructOps<FPackageClassFilter>(FName(TEXT("PackageClassFilter")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFPackageClassFilter;
	struct Z_Construct_UScriptStruct_FPackageClassFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AssetClass;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ContentPaths_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ContentPaths_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ContentPaths;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPackageClassFilter_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPackageClassFilter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_AssetClass_MetaData[] = {
		{ "Category", "Sync Settings" },
		{ "Comment", "/**\n\x09 *\x09Optional Class that will filter a specific asset type.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Optional Class that will filter a specific asset type." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_AssetClass = { "AssetClass", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPackageClassFilter, AssetClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_AssetClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_AssetClass_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_ContentPaths_Inner = { "ContentPaths", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_ContentPaths_MetaData[] = {
		{ "Category", "Sync Settings" },
		{ "Comment", "/**\n\x09 *  Paths on which package of a certain asset type will pass the filter, if no asset is specified all asset in those paths pass the filter\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Paths on which package of a certain asset type will pass the filter, if no asset is specified all asset in those paths pass the filter" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_ContentPaths = { "ContentPaths", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPackageClassFilter, ContentPaths), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_ContentPaths_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_ContentPaths_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPackageClassFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_AssetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_ContentPaths_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPackageClassFilter_Statics::NewProp_ContentPaths,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPackageClassFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"PackageClassFilter",
		sizeof(FPackageClassFilter),
		alignof(FPackageClassFilter),
		Z_Construct_UScriptStruct_FPackageClassFilter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPackageClassFilter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPackageClassFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPackageClassFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPackageClassFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPackageClassFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PackageClassFilter"), sizeof(FPackageClassFilter), Get_Z_Construct_UScriptStruct_FPackageClassFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPackageClassFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPackageClassFilter_Hash() { return 2604987322U; }
class UScriptStruct* FTransactionClassFilter::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCORE_API uint32 Get_Z_Construct_UScriptStruct_FTransactionClassFilter_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTransactionClassFilter, Z_Construct_UPackage__Script_ConcertSyncCore(), TEXT("TransactionClassFilter"), sizeof(FTransactionClassFilter), Get_Z_Construct_UScriptStruct_FTransactionClassFilter_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<FTransactionClassFilter>()
{
	return FTransactionClassFilter::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTransactionClassFilter(FTransactionClassFilter::StaticStruct, TEXT("/Script/ConcertSyncCore"), TEXT("TransactionClassFilter"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncCore_StaticRegisterNativesFTransactionClassFilter
{
	FScriptStruct_ConcertSyncCore_StaticRegisterNativesFTransactionClassFilter()
	{
		UScriptStruct::DeferCppStructOps<FTransactionClassFilter>(FName(TEXT("TransactionClassFilter")));
	}
} ScriptStruct_ConcertSyncCore_StaticRegisterNativesFTransactionClassFilter;
	struct Z_Construct_UScriptStruct_FTransactionClassFilter_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectOuterClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectOuterClass;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjectClasses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTransactionClassFilter>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectOuterClass_MetaData[] = {
		{ "Category", "Sync Settings" },
		{ "Comment", "/**\n\x09 *\x09Optional Outer Class that will allow object only if one of their outer match this class.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Optional Outer Class that will allow object only if one of their outer match this class." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectOuterClass = { "ObjectOuterClass", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransactionClassFilter, ObjectOuterClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectOuterClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectOuterClass_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectClasses_Inner = { "ObjectClasses", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectClasses_MetaData[] = {
		{ "Category", "Sync Settings" },
		{ "Comment", "/**\n\x09 * Object classes to filter transaction object on.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Object classes to filter transaction object on." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectClasses = { "ObjectClasses", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTransactionClassFilter, ObjectClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectClasses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectOuterClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::NewProp_ObjectClasses,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
		nullptr,
		&NewStructOps,
		"TransactionClassFilter",
		sizeof(FTransactionClassFilter),
		alignof(FTransactionClassFilter),
		Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTransactionClassFilter()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTransactionClassFilter_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TransactionClassFilter"), sizeof(FTransactionClassFilter), Get_Z_Construct_UScriptStruct_FTransactionClassFilter_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTransactionClassFilter_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTransactionClassFilter_Hash() { return 3552554479U; }
	void UConcertSyncConfig::StaticRegisterNativesUConcertSyncConfig()
	{
	}
	UClass* Z_Construct_UClass_UConcertSyncConfig_NoRegister()
	{
		return UConcertSyncConfig::StaticClass();
	}
	struct Z_Construct_UClass_UConcertSyncConfig_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInteractiveHotReload_MetaData[];
#endif
		static void NewProp_bInteractiveHotReload_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInteractiveHotReload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowPresenceInPIE_MetaData[];
#endif
		static void NewProp_bShowPresenceInPIE_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowPresenceInPIE;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SnapshotTransactionsPerSecond_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SnapshotTransactionsPerSecond;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_IncludeObjectClassFilters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IncludeObjectClassFilters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IncludeObjectClassFilters;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludeTransactionClassFilters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludeTransactionClassFilters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludeTransactionClassFilters;
		static const UE4CodeGen_Private::FFieldPathPropertyParams NewProp_AllowedTransientProperties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AllowedTransientProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AllowedTransientProperties;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludePackageClassFilters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludePackageClassFilters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludePackageClassFilters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertSyncConfig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConcertSyncSettings.h" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bInteractiveHotReload_MetaData[] = {
		{ "Category", "Transaction Settings" },
		{ "Comment", "/**\n\x09 * Should we ask before hot-reloading changed packages?\n\x09 * If disabled we will clobber any local changes when reloading packages.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Should we ask before hot-reloading changed packages?\nIf disabled we will clobber any local changes when reloading packages." },
	};
#endif
	void Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bInteractiveHotReload_SetBit(void* Obj)
	{
		((UConcertSyncConfig*)Obj)->bInteractiveHotReload = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bInteractiveHotReload = { "bInteractiveHotReload", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertSyncConfig), &Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bInteractiveHotReload_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bInteractiveHotReload_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bInteractiveHotReload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bShowPresenceInPIE_MetaData[] = {
		{ "Category", "Transaction Settings" },
		{ "Comment", "/**\n\x09 * Should we show presence when in PIE?\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Should we show presence when in PIE?" },
	};
#endif
	void Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bShowPresenceInPIE_SetBit(void* Obj)
	{
		((UConcertSyncConfig*)Obj)->bShowPresenceInPIE = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bShowPresenceInPIE = { "bShowPresenceInPIE", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UConcertSyncConfig), &Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bShowPresenceInPIE_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bShowPresenceInPIE_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bShowPresenceInPIE_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_SnapshotTransactionsPerSecond_MetaData[] = {
		{ "Category", "Transaction Settings" },
		{ "ClampMin", "1" },
		{ "Comment", "/**\n\x09 * Number of snapshot transactions (eg, moving an object or dragging a slider) that should be sent per-second to other clients.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Number of snapshot transactions (eg, moving an object or dragging a slider) that should be sent per-second to other clients." },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_SnapshotTransactionsPerSecond = { "SnapshotTransactionsPerSecond", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertSyncConfig, SnapshotTransactionsPerSecond), METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_SnapshotTransactionsPerSecond_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_SnapshotTransactionsPerSecond_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_IncludeObjectClassFilters_Inner = { "IncludeObjectClassFilters", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransactionClassFilter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_IncludeObjectClassFilters_MetaData[] = {
		{ "Category", "Transaction Settings" },
		{ "Comment", "/**\n\x09 * Array of Transaction class filter.\n\x09 * Only objects that pass those filters will be included in transaction updates.\n\x09 * @note If this is empty, then all class types will send transaction updates.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Array of Transaction class filter.\nOnly objects that pass those filters will be included in transaction updates.\n@note If this is empty, then all class types will send transaction updates." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_IncludeObjectClassFilters = { "IncludeObjectClassFilters", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertSyncConfig, IncludeObjectClassFilters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_IncludeObjectClassFilters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_IncludeObjectClassFilters_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludeTransactionClassFilters_Inner = { "ExcludeTransactionClassFilters", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransactionClassFilter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludeTransactionClassFilters_MetaData[] = {
		{ "Category", "Transaction Settings" },
		{ "Comment", "/**\n\x09 * Array of additional Transaction class filter.\n\x09 * Objects that matches those filters, will prevent the whole transaction from propagation.\n\x09 * @note These filters takes precedence over the IncludeObjectClassFilters\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Array of additional Transaction class filter.\nObjects that matches those filters, will prevent the whole transaction from propagation.\n@note These filters takes precedence over the IncludeObjectClassFilters" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludeTransactionClassFilters = { "ExcludeTransactionClassFilters", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertSyncConfig, ExcludeTransactionClassFilters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludeTransactionClassFilters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludeTransactionClassFilters_MetaData)) };
	const UE4CodeGen_Private::FFieldPathPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_AllowedTransientProperties_Inner = { "AllowedTransientProperties", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::FieldPath, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, &FProperty::StaticClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_AllowedTransientProperties_MetaData[] = {
		{ "AllowedClasses", "Property" },
		{ "Category", "Transaction Settings" },
		{ "Comment", "/**\n\x09 * Array of transient class properties that we should send transaction updates for even if usually filtered out.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Array of transient class properties that we should send transaction updates for even if usually filtered out." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_AllowedTransientProperties = { "AllowedTransientProperties", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertSyncConfig, AllowedTransientProperties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_AllowedTransientProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_AllowedTransientProperties_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludePackageClassFilters_Inner = { "ExcludePackageClassFilters", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPackageClassFilter, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludePackageClassFilters_MetaData[] = {
		{ "Category", "Package Settings" },
		{ "Comment", "/**\n\x09 * Array of package class filter.\n\x09 * Packages that matches those filters, will be excluded from propagating to the server when saved.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConcertSyncSettings.h" },
		{ "ToolTip", "Array of package class filter.\nPackages that matches those filters, will be excluded from propagating to the server when saved." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludePackageClassFilters = { "ExcludePackageClassFilters", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertSyncConfig, ExcludePackageClassFilters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludePackageClassFilters_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludePackageClassFilters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertSyncConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bInteractiveHotReload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_bShowPresenceInPIE,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_SnapshotTransactionsPerSecond,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_IncludeObjectClassFilters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_IncludeObjectClassFilters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludeTransactionClassFilters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludeTransactionClassFilters,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_AllowedTransientProperties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_AllowedTransientProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludePackageClassFilters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertSyncConfig_Statics::NewProp_ExcludePackageClassFilters,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertSyncConfig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertSyncConfig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertSyncConfig_Statics::ClassParams = {
		&UConcertSyncConfig::StaticClass,
		"ConcertSyncCore",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertSyncConfig_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertSyncConfig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertSyncConfig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertSyncConfig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertSyncConfig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertSyncConfig, 3859997239);
	template<> CONCERTSYNCCORE_API UClass* StaticClass<UConcertSyncConfig>()
	{
		return UConcertSyncConfig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertSyncConfig(Z_Construct_UClass_UConcertSyncConfig, &UConcertSyncConfig::StaticClass, TEXT("/Script/ConcertSyncCore"), TEXT("UConcertSyncConfig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertSyncConfig);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
