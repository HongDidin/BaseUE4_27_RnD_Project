// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCORE_ConcertPresenceEvents_generated_h
#error "ConcertPresenceEvents.generated.h already included, missing '#pragma once' in ConcertPresenceEvents.h"
#endif
#define CONCERTSYNCCORE_ConcertPresenceEvents_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h_114_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientVRPresenceUpdateEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertClientPresenceEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertClientVRPresenceUpdateEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h_87_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertLaserData_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertLaserData>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h_65_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientDesktopPresenceUpdateEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertClientPresenceEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertClientDesktopPresenceUpdateEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h_41_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientPresenceDataUpdateEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct(); \
	typedef FConcertClientPresenceEventBase Super;


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertClientPresenceDataUpdateEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h_32_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientPresenceInVREvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertClientPresenceInVREvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientPresenceVisibilityUpdateEvent_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertClientPresenceVisibilityUpdateEvent>();

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h_11_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertClientPresenceEventBase_Statics; \
	CONCERTSYNCCORE_API static class UScriptStruct* StaticStruct();


template<> CONCERTSYNCCORE_API UScriptStruct* StaticStruct<struct FConcertClientPresenceEventBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncCore_Source_ConcertSyncCore_Public_ConcertPresenceEvents_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
