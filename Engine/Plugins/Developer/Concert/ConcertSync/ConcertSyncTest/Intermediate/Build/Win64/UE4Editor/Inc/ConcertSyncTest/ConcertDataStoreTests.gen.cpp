// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncTest/Private/ConcertDataStoreTests.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertDataStoreTests() {}
// Cross Module References
	CONCERTSYNCTEST_API UScriptStruct* Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncTest();
// End Cross Module References
class UScriptStruct* FConcertDataStore_CustomTypeTest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCTEST_API uint32 Get_Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest, Z_Construct_UPackage__Script_ConcertSyncTest(), TEXT("ConcertDataStore_CustomTypeTest"), sizeof(FConcertDataStore_CustomTypeTest), Get_Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCTEST_API UScriptStruct* StaticStruct<FConcertDataStore_CustomTypeTest>()
{
	return FConcertDataStore_CustomTypeTest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertDataStore_CustomTypeTest(FConcertDataStore_CustomTypeTest::StaticStruct, TEXT("/Script/ConcertSyncTest"), TEXT("ConcertDataStore_CustomTypeTest"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncTest_StaticRegisterNativesFConcertDataStore_CustomTypeTest
{
	FScriptStruct_ConcertSyncTest_StaticRegisterNativesFConcertDataStore_CustomTypeTest()
	{
		UScriptStruct::DeferCppStructOps<FConcertDataStore_CustomTypeTest>(FName(TEXT("ConcertDataStore_CustomTypeTest")));
	}
} ScriptStruct_ConcertSyncTest_StaticRegisterNativesFConcertDataStore_CustomTypeTest;
	struct Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int8Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt8PropertyParams NewProp_Int8Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int64Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_Int64Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloatValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FloatValue;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_IntArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IntArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IntArray;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** A custom type for ConcertDatastore testing purpose. */" },
		{ "ModuleRelativePath", "Private/ConcertDataStoreTests.h" },
		{ "ToolTip", "A custom type for ConcertDatastore testing purpose." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertDataStore_CustomTypeTest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int8Value_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Private/ConcertDataStoreTests.h" },
	};
#endif
	const UE4CodeGen_Private::FInt8PropertyParams Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int8Value = { "Int8Value", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int8, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertDataStore_CustomTypeTest, Int8Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int8Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int8Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int64Value_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Private/ConcertDataStoreTests.h" },
	};
#endif
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int64Value = { "Int64Value", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertDataStore_CustomTypeTest, Int64Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int64Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int64Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_FloatValue_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Private/ConcertDataStoreTests.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_FloatValue = { "FloatValue", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertDataStore_CustomTypeTest, FloatValue), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_FloatValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_FloatValue_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_IntArray_Inner = { "IntArray", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_IntArray_MetaData[] = {
		{ "Category", "Concert Message" },
		{ "ModuleRelativePath", "Private/ConcertDataStoreTests.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_IntArray = { "IntArray", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertDataStore_CustomTypeTest, IntArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_IntArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_IntArray_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int8Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_Int64Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_FloatValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_IntArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::NewProp_IntArray,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncTest,
		nullptr,
		&NewStructOps,
		"ConcertDataStore_CustomTypeTest",
		sizeof(FConcertDataStore_CustomTypeTest),
		alignof(FConcertDataStore_CustomTypeTest),
		Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncTest();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertDataStore_CustomTypeTest"), sizeof(FConcertDataStore_CustomTypeTest), Get_Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertDataStore_CustomTypeTest_Hash() { return 4087672671U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
