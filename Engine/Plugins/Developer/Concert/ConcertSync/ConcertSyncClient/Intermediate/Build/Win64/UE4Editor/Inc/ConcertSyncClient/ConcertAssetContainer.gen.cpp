// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncClient/Private/ConcertAssetContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertAssetContainer() {}
// Cross Module References
	CONCERTSYNCCLIENT_API UClass* Z_Construct_UClass_UConcertAssetContainer_NoRegister();
	CONCERTSYNCCLIENT_API UClass* Z_Construct_UClass_UConcertAssetContainer();
	ENGINE_API UClass* Z_Construct_UClass_UDataAsset();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncClient();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UConcertAssetContainer::StaticRegisterNativesUConcertAssetContainer()
	{
	}
	UClass* Z_Construct_UClass_UConcertAssetContainer_NoRegister()
	{
		return UConcertAssetContainer::StaticClass();
	}
	struct Z_Construct_UClass_UConcertAssetContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenericDesktopMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GenericDesktopMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenericHMDMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GenericHMDMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VivePreControllerMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VivePreControllerMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OculusControllerMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OculusControllerMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GenericControllerMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GenericControllerMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserPointerMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserPointerMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserPointerEndMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserPointerEndMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserPointerStartMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserPointerStartMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresenceMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PresenceMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeadMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HeadMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserCoreMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserCoreMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresenceFadeMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PresenceFadeMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConcertAssetContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncClient,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Asset container for VREditor.\n */" },
		{ "IncludePath", "ConcertAssetContainer.h" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
		{ "ToolTip", "Asset container for VREditor." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericDesktopMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "Comment", "//\n// Meshes\n//\n" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
		{ "ToolTip", "Meshes" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericDesktopMesh = { "GenericDesktopMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, GenericDesktopMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericDesktopMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericDesktopMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericHMDMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericHMDMesh = { "GenericHMDMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, GenericHMDMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericHMDMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericHMDMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_VivePreControllerMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_VivePreControllerMesh = { "VivePreControllerMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, VivePreControllerMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_VivePreControllerMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_VivePreControllerMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_OculusControllerMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_OculusControllerMesh = { "OculusControllerMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, OculusControllerMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_OculusControllerMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_OculusControllerMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericControllerMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericControllerMesh = { "GenericControllerMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, GenericControllerMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericControllerMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericControllerMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerMesh = { "LaserPointerMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, LaserPointerMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerEndMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerEndMesh = { "LaserPointerEndMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, LaserPointerEndMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerEndMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerEndMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerStartMesh_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerStartMesh = { "LaserPointerStartMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, LaserPointerStartMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerStartMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerStartMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "Comment", "//\n// Materials\n//\n" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
		{ "ToolTip", "Materials" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceMaterial = { "PresenceMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, PresenceMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_TextMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_TextMaterial = { "TextMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, TextMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_TextMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_TextMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_HeadMaterial_MetaData[] = {
		{ "Category", "Desktop" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_HeadMaterial = { "HeadMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, HeadMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_HeadMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_HeadMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserCoreMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserCoreMaterial = { "LaserCoreMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, LaserCoreMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserCoreMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserCoreMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserMaterial = { "LaserMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, LaserMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceFadeMaterial_MetaData[] = {
		{ "Category", "Material" },
		{ "ModuleRelativePath", "Private/ConcertAssetContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceFadeMaterial = { "PresenceFadeMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConcertAssetContainer, PresenceFadeMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceFadeMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceFadeMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConcertAssetContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericDesktopMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericHMDMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_VivePreControllerMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_OculusControllerMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_GenericControllerMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerEndMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserPointerStartMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_TextMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_HeadMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserCoreMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_LaserMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConcertAssetContainer_Statics::NewProp_PresenceFadeMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConcertAssetContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConcertAssetContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConcertAssetContainer_Statics::ClassParams = {
		&UConcertAssetContainer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConcertAssetContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConcertAssetContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConcertAssetContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConcertAssetContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConcertAssetContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConcertAssetContainer, 1491236841);
	template<> CONCERTSYNCCLIENT_API UClass* StaticClass<UConcertAssetContainer>()
	{
		return UConcertAssetContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConcertAssetContainer(Z_Construct_UClass_UConcertAssetContainer, &UConcertAssetContainer::StaticClass, TEXT("/Script/ConcertSyncClient"), TEXT("UConcertAssetContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConcertAssetContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
