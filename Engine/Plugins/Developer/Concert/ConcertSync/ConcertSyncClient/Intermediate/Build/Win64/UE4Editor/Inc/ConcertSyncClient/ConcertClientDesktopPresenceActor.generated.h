// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCLIENT_ConcertClientDesktopPresenceActor_generated_h
#error "ConcertClientDesktopPresenceActor.generated.h already included, missing '#pragma once' in ConcertClientDesktopPresenceActor.h"
#endif
#define CONCERTSYNCCLIENT_ConcertClientDesktopPresenceActor_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAConcertClientDesktopPresenceActor(); \
	friend struct Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics; \
public: \
	DECLARE_CLASS(AConcertClientDesktopPresenceActor, AConcertClientPresenceActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertSyncClient"), NO_API) \
	DECLARE_SERIALIZER(AConcertClientDesktopPresenceActor)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAConcertClientDesktopPresenceActor(); \
	friend struct Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics; \
public: \
	DECLARE_CLASS(AConcertClientDesktopPresenceActor, AConcertClientPresenceActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertSyncClient"), NO_API) \
	DECLARE_SERIALIZER(AConcertClientDesktopPresenceActor)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AConcertClientDesktopPresenceActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AConcertClientDesktopPresenceActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AConcertClientDesktopPresenceActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AConcertClientDesktopPresenceActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AConcertClientDesktopPresenceActor(AConcertClientDesktopPresenceActor&&); \
	NO_API AConcertClientDesktopPresenceActor(const AConcertClientDesktopPresenceActor&); \
public:


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AConcertClientDesktopPresenceActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AConcertClientDesktopPresenceActor(AConcertClientDesktopPresenceActor&&); \
	NO_API AConcertClientDesktopPresenceActor(const AConcertClientDesktopPresenceActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AConcertClientDesktopPresenceActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AConcertClientDesktopPresenceActor); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AConcertClientDesktopPresenceActor)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__DesktopMeshComponent() { return STRUCT_OFFSET(AConcertClientDesktopPresenceActor, DesktopMeshComponent); } \
	FORCEINLINE static uint32 __PPO__LaserPointer() { return STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LaserPointer); } \
	FORCEINLINE static uint32 __PPO__LaserMid() { return STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LaserMid); } \
	FORCEINLINE static uint32 __PPO__LaserCoreMid() { return STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LaserCoreMid); } \
	FORCEINLINE static uint32 __PPO__bMovingCamera() { return STRUCT_OFFSET(AConcertClientDesktopPresenceActor, bMovingCamera); } \
	FORCEINLINE static uint32 __PPO__LastEndPoint() { return STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LastEndPoint); } \
	FORCEINLINE static uint32 __PPO__bIsLaserVisible() { return STRUCT_OFFSET(AConcertClientDesktopPresenceActor, bIsLaserVisible); }


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_17_PROLOG
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_INCLASS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ConcertClientDesktopPresenceActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERTSYNCCLIENT_API UClass* StaticClass<class AConcertClientDesktopPresenceActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientDesktopPresenceActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
