// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCLIENT_ConcertClientPresenceActor_generated_h
#error "ConcertClientPresenceActor.generated.h already included, missing '#pragma once' in ConcertClientPresenceActor.h"
#endif
#define CONCERTSYNCCLIENT_ConcertClientPresenceActor_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAConcertClientPresenceActor(); \
	friend struct Z_Construct_UClass_AConcertClientPresenceActor_Statics; \
public: \
	DECLARE_CLASS(AConcertClientPresenceActor, AActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertSyncClient"), NO_API) \
	DECLARE_SERIALIZER(AConcertClientPresenceActor)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_INCLASS \
private: \
	static void StaticRegisterNativesAConcertClientPresenceActor(); \
	friend struct Z_Construct_UClass_AConcertClientPresenceActor_Statics; \
public: \
	DECLARE_CLASS(AConcertClientPresenceActor, AActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ConcertSyncClient"), NO_API) \
	DECLARE_SERIALIZER(AConcertClientPresenceActor)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AConcertClientPresenceActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AConcertClientPresenceActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AConcertClientPresenceActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AConcertClientPresenceActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AConcertClientPresenceActor(AConcertClientPresenceActor&&); \
	NO_API AConcertClientPresenceActor(const AConcertClientPresenceActor&); \
public:


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AConcertClientPresenceActor(AConcertClientPresenceActor&&); \
	NO_API AConcertClientPresenceActor(const AConcertClientPresenceActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AConcertClientPresenceActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AConcertClientPresenceActor); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AConcertClientPresenceActor)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PresenceDeviceType() { return STRUCT_OFFSET(AConcertClientPresenceActor, PresenceDeviceType); } \
	FORCEINLINE static uint32 __PPO__PresenceMeshComponent() { return STRUCT_OFFSET(AConcertClientPresenceActor, PresenceMeshComponent); } \
	FORCEINLINE static uint32 __PPO__PresenceTextComponent() { return STRUCT_OFFSET(AConcertClientPresenceActor, PresenceTextComponent); } \
	FORCEINLINE static uint32 __PPO__PresenceMID() { return STRUCT_OFFSET(AConcertClientPresenceActor, PresenceMID); } \
	FORCEINLINE static uint32 __PPO__TextMID() { return STRUCT_OFFSET(AConcertClientPresenceActor, TextMID); }


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_18_PROLOG
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_INCLASS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERTSYNCCLIENT_API UClass* StaticClass<class AConcertClientPresenceActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Public_ConcertClientPresenceActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
