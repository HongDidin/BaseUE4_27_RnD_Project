// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncClient/Public/ConcertClientDesktopPresenceActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertClientDesktopPresenceActor() {}
// Cross Module References
	CONCERTSYNCCLIENT_API UClass* Z_Construct_UClass_AConcertClientDesktopPresenceActor_NoRegister();
	CONCERTSYNCCLIENT_API UClass* Z_Construct_UClass_AConcertClientDesktopPresenceActor();
	CONCERTSYNCCLIENT_API UClass* Z_Construct_UClass_AConcertClientPresenceActor();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncClient();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USplineMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void AConcertClientDesktopPresenceActor::StaticRegisterNativesAConcertClientDesktopPresenceActor()
	{
	}
	UClass* Z_Construct_UClass_AConcertClientDesktopPresenceActor_NoRegister()
	{
		return AConcertClientDesktopPresenceActor::StaticClass();
	}
	struct Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesktopMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DesktopMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserPointer_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserPointer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserMid_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserMid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LaserCoreMid_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LaserCoreMid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMovingCamera_MetaData[];
#endif
		static void NewProp_bMovingCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMovingCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastEndPoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LastEndPoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsLaserVisible_MetaData[];
#endif
		static void NewProp_bIsLaserVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsLaserVisible;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AConcertClientPresenceActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncClient,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n  * A AConcertClientDesktopPresenceActor is a child of AConcertClientPresenceActor that is used to represent users in desktop \n  */" },
		{ "IncludePath", "ConcertClientDesktopPresenceActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
		{ "ToolTip", "A AConcertClientDesktopPresenceActor is a child of AConcertClientPresenceActor that is used to represent users in desktop" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_DesktopMeshComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Rendering" },
		{ "Comment", "/** The camera mesh component to show visually where the camera is placed */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
		{ "ToolTip", "The camera mesh component to show visually where the camera is placed" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_DesktopMeshComponent = { "DesktopMeshComponent", nullptr, (EPropertyFlags)0x00400000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientDesktopPresenceActor, DesktopMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_DesktopMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_DesktopMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserPointer_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Rendering" },
		{ "Comment", "/** Spline mesh representing laser */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
		{ "ToolTip", "Spline mesh representing laser" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserPointer = { "LaserPointer", nullptr, (EPropertyFlags)0x00400000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LaserPointer), Z_Construct_UClass_USplineMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserPointer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserPointer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserMid_MetaData[] = {
		{ "Comment", "/** Dynamic material for the laser */" },
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
		{ "ToolTip", "Dynamic material for the laser" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserMid = { "LaserMid", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LaserMid), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserMid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserMid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserCoreMid_MetaData[] = {
		{ "Comment", "/** Dynamic material for the laser */" },
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
		{ "ToolTip", "Dynamic material for the laser" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserCoreMid = { "LaserCoreMid", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LaserCoreMid), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserCoreMid_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserCoreMid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bMovingCamera_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
	};
#endif
	void Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bMovingCamera_SetBit(void* Obj)
	{
		((AConcertClientDesktopPresenceActor*)Obj)->bMovingCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bMovingCamera = { "bMovingCamera", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AConcertClientDesktopPresenceActor), &Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bMovingCamera_SetBit, METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bMovingCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bMovingCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LastEndPoint_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LastEndPoint = { "LastEndPoint", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientDesktopPresenceActor, LastEndPoint), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LastEndPoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LastEndPoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bIsLaserVisible_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConcertClientDesktopPresenceActor.h" },
	};
#endif
	void Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bIsLaserVisible_SetBit(void* Obj)
	{
		((AConcertClientDesktopPresenceActor*)Obj)->bIsLaserVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bIsLaserVisible = { "bIsLaserVisible", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AConcertClientDesktopPresenceActor), &Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bIsLaserVisible_SetBit, METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bIsLaserVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bIsLaserVisible_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_DesktopMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserPointer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserMid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LaserCoreMid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bMovingCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_LastEndPoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::NewProp_bIsLaserVisible,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AConcertClientDesktopPresenceActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::ClassParams = {
		&AConcertClientDesktopPresenceActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::PropPointers),
		0,
		0x008002ADu,
		METADATA_PARAMS(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AConcertClientDesktopPresenceActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AConcertClientDesktopPresenceActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AConcertClientDesktopPresenceActor, 841648545);
	template<> CONCERTSYNCCLIENT_API UClass* StaticClass<AConcertClientDesktopPresenceActor>()
	{
		return AConcertClientDesktopPresenceActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AConcertClientDesktopPresenceActor(Z_Construct_UClass_AConcertClientDesktopPresenceActor, &AConcertClientDesktopPresenceActor::StaticClass, TEXT("/Script/ConcertSyncClient"), TEXT("AConcertClientDesktopPresenceActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AConcertClientDesktopPresenceActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
