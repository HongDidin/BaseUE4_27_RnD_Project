// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncClient/Public/ConcertClientPresenceActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertClientPresenceActor() {}
// Cross Module References
	CONCERTSYNCCLIENT_API UClass* Z_Construct_UClass_AConcertClientPresenceActor_NoRegister();
	CONCERTSYNCCLIENT_API UClass* Z_Construct_UClass_AConcertClientPresenceActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncClient();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextRenderComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	void AConcertClientPresenceActor::StaticRegisterNativesAConcertClientPresenceActor()
	{
	}
	UClass* Z_Construct_UClass_AConcertClientPresenceActor_NoRegister()
	{
		return AConcertClientPresenceActor::StaticClass();
	}
	struct Z_Construct_UClass_AConcertClientPresenceActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresenceDeviceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_PresenceDeviceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresenceMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PresenceMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresenceTextComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PresenceTextComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresenceMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PresenceMID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AConcertClientPresenceActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncClient,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientPresenceActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A ConcertClientPresenceActor is a transient actor representing other client presences during a concert client session.\n */" },
		{ "IncludePath", "ConcertClientPresenceActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ConcertClientPresenceActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "A ConcertClientPresenceActor is a transient actor representing other client presences during a concert client session." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceDeviceType_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Rendering" },
		{ "Comment", "/* The device type that this presence represent (i.e Oculus, Vive, Desktop) */" },
		{ "ModuleRelativePath", "Public/ConcertClientPresenceActor.h" },
		{ "ToolTip", "The device type that this presence represent (i.e Oculus, Vive, Desktop)" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceDeviceType = { "PresenceDeviceType", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientPresenceActor, PresenceDeviceType), METADATA_PARAMS(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceDeviceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceDeviceType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMeshComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Rendering" },
		{ "Comment", "/** The camera mesh component to show visually where the camera is placed */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ConcertClientPresenceActor.h" },
		{ "ToolTip", "The camera mesh component to show visually where the camera is placed" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMeshComponent = { "PresenceMeshComponent", nullptr, (EPropertyFlags)0x00200800000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientPresenceActor, PresenceMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceTextComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Rendering" },
		{ "Comment", "/** The text render component to display the associated client's name */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ConcertClientPresenceActor.h" },
		{ "ToolTip", "The text render component to display the associated client's name" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceTextComponent = { "PresenceTextComponent", nullptr, (EPropertyFlags)0x00200800000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientPresenceActor, PresenceTextComponent), Z_Construct_UClass_UTextRenderComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceTextComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceTextComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMID_MetaData[] = {
		{ "Comment", "/** Dynamic material for the presence actor */" },
		{ "ModuleRelativePath", "Public/ConcertClientPresenceActor.h" },
		{ "ToolTip", "Dynamic material for the presence actor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMID = { "PresenceMID", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientPresenceActor, PresenceMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_TextMID_MetaData[] = {
		{ "Comment", "/** Dynamic material for the presence text */" },
		{ "ModuleRelativePath", "Public/ConcertClientPresenceActor.h" },
		{ "ToolTip", "Dynamic material for the presence text" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_TextMID = { "TextMID", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AConcertClientPresenceActor, TextMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_TextMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_TextMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AConcertClientPresenceActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceDeviceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceTextComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_PresenceMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AConcertClientPresenceActor_Statics::NewProp_TextMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AConcertClientPresenceActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AConcertClientPresenceActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AConcertClientPresenceActor_Statics::ClassParams = {
		&AConcertClientPresenceActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AConcertClientPresenceActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientPresenceActor_Statics::PropPointers),
		0,
		0x008002ADu,
		METADATA_PARAMS(Z_Construct_UClass_AConcertClientPresenceActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AConcertClientPresenceActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AConcertClientPresenceActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AConcertClientPresenceActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AConcertClientPresenceActor, 2662774494);
	template<> CONCERTSYNCCLIENT_API UClass* StaticClass<AConcertClientPresenceActor>()
	{
		return AConcertClientPresenceActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AConcertClientPresenceActor(Z_Construct_UClass_AConcertClientPresenceActor, &AConcertClientPresenceActor::StaticClass, TEXT("/Script/ConcertSyncClient"), TEXT("AConcertClientPresenceActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AConcertClientPresenceActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
