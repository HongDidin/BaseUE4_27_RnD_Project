// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CONCERTSYNCCLIENT_ConcertAssetContainer_generated_h
#error "ConcertAssetContainer.generated.h already included, missing '#pragma once' in ConcertAssetContainer.h"
#endif
#define CONCERTSYNCCLIENT_ConcertAssetContainer_generated_h

#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConcertAssetContainer(); \
	friend struct Z_Construct_UClass_UConcertAssetContainer_Statics; \
public: \
	DECLARE_CLASS(UConcertAssetContainer, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ConcertSyncClient"), NO_API) \
	DECLARE_SERIALIZER(UConcertAssetContainer)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUConcertAssetContainer(); \
	friend struct Z_Construct_UClass_UConcertAssetContainer_Statics; \
public: \
	DECLARE_CLASS(UConcertAssetContainer, UDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ConcertSyncClient"), NO_API) \
	DECLARE_SERIALIZER(UConcertAssetContainer)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertAssetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertAssetContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertAssetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertAssetContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertAssetContainer(UConcertAssetContainer&&); \
	NO_API UConcertAssetContainer(const UConcertAssetContainer&); \
public:


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConcertAssetContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConcertAssetContainer(UConcertAssetContainer&&); \
	NO_API UConcertAssetContainer(const UConcertAssetContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConcertAssetContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConcertAssetContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConcertAssetContainer)


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_21_PROLOG
#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_INCLASS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CONCERTSYNCCLIENT_API UClass* StaticClass<class UConcertAssetContainer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_ConcertSync_ConcertSyncClient_Source_ConcertSyncClient_Private_ConcertAssetContainer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
