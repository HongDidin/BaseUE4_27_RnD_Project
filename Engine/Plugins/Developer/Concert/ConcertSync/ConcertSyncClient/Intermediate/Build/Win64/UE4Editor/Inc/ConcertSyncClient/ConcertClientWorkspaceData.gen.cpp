// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ConcertSyncClient/Private/ConcertClientWorkspaceData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertClientWorkspaceData() {}
// Cross Module References
	CONCERTSYNCCLIENT_API UScriptStruct* Z_Construct_UScriptStruct_FConcertClientWorkspaceData();
	UPackage* Z_Construct_UPackage__Script_ConcertSyncClient();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
// End Cross Module References
class UScriptStruct* FConcertClientWorkspaceData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CONCERTSYNCCLIENT_API uint32 Get_Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertClientWorkspaceData, Z_Construct_UPackage__Script_ConcertSyncClient(), TEXT("ConcertClientWorkspaceData"), sizeof(FConcertClientWorkspaceData), Get_Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Hash());
	}
	return Singleton;
}
template<> CONCERTSYNCCLIENT_API UScriptStruct* StaticStruct<FConcertClientWorkspaceData>()
{
	return FConcertClientWorkspaceData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertClientWorkspaceData(FConcertClientWorkspaceData::StaticStruct, TEXT("/Script/ConcertSyncClient"), TEXT("ConcertClientWorkspaceData"), false, nullptr, nullptr);
static struct FScriptStruct_ConcertSyncClient_StaticRegisterNativesFConcertClientWorkspaceData
{
	FScriptStruct_ConcertSyncClient_StaticRegisterNativesFConcertClientWorkspaceData()
	{
		UScriptStruct::DeferCppStructOps<FConcertClientWorkspaceData>(FName(TEXT("ConcertClientWorkspaceData")));
	}
} ScriptStruct_ConcertSyncClient_StaticRegisterNativesFConcertClientWorkspaceData;
	struct Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionIdentifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionIdentifier;
		static const UE4CodeGen_Private::FInt64PropertyParams NewProp_PersistedFiles_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PersistedFiles_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PersistedFiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PersistedFiles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Client workspace data associated with a specific\n * session that persist between connections.\n */" },
		{ "ModuleRelativePath", "Private/ConcertClientWorkspaceData.h" },
		{ "ToolTip", "Client workspace data associated with a specific\nsession that persist between connections." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertClientWorkspaceData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_SessionIdentifier_MetaData[] = {
		{ "Comment", "/** The session identifier that this client data is associated with. */" },
		{ "ModuleRelativePath", "Private/ConcertClientWorkspaceData.h" },
		{ "ToolTip", "The session identifier that this client data is associated with." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_SessionIdentifier = { "SessionIdentifier", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientWorkspaceData, SessionIdentifier), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_SessionIdentifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_SessionIdentifier_MetaData)) };
	const UE4CodeGen_Private::FInt64PropertyParams Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles_ValueProp = { "PersistedFiles", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int64, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles_Key_KeyProp = { "PersistedFiles_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles_MetaData[] = {
		{ "Comment", "/** Map of packages persisted locally from the session associated with their package revision.*/" },
		{ "ModuleRelativePath", "Private/ConcertClientWorkspaceData.h" },
		{ "ToolTip", "Map of packages persisted locally from the session associated with their package revision." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles = { "PersistedFiles", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertClientWorkspaceData, PersistedFiles), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_SessionIdentifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::NewProp_PersistedFiles,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ConcertSyncClient,
		nullptr,
		&NewStructOps,
		"ConcertClientWorkspaceData",
		sizeof(FConcertClientWorkspaceData),
		alignof(FConcertClientWorkspaceData),
		Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertClientWorkspaceData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ConcertSyncClient();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertClientWorkspaceData"), sizeof(FConcertClientWorkspaceData), Get_Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertClientWorkspaceData_Hash() { return 274152443U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
