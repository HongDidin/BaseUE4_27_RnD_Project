// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisasterRecoveryClient/Private/DisasterRecoverySessionInfo.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisasterRecoverySessionInfo() {}
// Cross Module References
	DISASTERRECOVERYCLIENT_API UScriptStruct* Z_Construct_UScriptStruct_FDisasterRecoveryInfo();
	UPackage* Z_Construct_UPackage__Script_DisasterRecoveryClient();
	DISASTERRECOVERYCLIENT_API UScriptStruct* Z_Construct_UScriptStruct_FDisasterRecoverySession();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	DISASTERRECOVERYCLIENT_API UScriptStruct* Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo();
// End Cross Module References
class UScriptStruct* FDisasterRecoveryInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISASTERRECOVERYCLIENT_API uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisasterRecoveryInfo, Z_Construct_UPackage__Script_DisasterRecoveryClient(), TEXT("DisasterRecoveryInfo"), sizeof(FDisasterRecoveryInfo), Get_Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Hash());
	}
	return Singleton;
}
template<> DISASTERRECOVERYCLIENT_API UScriptStruct* StaticStruct<FDisasterRecoveryInfo>()
{
	return FDisasterRecoveryInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisasterRecoveryInfo(FDisasterRecoveryInfo::StaticStruct, TEXT("/Script/DisasterRecoveryClient"), TEXT("DisasterRecoveryInfo"), false, nullptr, nullptr);
static struct FScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoveryInfo
{
	FScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoveryInfo()
	{
		UScriptStruct::DeferCppStructOps<FDisasterRecoveryInfo>(FName(TEXT("DisasterRecoveryInfo")));
	}
} ScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoveryInfo;
	struct Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Revision_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Revision;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ActiveSessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveSessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ActiveSessions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RecentSessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecentSessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RecentSessions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImportedSessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedSessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ImportedSessions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PendingSessions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PendingSessions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PendingSessions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DiscardedRepositoryIds_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DiscardedRepositoryIds_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DiscardedRepositoryIds;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Clients_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Clients_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Clients;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Hold the information for multiple disaster recovery sessions.\n */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "Hold the information for multiple disaster recovery sessions." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisasterRecoveryInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Revision_MetaData[] = {
		{ "Comment", "/** The revision number of the information. Updated everytime the recovery info file is written. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The revision number of the information. Updated everytime the recovery info file is written." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Revision = { "Revision", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryInfo, Revision), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Revision_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Revision_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ActiveSessions_Inner = { "ActiveSessions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisasterRecoverySession, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ActiveSessions_MetaData[] = {
		{ "Comment", "/** The list of running/crashing/crashed sessions. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The list of running/crashing/crashed sessions." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ActiveSessions = { "ActiveSessions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryInfo, ActiveSessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ActiveSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ActiveSessions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_RecentSessions_Inner = { "RecentSessions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisasterRecoverySession, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_RecentSessions_MetaData[] = {
		{ "Comment", "/** The list of recent sessions (rotated over time). */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The list of recent sessions (rotated over time)." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_RecentSessions = { "RecentSessions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryInfo, RecentSessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_RecentSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_RecentSessions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ImportedSessions_Inner = { "ImportedSessions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisasterRecoverySession, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ImportedSessions_MetaData[] = {
		{ "Comment", "/** The list of imported sessions (rotated over time). */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The list of imported sessions (rotated over time)." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ImportedSessions = { "ImportedSessions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryInfo, ImportedSessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ImportedSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ImportedSessions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_PendingSessions_Inner = { "PendingSessions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisasterRecoverySession, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_PendingSessions_MetaData[] = {
		{ "Comment", "/** The list of session being created, for which the repository was created and mounted, but the session not yet started. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The list of session being created, for which the repository was created and mounted, but the session not yet started." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_PendingSessions = { "PendingSessions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryInfo, PendingSessions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_PendingSessions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_PendingSessions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_DiscardedRepositoryIds_Inner = { "DiscardedRepositoryIds", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_DiscardedRepositoryIds_MetaData[] = {
		{ "Comment", "/** The list of session reporitories ID that are scheduled to be discarded, but kept around until the server confirms the deletion. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The list of session reporitories ID that are scheduled to be discarded, but kept around until the server confirms the deletion." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_DiscardedRepositoryIds = { "DiscardedRepositoryIds", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryInfo, DiscardedRepositoryIds), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_DiscardedRepositoryIds_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_DiscardedRepositoryIds_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Clients_Inner = { "Clients", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Clients_MetaData[] = {
		{ "Comment", "/** The list of client currently executing (in different processes). */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The list of client currently executing (in different processes)." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Clients = { "Clients", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryInfo, Clients), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Clients_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Clients_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Revision,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ActiveSessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ActiveSessions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_RecentSessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_RecentSessions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ImportedSessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_ImportedSessions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_PendingSessions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_PendingSessions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_DiscardedRepositoryIds_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_DiscardedRepositoryIds,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Clients_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::NewProp_Clients,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisasterRecoveryClient,
		nullptr,
		&NewStructOps,
		"DisasterRecoveryInfo",
		sizeof(FDisasterRecoveryInfo),
		alignof(FDisasterRecoveryInfo),
		Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisasterRecoveryInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisasterRecoveryClient();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisasterRecoveryInfo"), sizeof(FDisasterRecoveryInfo), Get_Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Hash() { return 851129665U; }
class UScriptStruct* FDisasterRecoveryClientInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISASTERRECOVERYCLIENT_API uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo, Z_Construct_UPackage__Script_DisasterRecoveryClient(), TEXT("DisasterRecoveryClientInfo"), sizeof(FDisasterRecoveryClientInfo), Get_Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Hash());
	}
	return Singleton;
}
template<> DISASTERRECOVERYCLIENT_API UScriptStruct* StaticStruct<FDisasterRecoveryClientInfo>()
{
	return FDisasterRecoveryClientInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisasterRecoveryClientInfo(FDisasterRecoveryClientInfo::StaticStruct, TEXT("/Script/DisasterRecoveryClient"), TEXT("DisasterRecoveryClientInfo"), false, nullptr, nullptr);
static struct FScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoveryClientInfo
{
	FScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoveryClientInfo()
	{
		UScriptStruct::DeferCppStructOps<FDisasterRecoveryClientInfo>(FName(TEXT("DisasterRecoveryClientInfo")));
	}
} ScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoveryClientInfo;
	struct Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientProcessId_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ClientProcessId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientAppId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientAppId;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Information about a disaster recovery client. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "Information about a disaster recovery client." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisasterRecoveryClientInfo>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientProcessId_MetaData[] = {
		{ "Comment", "/** The client process ID. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The client process ID." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientProcessId = { "ClientProcessId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryClientInfo, ClientProcessId), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientProcessId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientProcessId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientAppId_MetaData[] = {
		{ "Comment", "/** The client app ID. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The client app ID." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientAppId = { "ClientAppId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoveryClientInfo, ClientAppId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientAppId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientAppId_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientProcessId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::NewProp_ClientAppId,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisasterRecoveryClient,
		nullptr,
		&NewStructOps,
		"DisasterRecoveryClientInfo",
		sizeof(FDisasterRecoveryClientInfo),
		alignof(FDisasterRecoveryClientInfo),
		Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisasterRecoveryClient();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisasterRecoveryClientInfo"), sizeof(FDisasterRecoveryClientInfo), Get_Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Hash() { return 32304322U; }
class UScriptStruct* FDisasterRecoverySession::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern DISASTERRECOVERYCLIENT_API uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoverySession_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FDisasterRecoverySession, Z_Construct_UPackage__Script_DisasterRecoveryClient(), TEXT("DisasterRecoverySession"), sizeof(FDisasterRecoverySession), Get_Z_Construct_UScriptStruct_FDisasterRecoverySession_Hash());
	}
	return Singleton;
}
template<> DISASTERRECOVERYCLIENT_API UScriptStruct* StaticStruct<FDisasterRecoverySession>()
{
	return FDisasterRecoverySession::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FDisasterRecoverySession(FDisasterRecoverySession::StaticStruct, TEXT("/Script/DisasterRecoveryClient"), TEXT("DisasterRecoverySession"), false, nullptr, nullptr);
static struct FScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoverySession
{
	FScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoverySession()
	{
		UScriptStruct::DeferCppStructOps<FDisasterRecoverySession>(FName(TEXT("DisasterRecoverySession")));
	}
} ScriptStruct_DisasterRecoveryClient_StaticRegisterNativesFDisasterRecoverySession;
	struct Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryId_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RepositoryId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RepositoryRootDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RepositoryRootDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SessionName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MountedByProcessId_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_MountedByProcessId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientProcessId_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ClientProcessId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Flags_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Flags;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Information about a single session info. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "Information about a single session info." },
	};
#endif
	void* Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FDisasterRecoverySession>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryId_MetaData[] = {
		{ "Comment", "/** The repository ID created on the server to store that session. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The repository ID created on the server to store that session." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryId = { "RepositoryId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoverySession, RepositoryId), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryRootDir_MetaData[] = {
		{ "Comment", "/** The immediate parent directory containing this session repository. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The immediate parent directory containing this session repository." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryRootDir = { "RepositoryRootDir", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoverySession, RepositoryRootDir), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryRootDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryRootDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_SessionName_MetaData[] = {
		{ "Comment", "/** The name of the session. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The name of the session." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_SessionName = { "SessionName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoverySession, SessionName), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_SessionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_SessionName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_MountedByProcessId_MetaData[] = {
		{ "Comment", "/** The client that mounted that session repository. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The client that mounted that session repository." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_MountedByProcessId = { "MountedByProcessId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoverySession, MountedByProcessId), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_MountedByProcessId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_MountedByProcessId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_ClientProcessId_MetaData[] = {
		{ "Comment", "/** The process ID of the client connected to the session (the session is live and recording transactions for this client process). */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "The process ID of the client connected to the session (the session is live and recording transactions for this client process)." },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_ClientProcessId = { "ClientProcessId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoverySession, ClientProcessId), METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_ClientProcessId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_ClientProcessId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_Flags_MetaData[] = {
		{ "Comment", "/** Information about the session. */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySessionInfo.h" },
		{ "ToolTip", "Information about the session." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_Flags = { "Flags", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FDisasterRecoverySession, Flags), nullptr, METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_Flags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_Flags_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_RepositoryRootDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_SessionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_MountedByProcessId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_ClientProcessId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::NewProp_Flags,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_DisasterRecoveryClient,
		nullptr,
		&NewStructOps,
		"DisasterRecoverySession",
		sizeof(FDisasterRecoverySession),
		alignof(FDisasterRecoverySession),
		Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FDisasterRecoverySession()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoverySession_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_DisasterRecoveryClient();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("DisasterRecoverySession"), sizeof(FDisasterRecoverySession), Get_Z_Construct_UScriptStruct_FDisasterRecoverySession_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FDisasterRecoverySession_Hash() { return 3996020625U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
