// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISASTERRECOVERYCLIENT_DisasterRecoverySessionInfo_generated_h
#error "DisasterRecoverySessionInfo.generated.h already included, missing '#pragma once' in DisasterRecoverySessionInfo.h"
#endif
#define DISASTERRECOVERYCLIENT_DisasterRecoverySessionInfo_generated_h

#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySessionInfo_h_120_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisasterRecoveryInfo_Statics; \
	DISASTERRECOVERYCLIENT_API static class UScriptStruct* StaticStruct();


template<> DISASTERRECOVERYCLIENT_API UScriptStruct* StaticStruct<struct FDisasterRecoveryInfo>();

#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySessionInfo_h_102_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisasterRecoveryClientInfo_Statics; \
	DISASTERRECOVERYCLIENT_API static class UScriptStruct* StaticStruct();


template<> DISASTERRECOVERYCLIENT_API UScriptStruct* StaticStruct<struct FDisasterRecoveryClientInfo>();

#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySessionInfo_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDisasterRecoverySession_Statics; \
	DISASTERRECOVERYCLIENT_API static class UScriptStruct* StaticStruct();


template<> DISASTERRECOVERYCLIENT_API UScriptStruct* StaticStruct<struct FDisasterRecoverySession>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySessionInfo_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
