// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "DisasterRecoveryClient/Private/DisasterRecoverySettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDisasterRecoverySettings() {}
// Cross Module References
	DISASTERRECOVERYCLIENT_API UClass* Z_Construct_UClass_UDisasterRecoverClientConfig_NoRegister();
	DISASTERRECOVERYCLIENT_API UClass* Z_Construct_UClass_UDisasterRecoverClientConfig();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DisasterRecoveryClient();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UDisasterRecoverClientConfig::StaticRegisterNativesUDisasterRecoverClientConfig()
	{
	}
	UClass* Z_Construct_UClass_UDisasterRecoverClientConfig_NoRegister()
	{
		return UDisasterRecoverClientConfig::StaticClass();
	}
	struct Z_Construct_UClass_UDisasterRecoverClientConfig_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsEnabled_MetaData[];
#endif
		static void NewProp_bIsEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecoverySessionDir_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RecoverySessionDir;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecentSessionMaxCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RecentSessionMaxCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedSessionMaxCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ImportedSessionMaxCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DisasterRecoveryClient,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "DisasterRecoverySettings.h" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_bIsEnabled_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/**\n\x09 * Enables disaster recovery plugin to create and/or restore a recovery sessions when needed.\n\x09 */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySettings.h" },
		{ "ToolTip", "Enables disaster recovery plugin to create and/or restore a recovery sessions when needed." },
	};
#endif
	void Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_bIsEnabled_SetBit(void* Obj)
	{
		((UDisasterRecoverClientConfig*)Obj)->bIsEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_bIsEnabled = { "bIsEnabled", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDisasterRecoverClientConfig), &Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_bIsEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_bIsEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_bIsEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecoverySessionDir_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/**\n\x09 * The root directory where recovery sessions should be stored. If not set or\n\x09 * invalid, the recovery sessions are stored in the project saved directory. The\n\x09 * existing sessions are not moved (but remains accessible) when the directory is changed.\n\x09 */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySettings.h" },
		{ "ToolTip", "The root directory where recovery sessions should be stored. If not set or\ninvalid, the recovery sessions are stored in the project saved directory. The\nexisting sessions are not moved (but remains accessible) when the directory is changed." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecoverySessionDir = { "RecoverySessionDir", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisasterRecoverClientConfig, RecoverySessionDir), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecoverySessionDir_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecoverySessionDir_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecentSessionMaxCount_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/**\n\x09 * The maximum number of recent recovery sessions to keep around. The sessions are rotated\n\x09 * at Editor startup and oldest ones are discarded.\n\x09 */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySettings.h" },
		{ "ToolTip", "The maximum number of recent recovery sessions to keep around. The sessions are rotated\nat Editor startup and oldest ones are discarded." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecentSessionMaxCount = { "RecentSessionMaxCount", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisasterRecoverClientConfig, RecentSessionMaxCount), METADATA_PARAMS(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecentSessionMaxCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecentSessionMaxCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_ImportedSessionMaxCount_MetaData[] = {
		{ "Category", "Client Settings" },
		{ "Comment", "/**\n\x09 * The maximum number of imported recovery session to keep around. The sessions are rotated\n\x09 * at Editor startup and oldest imports are discarded.\n\x09 */" },
		{ "ModuleRelativePath", "Private/DisasterRecoverySettings.h" },
		{ "ToolTip", "The maximum number of imported recovery session to keep around. The sessions are rotated\nat Editor startup and oldest imports are discarded." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_ImportedSessionMaxCount = { "ImportedSessionMaxCount", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDisasterRecoverClientConfig, ImportedSessionMaxCount), METADATA_PARAMS(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_ImportedSessionMaxCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_ImportedSessionMaxCount_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_bIsEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecoverySessionDir,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_RecentSessionMaxCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::NewProp_ImportedSessionMaxCount,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDisasterRecoverClientConfig>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::ClassParams = {
		&UDisasterRecoverClientConfig::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDisasterRecoverClientConfig()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDisasterRecoverClientConfig_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDisasterRecoverClientConfig, 3829563297);
	template<> DISASTERRECOVERYCLIENT_API UClass* StaticClass<UDisasterRecoverClientConfig>()
	{
		return UDisasterRecoverClientConfig::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDisasterRecoverClientConfig(Z_Construct_UClass_UDisasterRecoverClientConfig, &UDisasterRecoverClientConfig::StaticClass, TEXT("/Script/DisasterRecoveryClient"), TEXT("UDisasterRecoverClientConfig"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDisasterRecoverClientConfig);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
