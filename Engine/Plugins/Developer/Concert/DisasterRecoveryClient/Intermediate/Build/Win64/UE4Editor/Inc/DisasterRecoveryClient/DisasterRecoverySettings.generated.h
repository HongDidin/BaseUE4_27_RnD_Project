// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DISASTERRECOVERYCLIENT_DisasterRecoverySettings_generated_h
#error "DisasterRecoverySettings.generated.h already included, missing '#pragma once' in DisasterRecoverySettings.h"
#endif
#define DISASTERRECOVERYCLIENT_DisasterRecoverySettings_generated_h

#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_SPARSE_DATA
#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_RPC_WRAPPERS
#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDisasterRecoverClientConfig(); \
	friend struct Z_Construct_UClass_UDisasterRecoverClientConfig_Statics; \
public: \
	DECLARE_CLASS(UDisasterRecoverClientConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisasterRecoveryClient"), NO_API) \
	DECLARE_SERIALIZER(UDisasterRecoverClientConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUDisasterRecoverClientConfig(); \
	friend struct Z_Construct_UClass_UDisasterRecoverClientConfig_Statics; \
public: \
	DECLARE_CLASS(UDisasterRecoverClientConfig, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DisasterRecoveryClient"), NO_API) \
	DECLARE_SERIALIZER(UDisasterRecoverClientConfig) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDisasterRecoverClientConfig(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDisasterRecoverClientConfig) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisasterRecoverClientConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisasterRecoverClientConfig); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisasterRecoverClientConfig(UDisasterRecoverClientConfig&&); \
	NO_API UDisasterRecoverClientConfig(const UDisasterRecoverClientConfig&); \
public:


#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDisasterRecoverClientConfig(UDisasterRecoverClientConfig&&); \
	NO_API UDisasterRecoverClientConfig(const UDisasterRecoverClientConfig&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDisasterRecoverClientConfig); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDisasterRecoverClientConfig); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDisasterRecoverClientConfig)


#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_10_PROLOG
#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_RPC_WRAPPERS \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_INCLASS \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_SPARSE_DATA \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DISASTERRECOVERYCLIENT_API UClass* StaticClass<class UDisasterRecoverClientConfig>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_Concert_DisasterRecoveryClient_Source_DisasterRecoveryClient_Private_DisasterRecoverySettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
