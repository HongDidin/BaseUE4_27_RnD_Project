// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ANIMATIONSHARINGED_AnimationSharingSetupFactory_generated_h
#error "AnimationSharingSetupFactory.generated.h already included, missing '#pragma once' in AnimationSharingSetupFactory.h"
#endif
#define ANIMATIONSHARINGED_AnimationSharingSetupFactory_generated_h

#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_SPARSE_DATA
#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimationSharingSetupFactory(); \
	friend struct Z_Construct_UClass_UAnimationSharingSetupFactory_Statics; \
public: \
	DECLARE_CLASS(UAnimationSharingSetupFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AnimationSharingEd"), ANIMATIONSHARINGED_API) \
	DECLARE_SERIALIZER(UAnimationSharingSetupFactory)


#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUAnimationSharingSetupFactory(); \
	friend struct Z_Construct_UClass_UAnimationSharingSetupFactory_Statics; \
public: \
	DECLARE_CLASS(UAnimationSharingSetupFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/AnimationSharingEd"), ANIMATIONSHARINGED_API) \
	DECLARE_SERIALIZER(UAnimationSharingSetupFactory)


#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ANIMATIONSHARINGED_API UAnimationSharingSetupFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimationSharingSetupFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ANIMATIONSHARINGED_API, UAnimationSharingSetupFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimationSharingSetupFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ANIMATIONSHARINGED_API UAnimationSharingSetupFactory(UAnimationSharingSetupFactory&&); \
	ANIMATIONSHARINGED_API UAnimationSharingSetupFactory(const UAnimationSharingSetupFactory&); \
public:


#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	ANIMATIONSHARINGED_API UAnimationSharingSetupFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	ANIMATIONSHARINGED_API UAnimationSharingSetupFactory(UAnimationSharingSetupFactory&&); \
	ANIMATIONSHARINGED_API UAnimationSharingSetupFactory(const UAnimationSharingSetupFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(ANIMATIONSHARINGED_API, UAnimationSharingSetupFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimationSharingSetupFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimationSharingSetupFactory)


#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_13_PROLOG
#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_INCLASS \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_SPARSE_DATA \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class AnimationSharingSetupFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ANIMATIONSHARINGED_API UClass* StaticClass<class UAnimationSharingSetupFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_AnimationSharing_Source_AnimationSharingEd_Public_AnimationSharingSetupFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
