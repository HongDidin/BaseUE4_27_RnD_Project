// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OneSkyLocalizationService/Private/OneSkyLocalizationServiceSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOneSkyLocalizationServiceSettings() {}
// Cross Module References
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting();
	UPackage* Z_Construct_UPackage__Script_OneSkyLocalizationService();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	ONESKYLOCALIZATIONSERVICE_API UClass* Z_Construct_UClass_UOneSkyLocalizationTargetSettings_NoRegister();
	ONESKYLOCALIZATIONSERVICE_API UClass* Z_Construct_UClass_UOneSkyLocalizationTargetSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FOneSkyLocalizationTargetSetting::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyLocalizationTargetSetting"), sizeof(FOneSkyLocalizationTargetSetting), Get_Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyLocalizationTargetSetting>()
{
	return FOneSkyLocalizationTargetSetting::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyLocalizationTargetSetting(FOneSkyLocalizationTargetSetting::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyLocalizationTargetSetting"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyLocalizationTargetSetting
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyLocalizationTargetSetting()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyLocalizationTargetSetting>(FName(TEXT("OneSkyLocalizationTargetSetting")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyLocalizationTargetSetting;
	struct Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetGuid_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetGuid;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OneSkyProjectId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OneSkyProjectId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OneSkyFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OneSkyFileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Holds the OneSky settings for a Localization Target.\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceSettings.h" },
		{ "ToolTip", "Holds the OneSky settings for a Localization Target." },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyLocalizationTargetSetting>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_TargetGuid_MetaData[] = {
		{ "Comment", "/**\n\x09* The GUID of the LocalizationTarget that these OneSky settings are for\n\x09*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceSettings.h" },
		{ "ToolTip", "The GUID of the LocalizationTarget that these OneSky settings are for" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_TargetGuid = { "TargetGuid", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyLocalizationTargetSetting, TargetGuid), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_TargetGuid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_TargetGuid_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyProjectId_MetaData[] = {
		{ "Comment", "/**\n\x09* The id of the OneSky Project  this target belongs to\n\x09*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceSettings.h" },
		{ "ToolTip", "The id of the OneSky Project  this target belongs to" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyProjectId = { "OneSkyProjectId", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyLocalizationTargetSetting, OneSkyProjectId), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyProjectId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyProjectId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyFileName_MetaData[] = {
		{ "Comment", "/**\n\x09* The name of the file that corresponds to this target\n\x09*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceSettings.h" },
		{ "ToolTip", "The name of the file that corresponds to this target" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyFileName = { "OneSkyFileName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyLocalizationTargetSetting, OneSkyFileName), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyFileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_TargetGuid,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyProjectId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::NewProp_OneSkyFileName,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyLocalizationTargetSetting",
		sizeof(FOneSkyLocalizationTargetSetting),
		alignof(FOneSkyLocalizationTargetSetting),
		Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyLocalizationTargetSetting"), sizeof(FOneSkyLocalizationTargetSetting), Get_Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Hash() { return 1929049518U; }
	void UOneSkyLocalizationTargetSettings::StaticRegisterNativesUOneSkyLocalizationTargetSettings()
	{
	}
	UClass* Z_Construct_UClass_UOneSkyLocalizationTargetSettings_NoRegister()
	{
		return UOneSkyLocalizationTargetSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TargetSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "OneSkyLocalizationServiceSettings.h" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::NewProp_TargetSettings_Inner = { "TargetSettings", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::NewProp_TargetSettings_MetaData[] = {
		{ "Comment", "/**\n\x09* The array of settings for each target of this project \n\x09*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceSettings.h" },
		{ "ToolTip", "The array of settings for each target of this project" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::NewProp_TargetSettings = { "TargetSettings", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOneSkyLocalizationTargetSettings, TargetSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::NewProp_TargetSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::NewProp_TargetSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::NewProp_TargetSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::NewProp_TargetSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOneSkyLocalizationTargetSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::ClassParams = {
		&UOneSkyLocalizationTargetSettings::StaticClass,
		"LocalizationServiceSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOneSkyLocalizationTargetSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOneSkyLocalizationTargetSettings, 39521938);
	template<> ONESKYLOCALIZATIONSERVICE_API UClass* StaticClass<UOneSkyLocalizationTargetSettings>()
	{
		return UOneSkyLocalizationTargetSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOneSkyLocalizationTargetSettings(Z_Construct_UClass_UOneSkyLocalizationTargetSettings, &UOneSkyLocalizationTargetSettings::StaticClass, TEXT("/Script/OneSkyLocalizationService"), TEXT("UOneSkyLocalizationTargetSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOneSkyLocalizationTargetSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
