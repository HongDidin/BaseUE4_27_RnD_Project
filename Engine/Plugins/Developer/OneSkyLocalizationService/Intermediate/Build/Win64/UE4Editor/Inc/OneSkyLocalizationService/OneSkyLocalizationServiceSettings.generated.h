// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ONESKYLOCALIZATIONSERVICE_OneSkyLocalizationServiceSettings_generated_h
#error "OneSkyLocalizationServiceSettings.generated.h already included, missing '#pragma once' in OneSkyLocalizationServiceSettings.h"
#endif
#define ONESKYLOCALIZATIONSERVICE_OneSkyLocalizationServiceSettings_generated_h

#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FOneSkyLocalizationTargetSetting_Statics; \
	ONESKYLOCALIZATIONSERVICE_API static class UScriptStruct* StaticStruct();


template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<struct FOneSkyLocalizationTargetSetting>();

#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_SPARSE_DATA
#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_RPC_WRAPPERS
#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOneSkyLocalizationTargetSettings(); \
	friend struct Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics; \
public: \
	DECLARE_CLASS(UOneSkyLocalizationTargetSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OneSkyLocalizationService"), NO_API) \
	DECLARE_SERIALIZER(UOneSkyLocalizationTargetSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("LocalizationServiceSettings");} \



#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUOneSkyLocalizationTargetSettings(); \
	friend struct Z_Construct_UClass_UOneSkyLocalizationTargetSettings_Statics; \
public: \
	DECLARE_CLASS(UOneSkyLocalizationTargetSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/OneSkyLocalizationService"), NO_API) \
	DECLARE_SERIALIZER(UOneSkyLocalizationTargetSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("LocalizationServiceSettings");} \



#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOneSkyLocalizationTargetSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOneSkyLocalizationTargetSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOneSkyLocalizationTargetSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOneSkyLocalizationTargetSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOneSkyLocalizationTargetSettings(UOneSkyLocalizationTargetSettings&&); \
	NO_API UOneSkyLocalizationTargetSettings(const UOneSkyLocalizationTargetSettings&); \
public:


#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOneSkyLocalizationTargetSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOneSkyLocalizationTargetSettings(UOneSkyLocalizationTargetSettings&&); \
	NO_API UOneSkyLocalizationTargetSettings(const UOneSkyLocalizationTargetSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOneSkyLocalizationTargetSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOneSkyLocalizationTargetSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOneSkyLocalizationTargetSettings)


#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_42_PROLOG
#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_SPARSE_DATA \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_RPC_WRAPPERS \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_INCLASS \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_SPARSE_DATA \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h_45_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class OneSkyLocalizationTargetSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ONESKYLOCALIZATIONSERVICE_API UClass* StaticClass<class UOneSkyLocalizationTargetSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Developer_OneSkyLocalizationService_Source_OneSkyLocalizationService_Private_OneSkyLocalizationServiceSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
