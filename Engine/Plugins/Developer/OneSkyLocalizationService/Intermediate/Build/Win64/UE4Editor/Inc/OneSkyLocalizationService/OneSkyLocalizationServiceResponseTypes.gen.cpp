// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "OneSkyLocalizationService/Private/OneSkyLocalizationServiceResponseTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOneSkyLocalizationServiceResponseTypes() {}
// Cross Module References
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse();
	UPackage* Z_Construct_UPackage__Script_OneSkyLocalizationService();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta();
	ONESKYLOCALIZATIONSERVICE_API UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem();
// End Cross Module References
class UScriptStruct* FOneSkyShowImportTaskResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowImportTaskResponse"), sizeof(FOneSkyShowImportTaskResponse), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowImportTaskResponse>()
{
	return FOneSkyShowImportTaskResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowImportTaskResponse(FOneSkyShowImportTaskResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowImportTaskResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowImportTaskResponse>(FName(TEXT("OneSkyShowImportTaskResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponse;
	struct Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a Show Import Task query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/import_task.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a Show Import Task query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/import_task.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowImportTaskResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponse, meta), Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_meta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponse, data), Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowImportTaskResponse",
		sizeof(FOneSkyShowImportTaskResponse),
		alignof(FOneSkyShowImportTaskResponse),
		Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowImportTaskResponse"), sizeof(FOneSkyShowImportTaskResponse), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponse_Hash() { return 4169206413U; }
class UScriptStruct* FOneSkyShowImportTaskResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowImportTaskResponseData"), sizeof(FOneSkyShowImportTaskResponseData), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowImportTaskResponseData>()
{
	return FOneSkyShowImportTaskResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowImportTaskResponseData(FOneSkyShowImportTaskResponseData::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowImportTaskResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseData
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseData()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowImportTaskResponseData>(FName(TEXT("OneSkyShowImportTaskResponseData")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseData;
	struct Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_file_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_file;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_string_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_string_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_word_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_word_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_status;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_created_at_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_created_at;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_created_at_timestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_created_at_timestamp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowImportTaskResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseData, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_file_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_file = { "file", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseData, file), Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_file_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_file_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_string_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_string_count = { "string_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseData, string_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_string_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_string_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_word_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_word_count = { "word_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseData, word_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_word_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_word_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseData, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_status_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at = { "created_at", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseData, created_at), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_timestamp_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_timestamp = { "created_at_timestamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseData, created_at_timestamp), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_timestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_timestamp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_file,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_string_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_word_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::NewProp_created_at_timestamp,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowImportTaskResponseData",
		sizeof(FOneSkyShowImportTaskResponseData),
		alignof(FOneSkyShowImportTaskResponseData),
		Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowImportTaskResponseData"), sizeof(FOneSkyShowImportTaskResponseData), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseData_Hash() { return 446600853U; }
class UScriptStruct* FOneSkyShowImportTaskResponseFile::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowImportTaskResponseFile"), sizeof(FOneSkyShowImportTaskResponseFile), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowImportTaskResponseFile>()
{
	return FOneSkyShowImportTaskResponseFile::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowImportTaskResponseFile(FOneSkyShowImportTaskResponseFile::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowImportTaskResponseFile"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseFile
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseFile()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowImportTaskResponseFile>(FName(TEXT("OneSkyShowImportTaskResponseFile")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseFile;
	struct Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_format_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_format;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_locale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowImportTaskResponseFile>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseFile, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_format_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_format = { "format", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseFile, format), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_format_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_format_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseFile, locale), Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_locale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_format,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::NewProp_locale,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowImportTaskResponseFile",
		sizeof(FOneSkyShowImportTaskResponseFile),
		alignof(FOneSkyShowImportTaskResponseFile),
		Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowImportTaskResponseFile"), sizeof(FOneSkyShowImportTaskResponseFile), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseFile_Hash() { return 3934134714U; }
class UScriptStruct* FOneSkyShowImportTaskResponseLocale::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowImportTaskResponseLocale"), sizeof(FOneSkyShowImportTaskResponseLocale), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowImportTaskResponseLocale>()
{
	return FOneSkyShowImportTaskResponseLocale::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowImportTaskResponseLocale(FOneSkyShowImportTaskResponseLocale::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowImportTaskResponseLocale"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseLocale
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseLocale()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowImportTaskResponseLocale>(FName(TEXT("OneSkyShowImportTaskResponseLocale")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseLocale;
	struct Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_english_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_english_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_local_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_local_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_locale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_region;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowImportTaskResponseLocale>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseLocale, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_english_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_english_name = { "english_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseLocale, english_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_english_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_english_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_local_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_local_name = { "local_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseLocale, local_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_local_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_local_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseLocale, locale), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_locale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_region_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_region = { "region", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseLocale, region), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_region_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_region_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_english_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_local_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_locale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::NewProp_region,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowImportTaskResponseLocale",
		sizeof(FOneSkyShowImportTaskResponseLocale),
		alignof(FOneSkyShowImportTaskResponseLocale),
		Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowImportTaskResponseLocale"), sizeof(FOneSkyShowImportTaskResponseLocale), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseLocale_Hash() { return 2137116635U; }
class UScriptStruct* FOneSkyShowImportTaskResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowImportTaskResponseMeta"), sizeof(FOneSkyShowImportTaskResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowImportTaskResponseMeta>()
{
	return FOneSkyShowImportTaskResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowImportTaskResponseMeta(FOneSkyShowImportTaskResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowImportTaskResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowImportTaskResponseMeta>(FName(TEXT("OneSkyShowImportTaskResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowImportTaskResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// SHOW IMPORT TASK \n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "SHOW IMPORT TASK" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowImportTaskResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowImportTaskResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowImportTaskResponseMeta",
		sizeof(FOneSkyShowImportTaskResponseMeta),
		alignof(FOneSkyShowImportTaskResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowImportTaskResponseMeta"), sizeof(FOneSkyShowImportTaskResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowImportTaskResponseMeta_Hash() { return 490934584U; }
class UScriptStruct* FOneSkyListPhraseCollectionsResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListPhraseCollectionsResponse"), sizeof(FOneSkyListPhraseCollectionsResponse), Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListPhraseCollectionsResponse>()
{
	return FOneSkyListPhraseCollectionsResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListPhraseCollectionsResponse(FOneSkyListPhraseCollectionsResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListPhraseCollectionsResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListPhraseCollectionsResponse>(FName(TEXT("OneSkyListPhraseCollectionsResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponse;
	struct Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a List Uploaded Files query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/file.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a List Uploaded Files query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/file.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListPhraseCollectionsResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponse, meta), Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_meta_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponse, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListPhraseCollectionsResponse",
		sizeof(FOneSkyListPhraseCollectionsResponse),
		alignof(FOneSkyListPhraseCollectionsResponse),
		Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListPhraseCollectionsResponse"), sizeof(FOneSkyListPhraseCollectionsResponse), Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponse_Hash() { return 3750918763U; }
class UScriptStruct* FOneSkyListPhraseCollectionsResponseDataItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListPhraseCollectionsResponseDataItem"), sizeof(FOneSkyListPhraseCollectionsResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListPhraseCollectionsResponseDataItem>()
{
	return FOneSkyListPhraseCollectionsResponseDataItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem(FOneSkyListPhraseCollectionsResponseDataItem::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListPhraseCollectionsResponseDataItem"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponseDataItem
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponseDataItem()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListPhraseCollectionsResponseDataItem>(FName(TEXT("OneSkyListPhraseCollectionsResponseDataItem")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponseDataItem;
	struct Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_key;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_created_at_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_created_at;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_created_at_timestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_created_at_timestamp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListPhraseCollectionsResponseDataItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_key_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_key = { "key", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseDataItem, key), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_key_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_key_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at = { "created_at", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseDataItem, created_at), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_timestamp_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_timestamp = { "created_at_timestamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseDataItem, created_at_timestamp), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_timestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_timestamp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::NewProp_created_at_timestamp,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListPhraseCollectionsResponseDataItem",
		sizeof(FOneSkyListPhraseCollectionsResponseDataItem),
		alignof(FOneSkyListPhraseCollectionsResponseDataItem),
		Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListPhraseCollectionsResponseDataItem"), sizeof(FOneSkyListPhraseCollectionsResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseDataItem_Hash() { return 849342151U; }
class UScriptStruct* FOneSkyListPhraseCollectionsResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListPhraseCollectionsResponseMeta"), sizeof(FOneSkyListPhraseCollectionsResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListPhraseCollectionsResponseMeta>()
{
	return FOneSkyListPhraseCollectionsResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta(FOneSkyListPhraseCollectionsResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListPhraseCollectionsResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListPhraseCollectionsResponseMeta>(FName(TEXT("OneSkyListPhraseCollectionsResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListPhraseCollectionsResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_record_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_record_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_page_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_page_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_next_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_next_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_prev_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_prev_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_first_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_first_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_last_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_last_page;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// LIST PHRASE COLLECTIONS\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "LIST PHRASE COLLECTIONS" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListPhraseCollectionsResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_status_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_record_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_record_count = { "record_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseMeta, record_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_record_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_record_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_page_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_page_count = { "page_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseMeta, page_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_page_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_page_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_next_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_next_page = { "next_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseMeta, next_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_next_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_next_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_prev_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_prev_page = { "prev_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseMeta, prev_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_prev_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_prev_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_first_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_first_page = { "first_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseMeta, first_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_first_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_first_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_last_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_last_page = { "last_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListPhraseCollectionsResponseMeta, last_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_last_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_last_page_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_record_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_page_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_next_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_prev_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_first_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::NewProp_last_page,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListPhraseCollectionsResponseMeta",
		sizeof(FOneSkyListPhraseCollectionsResponseMeta),
		alignof(FOneSkyListPhraseCollectionsResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListPhraseCollectionsResponseMeta"), sizeof(FOneSkyListPhraseCollectionsResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListPhraseCollectionsResponseMeta_Hash() { return 3614246980U; }
class UScriptStruct* FOneSkyUploadFileResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyUploadFileResponse"), sizeof(FOneSkyUploadFileResponse), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyUploadFileResponse>()
{
	return FOneSkyUploadFileResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyUploadFileResponse(FOneSkyUploadFileResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyUploadFileResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyUploadFileResponse>(FName(TEXT("OneSkyUploadFileResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponse;
	struct Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from an Upload File post to OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/file.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from an Upload File post to OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/file.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyUploadFileResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponse, meta), Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_meta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponse, data), Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyUploadFileResponse",
		sizeof(FOneSkyUploadFileResponse),
		alignof(FOneSkyUploadFileResponse),
		Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyUploadFileResponse"), sizeof(FOneSkyUploadFileResponse), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponse_Hash() { return 2608812970U; }
class UScriptStruct* FOneSkyUploadFileResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyUploadFileResponseData"), sizeof(FOneSkyUploadFileResponseData), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyUploadFileResponseData>()
{
	return FOneSkyUploadFileResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyUploadFileResponseData(FOneSkyUploadFileResponseData::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyUploadFileResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseData
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseData()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyUploadFileResponseData>(FName(TEXT("OneSkyUploadFileResponseData")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseData;
	struct Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_format_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_format;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_language_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_language;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_import_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_import;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyUploadFileResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseData, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_format_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_format = { "format", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseData, format), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_format_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_format_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_language_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_language = { "language", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseData, language), Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_language_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_language_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_import_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_import = { "import", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseData, import), Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_import_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_import_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_format,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_language,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::NewProp_import,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyUploadFileResponseData",
		sizeof(FOneSkyUploadFileResponseData),
		alignof(FOneSkyUploadFileResponseData),
		Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyUploadFileResponseData"), sizeof(FOneSkyUploadFileResponseData), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseData_Hash() { return 3583648701U; }
class UScriptStruct* FOneSkyUploadedFileResponseImport::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyUploadedFileResponseImport"), sizeof(FOneSkyUploadedFileResponseImport), Get_Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyUploadedFileResponseImport>()
{
	return FOneSkyUploadedFileResponseImport::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyUploadedFileResponseImport(FOneSkyUploadedFileResponseImport::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyUploadedFileResponseImport"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadedFileResponseImport
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadedFileResponseImport()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyUploadedFileResponseImport>(FName(TEXT("OneSkyUploadedFileResponseImport")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadedFileResponseImport;
	struct Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_created_at_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_created_at;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_created_at_timestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_created_at_timestamp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyUploadedFileResponseImport>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadedFileResponseImport, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at = { "created_at", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadedFileResponseImport, created_at), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_timestamp_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_timestamp = { "created_at_timestamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadedFileResponseImport, created_at_timestamp), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_timestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_timestamp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::NewProp_created_at_timestamp,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyUploadedFileResponseImport",
		sizeof(FOneSkyUploadedFileResponseImport),
		alignof(FOneSkyUploadedFileResponseImport),
		Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyUploadedFileResponseImport"), sizeof(FOneSkyUploadedFileResponseImport), Get_Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadedFileResponseImport_Hash() { return 2451364526U; }
class UScriptStruct* FOneSkyUploadFileResponseLanguage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyUploadFileResponseLanguage"), sizeof(FOneSkyUploadFileResponseLanguage), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyUploadFileResponseLanguage>()
{
	return FOneSkyUploadFileResponseLanguage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyUploadFileResponseLanguage(FOneSkyUploadFileResponseLanguage::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyUploadFileResponseLanguage"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseLanguage
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseLanguage()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyUploadFileResponseLanguage>(FName(TEXT("OneSkyUploadFileResponseLanguage")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseLanguage;
	struct Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_english_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_english_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_local_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_local_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_locale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_region;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyUploadFileResponseLanguage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseLanguage, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_english_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_english_name = { "english_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseLanguage, english_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_english_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_english_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_local_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_local_name = { "local_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseLanguage, local_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_local_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_local_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseLanguage, locale), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_locale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_region_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_region = { "region", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseLanguage, region), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_region_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_region_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_english_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_local_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_locale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::NewProp_region,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyUploadFileResponseLanguage",
		sizeof(FOneSkyUploadFileResponseLanguage),
		alignof(FOneSkyUploadFileResponseLanguage),
		Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyUploadFileResponseLanguage"), sizeof(FOneSkyUploadFileResponseLanguage), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseLanguage_Hash() { return 3853427647U; }
class UScriptStruct* FOneSkyUploadFileResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyUploadFileResponseMeta"), sizeof(FOneSkyUploadFileResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyUploadFileResponseMeta>()
{
	return FOneSkyUploadFileResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyUploadFileResponseMeta(FOneSkyUploadFileResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyUploadFileResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyUploadFileResponseMeta>(FName(TEXT("OneSkyUploadFileResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyUploadFileResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// UPLOAD FILE\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "UPLOAD FILE" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyUploadFileResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyUploadFileResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyUploadFileResponseMeta",
		sizeof(FOneSkyUploadFileResponseMeta),
		alignof(FOneSkyUploadFileResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyUploadFileResponseMeta"), sizeof(FOneSkyUploadFileResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyUploadFileResponseMeta_Hash() { return 1965212837U; }
class UScriptStruct* FOneSkyListUploadedFilesResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListUploadedFilesResponse"), sizeof(FOneSkyListUploadedFilesResponse), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListUploadedFilesResponse>()
{
	return FOneSkyListUploadedFilesResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListUploadedFilesResponse(FOneSkyListUploadedFilesResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListUploadedFilesResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListUploadedFilesResponse>(FName(TEXT("OneSkyListUploadedFilesResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponse;
	struct Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a List Uploaded Files query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/file.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a List Uploaded Files query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/file.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListUploadedFilesResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponse, meta), Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_meta_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponse, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListUploadedFilesResponse",
		sizeof(FOneSkyListUploadedFilesResponse),
		alignof(FOneSkyListUploadedFilesResponse),
		Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListUploadedFilesResponse"), sizeof(FOneSkyListUploadedFilesResponse), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponse_Hash() { return 3316666982U; }
class UScriptStruct* FOneSkyListUploadedFilesResponseDataItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListUploadedFilesResponseDataItem"), sizeof(FOneSkyListUploadedFilesResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListUploadedFilesResponseDataItem>()
{
	return FOneSkyListUploadedFilesResponseDataItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem(FOneSkyListUploadedFilesResponseDataItem::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListUploadedFilesResponseDataItem"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseDataItem
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseDataItem()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListUploadedFilesResponseDataItem>(FName(TEXT("OneSkyListUploadedFilesResponseDataItem")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseDataItem;
	struct Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_file_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_file_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_string_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_string_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_last_import_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_last_import;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_uploaded_at_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_uploaded_at;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_uploaded_at_timestamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_uploaded_at_timestamp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListUploadedFilesResponseDataItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_file_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_file_name = { "file_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseDataItem, file_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_file_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_file_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_string_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_string_count = { "string_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseDataItem, string_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_string_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_string_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_last_import_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_last_import = { "last_import", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseDataItem, last_import), Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_last_import_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_last_import_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at = { "uploaded_at", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseDataItem, uploaded_at), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_timestamp_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_timestamp = { "uploaded_at_timestamp", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseDataItem, uploaded_at_timestamp), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_timestamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_timestamp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_file_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_string_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_last_import,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::NewProp_uploaded_at_timestamp,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListUploadedFilesResponseDataItem",
		sizeof(FOneSkyListUploadedFilesResponseDataItem),
		alignof(FOneSkyListUploadedFilesResponseDataItem),
		Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListUploadedFilesResponseDataItem"), sizeof(FOneSkyListUploadedFilesResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseDataItem_Hash() { return 1906176221U; }
class UScriptStruct* FOneSkyListUploadedFilesResponseLastImport::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListUploadedFilesResponseLastImport"), sizeof(FOneSkyListUploadedFilesResponseLastImport), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListUploadedFilesResponseLastImport>()
{
	return FOneSkyListUploadedFilesResponseLastImport::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport(FOneSkyListUploadedFilesResponseLastImport::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListUploadedFilesResponseLastImport"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseLastImport
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseLastImport()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListUploadedFilesResponseLastImport>(FName(TEXT("OneSkyListUploadedFilesResponseLastImport")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseLastImport;
	struct Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListUploadedFilesResponseLastImport>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseLastImport, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseLastImport, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListUploadedFilesResponseLastImport",
		sizeof(FOneSkyListUploadedFilesResponseLastImport),
		alignof(FOneSkyListUploadedFilesResponseLastImport),
		Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListUploadedFilesResponseLastImport"), sizeof(FOneSkyListUploadedFilesResponseLastImport), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseLastImport_Hash() { return 3930768838U; }
class UScriptStruct* FOneSkyListUploadedFilesResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListUploadedFilesResponseMeta"), sizeof(FOneSkyListUploadedFilesResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListUploadedFilesResponseMeta>()
{
	return FOneSkyListUploadedFilesResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta(FOneSkyListUploadedFilesResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListUploadedFilesResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListUploadedFilesResponseMeta>(FName(TEXT("OneSkyListUploadedFilesResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListUploadedFilesResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_record_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_record_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_page_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_page_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_next_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_next_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_prev_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_prev_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_first_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_first_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_last_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_last_page;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// LIST UPLOADED FILES\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "LIST UPLOADED FILES" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListUploadedFilesResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_status_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_record_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_record_count = { "record_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseMeta, record_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_record_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_record_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_page_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_page_count = { "page_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseMeta, page_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_page_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_page_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_next_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_next_page = { "next_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseMeta, next_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_next_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_next_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_prev_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_prev_page = { "prev_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseMeta, prev_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_prev_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_prev_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_first_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_first_page = { "first_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseMeta, first_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_first_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_first_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_last_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_last_page = { "last_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListUploadedFilesResponseMeta, last_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_last_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_last_page_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_record_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_page_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_next_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_prev_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_first_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::NewProp_last_page,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListUploadedFilesResponseMeta",
		sizeof(FOneSkyListUploadedFilesResponseMeta),
		alignof(FOneSkyListUploadedFilesResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListUploadedFilesResponseMeta"), sizeof(FOneSkyListUploadedFilesResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListUploadedFilesResponseMeta_Hash() { return 1967212787U; }
class UScriptStruct* FOneSkyTranslationStatusResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyTranslationStatusResponse"), sizeof(FOneSkyTranslationStatusResponse), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyTranslationStatusResponse>()
{
	return FOneSkyTranslationStatusResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyTranslationStatusResponse(FOneSkyTranslationStatusResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyTranslationStatusResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyTranslationStatusResponse>(FName(TEXT("OneSkyTranslationStatusResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponse;
	struct Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a Translation Status query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/translation.md#status---translations-status\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a Translation Status query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/translation.md#status---translations-status" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyTranslationStatusResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponse, meta), Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_meta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponse, data), Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyTranslationStatusResponse",
		sizeof(FOneSkyTranslationStatusResponse),
		alignof(FOneSkyTranslationStatusResponse),
		Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyTranslationStatusResponse"), sizeof(FOneSkyTranslationStatusResponse), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponse_Hash() { return 1725517624U; }
class UScriptStruct* FOneSkyTranslationStatusResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyTranslationStatusResponseData"), sizeof(FOneSkyTranslationStatusResponseData), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyTranslationStatusResponseData>()
{
	return FOneSkyTranslationStatusResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyTranslationStatusResponseData(FOneSkyTranslationStatusResponseData::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyTranslationStatusResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseData
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseData()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyTranslationStatusResponseData>(FName(TEXT("OneSkyTranslationStatusResponseData")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseData;
	struct Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_file_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_file_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_locale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_progress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_progress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_string_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_string_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_word_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_word_count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyTranslationStatusResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_file_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_file_name = { "file_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseData, file_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_file_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_file_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseData, locale), Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_locale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_progress_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_progress = { "progress", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseData, progress), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_progress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_progress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_string_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_string_count = { "string_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseData, string_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_string_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_string_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_word_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_word_count = { "word_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseData, word_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_word_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_word_count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_file_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_locale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_progress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_string_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::NewProp_word_count,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyTranslationStatusResponseData",
		sizeof(FOneSkyTranslationStatusResponseData),
		alignof(FOneSkyTranslationStatusResponseData),
		Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyTranslationStatusResponseData"), sizeof(FOneSkyTranslationStatusResponseData), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseData_Hash() { return 3689838656U; }
class UScriptStruct* FOneSkyTranslationStatusResponseLocale::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyTranslationStatusResponseLocale"), sizeof(FOneSkyTranslationStatusResponseLocale), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyTranslationStatusResponseLocale>()
{
	return FOneSkyTranslationStatusResponseLocale::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyTranslationStatusResponseLocale(FOneSkyTranslationStatusResponseLocale::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyTranslationStatusResponseLocale"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseLocale
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseLocale()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyTranslationStatusResponseLocale>(FName(TEXT("OneSkyTranslationStatusResponseLocale")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseLocale;
	struct Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_english_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_english_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_local_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_local_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_locale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_region;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyTranslationStatusResponseLocale>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseLocale, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_english_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_english_name = { "english_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseLocale, english_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_english_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_english_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_local_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_local_name = { "local_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseLocale, local_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_local_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_local_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseLocale, locale), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_locale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_region_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_region = { "region", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseLocale, region), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_region_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_region_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_english_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_local_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_locale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::NewProp_region,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyTranslationStatusResponseLocale",
		sizeof(FOneSkyTranslationStatusResponseLocale),
		alignof(FOneSkyTranslationStatusResponseLocale),
		Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyTranslationStatusResponseLocale"), sizeof(FOneSkyTranslationStatusResponseLocale), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseLocale_Hash() { return 962452111U; }
class UScriptStruct* FOneSkyTranslationStatusResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyTranslationStatusResponseMeta"), sizeof(FOneSkyTranslationStatusResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyTranslationStatusResponseMeta>()
{
	return FOneSkyTranslationStatusResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyTranslationStatusResponseMeta(FOneSkyTranslationStatusResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyTranslationStatusResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyTranslationStatusResponseMeta>(FName(TEXT("OneSkyTranslationStatusResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyTranslationStatusResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// TRANSLATION STATUS\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "TRANSLATION STATUS" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyTranslationStatusResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyTranslationStatusResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyTranslationStatusResponseMeta",
		sizeof(FOneSkyTranslationStatusResponseMeta),
		alignof(FOneSkyTranslationStatusResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyTranslationStatusResponseMeta"), sizeof(FOneSkyTranslationStatusResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyTranslationStatusResponseMeta_Hash() { return 3162593490U; }
class UScriptStruct* FOneSkyListProjectLanguagesResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectLanguagesResponse"), sizeof(FOneSkyListProjectLanguagesResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectLanguagesResponse>()
{
	return FOneSkyListProjectLanguagesResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectLanguagesResponse(FOneSkyListProjectLanguagesResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectLanguagesResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectLanguagesResponse>(FName(TEXT("OneSkyListProjectLanguagesResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponse;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a List Project Languages query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a List Project Languages query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectLanguagesResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponse, meta), Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_meta_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponse, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectLanguagesResponse",
		sizeof(FOneSkyListProjectLanguagesResponse),
		alignof(FOneSkyListProjectLanguagesResponse),
		Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectLanguagesResponse"), sizeof(FOneSkyListProjectLanguagesResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponse_Hash() { return 4256539292U; }
class UScriptStruct* FOneSkyListProjectLanguagesResponseDataItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectLanguagesResponseDataItem"), sizeof(FOneSkyListProjectLanguagesResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectLanguagesResponseDataItem>()
{
	return FOneSkyListProjectLanguagesResponseDataItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem(FOneSkyListProjectLanguagesResponseDataItem::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectLanguagesResponseDataItem"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponseDataItem
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponseDataItem()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectLanguagesResponseDataItem>(FName(TEXT("OneSkyListProjectLanguagesResponseDataItem")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponseDataItem;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_english_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_english_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_local_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_local_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_locale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_region;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_is_base_language_MetaData[];
#endif
		static void NewProp_is_base_language_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_is_base_language;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_is_ready_to_publish_MetaData[];
#endif
		static void NewProp_is_ready_to_publish_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_is_ready_to_publish;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_translation_progress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_translation_progress;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectLanguagesResponseDataItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseDataItem, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_english_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_english_name = { "english_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseDataItem, english_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_english_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_english_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_local_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_local_name = { "local_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseDataItem, local_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_local_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_local_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseDataItem, locale), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_locale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_region_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_region = { "region", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseDataItem, region), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_region_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_region_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_base_language_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_base_language_SetBit(void* Obj)
	{
		((FOneSkyListProjectLanguagesResponseDataItem*)Obj)->is_base_language = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_base_language = { "is_base_language", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FOneSkyListProjectLanguagesResponseDataItem), &Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_base_language_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_base_language_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_base_language_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_ready_to_publish_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_ready_to_publish_SetBit(void* Obj)
	{
		((FOneSkyListProjectLanguagesResponseDataItem*)Obj)->is_ready_to_publish = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_ready_to_publish = { "is_ready_to_publish", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FOneSkyListProjectLanguagesResponseDataItem), &Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_ready_to_publish_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_ready_to_publish_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_ready_to_publish_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_translation_progress_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_translation_progress = { "translation_progress", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseDataItem, translation_progress), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_translation_progress_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_translation_progress_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_english_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_local_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_locale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_region,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_base_language,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_is_ready_to_publish,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::NewProp_translation_progress,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectLanguagesResponseDataItem",
		sizeof(FOneSkyListProjectLanguagesResponseDataItem),
		alignof(FOneSkyListProjectLanguagesResponseDataItem),
		Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectLanguagesResponseDataItem"), sizeof(FOneSkyListProjectLanguagesResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseDataItem_Hash() { return 1493996481U; }
class UScriptStruct* FOneSkyListProjectLanguagesResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectLanguagesResponseMeta"), sizeof(FOneSkyListProjectLanguagesResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectLanguagesResponseMeta>()
{
	return FOneSkyListProjectLanguagesResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta(FOneSkyListProjectLanguagesResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectLanguagesResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectLanguagesResponseMeta>(FName(TEXT("OneSkyListProjectLanguagesResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectLanguagesResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_record_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_record_count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// LIST PROJECT LANGUAGES\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "LIST PROJECT LANGUAGES" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectLanguagesResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_status_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_record_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_record_count = { "record_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectLanguagesResponseMeta, record_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_record_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_record_count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::NewProp_record_count,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectLanguagesResponseMeta",
		sizeof(FOneSkyListProjectLanguagesResponseMeta),
		alignof(FOneSkyListProjectLanguagesResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectLanguagesResponseMeta"), sizeof(FOneSkyListProjectLanguagesResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectLanguagesResponseMeta_Hash() { return 246751579U; }
class UScriptStruct* FOneSkyCreateProjectResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectResponse"), sizeof(FOneSkyCreateProjectResponse), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectResponse>()
{
	return FOneSkyCreateProjectResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectResponse(FOneSkyCreateProjectResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectResponse>(FName(TEXT("OneSkyCreateProjectResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponse;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a Create Project query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a Create Project query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponse, meta), Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_meta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponse, data), Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectResponse",
		sizeof(FOneSkyCreateProjectResponse),
		alignof(FOneSkyCreateProjectResponse),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectResponse"), sizeof(FOneSkyCreateProjectResponse), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponse_Hash() { return 3229172585U; }
class UScriptStruct* FOneSkyCreateProjectResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectResponseData"), sizeof(FOneSkyCreateProjectResponseData), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectResponseData>()
{
	return FOneSkyCreateProjectResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectResponseData(FOneSkyCreateProjectResponseData::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseData
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseData()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectResponseData>(FName(TEXT("OneSkyCreateProjectResponseData")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseData;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_project_type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_project_type;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponseData, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponseData, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_description_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_description = { "description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponseData, description), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_project_type_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_project_type = { "project_type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponseData, project_type), Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_project_type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_project_type_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::NewProp_project_type,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectResponseData",
		sizeof(FOneSkyCreateProjectResponseData),
		alignof(FOneSkyCreateProjectResponseData),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectResponseData"), sizeof(FOneSkyCreateProjectResponseData), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseData_Hash() { return 3354629625U; }
class UScriptStruct* FOneSkyCreateProjectResponseProjectType::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectResponseProjectType"), sizeof(FOneSkyCreateProjectResponseProjectType), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectResponseProjectType>()
{
	return FOneSkyCreateProjectResponseProjectType::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectResponseProjectType(FOneSkyCreateProjectResponseProjectType::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectResponseProjectType"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseProjectType
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseProjectType()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectResponseProjectType>(FName(TEXT("OneSkyCreateProjectResponseProjectType")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseProjectType;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectResponseProjectType>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponseProjectType, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponseProjectType, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::NewProp_name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectResponseProjectType",
		sizeof(FOneSkyCreateProjectResponseProjectType),
		alignof(FOneSkyCreateProjectResponseProjectType),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectResponseProjectType"), sizeof(FOneSkyCreateProjectResponseProjectType), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseProjectType_Hash() { return 489991180U; }
class UScriptStruct* FOneSkyCreateProjectResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectResponseMeta"), sizeof(FOneSkyCreateProjectResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectResponseMeta>()
{
	return FOneSkyCreateProjectResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectResponseMeta(FOneSkyCreateProjectResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectResponseMeta>(FName(TEXT("OneSkyCreateProjectResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// CREATE PROJECT \n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "CREATE PROJECT" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectResponseMeta",
		sizeof(FOneSkyCreateProjectResponseMeta),
		alignof(FOneSkyCreateProjectResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectResponseMeta"), sizeof(FOneSkyCreateProjectResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectResponseMeta_Hash() { return 3203658674U; }
class UScriptStruct* FOneSkyShowProjectResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowProjectResponse"), sizeof(FOneSkyShowProjectResponse), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowProjectResponse>()
{
	return FOneSkyShowProjectResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowProjectResponse(FOneSkyShowProjectResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowProjectResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowProjectResponse>(FName(TEXT("OneSkyShowProjectResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponse;
	struct Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a Show Project query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a Show Project query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowProjectResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponse, meta), Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_meta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponse, data), Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowProjectResponse",
		sizeof(FOneSkyShowProjectResponse),
		alignof(FOneSkyShowProjectResponse),
		Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowProjectResponse"), sizeof(FOneSkyShowProjectResponse), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponse_Hash() { return 2820730727U; }
class UScriptStruct* FOneSkyShowProjectResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowProjectResponseData"), sizeof(FOneSkyShowProjectResponseData), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowProjectResponseData>()
{
	return FOneSkyShowProjectResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowProjectResponseData(FOneSkyShowProjectResponseData::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowProjectResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseData
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseData()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowProjectResponseData>(FName(TEXT("OneSkyShowProjectResponseData")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseData;
	struct Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_project_type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_project_type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_string_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_string_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_word_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_word_count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowProjectResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseData, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseData, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_description_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_description = { "description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseData, description), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_project_type_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_project_type = { "project_type", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseData, project_type), Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_project_type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_project_type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_string_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_string_count = { "string_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseData, string_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_string_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_string_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_word_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_word_count = { "word_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseData, word_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_word_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_word_count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_project_type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_string_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::NewProp_word_count,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowProjectResponseData",
		sizeof(FOneSkyShowProjectResponseData),
		alignof(FOneSkyShowProjectResponseData),
		Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowProjectResponseData"), sizeof(FOneSkyShowProjectResponseData), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseData_Hash() { return 2557703637U; }
class UScriptStruct* FOneSkyShowProjectResponseProjectType::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowProjectResponseProjectType"), sizeof(FOneSkyShowProjectResponseProjectType), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowProjectResponseProjectType>()
{
	return FOneSkyShowProjectResponseProjectType::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowProjectResponseProjectType(FOneSkyShowProjectResponseProjectType::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowProjectResponseProjectType"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseProjectType
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseProjectType()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowProjectResponseProjectType>(FName(TEXT("OneSkyShowProjectResponseProjectType")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseProjectType;
	struct Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowProjectResponseProjectType>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseProjectType, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseProjectType, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::NewProp_name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowProjectResponseProjectType",
		sizeof(FOneSkyShowProjectResponseProjectType),
		alignof(FOneSkyShowProjectResponseProjectType),
		Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowProjectResponseProjectType"), sizeof(FOneSkyShowProjectResponseProjectType), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseProjectType_Hash() { return 2146630168U; }
class UScriptStruct* FOneSkyShowProjectResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowProjectResponseMeta"), sizeof(FOneSkyShowProjectResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowProjectResponseMeta>()
{
	return FOneSkyShowProjectResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowProjectResponseMeta(FOneSkyShowProjectResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowProjectResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowProjectResponseMeta>(FName(TEXT("OneSkyShowProjectResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// SHOW PROJECT \n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "SHOW PROJECT" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowProjectResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowProjectResponseMeta",
		sizeof(FOneSkyShowProjectResponseMeta),
		alignof(FOneSkyShowProjectResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowProjectResponseMeta"), sizeof(FOneSkyShowProjectResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectResponseMeta_Hash() { return 2922651928U; }
class UScriptStruct* FOneSkyListProjectsInGroupResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectsInGroupResponse"), sizeof(FOneSkyListProjectsInGroupResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectsInGroupResponse>()
{
	return FOneSkyListProjectsInGroupResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectsInGroupResponse(FOneSkyListProjectsInGroupResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectsInGroupResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectsInGroupResponse>(FName(TEXT("OneSkyListProjectsInGroupResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponse;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a List Projects in Group query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a List Projects in Group query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectsInGroupResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectsInGroupResponse, meta), Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_meta_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectsInGroupResponse, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectsInGroupResponse",
		sizeof(FOneSkyListProjectsInGroupResponse),
		alignof(FOneSkyListProjectsInGroupResponse),
		Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectsInGroupResponse"), sizeof(FOneSkyListProjectsInGroupResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponse_Hash() { return 496752318U; }
class UScriptStruct* FOneSkyListProjectsInGroupResponseDataItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectsInGroupResponseDataItem"), sizeof(FOneSkyListProjectsInGroupResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectsInGroupResponseDataItem>()
{
	return FOneSkyListProjectsInGroupResponseDataItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem(FOneSkyListProjectsInGroupResponseDataItem::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectsInGroupResponseDataItem"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponseDataItem
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponseDataItem()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectsInGroupResponseDataItem>(FName(TEXT("OneSkyListProjectsInGroupResponseDataItem")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponseDataItem;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectsInGroupResponseDataItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectsInGroupResponseDataItem, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectsInGroupResponseDataItem, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::NewProp_name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectsInGroupResponseDataItem",
		sizeof(FOneSkyListProjectsInGroupResponseDataItem),
		alignof(FOneSkyListProjectsInGroupResponseDataItem),
		Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectsInGroupResponseDataItem"), sizeof(FOneSkyListProjectsInGroupResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseDataItem_Hash() { return 3507639998U; }
class UScriptStruct* FOneSkyListProjectsInGroupResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectsInGroupResponseMeta"), sizeof(FOneSkyListProjectsInGroupResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectsInGroupResponseMeta>()
{
	return FOneSkyListProjectsInGroupResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta(FOneSkyListProjectsInGroupResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectsInGroupResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectsInGroupResponseMeta>(FName(TEXT("OneSkyListProjectsInGroupResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectsInGroupResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_record_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_record_count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// LIST PROJECTS IN GROUP\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "LIST PROJECTS IN GROUP" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectsInGroupResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectsInGroupResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_status_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_record_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_record_count = { "record_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectsInGroupResponseMeta, record_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_record_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_record_count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::NewProp_record_count,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectsInGroupResponseMeta",
		sizeof(FOneSkyListProjectsInGroupResponseMeta),
		alignof(FOneSkyListProjectsInGroupResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectsInGroupResponseMeta"), sizeof(FOneSkyListProjectsInGroupResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectsInGroupResponseMeta_Hash() { return 3315632739U; }
class UScriptStruct* FOneSkyListProjectGroupLanguagesResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectGroupLanguagesResponse"), sizeof(FOneSkyListProjectGroupLanguagesResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectGroupLanguagesResponse>()
{
	return FOneSkyListProjectGroupLanguagesResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse(FOneSkyListProjectGroupLanguagesResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectGroupLanguagesResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectGroupLanguagesResponse>(FName(TEXT("OneSkyListProjectGroupLanguagesResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponse;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a List Project Group Languages query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a List Project Group Languages query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectGroupLanguagesResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponse, meta), Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_meta_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponse, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectGroupLanguagesResponse",
		sizeof(FOneSkyListProjectGroupLanguagesResponse),
		alignof(FOneSkyListProjectGroupLanguagesResponse),
		Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectGroupLanguagesResponse"), sizeof(FOneSkyListProjectGroupLanguagesResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponse_Hash() { return 674750014U; }
class UScriptStruct* FOneSkyListProjectGroupLanguagesResponseDataItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectGroupLanguagesResponseDataItem"), sizeof(FOneSkyListProjectGroupLanguagesResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectGroupLanguagesResponseDataItem>()
{
	return FOneSkyListProjectGroupLanguagesResponseDataItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem(FOneSkyListProjectGroupLanguagesResponseDataItem::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectGroupLanguagesResponseDataItem"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponseDataItem
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponseDataItem()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectGroupLanguagesResponseDataItem>(FName(TEXT("OneSkyListProjectGroupLanguagesResponseDataItem")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponseDataItem;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_english_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_english_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_local_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_local_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_locale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_region;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_is_base_language_MetaData[];
#endif
		static void NewProp_is_base_language_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_is_base_language;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectGroupLanguagesResponseDataItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponseDataItem, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_english_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_english_name = { "english_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponseDataItem, english_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_english_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_english_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_local_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_local_name = { "local_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponseDataItem, local_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_local_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_local_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponseDataItem, locale), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_locale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_region_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_region = { "region", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponseDataItem, region), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_region_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_region_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_is_base_language_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_is_base_language_SetBit(void* Obj)
	{
		((FOneSkyListProjectGroupLanguagesResponseDataItem*)Obj)->is_base_language = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_is_base_language = { "is_base_language", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FOneSkyListProjectGroupLanguagesResponseDataItem), &Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_is_base_language_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_is_base_language_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_is_base_language_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_english_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_local_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_locale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_region,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::NewProp_is_base_language,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectGroupLanguagesResponseDataItem",
		sizeof(FOneSkyListProjectGroupLanguagesResponseDataItem),
		alignof(FOneSkyListProjectGroupLanguagesResponseDataItem),
		Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectGroupLanguagesResponseDataItem"), sizeof(FOneSkyListProjectGroupLanguagesResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseDataItem_Hash() { return 3303273696U; }
class UScriptStruct* FOneSkyListProjectGroupLanguagesResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectGroupLanguagesResponseMeta"), sizeof(FOneSkyListProjectGroupLanguagesResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectGroupLanguagesResponseMeta>()
{
	return FOneSkyListProjectGroupLanguagesResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta(FOneSkyListProjectGroupLanguagesResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectGroupLanguagesResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectGroupLanguagesResponseMeta>(FName(TEXT("OneSkyListProjectGroupLanguagesResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupLanguagesResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_record_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_record_count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// LIST PROJECT GROUP LANGUAGES\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "LIST PROJECT GROUP LANGUAGES" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectGroupLanguagesResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_status_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_record_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_record_count = { "record_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupLanguagesResponseMeta, record_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_record_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_record_count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::NewProp_record_count,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectGroupLanguagesResponseMeta",
		sizeof(FOneSkyListProjectGroupLanguagesResponseMeta),
		alignof(FOneSkyListProjectGroupLanguagesResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectGroupLanguagesResponseMeta"), sizeof(FOneSkyListProjectGroupLanguagesResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupLanguagesResponseMeta_Hash() { return 192018324U; }
class UScriptStruct* FOneSkyCreateProjectGroupResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectGroupResponse"), sizeof(FOneSkyCreateProjectGroupResponse), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectGroupResponse>()
{
	return FOneSkyCreateProjectGroupResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectGroupResponse(FOneSkyCreateProjectGroupResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectGroupResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectGroupResponse>(FName(TEXT("OneSkyCreateProjectGroupResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponse;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a Create Project Group query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a Create Project Group query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectGroupResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponse, meta), Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_meta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponse, data), Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectGroupResponse",
		sizeof(FOneSkyCreateProjectGroupResponse),
		alignof(FOneSkyCreateProjectGroupResponse),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectGroupResponse"), sizeof(FOneSkyCreateProjectGroupResponse), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponse_Hash() { return 2282079087U; }
class UScriptStruct* FOneSkyCreateProjectGroupResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectGroupResponseData"), sizeof(FOneSkyCreateProjectGroupResponseData), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectGroupResponseData>()
{
	return FOneSkyCreateProjectGroupResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectGroupResponseData(FOneSkyCreateProjectGroupResponseData::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectGroupResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseData
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseData()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectGroupResponseData>(FName(TEXT("OneSkyCreateProjectGroupResponseData")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseData;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_base_language_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_base_language;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectGroupResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseData, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseData, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_base_language_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_base_language = { "base_language", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseData, base_language), Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_base_language_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_base_language_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::NewProp_base_language,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectGroupResponseData",
		sizeof(FOneSkyCreateProjectGroupResponseData),
		alignof(FOneSkyCreateProjectGroupResponseData),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectGroupResponseData"), sizeof(FOneSkyCreateProjectGroupResponseData), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseData_Hash() { return 1490317928U; }
class UScriptStruct* FOneSkyCreateProjectGroupResponseBaseLanguage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectGroupResponseBaseLanguage"), sizeof(FOneSkyCreateProjectGroupResponseBaseLanguage), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectGroupResponseBaseLanguage>()
{
	return FOneSkyCreateProjectGroupResponseBaseLanguage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage(FOneSkyCreateProjectGroupResponseBaseLanguage::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectGroupResponseBaseLanguage"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseBaseLanguage
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseBaseLanguage()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectGroupResponseBaseLanguage>(FName(TEXT("OneSkyCreateProjectGroupResponseBaseLanguage")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseBaseLanguage;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_code_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_code;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_english_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_english_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_local_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_local_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_locale_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_locale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_region_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_region;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectGroupResponseBaseLanguage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_code_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_code = { "code", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseBaseLanguage, code), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_code_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_code_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_english_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_english_name = { "english_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseBaseLanguage, english_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_english_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_english_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_local_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_local_name = { "local_name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseBaseLanguage, local_name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_local_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_local_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_locale_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_locale = { "locale", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseBaseLanguage, locale), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_locale_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_locale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_region_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_region = { "region", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseBaseLanguage, region), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_region_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_region_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_code,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_english_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_local_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_locale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::NewProp_region,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectGroupResponseBaseLanguage",
		sizeof(FOneSkyCreateProjectGroupResponseBaseLanguage),
		alignof(FOneSkyCreateProjectGroupResponseBaseLanguage),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectGroupResponseBaseLanguage"), sizeof(FOneSkyCreateProjectGroupResponseBaseLanguage), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseBaseLanguage_Hash() { return 3296348173U; }
class UScriptStruct* FOneSkyCreateProjectGroupResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyCreateProjectGroupResponseMeta"), sizeof(FOneSkyCreateProjectGroupResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyCreateProjectGroupResponseMeta>()
{
	return FOneSkyCreateProjectGroupResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta(FOneSkyCreateProjectGroupResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyCreateProjectGroupResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyCreateProjectGroupResponseMeta>(FName(TEXT("OneSkyCreateProjectGroupResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyCreateProjectGroupResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// CREATE PROJECT GROUP\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "CREATE PROJECT GROUP" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyCreateProjectGroupResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyCreateProjectGroupResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyCreateProjectGroupResponseMeta",
		sizeof(FOneSkyCreateProjectGroupResponseMeta),
		alignof(FOneSkyCreateProjectGroupResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyCreateProjectGroupResponseMeta"), sizeof(FOneSkyCreateProjectGroupResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyCreateProjectGroupResponseMeta_Hash() { return 2080193899U; }
class UScriptStruct* FOneSkyShowProjectGroupResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowProjectGroupResponse"), sizeof(FOneSkyShowProjectGroupResponse), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowProjectGroupResponse>()
{
	return FOneSkyShowProjectGroupResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowProjectGroupResponse(FOneSkyShowProjectGroupResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowProjectGroupResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowProjectGroupResponse>(FName(TEXT("OneSkyShowProjectGroupResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponse;
	struct Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a Show Project Group query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a Show Project Group query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowProjectGroupResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectGroupResponse, meta), Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_meta_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectGroupResponse, data), Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowProjectGroupResponse",
		sizeof(FOneSkyShowProjectGroupResponse),
		alignof(FOneSkyShowProjectGroupResponse),
		Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowProjectGroupResponse"), sizeof(FOneSkyShowProjectGroupResponse), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponse_Hash() { return 806852367U; }
class UScriptStruct* FOneSkyShowProjectGroupResponseData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowProjectGroupResponseData"), sizeof(FOneSkyShowProjectGroupResponseData), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowProjectGroupResponseData>()
{
	return FOneSkyShowProjectGroupResponseData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowProjectGroupResponseData(FOneSkyShowProjectGroupResponseData::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowProjectGroupResponseData"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponseData
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponseData()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowProjectGroupResponseData>(FName(TEXT("OneSkyShowProjectGroupResponseData")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponseData;
	struct Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_description_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_project_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_project_count;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowProjectGroupResponseData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectGroupResponseData, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectGroupResponseData, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_description_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_description = { "description", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectGroupResponseData, description), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_project_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_project_count = { "project_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectGroupResponseData, project_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_project_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_project_count_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::NewProp_project_count,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowProjectGroupResponseData",
		sizeof(FOneSkyShowProjectGroupResponseData),
		alignof(FOneSkyShowProjectGroupResponseData),
		Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowProjectGroupResponseData"), sizeof(FOneSkyShowProjectGroupResponseData), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseData_Hash() { return 831526004U; }
class UScriptStruct* FOneSkyShowProjectGroupResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyShowProjectGroupResponseMeta"), sizeof(FOneSkyShowProjectGroupResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyShowProjectGroupResponseMeta>()
{
	return FOneSkyShowProjectGroupResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta(FOneSkyShowProjectGroupResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyShowProjectGroupResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyShowProjectGroupResponseMeta>(FName(TEXT("OneSkyShowProjectGroupResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyShowProjectGroupResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// SHOW PROJECT GROUP\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "SHOW PROJECT GROUP" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyShowProjectGroupResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyShowProjectGroupResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::NewProp_status_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::NewProp_status,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyShowProjectGroupResponseMeta",
		sizeof(FOneSkyShowProjectGroupResponseMeta),
		alignof(FOneSkyShowProjectGroupResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyShowProjectGroupResponseMeta"), sizeof(FOneSkyShowProjectGroupResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyShowProjectGroupResponseMeta_Hash() { return 2098586772U; }
class UScriptStruct* FOneSkyListProjectGroupsResponse::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectGroupsResponse"), sizeof(FOneSkyListProjectGroupsResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectGroupsResponse>()
{
	return FOneSkyListProjectGroupsResponse::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectGroupsResponse(FOneSkyListProjectGroupsResponse::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectGroupsResponse"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponse
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponse()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectGroupsResponse>(FName(TEXT("OneSkyListProjectGroupsResponse")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponse;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_meta_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_meta;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_data_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_data_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_data;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n* Response from a List Project Groups query on OneSky\n* //https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md\n*/" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "Response from a List Project Groups query on OneSky\n//https://github.com/onesky/api-documentation-platform/blob/master/resources/project_group.md" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectGroupsResponse>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_meta_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_meta = { "meta", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponse, meta), Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_meta_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_meta_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_data_Inner = { "data", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_data_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_data = { "data", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponse, data), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_data_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_data_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_meta,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_data_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::NewProp_data,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectGroupsResponse",
		sizeof(FOneSkyListProjectGroupsResponse),
		alignof(FOneSkyListProjectGroupsResponse),
		Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectGroupsResponse"), sizeof(FOneSkyListProjectGroupsResponse), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponse_Hash() { return 4050885177U; }
class UScriptStruct* FOneSkyListProjectGroupsResponseDataItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectGroupsResponseDataItem"), sizeof(FOneSkyListProjectGroupsResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectGroupsResponseDataItem>()
{
	return FOneSkyListProjectGroupsResponseDataItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem(FOneSkyListProjectGroupsResponseDataItem::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectGroupsResponseDataItem"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponseDataItem
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponseDataItem()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectGroupsResponseDataItem>(FName(TEXT("OneSkyListProjectGroupsResponseDataItem")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponseDataItem;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_id_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_id;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectGroupsResponseDataItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_id_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_id = { "id", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseDataItem, id), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_id_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_id_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_name_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_name = { "name", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseDataItem, name), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_id,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::NewProp_name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectGroupsResponseDataItem",
		sizeof(FOneSkyListProjectGroupsResponseDataItem),
		alignof(FOneSkyListProjectGroupsResponseDataItem),
		Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectGroupsResponseDataItem"), sizeof(FOneSkyListProjectGroupsResponseDataItem), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseDataItem_Hash() { return 3128070369U; }
class UScriptStruct* FOneSkyListProjectGroupsResponseMeta::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ONESKYLOCALIZATIONSERVICE_API uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta, Z_Construct_UPackage__Script_OneSkyLocalizationService(), TEXT("OneSkyListProjectGroupsResponseMeta"), sizeof(FOneSkyListProjectGroupsResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Hash());
	}
	return Singleton;
}
template<> ONESKYLOCALIZATIONSERVICE_API UScriptStruct* StaticStruct<FOneSkyListProjectGroupsResponseMeta>()
{
	return FOneSkyListProjectGroupsResponseMeta::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta(FOneSkyListProjectGroupsResponseMeta::StaticStruct, TEXT("/Script/OneSkyLocalizationService"), TEXT("OneSkyListProjectGroupsResponseMeta"), false, nullptr, nullptr);
static struct FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponseMeta
{
	FScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponseMeta()
	{
		UScriptStruct::DeferCppStructOps<FOneSkyListProjectGroupsResponseMeta>(FName(TEXT("OneSkyListProjectGroupsResponseMeta")));
	}
} ScriptStruct_OneSkyLocalizationService_StaticRegisterNativesFOneSkyListProjectGroupsResponseMeta;
	struct Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_status_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_status;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_record_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_record_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_page_count_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_page_count;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_next_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_next_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_prev_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_prev_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_first_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_first_page;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_last_page_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_last_page;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// LIST PROJECT GROUPS\n" },
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
		{ "ToolTip", "LIST PROJECT GROUPS" },
	};
#endif
	void* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FOneSkyListProjectGroupsResponseMeta>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_status_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_status = { "status", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseMeta, status), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_status_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_status_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_record_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_record_count = { "record_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseMeta, record_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_record_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_record_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_page_count_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_page_count = { "page_count", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseMeta, page_count), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_page_count_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_page_count_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_next_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_next_page = { "next_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseMeta, next_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_next_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_next_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_prev_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_prev_page = { "prev_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseMeta, prev_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_prev_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_prev_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_first_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_first_page = { "first_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseMeta, first_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_first_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_first_page_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_last_page_MetaData[] = {
		{ "ModuleRelativePath", "Private/OneSkyLocalizationServiceResponseTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_last_page = { "last_page", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FOneSkyListProjectGroupsResponseMeta, last_page), METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_last_page_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_last_page_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_status,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_record_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_page_count,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_next_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_prev_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_first_page,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::NewProp_last_page,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_OneSkyLocalizationService,
		nullptr,
		&NewStructOps,
		"OneSkyListProjectGroupsResponseMeta",
		sizeof(FOneSkyListProjectGroupsResponseMeta),
		alignof(FOneSkyListProjectGroupsResponseMeta),
		Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_OneSkyLocalizationService();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("OneSkyListProjectGroupsResponseMeta"), sizeof(FOneSkyListProjectGroupsResponseMeta), Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FOneSkyListProjectGroupsResponseMeta_Hash() { return 97431127U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
