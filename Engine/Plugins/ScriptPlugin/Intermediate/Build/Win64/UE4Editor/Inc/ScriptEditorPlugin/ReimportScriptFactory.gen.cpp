// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ScriptEditorPlugin/Classes/ReimportScriptFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReimportScriptFactory() {}
// Cross Module References
	SCRIPTEDITORPLUGIN_API UClass* Z_Construct_UClass_UReimportScriptFactory_NoRegister();
	SCRIPTEDITORPLUGIN_API UClass* Z_Construct_UClass_UReimportScriptFactory();
	SCRIPTEDITORPLUGIN_API UClass* Z_Construct_UClass_UScriptFactory();
	UPackage* Z_Construct_UPackage__Script_ScriptEditorPlugin();
	SCRIPTPLUGIN_API UClass* Z_Construct_UClass_UScriptBlueprint_NoRegister();
// End Cross Module References
	void UReimportScriptFactory::StaticRegisterNativesUReimportScriptFactory()
	{
	}
	UClass* Z_Construct_UClass_UReimportScriptFactory_NoRegister()
	{
		return UReimportScriptFactory::StaticClass();
	}
	struct Z_Construct_UClass_UReimportScriptFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OriginalScript_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OriginalScript;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReimportScriptFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UScriptFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ScriptEditorPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReimportScriptFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Script Blueprint re-import factory\n*/" },
		{ "DevelopmentStatus", "EarlyAccess" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "ReimportScriptFactory.h" },
		{ "ModuleRelativePath", "Classes/ReimportScriptFactory.h" },
		{ "ToolTip", "Script Blueprint re-import factory" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReimportScriptFactory_Statics::NewProp_OriginalScript_MetaData[] = {
		{ "ModuleRelativePath", "Classes/ReimportScriptFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UReimportScriptFactory_Statics::NewProp_OriginalScript = { "OriginalScript", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UReimportScriptFactory, OriginalScript), Z_Construct_UClass_UScriptBlueprint_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UReimportScriptFactory_Statics::NewProp_OriginalScript_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UReimportScriptFactory_Statics::NewProp_OriginalScript_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UReimportScriptFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UReimportScriptFactory_Statics::NewProp_OriginalScript,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReimportScriptFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReimportScriptFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReimportScriptFactory_Statics::ClassParams = {
		&UReimportScriptFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UReimportScriptFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UReimportScriptFactory_Statics::PropPointers),
		0,
		0x000820A0u,
		METADATA_PARAMS(Z_Construct_UClass_UReimportScriptFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReimportScriptFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReimportScriptFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReimportScriptFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReimportScriptFactory, 336063242);
	template<> SCRIPTEDITORPLUGIN_API UClass* StaticClass<UReimportScriptFactory>()
	{
		return UReimportScriptFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReimportScriptFactory(Z_Construct_UClass_UReimportScriptFactory, &UReimportScriptFactory::StaticClass, TEXT("/Script/ScriptEditorPlugin"), TEXT("UReimportScriptFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReimportScriptFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
