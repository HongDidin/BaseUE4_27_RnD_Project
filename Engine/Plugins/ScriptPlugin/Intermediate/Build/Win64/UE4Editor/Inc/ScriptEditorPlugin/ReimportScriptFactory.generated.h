// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SCRIPTEDITORPLUGIN_ReimportScriptFactory_generated_h
#error "ReimportScriptFactory.generated.h already included, missing '#pragma once' in ReimportScriptFactory.h"
#endif
#define SCRIPTEDITORPLUGIN_ReimportScriptFactory_generated_h

#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_SPARSE_DATA
#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_RPC_WRAPPERS
#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReimportScriptFactory(); \
	friend struct Z_Construct_UClass_UReimportScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UReimportScriptFactory, UScriptFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ScriptEditorPlugin"), SCRIPTEDITORPLUGIN_API) \
	DECLARE_SERIALIZER(UReimportScriptFactory)


#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUReimportScriptFactory(); \
	friend struct Z_Construct_UClass_UReimportScriptFactory_Statics; \
public: \
	DECLARE_CLASS(UReimportScriptFactory, UScriptFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ScriptEditorPlugin"), SCRIPTEDITORPLUGIN_API) \
	DECLARE_SERIALIZER(UReimportScriptFactory)


#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SCRIPTEDITORPLUGIN_API UReimportScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReimportScriptFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SCRIPTEDITORPLUGIN_API, UReimportScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReimportScriptFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SCRIPTEDITORPLUGIN_API UReimportScriptFactory(UReimportScriptFactory&&); \
	SCRIPTEDITORPLUGIN_API UReimportScriptFactory(const UReimportScriptFactory&); \
public:


#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SCRIPTEDITORPLUGIN_API UReimportScriptFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SCRIPTEDITORPLUGIN_API UReimportScriptFactory(UReimportScriptFactory&&); \
	SCRIPTEDITORPLUGIN_API UReimportScriptFactory(const UReimportScriptFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SCRIPTEDITORPLUGIN_API, UReimportScriptFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReimportScriptFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReimportScriptFactory)


#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_17_PROLOG
#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_SPARSE_DATA \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_RPC_WRAPPERS \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_INCLASS \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_SPARSE_DATA \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ReimportScriptFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SCRIPTEDITORPLUGIN_API UClass* StaticClass<class UReimportScriptFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_ScriptPlugin_Source_ScriptEditorPlugin_Classes_ReimportScriptFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
