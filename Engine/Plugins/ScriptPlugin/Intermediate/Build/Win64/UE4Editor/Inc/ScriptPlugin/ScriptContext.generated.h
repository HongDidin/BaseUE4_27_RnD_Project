// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SCRIPTPLUGIN_ScriptContext_generated_h
#error "ScriptContext.generated.h already included, missing '#pragma once' in ScriptContext.h"
#endif
#define SCRIPTPLUGIN_ScriptContext_generated_h

#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_SPARSE_DATA
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCallScriptFunction);


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCallScriptFunction);


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUScriptContext(); \
	friend struct Z_Construct_UClass_UScriptContext_Statics; \
public: \
	DECLARE_CLASS(UScriptContext, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UScriptContext)


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUScriptContext(); \
	friend struct Z_Construct_UClass_UScriptContext_Statics; \
public: \
	DECLARE_CLASS(UScriptContext, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UScriptContext)


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UScriptContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UScriptContext) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UScriptContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UScriptContext); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UScriptContext(UScriptContext&&); \
	NO_API UScriptContext(const UScriptContext&); \
public:


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UScriptContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UScriptContext(UScriptContext&&); \
	NO_API UScriptContext(const UScriptContext&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UScriptContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UScriptContext); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UScriptContext)


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_14_PROLOG
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_SPARSE_DATA \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_RPC_WRAPPERS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_INCLASS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_SPARSE_DATA \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ScriptContext."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SCRIPTPLUGIN_API UClass* StaticClass<class UScriptContext>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptContext_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
