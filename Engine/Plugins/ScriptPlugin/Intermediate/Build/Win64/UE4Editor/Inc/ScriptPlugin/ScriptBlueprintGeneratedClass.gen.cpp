// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ScriptPlugin/Classes/ScriptBlueprintGeneratedClass.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeScriptBlueprintGeneratedClass() {}
// Cross Module References
	SCRIPTPLUGIN_API UClass* Z_Construct_UClass_UScriptBlueprintGeneratedClass_NoRegister();
	SCRIPTPLUGIN_API UClass* Z_Construct_UClass_UScriptBlueprintGeneratedClass();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintGeneratedClass();
	UPackage* Z_Construct_UPackage__Script_ScriptPlugin();
	COREUOBJECT_API UClass* Z_Construct_UClass_UProperty();
// End Cross Module References
	void UScriptBlueprintGeneratedClass::StaticRegisterNativesUScriptBlueprintGeneratedClass()
	{
	}
	UClass* Z_Construct_UClass_UScriptBlueprintGeneratedClass_NoRegister()
	{
		return UScriptBlueprintGeneratedClass::StaticClass();
	}
	struct Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ByteCode_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ByteCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ByteCode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceCode;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScriptProperties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ScriptProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintGeneratedClass,
		(UObject* (*)())Z_Construct_UPackage__Script_ScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Script generated class\n*/" },
		{ "DevelopmentStatus", "EarlyAccess" },
		{ "IncludePath", "ScriptBlueprintGeneratedClass.h" },
		{ "ModuleRelativePath", "Classes/ScriptBlueprintGeneratedClass.h" },
		{ "ToolTip", "Script generated class" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ByteCode_Inner = { "ByteCode", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ByteCode_MetaData[] = {
		{ "Comment", "/** Generated script bytecode */" },
		{ "ModuleRelativePath", "Classes/ScriptBlueprintGeneratedClass.h" },
		{ "ToolTip", "Generated script bytecode" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ByteCode = { "ByteCode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UScriptBlueprintGeneratedClass, ByteCode), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ByteCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ByteCode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_SourceCode_MetaData[] = {
		{ "Comment", "/** Script source code. @todo: this should be editor-only */" },
		{ "ModuleRelativePath", "Classes/ScriptBlueprintGeneratedClass.h" },
		{ "ToolTip", "Script source code. @todo: this should be editor-only" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_SourceCode = { "SourceCode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UScriptBlueprintGeneratedClass, SourceCode), METADATA_PARAMS(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_SourceCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_SourceCode_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ScriptProperties_Inner = { "ScriptProperties", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UProperty, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ScriptProperties_MetaData[] = {
		{ "Comment", "/** Script-generated properties */" },
		{ "ModuleRelativePath", "Classes/ScriptBlueprintGeneratedClass.h" },
		{ "ToolTip", "Script-generated properties" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ScriptProperties = { "ScriptProperties", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UScriptBlueprintGeneratedClass, ScriptProperties_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ScriptProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ScriptProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ByteCode_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ByteCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_SourceCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ScriptProperties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::NewProp_ScriptProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UScriptBlueprintGeneratedClass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::ClassParams = {
		&UScriptBlueprintGeneratedClass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UScriptBlueprintGeneratedClass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UScriptBlueprintGeneratedClass, 2795448510);
	template<> SCRIPTPLUGIN_API UClass* StaticClass<UScriptBlueprintGeneratedClass>()
	{
		return UScriptBlueprintGeneratedClass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UScriptBlueprintGeneratedClass(Z_Construct_UClass_UScriptBlueprintGeneratedClass, &UScriptBlueprintGeneratedClass::StaticClass, TEXT("/Script/ScriptPlugin"), TEXT("UScriptBlueprintGeneratedClass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UScriptBlueprintGeneratedClass);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UScriptBlueprintGeneratedClass)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
