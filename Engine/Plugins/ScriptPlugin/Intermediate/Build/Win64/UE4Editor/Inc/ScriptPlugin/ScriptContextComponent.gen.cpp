// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ScriptPlugin/Classes/ScriptContextComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeScriptContextComponent() {}
// Cross Module References
	SCRIPTPLUGIN_API UClass* Z_Construct_UClass_UScriptContextComponent_NoRegister();
	SCRIPTPLUGIN_API UClass* Z_Construct_UClass_UScriptContextComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	UPackage* Z_Construct_UPackage__Script_ScriptPlugin();
// End Cross Module References
	DEFINE_FUNCTION(UScriptContextComponent::execCallScriptFunction)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FunctionName);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CallScriptFunction(Z_Param_FunctionName);
		P_NATIVE_END;
	}
	void UScriptContextComponent::StaticRegisterNativesUScriptContextComponent()
	{
		UClass* Class = UScriptContextComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CallScriptFunction", &UScriptContextComponent::execCallScriptFunction },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics
	{
		struct ScriptContextComponent_eventCallScriptFunction_Parms
		{
			FString FunctionName;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FunctionName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::NewProp_FunctionName = { "FunctionName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ScriptContextComponent_eventCallScriptFunction_Parms, FunctionName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::NewProp_FunctionName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::Function_MetaDataParams[] = {
		{ "Category", "ScriptContext" },
		{ "Comment", "/**\n\x09* Calls a script-defined function (no arguments)\n\x09* @param FunctionName Name of the function to call\n\x09*/" },
		{ "ModuleRelativePath", "Classes/ScriptContextComponent.h" },
		{ "ToolTip", "Calls a script-defined function (no arguments)\n@param FunctionName Name of the function to call" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UScriptContextComponent, nullptr, "CallScriptFunction", nullptr, nullptr, sizeof(ScriptContextComponent_eventCallScriptFunction_Parms), Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UScriptContextComponent_NoRegister()
	{
		return UScriptContextComponent::StaticClass();
	}
	struct Z_Construct_UClass_UScriptContextComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UScriptContextComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ScriptPlugin,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UScriptContextComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UScriptContextComponent_CallScriptFunction, "CallScriptFunction" }, // 1415397764
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UScriptContextComponent_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * Script-extendable component class\n */" },
		{ "DevelopmentStatus", "EarlyAccess" },
		{ "IncludePath", "ScriptContextComponent.h" },
		{ "ModuleRelativePath", "Classes/ScriptContextComponent.h" },
		{ "ToolTip", "Script-extendable component class" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UScriptContextComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UScriptContextComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UScriptContextComponent_Statics::ClassParams = {
		&UScriptContextComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UScriptContextComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UScriptContextComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UScriptContextComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UScriptContextComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UScriptContextComponent, 663169845);
	template<> SCRIPTPLUGIN_API UClass* StaticClass<UScriptContextComponent>()
	{
		return UScriptContextComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UScriptContextComponent(Z_Construct_UClass_UScriptContextComponent, &UScriptContextComponent::StaticClass, TEXT("/Script/ScriptPlugin"), TEXT("UScriptContextComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UScriptContextComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
