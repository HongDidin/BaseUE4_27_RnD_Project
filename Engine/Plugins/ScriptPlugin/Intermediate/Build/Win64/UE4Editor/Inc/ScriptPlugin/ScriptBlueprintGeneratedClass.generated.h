// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SCRIPTPLUGIN_ScriptBlueprintGeneratedClass_generated_h
#error "ScriptBlueprintGeneratedClass.generated.h already included, missing '#pragma once' in ScriptBlueprintGeneratedClass.h"
#endif
#define SCRIPTPLUGIN_ScriptBlueprintGeneratedClass_generated_h

#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_SPARSE_DATA
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_RPC_WRAPPERS
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UScriptBlueprintGeneratedClass, NO_API)


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUScriptBlueprintGeneratedClass(); \
	friend struct Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics; \
public: \
	DECLARE_CLASS(UScriptBlueprintGeneratedClass, UBlueprintGeneratedClass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UScriptBlueprintGeneratedClass) \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_ARCHIVESERIALIZER


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_INCLASS \
private: \
	static void StaticRegisterNativesUScriptBlueprintGeneratedClass(); \
	friend struct Z_Construct_UClass_UScriptBlueprintGeneratedClass_Statics; \
public: \
	DECLARE_CLASS(UScriptBlueprintGeneratedClass, UBlueprintGeneratedClass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UScriptBlueprintGeneratedClass) \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_ARCHIVESERIALIZER


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UScriptBlueprintGeneratedClass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UScriptBlueprintGeneratedClass) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UScriptBlueprintGeneratedClass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UScriptBlueprintGeneratedClass); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UScriptBlueprintGeneratedClass(UScriptBlueprintGeneratedClass&&); \
	NO_API UScriptBlueprintGeneratedClass(const UScriptBlueprintGeneratedClass&); \
public:


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UScriptBlueprintGeneratedClass(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UScriptBlueprintGeneratedClass(UScriptBlueprintGeneratedClass&&); \
	NO_API UScriptBlueprintGeneratedClass(const UScriptBlueprintGeneratedClass&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UScriptBlueprintGeneratedClass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UScriptBlueprintGeneratedClass); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UScriptBlueprintGeneratedClass)


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_137_PROLOG
#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_SPARSE_DATA \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_RPC_WRAPPERS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_INCLASS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_SPARSE_DATA \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h_140_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ScriptBlueprintGeneratedClass."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SCRIPTPLUGIN_API UClass* StaticClass<class UScriptBlueprintGeneratedClass>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_ScriptPlugin_Source_ScriptPlugin_Classes_ScriptBlueprintGeneratedClass_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
