// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAUNTLET_GauntletTestControllerBootTest_generated_h
#error "GauntletTestControllerBootTest.generated.h already included, missing '#pragma once' in GauntletTestControllerBootTest.h"
#endif
#define GAUNTLET_GauntletTestControllerBootTest_generated_h

#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_SPARSE_DATA
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGauntletTestControllerBootTest(); \
	friend struct Z_Construct_UClass_UGauntletTestControllerBootTest_Statics; \
public: \
	DECLARE_CLASS(UGauntletTestControllerBootTest, UGauntletTestController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Gauntlet"), NO_API) \
	DECLARE_SERIALIZER(UGauntletTestControllerBootTest)


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUGauntletTestControllerBootTest(); \
	friend struct Z_Construct_UClass_UGauntletTestControllerBootTest_Statics; \
public: \
	DECLARE_CLASS(UGauntletTestControllerBootTest, UGauntletTestController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Gauntlet"), NO_API) \
	DECLARE_SERIALIZER(UGauntletTestControllerBootTest)


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGauntletTestControllerBootTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGauntletTestControllerBootTest) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGauntletTestControllerBootTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGauntletTestControllerBootTest); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGauntletTestControllerBootTest(UGauntletTestControllerBootTest&&); \
	NO_API UGauntletTestControllerBootTest(const UGauntletTestControllerBootTest&); \
public:


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGauntletTestControllerBootTest(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGauntletTestControllerBootTest(UGauntletTestControllerBootTest&&); \
	NO_API UGauntletTestControllerBootTest(const UGauntletTestControllerBootTest&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGauntletTestControllerBootTest); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGauntletTestControllerBootTest); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGauntletTestControllerBootTest)


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_8_PROLOG
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_INCLASS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAUNTLET_API UClass* StaticClass<class UGauntletTestControllerBootTest>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestControllerBootTest_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
