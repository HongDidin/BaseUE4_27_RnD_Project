// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAUNTLET_GauntletTestController_generated_h
#error "GauntletTestController.generated.h already included, missing '#pragma once' in GauntletTestController.h"
#endif
#define GAUNTLET_GauntletTestController_generated_h

#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGauntletTestController(); \
	friend struct Z_Construct_UClass_UGauntletTestController_Statics; \
public: \
	DECLARE_CLASS(UGauntletTestController, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Gauntlet"), NO_API) \
	DECLARE_SERIALIZER(UGauntletTestController)


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUGauntletTestController(); \
	friend struct Z_Construct_UClass_UGauntletTestController_Statics; \
public: \
	DECLARE_CLASS(UGauntletTestController, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Gauntlet"), NO_API) \
	DECLARE_SERIALIZER(UGauntletTestController)


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGauntletTestController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGauntletTestController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGauntletTestController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGauntletTestController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGauntletTestController(UGauntletTestController&&); \
	NO_API UGauntletTestController(const UGauntletTestController&); \
public:


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGauntletTestController(UGauntletTestController&&); \
	NO_API UGauntletTestController(const UGauntletTestController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGauntletTestController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGauntletTestController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGauntletTestController)


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_19_PROLOG
#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_INCLASS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAUNTLET_API UClass* StaticClass<class UGauntletTestController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Gauntlet_Source_Gauntlet_Public_GauntletTestController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
