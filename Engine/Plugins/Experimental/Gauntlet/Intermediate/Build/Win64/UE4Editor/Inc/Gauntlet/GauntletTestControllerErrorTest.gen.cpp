// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Gauntlet/Public/GauntletTestControllerErrorTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGauntletTestControllerErrorTest() {}
// Cross Module References
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestControllerErrorTest_NoRegister();
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestControllerErrorTest();
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestController();
	UPackage* Z_Construct_UPackage__Script_Gauntlet();
// End Cross Module References
	void UGauntletTestControllerErrorTest::StaticRegisterNativesUGauntletTestControllerErrorTest()
	{
	}
	UClass* Z_Construct_UClass_UGauntletTestControllerErrorTest_NoRegister()
	{
		return UGauntletTestControllerErrorTest::StaticClass();
	}
	struct Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGauntletTestController,
		(UObject* (*)())Z_Construct_UPackage__Script_Gauntlet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GauntletTestControllerErrorTest.h" },
		{ "ModuleRelativePath", "Public/GauntletTestControllerErrorTest.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGauntletTestControllerErrorTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics::ClassParams = {
		&UGauntletTestControllerErrorTest::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGauntletTestControllerErrorTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGauntletTestControllerErrorTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGauntletTestControllerErrorTest, 96393314);
	template<> GAUNTLET_API UClass* StaticClass<UGauntletTestControllerErrorTest>()
	{
		return UGauntletTestControllerErrorTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGauntletTestControllerErrorTest(Z_Construct_UClass_UGauntletTestControllerErrorTest, &UGauntletTestControllerErrorTest::StaticClass, TEXT("/Script/Gauntlet"), TEXT("UGauntletTestControllerErrorTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGauntletTestControllerErrorTest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
