// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Gauntlet/Public/GauntletTestControllerBootTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGauntletTestControllerBootTest() {}
// Cross Module References
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestControllerBootTest_NoRegister();
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestControllerBootTest();
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestController();
	UPackage* Z_Construct_UPackage__Script_Gauntlet();
// End Cross Module References
	void UGauntletTestControllerBootTest::StaticRegisterNativesUGauntletTestControllerBootTest()
	{
	}
	UClass* Z_Construct_UClass_UGauntletTestControllerBootTest_NoRegister()
	{
		return UGauntletTestControllerBootTest::StaticClass();
	}
	struct Z_Construct_UClass_UGauntletTestControllerBootTest_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGauntletTestControllerBootTest_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGauntletTestController,
		(UObject* (*)())Z_Construct_UPackage__Script_Gauntlet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGauntletTestControllerBootTest_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GauntletTestControllerBootTest.h" },
		{ "ModuleRelativePath", "Public/GauntletTestControllerBootTest.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGauntletTestControllerBootTest_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGauntletTestControllerBootTest>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGauntletTestControllerBootTest_Statics::ClassParams = {
		&UGauntletTestControllerBootTest::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGauntletTestControllerBootTest_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGauntletTestControllerBootTest_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGauntletTestControllerBootTest()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGauntletTestControllerBootTest_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGauntletTestControllerBootTest, 2257747822);
	template<> GAUNTLET_API UClass* StaticClass<UGauntletTestControllerBootTest>()
	{
		return UGauntletTestControllerBootTest::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGauntletTestControllerBootTest(Z_Construct_UClass_UGauntletTestControllerBootTest, &UGauntletTestControllerBootTest::StaticClass, TEXT("/Script/Gauntlet"), TEXT("UGauntletTestControllerBootTest"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGauntletTestControllerBootTest);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
