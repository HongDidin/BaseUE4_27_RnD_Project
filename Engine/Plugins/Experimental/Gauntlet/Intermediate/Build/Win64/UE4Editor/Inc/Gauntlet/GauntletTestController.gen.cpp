// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Gauntlet/Public/GauntletTestController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGauntletTestController() {}
// Cross Module References
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestController_NoRegister();
	GAUNTLET_API UClass* Z_Construct_UClass_UGauntletTestController();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Gauntlet();
// End Cross Module References
	void UGauntletTestController::StaticRegisterNativesUGauntletTestController()
	{
	}
	UClass* Z_Construct_UClass_UGauntletTestController_NoRegister()
	{
		return UGauntletTestController::StaticClass();
	}
	struct Z_Construct_UClass_UGauntletTestController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGauntletTestController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Gauntlet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGauntletTestController_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\x09""Base class for games to implement test controllers that use the Gauntlet native\n *\x09""framework. This is a very thin class that is created automatically based on \n *\x09""command line params (-gauntlet=MyControllerName) and provides easily overridden\n *\x09""functions that represent state changes and ticking\n *\n *\x09In essence your derived class should implement logic that starts and monitors\n *\x09""a test, then calls EndTest(Result) when the desired criteria are met (or not!)\n */" },
		{ "IncludePath", "GauntletTestController.h" },
		{ "ModuleRelativePath", "Public/GauntletTestController.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Base class for games to implement test controllers that use the Gauntlet native\nframework. This is a very thin class that is created automatically based on\ncommand line params (-gauntlet=MyControllerName) and provides easily overridden\nfunctions that represent state changes and ticking\n\nIn essence your derived class should implement logic that starts and monitors\na test, then calls EndTest(Result) when the desired criteria are met (or not!)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGauntletTestController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGauntletTestController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGauntletTestController_Statics::ClassParams = {
		&UGauntletTestController::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGauntletTestController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGauntletTestController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGauntletTestController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGauntletTestController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGauntletTestController, 2686358766);
	template<> GAUNTLET_API UClass* StaticClass<UGauntletTestController>()
	{
		return UGauntletTestController::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGauntletTestController(Z_Construct_UClass_UGauntletTestController, &UGauntletTestController::StaticClass, TEXT("/Script/Gauntlet"), TEXT("UGauntletTestController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGauntletTestController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
