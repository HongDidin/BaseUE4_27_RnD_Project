// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheEd/Classes/GeometryCacheThumbnailRenderer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCacheThumbnailRenderer() {}
// Cross Module References
	GEOMETRYCACHEED_API UClass* Z_Construct_UClass_UGeometryCacheThumbnailRenderer_NoRegister();
	GEOMETRYCACHEED_API UClass* Z_Construct_UClass_UGeometryCacheThumbnailRenderer();
	UNREALED_API UClass* Z_Construct_UClass_UDefaultSizedThumbnailRenderer();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheEd();
// End Cross Module References
	void UGeometryCacheThumbnailRenderer::StaticRegisterNativesUGeometryCacheThumbnailRenderer()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCacheThumbnailRenderer_NoRegister()
	{
		return UGeometryCacheThumbnailRenderer::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDefaultSizedThumbnailRenderer,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheEd,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GeometryCacheThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Classes/GeometryCacheThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCacheThumbnailRenderer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics::ClassParams = {
		&UGeometryCacheThumbnailRenderer::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCacheThumbnailRenderer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCacheThumbnailRenderer, 4059773687);
	template<> GEOMETRYCACHEED_API UClass* StaticClass<UGeometryCacheThumbnailRenderer>()
	{
		return UGeometryCacheThumbnailRenderer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCacheThumbnailRenderer(Z_Construct_UClass_UGeometryCacheThumbnailRenderer, &UGeometryCacheThumbnailRenderer::StaticClass, TEXT("/Script/GeometryCacheEd"), TEXT("UGeometryCacheThumbnailRenderer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCacheThumbnailRenderer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
