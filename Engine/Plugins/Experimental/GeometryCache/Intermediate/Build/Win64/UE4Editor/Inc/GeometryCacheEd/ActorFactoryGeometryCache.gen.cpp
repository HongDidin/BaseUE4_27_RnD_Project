// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheEd/Classes/ActorFactoryGeometryCache.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryGeometryCache() {}
// Cross Module References
	GEOMETRYCACHEED_API UClass* Z_Construct_UClass_UActorFactoryGeometryCache_NoRegister();
	GEOMETRYCACHEED_API UClass* Z_Construct_UClass_UActorFactoryGeometryCache();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheEd();
// End Cross Module References
	void UActorFactoryGeometryCache::StaticRegisterNativesUActorFactoryGeometryCache()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryGeometryCache_NoRegister()
	{
		return UActorFactoryGeometryCache::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryGeometryCache_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryGeometryCache_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheEd,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryGeometryCache_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Factory class for spawning and creating GeometryCacheActors */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "ActorFactoryGeometryCache.h" },
		{ "ModuleRelativePath", "Classes/ActorFactoryGeometryCache.h" },
		{ "ToolTip", "Factory class for spawning and creating GeometryCacheActors" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryGeometryCache_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryGeometryCache>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryGeometryCache_Statics::ClassParams = {
		&UActorFactoryGeometryCache::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryGeometryCache_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryGeometryCache_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryGeometryCache()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryGeometryCache_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryGeometryCache, 4146182566);
	template<> GEOMETRYCACHEED_API UClass* StaticClass<UActorFactoryGeometryCache>()
	{
		return UActorFactoryGeometryCache::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryGeometryCache(Z_Construct_UClass_UActorFactoryGeometryCache, &UActorFactoryGeometryCache::StaticClass, TEXT("/Script/GeometryCacheEd"), TEXT("UActorFactoryGeometryCache"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryGeometryCache);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
