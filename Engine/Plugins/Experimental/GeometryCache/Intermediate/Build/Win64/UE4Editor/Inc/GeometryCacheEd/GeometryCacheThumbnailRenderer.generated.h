// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYCACHEED_GeometryCacheThumbnailRenderer_generated_h
#error "GeometryCacheThumbnailRenderer.generated.h already included, missing '#pragma once' in GeometryCacheThumbnailRenderer.h"
#endif
#define GEOMETRYCACHEED_GeometryCacheThumbnailRenderer_generated_h

#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGeometryCacheThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheThumbnailRenderer, UDefaultSizedThumbnailRenderer, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheEd"), GEOMETRYCACHEED_API) \
	DECLARE_SERIALIZER(UGeometryCacheThumbnailRenderer)


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUGeometryCacheThumbnailRenderer(); \
	friend struct Z_Construct_UClass_UGeometryCacheThumbnailRenderer_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheThumbnailRenderer, UDefaultSizedThumbnailRenderer, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheEd"), GEOMETRYCACHEED_API) \
	DECLARE_SERIALIZER(UGeometryCacheThumbnailRenderer)


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GEOMETRYCACHEED_API UGeometryCacheThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGeometryCacheThumbnailRenderer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GEOMETRYCACHEED_API, UGeometryCacheThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheThumbnailRenderer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GEOMETRYCACHEED_API UGeometryCacheThumbnailRenderer(UGeometryCacheThumbnailRenderer&&); \
	GEOMETRYCACHEED_API UGeometryCacheThumbnailRenderer(const UGeometryCacheThumbnailRenderer&); \
public:


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GEOMETRYCACHEED_API UGeometryCacheThumbnailRenderer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GEOMETRYCACHEED_API UGeometryCacheThumbnailRenderer(UGeometryCacheThumbnailRenderer&&); \
	GEOMETRYCACHEED_API UGeometryCacheThumbnailRenderer(const UGeometryCacheThumbnailRenderer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GEOMETRYCACHEED_API, UGeometryCacheThumbnailRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheThumbnailRenderer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGeometryCacheThumbnailRenderer)


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_19_PROLOG
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_INCLASS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GeometryCacheThumbnailRenderer."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYCACHEED_API UClass* StaticClass<class UGeometryCacheThumbnailRenderer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_GeometryCacheThumbnailRenderer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
