// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYCACHEED_ActorFactoryGeometryCache_generated_h
#error "ActorFactoryGeometryCache.generated.h already included, missing '#pragma once' in ActorFactoryGeometryCache.h"
#endif
#define GEOMETRYCACHEED_ActorFactoryGeometryCache_generated_h

#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryGeometryCache(); \
	friend struct Z_Construct_UClass_UActorFactoryGeometryCache_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryGeometryCache, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheEd"), GEOMETRYCACHEED_API) \
	DECLARE_SERIALIZER(UActorFactoryGeometryCache)


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryGeometryCache(); \
	friend struct Z_Construct_UClass_UActorFactoryGeometryCache_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryGeometryCache, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheEd"), GEOMETRYCACHEED_API) \
	DECLARE_SERIALIZER(UActorFactoryGeometryCache)


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GEOMETRYCACHEED_API UActorFactoryGeometryCache(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryGeometryCache) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GEOMETRYCACHEED_API, UActorFactoryGeometryCache); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryGeometryCache); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GEOMETRYCACHEED_API UActorFactoryGeometryCache(UActorFactoryGeometryCache&&); \
	GEOMETRYCACHEED_API UActorFactoryGeometryCache(const UActorFactoryGeometryCache&); \
public:


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	GEOMETRYCACHEED_API UActorFactoryGeometryCache(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	GEOMETRYCACHEED_API UActorFactoryGeometryCache(UActorFactoryGeometryCache&&); \
	GEOMETRYCACHEED_API UActorFactoryGeometryCache(const UActorFactoryGeometryCache&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(GEOMETRYCACHEED_API, UActorFactoryGeometryCache); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryGeometryCache); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryGeometryCache)


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_14_PROLOG
#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_INCLASS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ActorFactoryGeometryCache."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYCACHEED_API UClass* StaticClass<class UActorFactoryGeometryCache>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GeometryCache_Source_GeometryCacheEd_Classes_ActorFactoryGeometryCache_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
