// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LedWallCalibration/Public/LedWallArucoGenerationOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLedWallArucoGenerationOptions() {}
// Cross Module References
	LEDWALLCALIBRATION_API UEnum* Z_Construct_UEnum_LedWallCalibration_EArucoDictionary();
	UPackage* Z_Construct_UPackage__Script_LedWallCalibration();
	LEDWALLCALIBRATION_API UScriptStruct* Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions();
// End Cross Module References
	static UEnum* EArucoDictionary_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_LedWallCalibration_EArucoDictionary, Z_Construct_UPackage__Script_LedWallCalibration(), TEXT("EArucoDictionary"));
		}
		return Singleton;
	}
	template<> LEDWALLCALIBRATION_API UEnum* StaticEnum<EArucoDictionary>()
	{
		return EArucoDictionary_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EArucoDictionary(EArucoDictionary_StaticEnum, TEXT("/Script/LedWallCalibration"), TEXT("EArucoDictionary"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_LedWallCalibration_EArucoDictionary_Hash() { return 2724952339U; }
	UEnum* Z_Construct_UEnum_LedWallCalibration_EArucoDictionary()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_LedWallCalibration();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EArucoDictionary"), 0, Get_Z_Construct_UEnum_LedWallCalibration_EArucoDictionary_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "DICT_4X4_50", (int64)DICT_4X4_50 },
				{ "DICT_4X4_100", (int64)DICT_4X4_100 },
				{ "DICT_4X4_250", (int64)DICT_4X4_250 },
				{ "DICT_4X4_1000", (int64)DICT_4X4_1000 },
				{ "DICT_5X5_50", (int64)DICT_5X5_50 },
				{ "DICT_5X5_100", (int64)DICT_5X5_100 },
				{ "DICT_5X5_250", (int64)DICT_5X5_250 },
				{ "DICT_5X5_1000", (int64)DICT_5X5_1000 },
				{ "DICT_6X6_50", (int64)DICT_6X6_50 },
				{ "DICT_6X6_100", (int64)DICT_6X6_100 },
				{ "DICT_6X6_250", (int64)DICT_6X6_250 },
				{ "DICT_6X6_1000", (int64)DICT_6X6_1000 },
				{ "DICT_7X7_50", (int64)DICT_7X7_50 },
				{ "DICT_7X7_100", (int64)DICT_7X7_100 },
				{ "DICT_7X7_250", (int64)DICT_7X7_250 },
				{ "DICT_7X7_1000", (int64)DICT_7X7_1000 },
				{ "DICT_ARUCO_ORIGINAL", (int64)DICT_ARUCO_ORIGINAL },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/** Enumeration of known Aruco dictionaries */" },
				{ "DICT_4X4_100.DisplayName", "DICT_4X4_100" },
				{ "DICT_4X4_100.Name", "DICT_4X4_100" },
				{ "DICT_4X4_1000.DisplayName", "DICT_4X4_1000" },
				{ "DICT_4X4_1000.Name", "DICT_4X4_1000" },
				{ "DICT_4X4_250.DisplayName", "DICT_4X4_250" },
				{ "DICT_4X4_250.Name", "DICT_4X4_250" },
				{ "DICT_4X4_50.DisplayName", "DICT_4X4_50" },
				{ "DICT_4X4_50.Name", "DICT_4X4_50" },
				{ "DICT_5X5_100.DisplayName", "DICT_5X5_100" },
				{ "DICT_5X5_100.Name", "DICT_5X5_100" },
				{ "DICT_5X5_1000.DisplayName", "DICT_5X5_1000" },
				{ "DICT_5X5_1000.Name", "DICT_5X5_1000" },
				{ "DICT_5X5_250.DisplayName", "DICT_5X5_250" },
				{ "DICT_5X5_250.Name", "DICT_5X5_250" },
				{ "DICT_5X5_50.DisplayName", "DICT_5X5_50" },
				{ "DICT_5X5_50.Name", "DICT_5X5_50" },
				{ "DICT_6X6_100.DisplayName", "DICT_6X6_100" },
				{ "DICT_6X6_100.Name", "DICT_6X6_100" },
				{ "DICT_6X6_1000.DisplayName", "DICT_6X6_1000" },
				{ "DICT_6X6_1000.Name", "DICT_6X6_1000" },
				{ "DICT_6X6_250.DisplayName", "DICT_6X6_250" },
				{ "DICT_6X6_250.Name", "DICT_6X6_250" },
				{ "DICT_6X6_50.DisplayName", "DICT_6X6_50" },
				{ "DICT_6X6_50.Name", "DICT_6X6_50" },
				{ "DICT_7X7_100.DisplayName", "DICT_7X7_100" },
				{ "DICT_7X7_100.Name", "DICT_7X7_100" },
				{ "DICT_7X7_1000.DisplayName", "DICT_7X7_1000" },
				{ "DICT_7X7_1000.Name", "DICT_7X7_1000" },
				{ "DICT_7X7_250.DisplayName", "DICT_7X7_250" },
				{ "DICT_7X7_250.Name", "DICT_7X7_250" },
				{ "DICT_7X7_50.DisplayName", "DICT_7X7_50" },
				{ "DICT_7X7_50.Name", "DICT_7X7_50" },
				{ "DICT_ARUCO_ORIGINAL.DisplayName", "DICT_ARUCO_ORIGINAL" },
				{ "DICT_ARUCO_ORIGINAL.Name", "DICT_ARUCO_ORIGINAL" },
				{ "ModuleRelativePath", "Public/LedWallArucoGenerationOptions.h" },
				{ "ToolTip", "Enumeration of known Aruco dictionaries" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_LedWallCalibration,
				nullptr,
				"EArucoDictionary",
				"EArucoDictionary",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Regular,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FLedWallArucoGenerationOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEDWALLCALIBRATION_API uint32 Get_Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions, Z_Construct_UPackage__Script_LedWallCalibration(), TEXT("LedWallArucoGenerationOptions"), sizeof(FLedWallArucoGenerationOptions), Get_Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Hash());
	}
	return Singleton;
}
template<> LEDWALLCALIBRATION_API UScriptStruct* StaticStruct<FLedWallArucoGenerationOptions>()
{
	return FLedWallArucoGenerationOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLedWallArucoGenerationOptions(FLedWallArucoGenerationOptions::StaticStruct, TEXT("/Script/LedWallCalibration"), TEXT("LedWallArucoGenerationOptions"), false, nullptr, nullptr);
static struct FScriptStruct_LedWallCalibration_StaticRegisterNativesFLedWallArucoGenerationOptions
{
	FScriptStruct_LedWallCalibration_StaticRegisterNativesFLedWallArucoGenerationOptions()
	{
		UScriptStruct::DeferCppStructOps<FLedWallArucoGenerationOptions>(FName(TEXT("LedWallArucoGenerationOptions")));
	}
} ScriptStruct_LedWallCalibration_StaticRegisterNativesFLedWallArucoGenerationOptions;
	struct Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TextureWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TextureHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ArucoDictionary_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ArucoDictionary;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarkerId_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MarkerId;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaceModulus_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PlaceModulus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Structure that can be passed to the Aruco generation function\n */" },
		{ "ModuleRelativePath", "Public/LedWallArucoGenerationOptions.h" },
		{ "ToolTip", "Structure that can be passed to the Aruco generation function" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLedWallArucoGenerationOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureWidth_MetaData[] = {
		{ "Category", "Aruco" },
		{ "Comment", "/** Width of the texture that will contain the Aruco markers */" },
		{ "ModuleRelativePath", "Public/LedWallArucoGenerationOptions.h" },
		{ "ToolTip", "Width of the texture that will contain the Aruco markers" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureWidth = { "TextureWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLedWallArucoGenerationOptions, TextureWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureHeight_MetaData[] = {
		{ "Category", "Aruco" },
		{ "Comment", "/** Height of the texture that will contain the Aruco markers */" },
		{ "ModuleRelativePath", "Public/LedWallArucoGenerationOptions.h" },
		{ "ToolTip", "Height of the texture that will contain the Aruco markers" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureHeight = { "TextureHeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLedWallArucoGenerationOptions, TextureHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_ArucoDictionary_MetaData[] = {
		{ "Category", "Aruco" },
		{ "Comment", "/** Aruco dictionary to use when generating the markers */" },
		{ "ModuleRelativePath", "Public/LedWallArucoGenerationOptions.h" },
		{ "ToolTip", "Aruco dictionary to use when generating the markers" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_ArucoDictionary = { "ArucoDictionary", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLedWallArucoGenerationOptions, ArucoDictionary), Z_Construct_UEnum_LedWallCalibration_EArucoDictionary, METADATA_PARAMS(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_ArucoDictionary_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_ArucoDictionary_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_MarkerId_MetaData[] = {
		{ "Category", "Aruco" },
		{ "Comment", "/** Starting marker Id. Arucos will be generated with consecutive Marker Ids, starting from this one */" },
		{ "ModuleRelativePath", "Public/LedWallArucoGenerationOptions.h" },
		{ "ToolTip", "Starting marker Id. Arucos will be generated with consecutive Marker Ids, starting from this one" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_MarkerId = { "MarkerId", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLedWallArucoGenerationOptions, MarkerId), METADATA_PARAMS(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_MarkerId_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_MarkerId_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_PlaceModulus_MetaData[] = {
		{ "Category", "Aruco" },
		{ "Comment", "/** \n\x09 * Used to avoid using up the symbols in the dictionary as quickly.\n\x09 * Will place the next marker id when [(row + column) mod PlaceModulus] is zero.\n\x09 */" },
		{ "ModuleRelativePath", "Public/LedWallArucoGenerationOptions.h" },
		{ "ToolTip", "Used to avoid using up the symbols in the dictionary as quickly.\nWill place the next marker id when [(row + column) mod PlaceModulus] is zero." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_PlaceModulus = { "PlaceModulus", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLedWallArucoGenerationOptions, PlaceModulus), METADATA_PARAMS(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_PlaceModulus_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_PlaceModulus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_TextureHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_ArucoDictionary,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_MarkerId,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::NewProp_PlaceModulus,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LedWallCalibration,
		nullptr,
		&NewStructOps,
		"LedWallArucoGenerationOptions",
		sizeof(FLedWallArucoGenerationOptions),
		alignof(FLedWallArucoGenerationOptions),
		Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LedWallCalibration();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LedWallArucoGenerationOptions"), sizeof(FLedWallArucoGenerationOptions), Get_Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLedWallArucoGenerationOptions_Hash() { return 619223931U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
