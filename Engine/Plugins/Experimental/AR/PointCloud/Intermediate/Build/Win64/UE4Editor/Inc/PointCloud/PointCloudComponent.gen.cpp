// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PointCloud/Public/PointCloudComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePointCloudComponent() {}
// Cross Module References
	POINTCLOUD_API UClass* Z_Construct_UClass_UPointCloudComponent_NoRegister();
	POINTCLOUD_API UClass* Z_Construct_UClass_UPointCloudComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
	UPackage* Z_Construct_UPackage__Script_PointCloud();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FBox();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FBoxSphereBounds();
// End Cross Module References
	DEFINE_FUNCTION(UPointCloudComponent::execGetPointsOutsideBox)
	{
		P_GET_STRUCT_REF(FBox,Z_Param_Out_WorldSpaceBox);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FVector>*)Z_Param__Result=P_THIS->GetPointsOutsideBox(Z_Param_Out_WorldSpaceBox);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPointCloudComponent::execGetPointsInBox)
	{
		P_GET_STRUCT_REF(FBox,Z_Param_Out_WorldSpaceBox);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FVector>*)Z_Param__Result=P_THIS->GetPointsInBox(Z_Param_Out_WorldSpaceBox);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPointCloudComponent::execSetPointSize)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Size);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPointSize(Z_Param_Size);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPointCloudComponent::execSetPointColor)
	{
		P_GET_STRUCT_REF(FLinearColor,Z_Param_Out_Color);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPointColor(Z_Param_Out_Color);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPointCloudComponent::execClearPointCloud)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearPointCloud();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPointCloudComponent::execSetPointCloudWithColors)
	{
		P_GET_TARRAY_REF(FVector,Z_Param_Out_Points);
		P_GET_TARRAY_REF(FColor,Z_Param_Out_Colors);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPointCloudWithColors(Z_Param_Out_Points,Z_Param_Out_Colors);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPointCloudComponent::execSetPointCloud)
	{
		P_GET_TARRAY_REF(FVector,Z_Param_Out_Points);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPointCloud(Z_Param_Out_Points);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPointCloudComponent::execSetIsVisible)
	{
		P_GET_UBOOL(Z_Param_bNewVisibility);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetIsVisible(Z_Param_bNewVisibility);
		P_NATIVE_END;
	}
	void UPointCloudComponent::StaticRegisterNativesUPointCloudComponent()
	{
		UClass* Class = UPointCloudComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClearPointCloud", &UPointCloudComponent::execClearPointCloud },
			{ "GetPointsInBox", &UPointCloudComponent::execGetPointsInBox },
			{ "GetPointsOutsideBox", &UPointCloudComponent::execGetPointsOutsideBox },
			{ "SetIsVisible", &UPointCloudComponent::execSetIsVisible },
			{ "SetPointCloud", &UPointCloudComponent::execSetPointCloud },
			{ "SetPointCloudWithColors", &UPointCloudComponent::execSetPointCloudWithColors },
			{ "SetPointColor", &UPointCloudComponent::execSetPointColor },
			{ "SetPointSize", &UPointCloudComponent::execSetPointSize },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/**\n\x09 * Empties the point cloud\n\x09 */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Empties the point cloud" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "ClearPointCloud", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics
	{
		struct PointCloudComponent_eventGetPointsInBox_Parms
		{
			FBox WorldSpaceBox;
			TArray<FVector> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldSpaceBox_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldSpaceBox;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_WorldSpaceBox_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_WorldSpaceBox = { "WorldSpaceBox", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventGetPointsInBox_Parms, WorldSpaceBox), Z_Construct_UScriptStruct_FBox, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_WorldSpaceBox_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_WorldSpaceBox_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventGetPointsInBox_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_WorldSpaceBox,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** Determines which points are within the box and returns those to the caller */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Determines which points are within the box and returns those to the caller" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "GetPointsInBox", nullptr, nullptr, sizeof(PointCloudComponent_eventGetPointsInBox_Parms), Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics
	{
		struct PointCloudComponent_eventGetPointsOutsideBox_Parms
		{
			FBox WorldSpaceBox;
			TArray<FVector> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldSpaceBox_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldSpaceBox;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_WorldSpaceBox_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_WorldSpaceBox = { "WorldSpaceBox", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventGetPointsOutsideBox_Parms, WorldSpaceBox), Z_Construct_UScriptStruct_FBox, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_WorldSpaceBox_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_WorldSpaceBox_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventGetPointsOutsideBox_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_WorldSpaceBox,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** Determines which points are outside the box and returns those to the caller */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Determines which points are outside the box and returns those to the caller" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "GetPointsOutsideBox", nullptr, nullptr, sizeof(PointCloudComponent_eventGetPointsOutsideBox_Parms), Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics
	{
		struct PointCloudComponent_eventSetIsVisible_Parms
		{
			bool bNewVisibility;
		};
		static void NewProp_bNewVisibility_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewVisibility;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::NewProp_bNewVisibility_SetBit(void* Obj)
	{
		((PointCloudComponent_eventSetIsVisible_Parms*)Obj)->bNewVisibility = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::NewProp_bNewVisibility = { "bNewVisibility", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PointCloudComponent_eventSetIsVisible_Parms), &Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::NewProp_bNewVisibility_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::NewProp_bNewVisibility,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** Changes the visibility setting */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Changes the visibility setting" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "SetIsVisible", nullptr, nullptr, sizeof(PointCloudComponent_eventSetIsVisible_Parms), Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_SetIsVisible()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_SetIsVisible_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics
	{
		struct PointCloudComponent_eventSetPointCloud_Parms
		{
			TArray<FVector> Points;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Points_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Points_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Points;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::NewProp_Points_Inner = { "Points", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::NewProp_Points_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::NewProp_Points = { "Points", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventSetPointCloud_Parms, Points), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::NewProp_Points_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::NewProp_Points_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::NewProp_Points_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::NewProp_Points,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/**\n\x09 * Updates the point cloud data with the new set of points\n\x09 *\n\x09 * @param Points the new set of points to use\n\x09 */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Updates the point cloud data with the new set of points\n\n@param Points the new set of points to use" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "SetPointCloud", nullptr, nullptr, sizeof(PointCloudComponent_eventSetPointCloud_Parms), Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_SetPointCloud()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_SetPointCloud_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics
	{
		struct PointCloudComponent_eventSetPointCloudWithColors_Parms
		{
			TArray<FVector> Points;
			TArray<FColor> Colors;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Points_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Points_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Points;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Colors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Colors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Colors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Points_Inner = { "Points", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Points_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Points = { "Points", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventSetPointCloudWithColors_Parms, Points), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Points_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Points_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Colors_Inner = { "Colors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Colors_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Colors = { "Colors", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventSetPointCloudWithColors_Parms, Colors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Colors_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Colors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Points_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Points,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Colors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::NewProp_Colors,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/**\n\x09 * Updates the point cloud data with the new set of points and colors\n\x09 *\n\x09 * @param Points the new set of points to use\n\x09 * @param Colors the new set of colors to use, must match points size\n\x09 */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Updates the point cloud data with the new set of points and colors\n\n@param Points the new set of points to use\n@param Colors the new set of colors to use, must match points size" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "SetPointCloudWithColors", nullptr, nullptr, sizeof(PointCloudComponent_eventSetPointCloudWithColors_Parms), Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics
	{
		struct PointCloudComponent_eventSetPointColor_Parms
		{
			FLinearColor Color;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::NewProp_Color_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventSetPointColor_Parms, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::NewProp_Color,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** Allows you to change the color of the points being rendered */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Allows you to change the color of the points being rendered" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "SetPointColor", nullptr, nullptr, sizeof(PointCloudComponent_eventSetPointColor_Parms), Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_SetPointColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_SetPointColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics
	{
		struct PointCloudComponent_eventSetPointSize_Parms
		{
			float Size;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Size;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::NewProp_Size = { "Size", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PointCloudComponent_eventSetPointSize_Parms, Size), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::NewProp_Size,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** Allows you to change the size of the points being rendered */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Allows you to change the size of the points being rendered" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPointCloudComponent, nullptr, "SetPointSize", nullptr, nullptr, sizeof(PointCloudComponent_eventSetPointSize_Parms), Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPointCloudComponent_SetPointSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPointCloudComponent_SetPointSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPointCloudComponent_NoRegister()
	{
		return UPointCloudComponent::StaticClass();
	}
	struct Z_Construct_UClass_UPointCloudComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsVisible_MetaData[];
#endif
		static void NewProp_bIsVisible_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsVisible;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointCloudUpdateInterval_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PointCloudUpdateInterval;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PointCloud_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointCloud_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PointCloud;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PointColors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointColors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PointColors;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PointColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PointSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointCloudMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PointCloudMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpriteTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SpriteTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldBounds_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WorldBounds;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPointCloudComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_PointCloud,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPointCloudComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPointCloudComponent_ClearPointCloud, "ClearPointCloud" }, // 4129113769
		{ &Z_Construct_UFunction_UPointCloudComponent_GetPointsInBox, "GetPointsInBox" }, // 920925324
		{ &Z_Construct_UFunction_UPointCloudComponent_GetPointsOutsideBox, "GetPointsOutsideBox" }, // 2565311030
		{ &Z_Construct_UFunction_UPointCloudComponent_SetIsVisible, "SetIsVisible" }, // 2813755001
		{ &Z_Construct_UFunction_UPointCloudComponent_SetPointCloud, "SetPointCloud" }, // 884380400
		{ &Z_Construct_UFunction_UPointCloudComponent_SetPointCloudWithColors, "SetPointCloudWithColors" }, // 1326672817
		{ &Z_Construct_UFunction_UPointCloudComponent_SetPointColor, "SetPointColor" }, // 2975101345
		{ &Z_Construct_UFunction_UPointCloudComponent_SetPointSize, "SetPointSize" }, // 2483661734
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "PointCloud" },
		{ "Comment", "/**\n * Component for rendering a point cloud\n */" },
		{ "HideCategories", "Object LOD Mobility Trigger" },
		{ "IncludePath", "PointCloudComponent.h" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Component for rendering a point cloud" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_bIsVisible_MetaData[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/**\x09If true, each tick the component will render its point cloud */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "If true, each tick the component will render its point cloud" },
	};
#endif
	void Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_bIsVisible_SetBit(void* Obj)
	{
		((UPointCloudComponent*)Obj)->bIsVisible = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_bIsVisible = { "bIsVisible", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPointCloudComponent), &Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_bIsVisible_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_bIsVisible_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_bIsVisible_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudUpdateInterval_MetaData[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/**\x09If > 0, will automatically update the point cloud data from AR system based on this interval (sec) */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "If > 0, will automatically update the point cloud data from AR system based on this interval (sec)" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudUpdateInterval = { "PointCloudUpdateInterval", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, PointCloudUpdateInterval), METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudUpdateInterval_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudUpdateInterval_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloud_Inner = { "PointCloud", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloud_MetaData[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** Point cloud data that will be used for rendering, assumes each point is in world space */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Point cloud data that will be used for rendering, assumes each point is in world space" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloud = { "PointCloud", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, PointCloud), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloud_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloud_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColors_Inner = { "PointColors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColors_MetaData[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** Point cloud color data that will be used for rendering */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "Point cloud color data that will be used for rendering" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColors = { "PointColors", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, PointColors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColors_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColor_MetaData[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** The color to render the points with */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "The color to render the points with" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColor = { "PointColor", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, PointColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointSize_MetaData[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** The size of the point when rendering */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "The size of the point when rendering" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointSize = { "PointSize", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, PointSize), METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudMaterial_MetaData[] = {
		{ "Comment", "/** The material to render with */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "The material to render with" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudMaterial = { "PointCloudMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, PointCloudMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_SpriteTexture_MetaData[] = {
		{ "Category", "Point Cloud" },
		{ "Comment", "/** An optional sprite texture to render the point with */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "An optional sprite texture to render the point with" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_SpriteTexture = { "SpriteTexture", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, SpriteTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_SpriteTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_SpriteTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_WorldBounds_MetaData[] = {
		{ "Comment", "/** World space bounds of the point cloud */" },
		{ "ModuleRelativePath", "Public/PointCloudComponent.h" },
		{ "ToolTip", "World space bounds of the point cloud" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_WorldBounds = { "WorldBounds", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPointCloudComponent, WorldBounds), Z_Construct_UScriptStruct_FBoxSphereBounds, METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_WorldBounds_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_WorldBounds_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPointCloudComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_bIsVisible,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudUpdateInterval,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloud_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloud,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_PointCloudMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_SpriteTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPointCloudComponent_Statics::NewProp_WorldBounds,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPointCloudComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPointCloudComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPointCloudComponent_Statics::ClassParams = {
		&UPointCloudComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPointCloudComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPointCloudComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPointCloudComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPointCloudComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPointCloudComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPointCloudComponent, 2178161834);
	template<> POINTCLOUD_API UClass* StaticClass<UPointCloudComponent>()
	{
		return UPointCloudComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPointCloudComponent(Z_Construct_UClass_UPointCloudComponent, &UPointCloudComponent::StaticClass, TEXT("/Script/PointCloud"), TEXT("UPointCloudComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPointCloudComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
