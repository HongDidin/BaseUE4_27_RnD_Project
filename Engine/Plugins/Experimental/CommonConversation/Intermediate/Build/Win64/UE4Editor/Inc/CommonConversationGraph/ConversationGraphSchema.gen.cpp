// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationGraph/Public/ConversationGraphSchema.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationGraphSchema() {}
// Cross Module References
	COMMONCONVERSATIONGRAPH_API UScriptStruct* Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange();
	UPackage* Z_Construct_UPackage__Script_CommonConversationGraph();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FEdGraphSchemaAction();
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphSchema_NoRegister();
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphSchema();
	AIGRAPH_API UClass* Z_Construct_UClass_UAIGraphSchema();
// End Cross Module References

static_assert(std::is_polymorphic<FConversationGraphSchemaAction_AutoArrange>() == std::is_polymorphic<FEdGraphSchemaAction>(), "USTRUCT FConversationGraphSchemaAction_AutoArrange cannot be polymorphic unless super FEdGraphSchemaAction is polymorphic");

class UScriptStruct* FConversationGraphSchemaAction_AutoArrange::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONGRAPH_API uint32 Get_Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange, Z_Construct_UPackage__Script_CommonConversationGraph(), TEXT("ConversationGraphSchemaAction_AutoArrange"), sizeof(FConversationGraphSchemaAction_AutoArrange), Get_Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONGRAPH_API UScriptStruct* StaticStruct<FConversationGraphSchemaAction_AutoArrange>()
{
	return FConversationGraphSchemaAction_AutoArrange::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange(FConversationGraphSchemaAction_AutoArrange::StaticStruct, TEXT("/Script/CommonConversationGraph"), TEXT("ConversationGraphSchemaAction_AutoArrange"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationGraph_StaticRegisterNativesFConversationGraphSchemaAction_AutoArrange
{
	FScriptStruct_CommonConversationGraph_StaticRegisterNativesFConversationGraphSchemaAction_AutoArrange()
	{
		UScriptStruct::DeferCppStructOps<FConversationGraphSchemaAction_AutoArrange>(FName(TEXT("ConversationGraphSchemaAction_AutoArrange")));
	}
} ScriptStruct_CommonConversationGraph_StaticRegisterNativesFConversationGraphSchemaAction_AutoArrange;
	struct Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/** Action to auto arrange the graph */" },
		{ "ModuleRelativePath", "Public/ConversationGraphSchema.h" },
		{ "ToolTip", "Action to auto arrange the graph" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationGraphSchemaAction_AutoArrange>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationGraph,
		Z_Construct_UScriptStruct_FEdGraphSchemaAction,
		&NewStructOps,
		"ConversationGraphSchemaAction_AutoArrange",
		sizeof(FConversationGraphSchemaAction_AutoArrange),
		alignof(FConversationGraphSchemaAction_AutoArrange),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationGraph();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationGraphSchemaAction_AutoArrange"), sizeof(FConversationGraphSchemaAction_AutoArrange), Get_Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Hash() { return 1802893061U; }
	void UConversationGraphSchema::StaticRegisterNativesUConversationGraphSchema()
	{
	}
	UClass* Z_Construct_UClass_UConversationGraphSchema_NoRegister()
	{
		return UConversationGraphSchema::StaticClass();
	}
	struct Z_Construct_UClass_UConversationGraphSchema_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationGraphSchema_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAIGraphSchema,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationGraphSchema_Statics::Class_MetaDataParams[] = {
		{ "Comment", "//////////////////////////////////////////////////////////////////////\n" },
		{ "IncludePath", "ConversationGraphSchema.h" },
		{ "ModuleRelativePath", "Public/ConversationGraphSchema.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationGraphSchema_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationGraphSchema>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationGraphSchema_Statics::ClassParams = {
		&UConversationGraphSchema::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationGraphSchema_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationGraphSchema_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationGraphSchema()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationGraphSchema_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationGraphSchema, 3652346816);
	template<> COMMONCONVERSATIONGRAPH_API UClass* StaticClass<UConversationGraphSchema>()
	{
		return UConversationGraphSchema::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationGraphSchema(Z_Construct_UClass_UConversationGraphSchema, &UConversationGraphSchema::StaticClass, TEXT("/Script/CommonConversationGraph"), TEXT("UConversationGraphSchema"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationGraphSchema);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
