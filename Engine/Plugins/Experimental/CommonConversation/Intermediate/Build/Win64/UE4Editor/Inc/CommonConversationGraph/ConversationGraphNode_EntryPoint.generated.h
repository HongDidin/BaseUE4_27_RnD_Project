// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONCONVERSATIONGRAPH_ConversationGraphNode_EntryPoint_generated_h
#error "ConversationGraphNode_EntryPoint.generated.h already included, missing '#pragma once' in ConversationGraphNode_EntryPoint.h"
#endif
#define COMMONCONVERSATIONGRAPH_ConversationGraphNode_EntryPoint_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationGraphNode_EntryPoint(); \
	friend struct Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics; \
public: \
	DECLARE_CLASS(UConversationGraphNode_EntryPoint, UConversationGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationGraph"), NO_API) \
	DECLARE_SERIALIZER(UConversationGraphNode_EntryPoint)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUConversationGraphNode_EntryPoint(); \
	friend struct Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics; \
public: \
	DECLARE_CLASS(UConversationGraphNode_EntryPoint, UConversationGraphNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationGraph"), NO_API) \
	DECLARE_SERIALIZER(UConversationGraphNode_EntryPoint)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationGraphNode_EntryPoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationGraphNode_EntryPoint) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationGraphNode_EntryPoint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationGraphNode_EntryPoint); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationGraphNode_EntryPoint(UConversationGraphNode_EntryPoint&&); \
	NO_API UConversationGraphNode_EntryPoint(const UConversationGraphNode_EntryPoint&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationGraphNode_EntryPoint(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationGraphNode_EntryPoint(UConversationGraphNode_EntryPoint&&); \
	NO_API UConversationGraphNode_EntryPoint(const UConversationGraphNode_EntryPoint&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationGraphNode_EntryPoint); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationGraphNode_EntryPoint); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationGraphNode_EntryPoint)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_9_PROLOG
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ConversationGraphNode_EntryPoint."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONGRAPH_API UClass* StaticClass<class UConversationGraphNode_EntryPoint>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphNode_EntryPoint_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
