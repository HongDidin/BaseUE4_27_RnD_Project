// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationTypes() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UEnum* Z_Construct_UEnum_CommonConversationRuntime_EConversationChoiceType();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FClientConversationMessagePayload();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FClientConversationMessage();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationParticipants();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationNodeHandle();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FClientConversationOptionEntry();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationBranchPoint();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTagContainer();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationChoiceReference();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationNodeParameterPair();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAdvanceConversationRequest();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationParticipantEntry();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationChoiceDataHandle();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationChoiceData();
// End Cross Module References
	static UEnum* EConversationChoiceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonConversationRuntime_EConversationChoiceType, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("EConversationChoiceType"));
		}
		return Singleton;
	}
	template<> COMMONCONVERSATIONRUNTIME_API UEnum* StaticEnum<EConversationChoiceType>()
	{
		return EConversationChoiceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConversationChoiceType(EConversationChoiceType_StaticEnum, TEXT("/Script/CommonConversationRuntime"), TEXT("EConversationChoiceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonConversationRuntime_EConversationChoiceType_Hash() { return 1921288702U; }
	UEnum* Z_Construct_UEnum_CommonConversationRuntime_EConversationChoiceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConversationChoiceType"), 0, Get_Z_Construct_UEnum_CommonConversationRuntime_EConversationChoiceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConversationChoiceType::ServerOnly", (int64)EConversationChoiceType::ServerOnly },
				{ "EConversationChoiceType::UserChoiceAvailable", (int64)EConversationChoiceType::UserChoiceAvailable },
				{ "EConversationChoiceType::UserChoiceUnavailable", (int64)EConversationChoiceType::UserChoiceUnavailable },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**\n *\n */" },
				{ "ModuleRelativePath", "Public/ConversationTypes.h" },
				{ "ServerOnly.Comment", "/**\n\x09 * ServerOnly choices are the choices the user shouldn't see and are filtered from the client.\n\x09 */" },
				{ "ServerOnly.Name", "EConversationChoiceType::ServerOnly" },
				{ "ServerOnly.ToolTip", "ServerOnly choices are the choices the user shouldn't see and are filtered from the client." },
				{ "UserChoiceAvailable.Name", "EConversationChoiceType::UserChoiceAvailable" },
				{ "UserChoiceUnavailable.Name", "EConversationChoiceType::UserChoiceUnavailable" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
				nullptr,
				"EConversationChoiceType",
				"EConversationChoiceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FClientConversationMessagePayload::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FClientConversationMessagePayload_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FClientConversationMessagePayload, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ClientConversationMessagePayload"), sizeof(FClientConversationMessagePayload), Get_Z_Construct_UScriptStruct_FClientConversationMessagePayload_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FClientConversationMessagePayload>()
{
	return FClientConversationMessagePayload::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FClientConversationMessagePayload(FClientConversationMessagePayload::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ClientConversationMessagePayload"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationMessagePayload
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationMessagePayload()
	{
		UScriptStruct::DeferCppStructOps<FClientConversationMessagePayload>(FName(TEXT("ClientConversationMessagePayload")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationMessagePayload;
	struct Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Message_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Message;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Participants_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Participants;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentNode_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentNode;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Options_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Options_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Options;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FClientConversationMessagePayload>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Message_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Message = { "Message", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationMessagePayload, Message), Z_Construct_UScriptStruct_FClientConversationMessage, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Message_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Message_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Participants_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Participants = { "Participants", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationMessagePayload, Participants), Z_Construct_UScriptStruct_FConversationParticipants, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Participants_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Participants_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_CurrentNode_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_CurrentNode = { "CurrentNode", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationMessagePayload, CurrentNode), Z_Construct_UScriptStruct_FConversationNodeHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_CurrentNode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_CurrentNode_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Options_Inner = { "Options", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FClientConversationOptionEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Options_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Options = { "Options", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationMessagePayload, Options), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Options_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Options_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Message,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Participants,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_CurrentNode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Options_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::NewProp_Options,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ClientConversationMessagePayload",
		sizeof(FClientConversationMessagePayload),
		alignof(FClientConversationMessagePayload),
		Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FClientConversationMessagePayload()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FClientConversationMessagePayload_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ClientConversationMessagePayload"), sizeof(FClientConversationMessagePayload), Get_Z_Construct_UScriptStruct_FClientConversationMessagePayload_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FClientConversationMessagePayload_Hash() { return 2408064613U; }
class UScriptStruct* FClientConversationMessage::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FClientConversationMessage_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FClientConversationMessage, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ClientConversationMessage"), sizeof(FClientConversationMessage), Get_Z_Construct_UScriptStruct_FClientConversationMessage_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FClientConversationMessage>()
{
	return FClientConversationMessage::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FClientConversationMessage(FClientConversationMessage::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ClientConversationMessage"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationMessage
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationMessage()
	{
		UScriptStruct::DeferCppStructOps<FClientConversationMessage>(FName(TEXT("ClientConversationMessage")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationMessage;
	struct Z_Construct_UScriptStruct_FClientConversationMessage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpeakerID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SpeakerID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticipantDisplayName_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ParticipantDisplayName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Text_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Text;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessage_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FClientConversationMessage>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_SpeakerID_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_SpeakerID = { "SpeakerID", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationMessage, SpeakerID), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_SpeakerID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_SpeakerID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_ParticipantDisplayName_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_ParticipantDisplayName = { "ParticipantDisplayName", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationMessage, ParticipantDisplayName), METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_ParticipantDisplayName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_ParticipantDisplayName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_Text_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_Text = { "Text", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationMessage, Text), METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_Text_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_Text_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FClientConversationMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_SpeakerID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_ParticipantDisplayName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationMessage_Statics::NewProp_Text,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FClientConversationMessage_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ClientConversationMessage",
		sizeof(FClientConversationMessage),
		alignof(FClientConversationMessage),
		Z_Construct_UScriptStruct_FClientConversationMessage_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationMessage_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FClientConversationMessage()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FClientConversationMessage_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ClientConversationMessage"), sizeof(FClientConversationMessage), Get_Z_Construct_UScriptStruct_FClientConversationMessage_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FClientConversationMessage_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FClientConversationMessage_Hash() { return 1693347078U; }
class UScriptStruct* FConversationBranchPoint::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationBranchPoint_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationBranchPoint, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationBranchPoint"), sizeof(FConversationBranchPoint), Get_Z_Construct_UScriptStruct_FConversationBranchPoint_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationBranchPoint>()
{
	return FConversationBranchPoint::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationBranchPoint(FConversationBranchPoint::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationBranchPoint"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationBranchPoint
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationBranchPoint()
	{
		UScriptStruct::DeferCppStructOps<FConversationBranchPoint>(FName(TEXT("ConversationBranchPoint")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationBranchPoint;
	struct Z_Construct_UScriptStruct_FConversationBranchPoint_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnScopeStack_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnScopeStack_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnScopeStack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientChoice_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ClientChoice;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * You can think of the FConversationBranchPoint as the owner of FClientConversationOptionEntry.\n * We don't send this one to the client though, we store temporary state here so that when a user\n * picks a choice there may be other things we need to know or store for actions we need to take\n * due to the user picking this choice.  For example, some choices may introduce needing to\n * 'push' a new scope so that when that subtree terminates we return to the previous point, like\n * a stack.  There's no reason the client needs to understand this, so we just store it here.\n */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "You can think of the FConversationBranchPoint as the owner of FClientConversationOptionEntry.\nWe don't send this one to the client though, we store temporary state here so that when a user\npicks a choice there may be other things we need to know or store for actions we need to take\ndue to the user picking this choice.  For example, some choices may introduce needing to\n'push' a new scope so that when that subtree terminates we return to the previous point, like\na stack.  There's no reason the client needs to understand this, so we just store it here." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationBranchPoint>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ReturnScopeStack_Inner = { "ReturnScopeStack", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConversationNodeHandle, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ReturnScopeStack_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ReturnScopeStack = { "ReturnScopeStack", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationBranchPoint, ReturnScopeStack), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ReturnScopeStack_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ReturnScopeStack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ClientChoice_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ClientChoice = { "ClientChoice", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationBranchPoint, ClientChoice), Z_Construct_UScriptStruct_FClientConversationOptionEntry, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ClientChoice_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ClientChoice_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ReturnScopeStack_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ReturnScopeStack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::NewProp_ClientChoice,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationBranchPoint",
		sizeof(FConversationBranchPoint),
		alignof(FConversationBranchPoint),
		Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationBranchPoint()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationBranchPoint_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationBranchPoint"), sizeof(FConversationBranchPoint), Get_Z_Construct_UScriptStruct_FConversationBranchPoint_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationBranchPoint_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationBranchPoint_Hash() { return 3646319577U; }
class UScriptStruct* FClientConversationOptionEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FClientConversationOptionEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FClientConversationOptionEntry, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ClientConversationOptionEntry"), sizeof(FClientConversationOptionEntry), Get_Z_Construct_UScriptStruct_FClientConversationOptionEntry_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FClientConversationOptionEntry>()
{
	return FClientConversationOptionEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FClientConversationOptionEntry(FClientConversationOptionEntry::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ClientConversationOptionEntry"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationOptionEntry
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationOptionEntry()
	{
		UScriptStruct::DeferCppStructOps<FClientConversationOptionEntry>(FName(TEXT("ClientConversationOptionEntry")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFClientConversationOptionEntry;
	struct Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChoiceText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ChoiceText;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChoiceTags_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChoiceTags;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ChoiceType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChoiceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ChoiceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChoiceReference_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChoiceReference;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExtraData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtraData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExtraData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * The conversation option entry is what we send to the client, one entry per choice.\n */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "The conversation option entry is what we send to the client, one entry per choice." },
	};
#endif
	void* Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FClientConversationOptionEntry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceText_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceText = { "ChoiceText", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationOptionEntry, ChoiceText), METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceText_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceText_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceTags_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceTags = { "ChoiceTags", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationOptionEntry, ChoiceTags), Z_Construct_UScriptStruct_FGameplayTagContainer, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceTags_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceTags_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceType_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceType = { "ChoiceType", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationOptionEntry, ChoiceType), Z_Construct_UEnum_CommonConversationRuntime_EConversationChoiceType, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceReference_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceReference = { "ChoiceReference", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationOptionEntry, ChoiceReference), Z_Construct_UScriptStruct_FConversationChoiceReference, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceReference_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceReference_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ExtraData_Inner = { "ExtraData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConversationNodeParameterPair, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ExtraData_MetaData[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**\n\x09 * Occasionally a choice might need to send down metadata that's entirely extra.\n     * It's just bonus information for the client to do things like show more information\n     * in the UI.  This information is not used on the return to the server to make the choice.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "Occasionally a choice might need to send down metadata that's entirely extra.\nIt's just bonus information for the client to do things like show more information\nin the UI.  This information is not used on the return to the server to make the choice." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ExtraData = { "ExtraData", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FClientConversationOptionEntry, ExtraData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ExtraData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ExtraData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceTags,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ChoiceReference,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ExtraData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::NewProp_ExtraData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ClientConversationOptionEntry",
		sizeof(FClientConversationOptionEntry),
		alignof(FClientConversationOptionEntry),
		Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FClientConversationOptionEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FClientConversationOptionEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ClientConversationOptionEntry"), sizeof(FClientConversationOptionEntry), Get_Z_Construct_UScriptStruct_FClientConversationOptionEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FClientConversationOptionEntry_Hash() { return 3036138187U; }
class UScriptStruct* FAdvanceConversationRequest::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FAdvanceConversationRequest_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FAdvanceConversationRequest, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("AdvanceConversationRequest"), sizeof(FAdvanceConversationRequest), Get_Z_Construct_UScriptStruct_FAdvanceConversationRequest_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FAdvanceConversationRequest>()
{
	return FAdvanceConversationRequest::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FAdvanceConversationRequest(FAdvanceConversationRequest::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("AdvanceConversationRequest"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFAdvanceConversationRequest
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFAdvanceConversationRequest()
	{
		UScriptStruct::DeferCppStructOps<FAdvanceConversationRequest>(FName(TEXT("AdvanceConversationRequest")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFAdvanceConversationRequest;
	struct Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Choice_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Choice;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UserParameters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UserParameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////\n" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FAdvanceConversationRequest>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_Choice_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_Choice = { "Choice", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAdvanceConversationRequest, Choice), Z_Construct_UScriptStruct_FConversationChoiceReference, METADATA_PARAMS(Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_Choice_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_Choice_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_UserParameters_Inner = { "UserParameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConversationNodeParameterPair, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_UserParameters_MetaData[] = {
		{ "Category", "Conversation" },
		{ "Comment", "//@TODO: CONVERSATION: Not currently supported, TODO.\n" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "@TODO: CONVERSATION: Not currently supported, TODO." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_UserParameters = { "UserParameters", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FAdvanceConversationRequest, UserParameters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_UserParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_UserParameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_Choice,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_UserParameters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::NewProp_UserParameters,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"AdvanceConversationRequest",
		sizeof(FAdvanceConversationRequest),
		alignof(FAdvanceConversationRequest),
		Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FAdvanceConversationRequest()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FAdvanceConversationRequest_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("AdvanceConversationRequest"), sizeof(FAdvanceConversationRequest), Get_Z_Construct_UScriptStruct_FAdvanceConversationRequest_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FAdvanceConversationRequest_Hash() { return 13045344U; }
class UScriptStruct* FConversationChoiceReference::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceReference_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationChoiceReference, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationChoiceReference"), sizeof(FConversationChoiceReference), Get_Z_Construct_UScriptStruct_FConversationChoiceReference_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationChoiceReference>()
{
	return FConversationChoiceReference::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationChoiceReference(FConversationChoiceReference::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationChoiceReference"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceReference
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceReference()
	{
		UScriptStruct::DeferCppStructOps<FConversationChoiceReference>(FName(TEXT("ConversationChoiceReference")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceReference;
	struct Z_Construct_UScriptStruct_FConversationChoiceReference_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeReference_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NodeReference;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NodeParameters_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NodeParameters_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_NodeParameters;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * The conversation choice reference is the closest thing there is to a link at runtime for a choice. \n * Choices always map to a Task node, which is the NodeReference.  However some tasks could potentially\n * dynamically generate several possible choices.  In that case they would do so with a unique set of\n * NodeParameters.  These would allow one task, say, \"Sell N Items\", where it's one task offering 3\n * different items for sale, and so it needs to match-up those choices with what the user chooses later.\n * the NodeParameters would contain the dynamic data generated at runtime signifying for the server\n * what to choose.\n *\n * Luckily the user does not need to guard against cheating clients.  Any dynamically generated choice\n * is remembered by the server - so any choice selected dynamically can be verified by the server as\n * one it offered to the user.\n */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "The conversation choice reference is the closest thing there is to a link at runtime for a choice.\nChoices always map to a Task node, which is the NodeReference.  However some tasks could potentially\ndynamically generate several possible choices.  In that case they would do so with a unique set of\nNodeParameters.  These would allow one task, say, \"Sell N Items\", where it's one task offering 3\ndifferent items for sale, and so it needs to match-up those choices with what the user chooses later.\nthe NodeParameters would contain the dynamic data generated at runtime signifying for the server\nwhat to choose.\n\nLuckily the user does not need to guard against cheating clients.  Any dynamically generated choice\nis remembered by the server - so any choice selected dynamically can be verified by the server as\none it offered to the user." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationChoiceReference>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeReference_MetaData[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/** This is the node that we're targeting with our choice. */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "This is the node that we're targeting with our choice." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeReference = { "NodeReference", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationChoiceReference, NodeReference), Z_Construct_UScriptStruct_FConversationNodeHandle, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeReference_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeReference_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeParameters_Inner = { "NodeParameters", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConversationNodeParameterPair, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeParameters_MetaData[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**\n\x09 * These are the parameters required by that node to be activated.  The \n\x09 * same node could potentially handle several dynamic choices it provides, so these\n     * parameters are used to uniquely identify the choice when the client responds to the server.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "These are the parameters required by that node to be activated.  The\nsame node could potentially handle several dynamic choices it provides, so these\nparameters are used to uniquely identify the choice when the client responds to the server." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeParameters = { "NodeParameters", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationChoiceReference, NodeParameters), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeParameters_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeParameters_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeReference,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeParameters_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::NewProp_NodeParameters,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationChoiceReference",
		sizeof(FConversationChoiceReference),
		alignof(FConversationChoiceReference),
		Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationChoiceReference()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceReference_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationChoiceReference"), sizeof(FConversationChoiceReference), Get_Z_Construct_UScriptStruct_FConversationChoiceReference_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationChoiceReference_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceReference_Hash() { return 1223959569U; }
class UScriptStruct* FConversationNodeParameterPair::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationNodeParameterPair_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationNodeParameterPair, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationNodeParameterPair"), sizeof(FConversationNodeParameterPair), Get_Z_Construct_UScriptStruct_FConversationNodeParameterPair_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationNodeParameterPair>()
{
	return FConversationNodeParameterPair::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationNodeParameterPair(FConversationNodeParameterPair::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationNodeParameterPair"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationNodeParameterPair
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationNodeParameterPair()
	{
		UScriptStruct::DeferCppStructOps<FConversationNodeParameterPair>(FName(TEXT("ConversationNodeParameterPair")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationNodeParameterPair;
	struct Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//////////////////////////////////////////////////////////////////////\n" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationNodeParameterPair>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationNodeParameterPair, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationNodeParameterPair, Value), METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Value_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::NewProp_Value,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationNodeParameterPair",
		sizeof(FConversationNodeParameterPair),
		alignof(FConversationNodeParameterPair),
		Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationNodeParameterPair()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationNodeParameterPair_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationNodeParameterPair"), sizeof(FConversationNodeParameterPair), Get_Z_Construct_UScriptStruct_FConversationNodeParameterPair_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationNodeParameterPair_Hash() { return 2699497172U; }
class UScriptStruct* FConversationParticipants::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationParticipants_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationParticipants, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationParticipants"), sizeof(FConversationParticipants), Get_Z_Construct_UScriptStruct_FConversationParticipants_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationParticipants>()
{
	return FConversationParticipants::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationParticipants(FConversationParticipants::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationParticipants"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationParticipants
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationParticipants()
	{
		UScriptStruct::DeferCppStructOps<FConversationParticipants>(FName(TEXT("ConversationParticipants")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationParticipants;
	struct Z_Construct_UScriptStruct_FConversationParticipants_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_List_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_List_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_List;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationParticipants_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationParticipants>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewProp_List_Inner = { "List", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConversationParticipantEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewProp_List_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewProp_List = { "List", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationParticipants, List), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewProp_List_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewProp_List_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConversationParticipants_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewProp_List_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationParticipants_Statics::NewProp_List,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationParticipants_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationParticipants",
		sizeof(FConversationParticipants),
		alignof(FConversationParticipants),
		Z_Construct_UScriptStruct_FConversationParticipants_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationParticipants_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationParticipants_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationParticipants_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationParticipants()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationParticipants_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationParticipants"), sizeof(FConversationParticipants), Get_Z_Construct_UScriptStruct_FConversationParticipants_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationParticipants_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationParticipants_Hash() { return 3833918727U; }
class UScriptStruct* FConversationParticipantEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationParticipantEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationParticipantEntry, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationParticipantEntry"), sizeof(FConversationParticipantEntry), Get_Z_Construct_UScriptStruct_FConversationParticipantEntry_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationParticipantEntry>()
{
	return FConversationParticipantEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationParticipantEntry(FConversationParticipantEntry::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationParticipantEntry"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationParticipantEntry
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationParticipantEntry()
	{
		UScriptStruct::DeferCppStructOps<FConversationParticipantEntry>(FName(TEXT("ConversationParticipantEntry")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationParticipantEntry;
	struct Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ParticipantID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParticipantID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * \n */" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationParticipantEntry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_Actor_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationParticipantEntry, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_Actor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_Actor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_ParticipantID_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_ParticipantID = { "ParticipantID", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationParticipantEntry, ParticipantID), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_ParticipantID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_ParticipantID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::NewProp_ParticipantID,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationParticipantEntry",
		sizeof(FConversationParticipantEntry),
		alignof(FConversationParticipantEntry),
		Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationParticipantEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationParticipantEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationParticipantEntry"), sizeof(FConversationParticipantEntry), Get_Z_Construct_UScriptStruct_FConversationParticipantEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationParticipantEntry_Hash() { return 2502143731U; }
class UScriptStruct* FConversationChoiceDataHandle::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationChoiceDataHandle, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationChoiceDataHandle"), sizeof(FConversationChoiceDataHandle), Get_Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationChoiceDataHandle>()
{
	return FConversationChoiceDataHandle::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationChoiceDataHandle(FConversationChoiceDataHandle::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationChoiceDataHandle"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceDataHandle
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceDataHandle()
	{
		UScriptStruct::DeferCppStructOps<FConversationChoiceDataHandle>(FName(TEXT("ConversationChoiceDataHandle")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceDataHandle;
	struct Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "//template<>\n//struct TStructOpsTypeTraits<FConversationChoiceData> : public TStructOpsTypeTraitsBase2<FConversationChoiceData>\n//{\n//\x09""enum\n//\x09{\n//\x09\x09WithNetSerializer = true\x09// For now this is REQUIRED for FConversationChoiceDataHandle net serialization to work\n//\x09};\n//};\n" },
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
		{ "ToolTip", "template<>\nstruct TStructOpsTypeTraits<FConversationChoiceData> : public TStructOpsTypeTraitsBase2<FConversationChoiceData>\n{\n       enum\n       {\n               WithNetSerializer = true         For now this is REQUIRED for FConversationChoiceDataHandle net serialization to work\n       };\n};" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationChoiceDataHandle>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationChoiceDataHandle",
		sizeof(FConversationChoiceDataHandle),
		alignof(FConversationChoiceDataHandle),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationChoiceDataHandle()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationChoiceDataHandle"), sizeof(FConversationChoiceDataHandle), Get_Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Hash() { return 41122637U; }
class UScriptStruct* FConversationChoiceData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationChoiceData, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationChoiceData"), sizeof(FConversationChoiceData), Get_Z_Construct_UScriptStruct_FConversationChoiceData_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationChoiceData>()
{
	return FConversationChoiceData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationChoiceData(FConversationChoiceData::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationChoiceData"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceData
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceData()
	{
		UScriptStruct::DeferCppStructOps<FConversationChoiceData>(FName(TEXT("ConversationChoiceData")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationChoiceData;
	struct Z_Construct_UScriptStruct_FConversationChoiceData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationChoiceData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationChoiceData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationChoiceData>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationChoiceData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationChoiceData",
		sizeof(FConversationChoiceData),
		alignof(FConversationChoiceData),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationChoiceData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationChoiceData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationChoiceData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationChoiceData"), sizeof(FConversationChoiceData), Get_Z_Construct_UScriptStruct_FConversationChoiceData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationChoiceData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationChoiceData_Hash() { return 3354910311U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
