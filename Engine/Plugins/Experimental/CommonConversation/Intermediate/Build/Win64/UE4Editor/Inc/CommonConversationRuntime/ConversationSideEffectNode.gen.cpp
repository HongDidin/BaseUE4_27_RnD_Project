// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationSideEffectNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationSideEffectNode() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSideEffectNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSideEffectNode();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSubNode();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationContext();
// End Cross Module References
	DEFINE_FUNCTION(UConversationSideEffectNode::execClientCauseSideEffect)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientCauseSideEffect_Implementation(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationSideEffectNode::execServerCauseSideEffect)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerCauseSideEffect_Implementation(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	static FName NAME_UConversationSideEffectNode_ClientCauseSideEffect = FName(TEXT("ClientCauseSideEffect"));
	void UConversationSideEffectNode::ClientCauseSideEffect(FConversationContext const& Context) const
	{
		ConversationSideEffectNode_eventClientCauseSideEffect_Parms Parms;
		Parms.Context=Context;
		const_cast<UConversationSideEffectNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationSideEffectNode_ClientCauseSideEffect),&Parms);
	}
	static FName NAME_UConversationSideEffectNode_ServerCauseSideEffect = FName(TEXT("ServerCauseSideEffect"));
	void UConversationSideEffectNode::ServerCauseSideEffect(FConversationContext const& Context) const
	{
		ConversationSideEffectNode_eventServerCauseSideEffect_Parms Parms;
		Parms.Context=Context;
		const_cast<UConversationSideEffectNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationSideEffectNode_ServerCauseSideEffect),&Parms);
	}
	void UConversationSideEffectNode::StaticRegisterNativesUConversationSideEffectNode()
	{
		UClass* Class = UConversationSideEffectNode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClientCauseSideEffect", &UConversationSideEffectNode::execClientCauseSideEffect },
			{ "ServerCauseSideEffect", &UConversationSideEffectNode::execServerCauseSideEffect },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationSideEffectNode_eventClientCauseSideEffect_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::NewProp_Context,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationSideEffectNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationSideEffectNode, nullptr, "ClientCauseSideEffect", nullptr, nullptr, sizeof(ConversationSideEffectNode_eventClientCauseSideEffect_Parms), Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48480C08, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationSideEffectNode_eventServerCauseSideEffect_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::NewProp_Context,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationSideEffectNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationSideEffectNode, nullptr, "ServerCauseSideEffect", nullptr, nullptr, sizeof(ConversationSideEffectNode_eventServerCauseSideEffect_Parms), Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48480C04, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UConversationSideEffectNode_NoRegister()
	{
		return UConversationSideEffectNode::StaticClass();
	}
	struct Z_Construct_UClass_UConversationSideEffectNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationSideEffectNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationSubNode,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UConversationSideEffectNode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UConversationSideEffectNode_ClientCauseSideEffect, "ClientCauseSideEffect" }, // 551681272
		{ &Z_Construct_UFunction_UConversationSideEffectNode_ServerCauseSideEffect, "ServerCauseSideEffect" }, // 398088639
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationSideEffectNode_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Side effects are actions that are performed just after a task is executed\n * (this allows state-altering or cosmetic actions to be mixed in to other nodes)\n * \n * When a task executes on the server, it replicates to the client that it executed and\n * to then execute any client side effects that may be necessary for that task.\n */" },
		{ "IncludePath", "ConversationSideEffectNode.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ConversationSideEffectNode.h" },
		{ "ToolTip", "Side effects are actions that are performed just after a task is executed\n(this allows state-altering or cosmetic actions to be mixed in to other nodes)\n\nWhen a task executes on the server, it replicates to the client that it executed and\nto then execute any client side effects that may be necessary for that task." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationSideEffectNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationSideEffectNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationSideEffectNode_Statics::ClassParams = {
		&UConversationSideEffectNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001100A1u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationSideEffectNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationSideEffectNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationSideEffectNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationSideEffectNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationSideEffectNode, 4227584634);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationSideEffectNode>()
	{
		return UConversationSideEffectNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationSideEffectNode(Z_Construct_UClass_UConversationSideEffectNode, &UConversationSideEffectNode::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationSideEffectNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationSideEffectNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
