// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationTaskNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationTaskNode() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationTaskNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationTaskNode();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationNodeWithLinks();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationContext();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationTaskResult();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	COMMONCONVERSATIONRUNTIME_API UEnum* Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSubNode_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UConversationTaskNode::execExecuteClientEffects)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ExecuteClientEffects_Implementation(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationTaskNode::execExecuteTaskNode)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationTaskResult*)Z_Param__Result=P_THIS->ExecuteTaskNode_Implementation(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationTaskNode::execIsRequirementSatisfied)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EConversationRequirementResult*)Z_Param__Result=P_THIS->IsRequirementSatisfied_Implementation(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationTaskNode::execGetNodeBodyColor)
	{
		P_GET_STRUCT_REF(FLinearColor,Z_Param_Out_BodyColor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetNodeBodyColor_Implementation(Z_Param_Out_BodyColor);
		P_NATIVE_END;
	}
	static FName NAME_UConversationTaskNode_ExecuteClientEffects = FName(TEXT("ExecuteClientEffects"));
	void UConversationTaskNode::ExecuteClientEffects(FConversationContext const& Context) const
	{
		ConversationTaskNode_eventExecuteClientEffects_Parms Parms;
		Parms.Context=Context;
		const_cast<UConversationTaskNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationTaskNode_ExecuteClientEffects),&Parms);
	}
	static FName NAME_UConversationTaskNode_ExecuteTaskNode = FName(TEXT("ExecuteTaskNode"));
	FConversationTaskResult UConversationTaskNode::ExecuteTaskNode(FConversationContext const& Context) const
	{
		ConversationTaskNode_eventExecuteTaskNode_Parms Parms;
		Parms.Context=Context;
		const_cast<UConversationTaskNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationTaskNode_ExecuteTaskNode),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UConversationTaskNode_GetNodeBodyColor = FName(TEXT("GetNodeBodyColor"));
	bool UConversationTaskNode::GetNodeBodyColor(FLinearColor& BodyColor) const
	{
		ConversationTaskNode_eventGetNodeBodyColor_Parms Parms;
		Parms.BodyColor=BodyColor;
		const_cast<UConversationTaskNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationTaskNode_GetNodeBodyColor),&Parms);
		BodyColor=Parms.BodyColor;
		return !!Parms.ReturnValue;
	}
	static FName NAME_UConversationTaskNode_IsRequirementSatisfied = FName(TEXT("IsRequirementSatisfied"));
	EConversationRequirementResult UConversationTaskNode::IsRequirementSatisfied(FConversationContext const& Context) const
	{
		ConversationTaskNode_eventIsRequirementSatisfied_Parms Parms;
		Parms.Context=Context;
		const_cast<UConversationTaskNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationTaskNode_IsRequirementSatisfied),&Parms);
		return Parms.ReturnValue;
	}
	void UConversationTaskNode::StaticRegisterNativesUConversationTaskNode()
	{
		UClass* Class = UConversationTaskNode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ExecuteClientEffects", &UConversationTaskNode::execExecuteClientEffects },
			{ "ExecuteTaskNode", &UConversationTaskNode::execExecuteTaskNode },
			{ "GetNodeBodyColor", &UConversationTaskNode::execGetNodeBodyColor },
			{ "IsRequirementSatisfied", &UConversationTaskNode::execIsRequirementSatisfied },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationTaskNode_eventExecuteClientEffects_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::NewProp_Context,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationTaskNode, nullptr, "ExecuteClientEffects", nullptr, nullptr, sizeof(ConversationTaskNode_eventExecuteClientEffects_Parms), Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48480C08, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationTaskNode_eventExecuteTaskNode_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationTaskNode_eventExecuteTaskNode_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationTaskResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationTaskNode, nullptr, "ExecuteTaskNode", nullptr, nullptr, sizeof(ConversationTaskNode_eventExecuteTaskNode_Parms), Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48480C04, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BodyColor;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::NewProp_BodyColor = { "BodyColor", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationTaskNode_eventGetNodeBodyColor_Parms, BodyColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((ConversationTaskNode_eventGetNodeBodyColor_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ConversationTaskNode_eventGetNodeBodyColor_Parms), &Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::NewProp_BodyColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationTaskNode, nullptr, "GetNodeBodyColor", nullptr, nullptr, sizeof(ConversationTaskNode_eventGetNodeBodyColor_Parms), Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48C20C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationTaskNode_eventIsRequirementSatisfied_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationTaskNode_eventIsRequirementSatisfied_Parms, ReturnValue), Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationTaskNode, nullptr, "IsRequirementSatisfied", nullptr, nullptr, sizeof(ConversationTaskNode_eventIsRequirementSatisfied_Parms), Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48480C04, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UConversationTaskNode_NoRegister()
	{
		return UConversationTaskNode::StaticClass();
	}
	struct Z_Construct_UClass_UConversationTaskNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SubNodes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubNodes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SubNodes;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasRequirements_MetaData[];
#endif
		static void NewProp_bHasRequirements_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasRequirements;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasDynamicChoices_MetaData[];
#endif
		static void NewProp_bHasDynamicChoices_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasDynamicChoices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultNodeBodyColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultNodeBodyColor;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationTaskNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationNodeWithLinks,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UConversationTaskNode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UConversationTaskNode_ExecuteClientEffects, "ExecuteClientEffects" }, // 2631409423
		{ &Z_Construct_UFunction_UConversationTaskNode_ExecuteTaskNode, "ExecuteTaskNode" }, // 3938436281
		{ &Z_Construct_UFunction_UConversationTaskNode_GetNodeBodyColor, "GetNodeBodyColor" }, // 2367742773
		{ &Z_Construct_UFunction_UConversationTaskNode_IsRequirementSatisfied, "IsRequirementSatisfied" }, // 2407235967
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationTaskNode_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * The ConversationTaskNode is the basis of any task in the conversation graph,\n * that task may be as simple as saying some text to the user, and providing some choices.\n * However more complex tasks can fire off quests, can spawn actors, pretty much any arbitrary\n * thing you want.\n * \n * The conversation system is less about just a dialogue tree, and more about a graph of\n * actions the NPC can take, and choices they can provide to the player.\n */" },
		{ "IncludePath", "ConversationTaskNode.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
		{ "ToolTip", "The ConversationTaskNode is the basis of any task in the conversation graph,\nthat task may be as simple as saying some text to the user, and providing some choices.\nHowever more complex tasks can fire off quests, can spawn actors, pretty much any arbitrary\nthing you want.\n\nThe conversation system is less about just a dialogue tree, and more about a graph of\nactions the NPC can take, and choices they can provide to the player." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_SubNodes_Inner = { "SubNodes", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UConversationSubNode_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_SubNodes_MetaData[] = {
		{ "Comment", "// Requirements and side effects\n" },
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
		{ "ToolTip", "Requirements and side effects" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_SubNodes = { "SubNodes", nullptr, (EPropertyFlags)0x0010000000000010, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationTaskNode, SubNodes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_SubNodes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_SubNodes_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasRequirements_MetaData[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/* EDITOR ONLY VISUALS: Does this task internally have requirements? */" },
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
		{ "ToolTip", "EDITOR ONLY VISUALS: Does this task internally have requirements?" },
	};
#endif
	void Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasRequirements_SetBit(void* Obj)
	{
		((UConversationTaskNode*)Obj)->bHasRequirements = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasRequirements = { "bHasRequirements", nullptr, (EPropertyFlags)0x0010000800010011, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UConversationTaskNode), &Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasRequirements_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasRequirements_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasRequirements_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasDynamicChoices_MetaData[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/** EDITOR ONLY VISUALS: Does this task generate dynamic choices? */" },
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
		{ "ToolTip", "EDITOR ONLY VISUALS: Does this task generate dynamic choices?" },
	};
#endif
	void Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasDynamicChoices_SetBit(void* Obj)
	{
		((UConversationTaskNode*)Obj)->bHasDynamicChoices = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasDynamicChoices = { "bHasDynamicChoices", nullptr, (EPropertyFlags)0x0010000800010011, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(UConversationTaskNode), &Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasDynamicChoices_SetBit, METADATA_PARAMS(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasDynamicChoices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasDynamicChoices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_DefaultNodeBodyColor_MetaData[] = {
		{ "Category", "Description" },
		{ "Comment", "/** Default color of the node. */" },
		{ "ModuleRelativePath", "Public/ConversationTaskNode.h" },
		{ "ToolTip", "Default color of the node." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_DefaultNodeBodyColor = { "DefaultNodeBodyColor", nullptr, (EPropertyFlags)0x0020080800010011, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationTaskNode, DefaultNodeBodyColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_DefaultNodeBodyColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_DefaultNodeBodyColor_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConversationTaskNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_SubNodes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_SubNodes,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasRequirements,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_bHasDynamicChoices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationTaskNode_Statics::NewProp_DefaultNodeBodyColor,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationTaskNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationTaskNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationTaskNode_Statics::ClassParams = {
		&UConversationTaskNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UConversationTaskNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UConversationTaskNode_Statics::PropPointers),
		0,
		0x001100A1u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationTaskNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationTaskNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationTaskNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationTaskNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationTaskNode, 3084338385);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationTaskNode>()
	{
		return UConversationTaskNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationTaskNode(Z_Construct_UClass_UConversationTaskNode, &UConversationTaskNode::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationTaskNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationTaskNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
