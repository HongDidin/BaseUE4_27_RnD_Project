// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationChoiceNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationChoiceNode() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationChoiceNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationChoiceNode();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSubNode();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationContext();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FClientConversationOptionEntry();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTagContainer();
// End Cross Module References
	DEFINE_FUNCTION(UConversationChoiceNode::execFillChoice)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_GET_STRUCT_REF(FClientConversationOptionEntry,Z_Param_Out_ChoiceEntry);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FillChoice_Implementation(Z_Param_Out_Context,Z_Param_Out_ChoiceEntry);
		P_NATIVE_END;
	}
	static FName NAME_UConversationChoiceNode_FillChoice = FName(TEXT("FillChoice"));
	void UConversationChoiceNode::FillChoice(FConversationContext const& Context, FClientConversationOptionEntry& ChoiceEntry) const
	{
		ConversationChoiceNode_eventFillChoice_Parms Parms;
		Parms.Context=Context;
		Parms.ChoiceEntry=ChoiceEntry;
		const_cast<UConversationChoiceNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationChoiceNode_FillChoice),&Parms);
		ChoiceEntry=Parms.ChoiceEntry;
	}
	void UConversationChoiceNode::StaticRegisterNativesUConversationChoiceNode()
	{
		UClass* Class = UConversationChoiceNode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "FillChoice", &UConversationChoiceNode::execFillChoice },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChoiceEntry;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationChoiceNode_eventFillChoice_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::NewProp_ChoiceEntry = { "ChoiceEntry", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationChoiceNode_eventFillChoice_Parms, ChoiceEntry), Z_Construct_UScriptStruct_FClientConversationOptionEntry, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::NewProp_ChoiceEntry,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationChoiceNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationChoiceNode, nullptr, "FillChoice", nullptr, nullptr, sizeof(ConversationChoiceNode_eventFillChoice_Parms), Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48480C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationChoiceNode_FillChoice()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationChoiceNode_FillChoice_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UConversationChoiceNode_NoRegister()
	{
		return UConversationChoiceNode::StaticClass();
	}
	struct Z_Construct_UClass_UConversationChoiceNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultChoiceDisplayText_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_DefaultChoiceDisplayText;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChoiceTags_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChoiceTags;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationChoiceNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationSubNode,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UConversationChoiceNode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UConversationChoiceNode_FillChoice, "FillChoice" }, // 2673078386
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationChoiceNode_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A choice on a task indicates that an option be presented to the user when the owning task is one of\n * the available options of a preceding task.\n */" },
		{ "IncludePath", "ConversationChoiceNode.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ConversationChoiceNode.h" },
		{ "ToolTip", "A choice on a task indicates that an option be presented to the user when the owning task is one of\nthe available options of a preceding task." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_DefaultChoiceDisplayText_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ExposeOnSpawn", "" },
		{ "ModuleRelativePath", "Public/ConversationChoiceNode.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_DefaultChoiceDisplayText = { "DefaultChoiceDisplayText", nullptr, (EPropertyFlags)0x0011000000000015, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationChoiceNode, DefaultChoiceDisplayText), METADATA_PARAMS(Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_DefaultChoiceDisplayText_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_DefaultChoiceDisplayText_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_ChoiceTags_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationChoiceNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_ChoiceTags = { "ChoiceTags", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationChoiceNode, ChoiceTags), Z_Construct_UScriptStruct_FGameplayTagContainer, METADATA_PARAMS(Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_ChoiceTags_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_ChoiceTags_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConversationChoiceNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_DefaultChoiceDisplayText,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationChoiceNode_Statics::NewProp_ChoiceTags,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationChoiceNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationChoiceNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationChoiceNode_Statics::ClassParams = {
		&UConversationChoiceNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UConversationChoiceNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UConversationChoiceNode_Statics::PropPointers),
		0,
		0x001100A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationChoiceNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationChoiceNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationChoiceNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationChoiceNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationChoiceNode, 97266118);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationChoiceNode>()
	{
		return UConversationChoiceNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationChoiceNode(Z_Construct_UClass_UConversationChoiceNode, &UConversationChoiceNode::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationChoiceNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationChoiceNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
