// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationInstance() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UFunction* Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationInstance_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationInstance();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationParticipants();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics
	{
		struct _Script_CommonConversationRuntime_eventOnAllParticipantsNotifiedOfStartEvent_Parms
		{
			UConversationInstance* ConversationInstance;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConversationInstance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::NewProp_ConversationInstance = { "ConversationInstance", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_CommonConversationRuntime_eventOnAllParticipantsNotifiedOfStartEvent_Parms, ConversationInstance), Z_Construct_UClass_UConversationInstance_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::NewProp_ConversationInstance,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonConversationRuntime, nullptr, "OnAllParticipantsNotifiedOfStartEvent__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonConversationRuntime_eventOnAllParticipantsNotifiedOfStartEvent_Parms), Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonConversationRuntime_OnAllParticipantsNotifiedOfStartEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UConversationInstance::StaticRegisterNativesUConversationInstance()
	{
	}
	UClass* Z_Construct_UClass_UConversationInstance_NoRegister()
	{
		return UConversationInstance::StaticClass();
	}
	struct Z_Construct_UClass_UConversationInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Participants_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Participants;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationInstance_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * An active conversation between one or more participants\n */" },
		{ "IncludePath", "ConversationInstance.h" },
		{ "ModuleRelativePath", "Public/ConversationInstance.h" },
		{ "ToolTip", "An active conversation between one or more participants" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationInstance_Statics::NewProp_Participants_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConversationInstance_Statics::NewProp_Participants = { "Participants", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationInstance, Participants), Z_Construct_UScriptStruct_FConversationParticipants, METADATA_PARAMS(Z_Construct_UClass_UConversationInstance_Statics::NewProp_Participants_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationInstance_Statics::NewProp_Participants_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConversationInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationInstance_Statics::NewProp_Participants,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationInstance_Statics::ClassParams = {
		&UConversationInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConversationInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConversationInstance_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationInstance, 3132382932);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationInstance>()
	{
		return UConversationInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationInstance(Z_Construct_UClass_UConversationInstance, &UConversationInstance::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
