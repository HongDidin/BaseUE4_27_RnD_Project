// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationParticipantComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationParticipantComponent() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UFunction* Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationParticipantComponent_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationParticipantComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationNodeHandle();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationInstance_NoRegister();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FClientConversationMessagePayload();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationParticipants();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAdvanceConversationRequest();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics
	{
		struct _Script_CommonConversationRuntime_eventOnConversationStatusChanged_Parms
		{
			bool bIsInConversation;
		};
		static void NewProp_bIsInConversation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsInConversation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::NewProp_bIsInConversation_SetBit(void* Obj)
	{
		((_Script_CommonConversationRuntime_eventOnConversationStatusChanged_Parms*)Obj)->bIsInConversation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::NewProp_bIsInConversation = { "bIsInConversation", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_CommonConversationRuntime_eventOnConversationStatusChanged_Parms), &Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::NewProp_bIsInConversation_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::NewProp_bIsInConversation,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_CommonConversationRuntime, nullptr, "OnConversationStatusChanged__DelegateSignature", nullptr, nullptr, sizeof(_Script_CommonConversationRuntime_eventOnConversationStatusChanged_Parms), Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_CommonConversationRuntime_OnConversationStatusChanged__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execOnRep_ConversationsActive)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_OldConversationsActive);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnRep_ConversationsActive(Z_Param_OldConversationsActive);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execClientStartConversation)
	{
		P_GET_OBJECT(UConversationInstance,Z_Param_Conversation);
		P_GET_STRUCT(FGameplayTag,Z_Param_AsParticipant);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientStartConversation_Implementation(Z_Param_Conversation,Z_Param_AsParticipant);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execClientUpdateConversations)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InConversationsActive);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientUpdateConversations_Implementation(Z_Param_InConversationsActive);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execClientUpdateConversation)
	{
		P_GET_STRUCT(FClientConversationMessagePayload,Z_Param_Message);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientUpdateConversation_Implementation(Z_Param_Message);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execClientExecuteTaskAndSideEffects)
	{
		P_GET_STRUCT(FConversationNodeHandle,Z_Param_Handle);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientExecuteTaskAndSideEffects_Implementation(Z_Param_Handle);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execClientUpdateParticipants)
	{
		P_GET_STRUCT(FConversationParticipants,Z_Param_InParticipants);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClientUpdateParticipants_Implementation(Z_Param_InParticipants);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execServerAdvanceConversation)
	{
		P_GET_STRUCT(FAdvanceConversationRequest,Z_Param_InChoicePicked);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ServerAdvanceConversation_Implementation(Z_Param_InChoicePicked);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execGetParticipantDisplayName)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FText*)Z_Param__Result=P_THIS->GetParticipantDisplayName();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationParticipantComponent::execRequestServerAdvanceConversation)
	{
		P_GET_STRUCT_REF(FAdvanceConversationRequest,Z_Param_Out_InChoicePicked);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RequestServerAdvanceConversation(Z_Param_Out_InChoicePicked);
		P_NATIVE_END;
	}
	static FName NAME_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects = FName(TEXT("ClientExecuteTaskAndSideEffects"));
	void UConversationParticipantComponent::ClientExecuteTaskAndSideEffects(FConversationNodeHandle Handle)
	{
		ConversationParticipantComponent_eventClientExecuteTaskAndSideEffects_Parms Parms;
		Parms.Handle=Handle;
		ProcessEvent(FindFunctionChecked(NAME_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects),&Parms);
	}
	static FName NAME_UConversationParticipantComponent_ClientStartConversation = FName(TEXT("ClientStartConversation"));
	void UConversationParticipantComponent::ClientStartConversation(const UConversationInstance* Conversation, const FGameplayTag AsParticipant)
	{
		ConversationParticipantComponent_eventClientStartConversation_Parms Parms;
		Parms.Conversation=Conversation;
		Parms.AsParticipant=AsParticipant;
		ProcessEvent(FindFunctionChecked(NAME_UConversationParticipantComponent_ClientStartConversation),&Parms);
	}
	static FName NAME_UConversationParticipantComponent_ClientUpdateConversation = FName(TEXT("ClientUpdateConversation"));
	void UConversationParticipantComponent::ClientUpdateConversation(FClientConversationMessagePayload const& Message)
	{
		ConversationParticipantComponent_eventClientUpdateConversation_Parms Parms;
		Parms.Message=Message;
		ProcessEvent(FindFunctionChecked(NAME_UConversationParticipantComponent_ClientUpdateConversation),&Parms);
	}
	static FName NAME_UConversationParticipantComponent_ClientUpdateConversations = FName(TEXT("ClientUpdateConversations"));
	void UConversationParticipantComponent::ClientUpdateConversations(int32 InConversationsActive)
	{
		ConversationParticipantComponent_eventClientUpdateConversations_Parms Parms;
		Parms.InConversationsActive=InConversationsActive;
		ProcessEvent(FindFunctionChecked(NAME_UConversationParticipantComponent_ClientUpdateConversations),&Parms);
	}
	static FName NAME_UConversationParticipantComponent_ClientUpdateParticipants = FName(TEXT("ClientUpdateParticipants"));
	void UConversationParticipantComponent::ClientUpdateParticipants(FConversationParticipants const& InParticipants)
	{
		ConversationParticipantComponent_eventClientUpdateParticipants_Parms Parms;
		Parms.InParticipants=InParticipants;
		ProcessEvent(FindFunctionChecked(NAME_UConversationParticipantComponent_ClientUpdateParticipants),&Parms);
	}
	static FName NAME_UConversationParticipantComponent_ServerAdvanceConversation = FName(TEXT("ServerAdvanceConversation"));
	void UConversationParticipantComponent::ServerAdvanceConversation(FAdvanceConversationRequest const& InChoicePicked)
	{
		ConversationParticipantComponent_eventServerAdvanceConversation_Parms Parms;
		Parms.InChoicePicked=InChoicePicked;
		ProcessEvent(FindFunctionChecked(NAME_UConversationParticipantComponent_ServerAdvanceConversation),&Parms);
	}
	void UConversationParticipantComponent::StaticRegisterNativesUConversationParticipantComponent()
	{
		UClass* Class = UConversationParticipantComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ClientExecuteTaskAndSideEffects", &UConversationParticipantComponent::execClientExecuteTaskAndSideEffects },
			{ "ClientStartConversation", &UConversationParticipantComponent::execClientStartConversation },
			{ "ClientUpdateConversation", &UConversationParticipantComponent::execClientUpdateConversation },
			{ "ClientUpdateConversations", &UConversationParticipantComponent::execClientUpdateConversations },
			{ "ClientUpdateParticipants", &UConversationParticipantComponent::execClientUpdateParticipants },
			{ "GetParticipantDisplayName", &UConversationParticipantComponent::execGetParticipantDisplayName },
			{ "OnRep_ConversationsActive", &UConversationParticipantComponent::execOnRep_ConversationsActive },
			{ "RequestServerAdvanceConversation", &UConversationParticipantComponent::execRequestServerAdvanceConversation },
			{ "ServerAdvanceConversation", &UConversationParticipantComponent::execServerAdvanceConversation },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Handle;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::NewProp_Handle = { "Handle", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventClientExecuteTaskAndSideEffects_Parms, Handle), Z_Construct_UScriptStruct_FConversationNodeHandle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::NewProp_Handle,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "ClientExecuteTaskAndSideEffects", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventClientExecuteTaskAndSideEffects_Parms), Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x01080CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Conversation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Conversation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AsParticipant_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AsParticipant;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_Conversation_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_Conversation = { "Conversation", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventClientStartConversation_Parms, Conversation), Z_Construct_UClass_UConversationInstance_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_Conversation_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_Conversation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_AsParticipant_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_AsParticipant = { "AsParticipant", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventClientStartConversation_Parms, AsParticipant), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_AsParticipant_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_AsParticipant_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_Conversation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::NewProp_AsParticipant,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "ClientStartConversation", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventClientStartConversation_Parms), Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x01080CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Message_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Message;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::NewProp_Message_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::NewProp_Message = { "Message", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventClientUpdateConversation_Parms, Message), Z_Construct_UScriptStruct_FClientConversationMessagePayload, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::NewProp_Message_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::NewProp_Message_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::NewProp_Message,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "ClientUpdateConversation", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventClientUpdateConversation_Parms), Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x01080CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics
	{
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InConversationsActive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::NewProp_InConversationsActive = { "InConversationsActive", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventClientUpdateConversations_Parms, InConversationsActive), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::NewProp_InConversationsActive,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "ClientUpdateConversations", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventClientUpdateConversations_Parms), Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x01080CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InParticipants_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InParticipants;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::NewProp_InParticipants_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::NewProp_InParticipants = { "InParticipants", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventClientUpdateParticipants_Parms, InParticipants), Z_Construct_UScriptStruct_FConversationParticipants, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::NewProp_InParticipants_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::NewProp_InParticipants_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::NewProp_InParticipants,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "ClientUpdateParticipants", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventClientUpdateParticipants_Parms), Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x01080CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics
	{
		struct ConversationParticipantComponent_eventGetParticipantDisplayName_Parms
		{
			FText ReturnValue;
		};
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventGetParticipantDisplayName_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "GetParticipantDisplayName", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventGetParticipantDisplayName_Parms), Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics
	{
		struct ConversationParticipantComponent_eventOnRep_ConversationsActive_Parms
		{
			int32 OldConversationsActive;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OldConversationsActive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::NewProp_OldConversationsActive = { "OldConversationsActive", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventOnRep_ConversationsActive_Parms, OldConversationsActive), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::NewProp_OldConversationsActive,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "OnRep_ConversationsActive", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventOnRep_ConversationsActive_Parms), Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics
	{
		struct ConversationParticipantComponent_eventRequestServerAdvanceConversation_Parms
		{
			FAdvanceConversationRequest InChoicePicked;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InChoicePicked_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InChoicePicked;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::NewProp_InChoicePicked_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::NewProp_InChoicePicked = { "InChoicePicked", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventRequestServerAdvanceConversation_Parms, InChoicePicked), Z_Construct_UScriptStruct_FAdvanceConversationRequest, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::NewProp_InChoicePicked_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::NewProp_InChoicePicked_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::NewProp_InChoicePicked,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "RequestServerAdvanceConversation", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventRequestServerAdvanceConversation_Parms), Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InChoicePicked_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InChoicePicked;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::NewProp_InChoicePicked_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::NewProp_InChoicePicked = { "InChoicePicked", nullptr, (EPropertyFlags)0x0010000008000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationParticipantComponent_eventServerAdvanceConversation_Parms, InChoicePicked), Z_Construct_UScriptStruct_FAdvanceConversationRequest, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::NewProp_InChoicePicked_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::NewProp_InChoicePicked_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::NewProp_InChoicePicked,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationParticipantComponent, nullptr, "ServerAdvanceConversation", nullptr, nullptr, sizeof(ConversationParticipantComponent_eventServerAdvanceConversation_Parms), Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00280CC0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UConversationParticipantComponent_NoRegister()
	{
		return UConversationParticipantComponent::StaticClass();
	}
	struct Z_Construct_UClass_UConversationParticipantComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversationsActive_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ConversationsActive;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Auth_CurrentConversation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Auth_CurrentConversation;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Auth_Conversations_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Auth_Conversations_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Auth_Conversations;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationParticipantComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UConversationParticipantComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UConversationParticipantComponent_ClientExecuteTaskAndSideEffects, "ClientExecuteTaskAndSideEffects" }, // 112589905
		{ &Z_Construct_UFunction_UConversationParticipantComponent_ClientStartConversation, "ClientStartConversation" }, // 246529861
		{ &Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversation, "ClientUpdateConversation" }, // 180864086
		{ &Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateConversations, "ClientUpdateConversations" }, // 1651250515
		{ &Z_Construct_UFunction_UConversationParticipantComponent_ClientUpdateParticipants, "ClientUpdateParticipants" }, // 2491610662
		{ &Z_Construct_UFunction_UConversationParticipantComponent_GetParticipantDisplayName, "GetParticipantDisplayName" }, // 2501900867
		{ &Z_Construct_UFunction_UConversationParticipantComponent_OnRep_ConversationsActive, "OnRep_ConversationsActive" }, // 3911928452
		{ &Z_Construct_UFunction_UConversationParticipantComponent_RequestServerAdvanceConversation, "RequestServerAdvanceConversation" }, // 1634224071
		{ &Z_Construct_UFunction_UConversationParticipantComponent_ServerAdvanceConversation, "ServerAdvanceConversation" }, // 3673585276
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationParticipantComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Active conversation participants should have this component on them.\n * It keeps track of what conversations they are participating in (typically no more than one)\n */" },
		{ "IncludePath", "ConversationParticipantComponent.h" },
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
		{ "ToolTip", "Active conversation participants should have this component on them.\nIt keeps track of what conversations they are participating in (typically no more than one)" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_ConversationsActive_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_ConversationsActive = { "ConversationsActive", "OnRep_ConversationsActive", (EPropertyFlags)0x0040000100000020, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationParticipantComponent, ConversationsActive), METADATA_PARAMS(Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_ConversationsActive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_ConversationsActive_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_CurrentConversation_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_CurrentConversation = { "Auth_CurrentConversation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationParticipantComponent, Auth_CurrentConversation), Z_Construct_UClass_UConversationInstance_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_CurrentConversation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_CurrentConversation_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_Conversations_Inner = { "Auth_Conversations", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UConversationInstance_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_Conversations_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationParticipantComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_Conversations = { "Auth_Conversations", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationParticipantComponent, Auth_Conversations), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_Conversations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_Conversations_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConversationParticipantComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_ConversationsActive,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_CurrentConversation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_Conversations_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationParticipantComponent_Statics::NewProp_Auth_Conversations,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationParticipantComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationParticipantComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationParticipantComponent_Statics::ClassParams = {
		&UConversationParticipantComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UConversationParticipantComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UConversationParticipantComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationParticipantComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationParticipantComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationParticipantComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationParticipantComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationParticipantComponent, 2735994186);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationParticipantComponent>()
	{
		return UConversationParticipantComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationParticipantComponent(Z_Construct_UClass_UConversationParticipantComponent, &UConversationParticipantComponent::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationParticipantComponent"), false, nullptr, nullptr, nullptr);

	void UConversationParticipantComponent::ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const
	{
		static const FName Name_ConversationsActive(TEXT("ConversationsActive"));

		const bool bIsValid = true
			&& Name_ConversationsActive == ClassReps[(int32)ENetFields_Private::ConversationsActive].Property->GetFName();

		checkf(bIsValid, TEXT("UHT Generated Rep Indices do not match runtime populated Rep Indices for properties in UConversationParticipantComponent"));
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationParticipantComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
