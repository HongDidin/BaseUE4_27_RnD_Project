// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationGraph/Public/ConversationGraphNode_EntryPoint.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationGraphNode_EntryPoint() {}
// Cross Module References
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphNode_EntryPoint_NoRegister();
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphNode_EntryPoint();
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphNode();
	UPackage* Z_Construct_UPackage__Script_CommonConversationGraph();
// End Cross Module References
	void UConversationGraphNode_EntryPoint::StaticRegisterNativesUConversationGraphNode_EntryPoint()
	{
	}
	UClass* Z_Construct_UClass_UConversationGraphNode_EntryPoint_NoRegister()
	{
		return UConversationGraphNode_EntryPoint::StaticClass();
	}
	struct Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Root node of this conversation block */" },
		{ "IncludePath", "ConversationGraphNode_EntryPoint.h" },
		{ "ModuleRelativePath", "Public/ConversationGraphNode_EntryPoint.h" },
		{ "ToolTip", "Root node of this conversation block" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationGraphNode_EntryPoint>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics::ClassParams = {
		&UConversationGraphNode_EntryPoint::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationGraphNode_EntryPoint()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationGraphNode_EntryPoint_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationGraphNode_EntryPoint, 3081303191);
	template<> COMMONCONVERSATIONGRAPH_API UClass* StaticClass<UConversationGraphNode_EntryPoint>()
	{
		return UConversationGraphNode_EntryPoint::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationGraphNode_EntryPoint(Z_Construct_UClass_UConversationGraphNode_EntryPoint, &UConversationGraphNode_EntryPoint::StaticClass, TEXT("/Script/CommonConversationGraph"), TEXT("UConversationGraphNode_EntryPoint"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationGraphNode_EntryPoint);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
