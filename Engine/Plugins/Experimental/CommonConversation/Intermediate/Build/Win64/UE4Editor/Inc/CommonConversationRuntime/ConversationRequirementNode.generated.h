// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
enum class EConversationRequirementResult : uint8;
struct FConversationContext;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationRequirementNode_generated_h
#error "ConversationRequirementNode.generated.h already included, missing '#pragma once' in ConversationRequirementNode.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationRequirementNode_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_RPC_WRAPPERS \
	virtual EConversationRequirementResult IsRequirementSatisfied_Implementation(FConversationContext const& Context) const; \
 \
	DECLARE_FUNCTION(execIsRequirementSatisfied);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual EConversationRequirementResult IsRequirementSatisfied_Implementation(FConversationContext const& Context) const; \
 \
	DECLARE_FUNCTION(execIsRequirementSatisfied);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_EVENT_PARMS \
	struct ConversationRequirementNode_eventIsRequirementSatisfied_Parms \
	{ \
		FConversationContext Context; \
		EConversationRequirementResult ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		ConversationRequirementNode_eventIsRequirementSatisfied_Parms() \
			: ReturnValue((EConversationRequirementResult)0) \
		{ \
		} \
	};


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationRequirementNode(); \
	friend struct Z_Construct_UClass_UConversationRequirementNode_Statics; \
public: \
	DECLARE_CLASS(UConversationRequirementNode, UConversationSubNode, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationRequirementNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUConversationRequirementNode(); \
	friend struct Z_Construct_UClass_UConversationRequirementNode_Statics; \
public: \
	DECLARE_CLASS(UConversationRequirementNode, UConversationSubNode, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationRequirementNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationRequirementNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationRequirementNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationRequirementNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationRequirementNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationRequirementNode(UConversationRequirementNode&&); \
	NO_API UConversationRequirementNode(const UConversationRequirementNode&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationRequirementNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationRequirementNode(UConversationRequirementNode&&); \
	NO_API UConversationRequirementNode(const UConversationRequirementNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationRequirementNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationRequirementNode); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationRequirementNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_29_PROLOG \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationRequirementNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationRequirementNode_h


#define FOREACH_ENUM_ECONVERSATIONREQUIREMENTRESULT(op) \
	op(EConversationRequirementResult::Passed) \
	op(EConversationRequirementResult::FailedButVisible) \
	op(EConversationRequirementResult::FailedAndHidden) 

enum class EConversationRequirementResult : uint8;
template<> COMMONCONVERSATIONRUNTIME_API UEnum* StaticEnum<EConversationRequirementResult>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
