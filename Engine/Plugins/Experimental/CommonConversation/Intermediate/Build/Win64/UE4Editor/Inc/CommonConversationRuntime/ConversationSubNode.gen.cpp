// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationSubNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationSubNode() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSubNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSubNode();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationNode();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
// End Cross Module References
	void UConversationSubNode::StaticRegisterNativesUConversationSubNode()
	{
	}
	UClass* Z_Construct_UClass_UConversationSubNode_NoRegister()
	{
		return UConversationSubNode::StaticClass();
	}
	struct Z_Construct_UClass_UConversationSubNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationSubNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationNode,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationSubNode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * @TODO: CONVERSATION: Comment me\n */" },
		{ "IncludePath", "ConversationSubNode.h" },
		{ "ModuleRelativePath", "Public/ConversationSubNode.h" },
		{ "ToolTip", "@TODO: CONVERSATION: Comment me" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationSubNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationSubNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationSubNode_Statics::ClassParams = {
		&UConversationSubNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001100A1u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationSubNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationSubNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationSubNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationSubNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationSubNode, 2343432749);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationSubNode>()
	{
		return UConversationSubNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationSubNode(Z_Construct_UClass_UConversationSubNode, &UConversationSubNode::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationSubNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationSubNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
