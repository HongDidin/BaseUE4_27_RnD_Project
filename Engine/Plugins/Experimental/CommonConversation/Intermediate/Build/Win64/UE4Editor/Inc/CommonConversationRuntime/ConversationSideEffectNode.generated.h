// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FConversationContext;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationSideEffectNode_generated_h
#error "ConversationSideEffectNode.generated.h already included, missing '#pragma once' in ConversationSideEffectNode.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationSideEffectNode_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_RPC_WRAPPERS \
	virtual void ClientCauseSideEffect_Implementation(FConversationContext const& Context) const; \
	virtual void ServerCauseSideEffect_Implementation(FConversationContext const& Context) const; \
 \
	DECLARE_FUNCTION(execClientCauseSideEffect); \
	DECLARE_FUNCTION(execServerCauseSideEffect);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ClientCauseSideEffect_Implementation(FConversationContext const& Context) const; \
	virtual void ServerCauseSideEffect_Implementation(FConversationContext const& Context) const; \
 \
	DECLARE_FUNCTION(execClientCauseSideEffect); \
	DECLARE_FUNCTION(execServerCauseSideEffect);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_EVENT_PARMS \
	struct ConversationSideEffectNode_eventClientCauseSideEffect_Parms \
	{ \
		FConversationContext Context; \
	}; \
	struct ConversationSideEffectNode_eventServerCauseSideEffect_Parms \
	{ \
		FConversationContext Context; \
	};


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationSideEffectNode(); \
	friend struct Z_Construct_UClass_UConversationSideEffectNode_Statics; \
public: \
	DECLARE_CLASS(UConversationSideEffectNode, UConversationSubNode, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationSideEffectNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUConversationSideEffectNode(); \
	friend struct Z_Construct_UClass_UConversationSideEffectNode_Statics; \
public: \
	DECLARE_CLASS(UConversationSideEffectNode, UConversationSubNode, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationSideEffectNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationSideEffectNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationSideEffectNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationSideEffectNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationSideEffectNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationSideEffectNode(UConversationSideEffectNode&&); \
	NO_API UConversationSideEffectNode(const UConversationSideEffectNode&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationSideEffectNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationSideEffectNode(UConversationSideEffectNode&&); \
	NO_API UConversationSideEffectNode(const UConversationSideEffectNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationSideEffectNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationSideEffectNode); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationSideEffectNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_18_PROLOG \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationSideEffectNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationSideEffectNode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
