// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UConversationInstance;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationInstance_generated_h
#error "ConversationInstance.generated.h already included, missing '#pragma once' in ConversationInstance.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationInstance_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_18_DELEGATE \
struct _Script_CommonConversationRuntime_eventOnAllParticipantsNotifiedOfStartEvent_Parms \
{ \
	UConversationInstance* ConversationInstance; \
}; \
static inline void FOnAllParticipantsNotifiedOfStartEvent_DelegateWrapper(const FMulticastScriptDelegate& OnAllParticipantsNotifiedOfStartEvent, UConversationInstance* ConversationInstance) \
{ \
	_Script_CommonConversationRuntime_eventOnAllParticipantsNotifiedOfStartEvent_Parms Parms; \
	Parms.ConversationInstance=ConversationInstance; \
	OnAllParticipantsNotifiedOfStartEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationInstance(); \
	friend struct Z_Construct_UClass_UConversationInstance_Statics; \
public: \
	DECLARE_CLASS(UConversationInstance, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationInstance)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUConversationInstance(); \
	friend struct Z_Construct_UClass_UConversationInstance_Statics; \
public: \
	DECLARE_CLASS(UConversationInstance, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationInstance)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationInstance(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationInstance(UConversationInstance&&); \
	NO_API UConversationInstance(const UConversationInstance&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationInstance(UConversationInstance&&); \
	NO_API UConversationInstance(const UConversationInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationInstance); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationInstance); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConversationInstance)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Participants() { return STRUCT_OFFSET(UConversationInstance, Participants); }


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_25_PROLOG
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
