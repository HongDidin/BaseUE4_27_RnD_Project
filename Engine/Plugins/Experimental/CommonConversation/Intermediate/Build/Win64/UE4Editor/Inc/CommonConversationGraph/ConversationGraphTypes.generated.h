// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONCONVERSATIONGRAPH_ConversationGraphTypes_generated_h
#error "ConversationGraphTypes.generated.h already included, missing '#pragma once' in ConversationGraphTypes.h"
#endif
#define COMMONCONVERSATIONGRAPH_ConversationGraphTypes_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationGraphTypes(); \
	friend struct Z_Construct_UClass_UConversationGraphTypes_Statics; \
public: \
	DECLARE_CLASS(UConversationGraphTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationGraph"), NO_API) \
	DECLARE_SERIALIZER(UConversationGraphTypes)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUConversationGraphTypes(); \
	friend struct Z_Construct_UClass_UConversationGraphTypes_Statics; \
public: \
	DECLARE_CLASS(UConversationGraphTypes, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationGraph"), NO_API) \
	DECLARE_SERIALIZER(UConversationGraphTypes)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationGraphTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationGraphTypes) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationGraphTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationGraphTypes); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationGraphTypes(UConversationGraphTypes&&); \
	NO_API UConversationGraphTypes(const UConversationGraphTypes&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationGraphTypes(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationGraphTypes(UConversationGraphTypes&&); \
	NO_API UConversationGraphTypes(const UConversationGraphTypes&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationGraphTypes); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationGraphTypes); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationGraphTypes)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_46_PROLOG
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h_49_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ConversationGraphTypes."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONGRAPH_API UClass* StaticClass<class UConversationGraphTypes>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
