// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationContext.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationContext() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UEnum* Z_Construct_UEnum_CommonConversationRuntime_EConversationTaskResultType();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationContext();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationRegistry_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationInstance_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationParticipantComponent_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationTaskNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationNodeHandle();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationTaskResult();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FAdvanceConversationRequest();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FClientConversationMessage();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationContextHelpers_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationContextHelpers();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
// End Cross Module References
	static UEnum* EConversationTaskResultType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonConversationRuntime_EConversationTaskResultType, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("EConversationTaskResultType"));
		}
		return Singleton;
	}
	template<> COMMONCONVERSATIONRUNTIME_API UEnum* StaticEnum<EConversationTaskResultType>()
	{
		return EConversationTaskResultType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConversationTaskResultType(EConversationTaskResultType_StaticEnum, TEXT("/Script/CommonConversationRuntime"), TEXT("EConversationTaskResultType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonConversationRuntime_EConversationTaskResultType_Hash() { return 598950120U; }
	UEnum* Z_Construct_UEnum_CommonConversationRuntime_EConversationTaskResultType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConversationTaskResultType"), 0, Get_Z_Construct_UEnum_CommonConversationRuntime_EConversationTaskResultType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConversationTaskResultType::Invalid", (int64)EConversationTaskResultType::Invalid },
				{ "EConversationTaskResultType::AbortConversation", (int64)EConversationTaskResultType::AbortConversation },
				{ "EConversationTaskResultType::AdvanceConversation", (int64)EConversationTaskResultType::AdvanceConversation },
				{ "EConversationTaskResultType::AdvanceConversationWithChoice", (int64)EConversationTaskResultType::AdvanceConversationWithChoice },
				{ "EConversationTaskResultType::PauseConversationAndSendClientChoices", (int64)EConversationTaskResultType::PauseConversationAndSendClientChoices },
				{ "EConversationTaskResultType::ReturnToLastClientChoice", (int64)EConversationTaskResultType::ReturnToLastClientChoice },
				{ "EConversationTaskResultType::ReturnToCurrentClientChoice", (int64)EConversationTaskResultType::ReturnToCurrentClientChoice },
				{ "EConversationTaskResultType::ReturnToConversationStart", (int64)EConversationTaskResultType::ReturnToConversationStart },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AbortConversation.Comment", "/** Aborts the conversation. */" },
				{ "AbortConversation.Name", "EConversationTaskResultType::AbortConversation" },
				{ "AbortConversation.ToolTip", "Aborts the conversation." },
				{ "AdvanceConversation.Comment", "/** Advances the conversation to the next task, or a random one if there are multiple. */" },
				{ "AdvanceConversation.Name", "EConversationTaskResultType::AdvanceConversation" },
				{ "AdvanceConversation.ToolTip", "Advances the conversation to the next task, or a random one if there are multiple." },
				{ "AdvanceConversationWithChoice.Comment", "/**\n\x09 * Advances the conversation to a choice, this choice does not have to be one that would normally come next.\n\x09 * Consider using this in advanced situations where you want to potentially dynamically jump to any node in\n\x09 * existence.\n\x09 */" },
				{ "AdvanceConversationWithChoice.Name", "EConversationTaskResultType::AdvanceConversationWithChoice" },
				{ "AdvanceConversationWithChoice.ToolTip", "Advances the conversation to a choice, this choice does not have to be one that would normally come next.\nConsider using this in advanced situations where you want to potentially dynamically jump to any node in\nexistence." },
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * The conversation task result type gives the conversation system the instruction it needs\n * after running a task.  Should we continue to the next task? or stop and give the player\n * the choice of moving forward?\n */" },
				{ "Invalid.Name", "EConversationTaskResultType::Invalid" },
				{ "ModuleRelativePath", "Public/ConversationContext.h" },
				{ "PauseConversationAndSendClientChoices.Comment", "/**\n\x09 * Stops the conversation flow and notifies the client that there are choices, with a payload of anything\n\x09 * the NPC needs to say along with whatever choices the user has.\n\x09 */" },
				{ "PauseConversationAndSendClientChoices.Name", "EConversationTaskResultType::PauseConversationAndSendClientChoices" },
				{ "PauseConversationAndSendClientChoices.ToolTip", "Stops the conversation flow and notifies the client that there are choices, with a payload of anything\nthe NPC needs to say along with whatever choices the user has." },
				{ "ReturnToConversationStart.Comment", "/**\n\x09 * Allows jumping back to the beginning of the entire conversation tree, so that you can effectively, return\n\x09 * to the 'main menu'.\n\x09 */" },
				{ "ReturnToConversationStart.Name", "EConversationTaskResultType::ReturnToConversationStart" },
				{ "ReturnToConversationStart.ToolTip", "Allows jumping back to the beginning of the entire conversation tree, so that you can effectively, return\nto the 'main menu'." },
				{ "ReturnToCurrentClientChoice.Comment", "/**\n\x09 * Does not advance the conversation, just refreshes the current choices again. \n\x09 * This option is really useful if you need to have the user make a choice and then\n\x09 * make the same choice again, ex. User clicks an option to buy an item, and you want\n\x09 * them to be able to repeat that action.\n\x09 */" },
				{ "ReturnToCurrentClientChoice.Name", "EConversationTaskResultType::ReturnToCurrentClientChoice" },
				{ "ReturnToCurrentClientChoice.ToolTip", "Does not advance the conversation, just refreshes the current choices again.\nThis option is really useful if you need to have the user make a choice and then\nmake the same choice again, ex. User clicks an option to buy an item, and you want\nthem to be able to repeat that action." },
				{ "ReturnToLastClientChoice.Comment", "/**\n\x09 * Dynamically allows jumping 'back' one step in the conversation.  This does not go back one Task, but\n\x09 * to the last time in the conversation flow we paused conversation and sent the client choices.\n\x09 */" },
				{ "ReturnToLastClientChoice.Name", "EConversationTaskResultType::ReturnToLastClientChoice" },
				{ "ReturnToLastClientChoice.ToolTip", "Dynamically allows jumping 'back' one step in the conversation.  This does not go back one Task, but\nto the last time in the conversation flow we paused conversation and sent the client choices." },
				{ "ToolTip", "The conversation task result type gives the conversation system the instruction it needs\nafter running a task.  Should we continue to the next task? or stop and give the player\nthe choice of moving forward?" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
				nullptr,
				"EConversationTaskResultType",
				"EConversationTaskResultType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FConversationContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationContext, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationContext"), sizeof(FConversationContext), Get_Z_Construct_UScriptStruct_FConversationContext_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationContext>()
{
	return FConversationContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationContext(FConversationContext::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationContext"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationContext
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationContext()
	{
		UScriptStruct::DeferCppStructOps<FConversationContext>(FName(TEXT("ConversationContext")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationContext;
	struct Z_Construct_UScriptStruct_FConversationContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversationRegistry_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ConversationRegistry;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveConversation_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActiveConversation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClientParticipant_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ClientParticipant;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TaskBeingConsidered_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TaskBeingConsidered;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnScopeStack_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnScopeStack_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnScopeStack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bServer_MetaData[];
#endif
		static void NewProp_bServer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bServer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationContext_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// Information about a currently active conversation\n" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
		{ "ToolTip", "Information about a currently active conversation" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationContext>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ConversationRegistry_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ConversationRegistry = { "ConversationRegistry", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationContext, ConversationRegistry), Z_Construct_UClass_UConversationRegistry_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ConversationRegistry_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ConversationRegistry_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ActiveConversation_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ActiveConversation = { "ActiveConversation", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationContext, ActiveConversation), Z_Construct_UClass_UConversationInstance_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ActiveConversation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ActiveConversation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ClientParticipant_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ClientParticipant = { "ClientParticipant", nullptr, (EPropertyFlags)0x0040000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationContext, ClientParticipant), Z_Construct_UClass_UConversationParticipantComponent_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ClientParticipant_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ClientParticipant_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_TaskBeingConsidered_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_TaskBeingConsidered = { "TaskBeingConsidered", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationContext, TaskBeingConsidered), Z_Construct_UClass_UConversationTaskNode_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_TaskBeingConsidered_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_TaskBeingConsidered_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ReturnScopeStack_Inner = { "ReturnScopeStack", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FConversationNodeHandle, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ReturnScopeStack_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ReturnScopeStack = { "ReturnScopeStack", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationContext, ReturnScopeStack), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ReturnScopeStack_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ReturnScopeStack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_bServer_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_bServer_SetBit(void* Obj)
	{
		((FConversationContext*)Obj)->bServer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_bServer = { "bServer", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FConversationContext), &Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_bServer_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_bServer_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_bServer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConversationContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ConversationRegistry,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ActiveConversation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ClientParticipant,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_TaskBeingConsidered,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ReturnScopeStack_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_ReturnScopeStack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationContext_Statics::NewProp_bServer,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationContext",
		sizeof(FConversationContext),
		alignof(FConversationContext),
		Z_Construct_UScriptStruct_FConversationContext_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationContext"), sizeof(FConversationContext), Get_Z_Construct_UScriptStruct_FConversationContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationContext_Hash() { return 4004018519U; }
class UScriptStruct* FConversationTaskResult::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FConversationTaskResult_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConversationTaskResult, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("ConversationTaskResult"), sizeof(FConversationTaskResult), Get_Z_Construct_UScriptStruct_FConversationTaskResult_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FConversationTaskResult>()
{
	return FConversationTaskResult::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConversationTaskResult(FConversationTaskResult::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("ConversationTaskResult"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationTaskResult
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationTaskResult()
	{
		UScriptStruct::DeferCppStructOps<FConversationTaskResult>(FName(TEXT("ConversationTaskResult")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFConversationTaskResult;
	struct Z_Construct_UScriptStruct_FConversationTaskResult_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvanceToChoice_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdvanceToChoice;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Message_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Message;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationTaskResult_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * The FConversationTaskResult encompasses the type of result along with any extra data we need for\n * that kind of result, for example if we're giving the player a message and giving them a choice, what\n * what message do we need to send.\n */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
		{ "ToolTip", "The FConversationTaskResult encompasses the type of result along with any extra data we need for\nthat kind of result, for example if we're giving the player a message and giving them a choice, what\nwhat message do we need to send." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConversationTaskResult>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationTaskResult, Type), Z_Construct_UEnum_CommonConversationRuntime_EConversationTaskResultType, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_AdvanceToChoice_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_AdvanceToChoice = { "AdvanceToChoice", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationTaskResult, AdvanceToChoice), Z_Construct_UScriptStruct_FAdvanceConversationRequest, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_AdvanceToChoice_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_AdvanceToChoice_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Message_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Message = { "Message", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConversationTaskResult, Message), Z_Construct_UScriptStruct_FClientConversationMessage, METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Message_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Message_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConversationTaskResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_AdvanceToChoice,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConversationTaskResult_Statics::NewProp_Message,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConversationTaskResult_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"ConversationTaskResult",
		sizeof(FConversationTaskResult),
		alignof(FConversationTaskResult),
		Z_Construct_UScriptStruct_FConversationTaskResult_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConversationTaskResult_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConversationTaskResult()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConversationTaskResult_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConversationTaskResult"), sizeof(FConversationTaskResult), Get_Z_Construct_UScriptStruct_FConversationTaskResult_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConversationTaskResult_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConversationTaskResult_Hash() { return 3902040663U; }
	DEFINE_FUNCTION(UConversationContextHelpers::execFindConversationComponent)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UConversationParticipantComponent**)Z_Param__Result=UConversationContextHelpers::FindConversationComponent(Z_Param_Actor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execGetConversationParticipantActor)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_GET_STRUCT(FGameplayTag,Z_Param_ParticipantTag);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AActor**)Z_Param__Result=UConversationContextHelpers::GetConversationParticipantActor(Z_Param_Out_Context,Z_Param_ParticipantTag);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execGetConversationParticipant)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_GET_STRUCT(FGameplayTag,Z_Param_ParticipantTag);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UConversationParticipantComponent**)Z_Param__Result=UConversationContextHelpers::GetConversationParticipant(Z_Param_Out_Context,Z_Param_ParticipantTag);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execReturnToConversationStart)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationTaskResult*)Z_Param__Result=UConversationContextHelpers::ReturnToConversationStart(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execReturnToCurrentClientChoice)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationTaskResult*)Z_Param__Result=UConversationContextHelpers::ReturnToCurrentClientChoice(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execReturnToLastClientChoice)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationTaskResult*)Z_Param__Result=UConversationContextHelpers::ReturnToLastClientChoice(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execPauseConversationAndSendClientChoices)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_GET_STRUCT_REF(FClientConversationMessage,Z_Param_Out_Message);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationTaskResult*)Z_Param__Result=UConversationContextHelpers::PauseConversationAndSendClientChoices(Z_Param_Out_Context,Z_Param_Out_Message);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execAdvanceConversationWithChoice)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_GET_STRUCT_REF(FAdvanceConversationRequest,Z_Param_Out_Choice);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationTaskResult*)Z_Param__Result=UConversationContextHelpers::AdvanceConversationWithChoice(Z_Param_Out_Context,Z_Param_Out_Choice);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execAdvanceConversation)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationTaskResult*)Z_Param__Result=UConversationContextHelpers::AdvanceConversation(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execMakeConversationParticipant)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_GET_OBJECT(AActor,Z_Param_ParticipantActor);
		P_GET_STRUCT(FGameplayTag,Z_Param_ParticipantTag);
		P_FINISH;
		P_NATIVE_BEGIN;
		UConversationContextHelpers::MakeConversationParticipant(Z_Param_Out_Context,Z_Param_ParticipantActor,Z_Param_ParticipantTag);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execGetCurrentConversationNodeHandle)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FConversationNodeHandle*)Z_Param__Result=UConversationContextHelpers::GetCurrentConversationNodeHandle(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UConversationContextHelpers::execGetConversationInstance)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UConversationInstance**)Z_Param__Result=UConversationContextHelpers::GetConversationInstance(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	void UConversationContextHelpers::StaticRegisterNativesUConversationContextHelpers()
	{
		UClass* Class = UConversationContextHelpers::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AdvanceConversation", &UConversationContextHelpers::execAdvanceConversation },
			{ "AdvanceConversationWithChoice", &UConversationContextHelpers::execAdvanceConversationWithChoice },
			{ "FindConversationComponent", &UConversationContextHelpers::execFindConversationComponent },
			{ "GetConversationInstance", &UConversationContextHelpers::execGetConversationInstance },
			{ "GetConversationParticipant", &UConversationContextHelpers::execGetConversationParticipant },
			{ "GetConversationParticipantActor", &UConversationContextHelpers::execGetConversationParticipantActor },
			{ "GetCurrentConversationNodeHandle", &UConversationContextHelpers::execGetCurrentConversationNodeHandle },
			{ "MakeConversationParticipant", &UConversationContextHelpers::execMakeConversationParticipant },
			{ "PauseConversationAndSendClientChoices", &UConversationContextHelpers::execPauseConversationAndSendClientChoices },
			{ "ReturnToConversationStart", &UConversationContextHelpers::execReturnToConversationStart },
			{ "ReturnToCurrentClientChoice", &UConversationContextHelpers::execReturnToCurrentClientChoice },
			{ "ReturnToLastClientChoice", &UConversationContextHelpers::execReturnToLastClientChoice },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics
	{
		struct ConversationContextHelpers_eventAdvanceConversation_Parms
		{
			FConversationContext Context;
			FConversationTaskResult ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventAdvanceConversation_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventAdvanceConversation_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationTaskResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "AdvanceConversation", nullptr, nullptr, sizeof(ConversationContextHelpers_eventAdvanceConversation_Parms), Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics
	{
		struct ConversationContextHelpers_eventAdvanceConversationWithChoice_Parms
		{
			FConversationContext Context;
			FAdvanceConversationRequest Choice;
			FConversationTaskResult ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Choice_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Choice;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventAdvanceConversationWithChoice_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Context_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Choice_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Choice = { "Choice", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventAdvanceConversationWithChoice_Parms, Choice), Z_Construct_UScriptStruct_FAdvanceConversationRequest, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Choice_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Choice_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventAdvanceConversationWithChoice_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationTaskResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_Choice,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::Function_MetaDataParams[] = {
		{ "AutoCreateRefTerm", "Choice" },
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "AdvanceConversationWithChoice", nullptr, nullptr, sizeof(ConversationContextHelpers_eventAdvanceConversationWithChoice_Parms), Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics
	{
		struct ConversationContextHelpers_eventFindConversationComponent_Parms
		{
			AActor* Actor;
			UConversationParticipantComponent* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventFindConversationComponent_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventFindConversationComponent_Parms, ReturnValue), Z_Construct_UClass_UConversationParticipantComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "FindConversationComponent", nullptr, nullptr, sizeof(ConversationContextHelpers_eventFindConversationComponent_Parms), Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics
	{
		struct ConversationContextHelpers_eventGetConversationInstance_Parms
		{
			FConversationContext Context;
			UConversationInstance* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationInstance_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationInstance_Parms, ReturnValue), Z_Construct_UClass_UConversationInstance_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "GetConversationInstance", nullptr, nullptr, sizeof(ConversationContextHelpers_eventGetConversationInstance_Parms), Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics
	{
		struct ConversationContextHelpers_eventGetConversationParticipant_Parms
		{
			FConversationContext Context;
			FGameplayTag ParticipantTag;
			UConversationParticipantComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParticipantTag;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationParticipant_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_ParticipantTag = { "ParticipantTag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationParticipant_Parms, ParticipantTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationParticipant_Parms, ReturnValue), Z_Construct_UClass_UConversationParticipantComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_ParticipantTag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "GetConversationParticipant", nullptr, nullptr, sizeof(ConversationContextHelpers_eventGetConversationParticipant_Parms), Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics
	{
		struct ConversationContextHelpers_eventGetConversationParticipantActor_Parms
		{
			FConversationContext Context;
			FGameplayTag ParticipantTag;
			AActor* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParticipantTag;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationParticipantActor_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_ParticipantTag = { "ParticipantTag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationParticipantActor_Parms, ParticipantTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetConversationParticipantActor_Parms, ReturnValue), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_ParticipantTag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "GetConversationParticipantActor", nullptr, nullptr, sizeof(ConversationContextHelpers_eventGetConversationParticipantActor_Parms), Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics
	{
		struct ConversationContextHelpers_eventGetCurrentConversationNodeHandle_Parms
		{
			FConversationContext Context;
			FConversationNodeHandle ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetCurrentConversationNodeHandle_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventGetCurrentConversationNodeHandle_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationNodeHandle, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "GetCurrentConversationNodeHandle", nullptr, nullptr, sizeof(ConversationContextHelpers_eventGetCurrentConversationNodeHandle_Parms), Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics
	{
		struct ConversationContextHelpers_eventMakeConversationParticipant_Parms
		{
			FConversationContext Context;
			AActor* ParticipantActor;
			FGameplayTag ParticipantTag;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ParticipantActor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ParticipantTag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventMakeConversationParticipant_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_ParticipantActor = { "ParticipantActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventMakeConversationParticipant_Parms, ParticipantActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_ParticipantTag = { "ParticipantTag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventMakeConversationParticipant_Parms, ParticipantTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_ParticipantActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::NewProp_ParticipantTag,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**\n\x09 * Registers an actor as part of the conversation, that actor doesn't need to have the UConversationParticipantComponent\n\x09 * it won't be added though.\n\x09 */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
		{ "ToolTip", "Registers an actor as part of the conversation, that actor doesn't need to have the UConversationParticipantComponent\nit won't be added though." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "MakeConversationParticipant", nullptr, nullptr, sizeof(ConversationContextHelpers_eventMakeConversationParticipant_Parms), Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics
	{
		struct ConversationContextHelpers_eventPauseConversationAndSendClientChoices_Parms
		{
			FConversationContext Context;
			FClientConversationMessage Message;
			FConversationTaskResult ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Message_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Message;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventPauseConversationAndSendClientChoices_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Context_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Message_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Message = { "Message", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventPauseConversationAndSendClientChoices_Parms, Message), Z_Construct_UScriptStruct_FClientConversationMessage, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Message_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Message_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventPauseConversationAndSendClientChoices_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationTaskResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_Message,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "PauseConversationAndSendClientChoices", nullptr, nullptr, sizeof(ConversationContextHelpers_eventPauseConversationAndSendClientChoices_Parms), Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics
	{
		struct ConversationContextHelpers_eventReturnToConversationStart_Parms
		{
			FConversationContext Context;
			FConversationTaskResult ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventReturnToConversationStart_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventReturnToConversationStart_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationTaskResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "ReturnToConversationStart", nullptr, nullptr, sizeof(ConversationContextHelpers_eventReturnToConversationStart_Parms), Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics
	{
		struct ConversationContextHelpers_eventReturnToCurrentClientChoice_Parms
		{
			FConversationContext Context;
			FConversationTaskResult ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventReturnToCurrentClientChoice_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventReturnToCurrentClientChoice_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationTaskResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "ReturnToCurrentClientChoice", nullptr, nullptr, sizeof(ConversationContextHelpers_eventReturnToCurrentClientChoice_Parms), Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics
	{
		struct ConversationContextHelpers_eventReturnToLastClientChoice_Parms
		{
			FConversationContext Context;
			FConversationTaskResult ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventReturnToLastClientChoice_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationContextHelpers_eventReturnToLastClientChoice_Parms, ReturnValue), Z_Construct_UScriptStruct_FConversationTaskResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "Comment", "/**  */" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationContextHelpers, nullptr, "ReturnToLastClientChoice", nullptr, nullptr, sizeof(ConversationContextHelpers_eventReturnToLastClientChoice_Parms), Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422405, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UConversationContextHelpers_NoRegister()
	{
		return UConversationContextHelpers::StaticClass();
	}
	struct Z_Construct_UClass_UConversationContextHelpers_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationContextHelpers_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UConversationContextHelpers_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversation, "AdvanceConversation" }, // 3704609414
		{ &Z_Construct_UFunction_UConversationContextHelpers_AdvanceConversationWithChoice, "AdvanceConversationWithChoice" }, // 149018021
		{ &Z_Construct_UFunction_UConversationContextHelpers_FindConversationComponent, "FindConversationComponent" }, // 4214759226
		{ &Z_Construct_UFunction_UConversationContextHelpers_GetConversationInstance, "GetConversationInstance" }, // 3538835172
		{ &Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipant, "GetConversationParticipant" }, // 3320867346
		{ &Z_Construct_UFunction_UConversationContextHelpers_GetConversationParticipantActor, "GetConversationParticipantActor" }, // 46443546
		{ &Z_Construct_UFunction_UConversationContextHelpers_GetCurrentConversationNodeHandle, "GetCurrentConversationNodeHandle" }, // 332543713
		{ &Z_Construct_UFunction_UConversationContextHelpers_MakeConversationParticipant, "MakeConversationParticipant" }, // 3776562711
		{ &Z_Construct_UFunction_UConversationContextHelpers_PauseConversationAndSendClientChoices, "PauseConversationAndSendClientChoices" }, // 1935957877
		{ &Z_Construct_UFunction_UConversationContextHelpers_ReturnToConversationStart, "ReturnToConversationStart" }, // 3989908322
		{ &Z_Construct_UFunction_UConversationContextHelpers_ReturnToCurrentClientChoice, "ReturnToCurrentClientChoice" }, // 2616702084
		{ &Z_Construct_UFunction_UConversationContextHelpers_ReturnToLastClientChoice, "ReturnToLastClientChoice" }, // 4019421996
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationContextHelpers_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Wrapper methods from FConversationContext\n" },
		{ "IncludePath", "ConversationContext.h" },
		{ "ModuleRelativePath", "Public/ConversationContext.h" },
		{ "ToolTip", "Wrapper methods from FConversationContext" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationContextHelpers_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationContextHelpers>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationContextHelpers_Statics::ClassParams = {
		&UConversationContextHelpers::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationContextHelpers_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationContextHelpers_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationContextHelpers()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationContextHelpers_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationContextHelpers, 2085817890);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationContextHelpers>()
	{
		return UConversationContextHelpers::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationContextHelpers(Z_Construct_UClass_UConversationContextHelpers, &UConversationContextHelpers::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationContextHelpers"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationContextHelpers);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
