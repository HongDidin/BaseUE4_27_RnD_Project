// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FConversationContext;
struct FConversationTaskResult;
enum class EConversationRequirementResult : uint8;
struct FLinearColor;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationTaskNode_generated_h
#error "ConversationTaskNode.generated.h already included, missing '#pragma once' in ConversationTaskNode.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationTaskNode_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_RPC_WRAPPERS \
	virtual void ExecuteClientEffects_Implementation(FConversationContext const& Context) const; \
	virtual FConversationTaskResult ExecuteTaskNode_Implementation(FConversationContext const& Context) const; \
	virtual EConversationRequirementResult IsRequirementSatisfied_Implementation(FConversationContext const& Context) const; \
	virtual bool GetNodeBodyColor_Implementation(FLinearColor& BodyColor) const; \
 \
	DECLARE_FUNCTION(execExecuteClientEffects); \
	DECLARE_FUNCTION(execExecuteTaskNode); \
	DECLARE_FUNCTION(execIsRequirementSatisfied); \
	DECLARE_FUNCTION(execGetNodeBodyColor);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ExecuteClientEffects_Implementation(FConversationContext const& Context) const; \
	virtual FConversationTaskResult ExecuteTaskNode_Implementation(FConversationContext const& Context) const; \
	virtual EConversationRequirementResult IsRequirementSatisfied_Implementation(FConversationContext const& Context) const; \
	virtual bool GetNodeBodyColor_Implementation(FLinearColor& BodyColor) const; \
 \
	DECLARE_FUNCTION(execExecuteClientEffects); \
	DECLARE_FUNCTION(execExecuteTaskNode); \
	DECLARE_FUNCTION(execIsRequirementSatisfied); \
	DECLARE_FUNCTION(execGetNodeBodyColor);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_EVENT_PARMS \
	struct ConversationTaskNode_eventExecuteClientEffects_Parms \
	{ \
		FConversationContext Context; \
	}; \
	struct ConversationTaskNode_eventExecuteTaskNode_Parms \
	{ \
		FConversationContext Context; \
		FConversationTaskResult ReturnValue; \
	}; \
	struct ConversationTaskNode_eventGetNodeBodyColor_Parms \
	{ \
		FLinearColor BodyColor; \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		ConversationTaskNode_eventGetNodeBodyColor_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct ConversationTaskNode_eventIsRequirementSatisfied_Parms \
	{ \
		FConversationContext Context; \
		EConversationRequirementResult ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		ConversationTaskNode_eventIsRequirementSatisfied_Parms() \
			: ReturnValue((EConversationRequirementResult)0) \
		{ \
		} \
	};


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationTaskNode(); \
	friend struct Z_Construct_UClass_UConversationTaskNode_Statics; \
public: \
	DECLARE_CLASS(UConversationTaskNode, UConversationNodeWithLinks, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationTaskNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUConversationTaskNode(); \
	friend struct Z_Construct_UClass_UConversationTaskNode_Statics; \
public: \
	DECLARE_CLASS(UConversationTaskNode, UConversationNodeWithLinks, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationTaskNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationTaskNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationTaskNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationTaskNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationTaskNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationTaskNode(UConversationTaskNode&&); \
	NO_API UConversationTaskNode(const UConversationTaskNode&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationTaskNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationTaskNode(UConversationTaskNode&&); \
	NO_API UConversationTaskNode(const UConversationTaskNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationTaskNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationTaskNode); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationTaskNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_38_PROLOG \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationTaskNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTaskNode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
