// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationGraph/Public/ConversationGraphNode_Choice.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationGraphNode_Choice() {}
// Cross Module References
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphNode_Choice_NoRegister();
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphNode_Choice();
	COMMONCONVERSATIONGRAPH_API UClass* Z_Construct_UClass_UConversationGraphNode();
	UPackage* Z_Construct_UPackage__Script_CommonConversationGraph();
// End Cross Module References
	void UConversationGraphNode_Choice::StaticRegisterNativesUConversationGraphNode_Choice()
	{
	}
	UClass* Z_Construct_UClass_UConversationGraphNode_Choice_NoRegister()
	{
		return UConversationGraphNode_Choice::StaticClass();
	}
	struct Z_Construct_UClass_UConversationGraphNode_Choice_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationGraphNode_Choice_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationGraphNode,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationGraph,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationGraphNode_Choice_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConversationGraphNode_Choice.h" },
		{ "ModuleRelativePath", "Public/ConversationGraphNode_Choice.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationGraphNode_Choice_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationGraphNode_Choice>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationGraphNode_Choice_Statics::ClassParams = {
		&UConversationGraphNode_Choice::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationGraphNode_Choice_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationGraphNode_Choice_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationGraphNode_Choice()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationGraphNode_Choice_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationGraphNode_Choice, 388410101);
	template<> COMMONCONVERSATIONGRAPH_API UClass* StaticClass<UConversationGraphNode_Choice>()
	{
		return UConversationGraphNode_Choice::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationGraphNode_Choice(Z_Construct_UClass_UConversationGraphNode_Choice, &UConversationGraphNode_Choice::StaticClass, TEXT("/Script/CommonConversationGraph"), TEXT("UConversationGraphNode_Choice"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationGraphNode_Choice);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
