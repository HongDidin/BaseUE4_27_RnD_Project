// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationRegistry.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationRegistry() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COREUOBJECT_API UClass* Z_Construct_UClass_UScriptStruct();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationRegistry_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationRegistry();
	ENGINE_API UClass* Z_Construct_UClass_UWorldSubsystem();
// End Cross Module References
class UScriptStruct* FNetSerializeScriptStructCache_ConvVersion::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern COMMONCONVERSATIONRUNTIME_API uint32 Get_Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("NetSerializeScriptStructCache_ConvVersion"), sizeof(FNetSerializeScriptStructCache_ConvVersion), Get_Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Hash());
	}
	return Singleton;
}
template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<FNetSerializeScriptStructCache_ConvVersion>()
{
	return FNetSerializeScriptStructCache_ConvVersion::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion(FNetSerializeScriptStructCache_ConvVersion::StaticStruct, TEXT("/Script/CommonConversationRuntime"), TEXT("NetSerializeScriptStructCache_ConvVersion"), false, nullptr, nullptr);
static struct FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFNetSerializeScriptStructCache_ConvVersion
{
	FScriptStruct_CommonConversationRuntime_StaticRegisterNativesFNetSerializeScriptStructCache_ConvVersion()
	{
		UScriptStruct::DeferCppStructOps<FNetSerializeScriptStructCache_ConvVersion>(FName(TEXT("NetSerializeScriptStructCache_ConvVersion")));
	}
} ScriptStruct_CommonConversationRuntime_StaticRegisterNativesFNetSerializeScriptStructCache_ConvVersion;
	struct Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ScriptStructsToIndex_ValueProp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ScriptStructsToIndex_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ScriptStructsToIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ScriptStructsToIndex;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_IndexToScriptStructs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IndexToScriptStructs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_IndexToScriptStructs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "//  Container for safely replicating  script struct references (constrained to a specified parent struct)\n" },
		{ "ModuleRelativePath", "Public/ConversationRegistry.h" },
		{ "ToolTip", "Container for safely replicating  script struct references (constrained to a specified parent struct)" },
	};
#endif
	void* Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FNetSerializeScriptStructCache_ConvVersion>();
	}
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex_ValueProp = { "ScriptStructsToIndex", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex_Key_KeyProp = { "ScriptStructsToIndex_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UScriptStruct, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationRegistry.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex = { "ScriptStructsToIndex", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetSerializeScriptStructCache_ConvVersion, ScriptStructsToIndex), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_IndexToScriptStructs_Inner = { "IndexToScriptStructs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UScriptStruct, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_IndexToScriptStructs_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationRegistry.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_IndexToScriptStructs = { "IndexToScriptStructs", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FNetSerializeScriptStructCache_ConvVersion, IndexToScriptStructs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_IndexToScriptStructs_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_IndexToScriptStructs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_ScriptStructsToIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_IndexToScriptStructs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::NewProp_IndexToScriptStructs,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
		nullptr,
		&NewStructOps,
		"NetSerializeScriptStructCache_ConvVersion",
		sizeof(FNetSerializeScriptStructCache_ConvVersion),
		alignof(FNetSerializeScriptStructCache_ConvVersion),
		Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("NetSerializeScriptStructCache_ConvVersion"), sizeof(FNetSerializeScriptStructCache_ConvVersion), Get_Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion_Hash() { return 295876574U; }
	void UConversationRegistry::StaticRegisterNativesUConversationRegistry()
	{
	}
	UClass* Z_Construct_UClass_UConversationRegistry_NoRegister()
	{
		return UConversationRegistry::StaticClass();
	}
	struct Z_Construct_UClass_UConversationRegistry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversationChoiceDataStructCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConversationChoiceDataStructCache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationRegistry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWorldSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationRegistry_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A registry that can answer questions about all available dialogue assets\n */" },
		{ "IncludePath", "ConversationRegistry.h" },
		{ "ModuleRelativePath", "Public/ConversationRegistry.h" },
		{ "ToolTip", "A registry that can answer questions about all available dialogue assets" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationRegistry_Statics::NewProp_ConversationChoiceDataStructCache_MetaData[] = {
		{ "ModuleRelativePath", "Public/ConversationRegistry.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConversationRegistry_Statics::NewProp_ConversationChoiceDataStructCache = { "ConversationChoiceDataStructCache", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationRegistry, ConversationChoiceDataStructCache), Z_Construct_UScriptStruct_FNetSerializeScriptStructCache_ConvVersion, METADATA_PARAMS(Z_Construct_UClass_UConversationRegistry_Statics::NewProp_ConversationChoiceDataStructCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationRegistry_Statics::NewProp_ConversationChoiceDataStructCache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConversationRegistry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationRegistry_Statics::NewProp_ConversationChoiceDataStructCache,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationRegistry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationRegistry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationRegistry_Statics::ClassParams = {
		&UConversationRegistry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConversationRegistry_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConversationRegistry_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationRegistry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationRegistry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationRegistry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationRegistry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationRegistry, 1075734352);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationRegistry>()
	{
		return UConversationRegistry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationRegistry(Z_Construct_UClass_UConversationRegistry, &UConversationRegistry::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationRegistry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationRegistry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
