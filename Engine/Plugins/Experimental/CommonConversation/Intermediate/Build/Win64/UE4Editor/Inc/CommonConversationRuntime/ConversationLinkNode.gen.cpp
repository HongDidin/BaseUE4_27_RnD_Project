// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationLinkNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationLinkNode() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationLinkNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationLinkNode();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationTaskNode();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
// End Cross Module References
	void UConversationLinkNode::StaticRegisterNativesUConversationLinkNode()
	{
	}
	UClass* Z_Construct_UClass_UConversationLinkNode_NoRegister()
	{
		return UConversationLinkNode::StaticClass();
	}
	struct Z_Construct_UClass_UConversationLinkNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteEntryTag_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RemoteEntryTag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationLinkNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationTaskNode,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationLinkNode_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConversationLinkNode.h" },
		{ "ModuleRelativePath", "Public/ConversationLinkNode.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationLinkNode_Statics::NewProp_RemoteEntryTag_MetaData[] = {
		{ "Category", "Link" },
		{ "ModuleRelativePath", "Public/ConversationLinkNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConversationLinkNode_Statics::NewProp_RemoteEntryTag = { "RemoteEntryTag", nullptr, (EPropertyFlags)0x0020080000000011, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationLinkNode, RemoteEntryTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UClass_UConversationLinkNode_Statics::NewProp_RemoteEntryTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationLinkNode_Statics::NewProp_RemoteEntryTag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConversationLinkNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationLinkNode_Statics::NewProp_RemoteEntryTag,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationLinkNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationLinkNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationLinkNode_Statics::ClassParams = {
		&UConversationLinkNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConversationLinkNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConversationLinkNode_Statics::PropPointers),
		0,
		0x001100A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationLinkNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationLinkNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationLinkNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationLinkNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationLinkNode, 1488338918);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationLinkNode>()
	{
		return UConversationLinkNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationLinkNode(Z_Construct_UClass_UConversationLinkNode, &UConversationLinkNode::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationLinkNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationLinkNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
