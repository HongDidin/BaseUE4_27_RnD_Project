// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationRequirementNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationRequirementNode() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UEnum* Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationRequirementNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationRequirementNode();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationSubNode();
	COMMONCONVERSATIONRUNTIME_API UScriptStruct* Z_Construct_UScriptStruct_FConversationContext();
// End Cross Module References
	static UEnum* EConversationRequirementResult_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult, Z_Construct_UPackage__Script_CommonConversationRuntime(), TEXT("EConversationRequirementResult"));
		}
		return Singleton;
	}
	template<> COMMONCONVERSATIONRUNTIME_API UEnum* StaticEnum<EConversationRequirementResult>()
	{
		return EConversationRequirementResult_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EConversationRequirementResult(EConversationRequirementResult_StaticEnum, TEXT("/Script/CommonConversationRuntime"), TEXT("EConversationRequirementResult"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult_Hash() { return 1807101604U; }
	UEnum* Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CommonConversationRuntime();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EConversationRequirementResult"), 0, Get_Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EConversationRequirementResult::Passed", (int64)EConversationRequirementResult::Passed },
				{ "EConversationRequirementResult::FailedButVisible", (int64)EConversationRequirementResult::FailedButVisible },
				{ "EConversationRequirementResult::FailedAndHidden", (int64)EConversationRequirementResult::FailedAndHidden },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * The requirement result.\n */" },
				{ "FailedAndHidden.Comment", "/** This option is not available, and we should keep it hidden. */" },
				{ "FailedAndHidden.Name", "EConversationRequirementResult::FailedAndHidden" },
				{ "FailedAndHidden.ToolTip", "This option is not available, and we should keep it hidden." },
				{ "FailedButVisible.Comment", "/** This option is not available, but we should tell the player about it still. */" },
				{ "FailedButVisible.Name", "EConversationRequirementResult::FailedButVisible" },
				{ "FailedButVisible.ToolTip", "This option is not available, but we should tell the player about it still." },
				{ "ModuleRelativePath", "Public/ConversationRequirementNode.h" },
				{ "Passed.Comment", "/** This option is available */" },
				{ "Passed.Name", "EConversationRequirementResult::Passed" },
				{ "Passed.ToolTip", "This option is available" },
				{ "ToolTip", "The requirement result." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
				nullptr,
				"EConversationRequirementResult",
				"EConversationRequirementResult",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UConversationRequirementNode::execIsRequirementSatisfied)
	{
		P_GET_STRUCT_REF(FConversationContext,Z_Param_Out_Context);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EConversationRequirementResult*)Z_Param__Result=P_THIS->IsRequirementSatisfied_Implementation(Z_Param_Out_Context);
		P_NATIVE_END;
	}
	static FName NAME_UConversationRequirementNode_IsRequirementSatisfied = FName(TEXT("IsRequirementSatisfied"));
	EConversationRequirementResult UConversationRequirementNode::IsRequirementSatisfied(FConversationContext const& Context) const
	{
		ConversationRequirementNode_eventIsRequirementSatisfied_Parms Parms;
		Parms.Context=Context;
		const_cast<UConversationRequirementNode*>(this)->ProcessEvent(FindFunctionChecked(NAME_UConversationRequirementNode_IsRequirementSatisfied),&Parms);
		return Parms.ReturnValue;
	}
	void UConversationRequirementNode::StaticRegisterNativesUConversationRequirementNode()
	{
		UClass* Class = UConversationRequirementNode::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsRequirementSatisfied", &UConversationRequirementNode::execIsRequirementSatisfied },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Context_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_Context_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationRequirementNode_eventIsRequirementSatisfied_Parms, Context), Z_Construct_UScriptStruct_FConversationContext, METADATA_PARAMS(Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_Context_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_Context_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationRequirementNode_eventIsRequirementSatisfied_Parms, ReturnValue), Z_Construct_UEnum_CommonConversationRuntime_EConversationRequirementResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/ConversationRequirementNode.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationRequirementNode, nullptr, "IsRequirementSatisfied", nullptr, nullptr, sizeof(ConversationRequirementNode_eventIsRequirementSatisfied_Parms), Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UConversationRequirementNode_NoRegister()
	{
		return UConversationRequirementNode::StaticClass();
	}
	struct Z_Construct_UClass_UConversationRequirementNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationRequirementNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationSubNode,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UConversationRequirementNode_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UConversationRequirementNode_IsRequirementSatisfied, "IsRequirementSatisfied" }, // 3432511342
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationRequirementNode_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n *  A requirement is placed on a parent node to control whether or not it can be activated\n *  (when a link to the parent node is being evaluated, the requirement will be asked if it is satisfied or not)\n */" },
		{ "IncludePath", "ConversationRequirementNode.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/ConversationRequirementNode.h" },
		{ "ToolTip", "A requirement is placed on a parent node to control whether or not it can be activated\n(when a link to the parent node is being evaluated, the requirement will be asked if it is satisfied or not)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationRequirementNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationRequirementNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationRequirementNode_Statics::ClassParams = {
		&UConversationRequirementNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001100A1u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationRequirementNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationRequirementNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationRequirementNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationRequirementNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationRequirementNode, 4014153233);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationRequirementNode>()
	{
		return UConversationRequirementNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationRequirementNode(Z_Construct_UClass_UConversationRequirementNode, &UConversationRequirementNode::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationRequirementNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationRequirementNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
