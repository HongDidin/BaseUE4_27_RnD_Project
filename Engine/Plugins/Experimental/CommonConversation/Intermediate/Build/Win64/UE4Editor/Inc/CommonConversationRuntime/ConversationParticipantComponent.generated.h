// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UConversationInstance;
struct FGameplayTag;
struct FClientConversationMessagePayload;
struct FConversationNodeHandle;
struct FConversationParticipants;
struct FAdvanceConversationRequest;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationParticipantComponent_generated_h
#error "ConversationParticipantComponent.generated.h already included, missing '#pragma once' in ConversationParticipantComponent.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationParticipantComponent_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_14_DELEGATE \
struct _Script_CommonConversationRuntime_eventOnConversationStatusChanged_Parms \
{ \
	bool bIsInConversation; \
}; \
static inline void FOnConversationStatusChanged_DelegateWrapper(const FMulticastScriptDelegate& OnConversationStatusChanged, bool bIsInConversation) \
{ \
	_Script_CommonConversationRuntime_eventOnConversationStatusChanged_Parms Parms; \
	Parms.bIsInConversation=bIsInConversation ? true : false; \
	OnConversationStatusChanged.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_RPC_WRAPPERS \
	virtual void ClientStartConversation_Implementation(const UConversationInstance* Conversation, const FGameplayTag AsParticipant); \
	virtual void ClientUpdateConversations_Implementation(int32 InConversationsActive); \
	virtual void ClientUpdateConversation_Implementation(FClientConversationMessagePayload const& Message); \
	virtual void ClientExecuteTaskAndSideEffects_Implementation(FConversationNodeHandle Handle); \
	virtual void ClientUpdateParticipants_Implementation(FConversationParticipants const& InParticipants); \
	virtual void ServerAdvanceConversation_Implementation(FAdvanceConversationRequest const& InChoicePicked); \
 \
	DECLARE_FUNCTION(execOnRep_ConversationsActive); \
	DECLARE_FUNCTION(execClientStartConversation); \
	DECLARE_FUNCTION(execClientUpdateConversations); \
	DECLARE_FUNCTION(execClientUpdateConversation); \
	DECLARE_FUNCTION(execClientExecuteTaskAndSideEffects); \
	DECLARE_FUNCTION(execClientUpdateParticipants); \
	DECLARE_FUNCTION(execServerAdvanceConversation); \
	DECLARE_FUNCTION(execGetParticipantDisplayName); \
	DECLARE_FUNCTION(execRequestServerAdvanceConversation);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void ClientStartConversation_Implementation(const UConversationInstance* Conversation, const FGameplayTag AsParticipant); \
	virtual void ClientUpdateConversations_Implementation(int32 InConversationsActive); \
	virtual void ClientUpdateConversation_Implementation(FClientConversationMessagePayload const& Message); \
	virtual void ClientExecuteTaskAndSideEffects_Implementation(FConversationNodeHandle Handle); \
	virtual void ClientUpdateParticipants_Implementation(FConversationParticipants const& InParticipants); \
	virtual void ServerAdvanceConversation_Implementation(FAdvanceConversationRequest const& InChoicePicked); \
 \
	DECLARE_FUNCTION(execOnRep_ConversationsActive); \
	DECLARE_FUNCTION(execClientStartConversation); \
	DECLARE_FUNCTION(execClientUpdateConversations); \
	DECLARE_FUNCTION(execClientUpdateConversation); \
	DECLARE_FUNCTION(execClientExecuteTaskAndSideEffects); \
	DECLARE_FUNCTION(execClientUpdateParticipants); \
	DECLARE_FUNCTION(execServerAdvanceConversation); \
	DECLARE_FUNCTION(execGetParticipantDisplayName); \
	DECLARE_FUNCTION(execRequestServerAdvanceConversation);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_EVENT_PARMS \
	struct ConversationParticipantComponent_eventClientExecuteTaskAndSideEffects_Parms \
	{ \
		FConversationNodeHandle Handle; \
	}; \
	struct ConversationParticipantComponent_eventClientStartConversation_Parms \
	{ \
		const UConversationInstance* Conversation; \
		FGameplayTag AsParticipant; \
	}; \
	struct ConversationParticipantComponent_eventClientUpdateConversation_Parms \
	{ \
		FClientConversationMessagePayload Message; \
	}; \
	struct ConversationParticipantComponent_eventClientUpdateConversations_Parms \
	{ \
		int32 InConversationsActive; \
	}; \
	struct ConversationParticipantComponent_eventClientUpdateParticipants_Parms \
	{ \
		FConversationParticipants InParticipants; \
	}; \
	struct ConversationParticipantComponent_eventServerAdvanceConversation_Parms \
	{ \
		FAdvanceConversationRequest InChoicePicked; \
	};


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationParticipantComponent(); \
	friend struct Z_Construct_UClass_UConversationParticipantComponent_Statics; \
public: \
	DECLARE_CLASS(UConversationParticipantComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationParticipantComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		ConversationsActive=NETFIELD_REP_START, \
		NETFIELD_REP_END=ConversationsActive	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUConversationParticipantComponent(); \
	friend struct Z_Construct_UClass_UConversationParticipantComponent_Statics; \
public: \
	DECLARE_CLASS(UConversationParticipantComponent, UActorComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationParticipantComponent) \
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override; \
	enum class ENetFields_Private : uint16 \
	{ \
		NETFIELD_REP_START=(uint16)((int32)Super::ENetFields_Private::NETFIELD_REP_END + (int32)1), \
		ConversationsActive=NETFIELD_REP_START, \
		NETFIELD_REP_END=ConversationsActive	}; \
	NO_API virtual void ValidateGeneratedRepEnums(const TArray<struct FRepRecord>& ClassReps) const override;


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationParticipantComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationParticipantComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationParticipantComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationParticipantComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationParticipantComponent(UConversationParticipantComponent&&); \
	NO_API UConversationParticipantComponent(const UConversationParticipantComponent&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationParticipantComponent(UConversationParticipantComponent&&); \
	NO_API UConversationParticipantComponent(const UConversationParticipantComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationParticipantComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationParticipantComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConversationParticipantComponent)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ConversationsActive() { return STRUCT_OFFSET(UConversationParticipantComponent, ConversationsActive); } \
	FORCEINLINE static uint32 __PPO__Auth_CurrentConversation() { return STRUCT_OFFSET(UConversationParticipantComponent, Auth_CurrentConversation); } \
	FORCEINLINE static uint32 __PPO__Auth_Conversations() { return STRUCT_OFFSET(UConversationParticipantComponent, Auth_Conversations); }


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_20_PROLOG \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationParticipantComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationParticipantComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
