// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FConversationContext;
struct FClientConversationOptionEntry;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationChoiceNode_generated_h
#error "ConversationChoiceNode.generated.h already included, missing '#pragma once' in ConversationChoiceNode.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationChoiceNode_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_RPC_WRAPPERS \
	virtual void FillChoice_Implementation(FConversationContext const& Context, FClientConversationOptionEntry& ChoiceEntry) const; \
 \
	DECLARE_FUNCTION(execFillChoice);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void FillChoice_Implementation(FConversationContext const& Context, FClientConversationOptionEntry& ChoiceEntry) const; \
 \
	DECLARE_FUNCTION(execFillChoice);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_EVENT_PARMS \
	struct ConversationChoiceNode_eventFillChoice_Parms \
	{ \
		FConversationContext Context; \
		FClientConversationOptionEntry ChoiceEntry; \
	};


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationChoiceNode(); \
	friend struct Z_Construct_UClass_UConversationChoiceNode_Statics; \
public: \
	DECLARE_CLASS(UConversationChoiceNode, UConversationSubNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationChoiceNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUConversationChoiceNode(); \
	friend struct Z_Construct_UClass_UConversationChoiceNode_Statics; \
public: \
	DECLARE_CLASS(UConversationChoiceNode, UConversationSubNode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationChoiceNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationChoiceNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationChoiceNode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationChoiceNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationChoiceNode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationChoiceNode(UConversationChoiceNode&&); \
	NO_API UConversationChoiceNode(const UConversationChoiceNode&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationChoiceNode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationChoiceNode(UConversationChoiceNode&&); \
	NO_API UConversationChoiceNode(const UConversationChoiceNode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationChoiceNode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationChoiceNode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationChoiceNode)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_15_PROLOG \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_EVENT_PARMS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationChoiceNode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationChoiceNode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
