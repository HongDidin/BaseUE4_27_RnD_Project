// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UConversationParticipantComponent;
struct FConversationContext;
struct FGameplayTag;
struct FConversationTaskResult;
struct FClientConversationMessage;
struct FAdvanceConversationRequest;
struct FConversationNodeHandle;
class UConversationInstance;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationContext_generated_h
#error "ConversationContext.generated.h already included, missing '#pragma once' in ConversationContext.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationContext_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_165_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationContext_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__ConversationRegistry() { return STRUCT_OFFSET(FConversationContext, ConversationRegistry); } \
	FORCEINLINE static uint32 __PPO__ActiveConversation() { return STRUCT_OFFSET(FConversationContext, ActiveConversation); } \
	FORCEINLINE static uint32 __PPO__ClientParticipant() { return STRUCT_OFFSET(FConversationContext, ClientParticipant); } \
	FORCEINLINE static uint32 __PPO__TaskBeingConsidered() { return STRUCT_OFFSET(FConversationContext, TaskBeingConsidered); } \
	FORCEINLINE static uint32 __PPO__ReturnScopeStack() { return STRUCT_OFFSET(FConversationContext, ReturnScopeStack); } \
	FORCEINLINE static uint32 __PPO__bServer() { return STRUCT_OFFSET(FConversationContext, bServer); }


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationContext>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_71_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationTaskResult_Statics; \
	static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__Type() { return STRUCT_OFFSET(FConversationTaskResult, Type); } \
	FORCEINLINE static uint32 __PPO__AdvanceToChoice() { return STRUCT_OFFSET(FConversationTaskResult, AdvanceToChoice); } \
	FORCEINLINE static uint32 __PPO__Message() { return STRUCT_OFFSET(FConversationTaskResult, Message); }


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationTaskResult>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execFindConversationComponent); \
	DECLARE_FUNCTION(execGetConversationParticipantActor); \
	DECLARE_FUNCTION(execGetConversationParticipant); \
	DECLARE_FUNCTION(execReturnToConversationStart); \
	DECLARE_FUNCTION(execReturnToCurrentClientChoice); \
	DECLARE_FUNCTION(execReturnToLastClientChoice); \
	DECLARE_FUNCTION(execPauseConversationAndSendClientChoices); \
	DECLARE_FUNCTION(execAdvanceConversationWithChoice); \
	DECLARE_FUNCTION(execAdvanceConversation); \
	DECLARE_FUNCTION(execMakeConversationParticipant); \
	DECLARE_FUNCTION(execGetCurrentConversationNodeHandle); \
	DECLARE_FUNCTION(execGetConversationInstance);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFindConversationComponent); \
	DECLARE_FUNCTION(execGetConversationParticipantActor); \
	DECLARE_FUNCTION(execGetConversationParticipant); \
	DECLARE_FUNCTION(execReturnToConversationStart); \
	DECLARE_FUNCTION(execReturnToCurrentClientChoice); \
	DECLARE_FUNCTION(execReturnToLastClientChoice); \
	DECLARE_FUNCTION(execPauseConversationAndSendClientChoices); \
	DECLARE_FUNCTION(execAdvanceConversationWithChoice); \
	DECLARE_FUNCTION(execAdvanceConversation); \
	DECLARE_FUNCTION(execMakeConversationParticipant); \
	DECLARE_FUNCTION(execGetCurrentConversationNodeHandle); \
	DECLARE_FUNCTION(execGetConversationInstance);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationContextHelpers(); \
	friend struct Z_Construct_UClass_UConversationContextHelpers_Statics; \
public: \
	DECLARE_CLASS(UConversationContextHelpers, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationContextHelpers)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_INCLASS \
private: \
	static void StaticRegisterNativesUConversationContextHelpers(); \
	friend struct Z_Construct_UClass_UConversationContextHelpers_Statics; \
public: \
	DECLARE_CLASS(UConversationContextHelpers, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationContextHelpers)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationContextHelpers(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationContextHelpers) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationContextHelpers); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationContextHelpers); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationContextHelpers(UConversationContextHelpers&&); \
	NO_API UConversationContextHelpers(const UConversationContextHelpers&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationContextHelpers(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationContextHelpers(UConversationContextHelpers&&); \
	NO_API UConversationContextHelpers(const UConversationContextHelpers&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationContextHelpers); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationContextHelpers); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationContextHelpers)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_237_PROLOG
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h_240_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationContextHelpers>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationContext_h


#define FOREACH_ENUM_ECONVERSATIONTASKRESULTTYPE(op) \
	op(EConversationTaskResultType::Invalid) \
	op(EConversationTaskResultType::AbortConversation) \
	op(EConversationTaskResultType::AdvanceConversation) \
	op(EConversationTaskResultType::AdvanceConversationWithChoice) \
	op(EConversationTaskResultType::PauseConversationAndSendClientChoices) \
	op(EConversationTaskResultType::ReturnToLastClientChoice) \
	op(EConversationTaskResultType::ReturnToCurrentClientChoice) \
	op(EConversationTaskResultType::ReturnToConversationStart) 

enum class EConversationTaskResultType : uint8;
template<> COMMONCONVERSATIONRUNTIME_API UEnum* StaticEnum<EConversationTaskResultType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
