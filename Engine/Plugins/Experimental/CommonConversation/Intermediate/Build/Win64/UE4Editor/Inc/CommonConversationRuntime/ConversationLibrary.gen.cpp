// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationLibrary() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationLibrary_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationInstance_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UConversationLibrary::execStartConversation)
	{
		P_GET_STRUCT(FGameplayTag,Z_Param_ConversationEntryTag);
		P_GET_OBJECT(AActor,Z_Param_Instigator);
		P_GET_STRUCT(FGameplayTag,Z_Param_InstigatorTag);
		P_GET_OBJECT(AActor,Z_Param_Target);
		P_GET_STRUCT(FGameplayTag,Z_Param_TargetTag);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UConversationInstance**)Z_Param__Result=UConversationLibrary::StartConversation(Z_Param_ConversationEntryTag,Z_Param_Instigator,Z_Param_InstigatorTag,Z_Param_Target,Z_Param_TargetTag);
		P_NATIVE_END;
	}
	void UConversationLibrary::StaticRegisterNativesUConversationLibrary()
	{
		UClass* Class = UConversationLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "StartConversation", &UConversationLibrary::execStartConversation },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics
	{
		struct ConversationLibrary_eventStartConversation_Parms
		{
			FGameplayTag ConversationEntryTag;
			AActor* Instigator;
			FGameplayTag InstigatorTag;
			AActor* Target;
			FGameplayTag TargetTag;
			UConversationInstance* ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConversationEntryTag;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Instigator;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InstigatorTag;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Target;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetTag;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_ConversationEntryTag = { "ConversationEntryTag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationLibrary_eventStartConversation_Parms, ConversationEntryTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_Instigator = { "Instigator", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationLibrary_eventStartConversation_Parms, Instigator), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_InstigatorTag = { "InstigatorTag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationLibrary_eventStartConversation_Parms, InstigatorTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_Target = { "Target", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationLibrary_eventStartConversation_Parms, Target), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_TargetTag = { "TargetTag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationLibrary_eventStartConversation_Parms, TargetTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ConversationLibrary_eventStartConversation_Parms, ReturnValue), Z_Construct_UClass_UConversationInstance_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_ConversationEntryTag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_Instigator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_InstigatorTag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_Target,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_TargetTag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::Function_MetaDataParams[] = {
		{ "Category", "Conversation" },
		{ "DeterminesOutputType", "ConversationType" },
		{ "ModuleRelativePath", "Public/ConversationLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UConversationLibrary, nullptr, "StartConversation", nullptr, nullptr, sizeof(ConversationLibrary_eventStartConversation_Parms), Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UConversationLibrary_StartConversation()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UConversationLibrary_StartConversation_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UConversationLibrary_NoRegister()
	{
		return UConversationLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UConversationLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UConversationLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UConversationLibrary_StartConversation, "StartConversation" }, // 3890366019
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConversationLibrary.h" },
		{ "ModuleRelativePath", "Public/ConversationLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationLibrary_Statics::ClassParams = {
		&UConversationLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationLibrary, 1622080192);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationLibrary>()
	{
		return UConversationLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationLibrary(Z_Construct_UClass_UConversationLibrary, &UConversationLibrary::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
