// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONCONVERSATIONGRAPH_ConversationGraphSchema_generated_h
#error "ConversationGraphSchema.generated.h already included, missing '#pragma once' in ConversationGraphSchema.h"
#endif
#define COMMONCONVERSATIONGRAPH_ConversationGraphSchema_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_25_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationGraphSchemaAction_AutoArrange_Statics; \
	COMMONCONVERSATIONGRAPH_API static class UScriptStruct* StaticStruct(); \
	typedef FEdGraphSchemaAction Super;


template<> COMMONCONVERSATIONGRAPH_API UScriptStruct* StaticStruct<struct FConversationGraphSchemaAction_AutoArrange>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationGraphSchema(); \
	friend struct Z_Construct_UClass_UConversationGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UConversationGraphSchema, UAIGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationGraph"), NO_API) \
	DECLARE_SERIALIZER(UConversationGraphSchema)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUConversationGraphSchema(); \
	friend struct Z_Construct_UClass_UConversationGraphSchema_Statics; \
public: \
	DECLARE_CLASS(UConversationGraphSchema, UAIGraphSchema, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationGraph"), NO_API) \
	DECLARE_SERIALIZER(UConversationGraphSchema)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationGraphSchema) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationGraphSchema); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationGraphSchema(UConversationGraphSchema&&); \
	NO_API UConversationGraphSchema(const UConversationGraphSchema&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationGraphSchema(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationGraphSchema(UConversationGraphSchema&&); \
	NO_API UConversationGraphSchema(const UConversationGraphSchema&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationGraphSchema); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationGraphSchema); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationGraphSchema)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_42_PROLOG
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h_45_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ConversationGraphSchema."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONGRAPH_API UClass* StaticClass<class UConversationGraphSchema>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationGraph_Public_ConversationGraphSchema_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
