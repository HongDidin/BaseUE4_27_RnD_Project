// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONCONVERSATIONRUNTIME_ConversationDatabase_generated_h
#error "ConversationDatabase.generated.h already included, missing '#pragma once' in ConversationDatabase.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationDatabase_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_45_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCommonDialogueBankParticipant_Statics; \
	COMMONCONVERSATIONRUNTIME_API static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FCommonDialogueBankParticipant>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_25_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationEntryList_Statics; \
	COMMONCONVERSATIONRUNTIME_API static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationEntryList>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationDatabase(); \
	friend struct Z_Construct_UClass_UConversationDatabase_Statics; \
public: \
	DECLARE_CLASS(UConversationDatabase, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationDatabase)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_INCLASS \
private: \
	static void StaticRegisterNativesUConversationDatabase(); \
	friend struct Z_Construct_UClass_UConversationDatabase_Statics; \
public: \
	DECLARE_CLASS(UConversationDatabase, UPrimaryDataAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationDatabase)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationDatabase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationDatabase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationDatabase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationDatabase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationDatabase(UConversationDatabase&&); \
	NO_API UConversationDatabase(const UConversationDatabase&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationDatabase(UConversationDatabase&&); \
	NO_API UConversationDatabase(const UConversationDatabase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationDatabase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationDatabase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationDatabase)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CompilerVersion() { return STRUCT_OFFSET(UConversationDatabase, CompilerVersion); } \
	FORCEINLINE static uint32 __PPO__ReachableNodeMap() { return STRUCT_OFFSET(UConversationDatabase, ReachableNodeMap); } \
	FORCEINLINE static uint32 __PPO__EntryTags() { return STRUCT_OFFSET(UConversationDatabase, EntryTags); } \
	FORCEINLINE static uint32 __PPO__ExitTags() { return STRUCT_OFFSET(UConversationDatabase, ExitTags); } \
	FORCEINLINE static uint32 __PPO__InternalNodeIds() { return STRUCT_OFFSET(UConversationDatabase, InternalNodeIds); } \
	FORCEINLINE static uint32 __PPO__LinkedToNodeIds() { return STRUCT_OFFSET(UConversationDatabase, LinkedToNodeIds); } \
	FORCEINLINE static uint32 __PPO__Speakers() { return STRUCT_OFFSET(UConversationDatabase, Speakers); }


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_67_PROLOG
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h_70_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationDatabase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationDatabase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
