// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FGameplayTag;
class AActor;
class UConversationInstance;
#ifdef COMMONCONVERSATIONRUNTIME_ConversationLibrary_generated_h
#error "ConversationLibrary.generated.h already included, missing '#pragma once' in ConversationLibrary.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationLibrary_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_SPARSE_DATA
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execStartConversation);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execStartConversation);


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUConversationLibrary(); \
	friend struct Z_Construct_UClass_UConversationLibrary_Statics; \
public: \
	DECLARE_CLASS(UConversationLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationLibrary)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUConversationLibrary(); \
	friend struct Z_Construct_UClass_UConversationLibrary_Statics; \
public: \
	DECLARE_CLASS(UConversationLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CommonConversationRuntime"), NO_API) \
	DECLARE_SERIALIZER(UConversationLibrary)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UConversationLibrary(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UConversationLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationLibrary(UConversationLibrary&&); \
	NO_API UConversationLibrary(const UConversationLibrary&); \
public:


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UConversationLibrary(UConversationLibrary&&); \
	NO_API UConversationLibrary(const UConversationLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UConversationLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UConversationLibrary); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UConversationLibrary)


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_16_PROLOG
#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_INCLASS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<class UConversationLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
