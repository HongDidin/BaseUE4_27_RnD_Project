// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef COMMONCONVERSATIONRUNTIME_ConversationTypes_generated_h
#error "ConversationTypes.generated.h already included, missing '#pragma once' in ConversationTypes.h"
#endif
#define COMMONCONVERSATIONRUNTIME_ConversationTypes_generated_h

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_445_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FClientConversationMessagePayload_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FClientConversationMessagePayload>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_422_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FClientConversationMessage_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FClientConversationMessage>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_385_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationBranchPoint_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationBranchPoint>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_302_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FClientConversationOptionEntry_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FClientConversationOptionEntry>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_260_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FAdvanceConversationRequest_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FAdvanceConversationRequest>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_227_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationChoiceReference_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationChoiceReference>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_196_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationNodeParameterPair_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationNodeParameterPair>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_176_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationParticipants_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationParticipants>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_153_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationParticipantEntry_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationParticipantEntry>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_43_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationChoiceDataHandle_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationChoiceDataHandle>();

#define Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConversationChoiceData_Statics; \
	static class UScriptStruct* StaticStruct();


template<> COMMONCONVERSATIONRUNTIME_API UScriptStruct* StaticStruct<struct FConversationChoiceData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CommonConversation_Source_CommonConversationRuntime_Public_ConversationTypes_h


#define FOREACH_ENUM_ECONVERSATIONCHOICETYPE(op) \
	op(EConversationChoiceType::ServerOnly) \
	op(EConversationChoiceType::UserChoiceAvailable) \
	op(EConversationChoiceType::UserChoiceUnavailable) 

enum class EConversationChoiceType : uint8;
template<> COMMONCONVERSATIONRUNTIME_API UEnum* StaticEnum<EConversationChoiceType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
