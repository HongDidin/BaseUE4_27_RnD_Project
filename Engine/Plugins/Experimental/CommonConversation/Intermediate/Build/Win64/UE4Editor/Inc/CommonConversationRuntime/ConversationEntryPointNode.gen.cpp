// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CommonConversationRuntime/Public/ConversationEntryPointNode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConversationEntryPointNode() {}
// Cross Module References
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationEntryPointNode_NoRegister();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationEntryPointNode();
	COMMONCONVERSATIONRUNTIME_API UClass* Z_Construct_UClass_UConversationNodeWithLinks();
	UPackage* Z_Construct_UPackage__Script_CommonConversationRuntime();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
// End Cross Module References
	void UConversationEntryPointNode::StaticRegisterNativesUConversationEntryPointNode()
	{
	}
	UClass* Z_Construct_UClass_UConversationEntryPointNode_NoRegister()
	{
		return UConversationEntryPointNode::StaticClass();
	}
	struct Z_Construct_UClass_UConversationEntryPointNode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EntryTag_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EntryTag;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConversationEntryPointNode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UConversationNodeWithLinks,
		(UObject* (*)())Z_Construct_UPackage__Script_CommonConversationRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationEntryPointNode_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Entry Point" },
		{ "IncludePath", "ConversationEntryPointNode.h" },
		{ "ModuleRelativePath", "Public/ConversationEntryPointNode.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConversationEntryPointNode_Statics::NewProp_EntryTag_MetaData[] = {
		{ "Category", "Conversation" },
		{ "ModuleRelativePath", "Public/ConversationEntryPointNode.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UConversationEntryPointNode_Statics::NewProp_EntryTag = { "EntryTag", nullptr, (EPropertyFlags)0x0010000000000011, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConversationEntryPointNode, EntryTag), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UClass_UConversationEntryPointNode_Statics::NewProp_EntryTag_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationEntryPointNode_Statics::NewProp_EntryTag_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConversationEntryPointNode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConversationEntryPointNode_Statics::NewProp_EntryTag,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConversationEntryPointNode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConversationEntryPointNode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConversationEntryPointNode_Statics::ClassParams = {
		&UConversationEntryPointNode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConversationEntryPointNode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConversationEntryPointNode_Statics::PropPointers),
		0,
		0x001100A0u,
		METADATA_PARAMS(Z_Construct_UClass_UConversationEntryPointNode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConversationEntryPointNode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConversationEntryPointNode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConversationEntryPointNode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConversationEntryPointNode, 3357242616);
	template<> COMMONCONVERSATIONRUNTIME_API UClass* StaticClass<UConversationEntryPointNode>()
	{
		return UConversationEntryPointNode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConversationEntryPointNode(Z_Construct_UClass_UConversationEntryPointNode, &UConversationEntryPointNode::StaticClass, TEXT("/Script/CommonConversationRuntime"), TEXT("UConversationEntryPointNode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConversationEntryPointNode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
