// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMAGEPLATE_ImagePlateFrustumComponent_generated_h
#error "ImagePlateFrustumComponent.generated.h already included, missing '#pragma once' in ImagePlateFrustumComponent.h"
#endif
#define IMAGEPLATE_ImagePlateFrustumComponent_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImagePlateFrustumComponent(); \
	friend struct Z_Construct_UClass_UImagePlateFrustumComponent_Statics; \
public: \
	DECLARE_CLASS(UImagePlateFrustumComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), IMAGEPLATE_API) \
	DECLARE_SERIALIZER(UImagePlateFrustumComponent)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUImagePlateFrustumComponent(); \
	friend struct Z_Construct_UClass_UImagePlateFrustumComponent_Statics; \
public: \
	DECLARE_CLASS(UImagePlateFrustumComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), IMAGEPLATE_API) \
	DECLARE_SERIALIZER(UImagePlateFrustumComponent)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	IMAGEPLATE_API UImagePlateFrustumComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateFrustumComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATE_API, UImagePlateFrustumComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateFrustumComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATE_API UImagePlateFrustumComponent(UImagePlateFrustumComponent&&); \
	IMAGEPLATE_API UImagePlateFrustumComponent(const UImagePlateFrustumComponent&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATE_API UImagePlateFrustumComponent(UImagePlateFrustumComponent&&); \
	IMAGEPLATE_API UImagePlateFrustumComponent(const UImagePlateFrustumComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATE_API, UImagePlateFrustumComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateFrustumComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateFrustumComponent)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_14_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATE_API UClass* StaticClass<class UImagePlateFrustumComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_ImagePlateFrustumComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
