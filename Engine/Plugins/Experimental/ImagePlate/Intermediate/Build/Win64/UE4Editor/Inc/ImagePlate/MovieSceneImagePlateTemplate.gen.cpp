// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImagePlate/Private/MovieSceneImagePlateTemplate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneImagePlateTemplate() {}
// Cross Module References
	IMAGEPLATE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate();
	UPackage* Z_Construct_UPackage__Script_ImagePlate();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneEvalTemplate();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieScenePropertySectionData();
	IMAGEPLATE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameNumber();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateFileSequence_NoRegister();
// End Cross Module References

static_assert(std::is_polymorphic<FMovieSceneImagePlateSectionTemplate>() == std::is_polymorphic<FMovieSceneEvalTemplate>(), "USTRUCT FMovieSceneImagePlateSectionTemplate cannot be polymorphic unless super FMovieSceneEvalTemplate is polymorphic");

class UScriptStruct* FMovieSceneImagePlateSectionTemplate::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern IMAGEPLATE_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate, Z_Construct_UPackage__Script_ImagePlate(), TEXT("MovieSceneImagePlateSectionTemplate"), sizeof(FMovieSceneImagePlateSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Hash());
	}
	return Singleton;
}
template<> IMAGEPLATE_API UScriptStruct* StaticStruct<FMovieSceneImagePlateSectionTemplate>()
{
	return FMovieSceneImagePlateSectionTemplate::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneImagePlateSectionTemplate(FMovieSceneImagePlateSectionTemplate::StaticStruct, TEXT("/Script/ImagePlate"), TEXT("MovieSceneImagePlateSectionTemplate"), false, nullptr, nullptr);
static struct FScriptStruct_ImagePlate_StaticRegisterNativesFMovieSceneImagePlateSectionTemplate
{
	FScriptStruct_ImagePlate_StaticRegisterNativesFMovieSceneImagePlateSectionTemplate()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneImagePlateSectionTemplate>(FName(TEXT("MovieSceneImagePlateSectionTemplate")));
	}
} ScriptStruct_ImagePlate_StaticRegisterNativesFMovieSceneImagePlateSectionTemplate;
	struct Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PropertyData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/MovieSceneImagePlateTemplate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneImagePlateSectionTemplate>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_PropertyData_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneImagePlateTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_PropertyData = { "PropertyData", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneImagePlateSectionTemplate, PropertyData), Z_Construct_UScriptStruct_FMovieScenePropertySectionData, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_PropertyData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_PropertyData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_Params_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneImagePlateTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneImagePlateSectionTemplate, Params), Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_PropertyData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::NewProp_Params,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlate,
		Z_Construct_UScriptStruct_FMovieSceneEvalTemplate,
		&NewStructOps,
		"MovieSceneImagePlateSectionTemplate",
		sizeof(FMovieSceneImagePlateSectionTemplate),
		alignof(FMovieSceneImagePlateSectionTemplate),
		Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ImagePlate();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneImagePlateSectionTemplate"), sizeof(FMovieSceneImagePlateSectionTemplate), Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Hash() { return 2854522904U; }
class UScriptStruct* FMovieSceneImagePlateSectionParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern IMAGEPLATE_API uint32 Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams, Z_Construct_UPackage__Script_ImagePlate(), TEXT("MovieSceneImagePlateSectionParams"), sizeof(FMovieSceneImagePlateSectionParams), Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Hash());
	}
	return Singleton;
}
template<> IMAGEPLATE_API UScriptStruct* StaticStruct<FMovieSceneImagePlateSectionParams>()
{
	return FMovieSceneImagePlateSectionParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMovieSceneImagePlateSectionParams(FMovieSceneImagePlateSectionParams::StaticStruct, TEXT("/Script/ImagePlate"), TEXT("MovieSceneImagePlateSectionParams"), false, nullptr, nullptr);
static struct FScriptStruct_ImagePlate_StaticRegisterNativesFMovieSceneImagePlateSectionParams
{
	FScriptStruct_ImagePlate_StaticRegisterNativesFMovieSceneImagePlateSectionParams()
	{
		UScriptStruct::DeferCppStructOps<FMovieSceneImagePlateSectionParams>(FName(TEXT("MovieSceneImagePlateSectionParams")));
	}
} ScriptStruct_ImagePlate_StaticRegisterNativesFMovieSceneImagePlateSectionParams;
	struct Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SectionStartTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SectionStartTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FileSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReuseExistingTexture_MetaData[];
#endif
		static void NewProp_bReuseExistingTexture_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReuseExistingTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/MovieSceneImagePlateTemplate.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMovieSceneImagePlateSectionParams>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_SectionStartTime_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneImagePlateTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_SectionStartTime = { "SectionStartTime", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneImagePlateSectionParams, SectionStartTime), Z_Construct_UScriptStruct_FFrameNumber, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_SectionStartTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_SectionStartTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_FileSequence_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneImagePlateTemplate.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_FileSequence = { "FileSequence", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMovieSceneImagePlateSectionParams, FileSequence), Z_Construct_UClass_UImagePlateFileSequence_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_FileSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_FileSequence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_bReuseExistingTexture_MetaData[] = {
		{ "ModuleRelativePath", "Private/MovieSceneImagePlateTemplate.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_bReuseExistingTexture_SetBit(void* Obj)
	{
		((FMovieSceneImagePlateSectionParams*)Obj)->bReuseExistingTexture = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_bReuseExistingTexture = { "bReuseExistingTexture", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMovieSceneImagePlateSectionParams), &Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_bReuseExistingTexture_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_bReuseExistingTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_bReuseExistingTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_SectionStartTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_FileSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::NewProp_bReuseExistingTexture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlate,
		nullptr,
		&NewStructOps,
		"MovieSceneImagePlateSectionParams",
		sizeof(FMovieSceneImagePlateSectionParams),
		alignof(FMovieSceneImagePlateSectionParams),
		Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ImagePlate();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MovieSceneImagePlateSectionParams"), sizeof(FMovieSceneImagePlateSectionParams), Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Hash() { return 4021492321U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
