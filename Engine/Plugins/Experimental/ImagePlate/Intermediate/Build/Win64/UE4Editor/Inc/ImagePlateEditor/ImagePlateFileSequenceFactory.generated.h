// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMAGEPLATEEDITOR_ImagePlateFileSequenceFactory_generated_h
#error "ImagePlateFileSequenceFactory.generated.h already included, missing '#pragma once' in ImagePlateFileSequenceFactory.h"
#endif
#define IMAGEPLATEEDITOR_ImagePlateFileSequenceFactory_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImagePlateFileSequenceFactory(); \
	friend struct Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics; \
public: \
	DECLARE_CLASS(UImagePlateFileSequenceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlateEditor"), IMAGEPLATEEDITOR_API) \
	DECLARE_SERIALIZER(UImagePlateFileSequenceFactory)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUImagePlateFileSequenceFactory(); \
	friend struct Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics; \
public: \
	DECLARE_CLASS(UImagePlateFileSequenceFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlateEditor"), IMAGEPLATEEDITOR_API) \
	DECLARE_SERIALIZER(UImagePlateFileSequenceFactory)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	IMAGEPLATEEDITOR_API UImagePlateFileSequenceFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateFileSequenceFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATEEDITOR_API, UImagePlateFileSequenceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateFileSequenceFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATEEDITOR_API UImagePlateFileSequenceFactory(UImagePlateFileSequenceFactory&&); \
	IMAGEPLATEEDITOR_API UImagePlateFileSequenceFactory(const UImagePlateFileSequenceFactory&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATEEDITOR_API UImagePlateFileSequenceFactory(UImagePlateFileSequenceFactory&&); \
	IMAGEPLATEEDITOR_API UImagePlateFileSequenceFactory(const UImagePlateFileSequenceFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATEEDITOR_API, UImagePlateFileSequenceFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateFileSequenceFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateFileSequenceFactory)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_10_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATEEDITOR_API UClass* StaticClass<class UImagePlateFileSequenceFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlateEditor_Private_ImagePlateFileSequenceFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
