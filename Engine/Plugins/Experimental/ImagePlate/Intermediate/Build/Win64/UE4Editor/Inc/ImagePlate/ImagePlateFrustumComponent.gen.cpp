// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImagePlate/Private/ImagePlateFrustumComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImagePlateFrustumComponent() {}
// Cross Module References
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateFrustumComponent_NoRegister();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateFrustumComponent();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent();
	UPackage* Z_Construct_UPackage__Script_ImagePlate();
// End Cross Module References
	void UImagePlateFrustumComponent::StaticRegisterNativesUImagePlateFrustumComponent()
	{
	}
	UClass* Z_Construct_UClass_UImagePlateFrustumComponent_NoRegister()
	{
		return UImagePlateFrustumComponent::StaticClass();
	}
	struct Z_Construct_UClass_UImagePlateFrustumComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImagePlateFrustumComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPrimitiveComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateFrustumComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Rendering" },
		{ "Comment", "/** \n * A 2d material that will be rendered always facing the camera.\n */" },
		{ "HideCategories", "Object Activation Collision Components|Activation Physics Mobility VirtualTexture Trigger" },
		{ "IncludePath", "ImagePlateFrustumComponent.h" },
		{ "ModuleRelativePath", "Private/ImagePlateFrustumComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "A 2d material that will be rendered always facing the camera." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImagePlateFrustumComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImagePlateFrustumComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImagePlateFrustumComponent_Statics::ClassParams = {
		&UImagePlateFrustumComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00A810A4u,
		METADATA_PARAMS(Z_Construct_UClass_UImagePlateFrustumComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateFrustumComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImagePlateFrustumComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImagePlateFrustumComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImagePlateFrustumComponent, 806508366);
	template<> IMAGEPLATE_API UClass* StaticClass<UImagePlateFrustumComponent>()
	{
		return UImagePlateFrustumComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImagePlateFrustumComponent(Z_Construct_UClass_UImagePlateFrustumComponent, &UImagePlateFrustumComponent::StaticClass, TEXT("/Script/ImagePlate"), TEXT("UImagePlateFrustumComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImagePlateFrustumComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
