// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImagePlate/Public/ImagePlate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImagePlate() {}
// Cross Module References
	IMAGEPLATE_API UClass* Z_Construct_UClass_AImagePlate_NoRegister();
	IMAGEPLATE_API UClass* Z_Construct_UClass_AImagePlate();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_ImagePlate();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateComponent_NoRegister();
// End Cross Module References
	void AImagePlate::StaticRegisterNativesAImagePlate()
	{
	}
	UClass* Z_Construct_UClass_AImagePlate_NoRegister()
	{
		return AImagePlate::StaticClass();
	}
	struct Z_Construct_UClass_AImagePlate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImagePlate_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImagePlate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AImagePlate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImagePlate_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Rendering" },
		{ "HideCategories", "Object Activation Physics Collision Input PhysicsVolume" },
		{ "IncludePath", "ImagePlate.h" },
		{ "ModuleRelativePath", "Public/ImagePlate.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImagePlate_Statics::NewProp_ImagePlate_MetaData[] = {
		{ "Category", "ImagePlate" },
		{ "EditInline", "true" },
		{ "ExposeFunctionCategories", "Mesh,Rendering,Physics,Components|StaticMesh" },
		{ "ModuleRelativePath", "Public/ImagePlate.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AImagePlate_Statics::NewProp_ImagePlate = { "ImagePlate", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AImagePlate, ImagePlate), Z_Construct_UClass_UImagePlateComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AImagePlate_Statics::NewProp_ImagePlate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AImagePlate_Statics::NewProp_ImagePlate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AImagePlate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AImagePlate_Statics::NewProp_ImagePlate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AImagePlate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AImagePlate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AImagePlate_Statics::ClassParams = {
		&AImagePlate::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AImagePlate_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AImagePlate_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AImagePlate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AImagePlate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AImagePlate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AImagePlate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AImagePlate, 1427052201);
	template<> IMAGEPLATE_API UClass* StaticClass<AImagePlate>()
	{
		return AImagePlate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AImagePlate(Z_Construct_UClass_AImagePlate, &AImagePlate::StaticClass, TEXT("/Script/ImagePlate"), TEXT("AImagePlate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AImagePlate);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
