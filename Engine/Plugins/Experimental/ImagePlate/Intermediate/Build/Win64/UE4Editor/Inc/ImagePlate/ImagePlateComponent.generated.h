// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FImagePlateParameters;
#ifdef IMAGEPLATE_ImagePlateComponent_generated_h
#error "ImagePlateComponent.generated.h already included, missing '#pragma once' in ImagePlateComponent.h"
#endif
#define IMAGEPLATE_ImagePlateComponent_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_18_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FImagePlateParameters_Statics; \
	static class UScriptStruct* StaticStruct();


template<> IMAGEPLATE_API UScriptStruct* StaticStruct<struct FImagePlateParameters>();

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnRenderTextureChanged); \
	DECLARE_FUNCTION(execGetPlate); \
	DECLARE_FUNCTION(execSetImagePlate);


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnRenderTextureChanged); \
	DECLARE_FUNCTION(execGetPlate); \
	DECLARE_FUNCTION(execSetImagePlate);


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImagePlateComponent(); \
	friend struct Z_Construct_UClass_UImagePlateComponent_Statics; \
public: \
	DECLARE_CLASS(UImagePlateComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(UImagePlateComponent)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUImagePlateComponent(); \
	friend struct Z_Construct_UClass_UImagePlateComponent_Statics; \
public: \
	DECLARE_CLASS(UImagePlateComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(UImagePlateComponent)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImagePlateComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImagePlateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImagePlateComponent(UImagePlateComponent&&); \
	NO_API UImagePlateComponent(const UImagePlateComponent&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImagePlateComponent(UImagePlateComponent&&); \
	NO_API UImagePlateComponent(const UImagePlateComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImagePlateComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateComponent)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Plate() { return STRUCT_OFFSET(UImagePlateComponent, Plate); }


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_54_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h_59_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATE_API UClass* StaticClass<class UImagePlateComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
