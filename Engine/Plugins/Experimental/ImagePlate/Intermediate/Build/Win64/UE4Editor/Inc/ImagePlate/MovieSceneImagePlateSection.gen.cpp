// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImagePlate/Public/MovieSceneImagePlateSection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneImagePlateSection() {}
// Cross Module References
	IMAGEPLATE_API UClass* Z_Construct_UClass_UMovieSceneImagePlateSection_NoRegister();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UMovieSceneImagePlateSection();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection();
	UPackage* Z_Construct_UPackage__Script_ImagePlate();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateFileSequence_NoRegister();
// End Cross Module References
	void UMovieSceneImagePlateSection::StaticRegisterNativesUMovieSceneImagePlateSection()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneImagePlateSection_NoRegister()
	{
		return UMovieSceneImagePlateSection::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneImagePlateSection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FileSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bReuseExistingTexture_MetaData[];
#endif
		static void NewProp_bReuseExistingTexture_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bReuseExistingTexture;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ThumbnailReferenceOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ThumbnailReferenceOffset;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneSection,
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MovieSceneImagePlateSection.h" },
		{ "ModuleRelativePath", "Public/MovieSceneImagePlateSection.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_FileSequence_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Public/MovieSceneImagePlateSection.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_FileSequence = { "FileSequence", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneImagePlateSection, FileSequence), Z_Construct_UClass_UImagePlateFileSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_FileSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_FileSequence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_bReuseExistingTexture_MetaData[] = {
		{ "Category", "Image Plate" },
		{ "Comment", "/** Specifies whether this section can reuse a texture or render target already specified on the property. When false, a dynamic texture 2D will be created at runtime and assigned to the property where possible. */" },
		{ "ModuleRelativePath", "Public/MovieSceneImagePlateSection.h" },
		{ "ToolTip", "Specifies whether this section can reuse a texture or render target already specified on the property. When false, a dynamic texture 2D will be created at runtime and assigned to the property where possible." },
	};
#endif
	void Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_bReuseExistingTexture_SetBit(void* Obj)
	{
		((UMovieSceneImagePlateSection*)Obj)->bReuseExistingTexture = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_bReuseExistingTexture = { "bReuseExistingTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMovieSceneImagePlateSection), &Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_bReuseExistingTexture_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_bReuseExistingTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_bReuseExistingTexture_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_ThumbnailReferenceOffset_MetaData[] = {
		{ "Comment", "/** The reference frame offset for single thumbnail rendering */" },
		{ "ModuleRelativePath", "Public/MovieSceneImagePlateSection.h" },
		{ "ToolTip", "The reference frame offset for single thumbnail rendering" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_ThumbnailReferenceOffset = { "ThumbnailReferenceOffset", nullptr, (EPropertyFlags)0x0040000800000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMovieSceneImagePlateSection, ThumbnailReferenceOffset), METADATA_PARAMS(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_ThumbnailReferenceOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_ThumbnailReferenceOffset_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_FileSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_bReuseExistingTexture,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::NewProp_ThumbnailReferenceOffset,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneImagePlateSection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::ClassParams = {
		&UMovieSceneImagePlateSection::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::PropPointers),
		0,
		0x002800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneImagePlateSection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneImagePlateSection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneImagePlateSection, 823990795);
	template<> IMAGEPLATE_API UClass* StaticClass<UMovieSceneImagePlateSection>()
	{
		return UMovieSceneImagePlateSection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneImagePlateSection(Z_Construct_UClass_UMovieSceneImagePlateSection, &UMovieSceneImagePlateSection::StaticClass, TEXT("/Script/ImagePlate"), TEXT("UMovieSceneImagePlateSection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneImagePlateSection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
