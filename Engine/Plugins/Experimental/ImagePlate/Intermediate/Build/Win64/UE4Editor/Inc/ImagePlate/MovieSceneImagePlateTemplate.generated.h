// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMAGEPLATE_MovieSceneImagePlateTemplate_generated_h
#error "MovieSceneImagePlateTemplate.generated.h already included, missing '#pragma once' in MovieSceneImagePlateTemplate.h"
#endif
#define IMAGEPLATE_MovieSceneImagePlateTemplate_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_MovieSceneImagePlateTemplate_h_34_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionTemplate_Statics; \
	IMAGEPLATE_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__PropertyData() { return STRUCT_OFFSET(FMovieSceneImagePlateSectionTemplate, PropertyData); } \
	FORCEINLINE static uint32 __PPO__Params() { return STRUCT_OFFSET(FMovieSceneImagePlateSectionTemplate, Params); } \
	typedef FMovieSceneEvalTemplate Super;


template<> IMAGEPLATE_API UScriptStruct* StaticStruct<struct FMovieSceneImagePlateSectionTemplate>();

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_MovieSceneImagePlateTemplate_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMovieSceneImagePlateSectionParams_Statics; \
	IMAGEPLATE_API static class UScriptStruct* StaticStruct();


template<> IMAGEPLATE_API UScriptStruct* StaticStruct<struct FMovieSceneImagePlateSectionParams>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Private_MovieSceneImagePlateTemplate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
