// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImagePlate/Public/ImagePlateFileSequence.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImagePlateFileSequence() {}
// Cross Module References
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateSettings_NoRegister();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_ImagePlate();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateFileSequence_NoRegister();
	IMAGEPLATE_API UClass* Z_Construct_UClass_UImagePlateFileSequence();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UImagePlateSettings::StaticRegisterNativesUImagePlateSettings()
	{
	}
	UClass* Z_Construct_UClass_UImagePlateSettings_NoRegister()
	{
		return UImagePlateSettings::StaticClass();
	}
	struct Z_Construct_UClass_UImagePlateSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProxyName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ProxyName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImagePlateSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Implements the settings for the ImagePlate plugin.\n*/" },
		{ "IncludePath", "ImagePlateFileSequence.h" },
		{ "ModuleRelativePath", "Public/ImagePlateFileSequence.h" },
		{ "ToolTip", "Implements the settings for the ImagePlate plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateSettings_Statics::NewProp_ProxyName_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "/** Specifies a sub-directory to append to any image plate file sequences */" },
		{ "ModuleRelativePath", "Public/ImagePlateFileSequence.h" },
		{ "ToolTip", "Specifies a sub-directory to append to any image plate file sequences" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UImagePlateSettings_Statics::NewProp_ProxyName = { "ProxyName", nullptr, (EPropertyFlags)0x0010000000044001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImagePlateSettings, ProxyName), METADATA_PARAMS(Z_Construct_UClass_UImagePlateSettings_Statics::NewProp_ProxyName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateSettings_Statics::NewProp_ProxyName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UImagePlateSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImagePlateSettings_Statics::NewProp_ProxyName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImagePlateSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImagePlateSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImagePlateSettings_Statics::ClassParams = {
		&UImagePlateSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UImagePlateSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UImagePlateSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImagePlateSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImagePlateSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImagePlateSettings, 3512604030);
	template<> IMAGEPLATE_API UClass* StaticClass<UImagePlateSettings>()
	{
		return UImagePlateSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImagePlateSettings(Z_Construct_UClass_UImagePlateSettings, &UImagePlateSettings::StaticClass, TEXT("/Script/ImagePlate"), TEXT("UImagePlateSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImagePlateSettings);
	void UImagePlateFileSequence::StaticRegisterNativesUImagePlateFileSequence()
	{
	}
	UClass* Z_Construct_UClass_UImagePlateFileSequence_NoRegister()
	{
		return UImagePlateFileSequence::StaticClass();
	}
	struct Z_Construct_UClass_UImagePlateFileSequence_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SequencePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SequencePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileWildcard_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileWildcard;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Framerate_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Framerate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImagePlateFileSequence_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateFileSequence_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ImagePlateFileSequence.h" },
		{ "ModuleRelativePath", "Public/ImagePlateFileSequence.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_SequencePath_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Path to the directory in which the image sequence resides */" },
		{ "ContentDir", "" },
		{ "ModuleRelativePath", "Public/ImagePlateFileSequence.h" },
		{ "ToolTip", "Path to the directory in which the image sequence resides" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_SequencePath = { "SequencePath", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImagePlateFileSequence, SequencePath), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_SequencePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_SequencePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_FileWildcard_MetaData[] = {
		{ "Category", "General" },
		{ "Comment", "/** Wildcard used to find images within the directory (ie *.exr) */" },
		{ "ModuleRelativePath", "Public/ImagePlateFileSequence.h" },
		{ "ToolTip", "Wildcard used to find images within the directory (ie *.exr)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_FileWildcard = { "FileWildcard", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImagePlateFileSequence, FileWildcard), METADATA_PARAMS(Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_FileWildcard_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_FileWildcard_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_Framerate_MetaData[] = {
		{ "Category", "General" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Framerate at which to display the images */" },
		{ "ModuleRelativePath", "Public/ImagePlateFileSequence.h" },
		{ "ToolTip", "Framerate at which to display the images" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_Framerate = { "Framerate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UImagePlateFileSequence, Framerate), METADATA_PARAMS(Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_Framerate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_Framerate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UImagePlateFileSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_SequencePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_FileWildcard,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UImagePlateFileSequence_Statics::NewProp_Framerate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImagePlateFileSequence_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImagePlateFileSequence>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImagePlateFileSequence_Statics::ClassParams = {
		&UImagePlateFileSequence::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UImagePlateFileSequence_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateFileSequence_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UImagePlateFileSequence_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateFileSequence_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImagePlateFileSequence()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImagePlateFileSequence_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImagePlateFileSequence, 687818171);
	template<> IMAGEPLATE_API UClass* StaticClass<UImagePlateFileSequence>()
	{
		return UImagePlateFileSequence::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImagePlateFileSequence(Z_Construct_UClass_UImagePlateFileSequence, &UImagePlateFileSequence::StaticClass, TEXT("/Script/ImagePlate"), TEXT("UImagePlateFileSequence"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImagePlateFileSequence);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
