// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMAGEPLATE_ImagePlate_generated_h
#error "ImagePlate.generated.h already included, missing '#pragma once' in ImagePlate.h"
#endif
#define IMAGEPLATE_ImagePlate_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAImagePlate(); \
	friend struct Z_Construct_UClass_AImagePlate_Statics; \
public: \
	DECLARE_CLASS(AImagePlate, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(AImagePlate)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAImagePlate(); \
	friend struct Z_Construct_UClass_AImagePlate_Statics; \
public: \
	DECLARE_CLASS(AImagePlate, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(AImagePlate)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AImagePlate(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AImagePlate) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImagePlate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImagePlate); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImagePlate(AImagePlate&&); \
	NO_API AImagePlate(const AImagePlate&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImagePlate(AImagePlate&&); \
	NO_API AImagePlate(const AImagePlate&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImagePlate); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImagePlate); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AImagePlate)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImagePlate() { return STRUCT_OFFSET(AImagePlate, ImagePlate); }


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_12_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATE_API UClass* StaticClass<class AImagePlate>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlate_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
