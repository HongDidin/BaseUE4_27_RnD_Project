// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMAGEPLATE_ImagePlateFileSequence_generated_h
#error "ImagePlateFileSequence.generated.h already included, missing '#pragma once' in ImagePlateFileSequence.h"
#endif
#define IMAGEPLATE_ImagePlateFileSequence_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImagePlateSettings(); \
	friend struct Z_Construct_UClass_UImagePlateSettings_Statics; \
public: \
	DECLARE_CLASS(UImagePlateSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(UImagePlateSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUImagePlateSettings(); \
	friend struct Z_Construct_UClass_UImagePlateSettings_Statics; \
public: \
	DECLARE_CLASS(UImagePlateSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(UImagePlateSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImagePlateSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImagePlateSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImagePlateSettings(UImagePlateSettings&&); \
	NO_API UImagePlateSettings(const UImagePlateSettings&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImagePlateSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImagePlateSettings(UImagePlateSettings&&); \
	NO_API UImagePlateSettings(const UImagePlateSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImagePlateSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateSettings)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_20_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_24_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATE_API UClass* StaticClass<class UImagePlateSettings>();

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImagePlateFileSequence(); \
	friend struct Z_Construct_UClass_UImagePlateFileSequence_Statics; \
public: \
	DECLARE_CLASS(UImagePlateFileSequence, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(UImagePlateFileSequence)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUImagePlateFileSequence(); \
	friend struct Z_Construct_UClass_UImagePlateFileSequence_Statics; \
public: \
	DECLARE_CLASS(UImagePlateFileSequence, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlate"), NO_API) \
	DECLARE_SERIALIZER(UImagePlateFileSequence)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImagePlateFileSequence(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateFileSequence) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImagePlateFileSequence); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateFileSequence); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImagePlateFileSequence(UImagePlateFileSequence&&); \
	NO_API UImagePlateFileSequence(const UImagePlateFileSequence&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImagePlateFileSequence(UImagePlateFileSequence&&); \
	NO_API UImagePlateFileSequence(const UImagePlateFileSequence&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImagePlateFileSequence); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImagePlateFileSequence); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImagePlateFileSequence)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_32_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h_36_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATE_API UClass* StaticClass<class UImagePlateFileSequence>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_ImagePlateFileSequence_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
