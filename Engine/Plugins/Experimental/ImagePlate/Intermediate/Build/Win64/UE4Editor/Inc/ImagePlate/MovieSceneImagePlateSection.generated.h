// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMAGEPLATE_MovieSceneImagePlateSection_generated_h
#error "MovieSceneImagePlateSection.generated.h already included, missing '#pragma once' in MovieSceneImagePlateSection.h"
#endif
#define IMAGEPLATE_MovieSceneImagePlateSection_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneImagePlateSection(); \
	friend struct Z_Construct_UClass_UMovieSceneImagePlateSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneImagePlateSection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlate"), IMAGEPLATE_API) \
	DECLARE_SERIALIZER(UMovieSceneImagePlateSection)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneImagePlateSection(); \
	friend struct Z_Construct_UClass_UMovieSceneImagePlateSection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneImagePlateSection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlate"), IMAGEPLATE_API) \
	DECLARE_SERIALIZER(UMovieSceneImagePlateSection)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	IMAGEPLATE_API UMovieSceneImagePlateSection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneImagePlateSection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATE_API, UMovieSceneImagePlateSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneImagePlateSection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATE_API UMovieSceneImagePlateSection(UMovieSceneImagePlateSection&&); \
	IMAGEPLATE_API UMovieSceneImagePlateSection(const UMovieSceneImagePlateSection&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATE_API UMovieSceneImagePlateSection(UMovieSceneImagePlateSection&&); \
	IMAGEPLATE_API UMovieSceneImagePlateSection(const UMovieSceneImagePlateSection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATE_API, UMovieSceneImagePlateSection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneImagePlateSection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneImagePlateSection)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_14_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h_20_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATE_API UClass* StaticClass<class UMovieSceneImagePlateSection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateSection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
