// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMAGEPLATE_MovieSceneImagePlateTrack_generated_h
#error "MovieSceneImagePlateTrack.generated.h already included, missing '#pragma once' in MovieSceneImagePlateTrack.h"
#endif
#define IMAGEPLATE_MovieSceneImagePlateTrack_generated_h

#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneImagePlateTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneImagePlateTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneImagePlateTrack, UMovieScenePropertyTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlate"), IMAGEPLATE_API) \
	DECLARE_SERIALIZER(UMovieSceneImagePlateTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneImagePlateTrack*>(this); }


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneImagePlateTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneImagePlateTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneImagePlateTrack, UMovieScenePropertyTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImagePlate"), IMAGEPLATE_API) \
	DECLARE_SERIALIZER(UMovieSceneImagePlateTrack) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneImagePlateTrack*>(this); }


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	IMAGEPLATE_API UMovieSceneImagePlateTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneImagePlateTrack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATE_API, UMovieSceneImagePlateTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneImagePlateTrack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATE_API UMovieSceneImagePlateTrack(UMovieSceneImagePlateTrack&&); \
	IMAGEPLATE_API UMovieSceneImagePlateTrack(const UMovieSceneImagePlateTrack&); \
public:


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMAGEPLATE_API UMovieSceneImagePlateTrack(UMovieSceneImagePlateTrack&&); \
	IMAGEPLATE_API UMovieSceneImagePlateTrack(const UMovieSceneImagePlateTrack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMAGEPLATE_API, UMovieSceneImagePlateTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneImagePlateTrack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneImagePlateTrack)


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_15_PROLOG
#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_INCLASS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h_21_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMAGEPLATE_API UClass* StaticClass<class UMovieSceneImagePlateTrack>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ImagePlate_Source_ImagePlate_Public_MovieSceneImagePlateTrack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
