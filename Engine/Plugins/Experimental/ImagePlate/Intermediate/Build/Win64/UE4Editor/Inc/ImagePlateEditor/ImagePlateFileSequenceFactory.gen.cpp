// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImagePlateEditor/Private/ImagePlateFileSequenceFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImagePlateFileSequenceFactory() {}
// Cross Module References
	IMAGEPLATEEDITOR_API UClass* Z_Construct_UClass_UImagePlateFileSequenceFactory_NoRegister();
	IMAGEPLATEEDITOR_API UClass* Z_Construct_UClass_UImagePlateFileSequenceFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_ImagePlateEditor();
// End Cross Module References
	void UImagePlateFileSequenceFactory::StaticRegisterNativesUImagePlateFileSequenceFactory()
	{
	}
	UClass* Z_Construct_UClass_UImagePlateFileSequenceFactory_NoRegister()
	{
		return UImagePlateFileSequenceFactory::StaticClass();
	}
	struct Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_ImagePlateEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ImagePlateFileSequenceFactory.h" },
		{ "ModuleRelativePath", "Private/ImagePlateFileSequenceFactory.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UImagePlateFileSequenceFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics::ClassParams = {
		&UImagePlateFileSequenceFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UImagePlateFileSequenceFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UImagePlateFileSequenceFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UImagePlateFileSequenceFactory, 4293735767);
	template<> IMAGEPLATEEDITOR_API UClass* StaticClass<UImagePlateFileSequenceFactory>()
	{
		return UImagePlateFileSequenceFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UImagePlateFileSequenceFactory(Z_Construct_UClass_UImagePlateFileSequenceFactory, &UImagePlateFileSequenceFactory::StaticClass, TEXT("/Script/ImagePlateEditor"), TEXT("UImagePlateFileSequenceFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UImagePlateFileSequenceFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
