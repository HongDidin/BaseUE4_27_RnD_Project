// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOTIONTRAILEDITORMODE_MotionTrailEditorToolset_generated_h
#error "MotionTrailEditorToolset.generated.h already included, missing '#pragma once' in MotionTrailEditorToolset.h"
#endif
#define MOTIONTRAILEDITORMODE_MotionTrailEditorToolset_generated_h

#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_SPARSE_DATA
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTrailToolManagerBuilder(); \
	friend struct Z_Construct_UClass_UTrailToolManagerBuilder_Statics; \
public: \
	DECLARE_CLASS(UTrailToolManagerBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UTrailToolManagerBuilder)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_INCLASS \
private: \
	static void StaticRegisterNativesUTrailToolManagerBuilder(); \
	friend struct Z_Construct_UClass_UTrailToolManagerBuilder_Statics; \
public: \
	DECLARE_CLASS(UTrailToolManagerBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UTrailToolManagerBuilder)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTrailToolManagerBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTrailToolManagerBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTrailToolManagerBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTrailToolManagerBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTrailToolManagerBuilder(UTrailToolManagerBuilder&&); \
	NO_API UTrailToolManagerBuilder(const UTrailToolManagerBuilder&); \
public:


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTrailToolManagerBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTrailToolManagerBuilder(UTrailToolManagerBuilder&&); \
	NO_API UTrailToolManagerBuilder(const UTrailToolManagerBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTrailToolManagerBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTrailToolManagerBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTrailToolManagerBuilder)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_60_PROLOG
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_INCLASS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_63_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<class UTrailToolManagerBuilder>();

#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_SPARSE_DATA
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTrailToolManager(); \
	friend struct Z_Construct_UClass_UTrailToolManager_Statics; \
public: \
	DECLARE_CLASS(UTrailToolManager, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UTrailToolManager)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_INCLASS \
private: \
	static void StaticRegisterNativesUTrailToolManager(); \
	friend struct Z_Construct_UClass_UTrailToolManager_Statics; \
public: \
	DECLARE_CLASS(UTrailToolManager, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UTrailToolManager)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTrailToolManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTrailToolManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTrailToolManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTrailToolManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTrailToolManager(UTrailToolManager&&); \
	NO_API UTrailToolManager(const UTrailToolManager&); \
public:


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTrailToolManager() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTrailToolManager(UTrailToolManager&&); \
	NO_API UTrailToolManager(const UTrailToolManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTrailToolManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTrailToolManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTrailToolManager)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ToolProperties() { return STRUCT_OFFSET(UTrailToolManager, ToolProperties); }


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_77_PROLOG
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_INCLASS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h_80_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<class UTrailToolManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorToolset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
