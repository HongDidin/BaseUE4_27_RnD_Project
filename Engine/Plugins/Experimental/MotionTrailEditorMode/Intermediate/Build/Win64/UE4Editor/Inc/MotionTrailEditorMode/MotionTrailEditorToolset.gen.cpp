// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MotionTrailEditorMode/Public/MotionTrailEditorToolset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMotionTrailEditorToolset() {}
// Cross Module References
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UTrailToolManagerBuilder_NoRegister();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UTrailToolManagerBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MotionTrailEditorMode();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UTrailToolManager_NoRegister();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UTrailToolManager();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UTrailToolManagerBuilder::StaticRegisterNativesUTrailToolManagerBuilder()
	{
	}
	UClass* Z_Construct_UClass_UTrailToolManagerBuilder_NoRegister()
	{
		return UTrailToolManagerBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UTrailToolManagerBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTrailToolManagerBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MotionTrailEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTrailToolManagerBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder for UTrailToolManager\n */" },
		{ "IncludePath", "MotionTrailEditorToolset.h" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorToolset.h" },
		{ "ToolTip", "Builder for UTrailToolManager" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTrailToolManagerBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTrailToolManagerBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTrailToolManagerBuilder_Statics::ClassParams = {
		&UTrailToolManagerBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTrailToolManagerBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTrailToolManagerBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTrailToolManagerBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTrailToolManagerBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTrailToolManagerBuilder, 3470160988);
	template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<UTrailToolManagerBuilder>()
	{
		return UTrailToolManagerBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTrailToolManagerBuilder(Z_Construct_UClass_UTrailToolManagerBuilder, &UTrailToolManagerBuilder::StaticClass, TEXT("/Script/MotionTrailEditorMode"), TEXT("UTrailToolManagerBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTrailToolManagerBuilder);
	void UTrailToolManager::StaticRegisterNativesUTrailToolManager()
	{
	}
	UClass* Z_Construct_UClass_UTrailToolManager_NoRegister()
	{
		return UTrailToolManager::StaticClass();
	}
	struct Z_Construct_UClass_UTrailToolManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ToolProperties_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ToolProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ToolProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTrailToolManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MotionTrailEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTrailToolManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MotionTrailEditorToolset.h" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorToolset.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTrailToolManager_Statics::NewProp_ToolProperties_Inner = { "ToolProperties", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTrailToolManager_Statics::NewProp_ToolProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/MotionTrailEditorToolset.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTrailToolManager_Statics::NewProp_ToolProperties = { "ToolProperties", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTrailToolManager, ToolProperties), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTrailToolManager_Statics::NewProp_ToolProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTrailToolManager_Statics::NewProp_ToolProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTrailToolManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTrailToolManager_Statics::NewProp_ToolProperties_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTrailToolManager_Statics::NewProp_ToolProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTrailToolManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTrailToolManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTrailToolManager_Statics::ClassParams = {
		&UTrailToolManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTrailToolManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTrailToolManager_Statics::PropPointers),
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTrailToolManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTrailToolManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTrailToolManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTrailToolManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTrailToolManager, 4129545836);
	template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<UTrailToolManager>()
	{
		return UTrailToolManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTrailToolManager(Z_Construct_UClass_UTrailToolManager, &UTrailToolManager::StaticClass, TEXT("/Script/MotionTrailEditorMode"), TEXT("UTrailToolManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTrailToolManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
