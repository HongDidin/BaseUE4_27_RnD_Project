// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOTIONTRAILEDITORMODE_MovieSceneTransformTrailTool_generated_h
#error "MovieSceneTransformTrailTool.generated.h already included, missing '#pragma once' in MovieSceneTransformTrailTool.h"
#endif
#define MOTIONTRAILEDITORMODE_MovieSceneTransformTrailTool_generated_h

#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMSTrailKeyProperties(); \
	friend struct Z_Construct_UClass_UMSTrailKeyProperties_Statics; \
public: \
	DECLARE_CLASS(UMSTrailKeyProperties, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMSTrailKeyProperties)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMSTrailKeyProperties(); \
	friend struct Z_Construct_UClass_UMSTrailKeyProperties_Statics; \
public: \
	DECLARE_CLASS(UMSTrailKeyProperties, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMSTrailKeyProperties)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMSTrailKeyProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMSTrailKeyProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMSTrailKeyProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMSTrailKeyProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMSTrailKeyProperties(UMSTrailKeyProperties&&); \
	NO_API UMSTrailKeyProperties(const UMSTrailKeyProperties&); \
public:


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMSTrailKeyProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMSTrailKeyProperties(UMSTrailKeyProperties&&); \
	NO_API UMSTrailKeyProperties(const UMSTrailKeyProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMSTrailKeyProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMSTrailKeyProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMSTrailKeyProperties)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_14_PROLOG
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_INCLASS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<class UMSTrailKeyProperties>();

#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_SPARSE_DATA
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMSTrailTransformProxy(); \
	friend struct Z_Construct_UClass_UMSTrailTransformProxy_Statics; \
public: \
	DECLARE_CLASS(UMSTrailTransformProxy, UTransformProxy, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMSTrailTransformProxy)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_INCLASS \
private: \
	static void StaticRegisterNativesUMSTrailTransformProxy(); \
	friend struct Z_Construct_UClass_UMSTrailTransformProxy_Statics; \
public: \
	DECLARE_CLASS(UMSTrailTransformProxy, UTransformProxy, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMSTrailTransformProxy)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMSTrailTransformProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMSTrailTransformProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMSTrailTransformProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMSTrailTransformProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMSTrailTransformProxy(UMSTrailTransformProxy&&); \
	NO_API UMSTrailTransformProxy(const UMSTrailTransformProxy&); \
public:


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMSTrailTransformProxy(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMSTrailTransformProxy(UMSTrailTransformProxy&&); \
	NO_API UMSTrailTransformProxy(const UMSTrailTransformProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMSTrailTransformProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMSTrailTransformProxy); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMSTrailTransformProxy)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_119_PROLOG
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_INCLASS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h_122_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<class UMSTrailTransformProxy>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Private_Sequencer_MovieSceneTransformTrailTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
