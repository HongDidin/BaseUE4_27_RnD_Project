// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MotionTrailEditorMode/Public/MotionTrailEditorMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMotionTrailEditorMode() {}
// Cross Module References
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMotionTrailOptions_NoRegister();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMotionTrailOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MotionTrailEditorMode();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMotionTrailEditorMode_NoRegister();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMotionTrailEditorMode();
	UNREALED_API UClass* Z_Construct_UClass_UEdMode();
// End Cross Module References
	void UMotionTrailOptions::StaticRegisterNativesUMotionTrailOptions()
	{
	}
	UClass* Z_Construct_UClass_UMotionTrailOptions_NoRegister()
	{
		return UMotionTrailOptions::StaticClass();
	}
	struct Z_Construct_UClass_UMotionTrailOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowTrails_MetaData[];
#endif
		static void NewProp_bShowTrails_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowTrails;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowFullTrail_MetaData[];
#endif
		static void NewProp_bShowFullTrail_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowFullTrail;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FramesBefore_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FramesBefore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FramesAfter_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FramesAfter;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Subdivisions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLockTicksToFrames_MetaData[];
#endif
		static void NewProp_bLockTicksToFrames_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLockTicksToFrames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SecondsPerTick_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_SecondsPerTick;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TickSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_TickSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrailThickness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TrailThickness;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMotionTrailOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MotionTrailEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// TODO: option to make tick size proportional to distance from camera to get a sense of perspective and scale\n" },
		{ "IncludePath", "MotionTrailEditorMode.h" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
		{ "ToolTip", "TODO: option to make tick size proportional to distance from camera to get a sense of perspective and scale" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowTrails_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	void Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowTrails_SetBit(void* Obj)
	{
		((UMotionTrailOptions*)Obj)->bShowTrails = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowTrails = { "bShowTrails", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMotionTrailOptions), &Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowTrails_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowTrails_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowTrails_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowFullTrail_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "EditCondition", "bShowTrails" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	void Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowFullTrail_SetBit(void* Obj)
	{
		((UMotionTrailOptions*)Obj)->bShowFullTrail = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowFullTrail = { "bShowFullTrail", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMotionTrailOptions), &Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowFullTrail_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowFullTrail_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowFullTrail_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesBefore_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "ClampMin", "0" },
		{ "EditCondition", "!bShowFullTrail && bShowTrails" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesBefore = { "FramesBefore", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMotionTrailOptions, FramesBefore), METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesBefore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesBefore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesAfter_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "ClampMin", "0" },
		{ "EditCondition", "!bShowFullTrail && bShowTrails" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesAfter = { "FramesAfter", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMotionTrailOptions, FramesAfter), METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesAfter_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesAfter_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_Subdivisions_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "ClampMin", "2" },
		{ "EditCondition", "bShowTrails" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_Subdivisions = { "Subdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMotionTrailOptions, Subdivisions), METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_Subdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_Subdivisions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bLockTicksToFrames_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "EditCondition", "bShowTrails" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	void Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bLockTicksToFrames_SetBit(void* Obj)
	{
		((UMotionTrailOptions*)Obj)->bLockTicksToFrames = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bLockTicksToFrames = { "bLockTicksToFrames", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMotionTrailOptions), &Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bLockTicksToFrames_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bLockTicksToFrames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bLockTicksToFrames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_SecondsPerTick_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "ClampMin", "0.01" },
		{ "EditCondition", "bShowTrails && !bLockTicksToFrames" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_SecondsPerTick = { "SecondsPerTick", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMotionTrailOptions, SecondsPerTick), METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_SecondsPerTick_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_SecondsPerTick_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TickSize_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "ClampMin", "0.0" },
		{ "EditCondition", "bShowTrails" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TickSize = { "TickSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMotionTrailOptions, TickSize), METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TickSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TickSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TrailThickness_MetaData[] = {
		{ "Category", "ShowOptions" },
		{ "ClampMin", "0.0" },
		{ "EditCondition", "bShowTrails" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TrailThickness = { "TrailThickness", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMotionTrailOptions, TrailThickness), METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TrailThickness_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TrailThickness_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMotionTrailOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowTrails,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bShowFullTrail,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesBefore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_FramesAfter,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_Subdivisions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_bLockTicksToFrames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_SecondsPerTick,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TickSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailOptions_Statics::NewProp_TrailThickness,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMotionTrailOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMotionTrailOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMotionTrailOptions_Statics::ClassParams = {
		&UMotionTrailOptions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMotionTrailOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMotionTrailOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMotionTrailOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMotionTrailOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMotionTrailOptions, 130971870);
	template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<UMotionTrailOptions>()
	{
		return UMotionTrailOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMotionTrailOptions(Z_Construct_UClass_UMotionTrailOptions, &UMotionTrailOptions::StaticClass, TEXT("/Script/MotionTrailEditorMode"), TEXT("UMotionTrailOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMotionTrailOptions);
	void UMotionTrailEditorMode::StaticRegisterNativesUMotionTrailEditorMode()
	{
	}
	UClass* Z_Construct_UClass_UMotionTrailEditorMode_NoRegister()
	{
		return UMotionTrailEditorMode::StaticClass();
	}
	struct Z_Construct_UClass_UMotionTrailEditorMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrailOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TrailOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMotionTrailEditorMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdMode,
		(UObject* (*)())Z_Construct_UPackage__Script_MotionTrailEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailEditorMode_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MotionTrailEditorMode.h" },
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMotionTrailEditorMode_Statics::NewProp_TrailOptions_MetaData[] = {
		{ "ModuleRelativePath", "Public/MotionTrailEditorMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMotionTrailEditorMode_Statics::NewProp_TrailOptions = { "TrailOptions", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMotionTrailEditorMode, TrailOptions), Z_Construct_UClass_UMotionTrailOptions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMotionTrailEditorMode_Statics::NewProp_TrailOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailEditorMode_Statics::NewProp_TrailOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMotionTrailEditorMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMotionTrailEditorMode_Statics::NewProp_TrailOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMotionTrailEditorMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMotionTrailEditorMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMotionTrailEditorMode_Statics::ClassParams = {
		&UMotionTrailEditorMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMotionTrailEditorMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailEditorMode_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMotionTrailEditorMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMotionTrailEditorMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMotionTrailEditorMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMotionTrailEditorMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMotionTrailEditorMode, 119620271);
	template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<UMotionTrailEditorMode>()
	{
		return UMotionTrailEditorMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMotionTrailEditorMode(Z_Construct_UClass_UMotionTrailEditorMode, &UMotionTrailEditorMode::StaticClass, TEXT("/Script/MotionTrailEditorMode"), TEXT("UMotionTrailEditorMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMotionTrailEditorMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
