// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOTIONTRAILEDITORMODE_MotionTrailEditorMode_generated_h
#error "MotionTrailEditorMode.generated.h already included, missing '#pragma once' in MotionTrailEditorMode.h"
#endif
#define MOTIONTRAILEDITORMODE_MotionTrailEditorMode_generated_h

#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMotionTrailOptions(); \
	friend struct Z_Construct_UClass_UMotionTrailOptions_Statics; \
public: \
	DECLARE_CLASS(UMotionTrailOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMotionTrailOptions)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUMotionTrailOptions(); \
	friend struct Z_Construct_UClass_UMotionTrailOptions_Statics; \
public: \
	DECLARE_CLASS(UMotionTrailOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMotionTrailOptions)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMotionTrailOptions(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMotionTrailOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMotionTrailOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMotionTrailOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMotionTrailOptions(UMotionTrailOptions&&); \
	NO_API UMotionTrailOptions(const UMotionTrailOptions&); \
public:


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMotionTrailOptions(UMotionTrailOptions&&); \
	NO_API UMotionTrailOptions(const UMotionTrailOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMotionTrailOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMotionTrailOptions); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMotionTrailOptions)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_18_PROLOG
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_INCLASS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<class UMotionTrailOptions>();

#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_SPARSE_DATA
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMotionTrailEditorMode(); \
	friend struct Z_Construct_UClass_UMotionTrailEditorMode_Statics; \
public: \
	DECLARE_CLASS(UMotionTrailEditorMode, UEdMode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMotionTrailEditorMode)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_INCLASS \
private: \
	static void StaticRegisterNativesUMotionTrailEditorMode(); \
	friend struct Z_Construct_UClass_UMotionTrailEditorMode_Statics; \
public: \
	DECLARE_CLASS(UMotionTrailEditorMode, UEdMode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MotionTrailEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMotionTrailEditorMode)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMotionTrailEditorMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMotionTrailEditorMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMotionTrailEditorMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMotionTrailEditorMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMotionTrailEditorMode(UMotionTrailEditorMode&&); \
	NO_API UMotionTrailEditorMode(const UMotionTrailEditorMode&); \
public:


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMotionTrailEditorMode(UMotionTrailEditorMode&&); \
	NO_API UMotionTrailEditorMode(const UMotionTrailEditorMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMotionTrailEditorMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMotionTrailEditorMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMotionTrailEditorMode)


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TrailOptions() { return STRUCT_OFFSET(UMotionTrailEditorMode, TrailOptions); }


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_73_PROLOG
#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_INCLASS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_SPARSE_DATA \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h_76_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<class UMotionTrailEditorMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MotionTrailEditorMode_Source_MotionTrailEditorMode_Public_MotionTrailEditorMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
