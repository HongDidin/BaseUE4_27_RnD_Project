// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MotionTrailEditorMode/Private/Sequencer/MovieSceneTransformTrailTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneTransformTrailTool() {}
// Cross Module References
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMSTrailKeyProperties_NoRegister();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMSTrailKeyProperties();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MotionTrailEditorMode();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMSTrailTransformProxy_NoRegister();
	MOTIONTRAILEDITORMODE_API UClass* Z_Construct_UClass_UMSTrailTransformProxy();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy();
// End Cross Module References
	void UMSTrailKeyProperties::StaticRegisterNativesUMSTrailKeyProperties()
	{
	}
	UClass* Z_Construct_UClass_UMSTrailKeyProperties_NoRegister()
	{
		return UMSTrailKeyProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMSTrailKeyProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KeySize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_KeySize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMSTrailKeyProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MotionTrailEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMSTrailKeyProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sequencer/MovieSceneTransformTrailTool.h" },
		{ "ModuleRelativePath", "Private/Sequencer/MovieSceneTransformTrailTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMSTrailKeyProperties_Statics::NewProp_KeySize_MetaData[] = {
		{ "Category", "ToolShowOptions" },
		{ "ModuleRelativePath", "Private/Sequencer/MovieSceneTransformTrailTool.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMSTrailKeyProperties_Statics::NewProp_KeySize = { "KeySize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMSTrailKeyProperties, KeySize), METADATA_PARAMS(Z_Construct_UClass_UMSTrailKeyProperties_Statics::NewProp_KeySize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMSTrailKeyProperties_Statics::NewProp_KeySize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMSTrailKeyProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMSTrailKeyProperties_Statics::NewProp_KeySize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMSTrailKeyProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMSTrailKeyProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMSTrailKeyProperties_Statics::ClassParams = {
		&UMSTrailKeyProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMSTrailKeyProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMSTrailKeyProperties_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMSTrailKeyProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMSTrailKeyProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMSTrailKeyProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMSTrailKeyProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMSTrailKeyProperties, 826760757);
	template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<UMSTrailKeyProperties>()
	{
		return UMSTrailKeyProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMSTrailKeyProperties(Z_Construct_UClass_UMSTrailKeyProperties, &UMSTrailKeyProperties::StaticClass, TEXT("/Script/MotionTrailEditorMode"), TEXT("UMSTrailKeyProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMSTrailKeyProperties);
	void UMSTrailTransformProxy::StaticRegisterNativesUMSTrailTransformProxy()
	{
	}
	UClass* Z_Construct_UClass_UMSTrailTransformProxy_NoRegister()
	{
		return UMSTrailTransformProxy::StaticClass();
	}
	struct Z_Construct_UClass_UMSTrailTransformProxy_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMSTrailTransformProxy_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTransformProxy,
		(UObject* (*)())Z_Construct_UPackage__Script_MotionTrailEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMSTrailTransformProxy_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// namespace UE\n" },
		{ "IncludePath", "Sequencer/MovieSceneTransformTrailTool.h" },
		{ "ModuleRelativePath", "Private/Sequencer/MovieSceneTransformTrailTool.h" },
		{ "ToolTip", "namespace UE" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMSTrailTransformProxy_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMSTrailTransformProxy>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMSTrailTransformProxy_Statics::ClassParams = {
		&UMSTrailTransformProxy::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMSTrailTransformProxy_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMSTrailTransformProxy_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMSTrailTransformProxy()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMSTrailTransformProxy_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMSTrailTransformProxy, 3139718273);
	template<> MOTIONTRAILEDITORMODE_API UClass* StaticClass<UMSTrailTransformProxy>()
	{
		return UMSTrailTransformProxy::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMSTrailTransformProxy(Z_Construct_UClass_UMSTrailTransformProxy, &UMSTrailTransformProxy::StaticClass, TEXT("/Script/MotionTrailEditorMode"), TEXT("UMSTrailTransformProxy"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMSTrailTransformProxy);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
