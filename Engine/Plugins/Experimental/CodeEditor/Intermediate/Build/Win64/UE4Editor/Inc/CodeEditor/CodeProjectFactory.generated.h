// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODEEDITOR_CodeProjectFactory_generated_h
#error "CodeProjectFactory.generated.h already included, missing '#pragma once' in CodeProjectFactory.h"
#endif
#define CODEEDITOR_CodeProjectFactory_generated_h

#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCodeProjectFactory(); \
	friend struct Z_Construct_UClass_UCodeProjectFactory_Statics; \
public: \
	DECLARE_CLASS(UCodeProjectFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeEditor"), NO_API) \
	DECLARE_SERIALIZER(UCodeProjectFactory)


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUCodeProjectFactory(); \
	friend struct Z_Construct_UClass_UCodeProjectFactory_Statics; \
public: \
	DECLARE_CLASS(UCodeProjectFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeEditor"), NO_API) \
	DECLARE_SERIALIZER(UCodeProjectFactory)


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCodeProjectFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCodeProjectFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCodeProjectFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCodeProjectFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCodeProjectFactory(UCodeProjectFactory&&); \
	NO_API UCodeProjectFactory(const UCodeProjectFactory&); \
public:


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCodeProjectFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCodeProjectFactory(UCodeProjectFactory&&); \
	NO_API UCodeProjectFactory(const UCodeProjectFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCodeProjectFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCodeProjectFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCodeProjectFactory)


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_10_PROLOG
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_INCLASS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CodeProjectFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CODEEDITOR_API UClass* StaticClass<class UCodeProjectFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
