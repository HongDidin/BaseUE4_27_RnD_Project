// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODEEDITOR_CodeProjectItem_generated_h
#error "CodeProjectItem.generated.h already included, missing '#pragma once' in CodeProjectItem.h"
#endif
#define CODEEDITOR_CodeProjectItem_generated_h

#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCodeProjectItem(); \
	friend struct Z_Construct_UClass_UCodeProjectItem_Statics; \
public: \
	DECLARE_CLASS(UCodeProjectItem, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeEditor"), NO_API) \
	DECLARE_SERIALIZER(UCodeProjectItem)


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUCodeProjectItem(); \
	friend struct Z_Construct_UClass_UCodeProjectItem_Statics; \
public: \
	DECLARE_CLASS(UCodeProjectItem, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeEditor"), NO_API) \
	DECLARE_SERIALIZER(UCodeProjectItem)


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCodeProjectItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCodeProjectItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCodeProjectItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCodeProjectItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCodeProjectItem(UCodeProjectItem&&); \
	NO_API UCodeProjectItem(const UCodeProjectItem&); \
public:


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCodeProjectItem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCodeProjectItem(UCodeProjectItem&&); \
	NO_API UCodeProjectItem(const UCodeProjectItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCodeProjectItem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCodeProjectItem); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCodeProjectItem)


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_22_PROLOG
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_INCLASS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h_25_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CodeProjectItem."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CODEEDITOR_API UClass* StaticClass<class UCodeProjectItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeProjectItem_h


#define FOREACH_ENUM_ECODEPROJECTITEMTYPE(op) \
	op(ECodeProjectItemType::Project) \
	op(ECodeProjectItemType::Folder) \
	op(ECodeProjectItemType::File) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
