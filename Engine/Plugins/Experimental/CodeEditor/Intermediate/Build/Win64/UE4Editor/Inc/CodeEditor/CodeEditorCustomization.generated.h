// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CODEEDITOR_CodeEditorCustomization_generated_h
#error "CodeEditorCustomization.generated.h already included, missing '#pragma once' in CodeEditorCustomization.h"
#endif
#define CODEEDITOR_CodeEditorCustomization_generated_h

#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics; \
	CODEEDITOR_API static class UScriptStruct* StaticStruct();


template<> CODEEDITOR_API UScriptStruct* StaticStruct<struct FCodeEditorControlCustomization>();

#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_13_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics; \
	CODEEDITOR_API static class UScriptStruct* StaticStruct();


template<> CODEEDITOR_API UScriptStruct* StaticStruct<struct FCodeEditorTextCustomization>();

#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUCodeEditorCustomization(); \
	friend struct Z_Construct_UClass_UCodeEditorCustomization_Statics; \
public: \
	DECLARE_CLASS(UCodeEditorCustomization, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeEditor"), NO_API) \
	DECLARE_SERIALIZER(UCodeEditorCustomization) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUCodeEditorCustomization(); \
	friend struct Z_Construct_UClass_UCodeEditorCustomization_Statics; \
public: \
	DECLARE_CLASS(UCodeEditorCustomization, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/CodeEditor"), NO_API) \
	DECLARE_SERIALIZER(UCodeEditorCustomization) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCodeEditorCustomization(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCodeEditorCustomization) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCodeEditorCustomization); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCodeEditorCustomization); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCodeEditorCustomization(UCodeEditorCustomization&&); \
	NO_API UCodeEditorCustomization(const UCodeEditorCustomization&); \
public:


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UCodeEditorCustomization(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UCodeEditorCustomization(UCodeEditorCustomization&&); \
	NO_API UCodeEditorCustomization(const UCodeEditorCustomization&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UCodeEditorCustomization); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UCodeEditorCustomization); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UCodeEditorCustomization)


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Controls() { return STRUCT_OFFSET(UCodeEditorCustomization, Controls); } \
	FORCEINLINE static uint32 __PPO__Text() { return STRUCT_OFFSET(UCodeEditorCustomization, Text); }


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_42_PROLOG
#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_INCLASS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h_45_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class CodeEditorCustomization."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CODEEDITOR_API UClass* StaticClass<class UCodeEditorCustomization>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_CodeEditor_Source_CodeEditor_Private_CodeEditorCustomization_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
