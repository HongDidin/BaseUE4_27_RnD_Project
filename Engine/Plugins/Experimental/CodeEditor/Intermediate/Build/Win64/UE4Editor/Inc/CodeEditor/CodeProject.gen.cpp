// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeEditor/Private/CodeProject.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCodeProject() {}
// Cross Module References
	CODEEDITOR_API UClass* Z_Construct_UClass_UCodeProject_NoRegister();
	CODEEDITOR_API UClass* Z_Construct_UClass_UCodeProject();
	CODEEDITOR_API UClass* Z_Construct_UClass_UCodeProjectItem();
	UPackage* Z_Construct_UPackage__Script_CodeEditor();
// End Cross Module References
	void UCodeProject::StaticRegisterNativesUCodeProject()
	{
	}
	UClass* Z_Construct_UClass_UCodeProject_NoRegister()
	{
		return UCodeProject::StaticClass();
	}
	struct Z_Construct_UClass_UCodeProject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCodeProject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCodeProjectItem,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeProject_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CodeProject.h" },
		{ "ModuleRelativePath", "Private/CodeProject.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCodeProject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCodeProject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCodeProject_Statics::ClassParams = {
		&UCodeProject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCodeProject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCodeProject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCodeProject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCodeProject, 2553263972);
	template<> CODEEDITOR_API UClass* StaticClass<UCodeProject>()
	{
		return UCodeProject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCodeProject(Z_Construct_UClass_UCodeProject, &UCodeProject::StaticClass, TEXT("/Script/CodeEditor"), TEXT("UCodeProject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCodeProject);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
