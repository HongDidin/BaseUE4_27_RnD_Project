// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeEditor/Private/CodeProjectItem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCodeProjectItem() {}
// Cross Module References
	CODEEDITOR_API UEnum* Z_Construct_UEnum_CodeEditor_ECodeProjectItemType();
	UPackage* Z_Construct_UPackage__Script_CodeEditor();
	CODEEDITOR_API UClass* Z_Construct_UClass_UCodeProjectItem_NoRegister();
	CODEEDITOR_API UClass* Z_Construct_UClass_UCodeProjectItem();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* ECodeProjectItemType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CodeEditor_ECodeProjectItemType, Z_Construct_UPackage__Script_CodeEditor(), TEXT("ECodeProjectItemType"));
		}
		return Singleton;
	}
	template<> CODEEDITOR_API UEnum* StaticEnum<ECodeProjectItemType::Type>()
	{
		return ECodeProjectItemType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECodeProjectItemType(ECodeProjectItemType_StaticEnum, TEXT("/Script/CodeEditor"), TEXT("ECodeProjectItemType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CodeEditor_ECodeProjectItemType_Hash() { return 195499944U; }
	UEnum* Z_Construct_UEnum_CodeEditor_ECodeProjectItemType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CodeEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECodeProjectItemType"), 0, Get_Z_Construct_UEnum_CodeEditor_ECodeProjectItemType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECodeProjectItemType::Project", (int64)ECodeProjectItemType::Project },
				{ "ECodeProjectItemType::Folder", (int64)ECodeProjectItemType::Folder },
				{ "ECodeProjectItemType::File", (int64)ECodeProjectItemType::File },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Types of project items. Note that the enum ordering determines the tree sorting */" },
				{ "File.Name", "ECodeProjectItemType::File" },
				{ "Folder.Name", "ECodeProjectItemType::Folder" },
				{ "ModuleRelativePath", "Private/CodeProjectItem.h" },
				{ "Project.Name", "ECodeProjectItemType::Project" },
				{ "ToolTip", "Types of project items. Note that the enum ordering determines the tree sorting" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CodeEditor,
				nullptr,
				"ECodeProjectItemType",
				"ECodeProjectItemType::Type",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Namespaced,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UCodeProjectItem::StaticRegisterNativesUCodeProjectItem()
	{
	}
	UClass* Z_Construct_UClass_UCodeProjectItem_NoRegister()
	{
		return UCodeProjectItem::StaticClass();
	}
	struct Z_Construct_UClass_UCodeProjectItem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Extension_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Extension;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Path_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Path;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Children_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Children;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCodeProjectItem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeProjectItem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CodeProjectItem.h" },
		{ "ModuleRelativePath", "Private/CodeProjectItem.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Type_MetaData[] = {
		{ "ModuleRelativePath", "Private/CodeProjectItem.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCodeProjectItem, Type), Z_Construct_UEnum_CodeEditor_ECodeProjectItemType, METADATA_PARAMS(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Name_MetaData[] = {
		{ "ModuleRelativePath", "Private/CodeProjectItem.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCodeProjectItem, Name), METADATA_PARAMS(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Extension_MetaData[] = {
		{ "ModuleRelativePath", "Private/CodeProjectItem.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Extension = { "Extension", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCodeProjectItem, Extension), METADATA_PARAMS(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Extension_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Extension_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Path_MetaData[] = {
		{ "ModuleRelativePath", "Private/CodeProjectItem.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Path = { "Path", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCodeProjectItem, Path), METADATA_PARAMS(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Path_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Path_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Children_Inner = { "Children", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UCodeProjectItem_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Children_MetaData[] = {
		{ "ModuleRelativePath", "Private/CodeProjectItem.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCodeProjectItem, Children), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Children_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCodeProjectItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Extension,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Path,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Children_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeProjectItem_Statics::NewProp_Children,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCodeProjectItem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCodeProjectItem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCodeProjectItem_Statics::ClassParams = {
		&UCodeProjectItem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCodeProjectItem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProjectItem_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCodeProjectItem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeProjectItem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCodeProjectItem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCodeProjectItem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCodeProjectItem, 391215246);
	template<> CODEEDITOR_API UClass* StaticClass<UCodeProjectItem>()
	{
		return UCodeProjectItem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCodeProjectItem(Z_Construct_UClass_UCodeProjectItem, &UCodeProjectItem::StaticClass, TEXT("/Script/CodeEditor"), TEXT("UCodeProjectItem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCodeProjectItem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
