// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CodeEditor/Private/CodeEditorCustomization.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCodeEditorCustomization() {}
// Cross Module References
	CODEEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FCodeEditorControlCustomization();
	UPackage* Z_Construct_UPackage__Script_CodeEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	CODEEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FCodeEditorTextCustomization();
	CODEEDITOR_API UClass* Z_Construct_UClass_UCodeEditorCustomization_NoRegister();
	CODEEDITOR_API UClass* Z_Construct_UClass_UCodeEditorCustomization();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FCodeEditorControlCustomization::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CODEEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCodeEditorControlCustomization, Z_Construct_UPackage__Script_CodeEditor(), TEXT("CodeEditorControlCustomization"), sizeof(FCodeEditorControlCustomization), Get_Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Hash());
	}
	return Singleton;
}
template<> CODEEDITOR_API UScriptStruct* StaticStruct<FCodeEditorControlCustomization>()
{
	return FCodeEditorControlCustomization::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCodeEditorControlCustomization(FCodeEditorControlCustomization::StaticStruct, TEXT("/Script/CodeEditor"), TEXT("CodeEditorControlCustomization"), false, nullptr, nullptr);
static struct FScriptStruct_CodeEditor_StaticRegisterNativesFCodeEditorControlCustomization
{
	FScriptStruct_CodeEditor_StaticRegisterNativesFCodeEditorControlCustomization()
	{
		UScriptStruct::DeferCppStructOps<FCodeEditorControlCustomization>(FName(TEXT("CodeEditorControlCustomization")));
	}
} ScriptStruct_CodeEditor_StaticRegisterNativesFCodeEditorControlCustomization;
	struct Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCodeEditorControlCustomization>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Controls" },
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCodeEditorControlCustomization, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::NewProp_Color,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CodeEditor,
		nullptr,
		&NewStructOps,
		"CodeEditorControlCustomization",
		sizeof(FCodeEditorControlCustomization),
		alignof(FCodeEditorControlCustomization),
		Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCodeEditorControlCustomization()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CodeEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CodeEditorControlCustomization"), sizeof(FCodeEditorControlCustomization), Get_Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCodeEditorControlCustomization_Hash() { return 2155674716U; }
class UScriptStruct* FCodeEditorTextCustomization::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CODEEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FCodeEditorTextCustomization, Z_Construct_UPackage__Script_CodeEditor(), TEXT("CodeEditorTextCustomization"), sizeof(FCodeEditorTextCustomization), Get_Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Hash());
	}
	return Singleton;
}
template<> CODEEDITOR_API UScriptStruct* StaticStruct<FCodeEditorTextCustomization>()
{
	return FCodeEditorTextCustomization::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FCodeEditorTextCustomization(FCodeEditorTextCustomization::StaticStruct, TEXT("/Script/CodeEditor"), TEXT("CodeEditorTextCustomization"), false, nullptr, nullptr);
static struct FScriptStruct_CodeEditor_StaticRegisterNativesFCodeEditorTextCustomization
{
	FScriptStruct_CodeEditor_StaticRegisterNativesFCodeEditorTextCustomization()
	{
		UScriptStruct::DeferCppStructOps<FCodeEditorTextCustomization>(FName(TEXT("CodeEditorTextCustomization")));
	}
} ScriptStruct_CodeEditor_StaticRegisterNativesFCodeEditorTextCustomization;
	struct Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Font_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Font;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FCodeEditorTextCustomization>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Font_MetaData[] = {
		{ "Category", "Text" },
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Font = { "Font", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCodeEditorTextCustomization, Font), METADATA_PARAMS(Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Font_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Font_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Text" },
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FCodeEditorTextCustomization, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Color_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Font,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::NewProp_Color,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CodeEditor,
		nullptr,
		&NewStructOps,
		"CodeEditorTextCustomization",
		sizeof(FCodeEditorTextCustomization),
		alignof(FCodeEditorTextCustomization),
		Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FCodeEditorTextCustomization()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CodeEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("CodeEditorTextCustomization"), sizeof(FCodeEditorTextCustomization), Get_Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FCodeEditorTextCustomization_Hash() { return 788844316U; }
	void UCodeEditorCustomization::StaticRegisterNativesUCodeEditorCustomization()
	{
	}
	UClass* Z_Construct_UClass_UCodeEditorCustomization_NoRegister()
	{
		return UCodeEditorCustomization::StaticClass();
	}
	struct Z_Construct_UClass_UCodeEditorCustomization_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Controls_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Controls_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Controls;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Text_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Text_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Text;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCodeEditorCustomization_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_CodeEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeEditorCustomization_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "CodeEditorCustomization.h" },
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Controls_Inner = { "Controls", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCodeEditorControlCustomization, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Controls_MetaData[] = {
		{ "Category", "Controls" },
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Controls = { "Controls", nullptr, (EPropertyFlags)0x0040000000000041, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCodeEditorCustomization, Controls), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Controls_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Controls_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Text_Inner = { "Text", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FCodeEditorTextCustomization, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Text_MetaData[] = {
		{ "Category", "Text" },
		{ "ModuleRelativePath", "Private/CodeEditorCustomization.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Text = { "Text", nullptr, (EPropertyFlags)0x0040000000000041, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCodeEditorCustomization, Text), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Text_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Text_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCodeEditorCustomization_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Controls_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Controls,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Text_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCodeEditorCustomization_Statics::NewProp_Text,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCodeEditorCustomization_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCodeEditorCustomization>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCodeEditorCustomization_Statics::ClassParams = {
		&UCodeEditorCustomization::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCodeEditorCustomization_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCodeEditorCustomization_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCodeEditorCustomization_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCodeEditorCustomization_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCodeEditorCustomization()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCodeEditorCustomization_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCodeEditorCustomization, 3554209713);
	template<> CODEEDITOR_API UClass* StaticClass<UCodeEditorCustomization>()
	{
		return UCodeEditorCustomization::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCodeEditorCustomization(Z_Construct_UClass_UCodeEditorCustomization, &UCodeEditorCustomization::StaticClass, TEXT("/Script/CodeEditor"), TEXT("UCodeEditorCustomization"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCodeEditorCustomization);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
