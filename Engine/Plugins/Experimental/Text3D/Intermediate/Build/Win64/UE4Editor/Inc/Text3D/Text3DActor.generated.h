// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TEXT3D_Text3DActor_generated_h
#error "Text3DActor.generated.h already included, missing '#pragma once' in Text3DActor.h"
#endif
#define TEXT3D_Text3DActor_generated_h

#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_SPARSE_DATA
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAText3DActor(); \
	friend struct Z_Construct_UClass_AText3DActor_Statics; \
public: \
	DECLARE_CLASS(AText3DActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Text3D"), NO_API) \
	DECLARE_SERIALIZER(AText3DActor)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAText3DActor(); \
	friend struct Z_Construct_UClass_AText3DActor_Statics; \
public: \
	DECLARE_CLASS(AText3DActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Text3D"), NO_API) \
	DECLARE_SERIALIZER(AText3DActor)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AText3DActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AText3DActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AText3DActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AText3DActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AText3DActor(AText3DActor&&); \
	NO_API AText3DActor(const AText3DActor&); \
public:


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AText3DActor(AText3DActor&&); \
	NO_API AText3DActor(const AText3DActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AText3DActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AText3DActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AText3DActor)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Text3DComponent() { return STRUCT_OFFSET(AText3DActor, Text3DComponent); }


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_9_PROLOG
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_INCLASS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEXT3D_API UClass* StaticClass<class AText3DActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
