// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TEXT3D_Text3DEngineSubsystem_generated_h
#error "Text3DEngineSubsystem.generated.h already included, missing '#pragma once' in Text3DEngineSubsystem.h"
#endif
#define TEXT3D_Text3DEngineSubsystem_generated_h

#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCachedFontData_Statics; \
	TEXT3D_API static class UScriptStruct* StaticStruct();


template<> TEXT3D_API UScriptStruct* StaticStruct<struct FCachedFontData>();

#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FCachedFontMeshes_Statics; \
	TEXT3D_API static class UScriptStruct* StaticStruct();


template<> TEXT3D_API UScriptStruct* StaticStruct<struct FCachedFontMeshes>();

#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_SPARSE_DATA
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUText3DEngineSubsystem(); \
	friend struct Z_Construct_UClass_UText3DEngineSubsystem_Statics; \
public: \
	DECLARE_CLASS(UText3DEngineSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Text3D"), NO_API) \
	DECLARE_SERIALIZER(UText3DEngineSubsystem)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_INCLASS \
private: \
	static void StaticRegisterNativesUText3DEngineSubsystem(); \
	friend struct Z_Construct_UClass_UText3DEngineSubsystem_Statics; \
public: \
	DECLARE_CLASS(UText3DEngineSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Text3D"), NO_API) \
	DECLARE_SERIALIZER(UText3DEngineSubsystem)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UText3DEngineSubsystem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UText3DEngineSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UText3DEngineSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UText3DEngineSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UText3DEngineSubsystem(UText3DEngineSubsystem&&); \
	NO_API UText3DEngineSubsystem(const UText3DEngineSubsystem&); \
public:


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UText3DEngineSubsystem(UText3DEngineSubsystem&&); \
	NO_API UText3DEngineSubsystem(const UText3DEngineSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UText3DEngineSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UText3DEngineSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UText3DEngineSubsystem)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CachedFonts() { return STRUCT_OFFSET(UText3DEngineSubsystem, CachedFonts); }


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_70_PROLOG
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_SPARSE_DATA \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_INCLASS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_SPARSE_DATA \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h_73_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEXT3D_API UClass* StaticClass<class UText3DEngineSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Text3D_Source_Text3D_Private_Text3DEngineSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
