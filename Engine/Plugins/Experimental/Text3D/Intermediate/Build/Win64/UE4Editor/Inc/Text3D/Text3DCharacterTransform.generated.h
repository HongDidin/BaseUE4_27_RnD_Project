// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FRotator;
enum class EText3DCharacterEffectOrder : uint8;
struct FVector;
#ifdef TEXT3D_Text3DCharacterTransform_generated_h
#error "Text3DCharacterTransform.generated.h already included, missing '#pragma once' in Text3DCharacterTransform.h"
#endif
#define TEXT3D_Text3DCharacterTransform_generated_h

#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetRotateEnd); \
	DECLARE_FUNCTION(execSetRotateBegin); \
	DECLARE_FUNCTION(execSetRotateRange); \
	DECLARE_FUNCTION(execSetRotateOrder); \
	DECLARE_FUNCTION(execSetRotateProgress); \
	DECLARE_FUNCTION(execSetRotateEnabled); \
	DECLARE_FUNCTION(execSetScaleEnd); \
	DECLARE_FUNCTION(execSetScaleBegin); \
	DECLARE_FUNCTION(execSetScaleRange); \
	DECLARE_FUNCTION(execSetScaleOrder); \
	DECLARE_FUNCTION(execSetScaleProgress); \
	DECLARE_FUNCTION(execSetScaleEnabled); \
	DECLARE_FUNCTION(execSetLocationDistance); \
	DECLARE_FUNCTION(execSetLocationRange); \
	DECLARE_FUNCTION(execSetLocationOrder); \
	DECLARE_FUNCTION(execSetLocationProgress); \
	DECLARE_FUNCTION(execSetLocationEnabled);


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetRotateEnd); \
	DECLARE_FUNCTION(execSetRotateBegin); \
	DECLARE_FUNCTION(execSetRotateRange); \
	DECLARE_FUNCTION(execSetRotateOrder); \
	DECLARE_FUNCTION(execSetRotateProgress); \
	DECLARE_FUNCTION(execSetRotateEnabled); \
	DECLARE_FUNCTION(execSetScaleEnd); \
	DECLARE_FUNCTION(execSetScaleBegin); \
	DECLARE_FUNCTION(execSetScaleRange); \
	DECLARE_FUNCTION(execSetScaleOrder); \
	DECLARE_FUNCTION(execSetScaleProgress); \
	DECLARE_FUNCTION(execSetScaleEnabled); \
	DECLARE_FUNCTION(execSetLocationDistance); \
	DECLARE_FUNCTION(execSetLocationRange); \
	DECLARE_FUNCTION(execSetLocationOrder); \
	DECLARE_FUNCTION(execSetLocationProgress); \
	DECLARE_FUNCTION(execSetLocationEnabled);


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUText3DCharacterTransform(); \
	friend struct Z_Construct_UClass_UText3DCharacterTransform_Statics; \
public: \
	DECLARE_CLASS(UText3DCharacterTransform, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Text3D"), NO_API) \
	DECLARE_SERIALIZER(UText3DCharacterTransform)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUText3DCharacterTransform(); \
	friend struct Z_Construct_UClass_UText3DCharacterTransform_Statics; \
public: \
	DECLARE_CLASS(UText3DCharacterTransform, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Text3D"), NO_API) \
	DECLARE_SERIALIZER(UText3DCharacterTransform)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UText3DCharacterTransform(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UText3DCharacterTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UText3DCharacterTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UText3DCharacterTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UText3DCharacterTransform(UText3DCharacterTransform&&); \
	NO_API UText3DCharacterTransform(const UText3DCharacterTransform&); \
public:


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UText3DCharacterTransform(UText3DCharacterTransform&&); \
	NO_API UText3DCharacterTransform(const UText3DCharacterTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UText3DCharacterTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UText3DCharacterTransform); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UText3DCharacterTransform)


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_20_PROLOG
#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_INCLASS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEXT3D_API UClass* StaticClass<class UText3DCharacterTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Text3D_Source_Text3D_Public_Text3DCharacterTransform_h


#define FOREACH_ENUM_ETEXT3DCHARACTEREFFECTORDER(op) \
	op(EText3DCharacterEffectOrder::Normal) \
	op(EText3DCharacterEffectOrder::FromCenter) \
	op(EText3DCharacterEffectOrder::ToCenter) \
	op(EText3DCharacterEffectOrder::Opposite) 

enum class EText3DCharacterEffectOrder : uint8;
template<> TEXT3D_API UEnum* StaticEnum<EText3DCharacterEffectOrder>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
