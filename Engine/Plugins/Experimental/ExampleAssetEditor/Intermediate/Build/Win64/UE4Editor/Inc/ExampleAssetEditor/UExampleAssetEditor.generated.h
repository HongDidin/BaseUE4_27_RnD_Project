// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef EXAMPLEASSETEDITOR_UExampleAssetEditor_generated_h
#error "UExampleAssetEditor.generated.h already included, missing '#pragma once' in UExampleAssetEditor.h"
#endif
#define EXAMPLEASSETEDITOR_UExampleAssetEditor_generated_h

#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUExampleAssetEditor(); \
	friend struct Z_Construct_UClass_UExampleAssetEditor_Statics; \
public: \
	DECLARE_CLASS(UExampleAssetEditor, UAssetEditor, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ExampleAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UExampleAssetEditor)


#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUExampleAssetEditor(); \
	friend struct Z_Construct_UClass_UExampleAssetEditor_Statics; \
public: \
	DECLARE_CLASS(UExampleAssetEditor, UAssetEditor, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ExampleAssetEditor"), NO_API) \
	DECLARE_SERIALIZER(UExampleAssetEditor)


#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExampleAssetEditor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UExampleAssetEditor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExampleAssetEditor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExampleAssetEditor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExampleAssetEditor(UExampleAssetEditor&&); \
	NO_API UExampleAssetEditor(const UExampleAssetEditor&); \
public:


#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UExampleAssetEditor() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UExampleAssetEditor(UExampleAssetEditor&&); \
	NO_API UExampleAssetEditor(const UExampleAssetEditor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UExampleAssetEditor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UExampleAssetEditor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UExampleAssetEditor)


#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__InteractiveToolsContext() { return STRUCT_OFFSET(UExampleAssetEditor, InteractiveToolsContext); }


#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_12_PROLOG
#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_INCLASS \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> EXAMPLEASSETEDITOR_API UClass* StaticClass<class UExampleAssetEditor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ExampleAssetEditor_Source_ExampleAssetEditor_Public_UExampleAssetEditor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
