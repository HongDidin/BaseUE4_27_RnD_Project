// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheAbcFile/Private/ActorFactoryGeometryCacheAbcFile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryGeometryCacheAbcFile() {}
// Cross Module References
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_NoRegister();
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheAbcFile();
// End Cross Module References
	void UActorFactoryGeometryCacheAbcFile::StaticRegisterNativesUActorFactoryGeometryCacheAbcFile()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_NoRegister()
	{
		return UActorFactoryGeometryCacheAbcFile::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheAbcFile,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "ActorFactoryGeometryCacheAbcFile.h" },
		{ "ModuleRelativePath", "Private/ActorFactoryGeometryCacheAbcFile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryGeometryCacheAbcFile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics::ClassParams = {
		&UActorFactoryGeometryCacheAbcFile::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000030ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryGeometryCacheAbcFile, 1805485424);
	template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<UActorFactoryGeometryCacheAbcFile>()
	{
		return UActorFactoryGeometryCacheAbcFile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryGeometryCacheAbcFile(Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile, &UActorFactoryGeometryCacheAbcFile::StaticClass, TEXT("/Script/GeometryCacheAbcFile"), TEXT("UActorFactoryGeometryCacheAbcFile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryGeometryCacheAbcFile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
