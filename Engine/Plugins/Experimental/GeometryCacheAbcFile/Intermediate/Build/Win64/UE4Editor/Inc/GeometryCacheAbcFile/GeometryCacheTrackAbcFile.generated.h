// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYCACHEABCFILE_GeometryCacheTrackAbcFile_generated_h
#error "GeometryCacheTrackAbcFile.generated.h already included, missing '#pragma once' in GeometryCacheTrackAbcFile.h"
#endif
#define GEOMETRYCACHEABCFILE_GeometryCacheTrackAbcFile_generated_h

#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGeometryCacheTrackAbcFile(); \
	friend struct Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheTrackAbcFile, UGeometryCacheTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(UGeometryCacheTrackAbcFile)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUGeometryCacheTrackAbcFile(); \
	friend struct Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheTrackAbcFile, UGeometryCacheTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(UGeometryCacheTrackAbcFile)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGeometryCacheTrackAbcFile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGeometryCacheTrackAbcFile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGeometryCacheTrackAbcFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheTrackAbcFile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGeometryCacheTrackAbcFile(UGeometryCacheTrackAbcFile&&); \
	NO_API UGeometryCacheTrackAbcFile(const UGeometryCacheTrackAbcFile&); \
public:


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGeometryCacheTrackAbcFile(UGeometryCacheTrackAbcFile&&); \
	NO_API UGeometryCacheTrackAbcFile(const UGeometryCacheTrackAbcFile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGeometryCacheTrackAbcFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheTrackAbcFile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGeometryCacheTrackAbcFile)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_13_PROLOG
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_INCLASS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<class UGeometryCacheTrackAbcFile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheTrackAbcFile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
