// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYCACHEABCFILE_ActorFactoryGeometryCacheAbcFile_generated_h
#error "ActorFactoryGeometryCacheAbcFile.generated.h already included, missing '#pragma once' in ActorFactoryGeometryCacheAbcFile.h"
#endif
#define GEOMETRYCACHEABCFILE_ActorFactoryGeometryCacheAbcFile_generated_h

#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryGeometryCacheAbcFile(); \
	friend struct Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryGeometryCacheAbcFile, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(UActorFactoryGeometryCacheAbcFile)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryGeometryCacheAbcFile(); \
	friend struct Z_Construct_UClass_UActorFactoryGeometryCacheAbcFile_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryGeometryCacheAbcFile, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(UActorFactoryGeometryCacheAbcFile)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UActorFactoryGeometryCacheAbcFile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryGeometryCacheAbcFile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorFactoryGeometryCacheAbcFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryGeometryCacheAbcFile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorFactoryGeometryCacheAbcFile(UActorFactoryGeometryCacheAbcFile&&); \
	NO_API UActorFactoryGeometryCacheAbcFile(const UActorFactoryGeometryCacheAbcFile&); \
public:


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UActorFactoryGeometryCacheAbcFile(UActorFactoryGeometryCacheAbcFile&&); \
	NO_API UActorFactoryGeometryCacheAbcFile(const UActorFactoryGeometryCacheAbcFile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UActorFactoryGeometryCacheAbcFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryGeometryCacheAbcFile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UActorFactoryGeometryCacheAbcFile)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_11_PROLOG
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_INCLASS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<class UActorFactoryGeometryCacheAbcFile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_ActorFactoryGeometryCacheAbcFile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
