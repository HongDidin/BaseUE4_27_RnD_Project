// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheAbcFile/Private/GeometryCacheAbcFileActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCacheAbcFileActor() {}
// Cross Module References
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_AGeometryCacheAbcFileActor_NoRegister();
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_AGeometryCacheAbcFileActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheAbcFile();
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_UGeometryCacheAbcFileComponent_NoRegister();
// End Cross Module References
	void AGeometryCacheAbcFileActor::StaticRegisterNativesAGeometryCacheAbcFileActor()
	{
	}
	UClass* Z_Construct_UClass_AGeometryCacheAbcFileActor_NoRegister()
	{
		return AGeometryCacheAbcFileActor::StaticClass();
	}
	struct Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeometryCacheAbcFileComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GeometryCacheAbcFileComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheAbcFile,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** GeometryCacheAbcFile actor serves as a placeable actor for GeometryCache loading from an Alembic file */" },
		{ "IgnoreCategoryKeywordsInSubclasses", "true" },
		{ "IncludePath", "GeometryCacheAbcFileActor.h" },
		{ "ModuleRelativePath", "Private/GeometryCacheAbcFileActor.h" },
		{ "ToolTip", "GeometryCacheAbcFile actor serves as a placeable actor for GeometryCache loading from an Alembic file" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::NewProp_GeometryCacheAbcFileComponent_MetaData[] = {
		{ "Category", "GeometryCacheAbcFileActor" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/GeometryCacheAbcFileActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::NewProp_GeometryCacheAbcFileComponent = { "GeometryCacheAbcFileComponent", nullptr, (EPropertyFlags)0x00400000000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AGeometryCacheAbcFileActor, GeometryCacheAbcFileComponent), Z_Construct_UClass_UGeometryCacheAbcFileComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::NewProp_GeometryCacheAbcFileComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::NewProp_GeometryCacheAbcFileComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::NewProp_GeometryCacheAbcFileComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGeometryCacheAbcFileActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::ClassParams = {
		&AGeometryCacheAbcFileActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGeometryCacheAbcFileActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGeometryCacheAbcFileActor, 648436106);
	template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<AGeometryCacheAbcFileActor>()
	{
		return AGeometryCacheAbcFileActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGeometryCacheAbcFileActor(Z_Construct_UClass_AGeometryCacheAbcFileActor, &AGeometryCacheAbcFileActor::StaticClass, TEXT("/Script/GeometryCacheAbcFile"), TEXT("AGeometryCacheAbcFileActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGeometryCacheAbcFileActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
