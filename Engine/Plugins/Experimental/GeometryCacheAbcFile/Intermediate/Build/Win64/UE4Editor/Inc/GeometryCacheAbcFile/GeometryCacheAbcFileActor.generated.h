// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYCACHEABCFILE_GeometryCacheAbcFileActor_generated_h
#error "GeometryCacheAbcFileActor.generated.h already included, missing '#pragma once' in GeometryCacheAbcFileActor.h"
#endif
#define GEOMETRYCACHEABCFILE_GeometryCacheAbcFileActor_generated_h

#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGeometryCacheAbcFileActor(); \
	friend struct Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics; \
public: \
	DECLARE_CLASS(AGeometryCacheAbcFileActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(AGeometryCacheAbcFileActor)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAGeometryCacheAbcFileActor(); \
	friend struct Z_Construct_UClass_AGeometryCacheAbcFileActor_Statics; \
public: \
	DECLARE_CLASS(AGeometryCacheAbcFileActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(AGeometryCacheAbcFileActor)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGeometryCacheAbcFileActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGeometryCacheAbcFileActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGeometryCacheAbcFileActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGeometryCacheAbcFileActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGeometryCacheAbcFileActor(AGeometryCacheAbcFileActor&&); \
	NO_API AGeometryCacheAbcFileActor(const AGeometryCacheAbcFileActor&); \
public:


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGeometryCacheAbcFileActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGeometryCacheAbcFileActor(AGeometryCacheAbcFileActor&&); \
	NO_API AGeometryCacheAbcFileActor(const AGeometryCacheAbcFileActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGeometryCacheAbcFileActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGeometryCacheAbcFileActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGeometryCacheAbcFileActor)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__GeometryCacheAbcFileComponent() { return STRUCT_OFFSET(AGeometryCacheAbcFileActor, GeometryCacheAbcFileComponent); }


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_14_PROLOG
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_INCLASS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GeometryCacheAbcFileActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<class AGeometryCacheAbcFileActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Private_GeometryCacheAbcFileActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
