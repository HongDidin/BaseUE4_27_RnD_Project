// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheAbcFile/Public/GeometryCacheTrackAbcFile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCacheTrackAbcFile() {}
// Cross Module References
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_UGeometryCacheTrackAbcFile_NoRegister();
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_UGeometryCacheTrackAbcFile();
	GEOMETRYCACHE_API UClass* Z_Construct_UClass_UGeometryCacheTrack();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheAbcFile();
// End Cross Module References
	void UGeometryCacheTrackAbcFile::StaticRegisterNativesUGeometryCacheTrackAbcFile()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCacheTrackAbcFile_NoRegister()
	{
		return UGeometryCacheTrackAbcFile::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGeometryCacheTrack,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheAbcFile,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** GeometryCacheTrack for Alembic file querying */" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "GeometryCacheTrackAbcFile.h" },
		{ "ModuleRelativePath", "Public/GeometryCacheTrackAbcFile.h" },
		{ "ToolTip", "GeometryCacheTrack for Alembic file querying" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCacheTrackAbcFile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics::ClassParams = {
		&UGeometryCacheTrackAbcFile::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001020A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCacheTrackAbcFile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCacheTrackAbcFile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCacheTrackAbcFile, 3497167699);
	template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<UGeometryCacheTrackAbcFile>()
	{
		return UGeometryCacheTrackAbcFile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCacheTrackAbcFile(Z_Construct_UClass_UGeometryCacheTrackAbcFile, &UGeometryCacheTrackAbcFile::StaticClass, TEXT("/Script/GeometryCacheAbcFile"), TEXT("UGeometryCacheTrackAbcFile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCacheTrackAbcFile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
