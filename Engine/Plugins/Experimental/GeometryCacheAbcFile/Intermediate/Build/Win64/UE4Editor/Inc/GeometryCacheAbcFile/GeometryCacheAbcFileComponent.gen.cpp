// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheAbcFile/Public/GeometryCacheAbcFileComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCacheAbcFileComponent() {}
// Cross Module References
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_UGeometryCacheAbcFileComponent_NoRegister();
	GEOMETRYCACHEABCFILE_API UClass* Z_Construct_UClass_UGeometryCacheAbcFileComponent();
	GEOMETRYCACHE_API UClass* Z_Construct_UClass_UGeometryCacheComponent();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheAbcFile();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilePath();
	ALEMBICLIBRARY_API UScriptStruct* Z_Construct_UScriptStruct_FAbcSamplingSettings();
	ALEMBICLIBRARY_API UScriptStruct* Z_Construct_UScriptStruct_FAbcMaterialSettings();
	ALEMBICLIBRARY_API UScriptStruct* Z_Construct_UScriptStruct_FAbcConversionSettings();
	ALEMBICLIBRARY_API UScriptStruct* Z_Construct_UScriptStruct_FAbcNormalGenerationSettings();
	ALEMBICLIBRARY_API UClass* Z_Construct_UClass_UAbcImportSettings_NoRegister();
// End Cross Module References
	void UGeometryCacheAbcFileComponent::StaticRegisterNativesUGeometryCacheAbcFileComponent()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCacheAbcFileComponent_NoRegister()
	{
		return UGeometryCacheAbcFileComponent::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AlembicFilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AlembicFilePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SamplingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SamplingSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MaterialSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ConversionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ConversionSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalGenerationSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_NormalGenerationSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AbcSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AbcSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGeometryCacheComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheAbcFile,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Rendering Experimental" },
		{ "Comment", "/** GeometryCacheAbcFileComponent, encapsulates a transient GeometryCache asset instance that fetches its data from an Alembic file and implements functionality for rendering and playback */" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Geometry Cache Alembic File" },
		{ "HideCategories", "Object LOD Mobility Trigger" },
		{ "IncludePath", "GeometryCacheAbcFileComponent.h" },
		{ "ModuleRelativePath", "Public/GeometryCacheAbcFileComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "GeometryCacheAbcFileComponent, encapsulates a transient GeometryCache asset instance that fetches its data from an Alembic file and implements functionality for rendering and playback" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AlembicFilePath_MetaData[] = {
		{ "Category", "Alembic" },
		{ "FilePathFilter", "Alembic files (*.abc)|*.abc" },
		{ "ModuleRelativePath", "Public/GeometryCacheAbcFileComponent.h" },
		{ "RelativeToGameDir", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AlembicFilePath = { "AlembicFilePath", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGeometryCacheAbcFileComponent, AlembicFilePath), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AlembicFilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AlembicFilePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_SamplingSettings_MetaData[] = {
		{ "Category", "Alembic" },
		{ "ModuleRelativePath", "Public/GeometryCacheAbcFileComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_SamplingSettings = { "SamplingSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGeometryCacheAbcFileComponent, SamplingSettings), Z_Construct_UScriptStruct_FAbcSamplingSettings, METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_SamplingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_SamplingSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_MaterialSettings_MetaData[] = {
		{ "Category", "Alembic" },
		{ "ModuleRelativePath", "Public/GeometryCacheAbcFileComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_MaterialSettings = { "MaterialSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGeometryCacheAbcFileComponent, MaterialSettings), Z_Construct_UScriptStruct_FAbcMaterialSettings, METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_MaterialSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_MaterialSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_ConversionSettings_MetaData[] = {
		{ "Category", "Alembic" },
		{ "ModuleRelativePath", "Public/GeometryCacheAbcFileComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_ConversionSettings = { "ConversionSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGeometryCacheAbcFileComponent, ConversionSettings), Z_Construct_UScriptStruct_FAbcConversionSettings, METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_ConversionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_ConversionSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_NormalGenerationSettings_MetaData[] = {
		{ "Category", "Alembic" },
		{ "ModuleRelativePath", "Public/GeometryCacheAbcFileComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_NormalGenerationSettings = { "NormalGenerationSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGeometryCacheAbcFileComponent, NormalGenerationSettings), Z_Construct_UScriptStruct_FAbcNormalGenerationSettings, METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_NormalGenerationSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_NormalGenerationSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AbcSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/GeometryCacheAbcFileComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AbcSettings = { "AbcSettings", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGeometryCacheAbcFileComponent, AbcSettings), Z_Construct_UClass_UAbcImportSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AbcSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AbcSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AlembicFilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_SamplingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_MaterialSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_ConversionSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_NormalGenerationSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::NewProp_AbcSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCacheAbcFileComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::ClassParams = {
		&UGeometryCacheAbcFileComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCacheAbcFileComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCacheAbcFileComponent, 2377960308);
	template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<UGeometryCacheAbcFileComponent>()
	{
		return UGeometryCacheAbcFileComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCacheAbcFileComponent(Z_Construct_UClass_UGeometryCacheAbcFileComponent, &UGeometryCacheAbcFileComponent::StaticClass, TEXT("/Script/GeometryCacheAbcFile"), TEXT("UGeometryCacheAbcFileComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCacheAbcFileComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
