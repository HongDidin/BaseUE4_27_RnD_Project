// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GEOMETRYCACHEABCFILE_GeometryCacheAbcFileComponent_generated_h
#error "GeometryCacheAbcFileComponent.generated.h already included, missing '#pragma once' in GeometryCacheAbcFileComponent.h"
#endif
#define GEOMETRYCACHEABCFILE_GeometryCacheAbcFileComponent_generated_h

#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGeometryCacheAbcFileComponent(); \
	friend struct Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheAbcFileComponent, UGeometryCacheComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(UGeometryCacheAbcFileComponent)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUGeometryCacheAbcFileComponent(); \
	friend struct Z_Construct_UClass_UGeometryCacheAbcFileComponent_Statics; \
public: \
	DECLARE_CLASS(UGeometryCacheAbcFileComponent, UGeometryCacheComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GeometryCacheAbcFile"), NO_API) \
	DECLARE_SERIALIZER(UGeometryCacheAbcFileComponent)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGeometryCacheAbcFileComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGeometryCacheAbcFileComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGeometryCacheAbcFileComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheAbcFileComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGeometryCacheAbcFileComponent(UGeometryCacheAbcFileComponent&&); \
	NO_API UGeometryCacheAbcFileComponent(const UGeometryCacheAbcFileComponent&); \
public:


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGeometryCacheAbcFileComponent(UGeometryCacheAbcFileComponent&&); \
	NO_API UGeometryCacheAbcFileComponent(const UGeometryCacheAbcFileComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGeometryCacheAbcFileComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGeometryCacheAbcFileComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGeometryCacheAbcFileComponent)


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AbcSettings() { return STRUCT_OFFSET(UGeometryCacheAbcFileComponent, AbcSettings); }


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_14_PROLOG
#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_INCLASS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GEOMETRYCACHEABCFILE_API UClass* StaticClass<class UGeometryCacheAbcFileComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GeometryCacheAbcFile_Source_GeometryCacheAbcFile_Public_GeometryCacheAbcFileComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
