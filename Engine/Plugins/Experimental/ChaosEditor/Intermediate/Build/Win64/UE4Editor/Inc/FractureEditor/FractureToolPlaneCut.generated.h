// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolPlaneCut_generated_h
#error "FractureToolPlaneCut.generated.h already included, missing '#pragma once' in FractureToolPlaneCut.h"
#endif
#define FRACTUREEDITOR_FractureToolPlaneCut_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFracturePlaneCutSettings(); \
	friend struct Z_Construct_UClass_UFracturePlaneCutSettings_Statics; \
public: \
	DECLARE_CLASS(UFracturePlaneCutSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFracturePlaneCutSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUFracturePlaneCutSettings(); \
	friend struct Z_Construct_UClass_UFracturePlaneCutSettings_Statics; \
public: \
	DECLARE_CLASS(UFracturePlaneCutSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFracturePlaneCutSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFracturePlaneCutSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFracturePlaneCutSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFracturePlaneCutSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFracturePlaneCutSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFracturePlaneCutSettings(UFracturePlaneCutSettings&&); \
	NO_API UFracturePlaneCutSettings(const UFracturePlaneCutSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFracturePlaneCutSettings(UFracturePlaneCutSettings&&); \
	NO_API UFracturePlaneCutSettings(const UFracturePlaneCutSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFracturePlaneCutSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFracturePlaneCutSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFracturePlaneCutSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_12_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFracturePlaneCutSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolPlaneCut(); \
	friend struct Z_Construct_UClass_UFractureToolPlaneCut_Statics; \
public: \
	DECLARE_CLASS(UFractureToolPlaneCut, UFractureToolCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolPlaneCut)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolPlaneCut(); \
	friend struct Z_Construct_UClass_UFractureToolPlaneCut_Statics; \
public: \
	DECLARE_CLASS(UFractureToolPlaneCut, UFractureToolCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolPlaneCut)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolPlaneCut(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolPlaneCut) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolPlaneCut); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolPlaneCut); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolPlaneCut(UFractureToolPlaneCut&&); \
	NO_API UFractureToolPlaneCut(const UFractureToolPlaneCut&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolPlaneCut(UFractureToolPlaneCut&&); \
	NO_API UFractureToolPlaneCut(const UFractureToolPlaneCut&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolPlaneCut); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolPlaneCut); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolPlaneCut)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PlaneCutSettings() { return STRUCT_OFFSET(UFractureToolPlaneCut, PlaneCutSettings); }


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_33_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h_37_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolPlaneCut>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolPlaneCut_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
