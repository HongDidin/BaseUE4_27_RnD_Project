// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolProperties.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolProperties() {}
// Cross Module References
	FRACTUREEDITOR_API UEnum* Z_Construct_UEnum_FractureEditor_EDynamicStateOverrideEnum();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureInitialDynamicStateSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureInitialDynamicStateSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSetInitialDynamicState_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSetInitialDynamicState();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureModalTool();
// End Cross Module References
	static UEnum* EDynamicStateOverrideEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FractureEditor_EDynamicStateOverrideEnum, Z_Construct_UPackage__Script_FractureEditor(), TEXT("EDynamicStateOverrideEnum"));
		}
		return Singleton;
	}
	template<> FRACTUREEDITOR_API UEnum* StaticEnum<EDynamicStateOverrideEnum>()
	{
		return EDynamicStateOverrideEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EDynamicStateOverrideEnum(EDynamicStateOverrideEnum_StaticEnum, TEXT("/Script/FractureEditor"), TEXT("EDynamicStateOverrideEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FractureEditor_EDynamicStateOverrideEnum_Hash() { return 1460087426U; }
	UEnum* Z_Construct_UEnum_FractureEditor_EDynamicStateOverrideEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FractureEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EDynamicStateOverrideEnum"), 0, Get_Z_Construct_UEnum_FractureEditor_EDynamicStateOverrideEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EDynamicStateOverrideEnum::NoOverride", (int64)EDynamicStateOverrideEnum::NoOverride },
				{ "EDynamicStateOverrideEnum::Sleeping", (int64)EDynamicStateOverrideEnum::Sleeping },
				{ "EDynamicStateOverrideEnum::Kinematic", (int64)EDynamicStateOverrideEnum::Kinematic },
				{ "EDynamicStateOverrideEnum::Static", (int64)EDynamicStateOverrideEnum::Static },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Kinematic.Comment", "/*Chaos::EObjectStateType::Sleeping*/" },
				{ "Kinematic.DisplayName", "Kinematic" },
				{ "Kinematic.Name", "EDynamicStateOverrideEnum::Kinematic" },
				{ "Kinematic.ToolTip", "Chaos::EObjectStateType::Sleeping" },
				{ "ModuleRelativePath", "Private/FractureToolProperties.h" },
				{ "NoOverride.DisplayName", "No Override" },
				{ "NoOverride.Name", "EDynamicStateOverrideEnum::NoOverride" },
				{ "Sleeping.DisplayName", "Sleeping" },
				{ "Sleeping.Name", "EDynamicStateOverrideEnum::Sleeping" },
				{ "Static.Comment", "/*Chaos::EObjectStateType::Kinematic*/" },
				{ "Static.DisplayName", "Static" },
				{ "Static.Name", "EDynamicStateOverrideEnum::Static" },
				{ "Static.ToolTip", "Chaos::EObjectStateType::Kinematic" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FractureEditor,
				nullptr,
				"EDynamicStateOverrideEnum",
				"EDynamicStateOverrideEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UFractureInitialDynamicStateSettings::StaticRegisterNativesUFractureInitialDynamicStateSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureInitialDynamicStateSettings_NoRegister()
	{
		return UFractureInitialDynamicStateSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InitialDynamicState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialDynamicState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InitialDynamicState;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings specifically related to the one-time destructive fracturing of a mesh **/" },
		{ "IncludePath", "FractureToolProperties.h" },
		{ "ModuleRelativePath", "Private/FractureToolProperties.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Settings specifically related to the one-time destructive fracturing of a mesh *" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::NewProp_InitialDynamicState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::NewProp_InitialDynamicState_MetaData[] = {
		{ "Category", "SetInitialDynamicState" },
		{ "Comment", "/** Random number generator seed for repeatability */" },
		{ "DisplayName", "Initial Dynamic State" },
		{ "ModuleRelativePath", "Private/FractureToolProperties.h" },
		{ "ToolTip", "Random number generator seed for repeatability" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::NewProp_InitialDynamicState = { "InitialDynamicState", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureInitialDynamicStateSettings, InitialDynamicState), Z_Construct_UEnum_FractureEditor_EDynamicStateOverrideEnum, METADATA_PARAMS(Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::NewProp_InitialDynamicState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::NewProp_InitialDynamicState_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::NewProp_InitialDynamicState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::NewProp_InitialDynamicState,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureInitialDynamicStateSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::ClassParams = {
		&UFractureInitialDynamicStateSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureInitialDynamicStateSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureInitialDynamicStateSettings, 4209501061);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureInitialDynamicStateSettings>()
	{
		return UFractureInitialDynamicStateSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureInitialDynamicStateSettings(Z_Construct_UClass_UFractureInitialDynamicStateSettings, &UFractureInitialDynamicStateSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureInitialDynamicStateSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureInitialDynamicStateSettings);
	void UFractureToolSetInitialDynamicState::StaticRegisterNativesUFractureToolSetInitialDynamicState()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolSetInitialDynamicState_NoRegister()
	{
		return UFractureToolSetInitialDynamicState::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StateSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StateSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureModalTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "State" },
		{ "IncludePath", "FractureToolProperties.h" },
		{ "ModuleRelativePath", "Private/FractureToolProperties.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::NewProp_StateSettings_MetaData[] = {
		{ "Category", "InitialDynamicState" },
		{ "ModuleRelativePath", "Private/FractureToolProperties.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::NewProp_StateSettings = { "StateSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolSetInitialDynamicState, StateSettings), Z_Construct_UClass_UFractureInitialDynamicStateSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::NewProp_StateSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::NewProp_StateSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::NewProp_StateSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolSetInitialDynamicState>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::ClassParams = {
		&UFractureToolSetInitialDynamicState::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolSetInitialDynamicState()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolSetInitialDynamicState, 444837393);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolSetInitialDynamicState>()
	{
		return UFractureToolSetInitialDynamicState::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolSetInitialDynamicState(Z_Construct_UClass_UFractureToolSetInitialDynamicState, &UFractureToolSetInitialDynamicState::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolSetInitialDynamicState"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolSetInitialDynamicState);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
