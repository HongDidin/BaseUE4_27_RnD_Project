// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolClusterMagnet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolClusterMagnet() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureClusterMagnetSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureClusterMagnetSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolClusterMagnet_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolClusterMagnet();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureModalTool();
// End Cross Module References
	void UFractureClusterMagnetSettings::StaticRegisterNativesUFractureClusterMagnetSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureClusterMagnetSettings_NoRegister()
	{
		return UFractureClusterMagnetSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureClusterMagnetSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Iterations_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Iterations;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Cluster Magnet" },
		{ "IncludePath", "FractureToolClusterMagnet.h" },
		{ "ModuleRelativePath", "Private/FractureToolClusterMagnet.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::NewProp_Iterations_MetaData[] = {
		{ "Category", "ClusterMagnet" },
		{ "ClampMax", "500" },
		{ "ClampMin", "1" },
		{ "DisplayName", "Iterations" },
		{ "ModuleRelativePath", "Private/FractureToolClusterMagnet.h" },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::NewProp_Iterations = { "Iterations", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterMagnetSettings, Iterations), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::NewProp_Iterations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::NewProp_Iterations_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::NewProp_Iterations,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureClusterMagnetSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::ClassParams = {
		&UFractureClusterMagnetSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureClusterMagnetSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureClusterMagnetSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureClusterMagnetSettings, 1487457519);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureClusterMagnetSettings>()
	{
		return UFractureClusterMagnetSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureClusterMagnetSettings(Z_Construct_UClass_UFractureClusterMagnetSettings, &UFractureClusterMagnetSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureClusterMagnetSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureClusterMagnetSettings);
	void UFractureToolClusterMagnet::StaticRegisterNativesUFractureToolClusterMagnet()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolClusterMagnet_NoRegister()
	{
		return UFractureToolClusterMagnet::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolClusterMagnet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterMagnetSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ClusterMagnetSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolClusterMagnet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureModalTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolClusterMagnet_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "ClusterMagnet" },
		{ "IncludePath", "FractureToolClusterMagnet.h" },
		{ "ModuleRelativePath", "Private/FractureToolClusterMagnet.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolClusterMagnet_Statics::NewProp_ClusterMagnetSettings_MetaData[] = {
		{ "Category", "AutoCluster" },
		{ "ModuleRelativePath", "Private/FractureToolClusterMagnet.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolClusterMagnet_Statics::NewProp_ClusterMagnetSettings = { "ClusterMagnetSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolClusterMagnet, ClusterMagnetSettings), Z_Construct_UClass_UFractureClusterMagnetSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolClusterMagnet_Statics::NewProp_ClusterMagnetSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolClusterMagnet_Statics::NewProp_ClusterMagnetSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolClusterMagnet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolClusterMagnet_Statics::NewProp_ClusterMagnetSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolClusterMagnet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolClusterMagnet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolClusterMagnet_Statics::ClassParams = {
		&UFractureToolClusterMagnet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolClusterMagnet_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolClusterMagnet_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolClusterMagnet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolClusterMagnet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolClusterMagnet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolClusterMagnet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolClusterMagnet, 90490013);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolClusterMagnet>()
	{
		return UFractureToolClusterMagnet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolClusterMagnet(Z_Construct_UClass_UFractureToolClusterMagnet, &UFractureToolClusterMagnet::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolClusterMagnet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolClusterMagnet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
