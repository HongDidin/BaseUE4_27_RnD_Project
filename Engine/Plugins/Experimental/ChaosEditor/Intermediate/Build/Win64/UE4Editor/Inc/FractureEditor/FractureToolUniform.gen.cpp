// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolUniform.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolUniform() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureUniformSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureUniformSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolUniform_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolUniform();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolVoronoiCutterBase();
// End Cross Module References
	void UFractureUniformSettings::StaticRegisterNativesUFractureUniformSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureUniformSettings_NoRegister()
	{
		return UFractureUniformSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureUniformSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberVoronoiSitesMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberVoronoiSitesMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberVoronoiSitesMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberVoronoiSitesMax;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureUniformSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureUniformSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolUniform.h" },
		{ "ModuleRelativePath", "Private/FractureToolUniform.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMin_MetaData[] = {
		{ "Category", "UniformVoronoi" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Minimum Number of Voronoi sites - A random number will be chosen between the Min and Max for each bone you have selected */" },
		{ "DisplayName", "Minimum Voronoi Sites" },
		{ "ModuleRelativePath", "Private/FractureToolUniform.h" },
		{ "ToolTip", "Minimum Number of Voronoi sites - A random number will be chosen between the Min and Max for each bone you have selected" },
		{ "UIMax", "5000" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMin = { "NumberVoronoiSitesMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureUniformSettings, NumberVoronoiSitesMin), METADATA_PARAMS(Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMax_MetaData[] = {
		{ "Category", "UniformVoronoi" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum Number of Voronoi sites - A random number will be chosen between the Min and Max for each bone you have selected */" },
		{ "DisplayName", "Maximum Voronoi Sites" },
		{ "ModuleRelativePath", "Private/FractureToolUniform.h" },
		{ "ToolTip", "Maximum Number of Voronoi sites - A random number will be chosen between the Min and Max for each bone you have selected" },
		{ "UIMax", "5000" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMax = { "NumberVoronoiSitesMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureUniformSettings, NumberVoronoiSitesMax), METADATA_PARAMS(Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMax_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureUniformSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureUniformSettings_Statics::NewProp_NumberVoronoiSitesMax,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureUniformSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureUniformSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureUniformSettings_Statics::ClassParams = {
		&UFractureUniformSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureUniformSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureUniformSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureUniformSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureUniformSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureUniformSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureUniformSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureUniformSettings, 2820510990);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureUniformSettings>()
	{
		return UFractureUniformSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureUniformSettings(Z_Construct_UClass_UFractureUniformSettings, &UFractureUniformSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureUniformSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureUniformSettings);
	void UFractureToolUniform::StaticRegisterNativesUFractureToolUniform()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolUniform_NoRegister()
	{
		return UFractureToolUniform::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolUniform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UniformSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UniformSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolUniform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolVoronoiCutterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolUniform_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Uniform Voronoi" },
		{ "IncludePath", "FractureToolUniform.h" },
		{ "ModuleRelativePath", "Private/FractureToolUniform.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolUniform_Statics::NewProp_UniformSettings_MetaData[] = {
		{ "Category", "Uniform" },
		{ "Comment", "// Uniform Voronoi Fracture Input Settings\n" },
		{ "ModuleRelativePath", "Private/FractureToolUniform.h" },
		{ "ToolTip", "Uniform Voronoi Fracture Input Settings" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolUniform_Statics::NewProp_UniformSettings = { "UniformSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolUniform, UniformSettings), Z_Construct_UClass_UFractureUniformSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolUniform_Statics::NewProp_UniformSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolUniform_Statics::NewProp_UniformSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolUniform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolUniform_Statics::NewProp_UniformSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolUniform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolUniform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolUniform_Statics::ClassParams = {
		&UFractureToolUniform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolUniform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolUniform_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolUniform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolUniform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolUniform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolUniform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolUniform, 3736826318);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolUniform>()
	{
		return UFractureToolUniform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolUniform(Z_Construct_UClass_UFractureToolUniform, &UFractureToolUniform::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolUniform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolUniform);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
