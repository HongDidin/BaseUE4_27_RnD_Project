// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolEmbed.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolEmbed() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolAddEmbeddedGeometry();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureActionTool();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry();
// End Cross Module References
	void UFractureToolAddEmbeddedGeometry::StaticRegisterNativesUFractureToolAddEmbeddedGeometry()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_NoRegister()
	{
		return UFractureToolAddEmbeddedGeometry::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolEmbed.h" },
		{ "ModuleRelativePath", "Private/FractureToolEmbed.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolAddEmbeddedGeometry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics::ClassParams = {
		&UFractureToolAddEmbeddedGeometry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolAddEmbeddedGeometry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolAddEmbeddedGeometry, 4082387370);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolAddEmbeddedGeometry>()
	{
		return UFractureToolAddEmbeddedGeometry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolAddEmbeddedGeometry(Z_Construct_UClass_UFractureToolAddEmbeddedGeometry, &UFractureToolAddEmbeddedGeometry::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolAddEmbeddedGeometry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolAddEmbeddedGeometry);
	void UFractureToolDeleteEmbeddedGeometry::StaticRegisterNativesUFractureToolDeleteEmbeddedGeometry()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_NoRegister()
	{
		return UFractureToolDeleteEmbeddedGeometry::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolEmbed.h" },
		{ "ModuleRelativePath", "Private/FractureToolEmbed.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolDeleteEmbeddedGeometry>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics::ClassParams = {
		&UFractureToolDeleteEmbeddedGeometry::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolDeleteEmbeddedGeometry, 2248744103);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolDeleteEmbeddedGeometry>()
	{
		return UFractureToolDeleteEmbeddedGeometry::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolDeleteEmbeddedGeometry(Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry, &UFractureToolDeleteEmbeddedGeometry::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolDeleteEmbeddedGeometry"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolDeleteEmbeddedGeometry);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
