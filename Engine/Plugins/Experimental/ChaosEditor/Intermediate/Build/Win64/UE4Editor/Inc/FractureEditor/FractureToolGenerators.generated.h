// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolGenerators_generated_h
#error "FractureToolGenerators.generated.h already included, missing '#pragma once' in FractureToolGenerators.h"
#endif
#define FRACTUREEDITOR_FractureToolGenerators_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolGenerateAsset(); \
	friend struct Z_Construct_UClass_UFractureToolGenerateAsset_Statics; \
public: \
	DECLARE_CLASS(UFractureToolGenerateAsset, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolGenerateAsset)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolGenerateAsset(); \
	friend struct Z_Construct_UClass_UFractureToolGenerateAsset_Statics; \
public: \
	DECLARE_CLASS(UFractureToolGenerateAsset, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolGenerateAsset)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolGenerateAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolGenerateAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolGenerateAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolGenerateAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolGenerateAsset(UFractureToolGenerateAsset&&); \
	NO_API UFractureToolGenerateAsset(const UFractureToolGenerateAsset&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolGenerateAsset(UFractureToolGenerateAsset&&); \
	NO_API UFractureToolGenerateAsset(const UFractureToolGenerateAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolGenerateAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolGenerateAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolGenerateAsset)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AssetPath() { return STRUCT_OFFSET(UFractureToolGenerateAsset, AssetPath); }


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_12_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolGenerateAsset>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolResetAsset(); \
	friend struct Z_Construct_UClass_UFractureToolResetAsset_Statics; \
public: \
	DECLARE_CLASS(UFractureToolResetAsset, UFractureToolGenerateAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolResetAsset)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolResetAsset(); \
	friend struct Z_Construct_UClass_UFractureToolResetAsset_Statics; \
public: \
	DECLARE_CLASS(UFractureToolResetAsset, UFractureToolGenerateAsset, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolResetAsset)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolResetAsset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolResetAsset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolResetAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolResetAsset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolResetAsset(UFractureToolResetAsset&&); \
	NO_API UFractureToolResetAsset(const UFractureToolResetAsset&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolResetAsset(UFractureToolResetAsset&&); \
	NO_API UFractureToolResetAsset(const UFractureToolResetAsset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolResetAsset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolResetAsset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolResetAsset)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_47_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h_51_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolResetAsset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolGenerators_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
