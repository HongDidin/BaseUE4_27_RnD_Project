// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolClustering_generated_h
#error "FractureToolClustering.generated.h already included, missing '#pragma once' in FractureToolClustering.h"
#endif
#define FRACTUREEDITOR_FractureToolClustering_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolFlattenAll(); \
	friend struct Z_Construct_UClass_UFractureToolFlattenAll_Statics; \
public: \
	DECLARE_CLASS(UFractureToolFlattenAll, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolFlattenAll)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolFlattenAll(); \
	friend struct Z_Construct_UClass_UFractureToolFlattenAll_Statics; \
public: \
	DECLARE_CLASS(UFractureToolFlattenAll, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolFlattenAll)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolFlattenAll(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolFlattenAll) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolFlattenAll); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolFlattenAll); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolFlattenAll(UFractureToolFlattenAll&&); \
	NO_API UFractureToolFlattenAll(const UFractureToolFlattenAll&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolFlattenAll(UFractureToolFlattenAll&&); \
	NO_API UFractureToolFlattenAll(const UFractureToolFlattenAll&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolFlattenAll); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolFlattenAll); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolFlattenAll)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_10_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_14_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolFlattenAll>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolCluster(); \
	friend struct Z_Construct_UClass_UFractureToolCluster_Statics; \
public: \
	DECLARE_CLASS(UFractureToolCluster, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolCluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolCluster(); \
	friend struct Z_Construct_UClass_UFractureToolCluster_Statics; \
public: \
	DECLARE_CLASS(UFractureToolCluster, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolCluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolCluster(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolCluster) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolCluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolCluster); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolCluster(UFractureToolCluster&&); \
	NO_API UFractureToolCluster(const UFractureToolCluster&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolCluster(UFractureToolCluster&&); \
	NO_API UFractureToolCluster(const UFractureToolCluster&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolCluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolCluster); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolCluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_27_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_31_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolCluster>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolUncluster(); \
	friend struct Z_Construct_UClass_UFractureToolUncluster_Statics; \
public: \
	DECLARE_CLASS(UFractureToolUncluster, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolUncluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolUncluster(); \
	friend struct Z_Construct_UClass_UFractureToolUncluster_Statics; \
public: \
	DECLARE_CLASS(UFractureToolUncluster, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolUncluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolUncluster(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolUncluster) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolUncluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolUncluster); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolUncluster(UFractureToolUncluster&&); \
	NO_API UFractureToolUncluster(const UFractureToolUncluster&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolUncluster(UFractureToolUncluster&&); \
	NO_API UFractureToolUncluster(const UFractureToolUncluster&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolUncluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolUncluster); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolUncluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_44_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_48_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolUncluster>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolMoveUp(); \
	friend struct Z_Construct_UClass_UFractureToolMoveUp_Statics; \
public: \
	DECLARE_CLASS(UFractureToolMoveUp, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolMoveUp)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolMoveUp(); \
	friend struct Z_Construct_UClass_UFractureToolMoveUp_Statics; \
public: \
	DECLARE_CLASS(UFractureToolMoveUp, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolMoveUp)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolMoveUp(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolMoveUp) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolMoveUp); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolMoveUp); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolMoveUp(UFractureToolMoveUp&&); \
	NO_API UFractureToolMoveUp(const UFractureToolMoveUp&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolMoveUp(UFractureToolMoveUp&&); \
	NO_API UFractureToolMoveUp(const UFractureToolMoveUp&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolMoveUp); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolMoveUp); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolMoveUp)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_61_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h_65_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolMoveUp>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClustering_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
