// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolBrick.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolBrick() {}
// Cross Module References
	FRACTUREEDITOR_API UEnum* Z_Construct_UEnum_FractureEditor_EFractureBrickBond();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UEnum* Z_Construct_UEnum_FractureEditor_EFractureBrickProjection();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureBrickSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureBrickSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolBrick_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolBrick();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolCutterBase();
// End Cross Module References
	static UEnum* EFractureBrickBond_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FractureEditor_EFractureBrickBond, Z_Construct_UPackage__Script_FractureEditor(), TEXT("EFractureBrickBond"));
		}
		return Singleton;
	}
	template<> FRACTUREEDITOR_API UEnum* StaticEnum<EFractureBrickBond>()
	{
		return EFractureBrickBond_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFractureBrickBond(EFractureBrickBond_StaticEnum, TEXT("/Script/FractureEditor"), TEXT("EFractureBrickBond"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FractureEditor_EFractureBrickBond_Hash() { return 1580186439U; }
	UEnum* Z_Construct_UEnum_FractureEditor_EFractureBrickBond()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FractureEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFractureBrickBond"), 0, Get_Z_Construct_UEnum_FractureEditor_EFractureBrickBond_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFractureBrickBond::Stretcher", (int64)EFractureBrickBond::Stretcher },
				{ "EFractureBrickBond::Stack", (int64)EFractureBrickBond::Stack },
				{ "EFractureBrickBond::English", (int64)EFractureBrickBond::English },
				{ "EFractureBrickBond::Header", (int64)EFractureBrickBond::Header },
				{ "EFractureBrickBond::Flemish", (int64)EFractureBrickBond::Flemish },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Brick Projection Directions*/" },
				{ "English.DisplayName", "English" },
				{ "English.Name", "EFractureBrickBond::English" },
				{ "Flemish.DisplayName", "Flemish" },
				{ "Flemish.Name", "EFractureBrickBond::Flemish" },
				{ "Header.DisplayName", "Header" },
				{ "Header.Name", "EFractureBrickBond::Header" },
				{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
				{ "Stack.DisplayName", "Stack" },
				{ "Stack.Name", "EFractureBrickBond::Stack" },
				{ "Stretcher.DisplayName", "Stretcher" },
				{ "Stretcher.Name", "EFractureBrickBond::Stretcher" },
				{ "ToolTip", "Brick Projection Directions" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FractureEditor,
				nullptr,
				"EFractureBrickBond",
				"EFractureBrickBond",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EFractureBrickProjection_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FractureEditor_EFractureBrickProjection, Z_Construct_UPackage__Script_FractureEditor(), TEXT("EFractureBrickProjection"));
		}
		return Singleton;
	}
	template<> FRACTUREEDITOR_API UEnum* StaticEnum<EFractureBrickProjection>()
	{
		return EFractureBrickProjection_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EFractureBrickProjection(EFractureBrickProjection_StaticEnum, TEXT("/Script/FractureEditor"), TEXT("EFractureBrickProjection"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FractureEditor_EFractureBrickProjection_Hash() { return 1952586061U; }
	UEnum* Z_Construct_UEnum_FractureEditor_EFractureBrickProjection()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FractureEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EFractureBrickProjection"), 0, Get_Z_Construct_UEnum_FractureEditor_EFractureBrickProjection_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EFractureBrickProjection::X", (int64)EFractureBrickProjection::X },
				{ "EFractureBrickProjection::Y", (int64)EFractureBrickProjection::Y },
				{ "EFractureBrickProjection::Z", (int64)EFractureBrickProjection::Z },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Brick Projection Directions*/" },
				{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
				{ "ToolTip", "Brick Projection Directions" },
				{ "X.DisplayName", "X" },
				{ "X.Name", "EFractureBrickProjection::X" },
				{ "Y.DisplayName", "Y" },
				{ "Y.Name", "EFractureBrickProjection::Y" },
				{ "Z.DisplayName", "Z" },
				{ "Z.Name", "EFractureBrickProjection::Z" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FractureEditor,
				nullptr,
				"EFractureBrickProjection",
				"EFractureBrickProjection",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UFractureBrickSettings::StaticRegisterNativesUFractureBrickSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureBrickSettings_NoRegister()
	{
		return UFractureBrickSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureBrickSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Bond_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bond_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Bond;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Forward_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Forward_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Forward;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Up_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Up_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Up;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrickLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BrickLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrickHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BrickHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrickDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BrickDepth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureBrickSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureBrickSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolBrick.h" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Bond_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Bond_MetaData[] = {
		{ "Category", "Brick" },
		{ "Comment", "/** Forward Direction to project brick pattern. */" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ToolTip", "Forward Direction to project brick pattern." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Bond = { "Bond", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureBrickSettings, Bond), Z_Construct_UEnum_FractureEditor_EFractureBrickBond, METADATA_PARAMS(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Bond_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Bond_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Forward_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Forward_MetaData[] = {
		{ "Category", "Brick" },
		{ "Comment", "/** Forward Direction to project brick pattern. */" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ToolTip", "Forward Direction to project brick pattern." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Forward = { "Forward", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureBrickSettings, Forward), Z_Construct_UEnum_FractureEditor_EFractureBrickProjection, METADATA_PARAMS(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Forward_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Forward_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Up_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Up_MetaData[] = {
		{ "Category", "Brick" },
		{ "Comment", "/** Up Direction for vertical brick slices. */" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ToolTip", "Up Direction for vertical brick slices." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Up = { "Up", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureBrickSettings, Up), Z_Construct_UEnum_FractureEditor_EFractureBrickProjection, METADATA_PARAMS(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Up_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Up_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickLength_MetaData[] = {
		{ "Category", "Brick" },
		{ "ClampMin", "0.001" },
		{ "Comment", "/** Brick length */" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ToolTip", "Brick length" },
		{ "UIMax", "500.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickLength = { "BrickLength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureBrickSettings, BrickLength), METADATA_PARAMS(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickHeight_MetaData[] = {
		{ "Category", "Brick" },
		{ "ClampMin", "0.001" },
		{ "Comment", "/** Brick Height */" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ToolTip", "Brick Height" },
		{ "UIMax", "500.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickHeight = { "BrickHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureBrickSettings, BrickHeight), METADATA_PARAMS(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickDepth_MetaData[] = {
		{ "Category", "Brick" },
		{ "ClampMin", "0.001" },
		{ "Comment", "/** Brick Height */" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ToolTip", "Brick Height" },
		{ "UIMax", "500.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickDepth = { "BrickDepth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureBrickSettings, BrickDepth), METADATA_PARAMS(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickDepth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureBrickSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Bond_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Bond,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Forward_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Forward,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Up_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_Up,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureBrickSettings_Statics::NewProp_BrickDepth,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureBrickSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureBrickSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureBrickSettings_Statics::ClassParams = {
		&UFractureBrickSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureBrickSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureBrickSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureBrickSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureBrickSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureBrickSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureBrickSettings, 4189414507);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureBrickSettings>()
	{
		return UFractureBrickSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureBrickSettings(Z_Construct_UClass_UFractureBrickSettings, &UFractureBrickSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureBrickSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureBrickSettings);
	void UFractureToolBrick::StaticRegisterNativesUFractureToolBrick()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolBrick_NoRegister()
	{
		return UFractureToolBrick::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolBrick_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrickSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrickSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolBrick_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolCutterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolBrick_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Brick" },
		{ "IncludePath", "FractureToolBrick.h" },
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolBrick_Statics::NewProp_BrickSettings_MetaData[] = {
		{ "ModuleRelativePath", "Private/FractureToolBrick.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolBrick_Statics::NewProp_BrickSettings = { "BrickSettings", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolBrick, BrickSettings), Z_Construct_UClass_UFractureBrickSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolBrick_Statics::NewProp_BrickSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolBrick_Statics::NewProp_BrickSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolBrick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolBrick_Statics::NewProp_BrickSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolBrick_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolBrick>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolBrick_Statics::ClassParams = {
		&UFractureToolBrick::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolBrick_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolBrick_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolBrick_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolBrick_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolBrick()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolBrick_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolBrick, 3463788978);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolBrick>()
	{
		return UFractureToolBrick::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolBrick(Z_Construct_UClass_UFractureToolBrick, &UFractureToolBrick::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolBrick"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolBrick);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
