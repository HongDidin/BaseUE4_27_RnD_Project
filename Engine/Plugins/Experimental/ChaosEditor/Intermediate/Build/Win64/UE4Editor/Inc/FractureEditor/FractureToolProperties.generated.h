// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolProperties_generated_h
#error "FractureToolProperties.generated.h already included, missing '#pragma once' in FractureToolProperties.h"
#endif
#define FRACTUREEDITOR_FractureToolProperties_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureInitialDynamicStateSettings(); \
	friend struct Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureInitialDynamicStateSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureInitialDynamicStateSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUFractureInitialDynamicStateSettings(); \
	friend struct Z_Construct_UClass_UFractureInitialDynamicStateSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureInitialDynamicStateSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureInitialDynamicStateSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureInitialDynamicStateSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureInitialDynamicStateSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureInitialDynamicStateSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureInitialDynamicStateSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureInitialDynamicStateSettings(UFractureInitialDynamicStateSettings&&); \
	NO_API UFractureInitialDynamicStateSettings(const UFractureInitialDynamicStateSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureInitialDynamicStateSettings(UFractureInitialDynamicStateSettings&&); \
	NO_API UFractureInitialDynamicStateSettings(const UFractureInitialDynamicStateSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureInitialDynamicStateSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureInitialDynamicStateSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureInitialDynamicStateSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_19_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_24_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureInitialDynamicStateSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolSetInitialDynamicState(); \
	friend struct Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics; \
public: \
	DECLARE_CLASS(UFractureToolSetInitialDynamicState, UFractureModalTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolSetInitialDynamicState)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolSetInitialDynamicState(); \
	friend struct Z_Construct_UClass_UFractureToolSetInitialDynamicState_Statics; \
public: \
	DECLARE_CLASS(UFractureToolSetInitialDynamicState, UFractureModalTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolSetInitialDynamicState)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolSetInitialDynamicState(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolSetInitialDynamicState) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolSetInitialDynamicState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolSetInitialDynamicState); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolSetInitialDynamicState(UFractureToolSetInitialDynamicState&&); \
	NO_API UFractureToolSetInitialDynamicState(const UFractureToolSetInitialDynamicState&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolSetInitialDynamicState(UFractureToolSetInitialDynamicState&&); \
	NO_API UFractureToolSetInitialDynamicState(const UFractureToolSetInitialDynamicState&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolSetInitialDynamicState); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolSetInitialDynamicState); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolSetInitialDynamicState)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_38_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h_42_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolSetInitialDynamicState>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolProperties_h


#define FOREACH_ENUM_EDYNAMICSTATEOVERRIDEENUM(op) \
	op(EDynamicStateOverrideEnum::NoOverride) \
	op(EDynamicStateOverrideEnum::Sleeping) \
	op(EDynamicStateOverrideEnum::Kinematic) \
	op(EDynamicStateOverrideEnum::Static) 

enum class EDynamicStateOverrideEnum : uint8;
template<> FRACTUREEDITOR_API UEnum* StaticEnum<EDynamicStateOverrideEnum>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
