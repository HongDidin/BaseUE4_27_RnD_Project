// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolAutoCluster_generated_h
#error "FractureToolAutoCluster.generated.h already included, missing '#pragma once' in FractureToolAutoCluster.h"
#endif
#define FRACTUREEDITOR_FractureToolAutoCluster_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureAutoClusterSettings(); \
	friend struct Z_Construct_UClass_UFractureAutoClusterSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureAutoClusterSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureAutoClusterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUFractureAutoClusterSettings(); \
	friend struct Z_Construct_UClass_UFractureAutoClusterSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureAutoClusterSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureAutoClusterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureAutoClusterSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureAutoClusterSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureAutoClusterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureAutoClusterSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureAutoClusterSettings(UFractureAutoClusterSettings&&); \
	NO_API UFractureAutoClusterSettings(const UFractureAutoClusterSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureAutoClusterSettings(UFractureAutoClusterSettings&&); \
	NO_API UFractureAutoClusterSettings(const UFractureAutoClusterSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureAutoClusterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureAutoClusterSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureAutoClusterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_11_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureAutoClusterSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolAutoCluster(); \
	friend struct Z_Construct_UClass_UFractureToolAutoCluster_Statics; \
public: \
	DECLARE_CLASS(UFractureToolAutoCluster, UFractureModalTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolAutoCluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolAutoCluster(); \
	friend struct Z_Construct_UClass_UFractureToolAutoCluster_Statics; \
public: \
	DECLARE_CLASS(UFractureToolAutoCluster, UFractureModalTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolAutoCluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolAutoCluster(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolAutoCluster) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolAutoCluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolAutoCluster); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolAutoCluster(UFractureToolAutoCluster&&); \
	NO_API UFractureToolAutoCluster(const UFractureToolAutoCluster&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolAutoCluster(UFractureToolAutoCluster&&); \
	NO_API UFractureToolAutoCluster(const UFractureToolAutoCluster&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolAutoCluster); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolAutoCluster); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolAutoCluster)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_34_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h_38_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolAutoCluster>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolAutoCluster_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
