// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_SGeometryCollectionOutliner_generated_h
#error "SGeometryCollectionOutliner.generated.h already included, missing '#pragma once' in SGeometryCollectionOutliner.h"
#endif
#define FRACTUREEDITOR_SGeometryCollectionOutliner_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUOutlinerSettings(); \
	friend struct Z_Construct_UClass_UOutlinerSettings_Statics; \
public: \
	DECLARE_CLASS(UOutlinerSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UOutlinerSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_INCLASS \
private: \
	static void StaticRegisterNativesUOutlinerSettings(); \
	friend struct Z_Construct_UClass_UOutlinerSettings_Statics; \
public: \
	DECLARE_CLASS(UOutlinerSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UOutlinerSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UOutlinerSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOutlinerSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOutlinerSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOutlinerSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOutlinerSettings(UOutlinerSettings&&); \
	NO_API UOutlinerSettings(const UOutlinerSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UOutlinerSettings(UOutlinerSettings&&); \
	NO_API UOutlinerSettings(const UOutlinerSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UOutlinerSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UOutlinerSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UOutlinerSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_38_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h_42_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UOutlinerSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_SGeometryCollectionOutliner_h


#define FOREACH_ENUM_EOUTLINERITEMNAMEENUM(op) \
	op(EOutlinerItemNameEnum::BoneName) \
	op(EOutlinerItemNameEnum::BoneIndex) 

enum class EOutlinerItemNameEnum : uint8;
template<> FRACTUREEDITOR_API UEnum* StaticEnum<EOutlinerItemNameEnum>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
