// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/SGeometryCollectionOutliner.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSGeometryCollectionOutliner() {}
// Cross Module References
	FRACTUREEDITOR_API UEnum* Z_Construct_UEnum_FractureEditor_EOutlinerItemNameEnum();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UOutlinerSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UOutlinerSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
	static UEnum* EOutlinerItemNameEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_FractureEditor_EOutlinerItemNameEnum, Z_Construct_UPackage__Script_FractureEditor(), TEXT("EOutlinerItemNameEnum"));
		}
		return Singleton;
	}
	template<> FRACTUREEDITOR_API UEnum* StaticEnum<EOutlinerItemNameEnum>()
	{
		return EOutlinerItemNameEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOutlinerItemNameEnum(EOutlinerItemNameEnum_StaticEnum, TEXT("/Script/FractureEditor"), TEXT("EOutlinerItemNameEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_FractureEditor_EOutlinerItemNameEnum_Hash() { return 148601900U; }
	UEnum* Z_Construct_UEnum_FractureEditor_EOutlinerItemNameEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_FractureEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOutlinerItemNameEnum"), 0, Get_Z_Construct_UEnum_FractureEditor_EOutlinerItemNameEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOutlinerItemNameEnum::BoneName", (int64)EOutlinerItemNameEnum::BoneName },
				{ "EOutlinerItemNameEnum::BoneIndex", (int64)EOutlinerItemNameEnum::BoneIndex },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "BoneIndex.DisplayName", "Bone Index" },
				{ "BoneIndex.Name", "EOutlinerItemNameEnum::BoneIndex" },
				{ "BoneName.DisplayName", "Bone Name" },
				{ "BoneName.Name", "EOutlinerItemNameEnum::BoneName" },
				{ "ModuleRelativePath", "Private/SGeometryCollectionOutliner.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_FractureEditor,
				nullptr,
				"EOutlinerItemNameEnum",
				"EOutlinerItemNameEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UOutlinerSettings::StaticRegisterNativesUOutlinerSettings()
	{
	}
	UClass* Z_Construct_UClass_UOutlinerSettings_NoRegister()
	{
		return UOutlinerSettings::StaticClass();
	}
	struct Z_Construct_UClass_UOutlinerSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ItemText_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ItemText_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ItemText;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOutlinerSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOutlinerSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings for Outliner configuration. **/" },
		{ "IncludePath", "SGeometryCollectionOutliner.h" },
		{ "ModuleRelativePath", "Private/SGeometryCollectionOutliner.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Settings for Outliner configuration. *" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UOutlinerSettings_Statics::NewProp_ItemText_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOutlinerSettings_Statics::NewProp_ItemText_MetaData[] = {
		{ "Category", "OutlinerSettings" },
		{ "Comment", "/** What is displayed in Outliner text */" },
		{ "DisplayName", "Item Text" },
		{ "ModuleRelativePath", "Private/SGeometryCollectionOutliner.h" },
		{ "ToolTip", "What is displayed in Outliner text" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UOutlinerSettings_Statics::NewProp_ItemText = { "ItemText", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOutlinerSettings, ItemText), Z_Construct_UEnum_FractureEditor_EOutlinerItemNameEnum, METADATA_PARAMS(Z_Construct_UClass_UOutlinerSettings_Statics::NewProp_ItemText_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOutlinerSettings_Statics::NewProp_ItemText_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOutlinerSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOutlinerSettings_Statics::NewProp_ItemText_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOutlinerSettings_Statics::NewProp_ItemText,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOutlinerSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOutlinerSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOutlinerSettings_Statics::ClassParams = {
		&UOutlinerSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOutlinerSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOutlinerSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOutlinerSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOutlinerSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOutlinerSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOutlinerSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOutlinerSettings, 3155390293);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UOutlinerSettings>()
	{
		return UOutlinerSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOutlinerSettings(Z_Construct_UClass_UOutlinerSettings, &UOutlinerSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UOutlinerSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOutlinerSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
