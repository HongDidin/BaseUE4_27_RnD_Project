// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolClustering.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolClustering() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolFlattenAll_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolFlattenAll();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureActionTool();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolCluster_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolCluster();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolUncluster_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolUncluster();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolMoveUp_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolMoveUp();
// End Cross Module References
	void UFractureToolFlattenAll::StaticRegisterNativesUFractureToolFlattenAll()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolFlattenAll_NoRegister()
	{
		return UFractureToolFlattenAll::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolFlattenAll_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolFlattenAll_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolFlattenAll_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Flatten All" },
		{ "IncludePath", "FractureToolClustering.h" },
		{ "ModuleRelativePath", "Private/FractureToolClustering.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolFlattenAll_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolFlattenAll>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolFlattenAll_Statics::ClassParams = {
		&UFractureToolFlattenAll::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolFlattenAll_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolFlattenAll_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolFlattenAll()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolFlattenAll_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolFlattenAll, 1698402443);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolFlattenAll>()
	{
		return UFractureToolFlattenAll::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolFlattenAll(Z_Construct_UClass_UFractureToolFlattenAll, &UFractureToolFlattenAll::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolFlattenAll"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolFlattenAll);
	void UFractureToolCluster::StaticRegisterNativesUFractureToolCluster()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolCluster_NoRegister()
	{
		return UFractureToolCluster::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolCluster_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolCluster_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolCluster_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Cluster" },
		{ "IncludePath", "FractureToolClustering.h" },
		{ "ModuleRelativePath", "Private/FractureToolClustering.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolCluster_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolCluster>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolCluster_Statics::ClassParams = {
		&UFractureToolCluster::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolCluster_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolCluster_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolCluster()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolCluster_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolCluster, 560660209);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolCluster>()
	{
		return UFractureToolCluster::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolCluster(Z_Construct_UClass_UFractureToolCluster, &UFractureToolCluster::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolCluster"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolCluster);
	void UFractureToolUncluster::StaticRegisterNativesUFractureToolUncluster()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolUncluster_NoRegister()
	{
		return UFractureToolUncluster::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolUncluster_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolUncluster_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolUncluster_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Uncluster" },
		{ "IncludePath", "FractureToolClustering.h" },
		{ "ModuleRelativePath", "Private/FractureToolClustering.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolUncluster_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolUncluster>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolUncluster_Statics::ClassParams = {
		&UFractureToolUncluster::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolUncluster_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolUncluster_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolUncluster()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolUncluster_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolUncluster, 826007850);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolUncluster>()
	{
		return UFractureToolUncluster::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolUncluster(Z_Construct_UClass_UFractureToolUncluster, &UFractureToolUncluster::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolUncluster"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolUncluster);
	void UFractureToolMoveUp::StaticRegisterNativesUFractureToolMoveUp()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolMoveUp_NoRegister()
	{
		return UFractureToolMoveUp::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolMoveUp_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolMoveUp_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolMoveUp_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "MoveUp" },
		{ "IncludePath", "FractureToolClustering.h" },
		{ "ModuleRelativePath", "Private/FractureToolClustering.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolMoveUp_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolMoveUp>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolMoveUp_Statics::ClassParams = {
		&UFractureToolMoveUp::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolMoveUp_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolMoveUp_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolMoveUp()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolMoveUp_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolMoveUp, 605148297);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolMoveUp>()
	{
		return UFractureToolMoveUp::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolMoveUp(Z_Construct_UClass_UFractureToolMoveUp, &UFractureToolMoveUp::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolMoveUp"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolMoveUp);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
