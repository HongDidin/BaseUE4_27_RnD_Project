// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Public/FractureTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureTool() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureModalTool_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureActionTool_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureActionTool();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureModalTool();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureInteractiveTool_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureInteractiveTool();
// End Cross Module References
	void UFractureToolSettings::StaticRegisterNativesUFractureToolSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolSettings_NoRegister()
	{
		return UFractureToolSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OwnerTool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OwnerTool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureTool.h" },
		{ "ModuleRelativePath", "Public/FractureTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolSettings_Statics::NewProp_OwnerTool_MetaData[] = {
		{ "ModuleRelativePath", "Public/FractureTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolSettings_Statics::NewProp_OwnerTool = { "OwnerTool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolSettings, OwnerTool), Z_Construct_UClass_UFractureModalTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolSettings_Statics::NewProp_OwnerTool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSettings_Statics::NewProp_OwnerTool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolSettings_Statics::NewProp_OwnerTool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolSettings_Statics::ClassParams = {
		&UFractureToolSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSettings_Statics::PropPointers),
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolSettings, 1055659511);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolSettings>()
	{
		return UFractureToolSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolSettings(Z_Construct_UClass_UFractureToolSettings, &UFractureToolSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolSettings);
	void UFractureActionTool::StaticRegisterNativesUFractureActionTool()
	{
	}
	UClass* Z_Construct_UClass_UFractureActionTool_NoRegister()
	{
		return UFractureActionTool::StaticClass();
	}
	struct Z_Construct_UClass_UFractureActionTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureActionTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureActionTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Tools derived from this class should require parameter inputs from the user, only the bone selection. */" },
		{ "IncludePath", "FractureTool.h" },
		{ "ModuleRelativePath", "Public/FractureTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Tools derived from this class should require parameter inputs from the user, only the bone selection." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureActionTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureActionTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureActionTool_Statics::ClassParams = {
		&UFractureActionTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureActionTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureActionTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureActionTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureActionTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureActionTool, 3204600322);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureActionTool>()
	{
		return UFractureActionTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureActionTool(Z_Construct_UClass_UFractureActionTool, &UFractureActionTool::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureActionTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureActionTool);
	void UFractureModalTool::StaticRegisterNativesUFractureModalTool()
	{
	}
	UClass* Z_Construct_UClass_UFractureModalTool_NoRegister()
	{
		return UFractureModalTool::StaticClass();
	}
	struct Z_Construct_UClass_UFractureModalTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureModalTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureModalTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Tools derived from this class provide parameter details and operate modally. */" },
		{ "IncludePath", "FractureTool.h" },
		{ "ModuleRelativePath", "Public/FractureTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Tools derived from this class provide parameter details and operate modally." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureModalTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureModalTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureModalTool_Statics::ClassParams = {
		&UFractureModalTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureModalTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureModalTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureModalTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureModalTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureModalTool, 2644504674);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureModalTool>()
	{
		return UFractureModalTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureModalTool(Z_Construct_UClass_UFractureModalTool, &UFractureModalTool::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureModalTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureModalTool);
	void UFractureInteractiveTool::StaticRegisterNativesUFractureInteractiveTool()
	{
	}
	UClass* Z_Construct_UClass_UFractureInteractiveTool_NoRegister()
	{
		return UFractureInteractiveTool::StaticClass();
	}
	struct Z_Construct_UClass_UFractureInteractiveTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureInteractiveTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureModalTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureInteractiveTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Tools derived from this class provide parameter details, operate modally and use a viewport manipulator to set certain parameters. */" },
		{ "IncludePath", "FractureTool.h" },
		{ "ModuleRelativePath", "Public/FractureTool.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Tools derived from this class provide parameter details, operate modally and use a viewport manipulator to set certain parameters." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureInteractiveTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureInteractiveTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureInteractiveTool_Statics::ClassParams = {
		&UFractureInteractiveTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureInteractiveTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureInteractiveTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureInteractiveTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureInteractiveTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureInteractiveTool, 3637675825);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureInteractiveTool>()
	{
		return UFractureInteractiveTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureInteractiveTool(Z_Construct_UClass_UFractureInteractiveTool, &UFractureInteractiveTool::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureInteractiveTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureInteractiveTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
