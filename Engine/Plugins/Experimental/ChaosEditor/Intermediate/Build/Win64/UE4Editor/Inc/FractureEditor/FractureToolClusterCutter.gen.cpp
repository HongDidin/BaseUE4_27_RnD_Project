// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolClusterCutter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolClusterCutter() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureClusterCutterSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureClusterCutterSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolClusterCutter_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolClusterCutter();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolVoronoiCutterBase();
// End Cross Module References
	void UFractureClusterCutterSettings::StaticRegisterNativesUFractureClusterCutterSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureClusterCutterSettings_NoRegister()
	{
		return UFractureClusterCutterSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureClusterCutterSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberClustersMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberClustersMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberClustersMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberClustersMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SitesPerClusterMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SitesPerClusterMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SitesPerClusterMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SitesPerClusterMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterRadiusPercentageMin_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterRadiusPercentageMin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterRadiusPercentageMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterRadiusPercentageMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ClusterRadius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureClusterCutterSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolClusterCutter.h" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMin_MetaData[] = {
		{ "Category", "ClusterVoronoi" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Clusters - Cluster Voronoi Method */" },
		{ "DisplayName", "Minimum Cluster Sites" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ToolTip", "Number of Clusters - Cluster Voronoi Method" },
		{ "UIMax", "200" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMin = { "NumberClustersMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterCutterSettings, NumberClustersMin), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMax_MetaData[] = {
		{ "Category", "ClusterVoronoi" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Clusters - Cluster Voronoi Method */" },
		{ "DisplayName", "Maximum Cluster Sites" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ToolTip", "Number of Clusters - Cluster Voronoi Method" },
		{ "UIMax", "200" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMax = { "NumberClustersMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterCutterSettings, NumberClustersMax), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMin_MetaData[] = {
		{ "Category", "ClusterVoronoi" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Sites per # of Clusters - Cluster Voronoi Method */" },
		{ "DisplayName", "Minimum Sites Per Cluster" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ToolTip", "Sites per # of Clusters - Cluster Voronoi Method" },
		{ "UIMax", "200" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMin = { "SitesPerClusterMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterCutterSettings, SitesPerClusterMin), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMax_MetaData[] = {
		{ "Category", "ClusterVoronoi" },
		{ "ClampMin", "0" },
		{ "DisplayName", "Maximum Sites Per Cluster" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "UIMax", "200" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMax = { "SitesPerClusterMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterCutterSettings, SitesPerClusterMax), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMin_MetaData[] = {
		{ "Category", "ClusterVoronoi" },
		{ "Comment", "/** Cluster's Radius - Cluster Voronoi Method */" },
		{ "DisplayName", "Minimum distance from center as part of bounds max extent" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ToolTip", "Cluster's Radius - Cluster Voronoi Method" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMin = { "ClusterRadiusPercentageMin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterCutterSettings, ClusterRadiusPercentageMin), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMax_MetaData[] = {
		{ "Category", "ClusterVoronoi" },
		{ "Comment", "/** Cluster's Radius - Cluster Voronoi Method */" },
		{ "DisplayName", "Maximum distance from center as part of bounds max extent" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ToolTip", "Cluster's Radius - Cluster Voronoi Method" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMax = { "ClusterRadiusPercentageMax", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterCutterSettings, ClusterRadiusPercentageMax), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadius_MetaData[] = {
		{ "Category", "ClusterVoronoi" },
		{ "Comment", "/** Cluster's Radius - Cluster Voronoi Method */" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ToolTip", "Cluster's Radius - Cluster Voronoi Method" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadius = { "ClusterRadius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureClusterCutterSettings, ClusterRadius), METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureClusterCutterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_NumberClustersMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_SitesPerClusterMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadiusPercentageMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureClusterCutterSettings_Statics::NewProp_ClusterRadius,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureClusterCutterSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureClusterCutterSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureClusterCutterSettings_Statics::ClassParams = {
		&UFractureClusterCutterSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureClusterCutterSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureClusterCutterSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureClusterCutterSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureClusterCutterSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureClusterCutterSettings, 318673058);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureClusterCutterSettings>()
	{
		return UFractureClusterCutterSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureClusterCutterSettings(Z_Construct_UClass_UFractureClusterCutterSettings, &UFractureClusterCutterSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureClusterCutterSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureClusterCutterSettings);
	void UFractureToolClusterCutter::StaticRegisterNativesUFractureToolClusterCutter()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolClusterCutter_NoRegister()
	{
		return UFractureToolClusterCutter::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolClusterCutter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClusterSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ClusterSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolClusterCutter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolVoronoiCutterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolClusterCutter_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Cluster Voronoi" },
		{ "IncludePath", "FractureToolClusterCutter.h" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolClusterCutter_Statics::NewProp_ClusterSettings_MetaData[] = {
		{ "Category", "Cluster" },
		{ "ModuleRelativePath", "Private/FractureToolClusterCutter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolClusterCutter_Statics::NewProp_ClusterSettings = { "ClusterSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolClusterCutter, ClusterSettings), Z_Construct_UClass_UFractureClusterCutterSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolClusterCutter_Statics::NewProp_ClusterSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolClusterCutter_Statics::NewProp_ClusterSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolClusterCutter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolClusterCutter_Statics::NewProp_ClusterSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolClusterCutter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolClusterCutter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolClusterCutter_Statics::ClassParams = {
		&UFractureToolClusterCutter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolClusterCutter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolClusterCutter_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolClusterCutter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolClusterCutter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolClusterCutter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolClusterCutter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolClusterCutter, 3784858407);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolClusterCutter>()
	{
		return UFractureToolClusterCutter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolClusterCutter(Z_Construct_UClass_UFractureToolClusterCutter, &UFractureToolClusterCutter::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolClusterCutter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolClusterCutter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
