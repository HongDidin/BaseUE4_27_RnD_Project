// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolCutter_generated_h
#error "FractureToolCutter.generated.h already included, missing '#pragma once' in FractureToolCutter.h"
#endif
#define FRACTUREEDITOR_FractureToolCutter_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureCutterSettings(); \
	friend struct Z_Construct_UClass_UFractureCutterSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureCutterSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureCutterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUFractureCutterSettings(); \
	friend struct Z_Construct_UClass_UFractureCutterSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureCutterSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureCutterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureCutterSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureCutterSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureCutterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureCutterSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureCutterSettings(UFractureCutterSettings&&); \
	NO_API UFractureCutterSettings(const UFractureCutterSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureCutterSettings(UFractureCutterSettings&&); \
	NO_API UFractureCutterSettings(const UFractureCutterSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureCutterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureCutterSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureCutterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_12_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureCutterSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureCollisionSettings(); \
	friend struct Z_Construct_UClass_UFractureCollisionSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureCollisionSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureCollisionSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_INCLASS \
private: \
	static void StaticRegisterNativesUFractureCollisionSettings(); \
	friend struct Z_Construct_UClass_UFractureCollisionSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureCollisionSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureCollisionSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureCollisionSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureCollisionSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureCollisionSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureCollisionSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureCollisionSettings(UFractureCollisionSettings&&); \
	NO_API UFractureCollisionSettings(const UFractureCollisionSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureCollisionSettings(UFractureCollisionSettings&&); \
	NO_API UFractureCollisionSettings(const UFractureCollisionSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureCollisionSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureCollisionSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureCollisionSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_74_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_78_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureCollisionSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolCutterBase(); \
	friend struct Z_Construct_UClass_UFractureToolCutterBase_Statics; \
public: \
	DECLARE_CLASS(UFractureToolCutterBase, UFractureInteractiveTool, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolCutterBase)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolCutterBase(); \
	friend struct Z_Construct_UClass_UFractureToolCutterBase_Statics; \
public: \
	DECLARE_CLASS(UFractureToolCutterBase, UFractureInteractiveTool, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolCutterBase)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolCutterBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolCutterBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolCutterBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolCutterBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolCutterBase(UFractureToolCutterBase&&); \
	NO_API UFractureToolCutterBase(const UFractureToolCutterBase&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolCutterBase(UFractureToolCutterBase&&); \
	NO_API UFractureToolCutterBase(const UFractureToolCutterBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolCutterBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolCutterBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolCutterBase)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CutterSettings() { return STRUCT_OFFSET(UFractureToolCutterBase, CutterSettings); } \
	FORCEINLINE static uint32 __PPO__CollisionSettings() { return STRUCT_OFFSET(UFractureToolCutterBase, CollisionSettings); }


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_90_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_94_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolCutterBase>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolVoronoiCutterBase(); \
	friend struct Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics; \
public: \
	DECLARE_CLASS(UFractureToolVoronoiCutterBase, UFractureToolCutterBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolVoronoiCutterBase)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolVoronoiCutterBase(); \
	friend struct Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics; \
public: \
	DECLARE_CLASS(UFractureToolVoronoiCutterBase, UFractureToolCutterBase, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolVoronoiCutterBase)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolVoronoiCutterBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolVoronoiCutterBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolVoronoiCutterBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolVoronoiCutterBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolVoronoiCutterBase(UFractureToolVoronoiCutterBase&&); \
	NO_API UFractureToolVoronoiCutterBase(const UFractureToolVoronoiCutterBase&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolVoronoiCutterBase(UFractureToolVoronoiCutterBase&&); \
	NO_API UFractureToolVoronoiCutterBase(const UFractureToolVoronoiCutterBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolVoronoiCutterBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolVoronoiCutterBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolVoronoiCutterBase)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_115_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h_119_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolVoronoiCutterBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolCutter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
