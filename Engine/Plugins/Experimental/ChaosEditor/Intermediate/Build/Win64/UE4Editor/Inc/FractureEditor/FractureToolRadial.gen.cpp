// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolRadial.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolRadial() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureRadialSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureRadialSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolRadial_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolRadial();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolVoronoiCutterBase();
// End Cross Module References
	void UFractureRadialSettings::StaticRegisterNativesUFractureRadialSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureRadialSettings_NoRegister()
	{
		return UFractureRadialSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureRadialSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Center_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Center;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Normal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Normal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_AngularSteps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSteps_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RadialSteps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngleOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngleOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Variability_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Variability;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureRadialSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolRadial.h" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Center_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Center of generated pattern */" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Center of generated pattern" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Center = { "Center", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureRadialSettings, Center), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Center_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Center_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Normal_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "Comment", "/** Normal to plane in which sites are generated */" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Normal to plane in which sites are generated" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Normal = { "Normal", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureRadialSettings, Normal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Normal_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Normal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Pattern radius */" },
		{ "DisplayName", "Radius" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Pattern radius" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureRadialSettings, Radius), METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngularSteps_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of angular steps */" },
		{ "DisplayName", "Angular Steps" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Number of angular steps" },
		{ "UIMax", "50" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngularSteps = { "AngularSteps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureRadialSettings, AngularSteps), METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngularSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngularSteps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_RadialSteps_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of radial steps */" },
		{ "DisplayName", "Radial Steps" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Number of radial steps" },
		{ "UIMax", "50" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_RadialSteps = { "RadialSteps", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureRadialSettings, RadialSteps), METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_RadialSteps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_RadialSteps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngleOffset_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Angle offset at each radial step */" },
		{ "DisplayName", "Angle Offset" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Angle offset at each radial step" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngleOffset = { "AngleOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureRadialSettings, AngleOffset), METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngleOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngleOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Variability_MetaData[] = {
		{ "Category", "RadialVoronoi" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Randomness of sites distribution */" },
		{ "DisplayName", "Variability" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Randomness of sites distribution" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Variability = { "Variability", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureRadialSettings, Variability), METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Variability_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Variability_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureRadialSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Center,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Normal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngularSteps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_RadialSteps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_AngleOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureRadialSettings_Statics::NewProp_Variability,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureRadialSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureRadialSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureRadialSettings_Statics::ClassParams = {
		&UFractureRadialSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureRadialSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureRadialSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureRadialSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureRadialSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureRadialSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureRadialSettings, 505141430);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureRadialSettings>()
	{
		return UFractureRadialSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureRadialSettings(Z_Construct_UClass_UFractureRadialSettings, &UFractureRadialSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureRadialSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureRadialSettings);
	void UFractureToolRadial::StaticRegisterNativesUFractureToolRadial()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolRadial_NoRegister()
	{
		return UFractureToolRadial::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolRadial_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RadialSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RadialSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolRadial_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolVoronoiCutterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolRadial_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Radial Voronoi" },
		{ "IncludePath", "FractureToolRadial.h" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolRadial_Statics::NewProp_RadialSettings_MetaData[] = {
		{ "Category", "Uniform" },
		{ "Comment", "// Radial Voronoi Fracture Input Settings\n" },
		{ "ModuleRelativePath", "Private/FractureToolRadial.h" },
		{ "ToolTip", "Radial Voronoi Fracture Input Settings" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolRadial_Statics::NewProp_RadialSettings = { "RadialSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolRadial, RadialSettings), Z_Construct_UClass_UFractureRadialSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolRadial_Statics::NewProp_RadialSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolRadial_Statics::NewProp_RadialSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolRadial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolRadial_Statics::NewProp_RadialSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolRadial_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolRadial>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolRadial_Statics::ClassParams = {
		&UFractureToolRadial::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolRadial_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolRadial_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolRadial_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolRadial_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolRadial()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolRadial_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolRadial, 928747688);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolRadial>()
	{
		return UFractureToolRadial::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolRadial(Z_Construct_UClass_UFractureToolRadial, &UFractureToolRadial::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolRadial"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolRadial);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
