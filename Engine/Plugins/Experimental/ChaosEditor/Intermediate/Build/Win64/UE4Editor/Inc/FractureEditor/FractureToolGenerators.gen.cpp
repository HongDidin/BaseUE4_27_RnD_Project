// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolGenerators.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolGenerators() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolGenerateAsset_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolGenerateAsset();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureActionTool();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolResetAsset_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolResetAsset();
// End Cross Module References
	void UFractureToolGenerateAsset::StaticRegisterNativesUFractureToolGenerateAsset()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolGenerateAsset_NoRegister()
	{
		return UFractureToolGenerateAsset::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolGenerateAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolGenerateAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureActionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolGenerateAsset_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolGenerators.h" },
		{ "ModuleRelativePath", "Private/FractureToolGenerators.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolGenerateAsset_Statics::NewProp_AssetPath_MetaData[] = {
		{ "ModuleRelativePath", "Private/FractureToolGenerators.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UFractureToolGenerateAsset_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolGenerateAsset, AssetPath), METADATA_PARAMS(Z_Construct_UClass_UFractureToolGenerateAsset_Statics::NewProp_AssetPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolGenerateAsset_Statics::NewProp_AssetPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolGenerateAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolGenerateAsset_Statics::NewProp_AssetPath,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolGenerateAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolGenerateAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolGenerateAsset_Statics::ClassParams = {
		&UFractureToolGenerateAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolGenerateAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolGenerateAsset_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolGenerateAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolGenerateAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolGenerateAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolGenerateAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolGenerateAsset, 2626351291);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolGenerateAsset>()
	{
		return UFractureToolGenerateAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolGenerateAsset(Z_Construct_UClass_UFractureToolGenerateAsset, &UFractureToolGenerateAsset::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolGenerateAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolGenerateAsset);
	void UFractureToolResetAsset::StaticRegisterNativesUFractureToolResetAsset()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolResetAsset_NoRegister()
	{
		return UFractureToolResetAsset::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolResetAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolResetAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolGenerateAsset,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolResetAsset_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolGenerators.h" },
		{ "ModuleRelativePath", "Private/FractureToolGenerators.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolResetAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolResetAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolResetAsset_Statics::ClassParams = {
		&UFractureToolResetAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolResetAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolResetAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolResetAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolResetAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolResetAsset, 3638469539);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolResetAsset>()
	{
		return UFractureToolResetAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolResetAsset(Z_Construct_UClass_UFractureToolResetAsset, &UFractureToolResetAsset::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolResetAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolResetAsset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
