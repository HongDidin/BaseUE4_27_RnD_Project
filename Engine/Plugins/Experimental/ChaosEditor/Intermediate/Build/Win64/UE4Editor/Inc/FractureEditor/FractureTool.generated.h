// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureTool_generated_h
#error "FractureTool.generated.h already included, missing '#pragma once' in FractureTool.h"
#endif
#define FRACTUREEDITOR_FractureTool_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolSettings(); \
	friend struct Z_Construct_UClass_UFractureToolSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureToolSettings, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolSettings(); \
	friend struct Z_Construct_UClass_UFractureToolSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureToolSettings, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolSettings(UFractureToolSettings&&); \
	NO_API UFractureToolSettings(const UFractureToolSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolSettings(UFractureToolSettings&&); \
	NO_API UFractureToolSettings(const UFractureToolSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolSettings); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_24_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_27_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureActionTool(); \
	friend struct Z_Construct_UClass_UFractureActionTool_Statics; \
public: \
	DECLARE_CLASS(UFractureActionTool, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureActionTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_INCLASS \
private: \
	static void StaticRegisterNativesUFractureActionTool(); \
	friend struct Z_Construct_UClass_UFractureActionTool_Statics; \
public: \
	DECLARE_CLASS(UFractureActionTool, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureActionTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureActionTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureActionTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureActionTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureActionTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureActionTool(UFractureActionTool&&); \
	NO_API UFractureActionTool(const UFractureActionTool&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureActionTool(UFractureActionTool&&); \
	NO_API UFractureActionTool(const UFractureActionTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureActionTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureActionTool); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureActionTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_40_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_44_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureActionTool>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureModalTool(); \
	friend struct Z_Construct_UClass_UFractureModalTool_Statics; \
public: \
	DECLARE_CLASS(UFractureModalTool, UFractureActionTool, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureModalTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_INCLASS \
private: \
	static void StaticRegisterNativesUFractureModalTool(); \
	friend struct Z_Construct_UClass_UFractureModalTool_Statics; \
public: \
	DECLARE_CLASS(UFractureModalTool, UFractureActionTool, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureModalTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureModalTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureModalTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureModalTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureModalTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureModalTool(UFractureModalTool&&); \
	NO_API UFractureModalTool(const UFractureModalTool&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureModalTool(UFractureModalTool&&); \
	NO_API UFractureModalTool(const UFractureModalTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureModalTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureModalTool); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureModalTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_84_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_88_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureModalTool>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureInteractiveTool(); \
	friend struct Z_Construct_UClass_UFractureInteractiveTool_Statics; \
public: \
	DECLARE_CLASS(UFractureInteractiveTool, UFractureModalTool, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureInteractiveTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_INCLASS \
private: \
	static void StaticRegisterNativesUFractureInteractiveTool(); \
	friend struct Z_Construct_UClass_UFractureInteractiveTool_Statics; \
public: \
	DECLARE_CLASS(UFractureInteractiveTool, UFractureModalTool, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureInteractiveTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureInteractiveTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureInteractiveTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureInteractiveTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureInteractiveTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureInteractiveTool(UFractureInteractiveTool&&); \
	NO_API UFractureInteractiveTool(const UFractureInteractiveTool&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureInteractiveTool(UFractureInteractiveTool&&); \
	NO_API UFractureInteractiveTool(const UFractureInteractiveTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureInteractiveTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureInteractiveTool); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureInteractiveTool)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_115_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h_119_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureInteractiveTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Public_FractureTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
