// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolBrick_generated_h
#error "FractureToolBrick.generated.h already included, missing '#pragma once' in FractureToolBrick.h"
#endif
#define FRACTUREEDITOR_FractureToolBrick_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureBrickSettings(); \
	friend struct Z_Construct_UClass_UFractureBrickSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureBrickSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureBrickSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUFractureBrickSettings(); \
	friend struct Z_Construct_UClass_UFractureBrickSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureBrickSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureBrickSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureBrickSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureBrickSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureBrickSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureBrickSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureBrickSettings(UFractureBrickSettings&&); \
	NO_API UFractureBrickSettings(const UFractureBrickSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureBrickSettings(UFractureBrickSettings&&); \
	NO_API UFractureBrickSettings(const UFractureBrickSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureBrickSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureBrickSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureBrickSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_39_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_43_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureBrickSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolBrick(); \
	friend struct Z_Construct_UClass_UFractureToolBrick_Statics; \
public: \
	DECLARE_CLASS(UFractureToolBrick, UFractureToolCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolBrick)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolBrick(); \
	friend struct Z_Construct_UClass_UFractureToolBrick_Statics; \
public: \
	DECLARE_CLASS(UFractureToolBrick, UFractureToolCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolBrick)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolBrick(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolBrick) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolBrick); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolBrick); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolBrick(UFractureToolBrick&&); \
	NO_API UFractureToolBrick(const UFractureToolBrick&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolBrick(UFractureToolBrick&&); \
	NO_API UFractureToolBrick(const UFractureToolBrick&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolBrick); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolBrick); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolBrick)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BrickSettings() { return STRUCT_OFFSET(UFractureToolBrick, BrickSettings); }


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_81_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h_85_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolBrick>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolBrick_h


#define FOREACH_ENUM_EFRACTUREBRICKBOND(op) \
	op(EFractureBrickBond::Stretcher) \
	op(EFractureBrickBond::Stack) \
	op(EFractureBrickBond::English) \
	op(EFractureBrickBond::Header) \
	op(EFractureBrickBond::Flemish) 

enum class EFractureBrickBond : uint8;
template<> FRACTUREEDITOR_API UEnum* StaticEnum<EFractureBrickBond>();

#define FOREACH_ENUM_EFRACTUREBRICKPROJECTION(op) \
	op(EFractureBrickProjection::X) \
	op(EFractureBrickProjection::Y) \
	op(EFractureBrickProjection::Z) 

enum class EFractureBrickProjection : uint8;
template<> FRACTUREEDITOR_API UEnum* StaticEnum<EFractureBrickProjection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
