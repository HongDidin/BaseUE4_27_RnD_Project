// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_AutoClusterFracture_generated_h
#error "AutoClusterFracture.generated.h already included, missing '#pragma once' in AutoClusterFracture.h"
#endif
#define FRACTUREEDITOR_AutoClusterFracture_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAutoClusterFractureCommand(); \
	friend struct Z_Construct_UClass_UAutoClusterFractureCommand_Statics; \
public: \
	DECLARE_CLASS(UAutoClusterFractureCommand, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UAutoClusterFractureCommand)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUAutoClusterFractureCommand(); \
	friend struct Z_Construct_UClass_UAutoClusterFractureCommand_Statics; \
public: \
	DECLARE_CLASS(UAutoClusterFractureCommand, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UAutoClusterFractureCommand)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAutoClusterFractureCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAutoClusterFractureCommand) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAutoClusterFractureCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAutoClusterFractureCommand); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAutoClusterFractureCommand(UAutoClusterFractureCommand&&); \
	NO_API UAutoClusterFractureCommand(const UAutoClusterFractureCommand&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAutoClusterFractureCommand(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAutoClusterFractureCommand(UAutoClusterFractureCommand&&); \
	NO_API UAutoClusterFractureCommand(const UAutoClusterFractureCommand&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAutoClusterFractureCommand); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAutoClusterFractureCommand); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAutoClusterFractureCommand)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_28_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h_32_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UAutoClusterFractureCommand>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_AutoClusterFracture_h


#define FOREACH_ENUM_EFRACTUREAUTOCLUSTERMODE(op) \
	op(EFractureAutoClusterMode::BoundingBox) \
	op(EFractureAutoClusterMode::Proximity) \
	op(EFractureAutoClusterMode::Distance) \
	op(EFractureAutoClusterMode::Voronoi) 

enum class EFractureAutoClusterMode : uint8;
template<> FRACTUREEDITOR_API UEnum* StaticEnum<EFractureAutoClusterMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
