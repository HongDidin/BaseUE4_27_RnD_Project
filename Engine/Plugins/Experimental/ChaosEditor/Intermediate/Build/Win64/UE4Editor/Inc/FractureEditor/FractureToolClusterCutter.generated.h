// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolClusterCutter_generated_h
#error "FractureToolClusterCutter.generated.h already included, missing '#pragma once' in FractureToolClusterCutter.h"
#endif
#define FRACTUREEDITOR_FractureToolClusterCutter_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureClusterCutterSettings(); \
	friend struct Z_Construct_UClass_UFractureClusterCutterSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureClusterCutterSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureClusterCutterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUFractureClusterCutterSettings(); \
	friend struct Z_Construct_UClass_UFractureClusterCutterSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureClusterCutterSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureClusterCutterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureClusterCutterSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureClusterCutterSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureClusterCutterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureClusterCutterSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureClusterCutterSettings(UFractureClusterCutterSettings&&); \
	NO_API UFractureClusterCutterSettings(const UFractureClusterCutterSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureClusterCutterSettings(UFractureClusterCutterSettings&&); \
	NO_API UFractureClusterCutterSettings(const UFractureClusterCutterSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureClusterCutterSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureClusterCutterSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureClusterCutterSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_12_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureClusterCutterSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolClusterCutter(); \
	friend struct Z_Construct_UClass_UFractureToolClusterCutter_Statics; \
public: \
	DECLARE_CLASS(UFractureToolClusterCutter, UFractureToolVoronoiCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolClusterCutter)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolClusterCutter(); \
	friend struct Z_Construct_UClass_UFractureToolClusterCutter_Statics; \
public: \
	DECLARE_CLASS(UFractureToolClusterCutter, UFractureToolVoronoiCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolClusterCutter)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolClusterCutter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolClusterCutter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolClusterCutter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolClusterCutter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolClusterCutter(UFractureToolClusterCutter&&); \
	NO_API UFractureToolClusterCutter(const UFractureToolClusterCutter&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolClusterCutter(UFractureToolClusterCutter&&); \
	NO_API UFractureToolClusterCutter(const UFractureToolClusterCutter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolClusterCutter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolClusterCutter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolClusterCutter)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_58_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h_62_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolClusterCutter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterCutter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
