// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolAutoCluster.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolAutoCluster() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureAutoClusterSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureAutoClusterSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UEnum* Z_Construct_UEnum_FractureEditor_EFractureAutoClusterMode();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolAutoCluster_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolAutoCluster();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureModalTool();
// End Cross Module References
	void UFractureAutoClusterSettings::StaticRegisterNativesUFractureAutoClusterSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureAutoClusterSettings_NoRegister()
	{
		return UFractureAutoClusterSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureAutoClusterSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_AutoClusterMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutoClusterMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_AutoClusterMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SiteCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_SiteCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnforceConnectivity_MetaData[];
#endif
		static void NewProp_bEnforceConnectivity_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnforceConnectivity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureAutoClusterSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureAutoClusterSettings_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Auto Cluster" },
		{ "IncludePath", "FractureToolAutoCluster.h" },
		{ "ModuleRelativePath", "Private/FractureToolAutoCluster.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_AutoClusterMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_AutoClusterMode_MetaData[] = {
		{ "Category", "AutoCluster" },
		{ "DisplayName", "Mode" },
		{ "ModuleRelativePath", "Private/FractureToolAutoCluster.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_AutoClusterMode = { "AutoClusterMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureAutoClusterSettings, AutoClusterMode), Z_Construct_UEnum_FractureEditor_EFractureAutoClusterMode, METADATA_PARAMS(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_AutoClusterMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_AutoClusterMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_SiteCount_MetaData[] = {
		{ "Category", "AutoCluster" },
		{ "ClampMin", "1" },
		{ "DisplayName", "Cluster Sites" },
		{ "ModuleRelativePath", "Private/FractureToolAutoCluster.h" },
		{ "UIMax", "5000" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_SiteCount = { "SiteCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureAutoClusterSettings, SiteCount), METADATA_PARAMS(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_SiteCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_SiteCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_bEnforceConnectivity_MetaData[] = {
		{ "Category", "AutoCluster" },
		{ "DisplayName", "Enforce Cluster Connectivity" },
		{ "ModuleRelativePath", "Private/FractureToolAutoCluster.h" },
	};
#endif
	void Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_bEnforceConnectivity_SetBit(void* Obj)
	{
		((UFractureAutoClusterSettings*)Obj)->bEnforceConnectivity = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_bEnforceConnectivity = { "bEnforceConnectivity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFractureAutoClusterSettings), &Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_bEnforceConnectivity_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_bEnforceConnectivity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_bEnforceConnectivity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureAutoClusterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_AutoClusterMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_AutoClusterMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_SiteCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureAutoClusterSettings_Statics::NewProp_bEnforceConnectivity,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureAutoClusterSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureAutoClusterSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureAutoClusterSettings_Statics::ClassParams = {
		&UFractureAutoClusterSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureAutoClusterSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureAutoClusterSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureAutoClusterSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureAutoClusterSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureAutoClusterSettings, 1143179527);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureAutoClusterSettings>()
	{
		return UFractureAutoClusterSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureAutoClusterSettings(Z_Construct_UClass_UFractureAutoClusterSettings, &UFractureAutoClusterSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureAutoClusterSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureAutoClusterSettings);
	void UFractureToolAutoCluster::StaticRegisterNativesUFractureToolAutoCluster()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolAutoCluster_NoRegister()
	{
		return UFractureToolAutoCluster::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolAutoCluster_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AutoClusterSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AutoClusterSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolAutoCluster_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureModalTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolAutoCluster_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "AutoCluster" },
		{ "IncludePath", "FractureToolAutoCluster.h" },
		{ "ModuleRelativePath", "Private/FractureToolAutoCluster.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolAutoCluster_Statics::NewProp_AutoClusterSettings_MetaData[] = {
		{ "Category", "AutoCluster" },
		{ "ModuleRelativePath", "Private/FractureToolAutoCluster.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolAutoCluster_Statics::NewProp_AutoClusterSettings = { "AutoClusterSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolAutoCluster, AutoClusterSettings), Z_Construct_UClass_UFractureAutoClusterSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolAutoCluster_Statics::NewProp_AutoClusterSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolAutoCluster_Statics::NewProp_AutoClusterSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolAutoCluster_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolAutoCluster_Statics::NewProp_AutoClusterSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolAutoCluster_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolAutoCluster>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolAutoCluster_Statics::ClassParams = {
		&UFractureToolAutoCluster::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolAutoCluster_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolAutoCluster_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolAutoCluster_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolAutoCluster_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolAutoCluster()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolAutoCluster_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolAutoCluster, 2654962828);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolAutoCluster>()
	{
		return UFractureToolAutoCluster::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolAutoCluster(Z_Construct_UClass_UFractureToolAutoCluster, &UFractureToolAutoCluster::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolAutoCluster"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolAutoCluster);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
