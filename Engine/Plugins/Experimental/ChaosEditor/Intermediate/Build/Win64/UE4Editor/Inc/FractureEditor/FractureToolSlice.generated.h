// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolSlice_generated_h
#error "FractureToolSlice.generated.h already included, missing '#pragma once' in FractureToolSlice.h"
#endif
#define FRACTUREEDITOR_FractureToolSlice_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureSliceSettings(); \
	friend struct Z_Construct_UClass_UFractureSliceSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureSliceSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureSliceSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUFractureSliceSettings(); \
	friend struct Z_Construct_UClass_UFractureSliceSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureSliceSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureSliceSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureSliceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureSliceSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureSliceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureSliceSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureSliceSettings(UFractureSliceSettings&&); \
	NO_API UFractureSliceSettings(const UFractureSliceSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureSliceSettings(UFractureSliceSettings&&); \
	NO_API UFractureSliceSettings(const UFractureSliceSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureSliceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureSliceSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureSliceSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_13_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_17_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureSliceSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolSlice(); \
	friend struct Z_Construct_UClass_UFractureToolSlice_Statics; \
public: \
	DECLARE_CLASS(UFractureToolSlice, UFractureToolCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolSlice)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolSlice(); \
	friend struct Z_Construct_UClass_UFractureToolSlice_Statics; \
public: \
	DECLARE_CLASS(UFractureToolSlice, UFractureToolCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolSlice)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolSlice(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolSlice) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolSlice); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolSlice); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolSlice(UFractureToolSlice&&); \
	NO_API UFractureToolSlice(const UFractureToolSlice&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolSlice(UFractureToolSlice&&); \
	NO_API UFractureToolSlice(const UFractureToolSlice&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolSlice); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolSlice); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolSlice)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_49_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h_53_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolSlice>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolSlice_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
