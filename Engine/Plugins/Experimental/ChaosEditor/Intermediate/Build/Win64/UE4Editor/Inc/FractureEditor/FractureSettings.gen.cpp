// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Public/FractureSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureSettings() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
// End Cross Module References
	void UFractureSettings::StaticRegisterNativesUFractureSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureSettings_NoRegister()
	{
		return UFractureSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExplodeAmount_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ExplodeAmount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FractureLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FractureLevel;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings specifically related to viewing fractured meshes **/" },
		{ "IncludePath", "FractureSettings.h" },
		{ "ModuleRelativePath", "Public/FractureSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Settings specifically related to viewing fractured meshes *" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSettings_Statics::NewProp_ExplodeAmount_MetaData[] = {
		{ "Category", "ViewSettings" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** How much to seperate the drawing of the bones  */" },
		{ "DisplayName", "Explode Amount" },
		{ "ModuleRelativePath", "Public/FractureSettings.h" },
		{ "ToolTip", "How much to seperate the drawing of the bones" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureSettings_Statics::NewProp_ExplodeAmount = { "ExplodeAmount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureSettings, ExplodeAmount), METADATA_PARAMS(Z_Construct_UClass_UFractureSettings_Statics::NewProp_ExplodeAmount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSettings_Statics::NewProp_ExplodeAmount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSettings_Statics::NewProp_FractureLevel_MetaData[] = {
		{ "Category", "ViewSettings" },
		{ "Comment", "/** Current level of the geometry collection displayed  */" },
		{ "DisplayName", "Fracture Level" },
		{ "ModuleRelativePath", "Public/FractureSettings.h" },
		{ "ToolTip", "Current level of the geometry collection displayed" },
		{ "UIMin", "-1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureSettings_Statics::NewProp_FractureLevel = { "FractureLevel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureSettings, FractureLevel), METADATA_PARAMS(Z_Construct_UClass_UFractureSettings_Statics::NewProp_FractureLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSettings_Statics::NewProp_FractureLevel_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureSettings_Statics::NewProp_ExplodeAmount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureSettings_Statics::NewProp_FractureLevel,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureSettings_Statics::ClassParams = {
		&UFractureSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureSettings, 4122521735);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureSettings>()
	{
		return UFractureSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureSettings(Z_Construct_UClass_UFractureSettings, &UFractureSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
