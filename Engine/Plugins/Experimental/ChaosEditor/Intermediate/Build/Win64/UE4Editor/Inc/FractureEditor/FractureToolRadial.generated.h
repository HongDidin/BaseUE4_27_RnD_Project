// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolRadial_generated_h
#error "FractureToolRadial.generated.h already included, missing '#pragma once' in FractureToolRadial.h"
#endif
#define FRACTUREEDITOR_FractureToolRadial_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureRadialSettings(); \
	friend struct Z_Construct_UClass_UFractureRadialSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureRadialSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureRadialSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUFractureRadialSettings(); \
	friend struct Z_Construct_UClass_UFractureRadialSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureRadialSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureRadialSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureRadialSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureRadialSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureRadialSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureRadialSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureRadialSettings(UFractureRadialSettings&&); \
	NO_API UFractureRadialSettings(const UFractureRadialSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureRadialSettings(UFractureRadialSettings&&); \
	NO_API UFractureRadialSettings(const UFractureRadialSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureRadialSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureRadialSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureRadialSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_12_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_16_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureRadialSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolRadial(); \
	friend struct Z_Construct_UClass_UFractureToolRadial_Statics; \
public: \
	DECLARE_CLASS(UFractureToolRadial, UFractureToolVoronoiCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolRadial)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolRadial(); \
	friend struct Z_Construct_UClass_UFractureToolRadial_Statics; \
public: \
	DECLARE_CLASS(UFractureToolRadial, UFractureToolVoronoiCutterBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolRadial)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolRadial(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolRadial) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolRadial); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolRadial); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolRadial(UFractureToolRadial&&); \
	NO_API UFractureToolRadial(const UFractureToolRadial&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolRadial(UFractureToolRadial&&); \
	NO_API UFractureToolRadial(const UFractureToolRadial&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolRadial); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolRadial); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolRadial)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_59_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h_63_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolRadial>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolRadial_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
