// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolPlaneCut.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolPlaneCut() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFracturePlaneCutSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFracturePlaneCutSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolPlaneCut_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolPlaneCut();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolCutterBase();
// End Cross Module References
	void UFracturePlaneCutSettings::StaticRegisterNativesUFracturePlaneCutSettings()
	{
	}
	UClass* Z_Construct_UClass_UFracturePlaneCutSettings_NoRegister()
	{
		return UFracturePlaneCutSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFracturePlaneCutSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumberPlanarCuts_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumberPlanarCuts;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReferenceActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_ReferenceActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFracturePlaneCutSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFracturePlaneCutSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolPlaneCut.h" },
		{ "ModuleRelativePath", "Private/FractureToolPlaneCut.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_NumberPlanarCuts_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Number of Clusters - Cluster Voronoi Method */" },
		{ "DisplayName", "Number of Cuts" },
		{ "ModuleRelativePath", "Private/FractureToolPlaneCut.h" },
		{ "ToolTip", "Number of Clusters - Cluster Voronoi Method" },
		{ "UIMax", "20" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_NumberPlanarCuts = { "NumberPlanarCuts", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFracturePlaneCutSettings, NumberPlanarCuts), METADATA_PARAMS(Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_NumberPlanarCuts_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_NumberPlanarCuts_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_ReferenceActor_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "/** Actor to be used for voronoi bounds or plane cutting  */" },
		{ "DisplayName", "Reference Actor" },
		{ "ModuleRelativePath", "Private/FractureToolPlaneCut.h" },
		{ "ToolTip", "Actor to be used for voronoi bounds or plane cutting" },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_ReferenceActor = { "ReferenceActor", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFracturePlaneCutSettings, ReferenceActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_ReferenceActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_ReferenceActor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFracturePlaneCutSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_NumberPlanarCuts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFracturePlaneCutSettings_Statics::NewProp_ReferenceActor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFracturePlaneCutSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFracturePlaneCutSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFracturePlaneCutSettings_Statics::ClassParams = {
		&UFracturePlaneCutSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFracturePlaneCutSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFracturePlaneCutSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFracturePlaneCutSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFracturePlaneCutSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFracturePlaneCutSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFracturePlaneCutSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFracturePlaneCutSettings, 464221160);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFracturePlaneCutSettings>()
	{
		return UFracturePlaneCutSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFracturePlaneCutSettings(Z_Construct_UClass_UFracturePlaneCutSettings, &UFracturePlaneCutSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFracturePlaneCutSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFracturePlaneCutSettings);
	void UFractureToolPlaneCut::StaticRegisterNativesUFractureToolPlaneCut()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolPlaneCut_NoRegister()
	{
		return UFractureToolPlaneCut::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolPlaneCut_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneCutSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneCutSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolPlaneCut_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolCutterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolPlaneCut_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Plane Cut Tool" },
		{ "IncludePath", "FractureToolPlaneCut.h" },
		{ "ModuleRelativePath", "Private/FractureToolPlaneCut.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolPlaneCut_Statics::NewProp_PlaneCutSettings_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "// Slicing\n" },
		{ "ModuleRelativePath", "Private/FractureToolPlaneCut.h" },
		{ "ToolTip", "Slicing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolPlaneCut_Statics::NewProp_PlaneCutSettings = { "PlaneCutSettings", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolPlaneCut, PlaneCutSettings), Z_Construct_UClass_UFracturePlaneCutSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolPlaneCut_Statics::NewProp_PlaneCutSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolPlaneCut_Statics::NewProp_PlaneCutSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolPlaneCut_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolPlaneCut_Statics::NewProp_PlaneCutSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolPlaneCut_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolPlaneCut>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolPlaneCut_Statics::ClassParams = {
		&UFractureToolPlaneCut::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolPlaneCut_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolPlaneCut_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolPlaneCut_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolPlaneCut_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolPlaneCut()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolPlaneCut_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolPlaneCut, 1290318174);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolPlaneCut>()
	{
		return UFractureToolPlaneCut::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolPlaneCut(Z_Construct_UClass_UFractureToolPlaneCut, &UFractureToolPlaneCut::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolPlaneCut"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolPlaneCut);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
