// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolSlice.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolSlice() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureSliceSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureSliceSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSlice_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSlice();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolCutterBase();
// End Cross Module References
	void UFractureSliceSettings::StaticRegisterNativesUFractureSliceSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureSliceSettings_NoRegister()
	{
		return UFractureSliceSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureSliceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlicesX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SlicesX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlicesY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SlicesY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SlicesZ_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SlicesZ;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliceAngleVariation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SliceAngleVariation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliceOffsetVariation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SliceOffsetVariation;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureSliceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSliceSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FractureToolSlice.h" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesX_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Num Slices X axis - Slicing Method */" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ToolTip", "Num Slices X axis - Slicing Method" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesX = { "SlicesX", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureSliceSettings, SlicesX), METADATA_PARAMS(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesY_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Num Slices Y axis - Slicing Method */" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ToolTip", "Num Slices Y axis - Slicing Method" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesY = { "SlicesY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureSliceSettings, SlicesY), METADATA_PARAMS(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesZ_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Num Slices Z axis - Slicing Method */" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ToolTip", "Num Slices Z axis - Slicing Method" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesZ = { "SlicesZ", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureSliceSettings, SlicesZ), METADATA_PARAMS(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesZ_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesZ_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceAngleVariation_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Slicing Angle Variation - Slicing Method [0..1] */" },
		{ "DisplayName", "Random Angle Variation" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ToolTip", "Slicing Angle Variation - Slicing Method [0..1]" },
		{ "UIMax", "90.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceAngleVariation = { "SliceAngleVariation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureSliceSettings, SliceAngleVariation), METADATA_PARAMS(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceAngleVariation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceAngleVariation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceOffsetVariation_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "/** Slicing Offset Variation - Slicing Method [0..1] */" },
		{ "DisplayName", "Random Offset Variation" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ToolTip", "Slicing Offset Variation - Slicing Method [0..1]" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceOffsetVariation = { "SliceOffsetVariation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureSliceSettings, SliceOffsetVariation), METADATA_PARAMS(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceOffsetVariation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceOffsetVariation_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureSliceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SlicesZ,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceAngleVariation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureSliceSettings_Statics::NewProp_SliceOffsetVariation,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureSliceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureSliceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureSliceSettings_Statics::ClassParams = {
		&UFractureSliceSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureSliceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSliceSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureSliceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureSliceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureSliceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureSliceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureSliceSettings, 2158144162);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureSliceSettings>()
	{
		return UFractureSliceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureSliceSettings(Z_Construct_UClass_UFractureSliceSettings, &UFractureSliceSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureSliceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureSliceSettings);
	void UFractureToolSlice::StaticRegisterNativesUFractureToolSlice()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolSlice_NoRegister()
	{
		return UFractureToolSlice::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolSlice_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliceSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SliceSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolSlice_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolCutterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolSlice_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Slice Tool" },
		{ "IncludePath", "FractureToolSlice.h" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolSlice_Statics::NewProp_SliceSettings_MetaData[] = {
		{ "Category", "Slicing" },
		{ "Comment", "// Slicing\n" },
		{ "ModuleRelativePath", "Private/FractureToolSlice.h" },
		{ "ToolTip", "Slicing" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolSlice_Statics::NewProp_SliceSettings = { "SliceSettings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolSlice, SliceSettings), Z_Construct_UClass_UFractureSliceSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolSlice_Statics::NewProp_SliceSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSlice_Statics::NewProp_SliceSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolSlice_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolSlice_Statics::NewProp_SliceSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolSlice_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolSlice>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolSlice_Statics::ClassParams = {
		&UFractureToolSlice::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolSlice_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSlice_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolSlice_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolSlice_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolSlice()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolSlice_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolSlice, 156200729);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolSlice>()
	{
		return UFractureToolSlice::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolSlice(Z_Construct_UClass_UFractureToolSlice, &UFractureToolSlice::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolSlice"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolSlice);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
