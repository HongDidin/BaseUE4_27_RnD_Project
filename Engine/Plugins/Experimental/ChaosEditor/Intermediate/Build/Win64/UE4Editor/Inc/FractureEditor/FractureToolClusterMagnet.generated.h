// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolClusterMagnet_generated_h
#error "FractureToolClusterMagnet.generated.h already included, missing '#pragma once' in FractureToolClusterMagnet.h"
#endif
#define FRACTUREEDITOR_FractureToolClusterMagnet_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureClusterMagnetSettings(); \
	friend struct Z_Construct_UClass_UFractureClusterMagnetSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureClusterMagnetSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureClusterMagnetSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUFractureClusterMagnetSettings(); \
	friend struct Z_Construct_UClass_UFractureClusterMagnetSettings_Statics; \
public: \
	DECLARE_CLASS(UFractureClusterMagnetSettings, UFractureToolSettings, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureClusterMagnetSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureClusterMagnetSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureClusterMagnetSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureClusterMagnetSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureClusterMagnetSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureClusterMagnetSettings(UFractureClusterMagnetSettings&&); \
	NO_API UFractureClusterMagnetSettings(const UFractureClusterMagnetSettings&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureClusterMagnetSettings(UFractureClusterMagnetSettings&&); \
	NO_API UFractureClusterMagnetSettings(const UFractureClusterMagnetSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureClusterMagnetSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureClusterMagnetSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureClusterMagnetSettings)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_24_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_28_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureClusterMagnetSettings>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolClusterMagnet(); \
	friend struct Z_Construct_UClass_UFractureToolClusterMagnet_Statics; \
public: \
	DECLARE_CLASS(UFractureToolClusterMagnet, UFractureModalTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolClusterMagnet)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolClusterMagnet(); \
	friend struct Z_Construct_UClass_UFractureToolClusterMagnet_Statics; \
public: \
	DECLARE_CLASS(UFractureToolClusterMagnet, UFractureModalTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolClusterMagnet)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolClusterMagnet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolClusterMagnet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolClusterMagnet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolClusterMagnet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolClusterMagnet(UFractureToolClusterMagnet&&); \
	NO_API UFractureToolClusterMagnet(const UFractureToolClusterMagnet&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolClusterMagnet(UFractureToolClusterMagnet&&); \
	NO_API UFractureToolClusterMagnet(const UFractureToolClusterMagnet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolClusterMagnet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolClusterMagnet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolClusterMagnet)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_41_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h_45_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolClusterMagnet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolClusterMagnet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
