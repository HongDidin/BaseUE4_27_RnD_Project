// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FRACTUREEDITOR_FractureToolEmbed_generated_h
#error "FractureToolEmbed.generated.h already included, missing '#pragma once' in FractureToolEmbed.h"
#endif
#define FRACTUREEDITOR_FractureToolEmbed_generated_h

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolAddEmbeddedGeometry(); \
	friend struct Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics; \
public: \
	DECLARE_CLASS(UFractureToolAddEmbeddedGeometry, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolAddEmbeddedGeometry)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolAddEmbeddedGeometry(); \
	friend struct Z_Construct_UClass_UFractureToolAddEmbeddedGeometry_Statics; \
public: \
	DECLARE_CLASS(UFractureToolAddEmbeddedGeometry, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolAddEmbeddedGeometry)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolAddEmbeddedGeometry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolAddEmbeddedGeometry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolAddEmbeddedGeometry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolAddEmbeddedGeometry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolAddEmbeddedGeometry(UFractureToolAddEmbeddedGeometry&&); \
	NO_API UFractureToolAddEmbeddedGeometry(const UFractureToolAddEmbeddedGeometry&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolAddEmbeddedGeometry(UFractureToolAddEmbeddedGeometry&&); \
	NO_API UFractureToolAddEmbeddedGeometry(const UFractureToolAddEmbeddedGeometry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolAddEmbeddedGeometry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolAddEmbeddedGeometry); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolAddEmbeddedGeometry)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_10_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_14_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolAddEmbeddedGeometry>();

#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_SPARSE_DATA
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFractureToolDeleteEmbeddedGeometry(); \
	friend struct Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics; \
public: \
	DECLARE_CLASS(UFractureToolDeleteEmbeddedGeometry, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolDeleteEmbeddedGeometry)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUFractureToolDeleteEmbeddedGeometry(); \
	friend struct Z_Construct_UClass_UFractureToolDeleteEmbeddedGeometry_Statics; \
public: \
	DECLARE_CLASS(UFractureToolDeleteEmbeddedGeometry, UFractureActionTool, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/FractureEditor"), NO_API) \
	DECLARE_SERIALIZER(UFractureToolDeleteEmbeddedGeometry)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFractureToolDeleteEmbeddedGeometry(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolDeleteEmbeddedGeometry) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolDeleteEmbeddedGeometry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolDeleteEmbeddedGeometry); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolDeleteEmbeddedGeometry(UFractureToolDeleteEmbeddedGeometry&&); \
	NO_API UFractureToolDeleteEmbeddedGeometry(const UFractureToolDeleteEmbeddedGeometry&); \
public:


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFractureToolDeleteEmbeddedGeometry(UFractureToolDeleteEmbeddedGeometry&&); \
	NO_API UFractureToolDeleteEmbeddedGeometry(const UFractureToolDeleteEmbeddedGeometry&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFractureToolDeleteEmbeddedGeometry); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFractureToolDeleteEmbeddedGeometry); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFractureToolDeleteEmbeddedGeometry)


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_31_PROLOG
#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_INCLASS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_SPARSE_DATA \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h_35_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FRACTUREEDITOR_API UClass* StaticClass<class UFractureToolDeleteEmbeddedGeometry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ChaosEditor_Source_FractureEditor_Private_FractureToolEmbed_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
