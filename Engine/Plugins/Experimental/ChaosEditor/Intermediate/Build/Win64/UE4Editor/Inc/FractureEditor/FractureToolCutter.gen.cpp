// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "FractureEditor/Private/FractureToolCutter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFractureToolCutter() {}
// Cross Module References
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureCutterSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureCutterSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolSettings();
	UPackage* Z_Construct_UPackage__Script_FractureEditor();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureCollisionSettings_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureCollisionSettings();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolCutterBase_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolCutterBase();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureInteractiveTool();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolVoronoiCutterBase_NoRegister();
	FRACTUREEDITOR_API UClass* Z_Construct_UClass_UFractureToolVoronoiCutterBase();
// End Cross Module References
	void UFractureCutterSettings::StaticRegisterNativesUFractureCutterSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureCutterSettings_NoRegister()
	{
		return UFractureCutterSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureCutterSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RandomSeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RandomSeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChanceToFracture_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChanceToFracture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGroupFracture_MetaData[];
#endif
		static void NewProp_bGroupFracture_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGroupFracture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawSites_MetaData[];
#endif
		static void NewProp_bDrawSites_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawSites;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawDiagram_MetaData[];
#endif
		static void NewProp_bDrawDiagram_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawDiagram;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Grout_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Grout;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Amplitude_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Amplitude;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Frequency_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Frequency;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OctaveNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OctaveNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfaceResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SurfaceResolution;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureCutterSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings specifically related to the one-time destructive fracturing of a mesh **/" },
		{ "IncludePath", "FractureToolCutter.h" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Settings specifically related to the one-time destructive fracturing of a mesh *" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_RandomSeed_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "ClampMin", "-1" },
		{ "Comment", "/** Random number generator seed for repeatability */" },
		{ "DisplayName", "Random Seed" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Random number generator seed for repeatability" },
		{ "UIMax", "1000" },
		{ "UIMin", "-1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_RandomSeed = { "RandomSeed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCutterSettings, RandomSeed), METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_RandomSeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_RandomSeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_ChanceToFracture_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Chance to shatter each mesh.  Useful when shattering multiple selected meshes.  */" },
		{ "DisplayName", "Chance To Fracture Per Mesh" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Chance to shatter each mesh.  Useful when shattering multiple selected meshes." },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_ChanceToFracture = { "ChanceToFracture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCutterSettings, ChanceToFracture), METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_ChanceToFracture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_ChanceToFracture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bGroupFracture_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Generate a fracture pattern across all selected meshes.  */" },
		{ "DisplayName", "Group Fracture" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Generate a fracture pattern across all selected meshes." },
	};
#endif
	void Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bGroupFracture_SetBit(void* Obj)
	{
		((UFractureCutterSettings*)Obj)->bGroupFracture = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bGroupFracture = { "bGroupFracture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFractureCutterSettings), &Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bGroupFracture_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bGroupFracture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bGroupFracture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawSites_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Generate a fracture pattern across all selected meshes.  */" },
		{ "DisplayName", "Draw Sites" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Generate a fracture pattern across all selected meshes." },
	};
#endif
	void Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawSites_SetBit(void* Obj)
	{
		((UFractureCutterSettings*)Obj)->bDrawSites = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawSites = { "bDrawSites", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFractureCutterSettings), &Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawSites_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawSites_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawSites_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawDiagram_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "Comment", "/** Generate a fracture pattern across all selected meshes.  */" },
		{ "DisplayName", "Draw Diagram" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Generate a fracture pattern across all selected meshes." },
	};
#endif
	void Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawDiagram_SetBit(void* Obj)
	{
		((UFractureCutterSettings*)Obj)->bDrawDiagram = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawDiagram = { "bDrawDiagram", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFractureCutterSettings), &Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawDiagram_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawDiagram_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawDiagram_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Grout_MetaData[] = {
		{ "Category", "CommonFracture" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of space to leave between cut pieces */" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Amount of space to leave between cut pieces" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Grout = { "Grout", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCutterSettings, Grout), METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Grout_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Grout_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Amplitude_MetaData[] = {
		{ "Category", "Noise" },
		{ "Comment", "/** Size of the noise displacement in centimeters */" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Size of the noise displacement in centimeters" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Amplitude = { "Amplitude", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCutterSettings, Amplitude), METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Amplitude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Amplitude_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Frequency_MetaData[] = {
		{ "Category", "Noise" },
		{ "Comment", "/** Period of the Perlin noise.  Smaller values will create noise faces that are smoother */" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Period of the Perlin noise.  Smaller values will create noise faces that are smoother" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Frequency = { "Frequency", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCutterSettings, Frequency), METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Frequency_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Frequency_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_OctaveNumber_MetaData[] = {
		{ "Category", "Noise" },
		{ "Comment", "/** Number of fractal layers of Perlin noise to apply.  Smaller values (1 or 2) will create noise that looks like gentle rolling hills, while larger values (> 4) will tend to look more like craggy mountains */" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Number of fractal layers of Perlin noise to apply.  Smaller values (1 or 2) will create noise that looks like gentle rolling hills, while larger values (> 4) will tend to look more like craggy mountains" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_OctaveNumber = { "OctaveNumber", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCutterSettings, OctaveNumber), METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_OctaveNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_OctaveNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_SurfaceResolution_MetaData[] = {
		{ "Category", "Noise" },
		{ "ClampMin", "0.1" },
		{ "Comment", "/** Spacing between vertices on cut surfaces, where noise is added.  Larger spacing between vertices will create more efficient meshes with fewer triangles, but less resolution to see the shape of the added noise  */" },
		{ "DisplayName", "Point Spacing" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Spacing between vertices on cut surfaces, where noise is added.  Larger spacing between vertices will create more efficient meshes with fewer triangles, but less resolution to see the shape of the added noise" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_SurfaceResolution = { "SurfaceResolution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCutterSettings, SurfaceResolution), METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_SurfaceResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_SurfaceResolution_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureCutterSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_RandomSeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_ChanceToFracture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bGroupFracture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawSites,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_bDrawDiagram,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Grout,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Amplitude,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_Frequency,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_OctaveNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCutterSettings_Statics::NewProp_SurfaceResolution,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureCutterSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureCutterSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureCutterSettings_Statics::ClassParams = {
		&UFractureCutterSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureCutterSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureCutterSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCutterSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureCutterSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureCutterSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureCutterSettings, 1035540186);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureCutterSettings>()
	{
		return UFractureCutterSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureCutterSettings(Z_Construct_UClass_UFractureCutterSettings, &UFractureCutterSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureCutterSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureCutterSettings);
	void UFractureCollisionSettings::StaticRegisterNativesUFractureCollisionSettings()
	{
	}
	UClass* Z_Construct_UClass_UFractureCollisionSettings_NoRegister()
	{
		return UFractureCollisionSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFractureCollisionSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PointSpacing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_PointSpacing;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureCollisionSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCollisionSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Settings related to the collision properties of the fractured mesh pieces */" },
		{ "IncludePath", "FractureToolCutter.h" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Settings related to the collision properties of the fractured mesh pieces" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureCollisionSettings_Statics::NewProp_PointSpacing_MetaData[] = {
		{ "Category", "Collision" },
		{ "ClampMin", "0.1" },
		{ "Comment", "/** Target spacing between collision samples on the mesh surface. */" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ToolTip", "Target spacing between collision samples on the mesh surface." },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFractureCollisionSettings_Statics::NewProp_PointSpacing = { "PointSpacing", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureCollisionSettings, PointSpacing), METADATA_PARAMS(Z_Construct_UClass_UFractureCollisionSettings_Statics::NewProp_PointSpacing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCollisionSettings_Statics::NewProp_PointSpacing_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureCollisionSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureCollisionSettings_Statics::NewProp_PointSpacing,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureCollisionSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureCollisionSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureCollisionSettings_Statics::ClassParams = {
		&UFractureCollisionSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureCollisionSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCollisionSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureCollisionSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureCollisionSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureCollisionSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureCollisionSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureCollisionSettings, 1703075523);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureCollisionSettings>()
	{
		return UFractureCollisionSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureCollisionSettings(Z_Construct_UClass_UFractureCollisionSettings, &UFractureCollisionSettings::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureCollisionSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureCollisionSettings);
	void UFractureToolCutterBase::StaticRegisterNativesUFractureToolCutterBase()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolCutterBase_NoRegister()
	{
		return UFractureToolCutterBase::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolCutterBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutterSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CutterSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolCutterBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolCutterBase_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Cutter Base" },
		{ "IncludePath", "FractureToolCutter.h" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CutterSettings_MetaData[] = {
		{ "Category", "Slicing" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CutterSettings = { "CutterSettings", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolCutterBase, CutterSettings), Z_Construct_UClass_UFractureCutterSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CutterSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CutterSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CollisionSettings_MetaData[] = {
		{ "Category", "Collision" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CollisionSettings = { "CollisionSettings", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFractureToolCutterBase, CollisionSettings), Z_Construct_UClass_UFractureCollisionSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CollisionSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CollisionSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFractureToolCutterBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CutterSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFractureToolCutterBase_Statics::NewProp_CollisionSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolCutterBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolCutterBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolCutterBase_Statics::ClassParams = {
		&UFractureToolCutterBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFractureToolCutterBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolCutterBase_Statics::PropPointers),
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolCutterBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolCutterBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolCutterBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolCutterBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolCutterBase, 3634973994);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolCutterBase>()
	{
		return UFractureToolCutterBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolCutterBase(Z_Construct_UClass_UFractureToolCutterBase, &UFractureToolCutterBase::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolCutterBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolCutterBase);
	void UFractureToolVoronoiCutterBase::StaticRegisterNativesUFractureToolVoronoiCutterBase()
	{
	}
	UClass* Z_Construct_UClass_UFractureToolVoronoiCutterBase_NoRegister()
	{
		return UFractureToolVoronoiCutterBase::StaticClass();
	}
	struct Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFractureToolCutterBase,
		(UObject* (*)())Z_Construct_UPackage__Script_FractureEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics::Class_MetaDataParams[] = {
		{ "Category", "FractureTools" },
		{ "DisplayName", "Voronoi Base" },
		{ "IncludePath", "FractureToolCutter.h" },
		{ "ModuleRelativePath", "Private/FractureToolCutter.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFractureToolVoronoiCutterBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics::ClassParams = {
		&UFractureToolVoronoiCutterBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFractureToolVoronoiCutterBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFractureToolVoronoiCutterBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFractureToolVoronoiCutterBase, 141471996);
	template<> FRACTUREEDITOR_API UClass* StaticClass<UFractureToolVoronoiCutterBase>()
	{
		return UFractureToolVoronoiCutterBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFractureToolVoronoiCutterBase(Z_Construct_UClass_UFractureToolVoronoiCutterBase, &UFractureToolVoronoiCutterBase::StaticClass, TEXT("/Script/FractureEditor"), TEXT("UFractureToolVoronoiCutterBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFractureToolVoronoiCutterBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
