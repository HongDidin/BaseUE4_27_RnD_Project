// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Private/K2Node_ExecutePythonScript.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeK2Node_ExecutePythonScript() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UK2Node_ExecutePythonScript_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UK2Node_ExecutePythonScript();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_CallFunction();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
// End Cross Module References
	void UK2Node_ExecutePythonScript::StaticRegisterNativesUK2Node_ExecutePythonScript()
	{
	}
	UClass* Z_Construct_UClass_UK2Node_ExecutePythonScript_NoRegister()
	{
		return UK2Node_ExecutePythonScript::StaticClass();
	}
	struct Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Inputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Inputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Inputs;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Outputs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Outputs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Outputs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UK2Node_CallFunction,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "K2Node_ExecutePythonScript.h" },
		{ "ModuleRelativePath", "Private/K2Node_ExecutePythonScript.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Inputs_Inner = { "Inputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Inputs_MetaData[] = {
		{ "Category", "Arguments" },
		{ "Comment", "/** User-defined input pins */" },
		{ "ModuleRelativePath", "Private/K2Node_ExecutePythonScript.h" },
		{ "ToolTip", "User-defined input pins" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Inputs = { "Inputs", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_ExecutePythonScript, Inputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Inputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Inputs_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Outputs_Inner = { "Outputs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Outputs_MetaData[] = {
		{ "Category", "Arguments" },
		{ "Comment", "/** User-defined output pins */" },
		{ "ModuleRelativePath", "Private/K2Node_ExecutePythonScript.h" },
		{ "ToolTip", "User-defined output pins" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Outputs = { "Outputs", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UK2Node_ExecutePythonScript, Outputs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Outputs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Outputs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Inputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Inputs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Outputs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::NewProp_Outputs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UK2Node_ExecutePythonScript>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::ClassParams = {
		&UK2Node_ExecutePythonScript::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UK2Node_ExecutePythonScript()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UK2Node_ExecutePythonScript, 392299467);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UK2Node_ExecutePythonScript>()
	{
		return UK2Node_ExecutePythonScript::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UK2Node_ExecutePythonScript(Z_Construct_UClass_UK2Node_ExecutePythonScript, &UK2Node_ExecutePythonScript::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UK2Node_ExecutePythonScript"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UK2Node_ExecutePythonScript);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
