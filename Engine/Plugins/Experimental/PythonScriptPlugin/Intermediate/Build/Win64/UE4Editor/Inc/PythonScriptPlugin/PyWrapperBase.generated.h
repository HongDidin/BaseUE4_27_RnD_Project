// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PYTHONSCRIPTPLUGIN_PyWrapperBase_generated_h
#error "PyWrapperBase.generated.h already included, missing '#pragma once' in PyWrapperBase.h"
#endif
#define PYTHONSCRIPTPLUGIN_PyWrapperBase_generated_h

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PYTHONSCRIPTPLUGIN_API UPythonResourceOwner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonResourceOwner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PYTHONSCRIPTPLUGIN_API, UPythonResourceOwner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonResourceOwner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PYTHONSCRIPTPLUGIN_API UPythonResourceOwner(UPythonResourceOwner&&); \
	PYTHONSCRIPTPLUGIN_API UPythonResourceOwner(const UPythonResourceOwner&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	PYTHONSCRIPTPLUGIN_API UPythonResourceOwner(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	PYTHONSCRIPTPLUGIN_API UPythonResourceOwner(UPythonResourceOwner&&); \
	PYTHONSCRIPTPLUGIN_API UPythonResourceOwner(const UPythonResourceOwner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(PYTHONSCRIPTPLUGIN_API, UPythonResourceOwner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonResourceOwner); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonResourceOwner)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUPythonResourceOwner(); \
	friend struct Z_Construct_UClass_UPythonResourceOwner_Statics; \
public: \
	DECLARE_CLASS(UPythonResourceOwner, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), PYTHONSCRIPTPLUGIN_API) \
	DECLARE_SERIALIZER(UPythonResourceOwner)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IPythonResourceOwner() {} \
public: \
	typedef UPythonResourceOwner UClassType; \
	typedef IPythonResourceOwner ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_INCLASS_IINTERFACE \
protected: \
	virtual ~IPythonResourceOwner() {} \
public: \
	typedef UPythonResourceOwner UClassType; \
	typedef IPythonResourceOwner ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_91_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_99_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_99_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h_94_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPythonResourceOwner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
