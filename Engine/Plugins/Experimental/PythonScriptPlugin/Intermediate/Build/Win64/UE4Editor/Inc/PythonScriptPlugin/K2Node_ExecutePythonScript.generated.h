// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PYTHONSCRIPTPLUGIN_K2Node_ExecutePythonScript_generated_h
#error "K2Node_ExecutePythonScript.generated.h already included, missing '#pragma once' in K2Node_ExecutePythonScript.h"
#endif
#define PYTHONSCRIPTPLUGIN_K2Node_ExecutePythonScript_generated_h

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUK2Node_ExecutePythonScript(); \
	friend struct Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics; \
public: \
	DECLARE_CLASS(UK2Node_ExecutePythonScript, UK2Node_CallFunction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_ExecutePythonScript)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUK2Node_ExecutePythonScript(); \
	friend struct Z_Construct_UClass_UK2Node_ExecutePythonScript_Statics; \
public: \
	DECLARE_CLASS(UK2Node_ExecutePythonScript, UK2Node_CallFunction, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UK2Node_ExecutePythonScript)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UK2Node_ExecutePythonScript(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UK2Node_ExecutePythonScript) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_ExecutePythonScript); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_ExecutePythonScript); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_ExecutePythonScript(UK2Node_ExecutePythonScript&&); \
	NO_API UK2Node_ExecutePythonScript(const UK2Node_ExecutePythonScript&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UK2Node_ExecutePythonScript(UK2Node_ExecutePythonScript&&); \
	NO_API UK2Node_ExecutePythonScript(const UK2Node_ExecutePythonScript&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UK2Node_ExecutePythonScript); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UK2Node_ExecutePythonScript); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UK2Node_ExecutePythonScript)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Inputs() { return STRUCT_OFFSET(UK2Node_ExecutePythonScript, Inputs); } \
	FORCEINLINE static uint32 __PPO__Outputs() { return STRUCT_OFFSET(UK2Node_ExecutePythonScript, Outputs); }


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_13_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UK2Node_ExecutePythonScript>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_K2Node_ExecutePythonScript_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
