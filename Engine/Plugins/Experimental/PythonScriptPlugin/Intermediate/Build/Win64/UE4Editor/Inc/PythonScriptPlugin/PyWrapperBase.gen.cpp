// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Private/PyWrapperBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePyWrapperBase() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonResourceOwner_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonResourceOwner();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
// End Cross Module References
	void UPythonResourceOwner::StaticRegisterNativesUPythonResourceOwner()
	{
	}
	UClass* Z_Construct_UClass_UPythonResourceOwner_NoRegister()
	{
		return UPythonResourceOwner::StaticClass();
	}
	struct Z_Construct_UClass_UPythonResourceOwner_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPythonResourceOwner_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonResourceOwner_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/PyWrapperBase.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPythonResourceOwner_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IPythonResourceOwner>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPythonResourceOwner_Statics::ClassParams = {
		&UPythonResourceOwner::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UPythonResourceOwner_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonResourceOwner_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPythonResourceOwner()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPythonResourceOwner_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPythonResourceOwner, 4262291070);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPythonResourceOwner>()
	{
		return UPythonResourceOwner::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPythonResourceOwner(Z_Construct_UClass_UPythonResourceOwner, &UPythonResourceOwner::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPythonResourceOwner"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPythonResourceOwner);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
