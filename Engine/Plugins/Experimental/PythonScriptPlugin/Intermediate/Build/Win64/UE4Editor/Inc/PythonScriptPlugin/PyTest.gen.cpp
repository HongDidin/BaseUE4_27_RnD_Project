// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Private/PyTest.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePyTest() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UFunction* Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
	PYTHONSCRIPTPLUGIN_API UFunction* Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature();
	PYTHONSCRIPTPLUGIN_API UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPyTestEnum();
	PYTHONSCRIPTPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FPyTestChildStruct();
	PYTHONSCRIPTPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FPyTestStruct();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestStructLibrary_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestStructLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestObject_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestObject();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestChildObject_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestChildObject();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestObjectLibrary_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPyTestObjectLibrary();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics
	{
		struct _Script_PythonScriptPlugin_eventPyTestMulticastDelegate_Parms
		{
			FString InStr;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InStr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::NewProp_InStr = { "InStr", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_PythonScriptPlugin_eventPyTestMulticastDelegate_Parms, InStr), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::NewProp_InStr,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * Multicast delegate to allow testing of the various script delegate features that are exposed to Python wrapped types.\n */" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Multicast delegate to allow testing of the various script delegate features that are exposed to Python wrapped types." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_PythonScriptPlugin, nullptr, "PyTestMulticastDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_PythonScriptPlugin_eventPyTestMulticastDelegate_Parms), Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics
	{
		struct _Script_PythonScriptPlugin_eventPyTestDelegate_Parms
		{
			int32 InValue;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_PythonScriptPlugin_eventPyTestDelegate_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_PythonScriptPlugin_eventPyTestDelegate_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n * Delegate to allow testing of the various script delegate features that are exposed to Python wrapped types.\n */" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Delegate to allow testing of the various script delegate features that are exposed to Python wrapped types." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_PythonScriptPlugin, nullptr, "PyTestDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_PythonScriptPlugin_eventPyTestDelegate_Parms), Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EPyTestEnum_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PythonScriptPlugin_EPyTestEnum, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("EPyTestEnum"));
		}
		return Singleton;
	}
	template<> PYTHONSCRIPTPLUGIN_API UEnum* StaticEnum<EPyTestEnum>()
	{
		return EPyTestEnum_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPyTestEnum(EPyTestEnum_StaticEnum, TEXT("/Script/PythonScriptPlugin"), TEXT("EPyTestEnum"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PythonScriptPlugin_EPyTestEnum_Hash() { return 3299996576U; }
	UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPyTestEnum()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPyTestEnum"), 0, Get_Z_Construct_UEnum_PythonScriptPlugin_EPyTestEnum_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPyTestEnum::One", (int64)EPyTestEnum::One },
				{ "EPyTestEnum::Two", (int64)EPyTestEnum::Two },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n * Enum to allow testing of the various UEnum features that are exposed to Python wrapped types.\n */" },
				{ "ModuleRelativePath", "Private/PyTest.h" },
				{ "One.Name", "EPyTestEnum::One" },
				{ "ToolTip", "Enum to allow testing of the various UEnum features that are exposed to Python wrapped types." },
				{ "Two.Name", "EPyTestEnum::Two" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
				nullptr,
				"EPyTestEnum",
				"EPyTestEnum",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}

static_assert(std::is_polymorphic<FPyTestChildStruct>() == std::is_polymorphic<FPyTestStruct>(), "USTRUCT FPyTestChildStruct cannot be polymorphic unless super FPyTestStruct is polymorphic");

class UScriptStruct* FPyTestChildStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PYTHONSCRIPTPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FPyTestChildStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPyTestChildStruct, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("PyTestChildStruct"), sizeof(FPyTestChildStruct), Get_Z_Construct_UScriptStruct_FPyTestChildStruct_Hash());
	}
	return Singleton;
}
template<> PYTHONSCRIPTPLUGIN_API UScriptStruct* StaticStruct<FPyTestChildStruct>()
{
	return FPyTestChildStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPyTestChildStruct(FPyTestChildStruct::StaticStruct, TEXT("/Script/PythonScriptPlugin"), TEXT("PyTestChildStruct"), false, nullptr, nullptr);
static struct FScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPyTestChildStruct
{
	FScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPyTestChildStruct()
	{
		UScriptStruct::DeferCppStructOps<FPyTestChildStruct>(FName(TEXT("PyTestChildStruct")));
	}
} ScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPyTestChildStruct;
	struct Z_Construct_UScriptStruct_FPyTestChildStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestChildStruct_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Struct to allow testing of inheritance on Python wrapped types.\n */" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Struct to allow testing of inheritance on Python wrapped types." },
	};
#endif
	void* Z_Construct_UScriptStruct_FPyTestChildStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPyTestChildStruct>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPyTestChildStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
		Z_Construct_UScriptStruct_FPyTestStruct,
		&NewStructOps,
		"PyTestChildStruct",
		sizeof(FPyTestChildStruct),
		alignof(FPyTestChildStruct),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestChildStruct_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestChildStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPyTestChildStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPyTestChildStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PyTestChildStruct"), sizeof(FPyTestChildStruct), Get_Z_Construct_UScriptStruct_FPyTestChildStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPyTestChildStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPyTestChildStruct_Hash() { return 2799270787U; }
class UScriptStruct* FPyTestStruct::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PYTHONSCRIPTPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FPyTestStruct_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPyTestStruct, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("PyTestStruct"), sizeof(FPyTestStruct), Get_Z_Construct_UScriptStruct_FPyTestStruct_Hash());
	}
	return Singleton;
}
template<> PYTHONSCRIPTPLUGIN_API UScriptStruct* StaticStruct<FPyTestStruct>()
{
	return FPyTestStruct::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPyTestStruct(FPyTestStruct::StaticStruct, TEXT("/Script/PythonScriptPlugin"), TEXT("PyTestStruct"), false, nullptr, nullptr);
static struct FScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPyTestStruct
{
	FScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPyTestStruct()
	{
		UScriptStruct::DeferCppStructOps<FPyTestStruct>(FName(TEXT("PyTestStruct")));
	}
} ScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPyTestStruct;
	struct Z_Construct_UScriptStruct_FPyTestStruct_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bool_MetaData[];
#endif
		static void NewProp_Bool_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Bool;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Enum_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enum_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Enum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_String_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_String;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Text_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Text;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StringArray;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_StringSet;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StringIntMap_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringIntMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringIntMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_StringIntMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LegacyInt_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LegacyInt;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolInstanceOnly_MetaData[];
#endif
		static void NewProp_BoolInstanceOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolInstanceOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolDefaultsOnly_MetaData[];
#endif
		static void NewProp_BoolDefaultsOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolDefaultsOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Struct to allow testing of the various UStruct features that are exposed to Python wrapped types.\n */" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Struct to allow testing of the various UStruct features that are exposed to Python wrapped types." },
	};
#endif
	void* Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPyTestStruct>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Bool_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Bool_SetBit(void* Obj)
	{
		((FPyTestStruct*)Obj)->Bool = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Bool = { "Bool", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FPyTestStruct), &Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Bool_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Bool_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Bool_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Int_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Int = { "Int", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, Int), METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Int_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Int_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Float_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Float = { "Float", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, Float), METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Float_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Float_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Enum_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Enum_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Enum = { "Enum", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, Enum), Z_Construct_UEnum_PythonScriptPlugin_EPyTestEnum, METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Enum_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Enum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_String_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_String = { "String", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, String), METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_String_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_String_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Text_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Text = { "Text", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, Text), METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Text_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Text_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringArray_Inner = { "StringArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringArray_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringArray = { "StringArray", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, StringArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringArray_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringSet_ElementProp = { "StringSet", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringSet_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringSet = { "StringSet", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, StringSet), METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringSet_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap_ValueProp = { "StringIntMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap_Key_KeyProp = { "StringIntMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap = { "StringIntMap", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, StringIntMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_LegacyInt_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "LegacyInt is deprecated. Please use Int instead." },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_LegacyInt = { "LegacyInt", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPyTestStruct, LegacyInt_DEPRECATED), METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_LegacyInt_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_LegacyInt_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolInstanceOnly_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolInstanceOnly_SetBit(void* Obj)
	{
		((FPyTestStruct*)Obj)->BoolInstanceOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolInstanceOnly = { "BoolInstanceOnly", nullptr, (EPropertyFlags)0x0010000000000801, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FPyTestStruct), &Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolInstanceOnly_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolInstanceOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolInstanceOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolDefaultsOnly_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolDefaultsOnly_SetBit(void* Obj)
	{
		((FPyTestStruct*)Obj)->BoolDefaultsOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolDefaultsOnly = { "BoolDefaultsOnly", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FPyTestStruct), &Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolDefaultsOnly_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolDefaultsOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolDefaultsOnly_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPyTestStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Bool,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Int,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Float,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Enum_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Enum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_String,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_Text,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_StringIntMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_LegacyInt,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolInstanceOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPyTestStruct_Statics::NewProp_BoolDefaultsOnly,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPyTestStruct_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
		nullptr,
		&NewStructOps,
		"PyTestStruct",
		sizeof(FPyTestStruct),
		alignof(FPyTestStruct),
		Z_Construct_UScriptStruct_FPyTestStruct_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPyTestStruct_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPyTestStruct_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPyTestStruct()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPyTestStruct_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PyTestStruct"), sizeof(FPyTestStruct), Get_Z_Construct_UScriptStruct_FPyTestStruct_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPyTestStruct_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPyTestStruct_Hash() { return 1216404675U; }
	DEFINE_FUNCTION(UPyTestStructLibrary::execAddStr)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InStruct);
		P_GET_PROPERTY(FStrProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FPyTestStruct*)Z_Param__Result=UPyTestStructLibrary::AddStr(Z_Param_Out_InStruct,Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestStructLibrary::execAddFloat)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InStruct);
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FPyTestStruct*)Z_Param__Result=UPyTestStructLibrary::AddFloat(Z_Param_Out_InStruct,Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestStructLibrary::execAddInt)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InStruct);
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FPyTestStruct*)Z_Param__Result=UPyTestStructLibrary::AddInt(Z_Param_Out_InStruct,Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestStructLibrary::execGetConstantValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UPyTestStructLibrary::GetConstantValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestStructLibrary::execLegacyIsBoolSet)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InStruct);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UPyTestStructLibrary::LegacyIsBoolSet(Z_Param_Out_InStruct);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestStructLibrary::execIsBoolSet)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InStruct);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UPyTestStructLibrary::IsBoolSet(Z_Param_Out_InStruct);
		P_NATIVE_END;
	}
	void UPyTestStructLibrary::StaticRegisterNativesUPyTestStructLibrary()
	{
		UClass* Class = UPyTestStructLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddFloat", &UPyTestStructLibrary::execAddFloat },
			{ "AddInt", &UPyTestStructLibrary::execAddInt },
			{ "AddStr", &UPyTestStructLibrary::execAddStr },
			{ "GetConstantValue", &UPyTestStructLibrary::execGetConstantValue },
			{ "IsBoolSet", &UPyTestStructLibrary::execIsBoolSet },
			{ "LegacyIsBoolSet", &UPyTestStructLibrary::execLegacyIsBoolSet },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics
	{
		struct PyTestStructLibrary_eventAddFloat_Parms
		{
			FPyTestStruct InStruct;
			float InValue;
			FPyTestStruct ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddFloat_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InStruct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddFloat_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddFloat_Parms, ReturnValue), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptMethod", "" },
		{ "ScriptMethodSelfReturn", "" },
		{ "ScriptOperator", "+;+=" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestStructLibrary, nullptr, "AddFloat", nullptr, nullptr, sizeof(PyTestStructLibrary_eventAddFloat_Parms), Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestStructLibrary_AddFloat()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestStructLibrary_AddFloat_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics
	{
		struct PyTestStructLibrary_eventAddInt_Parms
		{
			FPyTestStruct InStruct;
			int32 InValue;
			FPyTestStruct ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddInt_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InStruct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddInt_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddInt_Parms, ReturnValue), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptMethod", "" },
		{ "ScriptMethodSelfReturn", "" },
		{ "ScriptOperator", "+;+=" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestStructLibrary, nullptr, "AddInt", nullptr, nullptr, sizeof(PyTestStructLibrary_eventAddInt_Parms), Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestStructLibrary_AddInt()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestStructLibrary_AddInt_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics
	{
		struct PyTestStructLibrary_eventAddStr_Parms
		{
			FPyTestStruct InStruct;
			FString InValue;
			FPyTestStruct ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddStr_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InStruct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddStr_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventAddStr_Parms, ReturnValue), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptMethod", "" },
		{ "ScriptMethodSelfReturn", "" },
		{ "ScriptOperator", "+;+=" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestStructLibrary, nullptr, "AddStr", nullptr, nullptr, sizeof(PyTestStructLibrary_eventAddStr_Parms), Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestStructLibrary_AddStr()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestStructLibrary_AddStr_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics
	{
		struct PyTestStructLibrary_eventGetConstantValue_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventGetConstantValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptConstant", "ConstantValue" },
		{ "ScriptConstantHost", "PyTestStruct" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestStructLibrary, nullptr, "GetConstantValue", nullptr, nullptr, sizeof(PyTestStructLibrary_eventGetConstantValue_Parms), Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics
	{
		struct PyTestStructLibrary_eventIsBoolSet_Parms
		{
			FPyTestStruct InStruct;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventIsBoolSet_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_InStruct_MetaData)) };
	void Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PyTestStructLibrary_eventIsBoolSet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PyTestStructLibrary_eventIsBoolSet_Parms), &Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_InStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptMethod", "IsBoolSet;IsBoolSetOld" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestStructLibrary, nullptr, "IsBoolSet", nullptr, nullptr, sizeof(PyTestStructLibrary_eventIsBoolSet_Parms), Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics
	{
		struct PyTestStructLibrary_eventLegacyIsBoolSet_Parms
		{
			FPyTestStruct InStruct;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestStructLibrary_eventLegacyIsBoolSet_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_InStruct_MetaData)) };
	void Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PyTestStructLibrary_eventLegacyIsBoolSet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PyTestStructLibrary_eventLegacyIsBoolSet_Parms), &Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_InStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "LegacyIsBoolSet is deprecated. Please use IsBoolSet instead." },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptMethod", "" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestStructLibrary, nullptr, "LegacyIsBoolSet", nullptr, nullptr, sizeof(PyTestStructLibrary_eventLegacyIsBoolSet_Parms), Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14442401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPyTestStructLibrary_NoRegister()
	{
		return UPyTestStructLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UPyTestStructLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPyTestStructLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPyTestStructLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPyTestStructLibrary_AddFloat, "AddFloat" }, // 2237358666
		{ &Z_Construct_UFunction_UPyTestStructLibrary_AddInt, "AddInt" }, // 540868620
		{ &Z_Construct_UFunction_UPyTestStructLibrary_AddStr, "AddStr" }, // 579854508
		{ &Z_Construct_UFunction_UPyTestStructLibrary_GetConstantValue, "GetConstantValue" }, // 2733728097
		{ &Z_Construct_UFunction_UPyTestStructLibrary_IsBoolSet, "IsBoolSet" }, // 2885971646
		{ &Z_Construct_UFunction_UPyTestStructLibrary_LegacyIsBoolSet, "LegacyIsBoolSet" }, // 1765349186
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestStructLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Function library containing methods that should be hoisted onto the test struct in Python.\n */" },
		{ "IncludePath", "PyTest.h" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Function library containing methods that should be hoisted onto the test struct in Python." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPyTestStructLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPyTestStructLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPyTestStructLibrary_Statics::ClassParams = {
		&UPyTestStructLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPyTestStructLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestStructLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPyTestStructLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPyTestStructLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPyTestStructLibrary, 240024106);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPyTestStructLibrary>()
	{
		return UPyTestStructLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPyTestStructLibrary(Z_Construct_UClass_UPyTestStructLibrary, &UPyTestStructLibrary::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPyTestStructLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPyTestStructLibrary);
	DEFINE_FUNCTION(UPyTestObject::execGetConstantValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UPyTestObject::GetConstantValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execEmitScriptWarning)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UPyTestObject::EmitScriptWarning();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execEmitScriptError)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UPyTestObject::EmitScriptError();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execReturnMap)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<int32,bool>*)Z_Param__Result=UPyTestObject::ReturnMap();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execReturnSet)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TSet<int32>*)Z_Param__Result=UPyTestObject::ReturnSet();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execReturnArray)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<int32>*)Z_Param__Result=UPyTestObject::ReturnArray();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execMulticastDelegatePropertyCallback)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InStr);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MulticastDelegatePropertyCallback(Z_Param_InStr);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execDelegatePropertyCallback)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->DelegatePropertyCallback(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execFuncTakingPyTestDelegate)
	{
		P_GET_PROPERTY_REF(FDelegateProperty,Z_Param_Out_InDelegate);
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->FuncTakingPyTestDelegate(FPyTestDelegate(Z_Param_Out_InDelegate),Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execLegacyFuncTakingPyTestStruct)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InStruct);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->LegacyFuncTakingPyTestStruct(Z_Param_Out_InStruct);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execFuncTakingPyTestChildStruct)
	{
		P_GET_STRUCT_REF(FPyTestChildStruct,Z_Param_Out_InStruct);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FuncTakingPyTestChildStruct(Z_Param_Out_InStruct);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execFuncTakingPyTestStruct)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InStruct);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FuncTakingPyTestStruct(Z_Param_Out_InStruct);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execCallFuncBlueprintNativeRef)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InOutStruct);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CallFuncBlueprintNativeRef(Z_Param_Out_InOutStruct);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execCallFuncBlueprintNative)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->CallFuncBlueprintNative(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execCallFuncBlueprintImplementable)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->CallFuncBlueprintImplementable(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execFuncBlueprintNativeRef)
	{
		P_GET_STRUCT_REF(FPyTestStruct,Z_Param_Out_InOutStruct);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->FuncBlueprintNativeRef_Implementation(Z_Param_Out_InOutStruct);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObject::execFuncBlueprintNative)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->FuncBlueprintNative_Implementation(Z_Param_InValue);
		P_NATIVE_END;
	}
	static FName NAME_UPyTestObject_FuncBlueprintImplementable = FName(TEXT("FuncBlueprintImplementable"));
	int32 UPyTestObject::FuncBlueprintImplementable(const int32 InValue) const
	{
		PyTestObject_eventFuncBlueprintImplementable_Parms Parms;
		Parms.InValue=InValue;
		const_cast<UPyTestObject*>(this)->ProcessEvent(FindFunctionChecked(NAME_UPyTestObject_FuncBlueprintImplementable),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UPyTestObject_FuncBlueprintNative = FName(TEXT("FuncBlueprintNative"));
	int32 UPyTestObject::FuncBlueprintNative(const int32 InValue) const
	{
		PyTestObject_eventFuncBlueprintNative_Parms Parms;
		Parms.InValue=InValue;
		const_cast<UPyTestObject*>(this)->ProcessEvent(FindFunctionChecked(NAME_UPyTestObject_FuncBlueprintNative),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UPyTestObject_FuncBlueprintNativeRef = FName(TEXT("FuncBlueprintNativeRef"));
	void UPyTestObject::FuncBlueprintNativeRef(FPyTestStruct& InOutStruct) const
	{
		PyTestObject_eventFuncBlueprintNativeRef_Parms Parms;
		Parms.InOutStruct=InOutStruct;
		const_cast<UPyTestObject*>(this)->ProcessEvent(FindFunctionChecked(NAME_UPyTestObject_FuncBlueprintNativeRef),&Parms);
		InOutStruct=Parms.InOutStruct;
	}
	void UPyTestObject::StaticRegisterNativesUPyTestObject()
	{
		UClass* Class = UPyTestObject::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CallFuncBlueprintImplementable", &UPyTestObject::execCallFuncBlueprintImplementable },
			{ "CallFuncBlueprintNative", &UPyTestObject::execCallFuncBlueprintNative },
			{ "CallFuncBlueprintNativeRef", &UPyTestObject::execCallFuncBlueprintNativeRef },
			{ "DelegatePropertyCallback", &UPyTestObject::execDelegatePropertyCallback },
			{ "EmitScriptError", &UPyTestObject::execEmitScriptError },
			{ "EmitScriptWarning", &UPyTestObject::execEmitScriptWarning },
			{ "FuncBlueprintNative", &UPyTestObject::execFuncBlueprintNative },
			{ "FuncBlueprintNativeRef", &UPyTestObject::execFuncBlueprintNativeRef },
			{ "FuncTakingPyTestChildStruct", &UPyTestObject::execFuncTakingPyTestChildStruct },
			{ "FuncTakingPyTestDelegate", &UPyTestObject::execFuncTakingPyTestDelegate },
			{ "FuncTakingPyTestStruct", &UPyTestObject::execFuncTakingPyTestStruct },
			{ "GetConstantValue", &UPyTestObject::execGetConstantValue },
			{ "LegacyFuncTakingPyTestStruct", &UPyTestObject::execLegacyFuncTakingPyTestStruct },
			{ "MulticastDelegatePropertyCallback", &UPyTestObject::execMulticastDelegatePropertyCallback },
			{ "ReturnArray", &UPyTestObject::execReturnArray },
			{ "ReturnMap", &UPyTestObject::execReturnMap },
			{ "ReturnSet", &UPyTestObject::execReturnSet },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics
	{
		struct PyTestObject_eventCallFuncBlueprintImplementable_Parms
		{
			int32 InValue;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventCallFuncBlueprintImplementable_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventCallFuncBlueprintImplementable_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "CallFuncBlueprintImplementable", nullptr, nullptr, sizeof(PyTestObject_eventCallFuncBlueprintImplementable_Parms), Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics
	{
		struct PyTestObject_eventCallFuncBlueprintNative_Parms
		{
			int32 InValue;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventCallFuncBlueprintNative_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventCallFuncBlueprintNative_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "CallFuncBlueprintNative", nullptr, nullptr, sizeof(PyTestObject_eventCallFuncBlueprintNative_Parms), Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics
	{
		struct PyTestObject_eventCallFuncBlueprintNativeRef_Parms
		{
			FPyTestStruct InOutStruct;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InOutStruct;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::NewProp_InOutStruct = { "InOutStruct", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventCallFuncBlueprintNativeRef_Parms, InOutStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::NewProp_InOutStruct,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "CallFuncBlueprintNativeRef", nullptr, nullptr, sizeof(PyTestObject_eventCallFuncBlueprintNativeRef_Parms), Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics
	{
		struct PyTestObject_eventDelegatePropertyCallback_Parms
		{
			int32 InValue;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventDelegatePropertyCallback_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventDelegatePropertyCallback_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "DelegatePropertyCallback", nullptr, nullptr, sizeof(PyTestObject_eventDelegatePropertyCallback_Parms), Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_EmitScriptError_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_EmitScriptError_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_EmitScriptError_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "EmitScriptError", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_EmitScriptError_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_EmitScriptError_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_EmitScriptError()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_EmitScriptError_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_EmitScriptWarning_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_EmitScriptWarning_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_EmitScriptWarning_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "EmitScriptWarning", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_EmitScriptWarning_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_EmitScriptWarning_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_EmitScriptWarning()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_EmitScriptWarning_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncBlueprintImplementable_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncBlueprintImplementable_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "FuncBlueprintImplementable", nullptr, nullptr, sizeof(PyTestObject_eventFuncBlueprintImplementable_Parms), Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncBlueprintNative_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncBlueprintNative_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "FuncBlueprintNative", nullptr, nullptr, sizeof(PyTestObject_eventFuncBlueprintNative_Parms), Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InOutStruct;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::NewProp_InOutStruct = { "InOutStruct", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncBlueprintNativeRef_Parms, InOutStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::NewProp_InOutStruct,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "FuncBlueprintNativeRef", nullptr, nullptr, sizeof(PyTestObject_eventFuncBlueprintNativeRef_Parms), Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics
	{
		struct PyTestObject_eventFuncTakingPyTestChildStruct_Parms
		{
			FPyTestChildStruct InStruct;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncTakingPyTestChildStruct_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestChildStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::NewProp_InStruct_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::NewProp_InStruct,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "FuncTakingPyTestChildStruct", nullptr, nullptr, sizeof(PyTestObject_eventFuncTakingPyTestChildStruct_Parms), Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics
	{
		struct PyTestObject_eventFuncTakingPyTestDelegate_Parms
		{
			FScriptDelegate InDelegate;
			int32 InValue;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_InDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InDelegate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InDelegate = { "InDelegate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncTakingPyTestDelegate_Parms, InDelegate), Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InValue_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncTakingPyTestDelegate_Parms, InValue), METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InValue_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncTakingPyTestDelegate_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_InValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "FuncTakingPyTestDelegate", nullptr, nullptr, sizeof(PyTestObject_eventFuncTakingPyTestDelegate_Parms), Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics
	{
		struct PyTestObject_eventFuncTakingPyTestStruct_Parms
		{
			FPyTestStruct InStruct;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventFuncTakingPyTestStruct_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::NewProp_InStruct_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::NewProp_InStruct,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "FuncTakingPyTestStruct", nullptr, nullptr, sizeof(PyTestObject_eventFuncTakingPyTestStruct_Parms), Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics
	{
		struct PyTestObject_eventGetConstantValue_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventGetConstantValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptConstant", "ConstantValue" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "GetConstantValue", nullptr, nullptr, sizeof(PyTestObject_eventGetConstantValue_Parms), Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_GetConstantValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_GetConstantValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics
	{
		struct PyTestObject_eventLegacyFuncTakingPyTestStruct_Parms
		{
			FPyTestStruct InStruct;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InStruct;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::NewProp_InStruct_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::NewProp_InStruct = { "InStruct", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventLegacyFuncTakingPyTestStruct_Parms, InStruct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::NewProp_InStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::NewProp_InStruct_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::NewProp_InStruct,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "LegacyFuncTakingPyTestStruct is deprecated. Please use FuncTakingPyTestStruct instead." },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "LegacyFuncTakingPyTestStruct", nullptr, nullptr, sizeof(PyTestObject_eventLegacyFuncTakingPyTestStruct_Parms), Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics
	{
		struct PyTestObject_eventMulticastDelegatePropertyCallback_Parms
		{
			FString InStr;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InStr;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::NewProp_InStr = { "InStr", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventMulticastDelegatePropertyCallback_Parms, InStr), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::NewProp_InStr,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "MulticastDelegatePropertyCallback", nullptr, nullptr, sizeof(PyTestObject_eventMulticastDelegatePropertyCallback_Parms), Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics
	{
		struct PyTestObject_eventReturnArray_Parms
		{
			TArray<int32> ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventReturnArray_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "ReturnArray", nullptr, nullptr, sizeof(PyTestObject_eventReturnArray_Parms), Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_ReturnArray()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_ReturnArray_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics
	{
		struct PyTestObject_eventReturnMap_Parms
		{
			TMap<int32,bool> ReturnValue;
		};
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventReturnMap_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "ReturnMap", nullptr, nullptr, sizeof(PyTestObject_eventReturnMap_Parms), Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_ReturnMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_ReturnMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics
	{
		struct PyTestObject_eventReturnSet_Parms
		{
			TSet<int32> ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue_ElementProp;
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::NewProp_ReturnValue_ElementProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObject_eventReturnSet_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::NewProp_ReturnValue_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObject, nullptr, "ReturnSet", nullptr, nullptr, sizeof(PyTestObject_eventReturnSet_Parms), Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObject_ReturnSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObject_ReturnSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPyTestObject_NoRegister()
	{
		return UPyTestObject::StaticClass();
	}
	struct Z_Construct_UClass_UPyTestObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Bool_MetaData[];
#endif
		static void NewProp_Bool_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Bool;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Int_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Int;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Float_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Float;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Enum_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Enum_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Enum;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_String_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_String;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Text_MetaData[];
#endif
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_Text;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StringArray;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringSet_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_StringSet;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_StringIntMap_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StringIntMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StringIntMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_StringIntMap;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Delegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MulticastDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_MulticastDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Struct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Struct;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StructArray_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StructArray_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StructArray;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChildStruct_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ChildStruct;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolInstanceOnly_MetaData[];
#endif
		static void NewProp_BoolInstanceOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolInstanceOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoolDefaultsOnly_MetaData[];
#endif
		static void NewProp_BoolDefaultsOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_BoolDefaultsOnly;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPyTestObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPyTestObject_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintImplementable, "CallFuncBlueprintImplementable" }, // 185639073
		{ &Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNative, "CallFuncBlueprintNative" }, // 2500242348
		{ &Z_Construct_UFunction_UPyTestObject_CallFuncBlueprintNativeRef, "CallFuncBlueprintNativeRef" }, // 587340598
		{ &Z_Construct_UFunction_UPyTestObject_DelegatePropertyCallback, "DelegatePropertyCallback" }, // 48043399
		{ &Z_Construct_UFunction_UPyTestObject_EmitScriptError, "EmitScriptError" }, // 3612465426
		{ &Z_Construct_UFunction_UPyTestObject_EmitScriptWarning, "EmitScriptWarning" }, // 3355219396
		{ &Z_Construct_UFunction_UPyTestObject_FuncBlueprintImplementable, "FuncBlueprintImplementable" }, // 3434132332
		{ &Z_Construct_UFunction_UPyTestObject_FuncBlueprintNative, "FuncBlueprintNative" }, // 2816378474
		{ &Z_Construct_UFunction_UPyTestObject_FuncBlueprintNativeRef, "FuncBlueprintNativeRef" }, // 3694897308
		{ &Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestChildStruct, "FuncTakingPyTestChildStruct" }, // 2298903216
		{ &Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestDelegate, "FuncTakingPyTestDelegate" }, // 989677282
		{ &Z_Construct_UFunction_UPyTestObject_FuncTakingPyTestStruct, "FuncTakingPyTestStruct" }, // 3643169066
		{ &Z_Construct_UFunction_UPyTestObject_GetConstantValue, "GetConstantValue" }, // 1646260184
		{ &Z_Construct_UFunction_UPyTestObject_LegacyFuncTakingPyTestStruct, "LegacyFuncTakingPyTestStruct" }, // 3797729652
		{ &Z_Construct_UFunction_UPyTestObject_MulticastDelegatePropertyCallback, "MulticastDelegatePropertyCallback" }, // 690889164
		{ &Z_Construct_UFunction_UPyTestObject_ReturnArray, "ReturnArray" }, // 992859314
		{ &Z_Construct_UFunction_UPyTestObject_ReturnMap, "ReturnMap" }, // 8458149
		{ &Z_Construct_UFunction_UPyTestObject_ReturnSet, "ReturnSet" }, // 2479104477
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Object to allow testing of the various UObject features that are exposed to Python wrapped types.\n */" },
		{ "IncludePath", "PyTest.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Object to allow testing of the various UObject features that are exposed to Python wrapped types." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Bool_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	void Z_Construct_UClass_UPyTestObject_Statics::NewProp_Bool_SetBit(void* Obj)
	{
		((UPyTestObject*)Obj)->Bool = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Bool = { "Bool", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPyTestObject), &Z_Construct_UClass_UPyTestObject_Statics::NewProp_Bool_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Bool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Bool_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Int_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Int = { "Int", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, Int), METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Int_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Int_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Float_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Float = { "Float", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, Float), METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Float_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Float_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Enum_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Enum_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Enum = { "Enum", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, Enum), Z_Construct_UEnum_PythonScriptPlugin_EPyTestEnum, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Enum_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Enum_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_String_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_String = { "String", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, String), METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_String_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_String_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, Name), METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Text_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Text = { "Text", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, Text), METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Text_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Text_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringArray_Inner = { "StringArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringArray_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringArray = { "StringArray", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, StringArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringArray_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringSet_ElementProp = { "StringSet", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringSet_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringSet = { "StringSet", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, StringSet), METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringSet_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap_ValueProp = { "StringIntMap", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap_Key_KeyProp = { "StringIntMap_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap = { "StringIntMap", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, StringIntMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Delegate_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000080005, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, Delegate), Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Delegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Delegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_MulticastDelegate_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_MulticastDelegate = { "MulticastDelegate", nullptr, (EPropertyFlags)0x0010000010080001, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, MulticastDelegate), Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_MulticastDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_MulticastDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_Struct_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_Struct = { "Struct", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, Struct), Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Struct_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_Struct_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StructArray_Inner = { "StructArray", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FPyTestStruct, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_StructArray_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_StructArray = { "StructArray", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, StructArray), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StructArray_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_StructArray_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_ChildStruct_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_ChildStruct = { "ChildStruct", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPyTestObject, ChildStruct), Z_Construct_UScriptStruct_FPyTestChildStruct, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_ChildStruct_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_ChildStruct_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolInstanceOnly_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	void Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolInstanceOnly_SetBit(void* Obj)
	{
		((UPyTestObject*)Obj)->BoolInstanceOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolInstanceOnly = { "BoolInstanceOnly", nullptr, (EPropertyFlags)0x0010000000000801, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPyTestObject), &Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolInstanceOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolInstanceOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolInstanceOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolDefaultsOnly_MetaData[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
	};
#endif
	void Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolDefaultsOnly_SetBit(void* Obj)
	{
		((UPyTestObject*)Obj)->BoolDefaultsOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolDefaultsOnly = { "BoolDefaultsOnly", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPyTestObject), &Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolDefaultsOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolDefaultsOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolDefaultsOnly_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPyTestObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Bool,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Int,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Float,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Enum_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Enum,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_String,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Text,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringSet_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StringIntMap,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Delegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_MulticastDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_Struct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StructArray_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_StructArray,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_ChildStruct,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolInstanceOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPyTestObject_Statics::NewProp_BoolDefaultsOnly,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPyTestObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPyTestObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPyTestObject_Statics::ClassParams = {
		&UPyTestObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UPyTestObject_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPyTestObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPyTestObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPyTestObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPyTestObject, 625293596);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPyTestObject>()
	{
		return UPyTestObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPyTestObject(Z_Construct_UClass_UPyTestObject, &UPyTestObject::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPyTestObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPyTestObject);
	void UPyTestChildObject::StaticRegisterNativesUPyTestChildObject()
	{
	}
	UClass* Z_Construct_UClass_UPyTestChildObject_NoRegister()
	{
		return UPyTestChildObject::StaticClass();
	}
	struct Z_Construct_UClass_UPyTestChildObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPyTestChildObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPyTestObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestChildObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Object to allow testing of inheritance on Python wrapped types.\n */" },
		{ "IncludePath", "PyTest.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Object to allow testing of inheritance on Python wrapped types." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPyTestChildObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPyTestChildObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPyTestChildObject_Statics::ClassParams = {
		&UPyTestChildObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPyTestChildObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestChildObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPyTestChildObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPyTestChildObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPyTestChildObject, 1646669619);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPyTestChildObject>()
	{
		return UPyTestChildObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPyTestChildObject(Z_Construct_UClass_UPyTestChildObject, &UPyTestChildObject::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPyTestChildObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPyTestChildObject);
	void UDEPRECATED_LegacyPyTestObject::StaticRegisterNativesUDEPRECATED_LegacyPyTestObject()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_NoRegister()
	{
		return UDEPRECATED_LegacyPyTestObject::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPyTestObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Object to test deprecation of Python wrapped types.\n */" },
		{ "DeprecationMessage", "LegacyPyTestObject is deprecated. Please use PyTestObject instead." },
		{ "IncludePath", "PyTest.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Object to test deprecation of Python wrapped types." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_LegacyPyTestObject>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics::ClassParams = {
		&UDEPRECATED_LegacyPyTestObject::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x020002A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_LegacyPyTestObject, 2617268032);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UDEPRECATED_LegacyPyTestObject>()
	{
		return UDEPRECATED_LegacyPyTestObject::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_LegacyPyTestObject(Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject, &UDEPRECATED_LegacyPyTestObject::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UDEPRECATED_LegacyPyTestObject"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_LegacyPyTestObject);
	DEFINE_FUNCTION(UPyTestObjectLibrary::execGetOtherConstantValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UPyTestObjectLibrary::GetOtherConstantValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UPyTestObjectLibrary::execIsBoolSet)
	{
		P_GET_OBJECT(UPyTestObject,Z_Param_InObj);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UPyTestObjectLibrary::IsBoolSet(Z_Param_InObj);
		P_NATIVE_END;
	}
	void UPyTestObjectLibrary::StaticRegisterNativesUPyTestObjectLibrary()
	{
		UClass* Class = UPyTestObjectLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetOtherConstantValue", &UPyTestObjectLibrary::execGetOtherConstantValue },
			{ "IsBoolSet", &UPyTestObjectLibrary::execIsBoolSet },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics
	{
		struct PyTestObjectLibrary_eventGetOtherConstantValue_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObjectLibrary_eventGetOtherConstantValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptConstant", "OtherConstantValue" },
		{ "ScriptConstantHost", "PyTestObject" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObjectLibrary, nullptr, "GetOtherConstantValue", nullptr, nullptr, sizeof(PyTestObjectLibrary_eventGetOtherConstantValue_Parms), Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics
	{
		struct PyTestObjectLibrary_eventIsBoolSet_Parms
		{
			const UPyTestObject* InObj;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InObj_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObj;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_InObj_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_InObj = { "InObj", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(PyTestObjectLibrary_eventIsBoolSet_Parms, InObj), Z_Construct_UClass_UPyTestObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_InObj_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_InObj_MetaData)) };
	void Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((PyTestObjectLibrary_eventIsBoolSet_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(PyTestObjectLibrary_eventIsBoolSet_Parms), &Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_InObj,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python|Internal" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ScriptMethod", "IsBoolSet" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UPyTestObjectLibrary, nullptr, "IsBoolSet", nullptr, nullptr, sizeof(PyTestObjectLibrary_eventIsBoolSet_Parms), Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UPyTestObjectLibrary_NoRegister()
	{
		return UPyTestObjectLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UPyTestObjectLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPyTestObjectLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UPyTestObjectLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UPyTestObjectLibrary_GetOtherConstantValue, "GetOtherConstantValue" }, // 4101441048
		{ &Z_Construct_UFunction_UPyTestObjectLibrary_IsBoolSet, "IsBoolSet" }, // 715807886
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPyTestObjectLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Function library containing methods that should be hoisted onto the test object in Python.\n */" },
		{ "IncludePath", "PyTest.h" },
		{ "ModuleRelativePath", "Private/PyTest.h" },
		{ "ToolTip", "Function library containing methods that should be hoisted onto the test object in Python." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPyTestObjectLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPyTestObjectLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPyTestObjectLibrary_Statics::ClassParams = {
		&UPyTestObjectLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPyTestObjectLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPyTestObjectLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPyTestObjectLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPyTestObjectLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPyTestObjectLibrary, 740007589);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPyTestObjectLibrary>()
	{
		return UPyTestObjectLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPyTestObjectLibrary(Z_Construct_UClass_UPyTestObjectLibrary, &UPyTestObjectLibrary::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPyTestObjectLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPyTestObjectLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
