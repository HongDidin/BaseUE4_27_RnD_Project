// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Private/PyWrapperStruct.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePyWrapperStruct() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonGeneratedStruct_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonGeneratedStruct();
	COREUOBJECT_API UClass* Z_Construct_UClass_UScriptStruct();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonResourceOwner_NoRegister();
// End Cross Module References
	void UPythonGeneratedStruct::StaticRegisterNativesUPythonGeneratedStruct()
	{
	}
	UClass* Z_Construct_UClass_UPythonGeneratedStruct_NoRegister()
	{
		return UPythonGeneratedStruct::StaticClass();
	}
	struct Z_Construct_UClass_UPythonGeneratedStruct_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPythonGeneratedStruct_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UScriptStruct,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonGeneratedStruct_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** An Unreal struct that was generated from a Python type */" },
		{ "IncludePath", "PyWrapperStruct.h" },
		{ "ModuleRelativePath", "Private/PyWrapperStruct.h" },
		{ "ToolTip", "An Unreal struct that was generated from a Python type" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UPythonGeneratedStruct_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UPythonResourceOwner_NoRegister, (int32)VTABLE_OFFSET(UPythonGeneratedStruct, IPythonResourceOwner), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPythonGeneratedStruct_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPythonGeneratedStruct>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPythonGeneratedStruct_Statics::ClassParams = {
		&UPythonGeneratedStruct::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPythonGeneratedStruct_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonGeneratedStruct_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPythonGeneratedStruct()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPythonGeneratedStruct_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPythonGeneratedStruct, 1363590949);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPythonGeneratedStruct>()
	{
		return UPythonGeneratedStruct::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPythonGeneratedStruct(Z_Construct_UClass_UPythonGeneratedStruct, &UPythonGeneratedStruct::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPythonGeneratedStruct"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPythonGeneratedStruct);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
