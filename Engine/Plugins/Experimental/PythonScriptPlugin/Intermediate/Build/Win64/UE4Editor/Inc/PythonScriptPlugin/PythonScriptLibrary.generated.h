// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FPythonLogOutputEntry;
enum class EPythonCommandExecutionMode : uint8;
enum class EPythonFileExecutionScope : uint8;
#ifdef PYTHONSCRIPTPLUGIN_PythonScriptLibrary_generated_h
#error "PythonScriptLibrary.generated.h already included, missing '#pragma once' in PythonScriptLibrary.h"
#endif
#define PYTHONSCRIPTPLUGIN_PythonScriptLibrary_generated_h

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execExecutePythonCommandEx); \
	DECLARE_FUNCTION(execExecutePythonCommand); \
	DECLARE_FUNCTION(execIsPythonAvailable);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execExecutePythonCommandEx); \
	DECLARE_FUNCTION(execExecutePythonCommand); \
	DECLARE_FUNCTION(execIsPythonAvailable);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPythonScriptLibrary(); \
	friend struct Z_Construct_UClass_UPythonScriptLibrary_Statics; \
public: \
	DECLARE_CLASS(UPythonScriptLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonScriptLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUPythonScriptLibrary(); \
	friend struct Z_Construct_UClass_UPythonScriptLibrary_Statics; \
public: \
	DECLARE_CLASS(UPythonScriptLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonScriptLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPythonScriptLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonScriptLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonScriptLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonScriptLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonScriptLibrary(UPythonScriptLibrary&&); \
	NO_API UPythonScriptLibrary(const UPythonScriptLibrary&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPythonScriptLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonScriptLibrary(UPythonScriptLibrary&&); \
	NO_API UPythonScriptLibrary(const UPythonScriptLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonScriptLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonScriptLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonScriptLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_10_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPythonScriptLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
