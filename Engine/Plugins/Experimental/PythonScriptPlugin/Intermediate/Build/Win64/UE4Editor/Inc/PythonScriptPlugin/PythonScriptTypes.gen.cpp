// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Public/PythonScriptTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePythonScriptTypes() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonFileExecutionScope();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
	PYTHONSCRIPTPLUGIN_API UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandExecutionMode();
	PYTHONSCRIPTPLUGIN_API UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandFlags();
	PYTHONSCRIPTPLUGIN_API UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonLogOutputType();
	PYTHONSCRIPTPLUGIN_API UScriptStruct* Z_Construct_UScriptStruct_FPythonLogOutputEntry();
// End Cross Module References
	static UEnum* EPythonFileExecutionScope_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PythonScriptPlugin_EPythonFileExecutionScope, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("EPythonFileExecutionScope"));
		}
		return Singleton;
	}
	template<> PYTHONSCRIPTPLUGIN_API UEnum* StaticEnum<EPythonFileExecutionScope>()
	{
		return EPythonFileExecutionScope_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPythonFileExecutionScope(EPythonFileExecutionScope_StaticEnum, TEXT("/Script/PythonScriptPlugin"), TEXT("EPythonFileExecutionScope"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonFileExecutionScope_Hash() { return 3630802404U; }
	UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonFileExecutionScope()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPythonFileExecutionScope"), 0, Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonFileExecutionScope_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPythonFileExecutionScope::Private", (int64)EPythonFileExecutionScope::Private },
				{ "EPythonFileExecutionScope::Public", (int64)EPythonFileExecutionScope::Public },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Controls the scope used when executing Python files. */" },
				{ "ModuleRelativePath", "Public/PythonScriptTypes.h" },
				{ "Private.Comment", "/** Execute the Python file with its own unique locals and globals dict to isolate any changes it makes to the environment (like imports). */" },
				{ "Private.Name", "EPythonFileExecutionScope::Private" },
				{ "Private.ToolTip", "Execute the Python file with its own unique locals and globals dict to isolate any changes it makes to the environment (like imports)." },
				{ "Public.Comment", "/** Execute the Python file with the shared locals and globals dict as used by the console, so that executing it behaves as if you'd ran the file contents directly in the console. */" },
				{ "Public.Name", "EPythonFileExecutionScope::Public" },
				{ "Public.ToolTip", "Execute the Python file with the shared locals and globals dict as used by the console, so that executing it behaves as if you'd ran the file contents directly in the console." },
				{ "ToolTip", "Controls the scope used when executing Python files." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
				nullptr,
				"EPythonFileExecutionScope",
				"EPythonFileExecutionScope",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPythonCommandExecutionMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandExecutionMode, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("EPythonCommandExecutionMode"));
		}
		return Singleton;
	}
	template<> PYTHONSCRIPTPLUGIN_API UEnum* StaticEnum<EPythonCommandExecutionMode>()
	{
		return EPythonCommandExecutionMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPythonCommandExecutionMode(EPythonCommandExecutionMode_StaticEnum, TEXT("/Script/PythonScriptPlugin"), TEXT("EPythonCommandExecutionMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandExecutionMode_Hash() { return 4200445368U; }
	UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandExecutionMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPythonCommandExecutionMode"), 0, Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandExecutionMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPythonCommandExecutionMode::ExecuteFile", (int64)EPythonCommandExecutionMode::ExecuteFile },
				{ "EPythonCommandExecutionMode::ExecuteStatement", (int64)EPythonCommandExecutionMode::ExecuteStatement },
				{ "EPythonCommandExecutionMode::EvaluateStatement", (int64)EPythonCommandExecutionMode::EvaluateStatement },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Controls the execution mode used for the Python command. */" },
				{ "EvaluateStatement.Comment", "/** Evaluate the Python command as a single statement. This will evaluate a single statement and return the result. This mode cannot run files. */" },
				{ "EvaluateStatement.Name", "EPythonCommandExecutionMode::EvaluateStatement" },
				{ "EvaluateStatement.ToolTip", "Evaluate the Python command as a single statement. This will evaluate a single statement and return the result. This mode cannot run files." },
				{ "ExecuteFile.Comment", "/** Execute the Python command as a file. This allows you to execute either a literal Python script containing multiple statements, or a file with optional arguments. */" },
				{ "ExecuteFile.Name", "EPythonCommandExecutionMode::ExecuteFile" },
				{ "ExecuteFile.ToolTip", "Execute the Python command as a file. This allows you to execute either a literal Python script containing multiple statements, or a file with optional arguments." },
				{ "ExecuteStatement.Comment", "/** Execute the Python command as a single statement. This will execute a single statement and print the result. This mode cannot run files. */" },
				{ "ExecuteStatement.Name", "EPythonCommandExecutionMode::ExecuteStatement" },
				{ "ExecuteStatement.ToolTip", "Execute the Python command as a single statement. This will execute a single statement and print the result. This mode cannot run files." },
				{ "ModuleRelativePath", "Public/PythonScriptTypes.h" },
				{ "ToolTip", "Controls the execution mode used for the Python command." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
				nullptr,
				"EPythonCommandExecutionMode",
				"EPythonCommandExecutionMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPythonCommandFlags_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandFlags, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("EPythonCommandFlags"));
		}
		return Singleton;
	}
	template<> PYTHONSCRIPTPLUGIN_API UEnum* StaticEnum<EPythonCommandFlags>()
	{
		return EPythonCommandFlags_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPythonCommandFlags(EPythonCommandFlags_StaticEnum, TEXT("/Script/PythonScriptPlugin"), TEXT("EPythonCommandFlags"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandFlags_Hash() { return 1223089418U; }
	UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandFlags()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPythonCommandFlags"), 0, Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonCommandFlags_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPythonCommandFlags::None", (int64)EPythonCommandFlags::None },
				{ "EPythonCommandFlags::Unattended", (int64)EPythonCommandFlags::Unattended },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Flags that can be specified when running Python commands. */" },
				{ "ModuleRelativePath", "Public/PythonScriptTypes.h" },
				{ "None.Comment", "/** No special behavior. */" },
				{ "None.Name", "EPythonCommandFlags::None" },
				{ "None.ToolTip", "No special behavior." },
				{ "ToolTip", "Flags that can be specified when running Python commands." },
				{ "Unattended.Comment", "/** Run the Python command in \"unattended\" mode (GIsRunningUnattendedScript set to true), which will suppress certain pieces of UI. */" },
				{ "Unattended.Name", "EPythonCommandFlags::Unattended" },
				{ "Unattended.ToolTip", "Run the Python command in \"unattended\" mode (GIsRunningUnattendedScript set to true), which will suppress certain pieces of UI." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
				nullptr,
				"EPythonCommandFlags",
				"EPythonCommandFlags",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EPythonLogOutputType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_PythonScriptPlugin_EPythonLogOutputType, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("EPythonLogOutputType"));
		}
		return Singleton;
	}
	template<> PYTHONSCRIPTPLUGIN_API UEnum* StaticEnum<EPythonLogOutputType>()
	{
		return EPythonLogOutputType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPythonLogOutputType(EPythonLogOutputType_StaticEnum, TEXT("/Script/PythonScriptPlugin"), TEXT("EPythonLogOutputType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonLogOutputType_Hash() { return 1295187048U; }
	UEnum* Z_Construct_UEnum_PythonScriptPlugin_EPythonLogOutputType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPythonLogOutputType"), 0, Get_Z_Construct_UEnum_PythonScriptPlugin_EPythonLogOutputType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPythonLogOutputType::Info", (int64)EPythonLogOutputType::Info },
				{ "EPythonLogOutputType::Warning", (int64)EPythonLogOutputType::Warning },
				{ "EPythonLogOutputType::Error", (int64)EPythonLogOutputType::Error },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Types of log output that Python can give. */" },
				{ "Error.Comment", "/** This log was an error. */" },
				{ "Error.Name", "EPythonLogOutputType::Error" },
				{ "Error.ToolTip", "This log was an error." },
				{ "Info.Comment", "/** This log was informative. */" },
				{ "Info.Name", "EPythonLogOutputType::Info" },
				{ "Info.ToolTip", "This log was informative." },
				{ "ModuleRelativePath", "Public/PythonScriptTypes.h" },
				{ "ToolTip", "Types of log output that Python can give." },
				{ "Warning.Comment", "/** This log was a warning. */" },
				{ "Warning.Name", "EPythonLogOutputType::Warning" },
				{ "Warning.ToolTip", "This log was a warning." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
				nullptr,
				"EPythonLogOutputType",
				"EPythonLogOutputType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FPythonLogOutputEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PYTHONSCRIPTPLUGIN_API uint32 Get_Z_Construct_UScriptStruct_FPythonLogOutputEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPythonLogOutputEntry, Z_Construct_UPackage__Script_PythonScriptPlugin(), TEXT("PythonLogOutputEntry"), sizeof(FPythonLogOutputEntry), Get_Z_Construct_UScriptStruct_FPythonLogOutputEntry_Hash());
	}
	return Singleton;
}
template<> PYTHONSCRIPTPLUGIN_API UScriptStruct* StaticStruct<FPythonLogOutputEntry>()
{
	return FPythonLogOutputEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPythonLogOutputEntry(FPythonLogOutputEntry::StaticStruct, TEXT("/Script/PythonScriptPlugin"), TEXT("PythonLogOutputEntry"), false, nullptr, nullptr);
static struct FScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPythonLogOutputEntry
{
	FScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPythonLogOutputEntry()
	{
		UScriptStruct::DeferCppStructOps<FPythonLogOutputEntry>(FName(TEXT("PythonLogOutputEntry")));
	}
} ScriptStruct_PythonScriptPlugin_StaticRegisterNativesFPythonLogOutputEntry;
	struct Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Type_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Type;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Output_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Output;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Log output entry captured from Python. */" },
		{ "ModuleRelativePath", "Public/PythonScriptTypes.h" },
		{ "ToolTip", "Log output entry captured from Python." },
	};
#endif
	void* Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPythonLogOutputEntry>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Type_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Python|Output" },
		{ "Comment", "/** The type of the log output. */" },
		{ "ModuleRelativePath", "Public/PythonScriptTypes.h" },
		{ "ToolTip", "The type of the log output." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPythonLogOutputEntry, Type), Z_Construct_UEnum_PythonScriptPlugin_EPythonLogOutputType, METADATA_PARAMS(Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Type_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Output_MetaData[] = {
		{ "Category", "Python|Output" },
		{ "Comment", "/** The log output string. */" },
		{ "ModuleRelativePath", "Public/PythonScriptTypes.h" },
		{ "ToolTip", "The log output string." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Output = { "Output", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPythonLogOutputEntry, Output), METADATA_PARAMS(Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Output_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Output_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Type_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Type,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::NewProp_Output,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
		nullptr,
		&NewStructOps,
		"PythonLogOutputEntry",
		sizeof(FPythonLogOutputEntry),
		alignof(FPythonLogOutputEntry),
		Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPythonLogOutputEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPythonLogOutputEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_PythonScriptPlugin();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PythonLogOutputEntry"), sizeof(FPythonLogOutputEntry), Get_Z_Construct_UScriptStruct_FPythonLogOutputEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPythonLogOutputEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPythonLogOutputEntry_Hash() { return 2057858719U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
