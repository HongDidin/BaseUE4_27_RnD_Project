// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PYTHONSCRIPTPLUGIN_PyWrapperEnum_generated_h
#error "PyWrapperEnum.generated.h already included, missing '#pragma once' in PyWrapperEnum.h"
#endif
#define PYTHONSCRIPTPLUGIN_PyWrapperEnum_generated_h

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPythonGeneratedEnum(); \
	friend struct Z_Construct_UClass_UPythonGeneratedEnum_Statics; \
public: \
	DECLARE_CLASS(UPythonGeneratedEnum, UEnum, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonGeneratedEnum) \
	virtual UObject* _getUObject() const override { return const_cast<UPythonGeneratedEnum*>(this); }


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_INCLASS \
private: \
	static void StaticRegisterNativesUPythonGeneratedEnum(); \
	friend struct Z_Construct_UClass_UPythonGeneratedEnum_Statics; \
public: \
	DECLARE_CLASS(UPythonGeneratedEnum, UEnum, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonGeneratedEnum) \
	virtual UObject* _getUObject() const override { return const_cast<UPythonGeneratedEnum*>(this); }


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPythonGeneratedEnum(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonGeneratedEnum) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonGeneratedEnum); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonGeneratedEnum); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonGeneratedEnum(UPythonGeneratedEnum&&); \
	NO_API UPythonGeneratedEnum(const UPythonGeneratedEnum&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPythonGeneratedEnum(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonGeneratedEnum(UPythonGeneratedEnum&&); \
	NO_API UPythonGeneratedEnum(const UPythonGeneratedEnum&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonGeneratedEnum); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonGeneratedEnum); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonGeneratedEnum)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_115_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h_118_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPythonGeneratedEnum>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyWrapperEnum_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
