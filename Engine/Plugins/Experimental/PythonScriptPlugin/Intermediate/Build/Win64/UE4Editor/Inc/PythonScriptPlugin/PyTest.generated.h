// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FPyTestStruct;
 
struct FPyTestChildStruct;
class UPyTestObject;
#ifdef PYTHONSCRIPTPLUGIN_PyTest_generated_h
#error "PyTest.generated.h already included, missing '#pragma once' in PyTest.h"
#endif
#define PYTHONSCRIPTPLUGIN_PyTest_generated_h

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_86_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPyTestChildStruct_Statics; \
	PYTHONSCRIPTPLUGIN_API static class UScriptStruct* StaticStruct(); \
	typedef FPyTestStruct Super;


template<> PYTHONSCRIPTPLUGIN_API UScriptStruct* StaticStruct<struct FPyTestChildStruct>();

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_35_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPyTestStruct_Statics; \
	PYTHONSCRIPTPLUGIN_API static class UScriptStruct* StaticStruct();


template<> PYTHONSCRIPTPLUGIN_API UScriptStruct* StaticStruct<struct FPyTestStruct>();

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_17_DELEGATE \
struct _Script_PythonScriptPlugin_eventPyTestMulticastDelegate_Parms \
{ \
	FString InStr; \
}; \
static inline void FPyTestMulticastDelegate_DelegateWrapper(const FMulticastScriptDelegate& PyTestMulticastDelegate, const FString& InStr) \
{ \
	_Script_PythonScriptPlugin_eventPyTestMulticastDelegate_Parms Parms; \
	Parms.InStr=InStr; \
	PyTestMulticastDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_12_DELEGATE \
struct _Script_PythonScriptPlugin_eventPyTestDelegate_Parms \
{ \
	int32 InValue; \
	int32 ReturnValue; \
 \
	/** Constructor, initializes return property only **/ \
	_Script_PythonScriptPlugin_eventPyTestDelegate_Parms() \
		: ReturnValue(0) \
	{ \
	} \
}; \
static inline int32 FPyTestDelegate_DelegateWrapper(const FScriptDelegate& PyTestDelegate, int32 InValue) \
{ \
	_Script_PythonScriptPlugin_eventPyTestDelegate_Parms Parms; \
	Parms.InValue=InValue; \
	PyTestDelegate.ProcessDelegate<UObject>(&Parms); \
	return Parms.ReturnValue; \
}


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execAddStr); \
	DECLARE_FUNCTION(execAddFloat); \
	DECLARE_FUNCTION(execAddInt); \
	DECLARE_FUNCTION(execGetConstantValue); \
	DECLARE_FUNCTION(execLegacyIsBoolSet); \
	DECLARE_FUNCTION(execIsBoolSet);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execAddStr); \
	DECLARE_FUNCTION(execAddFloat); \
	DECLARE_FUNCTION(execAddInt); \
	DECLARE_FUNCTION(execGetConstantValue); \
	DECLARE_FUNCTION(execLegacyIsBoolSet); \
	DECLARE_FUNCTION(execIsBoolSet);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPyTestStructLibrary(); \
	friend struct Z_Construct_UClass_UPyTestStructLibrary_Statics; \
public: \
	DECLARE_CLASS(UPyTestStructLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestStructLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_INCLASS \
private: \
	static void StaticRegisterNativesUPyTestStructLibrary(); \
	friend struct Z_Construct_UClass_UPyTestStructLibrary_Statics; \
public: \
	DECLARE_CLASS(UPyTestStructLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestStructLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyTestStructLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyTestStructLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestStructLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestStructLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestStructLibrary(UPyTestStructLibrary&&); \
	NO_API UPyTestStructLibrary(const UPyTestStructLibrary&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyTestStructLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestStructLibrary(UPyTestStructLibrary&&); \
	NO_API UPyTestStructLibrary(const UPyTestStructLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestStructLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestStructLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyTestStructLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_92_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_95_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPyTestStructLibrary>();

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_RPC_WRAPPERS \
	virtual void FuncBlueprintNativeRef_Implementation(FPyTestStruct& InOutStruct) const; \
	virtual int32 FuncBlueprintNative_Implementation(const int32 InValue) const; \
 \
	DECLARE_FUNCTION(execGetConstantValue); \
	DECLARE_FUNCTION(execEmitScriptWarning); \
	DECLARE_FUNCTION(execEmitScriptError); \
	DECLARE_FUNCTION(execReturnMap); \
	DECLARE_FUNCTION(execReturnSet); \
	DECLARE_FUNCTION(execReturnArray); \
	DECLARE_FUNCTION(execMulticastDelegatePropertyCallback); \
	DECLARE_FUNCTION(execDelegatePropertyCallback); \
	DECLARE_FUNCTION(execFuncTakingPyTestDelegate); \
	DECLARE_FUNCTION(execLegacyFuncTakingPyTestStruct); \
	DECLARE_FUNCTION(execFuncTakingPyTestChildStruct); \
	DECLARE_FUNCTION(execFuncTakingPyTestStruct); \
	DECLARE_FUNCTION(execCallFuncBlueprintNativeRef); \
	DECLARE_FUNCTION(execCallFuncBlueprintNative); \
	DECLARE_FUNCTION(execCallFuncBlueprintImplementable); \
	DECLARE_FUNCTION(execFuncBlueprintNativeRef); \
	DECLARE_FUNCTION(execFuncBlueprintNative);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void FuncBlueprintNativeRef_Implementation(FPyTestStruct& InOutStruct) const; \
	virtual int32 FuncBlueprintNative_Implementation(const int32 InValue) const; \
 \
	DECLARE_FUNCTION(execGetConstantValue); \
	DECLARE_FUNCTION(execEmitScriptWarning); \
	DECLARE_FUNCTION(execEmitScriptError); \
	DECLARE_FUNCTION(execReturnMap); \
	DECLARE_FUNCTION(execReturnSet); \
	DECLARE_FUNCTION(execReturnArray); \
	DECLARE_FUNCTION(execMulticastDelegatePropertyCallback); \
	DECLARE_FUNCTION(execDelegatePropertyCallback); \
	DECLARE_FUNCTION(execFuncTakingPyTestDelegate); \
	DECLARE_FUNCTION(execLegacyFuncTakingPyTestStruct); \
	DECLARE_FUNCTION(execFuncTakingPyTestChildStruct); \
	DECLARE_FUNCTION(execFuncTakingPyTestStruct); \
	DECLARE_FUNCTION(execCallFuncBlueprintNativeRef); \
	DECLARE_FUNCTION(execCallFuncBlueprintNative); \
	DECLARE_FUNCTION(execCallFuncBlueprintImplementable); \
	DECLARE_FUNCTION(execFuncBlueprintNativeRef); \
	DECLARE_FUNCTION(execFuncBlueprintNative);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_EVENT_PARMS \
	struct PyTestObject_eventFuncBlueprintImplementable_Parms \
	{ \
		int32 InValue; \
		int32 ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		PyTestObject_eventFuncBlueprintImplementable_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct PyTestObject_eventFuncBlueprintNative_Parms \
	{ \
		int32 InValue; \
		int32 ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		PyTestObject_eventFuncBlueprintNative_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct PyTestObject_eventFuncBlueprintNativeRef_Parms \
	{ \
		FPyTestStruct InOutStruct; \
	};


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPyTestObject(); \
	friend struct Z_Construct_UClass_UPyTestObject_Statics; \
public: \
	DECLARE_CLASS(UPyTestObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_INCLASS \
private: \
	static void StaticRegisterNativesUPyTestObject(); \
	friend struct Z_Construct_UClass_UPyTestObject_Statics; \
public: \
	DECLARE_CLASS(UPyTestObject, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyTestObject(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyTestObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestObject(UPyTestObject&&); \
	NO_API UPyTestObject(const UPyTestObject&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestObject(UPyTestObject&&); \
	NO_API UPyTestObject(const UPyTestObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPyTestObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_119_PROLOG \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_EVENT_PARMS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_122_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPyTestObject>();

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPyTestChildObject(); \
	friend struct Z_Construct_UClass_UPyTestChildObject_Statics; \
public: \
	DECLARE_CLASS(UPyTestChildObject, UPyTestObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestChildObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_INCLASS \
private: \
	static void StaticRegisterNativesUPyTestChildObject(); \
	friend struct Z_Construct_UClass_UPyTestChildObject_Statics; \
public: \
	DECLARE_CLASS(UPyTestChildObject, UPyTestObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestChildObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyTestChildObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyTestChildObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestChildObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestChildObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestChildObject(UPyTestChildObject&&); \
	NO_API UPyTestChildObject(const UPyTestChildObject&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyTestChildObject() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestChildObject(UPyTestChildObject&&); \
	NO_API UPyTestChildObject(const UPyTestChildObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestChildObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestChildObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPyTestChildObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_236_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_239_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPyTestChildObject>();

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_LegacyPyTestObject(); \
	friend struct Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_LegacyPyTestObject, UPyTestObject, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_LegacyPyTestObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_LegacyPyTestObject(); \
	friend struct Z_Construct_UClass_UDEPRECATED_LegacyPyTestObject_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_LegacyPyTestObject, UPyTestObject, COMPILED_IN_FLAGS(0 | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_LegacyPyTestObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_LegacyPyTestObject(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_LegacyPyTestObject) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_LegacyPyTestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_LegacyPyTestObject); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_LegacyPyTestObject(UDEPRECATED_LegacyPyTestObject&&); \
	NO_API UDEPRECATED_LegacyPyTestObject(const UDEPRECATED_LegacyPyTestObject&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_LegacyPyTestObject() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_LegacyPyTestObject(UDEPRECATED_LegacyPyTestObject&&); \
	NO_API UDEPRECATED_LegacyPyTestObject(const UDEPRECATED_LegacyPyTestObject&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_LegacyPyTestObject); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_LegacyPyTestObject); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDEPRECATED_LegacyPyTestObject)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_245_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_248_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UDEPRECATED_LegacyPyTestObject>();

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetOtherConstantValue); \
	DECLARE_FUNCTION(execIsBoolSet);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetOtherConstantValue); \
	DECLARE_FUNCTION(execIsBoolSet);


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPyTestObjectLibrary(); \
	friend struct Z_Construct_UClass_UPyTestObjectLibrary_Statics; \
public: \
	DECLARE_CLASS(UPyTestObjectLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestObjectLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_INCLASS \
private: \
	static void StaticRegisterNativesUPyTestObjectLibrary(); \
	friend struct Z_Construct_UClass_UPyTestObjectLibrary_Statics; \
public: \
	DECLARE_CLASS(UPyTestObjectLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPyTestObjectLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyTestObjectLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyTestObjectLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestObjectLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestObjectLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestObjectLibrary(UPyTestObjectLibrary&&); \
	NO_API UPyTestObjectLibrary(const UPyTestObjectLibrary&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPyTestObjectLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPyTestObjectLibrary(UPyTestObjectLibrary&&); \
	NO_API UPyTestObjectLibrary(const UPyTestObjectLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPyTestObjectLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPyTestObjectLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPyTestObjectLibrary)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_254_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h_257_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPyTestObjectLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PyTest_h


#define FOREACH_ENUM_EPYTESTENUM(op) \
	op(EPyTestEnum::One) \
	op(EPyTestEnum::Two) 

enum class EPyTestEnum : uint8;
template<> PYTHONSCRIPTPLUGIN_API UEnum* StaticEnum<EPyTestEnum>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
