// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Private/PythonScriptCommandlet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePythonScriptCommandlet() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonScriptCommandlet_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonScriptCommandlet();
	ENGINE_API UClass* Z_Construct_UClass_UCommandlet();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
// End Cross Module References
	void UPythonScriptCommandlet::StaticRegisterNativesUPythonScriptCommandlet()
	{
	}
	UClass* Z_Construct_UClass_UPythonScriptCommandlet_NoRegister()
	{
		return UPythonScriptCommandlet::StaticClass();
	}
	struct Z_Construct_UClass_UPythonScriptCommandlet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPythonScriptCommandlet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommandlet,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptCommandlet_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Minimal commandlet to invoke a Python script and exit */" },
		{ "IncludePath", "PythonScriptCommandlet.h" },
		{ "ModuleRelativePath", "Private/PythonScriptCommandlet.h" },
		{ "ToolTip", "Minimal commandlet to invoke a Python script and exit" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPythonScriptCommandlet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPythonScriptCommandlet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPythonScriptCommandlet_Statics::ClassParams = {
		&UPythonScriptCommandlet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPythonScriptCommandlet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptCommandlet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPythonScriptCommandlet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPythonScriptCommandlet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPythonScriptCommandlet, 3745371074);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPythonScriptCommandlet>()
	{
		return UPythonScriptCommandlet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPythonScriptCommandlet(Z_Construct_UClass_UPythonScriptCommandlet, &UPythonScriptCommandlet::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPythonScriptCommandlet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPythonScriptCommandlet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
