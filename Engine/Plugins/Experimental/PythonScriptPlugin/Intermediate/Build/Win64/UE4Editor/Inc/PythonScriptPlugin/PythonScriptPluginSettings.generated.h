// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PYTHONSCRIPTPLUGIN_PythonScriptPluginSettings_generated_h
#error "PythonScriptPluginSettings.generated.h already included, missing '#pragma once' in PythonScriptPluginSettings.h"
#endif
#define PYTHONSCRIPTPLUGIN_PythonScriptPluginSettings_generated_h

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPythonScriptPluginSettings(); \
	friend struct Z_Construct_UClass_UPythonScriptPluginSettings_Statics; \
public: \
	DECLARE_CLASS(UPythonScriptPluginSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonScriptPluginSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUPythonScriptPluginSettings(); \
	friend struct Z_Construct_UClass_UPythonScriptPluginSettings_Statics; \
public: \
	DECLARE_CLASS(UPythonScriptPluginSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonScriptPluginSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Engine");} \



#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPythonScriptPluginSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonScriptPluginSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonScriptPluginSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonScriptPluginSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonScriptPluginSettings(UPythonScriptPluginSettings&&); \
	NO_API UPythonScriptPluginSettings(const UPythonScriptPluginSettings&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonScriptPluginSettings(UPythonScriptPluginSettings&&); \
	NO_API UPythonScriptPluginSettings(const UPythonScriptPluginSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonScriptPluginSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonScriptPluginSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPythonScriptPluginSettings)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_13_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPythonScriptPluginSettings>();

#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_SPARSE_DATA
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPythonScriptPluginUserSettings(); \
	friend struct Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics; \
public: \
	DECLARE_CLASS(UPythonScriptPluginUserSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonScriptPluginUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_INCLASS \
private: \
	static void StaticRegisterNativesUPythonScriptPluginUserSettings(); \
	friend struct Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics; \
public: \
	DECLARE_CLASS(UPythonScriptPluginUserSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PythonScriptPlugin"), NO_API) \
	DECLARE_SERIALIZER(UPythonScriptPluginUserSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPythonScriptPluginUserSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPythonScriptPluginUserSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonScriptPluginUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonScriptPluginUserSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonScriptPluginUserSettings(UPythonScriptPluginUserSettings&&); \
	NO_API UPythonScriptPluginUserSettings(const UPythonScriptPluginUserSettings&); \
public:


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPythonScriptPluginUserSettings(UPythonScriptPluginUserSettings&&); \
	NO_API UPythonScriptPluginUserSettings(const UPythonScriptPluginUserSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPythonScriptPluginUserSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPythonScriptPluginUserSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPythonScriptPluginUserSettings)


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_74_PROLOG
#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_INCLASS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_SPARSE_DATA \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h_77_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<class UPythonScriptPluginUserSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PythonScriptPlugin_Source_PythonScriptPlugin_Private_PythonScriptPluginSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
