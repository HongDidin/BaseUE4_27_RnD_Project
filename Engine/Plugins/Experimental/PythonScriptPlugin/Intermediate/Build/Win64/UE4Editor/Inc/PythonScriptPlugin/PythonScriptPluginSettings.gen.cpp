// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Private/PythonScriptPluginSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePythonScriptPluginSettings() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonScriptPluginSettings_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonScriptPluginSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonScriptPluginUserSettings_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonScriptPluginUserSettings();
// End Cross Module References
	void UPythonScriptPluginSettings::StaticRegisterNativesUPythonScriptPluginSettings()
	{
	}
	UClass* Z_Construct_UClass_UPythonScriptPluginSettings_NoRegister()
	{
		return UPythonScriptPluginSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPythonScriptPluginSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StartupScripts_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartupScripts_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_StartupScripts;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdditionalPaths_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalPaths_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AdditionalPaths;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDeveloperMode_MetaData[];
#endif
		static void NewProp_bDeveloperMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDeveloperMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoteExecution_MetaData[];
#endif
		static void NewProp_bRemoteExecution_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoteExecution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteExecutionMulticastGroupEndpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RemoteExecutionMulticastGroupEndpoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteExecutionMulticastBindAddress_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RemoteExecutionMulticastBindAddress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteExecutionSendBufferSizeBytes_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RemoteExecutionSendBufferSizeBytes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteExecutionReceiveBufferSizeBytes_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RemoteExecutionReceiveBufferSizeBytes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteExecutionMulticastTtl_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RemoteExecutionMulticastTtl;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPythonScriptPluginSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Configure the Python plug-in.\n */" },
		{ "IncludePath", "PythonScriptPluginSettings.h" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "Configure the Python plug-in." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_StartupScripts_Inner = { "StartupScripts", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_StartupScripts_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Array of Python scripts to run at start-up (run before the first Tick after the Engine has initialized). */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "MultiLine", "TRUE" },
		{ "ToolTip", "Array of Python scripts to run at start-up (run before the first Tick after the Engine has initialized)." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_StartupScripts = { "StartupScripts", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPythonScriptPluginSettings, StartupScripts), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_StartupScripts_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_StartupScripts_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_AdditionalPaths_Inner = { "AdditionalPaths", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_AdditionalPaths_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Array of additional paths to add to the Python system paths. */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "RelativePath", "" },
		{ "ToolTip", "Array of additional paths to add to the Python system paths." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_AdditionalPaths = { "AdditionalPaths", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPythonScriptPluginSettings, AdditionalPaths), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_AdditionalPaths_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_AdditionalPaths_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bDeveloperMode_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/**\n\x09 * Should Developer Mode be enabled on the Python interpreter *for all users of the project*\n\x09 * Note: Most of the time you want to enable bDeveloperMode in the Editor Preferences instead!\n\x09 *\n\x09 * (will also enable extra warnings (e.g., for deprecated code), and enable stub code generation for\n\x09 * use with external IDEs).\n\x09 */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "DisplayName", "Developer Mode (all users)" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "Should Developer Mode be enabled on the Python interpreter *for all users of the project*\nNote: Most of the time you want to enable bDeveloperMode in the Editor Preferences instead!\n\n(will also enable extra warnings (e.g., for deprecated code), and enable stub code generation for\nuse with external IDEs)." },
	};
#endif
	void Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bDeveloperMode_SetBit(void* Obj)
	{
		((UPythonScriptPluginSettings*)Obj)->bDeveloperMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bDeveloperMode = { "bDeveloperMode", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPythonScriptPluginSettings), &Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bDeveloperMode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bDeveloperMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bDeveloperMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bRemoteExecution_MetaData[] = {
		{ "Category", "PythonRemoteExecution" },
		{ "Comment", "/** Should remote Python execution be enabled? */" },
		{ "DisplayName", "Enable Remote Execution?" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "Should remote Python execution be enabled?" },
	};
#endif
	void Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bRemoteExecution_SetBit(void* Obj)
	{
		((UPythonScriptPluginSettings*)Obj)->bRemoteExecution = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bRemoteExecution = { "bRemoteExecution", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPythonScriptPluginSettings), &Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bRemoteExecution_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bRemoteExecution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bRemoteExecution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastGroupEndpoint_MetaData[] = {
		{ "Category", "PythonRemoteExecution" },
		{ "Comment", "/** The multicast group endpoint (in the form of IP_ADDRESS:PORT_NUMBER) that the UDP multicast socket should join */" },
		{ "DisplayName", "Multicast Group Endpoint" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "The multicast group endpoint (in the form of IP_ADDRESS:PORT_NUMBER) that the UDP multicast socket should join" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastGroupEndpoint = { "RemoteExecutionMulticastGroupEndpoint", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPythonScriptPluginSettings, RemoteExecutionMulticastGroupEndpoint), METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastGroupEndpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastGroupEndpoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastBindAddress_MetaData[] = {
		{ "Category", "PythonRemoteExecution" },
		{ "Comment", "/** The adapter address that the UDP multicast socket should bind to, or 0.0.0.0 to bind to all adapters */" },
		{ "DisplayName", "Multicast Bind Address" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "The adapter address that the UDP multicast socket should bind to, or 0.0.0.0 to bind to all adapters" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastBindAddress = { "RemoteExecutionMulticastBindAddress", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPythonScriptPluginSettings, RemoteExecutionMulticastBindAddress), METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastBindAddress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastBindAddress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionSendBufferSizeBytes_MetaData[] = {
		{ "Category", "PythonRemoteExecution" },
		{ "Comment", "/** Size of the send buffer for the remote endpoint connection */" },
		{ "DisplayName", "Send Buffer Size" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "Size of the send buffer for the remote endpoint connection" },
		{ "Units", "Bytes" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionSendBufferSizeBytes = { "RemoteExecutionSendBufferSizeBytes", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPythonScriptPluginSettings, RemoteExecutionSendBufferSizeBytes), METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionSendBufferSizeBytes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionSendBufferSizeBytes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionReceiveBufferSizeBytes_MetaData[] = {
		{ "Category", "PythonRemoteExecution" },
		{ "Comment", "/** Size of the receive buffer for the remote endpoint connection */" },
		{ "DisplayName", "Receive Buffer Size" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "Size of the receive buffer for the remote endpoint connection" },
		{ "Units", "Bytes" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionReceiveBufferSizeBytes = { "RemoteExecutionReceiveBufferSizeBytes", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPythonScriptPluginSettings, RemoteExecutionReceiveBufferSizeBytes), METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionReceiveBufferSizeBytes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionReceiveBufferSizeBytes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastTtl_MetaData[] = {
		{ "Category", "PythonRemoteExecution" },
		{ "Comment", "/** The TTL that the UDP multicast socket should use (0 is limited to the local host, 1 is limited to the local subnet) */" },
		{ "DisplayName", "Multicast Time-To-Live" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "The TTL that the UDP multicast socket should use (0 is limited to the local host, 1 is limited to the local subnet)" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastTtl = { "RemoteExecutionMulticastTtl", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPythonScriptPluginSettings, RemoteExecutionMulticastTtl), nullptr, METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastTtl_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastTtl_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPythonScriptPluginSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_StartupScripts_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_StartupScripts,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_AdditionalPaths_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_AdditionalPaths,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bDeveloperMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_bRemoteExecution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastGroupEndpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastBindAddress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionSendBufferSizeBytes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionReceiveBufferSizeBytes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginSettings_Statics::NewProp_RemoteExecutionMulticastTtl,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPythonScriptPluginSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPythonScriptPluginSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPythonScriptPluginSettings_Statics::ClassParams = {
		&UPythonScriptPluginSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPythonScriptPluginSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::PropPointers),
		0,
		0x000000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPythonScriptPluginSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPythonScriptPluginSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPythonScriptPluginSettings, 3306887853);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPythonScriptPluginSettings>()
	{
		return UPythonScriptPluginSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPythonScriptPluginSettings(Z_Construct_UClass_UPythonScriptPluginSettings, &UPythonScriptPluginSettings::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPythonScriptPluginSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPythonScriptPluginSettings);
	void UPythonScriptPluginUserSettings::StaticRegisterNativesUPythonScriptPluginUserSettings()
	{
	}
	UClass* Z_Construct_UClass_UPythonScriptPluginUserSettings_NoRegister()
	{
		return UPythonScriptPluginUserSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDeveloperMode_MetaData[];
#endif
		static void NewProp_bDeveloperMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDeveloperMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableContentBrowserIntegration_MetaData[];
#endif
		static void NewProp_bEnableContentBrowserIntegration_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableContentBrowserIntegration;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PythonScriptPluginSettings.h" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bDeveloperMode_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/**\n\x09 * Should Developer Mode be enabled on the Python interpreter?\n\x09 *\n\x09 * (will also enable extra warnings (e.g., for deprecated code), and enable stub code generation for\n\x09 * use with external IDEs).\n\x09 */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "Should Developer Mode be enabled on the Python interpreter?\n\n(will also enable extra warnings (e.g., for deprecated code), and enable stub code generation for\nuse with external IDEs)." },
	};
#endif
	void Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bDeveloperMode_SetBit(void* Obj)
	{
		((UPythonScriptPluginUserSettings*)Obj)->bDeveloperMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bDeveloperMode = { "bDeveloperMode", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPythonScriptPluginUserSettings), &Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bDeveloperMode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bDeveloperMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bDeveloperMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bEnableContentBrowserIntegration_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Should Python scripts be available in the Content Browser? */" },
		{ "ConfigRestartRequired", "TRUE" },
		{ "ModuleRelativePath", "Private/PythonScriptPluginSettings.h" },
		{ "ToolTip", "Should Python scripts be available in the Content Browser?" },
	};
#endif
	void Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bEnableContentBrowserIntegration_SetBit(void* Obj)
	{
		((UPythonScriptPluginUserSettings*)Obj)->bEnableContentBrowserIntegration = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bEnableContentBrowserIntegration = { "bEnableContentBrowserIntegration", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPythonScriptPluginUserSettings), &Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bEnableContentBrowserIntegration_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bEnableContentBrowserIntegration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bEnableContentBrowserIntegration_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bDeveloperMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::NewProp_bEnableContentBrowserIntegration,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPythonScriptPluginUserSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::ClassParams = {
		&UPythonScriptPluginUserSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPythonScriptPluginUserSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPythonScriptPluginUserSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPythonScriptPluginUserSettings, 472708527);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPythonScriptPluginUserSettings>()
	{
		return UPythonScriptPluginUserSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPythonScriptPluginUserSettings(Z_Construct_UClass_UPythonScriptPluginUserSettings, &UPythonScriptPluginUserSettings::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPythonScriptPluginUserSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPythonScriptPluginUserSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
