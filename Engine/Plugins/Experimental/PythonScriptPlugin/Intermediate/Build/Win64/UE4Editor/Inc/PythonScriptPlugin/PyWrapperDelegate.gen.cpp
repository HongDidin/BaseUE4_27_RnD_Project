// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PythonScriptPlugin/Private/PyWrapperDelegate.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePyWrapperDelegate() {}
// Cross Module References
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonCallableForDelegate_NoRegister();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonCallableForDelegate();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin();
	PYTHONSCRIPTPLUGIN_API UClass* Z_Construct_UClass_UPythonResourceOwner_NoRegister();
// End Cross Module References
	void UPythonCallableForDelegate::StaticRegisterNativesUPythonCallableForDelegate()
	{
	}
	UClass* Z_Construct_UClass_UPythonCallableForDelegate_NoRegister()
	{
		return UPythonCallableForDelegate::StaticClass();
	}
	struct Z_Construct_UClass_UPythonCallableForDelegate_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPythonCallableForDelegate_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PythonScriptPlugin,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPythonCallableForDelegate_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UObject proxy base used to wrap a callable Python object so that it can be used with an Unreal delegate\n * @note This can't go inside the WITH_PYTHON block due to UHT parsing limitations (it doesn't understand that macro)\n */" },
		{ "IncludePath", "PyWrapperDelegate.h" },
		{ "ModuleRelativePath", "Private/PyWrapperDelegate.h" },
		{ "ToolTip", "UObject proxy base used to wrap a callable Python object so that it can be used with an Unreal delegate\n@note This can't go inside the WITH_PYTHON block due to UHT parsing limitations (it doesn't understand that macro)" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UPythonCallableForDelegate_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UPythonResourceOwner_NoRegister, (int32)VTABLE_OFFSET(UPythonCallableForDelegate, IPythonResourceOwner), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPythonCallableForDelegate_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPythonCallableForDelegate>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPythonCallableForDelegate_Statics::ClassParams = {
		&UPythonCallableForDelegate::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x040000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPythonCallableForDelegate_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPythonCallableForDelegate_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPythonCallableForDelegate()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPythonCallableForDelegate_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPythonCallableForDelegate, 1588066951);
	template<> PYTHONSCRIPTPLUGIN_API UClass* StaticClass<UPythonCallableForDelegate>()
	{
		return UPythonCallableForDelegate::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPythonCallableForDelegate(Z_Construct_UClass_UPythonCallableForDelegate, &UPythonCallableForDelegate::StaticClass, TEXT("/Script/PythonScriptPlugin"), TEXT("UPythonCallableForDelegate"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPythonCallableForDelegate);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
