// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePythonScriptPlugin_init() {}
	PYTHONSCRIPTPLUGIN_API UFunction* Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature();
	PYTHONSCRIPTPLUGIN_API UFunction* Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_PythonScriptPlugin()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestDelegate__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_PythonScriptPlugin_PyTestMulticastDelegate__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/PythonScriptPlugin",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000100,
				0x000721FC,
				0x9E828B76,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
