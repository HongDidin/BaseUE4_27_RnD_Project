// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef FIELDSYSTEMEDITOR_ActorFactoryFieldSystem_generated_h
#error "ActorFactoryFieldSystem.generated.h already included, missing '#pragma once' in ActorFactoryFieldSystem.h"
#endif
#define FIELDSYSTEMEDITOR_ActorFactoryFieldSystem_generated_h

#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUActorFactoryFieldSystem(); \
	friend struct Z_Construct_UClass_UActorFactoryFieldSystem_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryFieldSystem, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FieldSystemEditor"), FIELDSYSTEMEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryFieldSystem)


#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUActorFactoryFieldSystem(); \
	friend struct Z_Construct_UClass_UActorFactoryFieldSystem_Statics; \
public: \
	DECLARE_CLASS(UActorFactoryFieldSystem, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/FieldSystemEditor"), FIELDSYSTEMEDITOR_API) \
	DECLARE_SERIALIZER(UActorFactoryFieldSystem)


#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FIELDSYSTEMEDITOR_API UActorFactoryFieldSystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryFieldSystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIELDSYSTEMEDITOR_API, UActorFactoryFieldSystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryFieldSystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIELDSYSTEMEDITOR_API UActorFactoryFieldSystem(UActorFactoryFieldSystem&&); \
	FIELDSYSTEMEDITOR_API UActorFactoryFieldSystem(const UActorFactoryFieldSystem&); \
public:


#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	FIELDSYSTEMEDITOR_API UActorFactoryFieldSystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	FIELDSYSTEMEDITOR_API UActorFactoryFieldSystem(UActorFactoryFieldSystem&&); \
	FIELDSYSTEMEDITOR_API UActorFactoryFieldSystem(const UActorFactoryFieldSystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(FIELDSYSTEMEDITOR_API, UActorFactoryFieldSystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UActorFactoryFieldSystem); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UActorFactoryFieldSystem)


#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_14_PROLOG
#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_INCLASS \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class ActorFactoryFieldSystem."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> FIELDSYSTEMEDITOR_API UClass* StaticClass<class UActorFactoryFieldSystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_FieldSystemPlugin_Source_FieldSyStemEditor_Private_Field_ActorFactoryFieldSystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
