// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGTOOLSEDITORMODE_ModelingToolsEditorModeSettings_generated_h
#error "ModelingToolsEditorModeSettings.generated.h already included, missing '#pragma once' in ModelingToolsEditorModeSettings.h"
#endif
#define MODELINGTOOLSEDITORMODE_ModelingToolsEditorModeSettings_generated_h

#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_SPARSE_DATA
#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_RPC_WRAPPERS
#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUModelingToolsEditorModeSettings(); \
	friend struct Z_Construct_UClass_UModelingToolsEditorModeSettings_Statics; \
public: \
	DECLARE_CLASS(UModelingToolsEditorModeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingToolsEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UModelingToolsEditorModeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_INCLASS \
private: \
	static void StaticRegisterNativesUModelingToolsEditorModeSettings(); \
	friend struct Z_Construct_UClass_UModelingToolsEditorModeSettings_Statics; \
public: \
	DECLARE_CLASS(UModelingToolsEditorModeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingToolsEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UModelingToolsEditorModeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UModelingToolsEditorModeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UModelingToolsEditorModeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UModelingToolsEditorModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UModelingToolsEditorModeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UModelingToolsEditorModeSettings(UModelingToolsEditorModeSettings&&); \
	NO_API UModelingToolsEditorModeSettings(const UModelingToolsEditorModeSettings&); \
public:


#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UModelingToolsEditorModeSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UModelingToolsEditorModeSettings(UModelingToolsEditorModeSettings&&); \
	NO_API UModelingToolsEditorModeSettings(const UModelingToolsEditorModeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UModelingToolsEditorModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UModelingToolsEditorModeSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UModelingToolsEditorModeSettings)


#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_42_PROLOG
#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_SPARSE_DATA \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_RPC_WRAPPERS \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_INCLASS \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_SPARSE_DATA \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h_46_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGTOOLSEDITORMODE_API UClass* StaticClass<class UModelingToolsEditorModeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_ModelingToolsEditorMode_Source_ModelingToolsEditorMode_Public_ModelingToolsEditorModeSettings_h


#define FOREACH_ENUM_EMODELINGMODEASSETGENERATIONLOCATION(op) \
	op(EModelingModeAssetGenerationLocation::AutoGeneratedWorldRelativeAssetPath) \
	op(EModelingModeAssetGenerationLocation::AutoGeneratedGlobalAssetPath) \
	op(EModelingModeAssetGenerationLocation::CurrentAssetBrowserPathIfAvailable) 

enum class EModelingModeAssetGenerationLocation;
template<> MODELINGTOOLSEDITORMODE_API UEnum* StaticEnum<EModelingModeAssetGenerationLocation>();

#define FOREACH_ENUM_EMODELINGMODEASSETGENERATIONBEHAVIOR(op) \
	op(EModelingModeAssetGenerationBehavior::AutoGenerateAndAutosave) \
	op(EModelingModeAssetGenerationBehavior::AutoGenerateButDoNotAutosave) \
	op(EModelingModeAssetGenerationBehavior::InteractivePromptToSave) 

enum class EModelingModeAssetGenerationBehavior;
template<> MODELINGTOOLSEDITORMODE_API UEnum* StaticEnum<EModelingModeAssetGenerationBehavior>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
