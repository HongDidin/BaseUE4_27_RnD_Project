// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ActorPalette/Private/ActorPaletteSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorPaletteSettings() {}
// Cross Module References
	ACTORPALETTE_API UScriptStruct* Z_Construct_UScriptStruct_FActorPaletteMapEntry();
	UPackage* Z_Construct_UPackage__Script_ActorPalette();
	ACTORPALETTE_API UClass* Z_Construct_UClass_UActorPaletteSettings_NoRegister();
	ACTORPALETTE_API UClass* Z_Construct_UClass_UActorPaletteSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
// End Cross Module References
class UScriptStruct* FActorPaletteMapEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern ACTORPALETTE_API uint32 Get_Z_Construct_UScriptStruct_FActorPaletteMapEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FActorPaletteMapEntry, Z_Construct_UPackage__Script_ActorPalette(), TEXT("ActorPaletteMapEntry"), sizeof(FActorPaletteMapEntry), Get_Z_Construct_UScriptStruct_FActorPaletteMapEntry_Hash());
	}
	return Singleton;
}
template<> ACTORPALETTE_API UScriptStruct* StaticStruct<FActorPaletteMapEntry>()
{
	return FActorPaletteMapEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FActorPaletteMapEntry(FActorPaletteMapEntry::StaticStruct, TEXT("/Script/ActorPalette"), TEXT("ActorPaletteMapEntry"), false, nullptr, nullptr);
static struct FScriptStruct_ActorPalette_StaticRegisterNativesFActorPaletteMapEntry
{
	FScriptStruct_ActorPalette_StaticRegisterNativesFActorPaletteMapEntry()
	{
		UScriptStruct::DeferCppStructOps<FActorPaletteMapEntry>(FName(TEXT("ActorPaletteMapEntry")));
	}
} ScriptStruct_ActorPalette_StaticRegisterNativesFActorPaletteMapEntry;
	struct Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MapPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MapPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// Information about a single recent/favorite map\n" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
		{ "ToolTip", "Information about a single recent/favorite map" },
	};
#endif
	void* Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FActorPaletteMapEntry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::NewProp_MapPath_MetaData[] = {
		{ "Category", "ActorPalette" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::NewProp_MapPath = { "MapPath", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FActorPaletteMapEntry, MapPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::NewProp_MapPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::NewProp_MapPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::NewProp_MapPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_ActorPalette,
		nullptr,
		&NewStructOps,
		"ActorPaletteMapEntry",
		sizeof(FActorPaletteMapEntry),
		alignof(FActorPaletteMapEntry),
		Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FActorPaletteMapEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FActorPaletteMapEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_ActorPalette();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ActorPaletteMapEntry"), sizeof(FActorPaletteMapEntry), Get_Z_Construct_UScriptStruct_FActorPaletteMapEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FActorPaletteMapEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FActorPaletteMapEntry_Hash() { return 2364634363U; }
	void UActorPaletteSettings::StaticRegisterNativesUActorPaletteSettings()
	{
	}
	UClass* Z_Construct_UClass_UActorPaletteSettings_NoRegister()
	{
		return UActorPaletteSettings::StaticClass();
	}
	struct Z_Construct_UClass_UActorPaletteSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SettingsPerLevel_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SettingsPerLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SettingsPerLevel;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RecentlyUsedList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RecentlyUsedList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_RecentlyUsedList;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MostRecentLevelByTab_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MostRecentLevelByTab_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MostRecentLevelByTab;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FavoritesList_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FavoritesList_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FavoritesList;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumRecentLevelsToKeep_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumRecentLevelsToKeep;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorPaletteSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_ActorPalette,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorPaletteSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Settings/preferences for Actor Palettes\n" },
		{ "IncludePath", "ActorPaletteSettings.h" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
		{ "ToolTip", "Settings/preferences for Actor Palettes" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_SettingsPerLevel_Inner = { "SettingsPerLevel", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FActorPaletteMapEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_SettingsPerLevel_MetaData[] = {
		{ "Comment", "// Remembered settings for any recent/current/favorite actor palette maps\n" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
		{ "ToolTip", "Remembered settings for any recent/current/favorite actor palette maps" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_SettingsPerLevel = { "SettingsPerLevel", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorPaletteSettings, SettingsPerLevel), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_SettingsPerLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_SettingsPerLevel_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_RecentlyUsedList_Inner = { "RecentlyUsedList", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_RecentlyUsedList_MetaData[] = {
		{ "Comment", "// List of levels that were recently open in any Actor Palette tab\n" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
		{ "ToolTip", "List of levels that were recently open in any Actor Palette tab" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_RecentlyUsedList = { "RecentlyUsedList", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorPaletteSettings, RecentlyUsedList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_RecentlyUsedList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_RecentlyUsedList_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_MostRecentLevelByTab_Inner = { "MostRecentLevelByTab", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_MostRecentLevelByTab_MetaData[] = {
		{ "Comment", "// List of levels that were last open in each Actor Palette tab (indexed by tab index)\n" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
		{ "ToolTip", "List of levels that were last open in each Actor Palette tab (indexed by tab index)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_MostRecentLevelByTab = { "MostRecentLevelByTab", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorPaletteSettings, MostRecentLevelByTab), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_MostRecentLevelByTab_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_MostRecentLevelByTab_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_FavoritesList_Inner = { "FavoritesList", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_FavoritesList_MetaData[] = {
		{ "Comment", "// List of levels that were marked as favorite actor palettes\n" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
		{ "ToolTip", "List of levels that were marked as favorite actor palettes" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_FavoritesList = { "FavoritesList", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorPaletteSettings, FavoritesList), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_FavoritesList_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_FavoritesList_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_NumRecentLevelsToKeep_MetaData[] = {
		{ "Category", "ActorPalette" },
		{ "ClampMax", "25" },
		{ "ClampMin", "0" },
		{ "Comment", "// How many recent levels will be remembered?\n" },
		{ "ModuleRelativePath", "Private/ActorPaletteSettings.h" },
		{ "ToolTip", "How many recent levels will be remembered?" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_NumRecentLevelsToKeep = { "NumRecentLevelsToKeep", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UActorPaletteSettings, NumRecentLevelsToKeep), METADATA_PARAMS(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_NumRecentLevelsToKeep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_NumRecentLevelsToKeep_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UActorPaletteSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_SettingsPerLevel_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_SettingsPerLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_RecentlyUsedList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_RecentlyUsedList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_MostRecentLevelByTab_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_MostRecentLevelByTab,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_FavoritesList_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_FavoritesList,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UActorPaletteSettings_Statics::NewProp_NumRecentLevelsToKeep,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorPaletteSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorPaletteSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorPaletteSettings_Statics::ClassParams = {
		&UActorPaletteSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UActorPaletteSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UActorPaletteSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UActorPaletteSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorPaletteSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorPaletteSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorPaletteSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorPaletteSettings, 2839728895);
	template<> ACTORPALETTE_API UClass* StaticClass<UActorPaletteSettings>()
	{
		return UActorPaletteSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorPaletteSettings(Z_Construct_UClass_UActorPaletteSettings, &UActorPaletteSettings::StaticClass, TEXT("/Script/ActorPalette"), TEXT("UActorPaletteSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorPaletteSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
