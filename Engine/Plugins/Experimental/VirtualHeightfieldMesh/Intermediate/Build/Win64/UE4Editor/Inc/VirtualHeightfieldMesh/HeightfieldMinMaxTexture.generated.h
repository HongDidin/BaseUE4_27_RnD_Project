// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIRTUALHEIGHTFIELDMESH_HeightfieldMinMaxTexture_generated_h
#error "HeightfieldMinMaxTexture.generated.h already included, missing '#pragma once' in HeightfieldMinMaxTexture.h"
#endif
#define VIRTUALHEIGHTFIELDMESH_HeightfieldMinMaxTexture_generated_h

#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHeightfieldMinMaxTexture(); \
	friend struct Z_Construct_UClass_UHeightfieldMinMaxTexture_Statics; \
public: \
	DECLARE_CLASS(UHeightfieldMinMaxTexture, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualHeightfieldMesh"), NO_API) \
	DECLARE_SERIALIZER(UHeightfieldMinMaxTexture)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUHeightfieldMinMaxTexture(); \
	friend struct Z_Construct_UClass_UHeightfieldMinMaxTexture_Statics; \
public: \
	DECLARE_CLASS(UHeightfieldMinMaxTexture, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualHeightfieldMesh"), NO_API) \
	DECLARE_SERIALIZER(UHeightfieldMinMaxTexture)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHeightfieldMinMaxTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHeightfieldMinMaxTexture) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHeightfieldMinMaxTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHeightfieldMinMaxTexture); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHeightfieldMinMaxTexture(UHeightfieldMinMaxTexture&&); \
	NO_API UHeightfieldMinMaxTexture(const UHeightfieldMinMaxTexture&); \
public:


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHeightfieldMinMaxTexture(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHeightfieldMinMaxTexture(UHeightfieldMinMaxTexture&&); \
	NO_API UHeightfieldMinMaxTexture(const UHeightfieldMinMaxTexture&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHeightfieldMinMaxTexture); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHeightfieldMinMaxTexture); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHeightfieldMinMaxTexture)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MaxCPULevels() { return STRUCT_OFFSET(UHeightfieldMinMaxTexture, MaxCPULevels); }


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_25_PROLOG
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_INCLASS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class HeightfieldMinMaxTexture."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALHEIGHTFIELDMESH_API UClass* StaticClass<class UHeightfieldMinMaxTexture>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_HeightfieldMinMaxTexture_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
