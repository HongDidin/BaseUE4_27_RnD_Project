// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIRTUALHEIGHTFIELDMESH_VirtualHeightfieldMeshComponent_generated_h
#error "VirtualHeightfieldMeshComponent.generated.h already included, missing '#pragma once' in VirtualHeightfieldMeshComponent.h"
#endif
#define VIRTUALHEIGHTFIELDMESH_VirtualHeightfieldMeshComponent_generated_h

#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGatherHideFlags);


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGatherHideFlags);


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVirtualHeightfieldMeshComponent(); \
	friend struct Z_Construct_UClass_UVirtualHeightfieldMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UVirtualHeightfieldMeshComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualHeightfieldMesh"), NO_API) \
	DECLARE_SERIALIZER(UVirtualHeightfieldMeshComponent)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUVirtualHeightfieldMeshComponent(); \
	friend struct Z_Construct_UClass_UVirtualHeightfieldMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UVirtualHeightfieldMeshComponent, UPrimitiveComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualHeightfieldMesh"), NO_API) \
	DECLARE_SERIALIZER(UVirtualHeightfieldMeshComponent)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualHeightfieldMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualHeightfieldMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualHeightfieldMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualHeightfieldMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualHeightfieldMeshComponent(UVirtualHeightfieldMeshComponent&&); \
	NO_API UVirtualHeightfieldMeshComponent(const UVirtualHeightfieldMeshComponent&); \
public:


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualHeightfieldMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualHeightfieldMeshComponent(UVirtualHeightfieldMeshComponent&&); \
	NO_API UVirtualHeightfieldMeshComponent(const UVirtualHeightfieldMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualHeightfieldMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualHeightfieldMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualHeightfieldMeshComponent)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VirtualTexture() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, VirtualTexture); } \
	FORCEINLINE static uint32 __PPO__VirtualTextureRef() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, VirtualTextureRef); } \
	FORCEINLINE static uint32 __PPO__VirtualTextureThumbnail() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, VirtualTextureThumbnail); } \
	FORCEINLINE static uint32 __PPO__bCopyBoundsButton() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, bCopyBoundsButton); } \
	FORCEINLINE static uint32 __PPO__MinMaxTexture() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, MinMaxTexture); } \
	FORCEINLINE static uint32 __PPO__NumMinMaxTextureBuildLevels() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, NumMinMaxTextureBuildLevels); } \
	FORCEINLINE static uint32 __PPO__bBuildMinMaxTextureButton() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, bBuildMinMaxTextureButton); } \
	FORCEINLINE static uint32 __PPO__Material() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, Material); } \
	FORCEINLINE static uint32 __PPO__Lod0ScreenSize() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, Lod0ScreenSize); } \
	FORCEINLINE static uint32 __PPO__Lod0Distribution() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, Lod0Distribution); } \
	FORCEINLINE static uint32 __PPO__LodDistribution() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, LodDistribution); } \
	FORCEINLINE static uint32 __PPO__LodBiasScale() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, LodBiasScale); } \
	FORCEINLINE static uint32 __PPO__NumForceLoadLods() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, NumForceLoadLods); } \
	FORCEINLINE static uint32 __PPO__NumOcclusionLods() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, NumOcclusionLods); } \
	FORCEINLINE static uint32 __PPO__bHiddenInEditor() { return STRUCT_OFFSET(UVirtualHeightfieldMeshComponent, bHiddenInEditor); }


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_15_PROLOG
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_INCLASS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VirtualHeightfieldMeshComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALHEIGHTFIELDMESH_API UClass* StaticClass<class UVirtualHeightfieldMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
