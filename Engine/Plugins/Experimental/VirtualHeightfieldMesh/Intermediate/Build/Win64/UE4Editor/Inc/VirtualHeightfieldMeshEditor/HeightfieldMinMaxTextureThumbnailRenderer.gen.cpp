// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualHeightfieldMeshEditor/Private/HeightfieldMinMaxTextureThumbnailRenderer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHeightfieldMinMaxTextureThumbnailRenderer() {}
// Cross Module References
	VIRTUALHEIGHTFIELDMESHEDITOR_API UClass* Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_NoRegister();
	VIRTUALHEIGHTFIELDMESHEDITOR_API UClass* Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer();
	UNREALED_API UClass* Z_Construct_UClass_UTextureThumbnailRenderer();
	UPackage* Z_Construct_UPackage__Script_VirtualHeightfieldMeshEditor();
// End Cross Module References
	void UHeightfieldMinMaxTextureThumbnailRenderer::StaticRegisterNativesUHeightfieldMinMaxTextureThumbnailRenderer()
	{
	}
	UClass* Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_NoRegister()
	{
		return UHeightfieldMinMaxTextureThumbnailRenderer::StaticClass();
	}
	struct Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTextureThumbnailRenderer,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualHeightfieldMeshEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "HeightfieldMinMaxTextureThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Private/HeightfieldMinMaxTextureThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHeightfieldMinMaxTextureThumbnailRenderer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics::ClassParams = {
		&UHeightfieldMinMaxTextureThumbnailRenderer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHeightfieldMinMaxTextureThumbnailRenderer, 600748622);
	template<> VIRTUALHEIGHTFIELDMESHEDITOR_API UClass* StaticClass<UHeightfieldMinMaxTextureThumbnailRenderer>()
	{
		return UHeightfieldMinMaxTextureThumbnailRenderer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHeightfieldMinMaxTextureThumbnailRenderer(Z_Construct_UClass_UHeightfieldMinMaxTextureThumbnailRenderer, &UHeightfieldMinMaxTextureThumbnailRenderer::StaticClass, TEXT("/Script/VirtualHeightfieldMeshEditor"), TEXT("UHeightfieldMinMaxTextureThumbnailRenderer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHeightfieldMinMaxTextureThumbnailRenderer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
