// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualHeightfieldMesh/Public/VirtualHeightfieldMeshActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVirtualHeightfieldMeshActor() {}
// Cross Module References
	VIRTUALHEIGHTFIELDMESH_API UClass* Z_Construct_UClass_AVirtualHeightfieldMesh_NoRegister();
	VIRTUALHEIGHTFIELDMESH_API UClass* Z_Construct_UClass_AVirtualHeightfieldMesh();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_VirtualHeightfieldMesh();
	VIRTUALHEIGHTFIELDMESH_API UClass* Z_Construct_UClass_UVirtualHeightfieldMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
// End Cross Module References
	void AVirtualHeightfieldMesh::StaticRegisterNativesAVirtualHeightfieldMesh()
	{
	}
	UClass* Z_Construct_UClass_AVirtualHeightfieldMesh_NoRegister()
	{
		return AVirtualHeightfieldMesh::StaticClass();
	}
	struct Z_Construct_UClass_AVirtualHeightfieldMesh_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VirtualHeightfieldMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VirtualHeightfieldMeshComponent;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Box_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Box;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualHeightfieldMesh,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Actor Collision Cooking Input LOD Replication" },
		{ "IncludePath", "VirtualHeightfieldMeshActor.h" },
		{ "ModuleRelativePath", "Public/VirtualHeightfieldMeshActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_VirtualHeightfieldMeshComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "VirtualTexture" },
		{ "Comment", "/** Component for rendering the big mesh. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VirtualHeightfieldMeshActor.h" },
		{ "ToolTip", "Component for rendering the big mesh." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_VirtualHeightfieldMeshComponent = { "VirtualHeightfieldMeshComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualHeightfieldMesh, VirtualHeightfieldMeshComponent), Z_Construct_UClass_UVirtualHeightfieldMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_VirtualHeightfieldMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_VirtualHeightfieldMeshComponent_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_Box_MetaData[] = {
		{ "Comment", "/** Box for visualizing virtual texture extents. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VirtualHeightfieldMeshActor.h" },
		{ "ToolTip", "Box for visualizing virtual texture extents." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_Box = { "Box", nullptr, (EPropertyFlags)0x0040000800082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualHeightfieldMesh, Box), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_Box_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_Box_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_VirtualHeightfieldMeshComponent,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::NewProp_Box,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVirtualHeightfieldMesh>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::ClassParams = {
		&AVirtualHeightfieldMesh::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::PropPointers),
		0,
		0x008800A4u,
		METADATA_PARAMS(Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVirtualHeightfieldMesh()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVirtualHeightfieldMesh_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVirtualHeightfieldMesh, 1305701657);
	template<> VIRTUALHEIGHTFIELDMESH_API UClass* StaticClass<AVirtualHeightfieldMesh>()
	{
		return AVirtualHeightfieldMesh::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVirtualHeightfieldMesh(Z_Construct_UClass_AVirtualHeightfieldMesh, &AVirtualHeightfieldMesh::StaticClass, TEXT("/Script/VirtualHeightfieldMesh"), TEXT("AVirtualHeightfieldMesh"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVirtualHeightfieldMesh);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
