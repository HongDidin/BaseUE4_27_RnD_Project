// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIRTUALHEIGHTFIELDMESH_VirtualHeightfieldMeshActor_generated_h
#error "VirtualHeightfieldMeshActor.generated.h already included, missing '#pragma once' in VirtualHeightfieldMeshActor.h"
#endif
#define VIRTUALHEIGHTFIELDMESH_VirtualHeightfieldMeshActor_generated_h

#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVirtualHeightfieldMesh(); \
	friend struct Z_Construct_UClass_AVirtualHeightfieldMesh_Statics; \
public: \
	DECLARE_CLASS(AVirtualHeightfieldMesh, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualHeightfieldMesh"), VIRTUALHEIGHTFIELDMESH_API) \
	DECLARE_SERIALIZER(AVirtualHeightfieldMesh)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAVirtualHeightfieldMesh(); \
	friend struct Z_Construct_UClass_AVirtualHeightfieldMesh_Statics; \
public: \
	DECLARE_CLASS(AVirtualHeightfieldMesh, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualHeightfieldMesh"), VIRTUALHEIGHTFIELDMESH_API) \
	DECLARE_SERIALIZER(AVirtualHeightfieldMesh)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VIRTUALHEIGHTFIELDMESH_API AVirtualHeightfieldMesh(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVirtualHeightfieldMesh) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VIRTUALHEIGHTFIELDMESH_API, AVirtualHeightfieldMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVirtualHeightfieldMesh); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VIRTUALHEIGHTFIELDMESH_API AVirtualHeightfieldMesh(AVirtualHeightfieldMesh&&); \
	VIRTUALHEIGHTFIELDMESH_API AVirtualHeightfieldMesh(const AVirtualHeightfieldMesh&); \
public:


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VIRTUALHEIGHTFIELDMESH_API AVirtualHeightfieldMesh(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VIRTUALHEIGHTFIELDMESH_API AVirtualHeightfieldMesh(AVirtualHeightfieldMesh&&); \
	VIRTUALHEIGHTFIELDMESH_API AVirtualHeightfieldMesh(const AVirtualHeightfieldMesh&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VIRTUALHEIGHTFIELDMESH_API, AVirtualHeightfieldMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVirtualHeightfieldMesh); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVirtualHeightfieldMesh)


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VirtualHeightfieldMeshComponent() { return STRUCT_OFFSET(AVirtualHeightfieldMesh, VirtualHeightfieldMeshComponent); }


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_9_PROLOG
#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_INCLASS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h_12_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VirtualHeightfieldMesh."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALHEIGHTFIELDMESH_API UClass* StaticClass<class AVirtualHeightfieldMesh>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualHeightfieldMesh_Source_VirtualHeightfieldMesh_Public_VirtualHeightfieldMeshActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
