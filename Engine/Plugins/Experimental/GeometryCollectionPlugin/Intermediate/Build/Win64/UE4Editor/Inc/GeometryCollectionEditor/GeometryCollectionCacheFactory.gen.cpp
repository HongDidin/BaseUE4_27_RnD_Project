// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCollectionEditor/Public/GeometryCollection/GeometryCollectionCacheFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCollectionCacheFactory() {}
// Cross Module References
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UGeometryCollectionCacheFactory_NoRegister();
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UGeometryCollectionCacheFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_GeometryCollectionEditor();
	GEOMETRYCOLLECTIONENGINE_API UClass* Z_Construct_UClass_UGeometryCollection_NoRegister();
// End Cross Module References
	void UGeometryCollectionCacheFactory::StaticRegisterNativesUGeometryCollectionCacheFactory()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCollectionCacheFactory_NoRegister()
	{
		return UGeometryCollectionCacheFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetCollection;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCollectionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::Class_MetaDataParams[] = {
		{ "DevelopmentStatus", "Experimental" },
		{ "IncludePath", "GeometryCollection/GeometryCollectionCacheFactory.h" },
		{ "ModuleRelativePath", "Public/GeometryCollection/GeometryCollectionCacheFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::NewProp_TargetCollection_MetaData[] = {
		{ "Comment", "/** Config properties required for CreateNew */" },
		{ "ModuleRelativePath", "Public/GeometryCollection/GeometryCollectionCacheFactory.h" },
		{ "ToolTip", "Config properties required for CreateNew" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::NewProp_TargetCollection = { "TargetCollection", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGeometryCollectionCacheFactory, TargetCollection), Z_Construct_UClass_UGeometryCollection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::NewProp_TargetCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::NewProp_TargetCollection_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::NewProp_TargetCollection,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCollectionCacheFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::ClassParams = {
		&UGeometryCollectionCacheFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCollectionCacheFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCollectionCacheFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCollectionCacheFactory, 966603427);
	template<> GEOMETRYCOLLECTIONEDITOR_API UClass* StaticClass<UGeometryCollectionCacheFactory>()
	{
		return UGeometryCollectionCacheFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCollectionCacheFactory(Z_Construct_UClass_UGeometryCollectionCacheFactory, &UGeometryCollectionCacheFactory::StaticClass, TEXT("/Script/GeometryCollectionEditor"), TEXT("UGeometryCollectionCacheFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCollectionCacheFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
