// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCollectionEditor/Public/GeometryCollection/GeometryCollectionFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCollectionFactory() {}
// Cross Module References
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UGeometryCollectionFactory_NoRegister();
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UGeometryCollectionFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_GeometryCollectionEditor();
// End Cross Module References
	void UGeometryCollectionFactory::StaticRegisterNativesUGeometryCollectionFactory()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCollectionFactory_NoRegister()
	{
		return UGeometryCollectionFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCollectionFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCollectionFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCollectionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCollectionFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n* Factory for Simple Cube\n*/" },
		{ "IncludePath", "GeometryCollection/GeometryCollectionFactory.h" },
		{ "ModuleRelativePath", "Public/GeometryCollection/GeometryCollectionFactory.h" },
		{ "ToolTip", "Factory for Simple Cube" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCollectionFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCollectionFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCollectionFactory_Statics::ClassParams = {
		&UGeometryCollectionFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCollectionFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCollectionFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCollectionFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCollectionFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCollectionFactory, 3932155605);
	template<> GEOMETRYCOLLECTIONEDITOR_API UClass* StaticClass<UGeometryCollectionFactory>()
	{
		return UGeometryCollectionFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCollectionFactory(Z_Construct_UClass_UGeometryCollectionFactory, &UGeometryCollectionFactory::StaticClass, TEXT("/Script/GeometryCollectionEditor"), TEXT("UGeometryCollectionFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCollectionFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
