// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCollectionEditor/Public/GeometryCollection/GeometryCollectionThumbnailRenderer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCollectionThumbnailRenderer() {}
// Cross Module References
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_NoRegister();
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UGeometryCollectionThumbnailRenderer();
	UNREALED_API UClass* Z_Construct_UClass_UDefaultSizedThumbnailRenderer();
	UPackage* Z_Construct_UPackage__Script_GeometryCollectionEditor();
// End Cross Module References
	void UGeometryCollectionThumbnailRenderer::StaticRegisterNativesUGeometryCollectionThumbnailRenderer()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_NoRegister()
	{
		return UGeometryCollectionThumbnailRenderer::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDefaultSizedThumbnailRenderer,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCollectionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GeometryCollection/GeometryCollectionThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Public/GeometryCollection/GeometryCollectionThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCollectionThumbnailRenderer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics::ClassParams = {
		&UGeometryCollectionThumbnailRenderer::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCollectionThumbnailRenderer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCollectionThumbnailRenderer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCollectionThumbnailRenderer, 2696652587);
	template<> GEOMETRYCOLLECTIONEDITOR_API UClass* StaticClass<UGeometryCollectionThumbnailRenderer>()
	{
		return UGeometryCollectionThumbnailRenderer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCollectionThumbnailRenderer(Z_Construct_UClass_UGeometryCollectionThumbnailRenderer, &UGeometryCollectionThumbnailRenderer::StaticClass, TEXT("/Script/GeometryCollectionEditor"), TEXT("UGeometryCollectionThumbnailRenderer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCollectionThumbnailRenderer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
