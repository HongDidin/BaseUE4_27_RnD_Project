// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCollectionEditor/Private/GeometryCollection/ActorFactoryGeometryCollection.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeActorFactoryGeometryCollection() {}
// Cross Module References
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UActorFactoryGeometryCollection_NoRegister();
	GEOMETRYCOLLECTIONEDITOR_API UClass* Z_Construct_UClass_UActorFactoryGeometryCollection();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_GeometryCollectionEditor();
// End Cross Module References
	void UActorFactoryGeometryCollection::StaticRegisterNativesUActorFactoryGeometryCollection()
	{
	}
	UClass* Z_Construct_UClass_UActorFactoryGeometryCollection_NoRegister()
	{
		return UActorFactoryGeometryCollection::StaticClass();
	}
	struct Z_Construct_UClass_UActorFactoryGeometryCollection_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UActorFactoryGeometryCollection_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCollectionEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UActorFactoryGeometryCollection_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "GeometryCollection/ActorFactoryGeometryCollection.h" },
		{ "ModuleRelativePath", "Private/GeometryCollection/ActorFactoryGeometryCollection.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UActorFactoryGeometryCollection_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UActorFactoryGeometryCollection>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UActorFactoryGeometryCollection_Statics::ClassParams = {
		&UActorFactoryGeometryCollection::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UActorFactoryGeometryCollection_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UActorFactoryGeometryCollection_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UActorFactoryGeometryCollection()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UActorFactoryGeometryCollection_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UActorFactoryGeometryCollection, 1805374863);
	template<> GEOMETRYCOLLECTIONEDITOR_API UClass* StaticClass<UActorFactoryGeometryCollection>()
	{
		return UActorFactoryGeometryCollection::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UActorFactoryGeometryCollection(Z_Construct_UClass_UActorFactoryGeometryCollection, &UActorFactoryGeometryCollection::StaticClass, TEXT("/Script/GeometryCollectionEditor"), TEXT("UActorFactoryGeometryCollection"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UActorFactoryGeometryCollection);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
