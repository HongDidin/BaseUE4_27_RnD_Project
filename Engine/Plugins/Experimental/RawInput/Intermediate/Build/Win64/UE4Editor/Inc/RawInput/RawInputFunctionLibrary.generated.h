// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FRegisteredDeviceInfo;
#ifdef RAWINPUT_RawInputFunctionLibrary_generated_h
#error "RawInputFunctionLibrary.generated.h already included, missing '#pragma once' in RawInputFunctionLibrary.h"
#endif
#define RAWINPUT_RawInputFunctionLibrary_generated_h

#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_80_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRegisteredDeviceInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> RAWINPUT_API UScriptStruct* StaticStruct<struct FRegisteredDeviceInfo>();

#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_SPARSE_DATA
#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetRegisteredDevices);


#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetRegisteredDevices);


#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURawInputFunctionLibrary(); \
	friend struct Z_Construct_UClass_URawInputFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URawInputFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RawInput"), NO_API) \
	DECLARE_SERIALIZER(URawInputFunctionLibrary)


#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_INCLASS \
private: \
	static void StaticRegisterNativesURawInputFunctionLibrary(); \
	friend struct Z_Construct_UClass_URawInputFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(URawInputFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/RawInput"), NO_API) \
	DECLARE_SERIALIZER(URawInputFunctionLibrary)


#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URawInputFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URawInputFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URawInputFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URawInputFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URawInputFunctionLibrary(URawInputFunctionLibrary&&); \
	NO_API URawInputFunctionLibrary(const URawInputFunctionLibrary&); \
public:


#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URawInputFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URawInputFunctionLibrary(URawInputFunctionLibrary&&); \
	NO_API URawInputFunctionLibrary(const URawInputFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URawInputFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URawInputFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URawInputFunctionLibrary)


#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_98_PROLOG
#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_SPARSE_DATA \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_RPC_WRAPPERS \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_INCLASS \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_SPARSE_DATA \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h_101_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> RAWINPUT_API UClass* StaticClass<class URawInputFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_RawInput_Source_RawInput_Public_RawInputFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
