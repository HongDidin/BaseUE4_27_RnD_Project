// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UGPULightmassSettings;
#ifdef GPULIGHTMASS_GPULightmassSettings_generated_h
#error "GPULightmassSettings.generated.h already included, missing '#pragma once' in GPULightmassSettings.h"
#endif
#define GPULIGHTMASS_GPULightmassSettings_generated_h

#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_SPARSE_DATA
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGPULightmassSettings(); \
	friend struct Z_Construct_UClass_UGPULightmassSettings_Statics; \
public: \
	DECLARE_CLASS(UGPULightmassSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GPULightmass"), NO_API) \
	DECLARE_SERIALIZER(UGPULightmassSettings)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUGPULightmassSettings(); \
	friend struct Z_Construct_UClass_UGPULightmassSettings_Statics; \
public: \
	DECLARE_CLASS(UGPULightmassSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GPULightmass"), NO_API) \
	DECLARE_SERIALIZER(UGPULightmassSettings)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGPULightmassSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGPULightmassSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGPULightmassSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGPULightmassSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGPULightmassSettings(UGPULightmassSettings&&); \
	NO_API UGPULightmassSettings(const UGPULightmassSettings&); \
public:


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGPULightmassSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGPULightmassSettings(UGPULightmassSettings&&); \
	NO_API UGPULightmassSettings(const UGPULightmassSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGPULightmassSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGPULightmassSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGPULightmassSettings)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_25_PROLOG
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_INCLASS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_SPARSE_DATA \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GPULIGHTMASS_API UClass* StaticClass<class UGPULightmassSettings>();

#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_SPARSE_DATA
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_RPC_WRAPPERS
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGPULightmassSettingsActor(); \
	friend struct Z_Construct_UClass_AGPULightmassSettingsActor_Statics; \
public: \
	DECLARE_CLASS(AGPULightmassSettingsActor, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GPULightmass"), NO_API) \
	DECLARE_SERIALIZER(AGPULightmassSettingsActor)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_INCLASS \
private: \
	static void StaticRegisterNativesAGPULightmassSettingsActor(); \
	friend struct Z_Construct_UClass_AGPULightmassSettingsActor_Statics; \
public: \
	DECLARE_CLASS(AGPULightmassSettingsActor, AInfo, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/GPULightmass"), NO_API) \
	DECLARE_SERIALIZER(AGPULightmassSettingsActor)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGPULightmassSettingsActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGPULightmassSettingsActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGPULightmassSettingsActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGPULightmassSettingsActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGPULightmassSettingsActor(AGPULightmassSettingsActor&&); \
	NO_API AGPULightmassSettingsActor(const AGPULightmassSettingsActor&); \
public:


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGPULightmassSettingsActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGPULightmassSettingsActor(AGPULightmassSettingsActor&&); \
	NO_API AGPULightmassSettingsActor(const AGPULightmassSettingsActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGPULightmassSettingsActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGPULightmassSettingsActor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGPULightmassSettingsActor)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_129_PROLOG
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_SPARSE_DATA \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_INCLASS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_SPARSE_DATA \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_132_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class GPULightmassSettingsActor."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GPULIGHTMASS_API UClass* StaticClass<class AGPULightmassSettingsActor>();

#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_SPARSE_DATA
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSave); \
	DECLARE_FUNCTION(execSetRealtime); \
	DECLARE_FUNCTION(execGetPercentage); \
	DECLARE_FUNCTION(execEndRecordingVisibleTiles); \
	DECLARE_FUNCTION(execStartRecordingVisibleTiles); \
	DECLARE_FUNCTION(execIsRunning); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execLaunch); \
	DECLARE_FUNCTION(execGetSettings);


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSave); \
	DECLARE_FUNCTION(execSetRealtime); \
	DECLARE_FUNCTION(execGetPercentage); \
	DECLARE_FUNCTION(execEndRecordingVisibleTiles); \
	DECLARE_FUNCTION(execStartRecordingVisibleTiles); \
	DECLARE_FUNCTION(execIsRunning); \
	DECLARE_FUNCTION(execStop); \
	DECLARE_FUNCTION(execLaunch); \
	DECLARE_FUNCTION(execGetSettings);


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGPULightmassSubsystem(); \
	friend struct Z_Construct_UClass_UGPULightmassSubsystem_Statics; \
public: \
	DECLARE_CLASS(UGPULightmassSubsystem, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GPULightmass"), NO_API) \
	DECLARE_SERIALIZER(UGPULightmassSubsystem)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_INCLASS \
private: \
	static void StaticRegisterNativesUGPULightmassSubsystem(); \
	friend struct Z_Construct_UClass_UGPULightmassSubsystem_Statics; \
public: \
	DECLARE_CLASS(UGPULightmassSubsystem, UWorldSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/GPULightmass"), NO_API) \
	DECLARE_SERIALIZER(UGPULightmassSubsystem)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGPULightmassSubsystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGPULightmassSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGPULightmassSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGPULightmassSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGPULightmassSubsystem(UGPULightmassSubsystem&&); \
	NO_API UGPULightmassSubsystem(const UGPULightmassSubsystem&); \
public:


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGPULightmassSubsystem() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGPULightmassSubsystem(UGPULightmassSubsystem&&); \
	NO_API UGPULightmassSubsystem(const UGPULightmassSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGPULightmassSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGPULightmassSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGPULightmassSubsystem)


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_139_PROLOG
#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_SPARSE_DATA \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_RPC_WRAPPERS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_INCLASS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_SPARSE_DATA \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h_142_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GPULIGHTMASS_API UClass* StaticClass<class UGPULightmassSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_GPULightmass_Source_GPULightmass_Public_GPULightmassSettings_h


#define FOREACH_ENUM_EGPULIGHTMASSDENOISINGOPTIONS(op) \
	op(EGPULightmassDenoisingOptions::None) \
	op(EGPULightmassDenoisingOptions::OnCompletion) \
	op(EGPULightmassDenoisingOptions::DuringInteractivePreview) 

enum class EGPULightmassDenoisingOptions : uint8;
template<> GPULIGHTMASS_API UEnum* StaticEnum<EGPULightmassDenoisingOptions>();

#define FOREACH_ENUM_EGPULIGHTMASSMODE(op) \
	op(EGPULightmassMode::FullBake) \
	op(EGPULightmassMode::BakeWhatYouSee) 

enum class EGPULightmassMode : uint8;
template<> GPULIGHTMASS_API UEnum* StaticEnum<EGPULightmassMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
