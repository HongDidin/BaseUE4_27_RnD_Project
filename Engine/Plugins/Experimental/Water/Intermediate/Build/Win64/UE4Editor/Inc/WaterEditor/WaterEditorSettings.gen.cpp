// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Public/WaterEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterEditorSettings() {}
// Cross Module References
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyIslandDefaults();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushActorDefaults();
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyCustomDefaults();
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyDefaults();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyOceanDefaults();
	WATER_API UClass* Z_Construct_UClass_UWaterWavesBase_NoRegister();
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyLakeDefaults();
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyRiverDefaults();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterSplineCurveDefaults();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterCurveSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterEditorSettings_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterEditorSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_TextureGroup();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialParameterCollection_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
class UScriptStruct* FWaterBodyIslandDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyIslandDefaults, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBodyIslandDefaults"), sizeof(FWaterBodyIslandDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBodyIslandDefaults>()
{
	return FWaterBodyIslandDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyIslandDefaults(FWaterBodyIslandDefaults::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBodyIslandDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyIslandDefaults
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyIslandDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyIslandDefaults>(FName(TEXT("WaterBodyIslandDefaults")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyIslandDefaults;
	struct Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BrushDefaults;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyIslandDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::NewProp_BrushDefaults_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::NewProp_BrushDefaults = { "BrushDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyIslandDefaults, BrushDefaults), Z_Construct_UScriptStruct_FWaterBrushActorDefaults, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::NewProp_BrushDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::NewProp_BrushDefaults_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::NewProp_BrushDefaults,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		nullptr,
		&NewStructOps,
		"WaterBodyIslandDefaults",
		sizeof(FWaterBodyIslandDefaults),
		alignof(FWaterBodyIslandDefaults),
		Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyIslandDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyIslandDefaults"), sizeof(FWaterBodyIslandDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyIslandDefaults_Hash() { return 2032954771U; }

static_assert(std::is_polymorphic<FWaterBodyCustomDefaults>() == std::is_polymorphic<FWaterBodyDefaults>(), "USTRUCT FWaterBodyCustomDefaults cannot be polymorphic unless super FWaterBodyDefaults is polymorphic");

class UScriptStruct* FWaterBodyCustomDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyCustomDefaults, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBodyCustomDefaults"), sizeof(FWaterBodyCustomDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBodyCustomDefaults>()
{
	return FWaterBodyCustomDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyCustomDefaults(FWaterBodyCustomDefaults::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBodyCustomDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyCustomDefaults
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyCustomDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyCustomDefaults>(FName(TEXT("WaterBodyCustomDefaults")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyCustomDefaults;
	struct Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_WaterMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyCustomDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::NewProp_WaterMesh_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::NewProp_WaterMesh = { "WaterMesh", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyCustomDefaults, WaterMesh), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::NewProp_WaterMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::NewProp_WaterMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::NewProp_WaterMesh,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		Z_Construct_UScriptStruct_FWaterBodyDefaults,
		&NewStructOps,
		"WaterBodyCustomDefaults",
		sizeof(FWaterBodyCustomDefaults),
		alignof(FWaterBodyCustomDefaults),
		Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyCustomDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyCustomDefaults"), sizeof(FWaterBodyCustomDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyCustomDefaults_Hash() { return 2829039967U; }

static_assert(std::is_polymorphic<FWaterBodyOceanDefaults>() == std::is_polymorphic<FWaterBodyDefaults>(), "USTRUCT FWaterBodyOceanDefaults cannot be polymorphic unless super FWaterBodyDefaults is polymorphic");

class UScriptStruct* FWaterBodyOceanDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBodyOceanDefaults"), sizeof(FWaterBodyOceanDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBodyOceanDefaults>()
{
	return FWaterBodyOceanDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyOceanDefaults(FWaterBodyOceanDefaults::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBodyOceanDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyOceanDefaults
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyOceanDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyOceanDefaults>(FName(TEXT("WaterBodyOceanDefaults")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyOceanDefaults;
	struct Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BrushDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterWaves_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterWaves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyOceanDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_BrushDefaults_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_BrushDefaults = { "BrushDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyOceanDefaults, BrushDefaults), Z_Construct_UScriptStruct_FWaterBrushActorDefaults, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_BrushDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_BrushDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_WaterWaves_MetaData[] = {
		{ "Category", "Wave" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_WaterWaves = { "WaterWaves", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyOceanDefaults, WaterWaves), Z_Construct_UClass_UWaterWavesBase_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_WaterWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_WaterWaves_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_BrushDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::NewProp_WaterWaves,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		Z_Construct_UScriptStruct_FWaterBodyDefaults,
		&NewStructOps,
		"WaterBodyOceanDefaults",
		sizeof(FWaterBodyOceanDefaults),
		alignof(FWaterBodyOceanDefaults),
		Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyOceanDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyOceanDefaults"), sizeof(FWaterBodyOceanDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyOceanDefaults_Hash() { return 3447504321U; }

static_assert(std::is_polymorphic<FWaterBodyLakeDefaults>() == std::is_polymorphic<FWaterBodyDefaults>(), "USTRUCT FWaterBodyLakeDefaults cannot be polymorphic unless super FWaterBodyDefaults is polymorphic");

class UScriptStruct* FWaterBodyLakeDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBodyLakeDefaults"), sizeof(FWaterBodyLakeDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBodyLakeDefaults>()
{
	return FWaterBodyLakeDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyLakeDefaults(FWaterBodyLakeDefaults::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBodyLakeDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyLakeDefaults
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyLakeDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyLakeDefaults>(FName(TEXT("WaterBodyLakeDefaults")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyLakeDefaults;
	struct Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BrushDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterWaves_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterWaves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyLakeDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_BrushDefaults_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_BrushDefaults = { "BrushDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyLakeDefaults, BrushDefaults), Z_Construct_UScriptStruct_FWaterBrushActorDefaults, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_BrushDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_BrushDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_WaterWaves_MetaData[] = {
		{ "Category", "Wave" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_WaterWaves = { "WaterWaves", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyLakeDefaults, WaterWaves), Z_Construct_UClass_UWaterWavesBase_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_WaterWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_WaterWaves_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_BrushDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::NewProp_WaterWaves,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		Z_Construct_UScriptStruct_FWaterBodyDefaults,
		&NewStructOps,
		"WaterBodyLakeDefaults",
		sizeof(FWaterBodyLakeDefaults),
		alignof(FWaterBodyLakeDefaults),
		Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000205),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyLakeDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyLakeDefaults"), sizeof(FWaterBodyLakeDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyLakeDefaults_Hash() { return 363130823U; }

static_assert(std::is_polymorphic<FWaterBodyRiverDefaults>() == std::is_polymorphic<FWaterBodyDefaults>(), "USTRUCT FWaterBodyRiverDefaults cannot be polymorphic unless super FWaterBodyDefaults is polymorphic");

class UScriptStruct* FWaterBodyRiverDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBodyRiverDefaults"), sizeof(FWaterBodyRiverDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBodyRiverDefaults>()
{
	return FWaterBodyRiverDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyRiverDefaults(FWaterBodyRiverDefaults::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBodyRiverDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyRiverDefaults
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyRiverDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyRiverDefaults>(FName(TEXT("WaterBodyRiverDefaults")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyRiverDefaults;
	struct Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BrushDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RiverToOceanTransitionMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_RiverToOceanTransitionMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RiverToLakeTransitionMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_RiverToLakeTransitionMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyRiverDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_BrushDefaults_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_BrushDefaults = { "BrushDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyRiverDefaults, BrushDefaults), Z_Construct_UScriptStruct_FWaterBrushActorDefaults, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_BrushDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_BrushDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToOceanTransitionMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToOceanTransitionMaterial = { "RiverToOceanTransitionMaterial", nullptr, (EPropertyFlags)0x0024080000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyRiverDefaults, RiverToOceanTransitionMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToOceanTransitionMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToOceanTransitionMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToLakeTransitionMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToLakeTransitionMaterial = { "RiverToLakeTransitionMaterial", nullptr, (EPropertyFlags)0x0024080000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyRiverDefaults, RiverToLakeTransitionMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToLakeTransitionMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToLakeTransitionMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_BrushDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToOceanTransitionMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::NewProp_RiverToLakeTransitionMaterial,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		Z_Construct_UScriptStruct_FWaterBodyDefaults,
		&NewStructOps,
		"WaterBodyRiverDefaults",
		sizeof(FWaterBodyRiverDefaults),
		alignof(FWaterBodyRiverDefaults),
		Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyRiverDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyRiverDefaults"), sizeof(FWaterBodyRiverDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyRiverDefaults_Hash() { return 4002052046U; }
class UScriptStruct* FWaterBodyDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyDefaults, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBodyDefaults"), sizeof(FWaterBodyDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyDefaults_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBodyDefaults>()
{
	return FWaterBodyDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyDefaults(FWaterBodyDefaults::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBodyDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyDefaults
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyDefaults>(FName(TEXT("WaterBodyDefaults")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyDefaults;
	struct Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SplineDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SplineDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_WaterMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderwaterPostProcessMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_UnderwaterPostProcessMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_SplineDefaults_MetaData[] = {
		{ "Category", "Water Spline" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_SplineDefaults = { "SplineDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyDefaults, SplineDefaults), Z_Construct_UScriptStruct_FWaterSplineCurveDefaults, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_SplineDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_SplineDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_WaterMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_WaterMaterial = { "WaterMaterial", nullptr, (EPropertyFlags)0x0024080000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyDefaults, WaterMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_WaterMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_WaterMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_UnderwaterPostProcessMaterial = { "UnderwaterPostProcessMaterial", nullptr, (EPropertyFlags)0x0024080000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyDefaults, UnderwaterPostProcessMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_SplineDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_WaterMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::NewProp_UnderwaterPostProcessMaterial,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		nullptr,
		&NewStructOps,
		"WaterBodyDefaults",
		sizeof(FWaterBodyDefaults),
		alignof(FWaterBodyDefaults),
		Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyDefaults"), sizeof(FWaterBodyDefaults), Get_Z_Construct_UScriptStruct_FWaterBodyDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyDefaults_Hash() { return 1763647486U; }
class UScriptStruct* FWaterBrushActorDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushActorDefaults, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBrushActorDefaults"), sizeof(FWaterBrushActorDefaults), Get_Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBrushActorDefaults>()
{
	return FWaterBrushActorDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushActorDefaults(FWaterBrushActorDefaults::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBrushActorDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBrushActorDefaults
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBrushActorDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushActorDefaults>(FName(TEXT("WaterBrushActorDefaults")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBrushActorDefaults;
	struct Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightmapSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_HeightmapSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LayerWeightmapSettings_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LayerWeightmapSettings_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LayerWeightmapSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LayerWeightmapSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushActorDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_CurveSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_CurveSettings = { "CurveSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushActorDefaults, CurveSettings), Z_Construct_UScriptStruct_FWaterCurveSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_CurveSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_CurveSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_HeightmapSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_HeightmapSettings = { "HeightmapSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushActorDefaults, HeightmapSettings), Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_HeightmapSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_HeightmapSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings_ValueProp = { "LayerWeightmapSettings", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings_Key_KeyProp = { "LayerWeightmapSettings_Key", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings = { "LayerWeightmapSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushActorDefaults, LayerWeightmapSettings), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_CurveSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_HeightmapSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::NewProp_LayerWeightmapSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		nullptr,
		&NewStructOps,
		"WaterBrushActorDefaults",
		sizeof(FWaterBrushActorDefaults),
		alignof(FWaterBrushActorDefaults),
		Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushActorDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushActorDefaults"), sizeof(FWaterBrushActorDefaults), Get_Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushActorDefaults_Hash() { return 3735209257U; }
	void UWaterEditorSettings::StaticRegisterNativesUWaterEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UWaterEditorSettings_NoRegister()
	{
		return UWaterEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UWaterEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureGroupForGeneratedTextures_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TextureGroupForGeneratedTextures;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxWaterVelocityAndHeightTextureSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MaxWaterVelocityAndHeightTextureSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VisualizeWaterVelocityScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VisualizeWaterVelocityScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LandscapeMaterialParameterCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_LandscapeMaterialParameterCollection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyRiverDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterBodyRiverDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyLakeDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterBodyLakeDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyOceanDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterBodyOceanDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyCustomDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterBodyCustomDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyIslandDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterBodyIslandDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterManagerClassPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterManagerClassPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBrushAngleFalloffMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultBrushAngleFalloffMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBrushIslandFalloffMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultBrushIslandFalloffMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBrushWidthFalloffMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultBrushWidthFalloffMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBrushWeightmapMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultBrushWeightmapMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultCacheDistanceFieldCacheMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultCacheDistanceFieldCacheMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultCompositeWaterBodyTextureMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultCompositeWaterBodyTextureMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultFinalizeVelocityHeightMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultFinalizeVelocityHeightMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultJumpFloodStepMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultJumpFloodStepMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBlurEdgesMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultBlurEdgesMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultFindEdgesMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultFindEdgesMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDrawCanvasMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultDrawCanvasMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultRenderRiverSplineDepthsMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_DefaultRenderRiverSplineDepthsMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements the editor settings for the Water plugin.\n */" },
		{ "DisplayName", "Water Editor" },
		{ "IncludePath", "WaterEditorSettings.h" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Implements the editor settings for the Water plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_TextureGroupForGeneratedTextures_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** The texture group to use for generated textures such as the combined veloctiy and height texture */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "The texture group to use for generated textures such as the combined veloctiy and height texture" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_TextureGroupForGeneratedTextures = { "TextureGroupForGeneratedTextures", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, TextureGroupForGeneratedTextures), Z_Construct_UEnum_Engine_TextureGroup, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_TextureGroupForGeneratedTextures_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_TextureGroupForGeneratedTextures_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_MaxWaterVelocityAndHeightTextureSize_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ClampMax", "2048" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Maximum size of the water velocity/height texture for a WaterMeshActor */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Maximum size of the water velocity/height texture for a WaterMeshActor" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_MaxWaterVelocityAndHeightTextureSize = { "MaxWaterVelocityAndHeightTextureSize", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, MaxWaterVelocityAndHeightTextureSize), METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_MaxWaterVelocityAndHeightTextureSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_MaxWaterVelocityAndHeightTextureSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_VisualizeWaterVelocityScale_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ClampMin", "0.1" },
		{ "Comment", "/** Scale factor for visualizing water velocity */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Scale factor for visualizing water velocity" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_VisualizeWaterVelocityScale = { "VisualizeWaterVelocityScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, VisualizeWaterVelocityScale), METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_VisualizeWaterVelocityScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_VisualizeWaterVelocityScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_LandscapeMaterialParameterCollection_MetaData[] = {
		{ "Category", "Brush" },
		{ "Comment", "/** Material Parameter Collection for everything landscape-related */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Material Parameter Collection for everything landscape-related" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_LandscapeMaterialParameterCollection = { "LandscapeMaterialParameterCollection", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, LandscapeMaterialParameterCollection), Z_Construct_UClass_UMaterialParameterCollection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_LandscapeMaterialParameterCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_LandscapeMaterialParameterCollection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyRiverDefaults_MetaData[] = {
		{ "Category", "ActorDefaults" },
		{ "Comment", "/** Default values for base WaterBodyRiver actor */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Default values for base WaterBodyRiver actor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyRiverDefaults = { "WaterBodyRiverDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, WaterBodyRiverDefaults), Z_Construct_UScriptStruct_FWaterBodyRiverDefaults, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyRiverDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyRiverDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyLakeDefaults_MetaData[] = {
		{ "Category", "ActorDefaults" },
		{ "Comment", "/** Default values for base WaterBodyLake actor */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Default values for base WaterBodyLake actor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyLakeDefaults = { "WaterBodyLakeDefaults", nullptr, (EPropertyFlags)0x0010008000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, WaterBodyLakeDefaults), Z_Construct_UScriptStruct_FWaterBodyLakeDefaults, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyLakeDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyLakeDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyOceanDefaults_MetaData[] = {
		{ "Category", "ActorDefaults" },
		{ "Comment", "/** Default values for base WaterBodyOcean actor */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Default values for base WaterBodyOcean actor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyOceanDefaults = { "WaterBodyOceanDefaults", nullptr, (EPropertyFlags)0x0010008000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, WaterBodyOceanDefaults), Z_Construct_UScriptStruct_FWaterBodyOceanDefaults, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyOceanDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyOceanDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyCustomDefaults_MetaData[] = {
		{ "Category", "ActorDefaults" },
		{ "Comment", "/** Default values for base WaterBodyCustom actor */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Default values for base WaterBodyCustom actor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyCustomDefaults = { "WaterBodyCustomDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, WaterBodyCustomDefaults), Z_Construct_UScriptStruct_FWaterBodyCustomDefaults, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyCustomDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyCustomDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyIslandDefaults_MetaData[] = {
		{ "Category", "ActorDefaults" },
		{ "Comment", "/** Default values for base WaterBodyIsland actor */" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Default values for base WaterBodyIsland actor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyIslandDefaults = { "WaterBodyIslandDefaults", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, WaterBodyIslandDefaults), Z_Construct_UScriptStruct_FWaterBodyIslandDefaults, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyIslandDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyIslandDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterManagerClassPath_MetaData[] = {
		{ "Category", "Brush" },
		{ "Comment", "/** Class of the water brush to be used in landscape */" },
		{ "MetaClass", "WaterLandscapeBrush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
		{ "ToolTip", "Class of the water brush to be used in landscape" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterManagerClassPath = { "WaterManagerClassPath", nullptr, (EPropertyFlags)0x0040000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, WaterManagerClassPath), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterManagerClassPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterManagerClassPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushAngleFalloffMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushAngleFalloffMaterial = { "DefaultBrushAngleFalloffMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultBrushAngleFalloffMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushAngleFalloffMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushAngleFalloffMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushIslandFalloffMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushIslandFalloffMaterial = { "DefaultBrushIslandFalloffMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultBrushIslandFalloffMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushIslandFalloffMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushIslandFalloffMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWidthFalloffMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWidthFalloffMaterial = { "DefaultBrushWidthFalloffMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultBrushWidthFalloffMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWidthFalloffMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWidthFalloffMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWeightmapMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWeightmapMaterial = { "DefaultBrushWeightmapMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultBrushWeightmapMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWeightmapMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWeightmapMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCacheDistanceFieldCacheMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCacheDistanceFieldCacheMaterial = { "DefaultCacheDistanceFieldCacheMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultCacheDistanceFieldCacheMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCacheDistanceFieldCacheMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCacheDistanceFieldCacheMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCompositeWaterBodyTextureMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCompositeWaterBodyTextureMaterial = { "DefaultCompositeWaterBodyTextureMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultCompositeWaterBodyTextureMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCompositeWaterBodyTextureMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCompositeWaterBodyTextureMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFinalizeVelocityHeightMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFinalizeVelocityHeightMaterial = { "DefaultFinalizeVelocityHeightMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultFinalizeVelocityHeightMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFinalizeVelocityHeightMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFinalizeVelocityHeightMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultJumpFloodStepMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultJumpFloodStepMaterial = { "DefaultJumpFloodStepMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultJumpFloodStepMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultJumpFloodStepMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultJumpFloodStepMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBlurEdgesMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBlurEdgesMaterial = { "DefaultBlurEdgesMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultBlurEdgesMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBlurEdgesMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBlurEdgesMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFindEdgesMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFindEdgesMaterial = { "DefaultFindEdgesMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultFindEdgesMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFindEdgesMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFindEdgesMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultDrawCanvasMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultDrawCanvasMaterial = { "DefaultDrawCanvasMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultDrawCanvasMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultDrawCanvasMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultDrawCanvasMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultRenderRiverSplineDepthsMaterial_MetaData[] = {
		{ "Category", "Brush" },
		{ "ModuleRelativePath", "Public/WaterEditorSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultRenderRiverSplineDepthsMaterial = { "DefaultRenderRiverSplineDepthsMaterial", nullptr, (EPropertyFlags)0x0044000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSettings, DefaultRenderRiverSplineDepthsMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultRenderRiverSplineDepthsMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultRenderRiverSplineDepthsMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_TextureGroupForGeneratedTextures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_MaxWaterVelocityAndHeightTextureSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_VisualizeWaterVelocityScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_LandscapeMaterialParameterCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyRiverDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyLakeDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyOceanDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyCustomDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterBodyIslandDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_WaterManagerClassPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushAngleFalloffMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushIslandFalloffMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWidthFalloffMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBrushWeightmapMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCacheDistanceFieldCacheMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultCompositeWaterBodyTextureMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFinalizeVelocityHeightMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultJumpFloodStepMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultBlurEdgesMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultFindEdgesMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultDrawCanvasMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSettings_Statics::NewProp_DefaultRenderRiverSplineDepthsMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterEditorSettings_Statics::ClassParams = {
		&UWaterEditorSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::PropPointers),
		0,
		0x009000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterEditorSettings, 748176230);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterEditorSettings>()
	{
		return UWaterEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterEditorSettings(Z_Construct_UClass_UWaterEditorSettings, &UWaterEditorSettings::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
