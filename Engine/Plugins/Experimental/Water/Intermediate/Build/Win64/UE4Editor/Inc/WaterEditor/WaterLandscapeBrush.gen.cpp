// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Private/WaterLandscapeBrush.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterLandscapeBrush() {}
// Cross Module References
	WATEREDITOR_API UClass* Z_Construct_UClass_AWaterLandscapeBrush_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_AWaterLandscapeBrush();
	LANDSCAPEEDITORUTILITIES_API UClass* Z_Construct_UClass_ALandscapeBlueprintBrush();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterBrushActorInterface_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyIsland_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBillboardComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AWaterLandscapeBrush::execForceWaterTextureUpdate)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ForceWaterTextureUpdate();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execBlueprintOnRenderTargetTexturesUpdated)
	{
		P_GET_OBJECT(UTexture2D,Z_Param_VelocityTexture);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BlueprintOnRenderTargetTexturesUpdated_Implementation(Z_Param_VelocityTexture);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execBlueprintGetRenderTargets)
	{
		P_GET_OBJECT(UTextureRenderTarget2D,Z_Param_InHeightRenderTarget);
		P_GET_OBJECT_REF(UTextureRenderTarget2D,Z_Param_Out_OutVelocityRenderTarget);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BlueprintGetRenderTargets_Implementation(Z_Param_InHeightRenderTarget,Z_Param_Out_OutVelocityRenderTarget);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execClearActorCache)
	{
		P_GET_OBJECT(AActor,Z_Param_InActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearActorCache(Z_Param_InActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execGetActorCache)
	{
		P_GET_OBJECT(AActor,Z_Param_InActor);
		P_GET_OBJECT(UClass,Z_Param_CacheClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UObject**)Z_Param__Result=P_THIS->GetActorCache(Z_Param_InActor,Z_Param_CacheClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execSetActorCache)
	{
		P_GET_OBJECT(AActor,Z_Param_InActor);
		P_GET_OBJECT(UObject,Z_Param_InCache);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetActorCache(Z_Param_InActor,Z_Param_InCache);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execClearWaterBodyCache)
	{
		P_GET_OBJECT(AWaterBody,Z_Param_WaterBody);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ClearWaterBodyCache(Z_Param_WaterBody);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execGetWaterBodyCache)
	{
		P_GET_OBJECT(AWaterBody,Z_Param_WaterBody);
		P_GET_OBJECT(UClass,Z_Param_CacheClass);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UObject**)Z_Param__Result=P_THIS->GetWaterBodyCache(Z_Param_WaterBody,Z_Param_CacheClass);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execSetWaterBodyCache)
	{
		P_GET_OBJECT(AWaterBody,Z_Param_WaterBody);
		P_GET_OBJECT(UObject,Z_Param_Cache);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetWaterBodyCache(Z_Param_WaterBody,Z_Param_Cache);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execBlueprintWaterBodyChanged)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BlueprintWaterBodyChanged_Implementation(Z_Param_Actor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execBlueprintWaterBodiesChanged)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BlueprintWaterBodiesChanged_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execGetActorsAffectingLandscape)
	{
		P_GET_TARRAY_REF(TScriptInterface<IWaterBrushActorInterface>,Z_Param_Out_OutWaterBrushActors);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetActorsAffectingLandscape(Z_Param_Out_OutWaterBrushActors);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execGetWaterBodyIslands)
	{
		P_GET_OBJECT(UClass,Z_Param_WaterBodyIslandClass);
		P_GET_TARRAY_REF(AWaterBodyIsland*,Z_Param_Out_OutWaterBodyIslands);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetWaterBodyIslands(Z_Param_WaterBodyIslandClass,Z_Param_Out_OutWaterBodyIslands);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterLandscapeBrush::execGetWaterBodies)
	{
		P_GET_OBJECT(UClass,Z_Param_WaterBodyClass);
		P_GET_TARRAY_REF(AWaterBody*,Z_Param_Out_OutWaterBodies);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetWaterBodies(Z_Param_WaterBodyClass,Z_Param_Out_OutWaterBodies);
		P_NATIVE_END;
	}
	static FName NAME_AWaterLandscapeBrush_BlueprintGetRenderTargets = FName(TEXT("BlueprintGetRenderTargets"));
	void AWaterLandscapeBrush::BlueprintGetRenderTargets(UTextureRenderTarget2D* InHeightRenderTarget, UTextureRenderTarget2D*& OutVelocityRenderTarget)
	{
		WaterLandscapeBrush_eventBlueprintGetRenderTargets_Parms Parms;
		Parms.InHeightRenderTarget=InHeightRenderTarget;
		Parms.OutVelocityRenderTarget=OutVelocityRenderTarget;
		ProcessEvent(FindFunctionChecked(NAME_AWaterLandscapeBrush_BlueprintGetRenderTargets),&Parms);
		OutVelocityRenderTarget=Parms.OutVelocityRenderTarget;
	}
	static FName NAME_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated = FName(TEXT("BlueprintOnRenderTargetTexturesUpdated"));
	void AWaterLandscapeBrush::BlueprintOnRenderTargetTexturesUpdated(UTexture2D* VelocityTexture)
	{
		WaterLandscapeBrush_eventBlueprintOnRenderTargetTexturesUpdated_Parms Parms;
		Parms.VelocityTexture=VelocityTexture;
		ProcessEvent(FindFunctionChecked(NAME_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated),&Parms);
	}
	static FName NAME_AWaterLandscapeBrush_BlueprintWaterBodiesChanged = FName(TEXT("BlueprintWaterBodiesChanged"));
	void AWaterLandscapeBrush::BlueprintWaterBodiesChanged()
	{
		ProcessEvent(FindFunctionChecked(NAME_AWaterLandscapeBrush_BlueprintWaterBodiesChanged),NULL);
	}
	static FName NAME_AWaterLandscapeBrush_BlueprintWaterBodyChanged = FName(TEXT("BlueprintWaterBodyChanged"));
	void AWaterLandscapeBrush::BlueprintWaterBodyChanged(AActor* Actor)
	{
		WaterLandscapeBrush_eventBlueprintWaterBodyChanged_Parms Parms;
		Parms.Actor=Actor;
		ProcessEvent(FindFunctionChecked(NAME_AWaterLandscapeBrush_BlueprintWaterBodyChanged),&Parms);
	}
	void AWaterLandscapeBrush::StaticRegisterNativesAWaterLandscapeBrush()
	{
		UClass* Class = AWaterLandscapeBrush::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BlueprintGetRenderTargets", &AWaterLandscapeBrush::execBlueprintGetRenderTargets },
			{ "BlueprintOnRenderTargetTexturesUpdated", &AWaterLandscapeBrush::execBlueprintOnRenderTargetTexturesUpdated },
			{ "BlueprintWaterBodiesChanged", &AWaterLandscapeBrush::execBlueprintWaterBodiesChanged },
			{ "BlueprintWaterBodyChanged", &AWaterLandscapeBrush::execBlueprintWaterBodyChanged },
			{ "ClearActorCache", &AWaterLandscapeBrush::execClearActorCache },
			{ "ClearWaterBodyCache", &AWaterLandscapeBrush::execClearWaterBodyCache },
			{ "ForceWaterTextureUpdate", &AWaterLandscapeBrush::execForceWaterTextureUpdate },
			{ "GetActorCache", &AWaterLandscapeBrush::execGetActorCache },
			{ "GetActorsAffectingLandscape", &AWaterLandscapeBrush::execGetActorsAffectingLandscape },
			{ "GetWaterBodies", &AWaterLandscapeBrush::execGetWaterBodies },
			{ "GetWaterBodyCache", &AWaterLandscapeBrush::execGetWaterBodyCache },
			{ "GetWaterBodyIslands", &AWaterLandscapeBrush::execGetWaterBodyIslands },
			{ "SetActorCache", &AWaterLandscapeBrush::execSetActorCache },
			{ "SetWaterBodyCache", &AWaterLandscapeBrush::execSetWaterBodyCache },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InHeightRenderTarget;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutVelocityRenderTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::NewProp_InHeightRenderTarget = { "InHeightRenderTarget", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventBlueprintGetRenderTargets_Parms, InHeightRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::NewProp_OutVelocityRenderTarget = { "OutVelocityRenderTarget", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventBlueprintGetRenderTargets_Parms, OutVelocityRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::NewProp_InHeightRenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::NewProp_OutVelocityRenderTarget,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Cache" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "BlueprintGetRenderTargets", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventBlueprintGetRenderTargets_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08420C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VelocityTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::NewProp_VelocityTexture = { "VelocityTexture", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventBlueprintOnRenderTargetTexturesUpdated_Parms, VelocityTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::NewProp_VelocityTexture,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Cache" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "BlueprintOnRenderTargetTexturesUpdated", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventBlueprintOnRenderTargetTexturesUpdated_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "BlueprintWaterBodiesChanged", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventBlueprintWaterBodyChanged_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::NewProp_Actor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "BlueprintWaterBodyChanged", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventBlueprintWaterBodyChanged_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics
	{
		struct WaterLandscapeBrush_eventClearActorCache_Parms
		{
			AActor* InActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::NewProp_InActor = { "InActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventClearActorCache_Parms, InActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::NewProp_InActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cache" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "ClearActorCache", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventClearActorCache_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics
	{
		struct WaterLandscapeBrush_eventClearWaterBodyCache_Parms
		{
			AWaterBody* WaterBody;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterBody;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::NewProp_WaterBody = { "WaterBody", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventClearWaterBodyCache_Parms, WaterBody), Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::NewProp_WaterBody,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cache" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use ClearActorCache instead" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "ClearWaterBodyCache", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventClearWaterBodyCache_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cache" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "ForceWaterTextureUpdate", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics
	{
		struct WaterLandscapeBrush_eventGetActorCache_Parms
		{
			AActor* InActor;
			TSubclassOf<UObject>  CacheClass;
			UObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InActor;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CacheClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::NewProp_InActor = { "InActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetActorCache_Parms, InActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::NewProp_CacheClass = { "CacheClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetActorCache_Parms, CacheClass), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetActorCache_Parms, ReturnValue), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::NewProp_InActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::NewProp_CacheClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cache" },
		{ "DeterminesOutputType", "CacheClass" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "GetActorCache", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventGetActorCache_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics
	{
		struct WaterLandscapeBrush_eventGetActorsAffectingLandscape_Parms
		{
			TArray<TScriptInterface<IWaterBrushActorInterface> > OutWaterBrushActors;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_OutWaterBrushActors_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutWaterBrushActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::NewProp_OutWaterBrushActors_Inner = { "OutWaterBrushActors", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UWaterBrushActorInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::NewProp_OutWaterBrushActors = { "OutWaterBrushActors", nullptr, (EPropertyFlags)0x0014000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetActorsAffectingLandscape_Parms, OutWaterBrushActors), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::NewProp_OutWaterBrushActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::NewProp_OutWaterBrushActors,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "GetActorsAffectingLandscape", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventGetActorsAffectingLandscape_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics
	{
		struct WaterLandscapeBrush_eventGetWaterBodies_Parms
		{
			TSubclassOf<AWaterBody>  WaterBodyClass;
			TArray<AWaterBody*> OutWaterBodies;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WaterBodyClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutWaterBodies_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutWaterBodies;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::NewProp_WaterBodyClass = { "WaterBodyClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetWaterBodies_Parms, WaterBodyClass), Z_Construct_UClass_AWaterBody_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::NewProp_OutWaterBodies_Inner = { "OutWaterBodies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::NewProp_OutWaterBodies = { "OutWaterBodies", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetWaterBodies_Parms, OutWaterBodies), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::NewProp_WaterBodyClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::NewProp_OutWaterBodies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::NewProp_OutWaterBodies,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "DeterminesOutputType", "WaterBodyClass" },
		{ "DynamicOutputParam", "OutWaterBodies" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "GetWaterBodies", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventGetWaterBodies_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics
	{
		struct WaterLandscapeBrush_eventGetWaterBodyCache_Parms
		{
			AWaterBody* WaterBody;
			TSubclassOf<UObject>  CacheClass;
			UObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterBody;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CacheClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::NewProp_WaterBody = { "WaterBody", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetWaterBodyCache_Parms, WaterBody), Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::NewProp_CacheClass = { "CacheClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetWaterBodyCache_Parms, CacheClass), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetWaterBodyCache_Parms, ReturnValue), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::NewProp_WaterBody,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::NewProp_CacheClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cache" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use GetActorCache instead" },
		{ "DeterminesOutputType", "CacheClass" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "GetWaterBodyCache", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventGetWaterBodyCache_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics
	{
		struct WaterLandscapeBrush_eventGetWaterBodyIslands_Parms
		{
			TSubclassOf<AWaterBodyIsland>  WaterBodyIslandClass;
			TArray<AWaterBodyIsland*> OutWaterBodyIslands;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WaterBodyIslandClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutWaterBodyIslands_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutWaterBodyIslands;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::NewProp_WaterBodyIslandClass = { "WaterBodyIslandClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetWaterBodyIslands_Parms, WaterBodyIslandClass), Z_Construct_UClass_AWaterBodyIsland_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::NewProp_OutWaterBodyIslands_Inner = { "OutWaterBodyIslands", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBodyIsland_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::NewProp_OutWaterBodyIslands = { "OutWaterBodyIslands", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventGetWaterBodyIslands_Parms, OutWaterBodyIslands), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::NewProp_WaterBodyIslandClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::NewProp_OutWaterBodyIslands_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::NewProp_OutWaterBodyIslands,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "DeterminesOutputType", "WaterBodyIslandClass" },
		{ "DynamicOutputParam", "OutWaterBodyIslands" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "GetWaterBodyIslands", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventGetWaterBodyIslands_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics
	{
		struct WaterLandscapeBrush_eventSetActorCache_Parms
		{
			AActor* InActor;
			UObject* InCache;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InCache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::NewProp_InActor = { "InActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventSetActorCache_Parms, InActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::NewProp_InCache = { "InCache", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventSetActorCache_Parms, InCache), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::NewProp_InActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::NewProp_InCache,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cache" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "SetActorCache", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventSetActorCache_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics
	{
		struct WaterLandscapeBrush_eventSetWaterBodyCache_Parms
		{
			AWaterBody* WaterBody;
			UObject* Cache;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterBody;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::NewProp_WaterBody = { "WaterBody", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventSetWaterBodyCache_Parms, WaterBody), Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::NewProp_Cache = { "Cache", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterLandscapeBrush_eventSetWaterBodyCache_Parms, Cache), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::NewProp_WaterBody,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::NewProp_Cache,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cache" },
		{ "DeprecatedFunction", "" },
		{ "DeprecationMessage", "Use SetActorCache instead" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterLandscapeBrush, nullptr, "SetWaterBodyCache", nullptr, nullptr, sizeof(WaterLandscapeBrush_eventSetWaterBodyCache_Parms), Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWaterLandscapeBrush_NoRegister()
	{
		return AWaterLandscapeBrush::StaticClass();
	}
	struct Z_Construct_UClass_AWaterLandscapeBrush_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorIcon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorIcon;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Cache_ValueProp;
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Cache_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cache_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Cache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterLandscapeBrush_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ALandscapeBlueprintBrush,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AWaterLandscapeBrush_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintGetRenderTargets, "BlueprintGetRenderTargets" }, // 3473129620
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintOnRenderTargetTexturesUpdated, "BlueprintOnRenderTargetTexturesUpdated" }, // 2966497970
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodiesChanged, "BlueprintWaterBodiesChanged" }, // 3149591184
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_BlueprintWaterBodyChanged, "BlueprintWaterBodyChanged" }, // 2517279569
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_ClearActorCache, "ClearActorCache" }, // 2456286834
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_ClearWaterBodyCache, "ClearWaterBodyCache" }, // 3584434464
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_ForceWaterTextureUpdate, "ForceWaterTextureUpdate" }, // 4143011457
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_GetActorCache, "GetActorCache" }, // 615671897
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_GetActorsAffectingLandscape, "GetActorsAffectingLandscape" }, // 1841760618
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodies, "GetWaterBodies" }, // 2191888262
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyCache, "GetWaterBodyCache" }, // 3388923098
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_GetWaterBodyIslands, "GetWaterBodyIslands" }, // 2920460477
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_SetActorCache, "SetActorCache" }, // 2716613393
		{ &Z_Construct_UFunction_AWaterLandscapeBrush_SetWaterBodyCache, "SetWaterBodyCache" }, // 2785818575
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterLandscapeBrush_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Collision Replication Input LOD Actor Cooking Rendering Replication Input LOD Actor Cooking Rendering" },
		{ "IncludePath", "WaterLandscapeBrush.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_ActorIcon_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_ActorIcon = { "ActorIcon", nullptr, (EPropertyFlags)0x0010000800082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterLandscapeBrush, ActorIcon), Z_Construct_UClass_UBillboardComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_ActorIcon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_ActorIcon_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache_ValueProp = { "Cache", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache_Key_KeyProp = { "Cache_Key", nullptr, (EPropertyFlags)0x0004000000020001, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache_MetaData[] = {
		{ "Category", "Debug" },
		{ "ModuleRelativePath", "Private/WaterLandscapeBrush.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache = { "Cache", nullptr, (EPropertyFlags)0x0040040000222001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterLandscapeBrush, Cache), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterLandscapeBrush_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_ActorIcon,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterLandscapeBrush_Statics::NewProp_Cache,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterLandscapeBrush_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterLandscapeBrush>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterLandscapeBrush_Statics::ClassParams = {
		&AWaterLandscapeBrush::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AWaterLandscapeBrush_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterLandscapeBrush_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterLandscapeBrush_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterLandscapeBrush_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterLandscapeBrush()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterLandscapeBrush_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterLandscapeBrush, 490176228);
	template<> WATEREDITOR_API UClass* StaticClass<AWaterLandscapeBrush>()
	{
		return AWaterLandscapeBrush::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterLandscapeBrush(Z_Construct_UClass_AWaterLandscapeBrush, &AWaterLandscapeBrush::StaticClass, TEXT("/Script/WaterEditor"), TEXT("AWaterLandscapeBrush"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterLandscapeBrush);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
