// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/BuoyancyComponent.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuoyancyComponent() {}
// Cross Module References
	WATER_API UFunction* Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FSphericalPontoon();
	WATER_API UFunction* Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature();
	WATER_API UClass* Z_Construct_UClass_UBuoyancyComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UBuoyancyComponent();
	ENGINE_API UClass* Z_Construct_UClass_UActorComponent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FBuoyancyData();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics
	{
		struct _Script_Water_eventOnPontoonExitedWater_Parms
		{
			FSphericalPontoon Pontoon;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pontoon_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pontoon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::NewProp_Pontoon_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::NewProp_Pontoon = { "Pontoon", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Water_eventOnPontoonExitedWater_Parms, Pontoon), Z_Construct_UScriptStruct_FSphericalPontoon, METADATA_PARAMS(Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::NewProp_Pontoon_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::NewProp_Pontoon_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::NewProp_Pontoon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Water, nullptr, "OnPontoonExitedWater__DelegateSignature", nullptr, nullptr, sizeof(_Script_Water_eventOnPontoonExitedWater_Parms), Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics
	{
		struct _Script_Water_eventOnPontoonEnteredWater_Parms
		{
			FSphericalPontoon Pontoon;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pontoon_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pontoon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::NewProp_Pontoon_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::NewProp_Pontoon = { "Pontoon", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_Water_eventOnPontoonEnteredWater_Parms, Pontoon), Z_Construct_UScriptStruct_FSphericalPontoon, METADATA_PARAMS(Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::NewProp_Pontoon_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::NewProp_Pontoon_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::NewProp_Pontoon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_Water, nullptr, "OnPontoonEnteredWater__DelegateSignature", nullptr, nullptr, sizeof(_Script_Water_eventOnPontoonEnteredWater_Parms), Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UBuoyancyComponent::execGetLastWaterSurfaceInfo)
	{
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutWaterPlaneLocation);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutWaterPlaneNormal);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutWaterSurfacePosition);
		P_GET_PROPERTY_REF(FFloatProperty,Z_Param_Out_OutWaterDepth);
		P_GET_PROPERTY_REF(FIntProperty,Z_Param_Out_OutWaterBodyIdx);
		P_GET_STRUCT_REF(FVector,Z_Param_Out_OutWaterVelocity);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetLastWaterSurfaceInfo(Z_Param_Out_OutWaterPlaneLocation,Z_Param_Out_OutWaterPlaneNormal,Z_Param_Out_OutWaterSurfacePosition,Z_Param_Out_OutWaterDepth,Z_Param_Out_OutWaterBodyIdx,Z_Param_Out_OutWaterVelocity);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuoyancyComponent::execOnPontoonExitedWater)
	{
		P_GET_STRUCT_REF(FSphericalPontoon,Z_Param_Out_Pontoon);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPontoonExitedWater(Z_Param_Out_Pontoon);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuoyancyComponent::execOnPontoonEnteredWater)
	{
		P_GET_STRUCT_REF(FSphericalPontoon,Z_Param_Out_Pontoon);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPontoonEnteredWater(Z_Param_Out_Pontoon);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UBuoyancyComponent::execIsInWaterBody)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsInWaterBody();
		P_NATIVE_END;
	}
	void UBuoyancyComponent::StaticRegisterNativesUBuoyancyComponent()
	{
		UClass* Class = UBuoyancyComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetLastWaterSurfaceInfo", &UBuoyancyComponent::execGetLastWaterSurfaceInfo },
			{ "IsInWaterBody", &UBuoyancyComponent::execIsInWaterBody },
			{ "OnPontoonEnteredWater", &UBuoyancyComponent::execOnPontoonEnteredWater },
			{ "OnPontoonExitedWater", &UBuoyancyComponent::execOnPontoonExitedWater },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics
	{
		struct BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms
		{
			FVector OutWaterPlaneLocation;
			FVector OutWaterPlaneNormal;
			FVector OutWaterSurfacePosition;
			float OutWaterDepth;
			int32 OutWaterBodyIdx;
			FVector OutWaterVelocity;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutWaterPlaneLocation;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutWaterPlaneNormal;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutWaterSurfacePosition;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OutWaterDepth;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OutWaterBodyIdx;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutWaterVelocity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterPlaneLocation = { "OutWaterPlaneLocation", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms, OutWaterPlaneLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterPlaneNormal = { "OutWaterPlaneNormal", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms, OutWaterPlaneNormal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterSurfacePosition = { "OutWaterSurfacePosition", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms, OutWaterSurfacePosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterDepth = { "OutWaterDepth", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms, OutWaterDepth), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterBodyIdx = { "OutWaterBodyIdx", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms, OutWaterBodyIdx), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterVelocity = { "OutWaterVelocity", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms, OutWaterVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterPlaneLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterPlaneNormal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterSurfacePosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterBodyIdx,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::NewProp_OutWaterVelocity,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuoyancyComponent, nullptr, "GetLastWaterSurfaceInfo", nullptr, nullptr, sizeof(BuoyancyComponent_eventGetLastWaterSurfaceInfo_Parms), Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04C20401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics
	{
		struct BuoyancyComponent_eventIsInWaterBody_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BuoyancyComponent_eventIsInWaterBody_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BuoyancyComponent_eventIsInWaterBody_Parms), &Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::Function_MetaDataParams[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuoyancyComponent, nullptr, "IsInWaterBody", nullptr, nullptr, sizeof(BuoyancyComponent_eventIsInWaterBody_Parms), Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics
	{
		struct BuoyancyComponent_eventOnPontoonEnteredWater_Parms
		{
			FSphericalPontoon Pontoon;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pontoon_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pontoon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::NewProp_Pontoon_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::NewProp_Pontoon = { "Pontoon", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventOnPontoonEnteredWater_Parms, Pontoon), Z_Construct_UScriptStruct_FSphericalPontoon, METADATA_PARAMS(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::NewProp_Pontoon_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::NewProp_Pontoon_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::NewProp_Pontoon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cosmetic" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuoyancyComponent, nullptr, "OnPontoonEnteredWater", nullptr, nullptr, sizeof(BuoyancyComponent_eventOnPontoonEnteredWater_Parms), Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics
	{
		struct BuoyancyComponent_eventOnPontoonExitedWater_Parms
		{
			FSphericalPontoon Pontoon;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pontoon_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pontoon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::NewProp_Pontoon_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::NewProp_Pontoon = { "Pontoon", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyComponent_eventOnPontoonExitedWater_Parms, Pontoon), Z_Construct_UScriptStruct_FSphericalPontoon, METADATA_PARAMS(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::NewProp_Pontoon_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::NewProp_Pontoon_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::NewProp_Pontoon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::Function_MetaDataParams[] = {
		{ "Category", "Cosmetic" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBuoyancyComponent, nullptr, "OnPontoonExitedWater", nullptr, nullptr, sizeof(BuoyancyComponent_eventOnPontoonExitedWater_Parms), Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UBuoyancyComponent_NoRegister()
	{
		return UBuoyancyComponent::StaticClass();
	}
	struct Z_Construct_UClass_UBuoyancyComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pontoons_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pontoons_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Pontoons;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnEnteredWaterDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnEnteredWaterDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnExitedWaterDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnExitedWaterDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BuoyancyData;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentWaterBodies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWaterBodies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CurrentWaterBodies;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SimulatingComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SimulatingComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBuoyancyComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UBuoyancyComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UBuoyancyComponent_GetLastWaterSurfaceInfo, "GetLastWaterSurfaceInfo" }, // 4261584901
		{ &Z_Construct_UFunction_UBuoyancyComponent_IsInWaterBody, "IsInWaterBody" }, // 393277135
		{ &Z_Construct_UFunction_UBuoyancyComponent_OnPontoonEnteredWater, "OnPontoonEnteredWater" }, // 166057456
		{ &Z_Construct_UFunction_UBuoyancyComponent_OnPontoonExitedWater, "OnPontoonExitedWater" }, // 871110331
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuoyancyComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "IncludePath", "BuoyancyComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_Pontoons_Inner = { "Pontoons", nullptr, (EPropertyFlags)0x0000000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSphericalPontoon, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_Pontoons_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use BuoyancyData.Pontoons instead." },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_Pontoons = { "Pontoons", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuoyancyComponent, Pontoons_DEPRECATED), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_Pontoons_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_Pontoons_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnEnteredWaterDelegate_MetaData[] = {
		{ "Category", "Cosmetic" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnEnteredWaterDelegate = { "OnEnteredWaterDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuoyancyComponent, OnEnteredWaterDelegate), Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnEnteredWaterDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnEnteredWaterDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnExitedWaterDelegate_MetaData[] = {
		{ "Category", "Cosmetic" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnExitedWaterDelegate = { "OnExitedWaterDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuoyancyComponent, OnExitedWaterDelegate), Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnExitedWaterDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnExitedWaterDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_BuoyancyData_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_BuoyancyData = { "BuoyancyData", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuoyancyComponent, BuoyancyData), Z_Construct_UScriptStruct_FBuoyancyData, METADATA_PARAMS(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_BuoyancyData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_BuoyancyData_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_CurrentWaterBodies_Inner = { "CurrentWaterBodies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_CurrentWaterBodies_MetaData[] = {
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_CurrentWaterBodies = { "CurrentWaterBodies", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuoyancyComponent, CurrentWaterBodies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_CurrentWaterBodies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_CurrentWaterBodies_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_SimulatingComponent_MetaData[] = {
		{ "Comment", "// Primitive component that will be used for physics simulation.\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/BuoyancyComponent.h" },
		{ "ToolTip", "Primitive component that will be used for physics simulation." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_SimulatingComponent = { "SimulatingComponent", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UBuoyancyComponent, SimulatingComponent), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_SimulatingComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_SimulatingComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UBuoyancyComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_Pontoons_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_Pontoons,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnEnteredWaterDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_OnExitedWaterDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_BuoyancyData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_CurrentWaterBodies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_CurrentWaterBodies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UBuoyancyComponent_Statics::NewProp_SimulatingComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBuoyancyComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBuoyancyComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBuoyancyComponent_Statics::ClassParams = {
		&UBuoyancyComponent::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UBuoyancyComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UBuoyancyComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBuoyancyComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBuoyancyComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBuoyancyComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBuoyancyComponent, 3708666403);
	template<> WATER_API UClass* StaticClass<UBuoyancyComponent>()
	{
		return UBuoyancyComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBuoyancyComponent(Z_Construct_UClass_UBuoyancyComponent, &UBuoyancyComponent::StaticClass, TEXT("/Script/Water"), TEXT("UBuoyancyComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBuoyancyComponent);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UBuoyancyComponent)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
