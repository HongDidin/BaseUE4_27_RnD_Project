// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/GerstnerWaterWaveSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGerstnerWaterWaveSubsystem() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UGerstnerWaterWaveSubsystem_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UGerstnerWaterWaveSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UEngineSubsystem();
	UPackage* Z_Construct_UPackage__Script_Water();
// End Cross Module References
	void UGerstnerWaterWaveSubsystem::StaticRegisterNativesUGerstnerWaterWaveSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UGerstnerWaterWaveSubsystem_NoRegister()
	{
		return UGerstnerWaterWaveSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEngineSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** UGerstnerWaterWaveSubsystem manages all UGerstnerWaterWaves objects, regardless of which world they belong to (it's a UEngineSubsystem) */" },
		{ "IncludePath", "GerstnerWaterWaveSubsystem.h" },
		{ "ModuleRelativePath", "Public/GerstnerWaterWaveSubsystem.h" },
		{ "ToolTip", "UGerstnerWaterWaveSubsystem manages all UGerstnerWaterWaves objects, regardless of which world they belong to (it's a UEngineSubsystem)" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGerstnerWaterWaveSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics::ClassParams = {
		&UGerstnerWaterWaveSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGerstnerWaterWaveSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGerstnerWaterWaveSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGerstnerWaterWaveSubsystem, 2025310155);
	template<> WATER_API UClass* StaticClass<UGerstnerWaterWaveSubsystem>()
	{
		return UGerstnerWaterWaveSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGerstnerWaterWaveSubsystem(Z_Construct_UClass_UGerstnerWaterWaveSubsystem, &UGerstnerWaterWaveSubsystem::StaticClass, TEXT("/Script/Water"), TEXT("UGerstnerWaterWaveSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGerstnerWaterWaveSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
