// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/NiagaraDataInterfaceWater.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraDataInterfaceWater() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UNiagaraDataInterfaceWater_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UNiagaraDataInterfaceWater();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraDataInterface();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
// End Cross Module References
	void UNiagaraDataInterfaceWater::StaticRegisterNativesUNiagaraDataInterfaceWater()
	{
	}
	UClass* Z_Construct_UClass_UNiagaraDataInterfaceWater_NoRegister()
	{
		return UNiagaraDataInterfaceWater::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceBody_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceBody;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UNiagaraDataInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::Class_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "DisplayName", "Water" },
		{ "IncludePath", "NiagaraDataInterfaceWater.h" },
		{ "ModuleRelativePath", "Public/NiagaraDataInterfaceWater.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::NewProp_SourceBody_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/NiagaraDataInterfaceWater.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::NewProp_SourceBody = { "SourceBody", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNiagaraDataInterfaceWater, SourceBody), Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::NewProp_SourceBody_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::NewProp_SourceBody_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::NewProp_SourceBody,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraDataInterfaceWater>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::ClassParams = {
		&UNiagaraDataInterfaceWater::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraDataInterfaceWater()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraDataInterfaceWater_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraDataInterfaceWater, 1863605403);
	template<> WATER_API UClass* StaticClass<UNiagaraDataInterfaceWater>()
	{
		return UNiagaraDataInterfaceWater::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraDataInterfaceWater(Z_Construct_UClass_UNiagaraDataInterfaceWater, &UNiagaraDataInterfaceWater::StaticClass, TEXT("/Script/Water"), TEXT("UNiagaraDataInterfaceWater"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraDataInterfaceWater);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
