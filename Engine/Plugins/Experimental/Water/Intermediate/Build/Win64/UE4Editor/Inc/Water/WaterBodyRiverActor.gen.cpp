// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyRiverActor.h"
#include "Runtime/Public/WaterBodyActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyRiverActor() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_URiverGenerator_NoRegister();
	WATER_API UClass* Z_Construct_UClass_URiverGenerator();
	WATER_API UClass* Z_Construct_UClass_UWaterBodyGenerator();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UClass* Z_Construct_UClass_USplineMeshComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyRiver_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyRiver();
	WATER_API UClass* Z_Construct_UClass_AWaterBody();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
// End Cross Module References
	void URiverGenerator::StaticRegisterNativesURiverGenerator()
	{
	}
	UClass* Z_Construct_UClass_URiverGenerator_NoRegister()
	{
		return URiverGenerator::StaticClass();
	}
	struct Z_Construct_UClass_URiverGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SplineMeshComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SplineMeshComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SplineMeshComponents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URiverGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URiverGenerator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "IncludePath", "WaterBodyRiverActor.h" },
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URiverGenerator_Statics::NewProp_SplineMeshComponents_Inner = { "SplineMeshComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USplineMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URiverGenerator_Statics::NewProp_SplineMeshComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URiverGenerator_Statics::NewProp_SplineMeshComponents = { "SplineMeshComponents", nullptr, (EPropertyFlags)0x0040808000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URiverGenerator, SplineMeshComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URiverGenerator_Statics::NewProp_SplineMeshComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URiverGenerator_Statics::NewProp_SplineMeshComponents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URiverGenerator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URiverGenerator_Statics::NewProp_SplineMeshComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URiverGenerator_Statics::NewProp_SplineMeshComponents,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URiverGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URiverGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URiverGenerator_Statics::ClassParams = {
		&URiverGenerator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URiverGenerator_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URiverGenerator_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_URiverGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URiverGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URiverGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URiverGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URiverGenerator, 791163609);
	template<> WATER_API UClass* StaticClass<URiverGenerator>()
	{
		return URiverGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URiverGenerator(Z_Construct_UClass_URiverGenerator, &URiverGenerator::StaticClass, TEXT("/Script/Water"), TEXT("URiverGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URiverGenerator);
	void AWaterBodyRiver::StaticRegisterNativesAWaterBodyRiver()
	{
	}
	UClass* Z_Construct_UClass_AWaterBodyRiver_NoRegister()
	{
		return AWaterBodyRiver::StaticClass();
	}
	struct Z_Construct_UClass_AWaterBodyRiver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RiverGenerator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RiverGenerator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LakeTransitionMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LakeTransitionMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LakeTransitionMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LakeTransitionMID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OceanTransitionMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OceanTransitionMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OceanTransitionMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OceanTransitionMID;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterBodyRiver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AWaterBody,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyRiver_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "HideCategories", "Tags Activation Cooking Replication Input Actor AssetUserData" },
		{ "IncludePath", "WaterBodyRiverActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_RiverGenerator_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_RiverGenerator = { "RiverGenerator", nullptr, (EPropertyFlags)0x0020880000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyRiver, RiverGenerator), Z_Construct_UClass_URiverGenerator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_RiverGenerator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_RiverGenerator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Material used when a river is overlapping a lake. */" },
		{ "DisplayName", "River to Lake Transition" },
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
		{ "ToolTip", "Material used when a river is overlapping a lake." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMaterial = { "LakeTransitionMaterial", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyRiver, LakeTransitionMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMID_MetaData[] = {
		{ "Category", "Debug" },
		{ "DisplayAfter", "LakeTransitionMaterial" },
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMID = { "LakeTransitionMID", nullptr, (EPropertyFlags)0x0020c80000022801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyRiver, LakeTransitionMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** This is the material used when a river is overlapping the ocean. */" },
		{ "DisplayName", "River to Ocean Transition" },
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
		{ "ToolTip", "This is the material used when a river is overlapping the ocean." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMaterial = { "OceanTransitionMaterial", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyRiver, OceanTransitionMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMID_MetaData[] = {
		{ "Category", "Debug" },
		{ "DisplayAfter", "OceanTransitionMaterial" },
		{ "ModuleRelativePath", "Public/WaterBodyRiverActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMID = { "OceanTransitionMID", nullptr, (EPropertyFlags)0x0020c80000022801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyRiver, OceanTransitionMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMID_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterBodyRiver_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_RiverGenerator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_LakeTransitionMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyRiver_Statics::NewProp_OceanTransitionMID,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterBodyRiver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterBodyRiver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterBodyRiver_Statics::ClassParams = {
		&AWaterBodyRiver::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWaterBodyRiver_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyRiver_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterBodyRiver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyRiver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterBodyRiver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterBodyRiver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterBodyRiver, 4078215350);
	template<> WATER_API UClass* StaticClass<AWaterBodyRiver>()
	{
		return AWaterBodyRiver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterBodyRiver(Z_Construct_UClass_AWaterBodyRiver, &AWaterBodyRiver::StaticClass, TEXT("/Script/Water"), TEXT("AWaterBodyRiver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterBodyRiver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
