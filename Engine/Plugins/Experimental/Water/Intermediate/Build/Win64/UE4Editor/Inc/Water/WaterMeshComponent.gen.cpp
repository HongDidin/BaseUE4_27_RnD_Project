// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterMeshComponent() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UWaterMeshComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterMeshComponent();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent();
	UPackage* Z_Construct_UPackage__Script_Water();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	DEFINE_FUNCTION(UWaterMeshComponent::execIsEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsEnabled();
		P_NATIVE_END;
	}
	void UWaterMeshComponent::StaticRegisterNativesUWaterMeshComponent()
	{
		UClass* Class = UWaterMeshComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "IsEnabled", &UWaterMeshComponent::execIsEnabled },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics
	{
		struct WaterMeshComponent_eventIsEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((WaterMeshComponent_eventIsEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WaterMeshComponent_eventIsEnabled_Parms), &Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "Mesh" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UWaterMeshComponent, nullptr, "IsEnabled", nullptr, nullptr, sizeof(WaterMeshComponent_eventIsEnabled_Parms), Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UWaterMeshComponent_IsEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UWaterMeshComponent_IsEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UWaterMeshComponent_NoRegister()
	{
		return UWaterMeshComponent::StaticClass();
	}
	struct Z_Construct_UClass_UWaterMeshComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ForceCollapseDensityLevel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ForceCollapseDensityLevel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TileSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TileSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtentInTiles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExtentInTiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FarDistanceMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FarDistanceMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FarDistanceMeshExtent_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FarDistanceMeshExtent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RTWorldLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RTWorldLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RTWorldSizeVector_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RTWorldSizeVector;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UsedMaterials_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UsedMaterials_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_UsedMaterials;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TessellationFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TessellationFactor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_LODScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterMeshComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UWaterMeshComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UWaterMeshComponent_IsEnabled, "IsEnabled" }, // 2746490938
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Rendering Water" },
		{ "Comment", "/**\n * Water Mesh Component responsible for generating and rendering a continuous water mesh on top of all the existing water body actors in the world\n * The component contains a quadtree which defines where there are water tiles. A function for traversing the quadtree and outputing a list of instance data for each tile to be rendered from a point of view is included\n */" },
		{ "HideCategories", "Object Activation Components|Activation Mobility Trigger" },
		{ "IncludePath", "WaterMeshComponent.h" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
		{ "ToolTip", "Water Mesh Component responsible for generating and rendering a continuous water mesh on top of all the existing water body actors in the world\nThe component contains a quadtree which defines where there are water tiles. A function for traversing the quadtree and outputing a list of instance data for each tile to be rendered from a point of view is included" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ForceCollapseDensityLevel_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ClampMin", "-1" },
		{ "Comment", "/** At above what density level a tile is allowed to force collapse even if not all leaf nodes in the subtree are present.\n\x09 *\x09""Collapsing will not occus if any child node in the subtree has different materials.\n\x09 *\x09Setting this to -1 means no collapsing is allowed and the water mesh will always keep it's silhouette at any distance.\n\x09 *\x09Setting this to 0 will allow every level to collapse\n\x09 *\x09Setting this to something higher than the LODCount will have no effect\n\x09 */" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
		{ "ToolTip", "At above what density level a tile is allowed to force collapse even if not all leaf nodes in the subtree are present.\n    Collapsing will not occus if any child node in the subtree has different materials.\n    Setting this to -1 means no collapsing is allowed and the water mesh will always keep it's silhouette at any distance.\n    Setting this to 0 will allow every level to collapse\n    Setting this to something higher than the LODCount will have no effect" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ForceCollapseDensityLevel = { "ForceCollapseDensityLevel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, ForceCollapseDensityLevel), METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ForceCollapseDensityLevel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ForceCollapseDensityLevel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TileSize_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ClampMin", "100" },
		{ "Comment", "/** World size of the water tiles at LOD0. Multiply this with the ExtentInTiles to get the world extents of the system */" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
		{ "ToolTip", "World size of the water tiles at LOD0. Multiply this with the ExtentInTiles to get the world extents of the system" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TileSize = { "TileSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, TileSize), METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TileSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TileSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ExtentInTiles_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ClampMin", "1" },
		{ "Comment", "/** The extent of the system in number of tiles. Maximum number of tiles for this system will be ExtentInTiles.X*2*ExtentInTiles.Y*2 */" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
		{ "ToolTip", "The extent of the system in number of tiles. Maximum number of tiles for this system will be ExtentInTiles.X*2*ExtentInTiles.Y*2" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ExtentInTiles = { "ExtentInTiles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, ExtentInTiles), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ExtentInTiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ExtentInTiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMaterial_MetaData[] = {
		{ "Category", "Mesh|FarDistance" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMaterial = { "FarDistanceMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, FarDistanceMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMeshExtent_MetaData[] = {
		{ "Category", "Mesh|FarDistance" },
		{ "ClampMin", "0" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMeshExtent = { "FarDistanceMeshExtent", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, FarDistanceMeshExtent), METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMeshExtent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMeshExtent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldLocation_MetaData[] = {
		{ "Category", "Texture" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldLocation = { "RTWorldLocation", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, RTWorldLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldSizeVector_MetaData[] = {
		{ "Category", "Texture" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldSizeVector = { "RTWorldSizeVector", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, RTWorldSizeVector), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldSizeVector_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldSizeVector_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_UsedMaterials_ElementProp = { "UsedMaterials", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_UsedMaterials_MetaData[] = {
		{ "Comment", "/** Unique list of materials used by this component */" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
		{ "ToolTip", "Unique list of materials used by this component" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_UsedMaterials = { "UsedMaterials", nullptr, (EPropertyFlags)0x0040c00000002000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, UsedMaterials), METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_UsedMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_UsedMaterials_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TessellationFactor_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ClampMax", "12" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Highest tessellation factor of a water tile. Max number of verts on the side of a tile will be (2^TessellationFactor)+1)  */" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
		{ "ToolTip", "Highest tessellation factor of a water tile. Max number of verts on the side of a tile will be (2^TessellationFactor)+1)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TessellationFactor = { "TessellationFactor", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, TessellationFactor), METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TessellationFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TessellationFactor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_LODScale_MetaData[] = {
		{ "Category", "Mesh" },
		{ "ClampMin", "0.5" },
		{ "Comment", "/** World scale of the concentric LODs */" },
		{ "ModuleRelativePath", "Public/WaterMeshComponent.h" },
		{ "ToolTip", "World scale of the concentric LODs" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_LODScale = { "LODScale", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterMeshComponent, LODScale), METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_LODScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_LODScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterMeshComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ForceCollapseDensityLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TileSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_ExtentInTiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_FarDistanceMeshExtent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_RTWorldSizeVector,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_UsedMaterials_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_UsedMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_TessellationFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterMeshComponent_Statics::NewProp_LODScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterMeshComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterMeshComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterMeshComponent_Statics::ClassParams = {
		&UWaterMeshComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UWaterMeshComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterMeshComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterMeshComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterMeshComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterMeshComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterMeshComponent, 3546448515);
	template<> WATER_API UClass* StaticClass<UWaterMeshComponent>()
	{
		return UWaterMeshComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterMeshComponent(Z_Construct_UClass_UWaterMeshComponent, &UWaterMeshComponent::StaticClass, TEXT("/Script/Water"), TEXT("UWaterMeshComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterMeshComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
