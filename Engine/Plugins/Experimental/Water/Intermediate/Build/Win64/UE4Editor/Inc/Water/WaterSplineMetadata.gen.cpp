// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterSplineMetadata.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterSplineMetadata() {}
// Cross Module References
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterSplineCurveDefaults();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UClass* Z_Construct_UClass_UWaterSplineMetadata_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterSplineMetadata();
	ENGINE_API UClass* Z_Construct_UClass_USplineMetadata();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FInterpCurveFloat();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FInterpCurveVector();
// End Cross Module References
class UScriptStruct* FWaterSplineCurveDefaults::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults, Z_Construct_UPackage__Script_Water(), TEXT("WaterSplineCurveDefaults"), sizeof(FWaterSplineCurveDefaults), Get_Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterSplineCurveDefaults>()
{
	return FWaterSplineCurveDefaults::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterSplineCurveDefaults(FWaterSplineCurveDefaults::StaticStruct, TEXT("/Script/Water"), TEXT("WaterSplineCurveDefaults"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterSplineCurveDefaults
{
	FScriptStruct_Water_StaticRegisterNativesFWaterSplineCurveDefaults()
	{
		UScriptStruct::DeferCppStructOps<FWaterSplineCurveDefaults>(FName(TEXT("WaterSplineCurveDefaults")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterSplineCurveDefaults;
	struct Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultAudioIntensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DefaultAudioIntensity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterSplineCurveDefaults>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultDepth_MetaData[] = {
		{ "Category", "WaterCurveDefaults" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultDepth = { "DefaultDepth", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterSplineCurveDefaults, DefaultDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultWidth_MetaData[] = {
		{ "Category", "WaterCurveDefaults" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultWidth = { "DefaultWidth", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterSplineCurveDefaults, DefaultWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultVelocity_MetaData[] = {
		{ "Category", "WaterCurveDefaults" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultVelocity = { "DefaultVelocity", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterSplineCurveDefaults, DefaultVelocity), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultAudioIntensity_MetaData[] = {
		{ "Category", "WaterCurveDefaults" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultAudioIntensity = { "DefaultAudioIntensity", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterSplineCurveDefaults, DefaultAudioIntensity), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultAudioIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultAudioIntensity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::NewProp_DefaultAudioIntensity,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterSplineCurveDefaults",
		sizeof(FWaterSplineCurveDefaults),
		alignof(FWaterSplineCurveDefaults),
		Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterSplineCurveDefaults()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterSplineCurveDefaults"), sizeof(FWaterSplineCurveDefaults), Get_Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterSplineCurveDefaults_Hash() { return 1697047633U; }
	void UWaterSplineMetadata::StaticRegisterNativesUWaterSplineMetadata()
	{
	}
	UClass* Z_Construct_UClass_UWaterSplineMetadata_NoRegister()
	{
		return UWaterSplineMetadata::StaticClass();
	}
	struct Z_Construct_UClass_UWaterSplineMetadata_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Depth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterVelocityScalar_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterVelocityScalar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RiverWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RiverWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AudioIntensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AudioIntensity;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldVisualizeWaterVelocity_MetaData[];
#endif
		static void NewProp_bShouldVisualizeWaterVelocity_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldVisualizeWaterVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldVisualizeRiverWidth_MetaData[];
#endif
		static void NewProp_bShouldVisualizeRiverWidth_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldVisualizeRiverWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShouldVisualizeDepth_MetaData[];
#endif
		static void NewProp_bShouldVisualizeDepth_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShouldVisualizeDepth;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterVelocity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterSplineMetadata_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USplineMetadata,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WaterSplineMetadata.h" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterSplineMetadata, Depth), Z_Construct_UScriptStruct_FInterpCurveFloat, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_Depth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocityScalar_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** The Current of the water at this vertex.  Magnitude and direction */" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
		{ "ToolTip", "The Current of the water at this vertex.  Magnitude and direction" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocityScalar = { "WaterVelocityScalar", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterSplineMetadata, WaterVelocityScalar), Z_Construct_UScriptStruct_FInterpCurveFloat, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocityScalar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocityScalar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_RiverWidth_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Rivers Only: The width of the river (from center) in each direction  */" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
		{ "ToolTip", "Rivers Only: The width of the river (from center) in each direction" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_RiverWidth = { "RiverWidth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterSplineMetadata, RiverWidth), Z_Construct_UScriptStruct_FInterpCurveFloat, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_RiverWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_RiverWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_AudioIntensity_MetaData[] = {
		{ "Category", "Water|Audio" },
		{ "Comment", "/** A scalar used to define intensity of the water audio along the spline */" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
		{ "ToolTip", "A scalar used to define intensity of the water audio along the spline" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_AudioIntensity = { "AudioIntensity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterSplineMetadata, AudioIntensity), Z_Construct_UScriptStruct_FInterpCurveFloat, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_AudioIntensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_AudioIntensity_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeWaterVelocity_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Whether water velocity visualization should be displayed */" },
		{ "InlineEditConditionToggle", "TRUE" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
		{ "ToolTip", "Whether water velocity visualization should be displayed" },
	};
#endif
	void Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeWaterVelocity_SetBit(void* Obj)
	{
		((UWaterSplineMetadata*)Obj)->bShouldVisualizeWaterVelocity = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeWaterVelocity = { "bShouldVisualizeWaterVelocity", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UWaterSplineMetadata), &Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeWaterVelocity_SetBit, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeWaterVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeWaterVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeRiverWidth_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Whether river width visualization should be displayed */" },
		{ "InlineEditConditionToggle", "TRUE" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
		{ "ToolTip", "Whether river width visualization should be displayed" },
	};
#endif
	void Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeRiverWidth_SetBit(void* Obj)
	{
		((UWaterSplineMetadata*)Obj)->bShouldVisualizeRiverWidth = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeRiverWidth = { "bShouldVisualizeRiverWidth", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UWaterSplineMetadata), &Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeRiverWidth_SetBit, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeRiverWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeRiverWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeDepth_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Whether depth visualization should be displayed */" },
		{ "InlineEditConditionToggle", "TRUE" },
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
		{ "ToolTip", "Whether depth visualization should be displayed" },
	};
#endif
	void Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeDepth_SetBit(void* Obj)
	{
		((UWaterSplineMetadata*)Obj)->bShouldVisualizeDepth = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeDepth = { "bShouldVisualizeDepth", nullptr, (EPropertyFlags)0x0010000800000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UWaterSplineMetadata), &Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeDepth_SetBit, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeDepth_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocity_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterSplineMetadata.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocity = { "WaterVelocity", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterSplineMetadata, WaterVelocity_DEPRECATED), Z_Construct_UScriptStruct_FInterpCurveVector, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterSplineMetadata_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_Depth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocityScalar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_RiverWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_AudioIntensity,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeWaterVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeRiverWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_bShouldVisualizeDepth,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineMetadata_Statics::NewProp_WaterVelocity,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterSplineMetadata_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterSplineMetadata>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterSplineMetadata_Statics::ClassParams = {
		&UWaterSplineMetadata::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterSplineMetadata_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterSplineMetadata_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineMetadata_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterSplineMetadata()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterSplineMetadata_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterSplineMetadata, 1762860328);
	template<> WATER_API UClass* StaticClass<UWaterSplineMetadata>()
	{
		return UWaterSplineMetadata::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterSplineMetadata(Z_Construct_UClass_UWaterSplineMetadata, &UWaterSplineMetadata::StaticClass, TEXT("/Script/Water"), TEXT("UWaterSplineMetadata"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterSplineMetadata);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
