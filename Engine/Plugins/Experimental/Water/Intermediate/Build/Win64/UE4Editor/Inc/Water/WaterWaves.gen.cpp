// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterWaves.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterWaves() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UWaterWavesBase_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterWavesBase();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UClass* Z_Construct_UClass_UWaterWaves_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterWaves();
	WATER_API UClass* Z_Construct_UClass_UWaterWavesAsset_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterWavesAsset();
	WATER_API UClass* Z_Construct_UClass_UWaterWavesAssetReference_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterWavesAssetReference();
// End Cross Module References
	void UWaterWavesBase::StaticRegisterNativesUWaterWavesBase()
	{
	}
	UClass* Z_Construct_UClass_UWaterWavesBase_NoRegister()
	{
		return UWaterWavesBase::StaticClass();
	}
	struct Z_Construct_UClass_UWaterWavesBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterWavesBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterWavesBase_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WaterWaves.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/WaterWaves.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterWavesBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterWavesBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterWavesBase_Statics::ClassParams = {
		&UWaterWavesBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterWavesBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterWavesBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterWavesBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterWavesBase, 686078875);
	template<> WATER_API UClass* StaticClass<UWaterWavesBase>()
	{
		return UWaterWavesBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterWavesBase(Z_Construct_UClass_UWaterWavesBase, &UWaterWavesBase::StaticClass, TEXT("/Script/Water"), TEXT("UWaterWavesBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterWavesBase);
	void UWaterWaves::StaticRegisterNativesUWaterWaves()
	{
	}
	UClass* Z_Construct_UClass_UWaterWaves_NoRegister()
	{
		return UWaterWaves::StaticClass();
	}
	struct Z_Construct_UClass_UWaterWaves_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterWaves_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterWavesBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterWaves_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WaterWaves.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/WaterWaves.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterWaves_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterWaves>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterWaves_Statics::ClassParams = {
		&UWaterWaves::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterWaves_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWaves_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterWaves()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterWaves_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterWaves, 1479639499);
	template<> WATER_API UClass* StaticClass<UWaterWaves>()
	{
		return UWaterWaves::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterWaves(Z_Construct_UClass_UWaterWaves, &UWaterWaves::StaticClass, TEXT("/Script/Water"), TEXT("UWaterWaves"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterWaves);
	void UWaterWavesAsset::StaticRegisterNativesUWaterWavesAsset()
	{
	}
	UClass* Z_Construct_UClass_UWaterWavesAsset_NoRegister()
	{
		return UWaterWavesAsset::StaticClass();
	}
	struct Z_Construct_UClass_UWaterWavesAsset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterWaves_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterWaves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterWavesAsset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterWavesAsset_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "Water Waves" },
		{ "IncludePath", "WaterWaves.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/WaterWaves.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterWavesAsset_Statics::NewProp_WaterWaves_MetaData[] = {
		{ "Category", "Water Waves" },
		{ "DisplayName", "Waves Source" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterWaves.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterWavesAsset_Statics::NewProp_WaterWaves = { "WaterWaves", nullptr, (EPropertyFlags)0x0042000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterWavesAsset, WaterWaves), Z_Construct_UClass_UWaterWaves_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterWavesAsset_Statics::NewProp_WaterWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesAsset_Statics::NewProp_WaterWaves_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterWavesAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterWavesAsset_Statics::NewProp_WaterWaves,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterWavesAsset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterWavesAsset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterWavesAsset_Statics::ClassParams = {
		&UWaterWavesAsset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterWavesAsset_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesAsset_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterWavesAsset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesAsset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterWavesAsset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterWavesAsset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterWavesAsset, 2494585009);
	template<> WATER_API UClass* StaticClass<UWaterWavesAsset>()
	{
		return UWaterWavesAsset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterWavesAsset(Z_Construct_UClass_UWaterWavesAsset, &UWaterWavesAsset::StaticClass, TEXT("/Script/Water"), TEXT("UWaterWavesAsset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterWavesAsset);
	void UWaterWavesAssetReference::StaticRegisterNativesUWaterWavesAssetReference()
	{
	}
	UClass* Z_Construct_UClass_UWaterWavesAssetReference_NoRegister()
	{
		return UWaterWavesAssetReference::StaticClass();
	}
	struct Z_Construct_UClass_UWaterWavesAssetReference_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterWavesAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterWavesAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterWavesAssetReference_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterWavesBase,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterWavesAssetReference_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "Water Waves Asset" },
		{ "IncludePath", "WaterWaves.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Public/WaterWaves.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterWavesAssetReference_Statics::NewProp_WaterWavesAsset_MetaData[] = {
		{ "Category", "Water Waves Asset" },
		{ "ModuleRelativePath", "Public/WaterWaves.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterWavesAssetReference_Statics::NewProp_WaterWavesAsset = { "WaterWavesAsset", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterWavesAssetReference, WaterWavesAsset), Z_Construct_UClass_UWaterWavesAsset_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterWavesAssetReference_Statics::NewProp_WaterWavesAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesAssetReference_Statics::NewProp_WaterWavesAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterWavesAssetReference_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterWavesAssetReference_Statics::NewProp_WaterWavesAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterWavesAssetReference_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterWavesAssetReference>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterWavesAssetReference_Statics::ClassParams = {
		&UWaterWavesAssetReference::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterWavesAssetReference_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesAssetReference_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterWavesAssetReference_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesAssetReference_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterWavesAssetReference()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterWavesAssetReference_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterWavesAssetReference, 1577678699);
	template<> WATER_API UClass* StaticClass<UWaterWavesAssetReference>()
	{
		return UWaterWavesAssetReference::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterWavesAssetReference(Z_Construct_UClass_UWaterWavesAssetReference, &UWaterWavesAssetReference::StaticClass, TEXT("/Script/Water"), TEXT("UWaterWavesAssetReference"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterWavesAssetReference);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
