// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/OceanCollisionComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeOceanCollisionComponent() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UOceanCollisionComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UOceanCollisionComponent();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UClass* Z_Construct_UClass_UBodySetup_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UOceanBoxCollisionComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UOceanBoxCollisionComponent();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent();
// End Cross Module References
	void UOceanCollisionComponent::StaticRegisterNativesUOceanCollisionComponent()
	{
	}
	UClass* Z_Construct_UClass_UOceanCollisionComponent_NoRegister()
	{
		return UOceanCollisionComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOceanCollisionComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedBodySetup_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedBodySetup;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOceanCollisionComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPrimitiveComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOceanCollisionComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Mobility VirtualTexture Trigger" },
		{ "IncludePath", "OceanCollisionComponent.h" },
		{ "ModuleRelativePath", "Public/OceanCollisionComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOceanCollisionComponent_Statics::NewProp_CachedBodySetup_MetaData[] = {
		{ "ModuleRelativePath", "Public/OceanCollisionComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOceanCollisionComponent_Statics::NewProp_CachedBodySetup = { "CachedBodySetup", nullptr, (EPropertyFlags)0x0040800000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOceanCollisionComponent, CachedBodySetup), Z_Construct_UClass_UBodySetup_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UOceanCollisionComponent_Statics::NewProp_CachedBodySetup_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOceanCollisionComponent_Statics::NewProp_CachedBodySetup_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOceanCollisionComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOceanCollisionComponent_Statics::NewProp_CachedBodySetup,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOceanCollisionComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOceanCollisionComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOceanCollisionComponent_Statics::ClassParams = {
		&UOceanCollisionComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOceanCollisionComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOceanCollisionComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOceanCollisionComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOceanCollisionComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOceanCollisionComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOceanCollisionComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOceanCollisionComponent, 1779496462);
	template<> WATER_API UClass* StaticClass<UOceanCollisionComponent>()
	{
		return UOceanCollisionComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOceanCollisionComponent(Z_Construct_UClass_UOceanCollisionComponent, &UOceanCollisionComponent::StaticClass, TEXT("/Script/Water"), TEXT("UOceanCollisionComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOceanCollisionComponent);
	void UOceanBoxCollisionComponent::StaticRegisterNativesUOceanBoxCollisionComponent()
	{
	}
	UClass* Z_Construct_UClass_UOceanBoxCollisionComponent_NoRegister()
	{
		return UOceanBoxCollisionComponent::StaticClass();
	}
	struct Z_Construct_UClass_UOceanBoxCollisionComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOceanBoxCollisionComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBoxComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOceanBoxCollisionComponent_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object LOD Lighting TextureStreaming Object LOD Lighting TextureStreaming Activation Components|Activation Trigger VirtualTexture" },
		{ "IncludePath", "OceanCollisionComponent.h" },
		{ "ModuleRelativePath", "Public/OceanCollisionComponent.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOceanBoxCollisionComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOceanBoxCollisionComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOceanBoxCollisionComponent_Statics::ClassParams = {
		&UOceanBoxCollisionComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UOceanBoxCollisionComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOceanBoxCollisionComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOceanBoxCollisionComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOceanBoxCollisionComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOceanBoxCollisionComponent, 2116944525);
	template<> WATER_API UClass* StaticClass<UOceanBoxCollisionComponent>()
	{
		return UOceanBoxCollisionComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOceanBoxCollisionComponent(Z_Construct_UClass_UOceanBoxCollisionComponent, &UOceanBoxCollisionComponent::StaticClass, TEXT("/Script/Water"), TEXT("UOceanBoxCollisionComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOceanBoxCollisionComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
