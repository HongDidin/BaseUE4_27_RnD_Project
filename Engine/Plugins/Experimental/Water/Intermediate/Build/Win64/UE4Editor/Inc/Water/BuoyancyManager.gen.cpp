// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/BuoyancyManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuoyancyManager() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_ABuoyancyManager_NoRegister();
	WATER_API UClass* Z_Construct_UClass_ABuoyancyManager();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Water();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UBuoyancyComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ABuoyancyManager::execGetBuoyancyComponentManager)
	{
		P_GET_OBJECT(UObject,Z_Param_WorldContextObject);
		P_GET_OBJECT_REF(ABuoyancyManager,Z_Param_Out_Manager);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=ABuoyancyManager::GetBuoyancyComponentManager(Z_Param_WorldContextObject,Z_Param_Out_Manager);
		P_NATIVE_END;
	}
	void ABuoyancyManager::StaticRegisterNativesABuoyancyManager()
	{
		UClass* Class = ABuoyancyManager::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetBuoyancyComponentManager", &ABuoyancyManager::execGetBuoyancyComponentManager },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics
	{
		struct BuoyancyManager_eventGetBuoyancyComponentManager_Parms
		{
			const UObject* WorldContextObject;
			ABuoyancyManager* Manager;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WorldContextObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WorldContextObject;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Manager;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_WorldContextObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_WorldContextObject = { "WorldContextObject", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyManager_eventGetBuoyancyComponentManager_Parms, WorldContextObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_WorldContextObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_WorldContextObject_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_Manager = { "Manager", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BuoyancyManager_eventGetBuoyancyComponentManager_Parms, Manager), Z_Construct_UClass_ABuoyancyManager_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BuoyancyManager_eventGetBuoyancyComponentManager_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BuoyancyManager_eventGetBuoyancyComponentManager_Parms), &Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_WorldContextObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_Manager,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/BuoyancyManager.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABuoyancyManager, nullptr, "GetBuoyancyComponentManager", nullptr, nullptr, sizeof(BuoyancyManager_eventGetBuoyancyComponentManager_Parms), Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABuoyancyManager_NoRegister()
	{
		return ABuoyancyManager::StaticClass();
	}
	struct Z_Construct_UClass_ABuoyancyManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BuoyancyComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BuoyancyComponents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABuoyancyManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABuoyancyManager_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABuoyancyManager_GetBuoyancyComponentManager, "GetBuoyancyComponentManager" }, // 68135579
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuoyancyManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BuoyancyManager.h" },
		{ "ModuleRelativePath", "Public/BuoyancyManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABuoyancyManager_Statics::NewProp_BuoyancyComponents_Inner = { "BuoyancyComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UBuoyancyComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABuoyancyManager_Statics::NewProp_BuoyancyComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/BuoyancyManager.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ABuoyancyManager_Statics::NewProp_BuoyancyComponents = { "BuoyancyComponents", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABuoyancyManager, BuoyancyComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ABuoyancyManager_Statics::NewProp_BuoyancyComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABuoyancyManager_Statics::NewProp_BuoyancyComponents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABuoyancyManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABuoyancyManager_Statics::NewProp_BuoyancyComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABuoyancyManager_Statics::NewProp_BuoyancyComponents,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABuoyancyManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABuoyancyManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABuoyancyManager_Statics::ClassParams = {
		&ABuoyancyManager::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABuoyancyManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABuoyancyManager_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABuoyancyManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABuoyancyManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABuoyancyManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABuoyancyManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABuoyancyManager, 966789193);
	template<> WATER_API UClass* StaticClass<ABuoyancyManager>()
	{
		return ABuoyancyManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABuoyancyManager(Z_Construct_UClass_ABuoyancyManager, &ABuoyancyManager::StaticClass, TEXT("/Script/Water"), TEXT("ABuoyancyManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABuoyancyManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
