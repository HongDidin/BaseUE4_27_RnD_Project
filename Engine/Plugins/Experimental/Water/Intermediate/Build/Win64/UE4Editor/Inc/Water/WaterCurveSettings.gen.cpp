// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterCurveSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterCurveSettings() {}
// Cross Module References
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterCurveSettings();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
// End Cross Module References
class UScriptStruct* FWaterCurveSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterCurveSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterCurveSettings, Z_Construct_UPackage__Script_Water(), TEXT("WaterCurveSettings"), sizeof(FWaterCurveSettings), Get_Z_Construct_UScriptStruct_FWaterCurveSettings_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterCurveSettings>()
{
	return FWaterCurveSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterCurveSettings(FWaterCurveSettings::StaticStruct, TEXT("/Script/Water"), TEXT("WaterCurveSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterCurveSettings
{
	FScriptStruct_Water_StaticRegisterNativesFWaterCurveSettings()
	{
		UScriptStruct::DeferCppStructOps<FWaterCurveSettings>(FName(TEXT("WaterCurveSettings")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterCurveSettings;
	struct Z_Construct_UScriptStruct_FWaterCurveSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCurveChannel_MetaData[];
#endif
		static void NewProp_bUseCurveChannel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCurveChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElevationCurveAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ElevationCurveAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelEdgeOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChannelEdgeOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChannelDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveRampWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurveRampWidth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterCurveSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterCurveSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_bUseCurveChannel_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/WaterCurveSettings.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_bUseCurveChannel_SetBit(void* Obj)
	{
		((FWaterCurveSettings*)Obj)->bUseCurveChannel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_bUseCurveChannel = { "bUseCurveChannel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FWaterCurveSettings), &Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_bUseCurveChannel_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_bUseCurveChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_bUseCurveChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ElevationCurveAsset_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/WaterCurveSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ElevationCurveAsset = { "ElevationCurveAsset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterCurveSettings, ElevationCurveAsset), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ElevationCurveAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ElevationCurveAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelEdgeOffset_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/WaterCurveSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelEdgeOffset = { "ChannelEdgeOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterCurveSettings, ChannelEdgeOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelEdgeOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelEdgeOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelDepth_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/WaterCurveSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelDepth = { "ChannelDepth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterCurveSettings, ChannelDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_CurveRampWidth_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/WaterCurveSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_CurveRampWidth = { "CurveRampWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterCurveSettings, CurveRampWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_CurveRampWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_CurveRampWidth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_bUseCurveChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ElevationCurveAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelEdgeOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_ChannelDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::NewProp_CurveRampWidth,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterCurveSettings",
		sizeof(FWaterCurveSettings),
		alignof(FWaterCurveSettings),
		Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterCurveSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterCurveSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterCurveSettings"), sizeof(FWaterCurveSettings), Get_Z_Construct_UScriptStruct_FWaterCurveSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterCurveSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterCurveSettings_Hash() { return 726950336U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
