// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterMeshActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterMeshActor() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_AWaterMeshActor_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterMeshActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBillboardComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterMeshComponent_NoRegister();
// End Cross Module References
	void AWaterMeshActor::StaticRegisterNativesAWaterMeshActor()
	{
	}
	UClass* Z_Construct_UClass_AWaterMeshActor_NoRegister()
	{
		return AWaterMeshActor::StaticClass();
	}
	struct Z_Construct_UClass_AWaterMeshActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterVelocityTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterVelocityTexture;
#if WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_SelectedWaterBodies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedWaterBodies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectedWaterBodies;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorIcon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorIcon;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterMeshActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterMeshActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "WaterMeshActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/WaterMeshActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterVelocityTexture_MetaData[] = {
		{ "Category", "Texture" },
		{ "ModuleRelativePath", "Public/WaterMeshActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterVelocityTexture = { "WaterVelocityTexture", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterMeshActor, WaterVelocityTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterVelocityTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterVelocityTexture_MetaData)) };
#if WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_SelectedWaterBodies_Inner = { "SelectedWaterBodies", nullptr, (EPropertyFlags)0x0004000800000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_SelectedWaterBodies_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterMeshActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_SelectedWaterBodies = { "SelectedWaterBodies", nullptr, (EPropertyFlags)0x0044000800002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterMeshActor, SelectedWaterBodies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_SelectedWaterBodies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_SelectedWaterBodies_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_ActorIcon_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterMeshActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_ActorIcon = { "ActorIcon", nullptr, (EPropertyFlags)0x0040000800082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterMeshActor, ActorIcon), Z_Construct_UClass_UBillboardComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_ActorIcon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_ActorIcon_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterMesh_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Water" },
		{ "Comment", "/** The water mesh component */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterMeshActor.h" },
		{ "ToolTip", "The water mesh component" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterMesh = { "WaterMesh", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterMeshActor, WaterMesh), Z_Construct_UClass_UWaterMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterMeshActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterVelocityTexture,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_SelectedWaterBodies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_SelectedWaterBodies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_ActorIcon,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterMeshActor_Statics::NewProp_WaterMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterMeshActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterMeshActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterMeshActor_Statics::ClassParams = {
		&AWaterMeshActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWaterMeshActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterMeshActor_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterMeshActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterMeshActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterMeshActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterMeshActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterMeshActor, 2911976038);
	template<> WATER_API UClass* StaticClass<AWaterMeshActor>()
	{
		return AWaterMeshActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterMeshActor(Z_Construct_UClass_AWaterMeshActor, &AWaterMeshActor::StaticClass, TEXT("/Script/Water"), TEXT("AWaterMeshActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterMeshActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
