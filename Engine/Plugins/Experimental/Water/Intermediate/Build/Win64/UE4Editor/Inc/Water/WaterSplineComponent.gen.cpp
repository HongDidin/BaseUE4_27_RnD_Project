// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterSplineComponent.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterSplineComponent() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UWaterSplineComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterSplineComponent();
	ENGINE_API UClass* Z_Construct_UClass_USplineComponent();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterSplineCurveDefaults();
// End Cross Module References
	void UWaterSplineComponent::StaticRegisterNativesUWaterSplineComponent()
	{
	}
	UClass* Z_Construct_UClass_UWaterSplineComponent_NoRegister()
	{
		return UWaterSplineComponent::StaticClass();
	}
	struct Z_Construct_UClass_UWaterSplineComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterSplineDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterSplineDefaults;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousWaterSplineDefaults_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PreviousWaterSplineDefaults;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterSplineComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USplineComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "ClassGroupNames", "Custom" },
		{ "HideCategories", "Physics Collision Lighting Rendering Mobile Trigger VirtualTexture" },
		{ "IncludePath", "WaterSplineComponent.h" },
		{ "ModuleRelativePath", "Public/WaterSplineComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_WaterSplineDefaults_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/**\n\x09 * Defaults which are used to propagate values to spline points on instances of this in the world\n\x09 */" },
		{ "ModuleRelativePath", "Public/WaterSplineComponent.h" },
		{ "ToolTip", "Defaults which are used to propagate values to spline points on instances of this in the world" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_WaterSplineDefaults = { "WaterSplineDefaults", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterSplineComponent, WaterSplineDefaults), Z_Construct_UScriptStruct_FWaterSplineCurveDefaults, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_WaterSplineDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_WaterSplineDefaults_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_PreviousWaterSplineDefaults_MetaData[] = {
		{ "Comment", "/** \n\x09 * This stores the last defaults propagated to spline points on an instance of this component \n\x09 *  Used to determine if spline points were modifed by users or if they exist at a current default value\n\x09 */" },
		{ "ModuleRelativePath", "Public/WaterSplineComponent.h" },
		{ "ToolTip", "This stores the last defaults propagated to spline points on an instance of this component\n Used to determine if spline points were modifed by users or if they exist at a current default value" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_PreviousWaterSplineDefaults = { "PreviousWaterSplineDefaults", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterSplineComponent, PreviousWaterSplineDefaults), Z_Construct_UScriptStruct_FWaterSplineCurveDefaults, METADATA_PARAMS(Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_PreviousWaterSplineDefaults_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_PreviousWaterSplineDefaults_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterSplineComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_WaterSplineDefaults,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterSplineComponent_Statics::NewProp_PreviousWaterSplineDefaults,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterSplineComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterSplineComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterSplineComponent_Statics::ClassParams = {
		&UWaterSplineComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterSplineComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterSplineComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterSplineComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterSplineComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterSplineComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterSplineComponent, 2148825091);
	template<> WATER_API UClass* StaticClass<UWaterSplineComponent>()
	{
		return UWaterSplineComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterSplineComponent(Z_Construct_UClass_UWaterSplineComponent, &UWaterSplineComponent::StaticClass, TEXT("/Script/Water"), TEXT("UWaterSplineComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterSplineComponent);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UWaterSplineComponent)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
