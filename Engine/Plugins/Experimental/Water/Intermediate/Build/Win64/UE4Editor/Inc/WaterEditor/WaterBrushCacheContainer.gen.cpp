// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Private/WaterBrushCacheContainer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBrushCacheContainer() {}
// Cross Module References
	WATEREDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyBrushCache();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainer_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainer();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FWaterBodyBrushCache::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATEREDITOR_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyBrushCache_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyBrushCache, Z_Construct_UPackage__Script_WaterEditor(), TEXT("WaterBodyBrushCache"), sizeof(FWaterBodyBrushCache), Get_Z_Construct_UScriptStruct_FWaterBodyBrushCache_Hash());
	}
	return Singleton;
}
template<> WATEREDITOR_API UScriptStruct* StaticStruct<FWaterBodyBrushCache>()
{
	return FWaterBodyBrushCache::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyBrushCache(FWaterBodyBrushCache::StaticStruct, TEXT("/Script/WaterEditor"), TEXT("WaterBodyBrushCache"), false, nullptr, nullptr);
static struct FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyBrushCache
{
	FScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyBrushCache()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyBrushCache>(FName(TEXT("WaterBodyBrushCache")));
	}
} ScriptStruct_WaterEditor_StaticRegisterNativesFWaterBodyBrushCache;
	struct Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CacheRenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CacheRenderTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CacheIsValid_MetaData[];
#endif
		static void NewProp_CacheIsValid_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_CacheIsValid;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// TODO [jonathan.bard] : rename : this is not a WaterBodyBrushCache, this a simple RenderTarget with a boolean to force an update on it\n//  This is also used for caching curves...\n" },
		{ "ModuleRelativePath", "Private/WaterBrushCacheContainer.h" },
		{ "ToolTip", "TODO [jonathan.bard] : rename : this is not a WaterBodyBrushCache, this a simple RenderTarget with a boolean to force an update on it\n This is also used for caching curves..." },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyBrushCache>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheRenderTarget_MetaData[] = {
		{ "Category", "Cache" },
		{ "ModuleRelativePath", "Private/WaterBrushCacheContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheRenderTarget = { "CacheRenderTarget", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyBrushCache, CacheRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheRenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheRenderTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheIsValid_MetaData[] = {
		{ "Category", "Cache" },
		{ "ModuleRelativePath", "Private/WaterBrushCacheContainer.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheIsValid_SetBit(void* Obj)
	{
		((FWaterBodyBrushCache*)Obj)->CacheIsValid = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheIsValid = { "CacheIsValid", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FWaterBodyBrushCache), &Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheIsValid_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheIsValid_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheIsValid_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheRenderTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::NewProp_CacheIsValid,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
		nullptr,
		&NewStructOps,
		"WaterBodyBrushCache",
		sizeof(FWaterBodyBrushCache),
		alignof(FWaterBodyBrushCache),
		Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyBrushCache()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyBrushCache_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_WaterEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyBrushCache"), sizeof(FWaterBodyBrushCache), Get_Z_Construct_UScriptStruct_FWaterBodyBrushCache_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyBrushCache_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyBrushCache_Hash() { return 853249658U; }
	void UWaterBodyBrushCacheContainer::StaticRegisterNativesUWaterBodyBrushCacheContainer()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainer_NoRegister()
	{
		return UWaterBodyBrushCacheContainer::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Cache_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Cache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "WaterBrushCacheContainer.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/WaterBrushCacheContainer.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::NewProp_Cache_MetaData[] = {
		{ "Category", "Default" },
		{ "DisplayName", "Cache" },
		{ "ModuleRelativePath", "Private/WaterBrushCacheContainer.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::NewProp_Cache = { "Cache", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterBodyBrushCacheContainer, Cache), Z_Construct_UScriptStruct_FWaterBodyBrushCache, METADATA_PARAMS(Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::NewProp_Cache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::NewProp_Cache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::NewProp_Cache,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyBrushCacheContainer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::ClassParams = {
		&UWaterBodyBrushCacheContainer::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyBrushCacheContainer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyBrushCacheContainer, 1780662346);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyBrushCacheContainer>()
	{
		return UWaterBodyBrushCacheContainer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyBrushCacheContainer(Z_Construct_UClass_UWaterBodyBrushCacheContainer, &UWaterBodyBrushCacheContainer::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyBrushCacheContainer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyBrushCacheContainer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
