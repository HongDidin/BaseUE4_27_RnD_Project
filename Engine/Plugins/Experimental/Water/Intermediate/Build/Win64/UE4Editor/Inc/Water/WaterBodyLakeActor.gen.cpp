// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyLakeActor.h"
#include "Runtime/Public/WaterBodyActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyLakeActor() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_ULakeGenerator_NoRegister();
	WATER_API UClass* Z_Construct_UClass_ULakeGenerator();
	WATER_API UClass* Z_Construct_UClass_UWaterBodyGenerator();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBoxComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_ULakeCollisionComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyLake_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyLake();
	WATER_API UClass* Z_Construct_UClass_AWaterBody();
// End Cross Module References
	void ULakeGenerator::StaticRegisterNativesULakeGenerator()
	{
	}
	UClass* Z_Construct_UClass_ULakeGenerator_NoRegister()
	{
		return ULakeGenerator::StaticClass();
	}
	struct Z_Construct_UClass_ULakeGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LakeMeshComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LakeMeshComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LakeCollisionComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LakeCollisionComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LakeCollision_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LakeCollision;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULakeGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULakeGenerator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "IncludePath", "WaterBodyLakeActor.h" },
		{ "ModuleRelativePath", "Public/WaterBodyLakeActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeMeshComp_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyLakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeMeshComp = { "LakeMeshComp", nullptr, (EPropertyFlags)0x0040800000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULakeGenerator, LakeMeshComp), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeMeshComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeMeshComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollisionComp_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyLakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollisionComp = { "LakeCollisionComp", nullptr, (EPropertyFlags)0x0040000020080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULakeGenerator, LakeCollisionComp_DEPRECATED), Z_Construct_UClass_UBoxComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollisionComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollisionComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollision_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyLakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollision = { "LakeCollision", nullptr, (EPropertyFlags)0x0040800000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULakeGenerator, LakeCollision), Z_Construct_UClass_ULakeCollisionComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollision_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollision_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULakeGenerator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeMeshComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollisionComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULakeGenerator_Statics::NewProp_LakeCollision,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULakeGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULakeGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULakeGenerator_Statics::ClassParams = {
		&ULakeGenerator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULakeGenerator_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULakeGenerator_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULakeGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULakeGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULakeGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULakeGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULakeGenerator, 1900416502);
	template<> WATER_API UClass* StaticClass<ULakeGenerator>()
	{
		return ULakeGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULakeGenerator(Z_Construct_UClass_ULakeGenerator, &ULakeGenerator::StaticClass, TEXT("/Script/Water"), TEXT("ULakeGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULakeGenerator);
	void AWaterBodyLake::StaticRegisterNativesAWaterBodyLake()
	{
	}
	UClass* Z_Construct_UClass_AWaterBodyLake_NoRegister()
	{
		return AWaterBodyLake::StaticClass();
	}
	struct Z_Construct_UClass_AWaterBodyLake_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LakeGenerator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LakeGenerator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterBodyLake_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AWaterBody,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyLake_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "HideCategories", "Tags Activation Cooking Replication Input Actor AssetUserData" },
		{ "IncludePath", "WaterBodyLakeActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyLakeActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyLake_Statics::NewProp_LakeGenerator_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyLakeActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyLake_Statics::NewProp_LakeGenerator = { "LakeGenerator", nullptr, (EPropertyFlags)0x0020880000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyLake, LakeGenerator), Z_Construct_UClass_ULakeGenerator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyLake_Statics::NewProp_LakeGenerator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyLake_Statics::NewProp_LakeGenerator_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterBodyLake_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyLake_Statics::NewProp_LakeGenerator,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterBodyLake_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterBodyLake>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterBodyLake_Statics::ClassParams = {
		&AWaterBodyLake::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWaterBodyLake_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyLake_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterBodyLake_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyLake_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterBodyLake()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterBodyLake_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterBodyLake, 3227504587);
	template<> WATER_API UClass* StaticClass<AWaterBodyLake>()
	{
		return AWaterBodyLake::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterBodyLake(Z_Construct_UClass_AWaterBodyLake, &AWaterBodyLake::StaticClass, TEXT("/Script/Water"), TEXT("AWaterBodyLake"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterBodyLake);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
