// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyActor.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyActor() {}
// Cross Module References
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FPostProcessSettings();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterBodyGenerator_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterBodyGenerator();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBody();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyExclusionVolume_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyIsland_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterSplineComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterWavesBase_NoRegister();
	PHYSICSCORE_API UClass* Z_Construct_UClass_UPhysicalMaterial_NoRegister();
	WATER_API UEnum* Z_Construct_UEnum_Water_EWaterBodyType();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterCurveSettings();
	LANDMASS_API UScriptStruct* Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterSplineMetadata_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBillboardComponent_NoRegister();
	LANDSCAPE_API UClass* Z_Construct_UClass_ALandscapeProxy_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	ENGINE_API UClass* Z_Construct_UClass_UNavAreaBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UNavRelevantInterface_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterBrushActorInterface_NoRegister();
// End Cross Module References
class UScriptStruct* FUnderwaterPostProcessSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings, Z_Construct_UPackage__Script_Water(), TEXT("UnderwaterPostProcessSettings"), sizeof(FUnderwaterPostProcessSettings), Get_Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FUnderwaterPostProcessSettings>()
{
	return FUnderwaterPostProcessSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUnderwaterPostProcessSettings(FUnderwaterPostProcessSettings::StaticStruct, TEXT("/Script/Water"), TEXT("UnderwaterPostProcessSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFUnderwaterPostProcessSettings
{
	FScriptStruct_Water_StaticRegisterNativesFUnderwaterPostProcessSettings()
	{
		UScriptStruct::DeferCppStructOps<FUnderwaterPostProcessSettings>(FName(TEXT("UnderwaterPostProcessSettings")));
	}
} ScriptStruct_Water_StaticRegisterNativesFUnderwaterPostProcessSettings;
	struct Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Priority_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Priority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendRadius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BlendRadius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendWeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BlendWeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PostProcessSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PostProcessSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderwaterPostProcessMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UnderwaterPostProcessMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUnderwaterPostProcessSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FUnderwaterPostProcessSettings*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FUnderwaterPostProcessSettings), &Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_Priority_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_Priority = { "Priority", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUnderwaterPostProcessSettings, Priority), METADATA_PARAMS(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_Priority_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_Priority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendRadius_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** World space radius around the volume that is used for blending (only if not unbound).\x09\x09\x09*/" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "World space radius around the volume that is used for blending (only if not unbound)." },
		{ "UIMax", "6000.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendRadius = { "BlendRadius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUnderwaterPostProcessSettings, BlendRadius), METADATA_PARAMS(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendRadius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendRadius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendWeight_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** 0:no effect, 1:full effect */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "0:no effect, 1:full effect" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendWeight = { "BlendWeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUnderwaterPostProcessSettings, BlendWeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendWeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendWeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_PostProcessSettings_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** List of all post-process settings to use when underwater : note : use UnderwaterPostProcessMaterial for setting the actual post process material. */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "List of all post-process settings to use when underwater : note : use UnderwaterPostProcessMaterial for setting the actual post process material." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_PostProcessSettings = { "PostProcessSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUnderwaterPostProcessSettings, PostProcessSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_PostProcessSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_PostProcessSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData[] = {
		{ "Comment", "/** This is the parent post process material for the PostProcessSettings */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "This is the parent post process material for the PostProcessSettings" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_UnderwaterPostProcessMaterial = { "UnderwaterPostProcessMaterial", nullptr, (EPropertyFlags)0x0010000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUnderwaterPostProcessSettings, UnderwaterPostProcessMaterial_DEPRECATED), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_Priority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendRadius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_BlendWeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_PostProcessSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::NewProp_UnderwaterPostProcessMaterial,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"UnderwaterPostProcessSettings",
		sizeof(FUnderwaterPostProcessSettings),
		alignof(FUnderwaterPostProcessSettings),
		Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UnderwaterPostProcessSettings"), sizeof(FUnderwaterPostProcessSettings), Get_Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings_Hash() { return 1076074709U; }
	void UWaterBodyGenerator::StaticRegisterNativesUWaterBodyGenerator()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyGenerator_NoRegister()
	{
		return UWaterBodyGenerator::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyGenerator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// For internal use.\n" },
		{ "IncludePath", "WaterBodyActor.h" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "For internal use." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyGenerator_Statics::ClassParams = {
		&UWaterBodyGenerator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyGenerator, 626816683);
	template<> WATER_API UClass* StaticClass<UWaterBodyGenerator>()
	{
		return UWaterBodyGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyGenerator(Z_Construct_UClass_UWaterBodyGenerator, &UWaterBodyGenerator::StaticClass, TEXT("/Script/Water"), TEXT("UWaterBodyGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyGenerator);
	DEFINE_FUNCTION(AWaterBody::execGetMaxWaveHeight)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetMaxWaveHeight();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execOnWaterBodyChanged)
	{
		P_GET_UBOOL(Z_Param_bShapeOrPositionChanged);
		P_GET_UBOOL(Z_Param_bWeightmapSettingsChanged);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnWaterBodyChanged(Z_Param_bShapeOrPositionChanged,Z_Param_bWeightmapSettingsChanged);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execSetWaterWaves)
	{
		P_GET_OBJECT(UWaterWavesBase,Z_Param_InWaterWaves);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetWaterWaves(Z_Param_InWaterWaves);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetExclusionVolumes)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AWaterBodyExclusionVolume*>*)Z_Param__Result=P_THIS->GetExclusionVolumes();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetIslands)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AWaterBodyIsland*>*)Z_Param__Result=P_THIS->GetIslands();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetUnderwaterPostProcessMaterialInstance)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMaterialInstanceDynamic**)Z_Param__Result=P_THIS->GetUnderwaterPostProcessMaterialInstance();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetWaterMaterialInstance)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMaterialInstanceDynamic**)Z_Param__Result=P_THIS->GetWaterMaterialInstance();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetWaterMaterial)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMaterialInterface**)Z_Param__Result=P_THIS->GetWaterMaterial();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetWaterSpline)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWaterSplineComponent**)Z_Param__Result=P_THIS->GetWaterSpline();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetRiverToOceanTransitionMaterialInstance)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMaterialInstanceDynamic**)Z_Param__Result=P_THIS->GetRiverToOceanTransitionMaterialInstance();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AWaterBody::execGetRiverToLakeTransitionMaterialInstance)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMaterialInstanceDynamic**)Z_Param__Result=P_THIS->GetRiverToLakeTransitionMaterialInstance();
		P_NATIVE_END;
	}
	void AWaterBody::StaticRegisterNativesAWaterBody()
	{
		UClass* Class = AWaterBody::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetExclusionVolumes", &AWaterBody::execGetExclusionVolumes },
			{ "GetIslands", &AWaterBody::execGetIslands },
			{ "GetMaxWaveHeight", &AWaterBody::execGetMaxWaveHeight },
			{ "GetRiverToLakeTransitionMaterialInstance", &AWaterBody::execGetRiverToLakeTransitionMaterialInstance },
			{ "GetRiverToOceanTransitionMaterialInstance", &AWaterBody::execGetRiverToOceanTransitionMaterialInstance },
			{ "GetUnderwaterPostProcessMaterialInstance", &AWaterBody::execGetUnderwaterPostProcessMaterialInstance },
			{ "GetWaterMaterial", &AWaterBody::execGetWaterMaterial },
			{ "GetWaterMaterialInstance", &AWaterBody::execGetWaterMaterialInstance },
			{ "GetWaterSpline", &AWaterBody::execGetWaterSpline },
			{ "OnWaterBodyChanged", &AWaterBody::execOnWaterBodyChanged },
			{ "SetWaterWaves", &AWaterBody::execSetWaterWaves },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics
	{
		struct WaterBody_eventGetExclusionVolumes_Parms
		{
			TArray<AWaterBodyExclusionVolume*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBodyExclusionVolume_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetExclusionVolumes_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "Comment", "/**\n\x09 * Gets the exclusion volume that influence this water body\n\x09 */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Gets the exclusion volume that influence this water body" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetExclusionVolumes", nullptr, nullptr, sizeof(WaterBody_eventGetExclusionVolumes_Parms), Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetExclusionVolumes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetExclusionVolumes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetIslands_Statics
	{
		struct WaterBody_eventGetIslands_Parms
		{
			TArray<AWaterBodyIsland*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetIslands_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBodyIsland_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AWaterBody_GetIslands_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetIslands_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetIslands_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetIslands_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetIslands_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetIslands_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "Comment", "/**\n\x09 * Gets the islands that influence this water body\n\x09 */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Gets the islands that influence this water body" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetIslands_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetIslands", nullptr, nullptr, sizeof(WaterBody_eventGetIslands_Parms), Z_Construct_UFunction_AWaterBody_GetIslands_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetIslands_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetIslands_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetIslands_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetIslands()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetIslands_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics
	{
		struct WaterBody_eventGetMaxWaveHeight_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetMaxWaveHeight_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::Function_MetaDataParams[] = {
		{ "Category", "Wave" },
		{ "Comment", "/** Returns the max height that this water body's waves can hit. Can be called regardless of whether the water body supports waves or not */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Returns the max height that this water body's waves can hit. Can be called regardless of whether the water body supports waves or not" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetMaxWaveHeight", nullptr, nullptr, sizeof(WaterBody_eventGetMaxWaveHeight_Parms), Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics
	{
		struct WaterBody_eventGetRiverToLakeTransitionMaterialInstance_Parms
		{
			UMaterialInstanceDynamic* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetRiverToLakeTransitionMaterialInstance_Parms, ReturnValue), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Returns River to lake transition material instance (For internal use. Please use AWaterBodyRiver instead.) */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Returns River to lake transition material instance (For internal use. Please use AWaterBodyRiver instead.)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetRiverToLakeTransitionMaterialInstance", nullptr, nullptr, sizeof(WaterBody_eventGetRiverToLakeTransitionMaterialInstance_Parms), Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics
	{
		struct WaterBody_eventGetRiverToOceanTransitionMaterialInstance_Parms
		{
			UMaterialInstanceDynamic* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetRiverToOceanTransitionMaterialInstance_Parms, ReturnValue), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Returns River to ocean transition material instance (For internal use. Please use AWaterBodyRiver instead.) */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Returns River to ocean transition material instance (For internal use. Please use AWaterBodyRiver instead.)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetRiverToOceanTransitionMaterialInstance", nullptr, nullptr, sizeof(WaterBody_eventGetRiverToOceanTransitionMaterialInstance_Parms), Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics
	{
		struct WaterBody_eventGetUnderwaterPostProcessMaterialInstance_Parms
		{
			UMaterialInstanceDynamic* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetUnderwaterPostProcessMaterialInstance_Parms, ReturnValue), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Returns under water post process MID */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Returns under water post process MID" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetUnderwaterPostProcessMaterialInstance", nullptr, nullptr, sizeof(WaterBody_eventGetUnderwaterPostProcessMaterialInstance_Parms), Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics
	{
		struct WaterBody_eventGetWaterMaterial_Parms
		{
			UMaterialInterface* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetWaterMaterial_Parms, ReturnValue), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::Function_MetaDataParams[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Returns water material */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Returns water material" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetWaterMaterial", nullptr, nullptr, sizeof(WaterBody_eventGetWaterMaterial_Parms), Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetWaterMaterial()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetWaterMaterial_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics
	{
		struct WaterBody_eventGetWaterMaterialInstance_Parms
		{
			UMaterialInstanceDynamic* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetWaterMaterialInstance_Parms, ReturnValue), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Returns water MID */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Returns water MID" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetWaterMaterialInstance", nullptr, nullptr, sizeof(WaterBody_eventGetWaterMaterialInstance_Parms), Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics
	{
		struct WaterBody_eventGetWaterSpline_Parms
		{
			UWaterSplineComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventGetWaterSpline_Parms, ReturnValue), Z_Construct_UClass_UWaterSplineComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Returns water spline component */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Returns water spline component" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "GetWaterSpline", nullptr, nullptr, sizeof(WaterBody_eventGetWaterSpline_Parms), Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_GetWaterSpline()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_GetWaterSpline_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics
	{
		struct WaterBody_eventOnWaterBodyChanged_Parms
		{
			bool bShapeOrPositionChanged;
			bool bWeightmapSettingsChanged;
		};
		static void NewProp_bShapeOrPositionChanged_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShapeOrPositionChanged;
		static void NewProp_bWeightmapSettingsChanged_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWeightmapSettingsChanged;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bShapeOrPositionChanged_SetBit(void* Obj)
	{
		((WaterBody_eventOnWaterBodyChanged_Parms*)Obj)->bShapeOrPositionChanged = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bShapeOrPositionChanged = { "bShapeOrPositionChanged", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WaterBody_eventOnWaterBodyChanged_Parms), &Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bShapeOrPositionChanged_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bWeightmapSettingsChanged_SetBit(void* Obj)
	{
		((WaterBody_eventOnWaterBodyChanged_Parms*)Obj)->bWeightmapSettingsChanged = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bWeightmapSettingsChanged = { "bWeightmapSettingsChanged", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(WaterBody_eventOnWaterBodyChanged_Parms), &Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bWeightmapSettingsChanged_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bShapeOrPositionChanged,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::NewProp_bWeightmapSettingsChanged,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "CPP_Default_bWeightmapSettingsChanged", "false" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "OnWaterBodyChanged", nullptr, nullptr, sizeof(WaterBody_eventOnWaterBodyChanged_Parms), Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics
	{
		struct WaterBody_eventSetWaterWaves_Parms
		{
			UWaterWavesBase* InWaterWaves;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWaterWaves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::NewProp_InWaterWaves = { "InWaterWaves", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBody_eventSetWaterWaves_Parms, InWaterWaves), Z_Construct_UClass_UWaterWavesBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::NewProp_InWaterWaves,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::Function_MetaDataParams[] = {
		{ "Category", "Wave" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBody, nullptr, "SetWaterWaves", nullptr, nullptr, sizeof(WaterBody_eventSetWaterWaves_Parms), Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBody_SetWaterWaves()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBody_SetWaterWaves_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWaterBody_NoRegister()
	{
		return AWaterBody::StaticClass();
	}
	struct Z_Construct_UClass_AWaterBody_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PhysicalMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PhysicalMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetWaveMaskDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TargetWaveMaskDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxWaveHeightOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxWaveHeightOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_WaterBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFillCollisionUnderWaterBodiesForNavmesh_MetaData[];
#endif
		static void NewProp_bFillCollisionUnderWaterBodiesForNavmesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFillCollisionUnderWaterBodiesForNavmesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderwaterPostProcessSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UnderwaterPostProcessSettings;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_WaterBodyType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WaterBodyType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurveSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderwaterPostProcessMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UnderwaterPostProcessMaterial;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerrainCarvingSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TerrainCarvingSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterHeightmapSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterHeightmapSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LayerWeightmapSettings_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_LayerWeightmapSettings_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LayerWeightmapSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_LayerWeightmapSettings;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAffectsLandscape_MetaData[];
#endif
		static void NewProp_bAffectsLandscape_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAffectsLandscape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bGenerateCollisions_MetaData[];
#endif
		static void NewProp_bGenerateCollisions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bGenerateCollisions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOverrideWaterMesh_MetaData[];
#endif
		static void NewProp_bOverrideWaterMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOverrideWaterMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterMeshOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterMeshOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverlapMaterialPriority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OverlapMaterialPriority;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionProfileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CollisionProfileName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SplineComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SplineComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterSplineMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterSplineMetadata;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorIcon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorIcon;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterMID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UnderwaterPostProcessMID_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UnderwaterPostProcessMID;
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_Islands_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Islands_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Islands;
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_ExclusionVolumes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExclusionVolumes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExclusionVolumes;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Landscape_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Landscape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentPostProcessSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentPostProcessSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCanAffectNavigation_MetaData[];
#endif
		static void NewProp_bCanAffectNavigation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCanAffectNavigation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterNavAreaClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WaterNavAreaClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterWaves_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterWaves;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterBody_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AWaterBody_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AWaterBody_GetExclusionVolumes, "GetExclusionVolumes" }, // 881444007
		{ &Z_Construct_UFunction_AWaterBody_GetIslands, "GetIslands" }, // 3512326645
		{ &Z_Construct_UFunction_AWaterBody_GetMaxWaveHeight, "GetMaxWaveHeight" }, // 226732147
		{ &Z_Construct_UFunction_AWaterBody_GetRiverToLakeTransitionMaterialInstance, "GetRiverToLakeTransitionMaterialInstance" }, // 2348731742
		{ &Z_Construct_UFunction_AWaterBody_GetRiverToOceanTransitionMaterialInstance, "GetRiverToOceanTransitionMaterialInstance" }, // 3309948012
		{ &Z_Construct_UFunction_AWaterBody_GetUnderwaterPostProcessMaterialInstance, "GetUnderwaterPostProcessMaterialInstance" }, // 260431611
		{ &Z_Construct_UFunction_AWaterBody_GetWaterMaterial, "GetWaterMaterial" }, // 1574260012
		{ &Z_Construct_UFunction_AWaterBody_GetWaterMaterialInstance, "GetWaterMaterialInstance" }, // 3137055439
		{ &Z_Construct_UFunction_AWaterBody_GetWaterSpline, "GetWaterSpline" }, // 1783285378
		{ &Z_Construct_UFunction_AWaterBody_OnWaterBodyChanged, "OnWaterBodyChanged" }, // 1413131271
		{ &Z_Construct_UFunction_AWaterBody_SetWaterWaves, "SetWaterWaves" }, // 3820513697
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "//@todo_water: Remove Blueprintable\n" },
		{ "HideCategories", "Tags Activation Cooking Replication Input Actor AssetUserData" },
		{ "IncludePath", "WaterBodyActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "@todo_water: Remove Blueprintable" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_PhysicalMaterial_MetaData[] = {
		{ "Category", "Collision" },
		{ "EditCondition", "bGenerateCollisions" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_PhysicalMaterial = { "PhysicalMaterial", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, PhysicalMaterial), Z_Construct_UClass_UPhysicalMaterial_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_PhysicalMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_PhysicalMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_TargetWaveMaskDepth_MetaData[] = {
		{ "Category", "Wave" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Water depth at which waves start being attenuated. */" },
		{ "DisplayName", "Wave Attenuation Water Depth" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Water depth at which waves start being attenuated." },
		{ "UIMax", "10000.000000" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_TargetWaveMaskDepth = { "TargetWaveMaskDepth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, TargetWaveMaskDepth), METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_TargetWaveMaskDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_TargetWaveMaskDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_MaxWaveHeightOffset_MetaData[] = {
		{ "Category", "Wave" },
		{ "Comment", "/** Offset added to the automatically calculated max wave height bounds. Use this in case the automatically calculated max height bounds don't match your waves. This can happen if the water surface is manually altered through World Position Offset or other means.*/" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Offset added to the automatically calculated max wave height bounds. Use this in case the automatically calculated max height bounds don't match your waves. This can happen if the water surface is manually altered through World Position Offset or other means." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_MaxWaveHeightOffset = { "MaxWaveHeightOffset", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, MaxWaveHeightOffset), METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_MaxWaveHeightOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_MaxWaveHeightOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyIndex_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Unique Id for accessing (wave, ... ) data in GPU buffers */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Unique Id for accessing (wave, ... ) data in GPU buffers" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyIndex = { "WaterBodyIndex", nullptr, (EPropertyFlags)0x0010000400222015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterBodyIndex), METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_bFillCollisionUnderWaterBodiesForNavmesh_MetaData[] = {
		{ "Category", "Navigation" },
		{ "Comment", "/** Prevent navmesh generation under the water geometry */" },
		{ "EditCondition", "bGenerateCollisions" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Prevent navmesh generation under the water geometry" },
	};
#endif
	void Z_Construct_UClass_AWaterBody_Statics::NewProp_bFillCollisionUnderWaterBodiesForNavmesh_SetBit(void* Obj)
	{
		((AWaterBody*)Obj)->bFillCollisionUnderWaterBodiesForNavmesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_bFillCollisionUnderWaterBodiesForNavmesh = { "bFillCollisionUnderWaterBodiesForNavmesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWaterBody), &Z_Construct_UClass_AWaterBody_Statics::NewProp_bFillCollisionUnderWaterBodiesForNavmesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_bFillCollisionUnderWaterBodiesForNavmesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_bFillCollisionUnderWaterBodiesForNavmesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessSettings_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Post process settings to apply when the camera goes underwater (only available when bGenerateCollisions is true because collisions are needed to detect if it's under water).\n\x09Note: Underwater post process material is setup using UnderwaterPostProcessMaterial. */" },
		{ "DisplayAfter", "UnderwaterPostProcessMaterial" },
		{ "EditCondition", "bGenerateCollisions && UnderwaterPostProcessMaterial != nullptr" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Post process settings to apply when the camera goes underwater (only available when bGenerateCollisions is true because collisions are needed to detect if it's under water).\n      Note: Underwater post process material is setup using UnderwaterPostProcessMaterial." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessSettings = { "UnderwaterPostProcessSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, UnderwaterPostProcessSettings), Z_Construct_UScriptStruct_FUnderwaterPostProcessSettings, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessSettings_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyType_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "// @todo_water: Remove and always use GetWaterBodyType()\n" },
		{ "ExposeOnSpawn", "" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "@todo_water: Remove and always use GetWaterBodyType()" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyType = { "WaterBodyType", nullptr, (EPropertyFlags)0x0011000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterBodyType), Z_Construct_UEnum_Water_EWaterBodyType, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_CurveSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_CurveSettings = { "CurveSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, CurveSettings), Z_Construct_UScriptStruct_FWaterCurveSettings, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_CurveSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_CurveSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMaterial = { "WaterMaterial", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Post process material to apply when the camera goes underwater (only available when bGenerateCollisions is true because collisions are needed to detect if it's under water). */" },
		{ "DisplayAfter", "WaterMaterial" },
		{ "EditCondition", "bGenerateCollisions" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Post process material to apply when the camera goes underwater (only available when bGenerateCollisions is true because collisions are needed to detect if it's under water)." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMaterial = { "UnderwaterPostProcessMaterial", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, UnderwaterPostProcessMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMaterial_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_TerrainCarvingSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_TerrainCarvingSettings = { "TerrainCarvingSettings", nullptr, (EPropertyFlags)0x0010000820000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, TerrainCarvingSettings_DEPRECATED), Z_Construct_UScriptStruct_FLandmassTerrainCarvingSettings, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_TerrainCarvingSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_TerrainCarvingSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterHeightmapSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterHeightmapSettings = { "WaterHeightmapSettings", nullptr, (EPropertyFlags)0x0010000800000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterHeightmapSettings), Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterHeightmapSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterHeightmapSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings_ValueProp = { "LayerWeightmapSettings", nullptr, (EPropertyFlags)0x0000000800000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings_Key_KeyProp = { "LayerWeightmapSettings_Key", nullptr, (EPropertyFlags)0x0000000800000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings = { "LayerWeightmapSettings", nullptr, (EPropertyFlags)0x0010000800000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, LayerWeightmapSettings), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_bAffectsLandscape_MetaData[] = {
		{ "Category", "Terrain" },
		{ "Comment", "/** If enabled, landscape will be deformed based on this water body placed on top of it and landscape height will be considered when determining water depth at runtime */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "If enabled, landscape will be deformed based on this water body placed on top of it and landscape height will be considered when determining water depth at runtime" },
	};
#endif
	void Z_Construct_UClass_AWaterBody_Statics::NewProp_bAffectsLandscape_SetBit(void* Obj)
	{
		((AWaterBody*)Obj)->bAffectsLandscape = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_bAffectsLandscape = { "bAffectsLandscape", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWaterBody), &Z_Construct_UClass_AWaterBody_Statics::NewProp_bAffectsLandscape_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_bAffectsLandscape_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_bAffectsLandscape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_bGenerateCollisions_MetaData[] = {
		{ "Category", "Collision" },
		{ "Comment", "/** If true, one or more collision components associated with this water will be generated. Otherwise, this water body will only affect visuals. */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "If true, one or more collision components associated with this water will be generated. Otherwise, this water body will only affect visuals." },
	};
#endif
	void Z_Construct_UClass_AWaterBody_Statics::NewProp_bGenerateCollisions_SetBit(void* Obj)
	{
		((AWaterBody*)Obj)->bGenerateCollisions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_bGenerateCollisions = { "bGenerateCollisions", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWaterBody), &Z_Construct_UClass_AWaterBody_Statics::NewProp_bGenerateCollisions_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_bGenerateCollisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_bGenerateCollisions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_bOverrideWaterMesh_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "// TODO [jonathan.bard] : make sure override water mesh works for all types and remove the bool (WaterMeshOverride is already a pointer\n" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "TODO [jonathan.bard] : make sure override water mesh works for all types and remove the bool (WaterMeshOverride is already a pointer" },
	};
#endif
	void Z_Construct_UClass_AWaterBody_Statics::NewProp_bOverrideWaterMesh_SetBit(void* Obj)
	{
		((AWaterBody*)Obj)->bOverrideWaterMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_bOverrideWaterMesh = { "bOverrideWaterMesh", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWaterBody), &Z_Construct_UClass_AWaterBody_Statics::NewProp_bOverrideWaterMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_bOverrideWaterMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_bOverrideWaterMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMeshOverride_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMeshOverride = { "WaterMeshOverride", nullptr, (EPropertyFlags)0x00200c0000000015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterMeshOverride), Z_Construct_UClass_UStaticMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMeshOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMeshOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_OverlapMaterialPriority_MetaData[] = {
		{ "Category", "Rendering" },
		{ "ClampMax", "8191" },
		{ "ClampMin", "-8192" },
		{ "Comment", "/** Higher number is higher priority. If two water bodies overlap and they don't have a transition material specified, this will be used to determine which water body to use the material from. Valid range is -8192 to 8191 */" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Higher number is higher priority. If two water bodies overlap and they don't have a transition material specified, this will be used to determine which water body to use the material from. Valid range is -8192 to 8191" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_OverlapMaterialPriority = { "OverlapMaterialPriority", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, OverlapMaterialPriority), METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_OverlapMaterialPriority_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_OverlapMaterialPriority_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_CollisionProfileName_MetaData[] = {
		{ "Category", "Collision" },
		{ "EditCondition", "bGenerateCollisions" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_CollisionProfileName = { "CollisionProfileName", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, CollisionProfileName), METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_CollisionProfileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_CollisionProfileName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_SplineComp_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Water" },
		{ "Comment", "/**\n\x09 * The spline data attached to this water type.\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "The spline data attached to this water type." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_SplineComp = { "SplineComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, SplineComp), Z_Construct_UClass_UWaterSplineComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_SplineComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_SplineComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterSplineMetadata_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterSplineMetadata = { "WaterSplineMetadata", nullptr, (EPropertyFlags)0x0022080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterSplineMetadata), Z_Construct_UClass_UWaterSplineMetadata_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterSplineMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterSplineMetadata_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_ActorIcon_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_ActorIcon = { "ActorIcon", nullptr, (EPropertyFlags)0x0020080800082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, ActorIcon), Z_Construct_UClass_UBillboardComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_ActorIcon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_ActorIcon_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMID_MetaData[] = {
		{ "Category", "Debug" },
		{ "DisplayAfter", "WaterMaterial" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMID = { "WaterMID", nullptr, (EPropertyFlags)0x0020c80000022801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMID_MetaData[] = {
		{ "Category", "Debug" },
		{ "DisplayAfter", "UnderwaterPostProcessMaterial" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMID = { "UnderwaterPostProcessMID", nullptr, (EPropertyFlags)0x0020c80000022801, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, UnderwaterPostProcessMID), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMID_MetaData)) };
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_Islands_Inner = { "Islands", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBodyIsland_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_Islands_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Islands in this water body*/" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "Islands in this water body" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_Islands = { "Islands", nullptr, (EPropertyFlags)0x00240c0000000801, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, Islands), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_Islands_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_Islands_MetaData)) };
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_ExclusionVolumes_Inner = { "ExclusionVolumes", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBodyExclusionVolume_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_ExclusionVolumes_MetaData[] = {
		{ "Category", "Water" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_ExclusionVolumes = { "ExclusionVolumes", nullptr, (EPropertyFlags)0x00240c0000000801, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, ExclusionVolumes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_ExclusionVolumes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_ExclusionVolumes_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_Landscape_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_Landscape = { "Landscape", nullptr, (EPropertyFlags)0x0024080000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, Landscape), Z_Construct_UClass_ALandscapeProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_Landscape_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_Landscape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_CurrentPostProcessSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_CurrentPostProcessSettings = { "CurrentPostProcessSettings", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, CurrentPostProcessSettings), Z_Construct_UScriptStruct_FPostProcessSettings, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_CurrentPostProcessSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_CurrentPostProcessSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_bCanAffectNavigation_MetaData[] = {
		{ "Category", "Navigation" },
		{ "EditCondition", "bGenerateCollisions" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	void Z_Construct_UClass_AWaterBody_Statics::NewProp_bCanAffectNavigation_SetBit(void* Obj)
	{
		((AWaterBody*)Obj)->bCanAffectNavigation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_bCanAffectNavigation = { "bCanAffectNavigation", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWaterBody), &Z_Construct_UClass_AWaterBody_Statics::NewProp_bCanAffectNavigation_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_bCanAffectNavigation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_bCanAffectNavigation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterNavAreaClass_MetaData[] = {
		{ "Category", "Navigation" },
		{ "Comment", "// The navigation area class that will be generated on nav mesh\n" },
		{ "EditCondition", "bCanAffectNavigation && bGenerateCollisions" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
		{ "ToolTip", "The navigation area class that will be generated on nav mesh" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterNavAreaClass = { "WaterNavAreaClass", nullptr, (EPropertyFlags)0x0024080000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterNavAreaClass), Z_Construct_UClass_UNavAreaBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterNavAreaClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterNavAreaClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterWaves_MetaData[] = {
		{ "Category", "Wave" },
		{ "DisplayName", "Waves Source" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterWaves = { "WaterWaves", nullptr, (EPropertyFlags)0x002208000008001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBody, WaterWaves), Z_Construct_UClass_UWaterWavesBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterWaves_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_PhysicalMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_TargetWaveMaskDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_MaxWaveHeightOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_bFillCollisionUnderWaterBodiesForNavmesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterBodyType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_CurveSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMaterial,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_TerrainCarvingSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterHeightmapSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_LayerWeightmapSettings,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_bAffectsLandscape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_bGenerateCollisions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_bOverrideWaterMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMeshOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_OverlapMaterialPriority,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_CollisionProfileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_SplineComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterSplineMetadata,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_ActorIcon,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_UnderwaterPostProcessMID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_Islands_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_Islands,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_ExclusionVolumes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_ExclusionVolumes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_Landscape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_CurrentPostProcessSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_bCanAffectNavigation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterNavAreaClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBody_Statics::NewProp_WaterWaves,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWaterBody_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UNavRelevantInterface_NoRegister, (int32)VTABLE_OFFSET(AWaterBody, INavRelevantInterface), false },
			{ Z_Construct_UClass_UWaterBrushActorInterface_NoRegister, (int32)VTABLE_OFFSET(AWaterBody, IWaterBrushActorInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterBody_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterBody>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterBody_Statics::ClassParams = {
		&AWaterBody::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AWaterBody_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A5u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterBody_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBody_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterBody()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterBody_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterBody, 1186071358);
	template<> WATER_API UClass* StaticClass<AWaterBody>()
	{
		return AWaterBody::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterBody(Z_Construct_UClass_AWaterBody, &AWaterBody::StaticClass, TEXT("/Script/Water"), TEXT("AWaterBody"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterBody);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(AWaterBody)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
