// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Public/WaterEditorSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterEditorSubsystem() {}
// Cross Module References
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterEditorSubsystem_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterEditorSubsystem();
	EDITORSUBSYSTEM_API UClass* Z_Construct_UClass_UEditorSubsystem();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialParameterCollection_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UWaterEditorSubsystem::StaticRegisterNativesUWaterEditorSubsystem()
	{
	}
	UClass* Z_Construct_UClass_UWaterEditorSubsystem_NoRegister()
	{
		return UWaterEditorSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UWaterEditorSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LandscapeMaterialParameterCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LandscapeMaterialParameterCollection;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterActorSprites_ValueProp;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_WaterActorSprites_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterActorSprites_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_WaterActorSprites;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultWaterActorSprite_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DefaultWaterActorSprite;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ErrorSprite_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ErrorSprite;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterEditorSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEditorSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSubsystem_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WaterEditorSubsystem.h" },
		{ "ModuleRelativePath", "Public/WaterEditorSubsystem.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_LandscapeMaterialParameterCollection_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_LandscapeMaterialParameterCollection = { "LandscapeMaterialParameterCollection", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSubsystem, LandscapeMaterialParameterCollection), Z_Construct_UClass_UMaterialParameterCollection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_LandscapeMaterialParameterCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_LandscapeMaterialParameterCollection_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites_ValueProp = { "WaterActorSprites", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites_Key_KeyProp = { "WaterActorSprites_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites = { "WaterActorSprites", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSubsystem, WaterActorSprites), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_DefaultWaterActorSprite_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_DefaultWaterActorSprite = { "DefaultWaterActorSprite", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSubsystem, DefaultWaterActorSprite), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_DefaultWaterActorSprite_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_DefaultWaterActorSprite_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_ErrorSprite_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterEditorSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_ErrorSprite = { "ErrorSprite", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterEditorSubsystem, ErrorSprite), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_ErrorSprite_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_ErrorSprite_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterEditorSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_LandscapeMaterialParameterCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_WaterActorSprites,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_DefaultWaterActorSprite,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterEditorSubsystem_Statics::NewProp_ErrorSprite,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterEditorSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterEditorSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterEditorSubsystem_Statics::ClassParams = {
		&UWaterEditorSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterEditorSubsystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSubsystem_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterEditorSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterEditorSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterEditorSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterEditorSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterEditorSubsystem, 1310367010);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterEditorSubsystem>()
	{
		return UWaterEditorSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterEditorSubsystem(Z_Construct_UClass_UWaterEditorSubsystem, &UWaterEditorSubsystem::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterEditorSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterEditorSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
