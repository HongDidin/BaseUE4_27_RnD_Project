// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterRuntimeSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterRuntimeSettings() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UWaterRuntimeSettings_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterRuntimeSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UEnum* Z_Construct_UEnum_Engine_ECollisionChannel();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialParameterCollection_NoRegister();
// End Cross Module References
	void UWaterRuntimeSettings::StaticRegisterNativesUWaterRuntimeSettings()
	{
	}
	UClass* Z_Construct_UClass_UWaterRuntimeSettings_NoRegister()
	{
		return UWaterRuntimeSettings::StaticClass();
	}
	struct Z_Construct_UClass_UWaterRuntimeSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionChannelForWaterTraces_MetaData[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CollisionChannelForWaterTraces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialParameterCollection_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_MaterialParameterCollection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyIconWorldSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaterBodyIconWorldSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyIconWorldZOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaterBodyIconWorldZOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultWaterCollisionProfileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_DefaultWaterCollisionProfileName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterRuntimeSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterRuntimeSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements the runtime settings for the Water plugin.\n */" },
		{ "DisplayName", "Water" },
		{ "IncludePath", "WaterRuntimeSettings.h" },
		{ "ModuleRelativePath", "Public/WaterRuntimeSettings.h" },
		{ "ToolTip", "Implements the runtime settings for the Water plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_CollisionChannelForWaterTraces_MetaData[] = {
		{ "Category", "Collision" },
		{ "Comment", "/** Collision channel to use for tracing and blocking water bodies */" },
		{ "ModuleRelativePath", "Public/WaterRuntimeSettings.h" },
		{ "ToolTip", "Collision channel to use for tracing and blocking water bodies" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_CollisionChannelForWaterTraces = { "CollisionChannelForWaterTraces", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterRuntimeSettings, CollisionChannelForWaterTraces), Z_Construct_UEnum_Engine_ECollisionChannel, METADATA_PARAMS(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_CollisionChannelForWaterTraces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_CollisionChannelForWaterTraces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_MaterialParameterCollection_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Material Parameter Collection for everything water-related */" },
		{ "ModuleRelativePath", "Public/WaterRuntimeSettings.h" },
		{ "ToolTip", "Material Parameter Collection for everything water-related" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_MaterialParameterCollection = { "MaterialParameterCollection", nullptr, (EPropertyFlags)0x0014000000004001, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterRuntimeSettings, MaterialParameterCollection), Z_Construct_UClass_UMaterialParameterCollection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_MaterialParameterCollection_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_MaterialParameterCollection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldSize_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Size of the water body icon in world-space. */" },
		{ "ModuleRelativePath", "Public/WaterRuntimeSettings.h" },
		{ "ToolTip", "Size of the water body icon in world-space." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldSize = { "WaterBodyIconWorldSize", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterRuntimeSettings, WaterBodyIconWorldSize), METADATA_PARAMS(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldZOffset_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Offset in Z for the water body icon in world-space. */" },
		{ "ModuleRelativePath", "Public/WaterRuntimeSettings.h" },
		{ "ToolTip", "Offset in Z for the water body icon in world-space." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldZOffset = { "WaterBodyIconWorldZOffset", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterRuntimeSettings, WaterBodyIconWorldZOffset), METADATA_PARAMS(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldZOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldZOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_DefaultWaterCollisionProfileName_MetaData[] = {
		{ "Category", "Collision" },
		{ "Comment", "/** Default collision profile name of water bodies */" },
		{ "ModuleRelativePath", "Public/WaterRuntimeSettings.h" },
		{ "ToolTip", "Default collision profile name of water bodies" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_DefaultWaterCollisionProfileName = { "DefaultWaterCollisionProfileName", nullptr, (EPropertyFlags)0x0040000000024001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UWaterRuntimeSettings, DefaultWaterCollisionProfileName), METADATA_PARAMS(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_DefaultWaterCollisionProfileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_DefaultWaterCollisionProfileName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UWaterRuntimeSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_CollisionChannelForWaterTraces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_MaterialParameterCollection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_WaterBodyIconWorldZOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UWaterRuntimeSettings_Statics::NewProp_DefaultWaterCollisionProfileName,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterRuntimeSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterRuntimeSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterRuntimeSettings_Statics::ClassParams = {
		&UWaterRuntimeSettings::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UWaterRuntimeSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UWaterRuntimeSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterRuntimeSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterRuntimeSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterRuntimeSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterRuntimeSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterRuntimeSettings, 1920343736);
	template<> WATER_API UClass* StaticClass<UWaterRuntimeSettings>()
	{
		return UWaterRuntimeSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterRuntimeSettings(Z_Construct_UClass_UWaterRuntimeSettings, &UWaterRuntimeSettings::StaticClass, TEXT("/Script/Water"), TEXT("UWaterRuntimeSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterRuntimeSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
