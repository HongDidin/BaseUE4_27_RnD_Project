// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Private/WaterWavesAssetFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterWavesAssetFactory() {}
// Cross Module References
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterWavesAssetFactory_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterWavesAssetFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
// End Cross Module References
	void UWaterWavesAssetFactory::StaticRegisterNativesUWaterWavesAssetFactory()
	{
	}
	UClass* Z_Construct_UClass_UWaterWavesAssetFactory_NoRegister()
	{
		return UWaterWavesAssetFactory::StaticClass();
	}
	struct Z_Construct_UClass_UWaterWavesAssetFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterWavesAssetFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterWavesAssetFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WaterWavesAssetFactory.h" },
		{ "ModuleRelativePath", "Private/WaterWavesAssetFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterWavesAssetFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterWavesAssetFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterWavesAssetFactory_Statics::ClassParams = {
		&UWaterWavesAssetFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterWavesAssetFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterWavesAssetFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterWavesAssetFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterWavesAssetFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterWavesAssetFactory, 4061731364);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterWavesAssetFactory>()
	{
		return UWaterWavesAssetFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterWavesAssetFactory(Z_Construct_UClass_UWaterWavesAssetFactory, &UWaterWavesAssetFactory::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterWavesAssetFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterWavesAssetFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
