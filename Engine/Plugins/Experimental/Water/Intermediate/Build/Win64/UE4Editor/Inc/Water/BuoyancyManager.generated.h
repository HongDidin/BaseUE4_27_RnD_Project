// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
class ABuoyancyManager;
#ifdef WATER_BuoyancyManager_generated_h
#error "BuoyancyManager.generated.h already included, missing '#pragma once' in BuoyancyManager.h"
#endif
#define WATER_BuoyancyManager_generated_h

#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_SPARSE_DATA
#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetBuoyancyComponentManager);


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetBuoyancyComponentManager);


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABuoyancyManager(); \
	friend struct Z_Construct_UClass_ABuoyancyManager_Statics; \
public: \
	DECLARE_CLASS(ABuoyancyManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Water"), NO_API) \
	DECLARE_SERIALIZER(ABuoyancyManager)


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_INCLASS \
private: \
	static void StaticRegisterNativesABuoyancyManager(); \
	friend struct Z_Construct_UClass_ABuoyancyManager_Statics; \
public: \
	DECLARE_CLASS(ABuoyancyManager, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Water"), NO_API) \
	DECLARE_SERIALIZER(ABuoyancyManager)


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABuoyancyManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABuoyancyManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuoyancyManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuoyancyManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuoyancyManager(ABuoyancyManager&&); \
	NO_API ABuoyancyManager(const ABuoyancyManager&); \
public:


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABuoyancyManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABuoyancyManager(ABuoyancyManager&&); \
	NO_API ABuoyancyManager(const ABuoyancyManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABuoyancyManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABuoyancyManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABuoyancyManager)


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BuoyancyComponents() { return STRUCT_OFFSET(ABuoyancyManager, BuoyancyComponents); }


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_29_PROLOG
#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_INCLASS \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_SPARSE_DATA \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h_32_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class BuoyancyManager."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> WATER_API UClass* StaticClass<class ABuoyancyManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyManager_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
