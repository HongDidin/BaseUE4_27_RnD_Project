// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/NiagaraWaterFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeNiagaraWaterFunctionLibrary() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UNiagaraWaterFunctionLibrary_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UNiagaraWaterFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_Water();
	NIAGARA_API UClass* Z_Construct_UClass_UNiagaraComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UNiagaraWaterFunctionLibrary::execSetWaterBody)
	{
		P_GET_OBJECT(UNiagaraComponent,Z_Param_NiagaraSystem);
		P_GET_PROPERTY(FStrProperty,Z_Param_OverrideName);
		P_GET_OBJECT(AWaterBody,Z_Param_WaterBody);
		P_FINISH;
		P_NATIVE_BEGIN;
		UNiagaraWaterFunctionLibrary::SetWaterBody(Z_Param_NiagaraSystem,Z_Param_OverrideName,Z_Param_WaterBody);
		P_NATIVE_END;
	}
	void UNiagaraWaterFunctionLibrary::StaticRegisterNativesUNiagaraWaterFunctionLibrary()
	{
		UClass* Class = UNiagaraWaterFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SetWaterBody", &UNiagaraWaterFunctionLibrary::execSetWaterBody },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics
	{
		struct NiagaraWaterFunctionLibrary_eventSetWaterBody_Parms
		{
			UNiagaraComponent* NiagaraSystem;
			FString OverrideName;
			AWaterBody* WaterBody;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NiagaraSystem_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NiagaraSystem;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OverrideName;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterBody;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_NiagaraSystem_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_NiagaraSystem = { "NiagaraSystem", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraWaterFunctionLibrary_eventSetWaterBody_Parms, NiagaraSystem), Z_Construct_UClass_UNiagaraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_NiagaraSystem_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_NiagaraSystem_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_OverrideName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_OverrideName = { "OverrideName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraWaterFunctionLibrary_eventSetWaterBody_Parms, OverrideName), METADATA_PARAMS(Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_OverrideName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_OverrideName_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_WaterBody = { "WaterBody", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(NiagaraWaterFunctionLibrary_eventSetWaterBody_Parms, WaterBody), Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_NiagaraSystem,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_OverrideName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::NewProp_WaterBody,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Sets the water body on the Niagra water data interface on a Niagara particle system*/" },
		{ "DisplayName", "Set Water Body" },
		{ "ModuleRelativePath", "Public/NiagaraWaterFunctionLibrary.h" },
		{ "ToolTip", "Sets the water body on the Niagra water data interface on a Niagara particle system" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UNiagaraWaterFunctionLibrary, nullptr, "SetWaterBody", nullptr, nullptr, sizeof(NiagaraWaterFunctionLibrary_eventSetWaterBody_Parms), Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UNiagaraWaterFunctionLibrary_NoRegister()
	{
		return UNiagaraWaterFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UNiagaraWaterFunctionLibrary_SetWaterBody, "SetWaterBody" }, // 3085679982
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "NiagaraWaterFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/NiagaraWaterFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNiagaraWaterFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::ClassParams = {
		&UNiagaraWaterFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNiagaraWaterFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNiagaraWaterFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNiagaraWaterFunctionLibrary, 771448903);
	template<> WATER_API UClass* StaticClass<UNiagaraWaterFunctionLibrary>()
	{
		return UNiagaraWaterFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNiagaraWaterFunctionLibrary(Z_Construct_UClass_UNiagaraWaterFunctionLibrary, &UNiagaraWaterFunctionLibrary::StaticClass, TEXT("/Script/Water"), TEXT("UNiagaraWaterFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNiagaraWaterFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
