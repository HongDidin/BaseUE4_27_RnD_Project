// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Private/WaterBodyIslandActorFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyIslandActorFactory() {}
// Cross Module References
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyIslandActorFactory_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyIslandActorFactory();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
// End Cross Module References
	void UWaterBodyIslandActorFactory::StaticRegisterNativesUWaterBodyIslandActorFactory()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyIslandActorFactory_NoRegister()
	{
		return UWaterBodyIslandActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "WaterBodyIslandActorFactory.h" },
		{ "ModuleRelativePath", "Private/WaterBodyIslandActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyIslandActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics::ClassParams = {
		&UWaterBodyIslandActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyIslandActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyIslandActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyIslandActorFactory, 3697407672);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyIslandActorFactory>()
	{
		return UWaterBodyIslandActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyIslandActorFactory(Z_Construct_UClass_UWaterBodyIslandActorFactory, &UWaterBodyIslandActorFactory::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyIslandActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyIslandActorFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
