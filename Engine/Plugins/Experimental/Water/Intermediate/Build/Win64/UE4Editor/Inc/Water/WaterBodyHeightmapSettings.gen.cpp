// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyHeightmapSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyHeightmapSettings() {}
// Cross Module References
	WATER_API UEnum* Z_Construct_UEnum_Water_EWaterBrushBlendType();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterFalloffSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffects();
// End Cross Module References
	static UEnum* EWaterBrushBlendType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Water_EWaterBrushBlendType, Z_Construct_UPackage__Script_Water(), TEXT("EWaterBrushBlendType"));
		}
		return Singleton;
	}
	template<> WATER_API UEnum* StaticEnum<EWaterBrushBlendType>()
	{
		return EWaterBrushBlendType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EWaterBrushBlendType(EWaterBrushBlendType_StaticEnum, TEXT("/Script/Water"), TEXT("EWaterBrushBlendType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Water_EWaterBrushBlendType_Hash() { return 164017397U; }
	UEnum* Z_Construct_UEnum_Water_EWaterBrushBlendType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EWaterBrushBlendType"), 0, Get_Z_Construct_UEnum_Water_EWaterBrushBlendType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EWaterBrushBlendType::AlphaBlend", (int64)EWaterBrushBlendType::AlphaBlend },
				{ "EWaterBrushBlendType::Min", (int64)EWaterBrushBlendType::Min },
				{ "EWaterBrushBlendType::Max", (int64)EWaterBrushBlendType::Max },
				{ "EWaterBrushBlendType::Additive", (int64)EWaterBrushBlendType::Additive },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Additive.Comment", "/** Performs an additive blend, using a flat Z=0 terrain as the input. Useful when you want to preserve underlying detail or ramps. */" },
				{ "Additive.Name", "EWaterBrushBlendType::Additive" },
				{ "Additive.ToolTip", "Performs an additive blend, using a flat Z=0 terrain as the input. Useful when you want to preserve underlying detail or ramps." },
				{ "AlphaBlend.Comment", "/** Alpha Blend will affect the heightmap both upwards and downwards. */" },
				{ "AlphaBlend.Name", "EWaterBrushBlendType::AlphaBlend" },
				{ "AlphaBlend.ToolTip", "Alpha Blend will affect the heightmap both upwards and downwards." },
				{ "BlueprintType", "true" },
				{ "Comment", "/** The blend mode changes how the brush material is applied to the terrain. */" },
				{ "Max.Comment", "/** Limits the brush to only raising the terrain. */" },
				{ "Max.Name", "EWaterBrushBlendType::Max" },
				{ "Max.ToolTip", "Limits the brush to only raising the terrain." },
				{ "Min.Comment", "/** Limits the brush to only lowering the terrain. */" },
				{ "Min.Name", "EWaterBrushBlendType::Min" },
				{ "Min.ToolTip", "Limits the brush to only lowering the terrain." },
				{ "ModuleRelativePath", "Public/WaterBodyHeightmapSettings.h" },
				{ "ToolTip", "The blend mode changes how the brush material is applied to the terrain." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Water,
				nullptr,
				"EWaterBrushBlendType",
				"EWaterBrushBlendType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FWaterBodyHeightmapSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings, Z_Construct_UPackage__Script_Water(), TEXT("WaterBodyHeightmapSettings"), sizeof(FWaterBodyHeightmapSettings), Get_Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBodyHeightmapSettings>()
{
	return FWaterBodyHeightmapSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyHeightmapSettings(FWaterBodyHeightmapSettings::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBodyHeightmapSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBodyHeightmapSettings
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBodyHeightmapSettings()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyHeightmapSettings>(FName(TEXT("WaterBodyHeightmapSettings")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBodyHeightmapSettings;
	struct Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BlendMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BlendMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvertShape_MetaData[];
#endif
		static void NewProp_bInvertShape_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvertShape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FalloffSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Effects_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Effects;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Priority_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Priority;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyHeightmapSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyHeightmapSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_BlendMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_BlendMode_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyHeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_BlendMode = { "BlendMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyHeightmapSettings, BlendMode), Z_Construct_UEnum_Water_EWaterBrushBlendType, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_BlendMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_BlendMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_bInvertShape_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyHeightmapSettings.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_bInvertShape_SetBit(void* Obj)
	{
		((FWaterBodyHeightmapSettings*)Obj)->bInvertShape = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_bInvertShape = { "bInvertShape", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FWaterBodyHeightmapSettings), &Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_bInvertShape_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_bInvertShape_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_bInvertShape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_FalloffSettings_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyHeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_FalloffSettings = { "FalloffSettings", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyHeightmapSettings, FalloffSettings), Z_Construct_UScriptStruct_FWaterFalloffSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_FalloffSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_FalloffSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Effects_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyHeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Effects = { "Effects", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyHeightmapSettings, Effects), Z_Construct_UScriptStruct_FWaterBrushEffects, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Effects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Effects_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Priority_MetaData[] = {
		{ "Category", "TerrainCarvingSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyHeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Priority = { "Priority", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyHeightmapSettings, Priority), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Priority_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Priority_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_BlendMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_BlendMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_bInvertShape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_FalloffSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Effects,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::NewProp_Priority,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBodyHeightmapSettings",
		sizeof(FWaterBodyHeightmapSettings),
		alignof(FWaterBodyHeightmapSettings),
		Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyHeightmapSettings"), sizeof(FWaterBodyHeightmapSettings), Get_Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings_Hash() { return 2078896477U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
