// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/ConvertWaterBodyActorsCommandlet.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConvertWaterBodyActorsCommandlet() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UConvertWaterBodyActorsCommandlet();
	ENGINE_API UClass* Z_Construct_UClass_UCommandlet();
	UPackage* Z_Construct_UPackage__Script_Water();
// End Cross Module References
	void UConvertWaterBodyActorsCommandlet::StaticRegisterNativesUConvertWaterBodyActorsCommandlet()
	{
	}
	UClass* Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_NoRegister()
	{
		return UConvertWaterBodyActorsCommandlet::StaticClass();
	}
	struct Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCommandlet,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ConvertWaterBodyActorsCommandlet.h" },
		{ "ModuleRelativePath", "Public/ConvertWaterBodyActorsCommandlet.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConvertWaterBodyActorsCommandlet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics::ClassParams = {
		&UConvertWaterBodyActorsCommandlet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A8u,
		METADATA_PARAMS(Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConvertWaterBodyActorsCommandlet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConvertWaterBodyActorsCommandlet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConvertWaterBodyActorsCommandlet, 860592792);
	template<> WATER_API UClass* StaticClass<UConvertWaterBodyActorsCommandlet>()
	{
		return UConvertWaterBodyActorsCommandlet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConvertWaterBodyActorsCommandlet(Z_Construct_UClass_UConvertWaterBodyActorsCommandlet, &UConvertWaterBodyActorsCommandlet::StaticClass, TEXT("/Script/Water"), TEXT("UConvertWaterBodyActorsCommandlet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConvertWaterBodyActorsCommandlet);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
