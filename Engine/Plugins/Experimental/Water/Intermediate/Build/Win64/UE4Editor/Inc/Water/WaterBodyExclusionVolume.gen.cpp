// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyExclusionVolume.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyExclusionVolume() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_AWaterBodyExclusionVolume_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyExclusionVolume();
	ENGINE_API UClass* Z_Construct_UClass_APhysicsVolume();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UBillboardComponent_NoRegister();
// End Cross Module References
	void AWaterBodyExclusionVolume::StaticRegisterNativesAWaterBodyExclusionVolume()
	{
	}
	UClass* Z_Construct_UClass_AWaterBodyExclusionVolume_NoRegister()
	{
		return AWaterBodyExclusionVolume::StaticClass();
	}
	struct Z_Construct_UClass_AWaterBodyExclusionVolume_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreAllOverlappingWaterBodies_MetaData[];
#endif
		static void NewProp_bIgnoreAllOverlappingWaterBodies_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreAllOverlappingWaterBodies;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterBodiesToIgnore_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodiesToIgnore_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_WaterBodiesToIgnore;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyToIgnore_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WaterBodyToIgnore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorIcon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorIcon;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APhysicsVolume,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * WaterBodyExclusionVolume allows players not enter surface swimming when touching a water volume\n */" },
		{ "HideCategories", "Brush Physics Object Blueprint Display Rendering Physics Input" },
		{ "IncludePath", "WaterBodyExclusionVolume.h" },
		{ "ModuleRelativePath", "Public/WaterBodyExclusionVolume.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
		{ "ToolTip", "WaterBodyExclusionVolume allows players not enter surface swimming when touching a water volume" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_bIgnoreAllOverlappingWaterBodies_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** If checked, all water bodies overlapping with this exclusion volumes will be affected. */" },
		{ "ModuleRelativePath", "Public/WaterBodyExclusionVolume.h" },
		{ "ToolTip", "If checked, all water bodies overlapping with this exclusion volumes will be affected." },
	};
#endif
	void Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_bIgnoreAllOverlappingWaterBodies_SetBit(void* Obj)
	{
		((AWaterBodyExclusionVolume*)Obj)->bIgnoreAllOverlappingWaterBodies = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_bIgnoreAllOverlappingWaterBodies = { "bIgnoreAllOverlappingWaterBodies", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AWaterBodyExclusionVolume), &Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_bIgnoreAllOverlappingWaterBodies_SetBit, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_bIgnoreAllOverlappingWaterBodies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_bIgnoreAllOverlappingWaterBodies_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodiesToIgnore_Inner = { "WaterBodiesToIgnore", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodiesToIgnore_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** List of water bodies that will be affected by this exclusion volume */" },
		{ "EditCondition", "!bIgnoreAllOverlappingWaterBodies" },
		{ "ModuleRelativePath", "Public/WaterBodyExclusionVolume.h" },
		{ "ToolTip", "List of water bodies that will be affected by this exclusion volume" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodiesToIgnore = { "WaterBodiesToIgnore", nullptr, (EPropertyFlags)0x0010000000000801, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyExclusionVolume, WaterBodiesToIgnore), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodiesToIgnore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodiesToIgnore_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodyToIgnore_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "ModuleRelativePath", "Public/WaterBodyExclusionVolume.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodyToIgnore = { "WaterBodyToIgnore", nullptr, (EPropertyFlags)0x0010000820000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyExclusionVolume, WaterBodyToIgnore_DEPRECATED), Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodyToIgnore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodyToIgnore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_ActorIcon_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyExclusionVolume.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_ActorIcon = { "ActorIcon", nullptr, (EPropertyFlags)0x0010000800082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyExclusionVolume, ActorIcon), Z_Construct_UClass_UBillboardComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_ActorIcon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_ActorIcon_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_bIgnoreAllOverlappingWaterBodies,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodiesToIgnore_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodiesToIgnore,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_WaterBodyToIgnore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::NewProp_ActorIcon,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterBodyExclusionVolume>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::ClassParams = {
		&AWaterBodyExclusionVolume::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterBodyExclusionVolume()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterBodyExclusionVolume_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterBodyExclusionVolume, 84246008);
	template<> WATER_API UClass* StaticClass<AWaterBodyExclusionVolume>()
	{
		return AWaterBodyExclusionVolume::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterBodyExclusionVolume(Z_Construct_UClass_AWaterBodyExclusionVolume, &AWaterBodyExclusionVolume::StaticClass, TEXT("/Script/Water"), TEXT("AWaterBodyExclusionVolume"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterBodyExclusionVolume);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
