// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyWeightmapSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyWeightmapSettings() {}
// Cross Module References
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
// End Cross Module References
class UScriptStruct* FWaterBodyWeightmapSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings, Z_Construct_UPackage__Script_Water(), TEXT("WaterBodyWeightmapSettings"), sizeof(FWaterBodyWeightmapSettings), Get_Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBodyWeightmapSettings>()
{
	return FWaterBodyWeightmapSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBodyWeightmapSettings(FWaterBodyWeightmapSettings::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBodyWeightmapSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBodyWeightmapSettings
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBodyWeightmapSettings()
	{
		UScriptStruct::DeferCppStructOps<FWaterBodyWeightmapSettings>(FName(TEXT("WaterBodyWeightmapSettings")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBodyWeightmapSettings;
	struct Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FalloffWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EdgeOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EdgeOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModulationTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ModulationTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureTiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TextureTiling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureInfluence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TextureInfluence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Midpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Midpoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalOpacity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FinalOpacity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBodyWeightmapSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FalloffWidth_MetaData[] = {
		{ "Category", "WaterBodyWeightmapSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FalloffWidth = { "FalloffWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyWeightmapSettings, FalloffWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FalloffWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FalloffWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_EdgeOffset_MetaData[] = {
		{ "Category", "WaterBodyWeightmapSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_EdgeOffset = { "EdgeOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyWeightmapSettings, EdgeOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_EdgeOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_EdgeOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_ModulationTexture_MetaData[] = {
		{ "Category", "WaterBodyWeightmapSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_ModulationTexture = { "ModulationTexture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyWeightmapSettings, ModulationTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_ModulationTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_ModulationTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureTiling_MetaData[] = {
		{ "Category", "WaterBodyWeightmapSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureTiling = { "TextureTiling", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyWeightmapSettings, TextureTiling), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureTiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureTiling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureInfluence_MetaData[] = {
		{ "Category", "WaterBodyWeightmapSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureInfluence = { "TextureInfluence", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyWeightmapSettings, TextureInfluence), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureInfluence_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureInfluence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_Midpoint_MetaData[] = {
		{ "Category", "WaterBodyWeightmapSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_Midpoint = { "Midpoint", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyWeightmapSettings, Midpoint), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_Midpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_Midpoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FinalOpacity_MetaData[] = {
		{ "Category", "WaterBodyWeightmapSettings" },
		{ "ModuleRelativePath", "Public/WaterBodyWeightmapSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FinalOpacity = { "FinalOpacity", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBodyWeightmapSettings, FinalOpacity), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FinalOpacity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FinalOpacity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FalloffWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_EdgeOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_ModulationTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureTiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_TextureInfluence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_Midpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::NewProp_FinalOpacity,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBodyWeightmapSettings",
		sizeof(FWaterBodyWeightmapSettings),
		alignof(FWaterBodyWeightmapSettings),
		Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBodyWeightmapSettings"), sizeof(FWaterBodyWeightmapSettings), Get_Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings_Hash() { return 1404710108U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
