// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBrushEffects.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBrushEffects() {}
// Cross Module References
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffects();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectBlurring();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectTerracing();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectCurves();
	ENGINE_API UClass* Z_Construct_UClass_UCurveFloat_NoRegister();
// End Cross Module References
class UScriptStruct* FWaterBrushEffects::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffects_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushEffects, Z_Construct_UPackage__Script_Water(), TEXT("WaterBrushEffects"), sizeof(FWaterBrushEffects), Get_Z_Construct_UScriptStruct_FWaterBrushEffects_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBrushEffects>()
{
	return FWaterBrushEffects::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushEffects(FWaterBrushEffects::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBrushEffects"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffects
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffects()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushEffects>(FName(TEXT("WaterBrushEffects")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBrushEffects;
	struct Z_Construct_UScriptStruct_FWaterBrushEffects_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Blurring_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Blurring;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurlNoise_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurlNoise;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Displacement_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Displacement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothBlending_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SmoothBlending;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Terracing_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Terracing;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushEffects>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Blurring_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Blurring = { "Blurring", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffects, Blurring), Z_Construct_UScriptStruct_FWaterBrushEffectBlurring, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Blurring_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Blurring_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_CurlNoise_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_CurlNoise = { "CurlNoise", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffects, CurlNoise), Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_CurlNoise_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_CurlNoise_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Displacement_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Displacement = { "Displacement", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffects, Displacement), Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Displacement_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Displacement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_SmoothBlending_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_SmoothBlending = { "SmoothBlending", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffects, SmoothBlending), Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_SmoothBlending_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_SmoothBlending_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Terracing_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Terracing = { "Terracing", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffects, Terracing), Z_Construct_UScriptStruct_FWaterBrushEffectTerracing, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Terracing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Terracing_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Blurring,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_CurlNoise,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Displacement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_SmoothBlending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::NewProp_Terracing,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBrushEffects",
		sizeof(FWaterBrushEffects),
		alignof(FWaterBrushEffects),
		Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffects()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffects_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushEffects"), sizeof(FWaterBrushEffects), Get_Z_Construct_UScriptStruct_FWaterBrushEffects_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushEffects_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffects_Hash() { return 3124524488U; }
class UScriptStruct* FWaterBrushEffectTerracing::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing, Z_Construct_UPackage__Script_Water(), TEXT("WaterBrushEffectTerracing"), sizeof(FWaterBrushEffectTerracing), Get_Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBrushEffectTerracing>()
{
	return FWaterBrushEffectTerracing::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushEffectTerracing(FWaterBrushEffectTerracing::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBrushEffectTerracing"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectTerracing
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectTerracing()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushEffectTerracing>(FName(TEXT("WaterBrushEffectTerracing")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectTerracing;
	struct Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerraceAlpha_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TerraceAlpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerraceSpacing_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TerraceSpacing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TerraceSmoothness_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TerraceSmoothness;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaskLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaskLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaskStartOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaskStartOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushEffectTerracing>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceAlpha_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceAlpha = { "TerraceAlpha", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectTerracing, TerraceAlpha), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceAlpha_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceAlpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSpacing_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSpacing = { "TerraceSpacing", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectTerracing, TerraceSpacing), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSpacing_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSpacing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSmoothness_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSmoothness = { "TerraceSmoothness", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectTerracing, TerraceSmoothness), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSmoothness_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSmoothness_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskLength_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskLength = { "MaskLength", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectTerracing, MaskLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskStartOffset_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskStartOffset = { "MaskStartOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectTerracing, MaskStartOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskStartOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskStartOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceAlpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSpacing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_TerraceSmoothness,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::NewProp_MaskStartOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBrushEffectTerracing",
		sizeof(FWaterBrushEffectTerracing),
		alignof(FWaterBrushEffectTerracing),
		Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectTerracing()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushEffectTerracing"), sizeof(FWaterBrushEffectTerracing), Get_Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectTerracing_Hash() { return 2640108308U; }
class UScriptStruct* FWaterBrushEffectSmoothBlending::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending, Z_Construct_UPackage__Script_Water(), TEXT("WaterBrushEffectSmoothBlending"), sizeof(FWaterBrushEffectSmoothBlending), Get_Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBrushEffectSmoothBlending>()
{
	return FWaterBrushEffectSmoothBlending::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushEffectSmoothBlending(FWaterBrushEffectSmoothBlending::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBrushEffectSmoothBlending"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectSmoothBlending
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectSmoothBlending()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushEffectSmoothBlending>(FName(TEXT("WaterBrushEffectSmoothBlending")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectSmoothBlending;
	struct Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerSmoothDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InnerSmoothDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OuterSmoothDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OuterSmoothDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushEffectSmoothBlending>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance = { "InnerSmoothDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectSmoothBlending, InnerSmoothDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance = { "OuterSmoothDistance", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectSmoothBlending, OuterSmoothDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_InnerSmoothDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::NewProp_OuterSmoothDistance,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBrushEffectSmoothBlending",
		sizeof(FWaterBrushEffectSmoothBlending),
		alignof(FWaterBrushEffectSmoothBlending),
		Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushEffectSmoothBlending"), sizeof(FWaterBrushEffectSmoothBlending), Get_Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectSmoothBlending_Hash() { return 47512592U; }
class UScriptStruct* FWaterBrushEffectDisplacement::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement, Z_Construct_UPackage__Script_Water(), TEXT("WaterBrushEffectDisplacement"), sizeof(FWaterBrushEffectDisplacement), Get_Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBrushEffectDisplacement>()
{
	return FWaterBrushEffectDisplacement::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushEffectDisplacement(FWaterBrushEffectDisplacement::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBrushEffectDisplacement"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectDisplacement
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectDisplacement()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushEffectDisplacement>(FName(TEXT("WaterBrushEffectDisplacement")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectDisplacement;
	struct Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DisplacementHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplacementTiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DisplacementTiling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Texture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Midpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Midpoint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channel_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Channel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightmapInfluence_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WeightmapInfluence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushEffectDisplacement>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementHeight_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementHeight = { "DisplacementHeight", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectDisplacement, DisplacementHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementTiling_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementTiling = { "DisplacementTiling", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectDisplacement, DisplacementTiling), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementTiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementTiling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Texture_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectDisplacement, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Texture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Texture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Midpoint_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Midpoint = { "Midpoint", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectDisplacement, Midpoint), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Midpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Midpoint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Channel_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Channel = { "Channel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectDisplacement, Channel), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Channel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Channel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence = { "WeightmapInfluence", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectDisplacement, WeightmapInfluence), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_DisplacementTiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Texture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Midpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_Channel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::NewProp_WeightmapInfluence,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBrushEffectDisplacement",
		sizeof(FWaterBrushEffectDisplacement),
		alignof(FWaterBrushEffectDisplacement),
		Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushEffectDisplacement"), sizeof(FWaterBrushEffectDisplacement), Get_Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectDisplacement_Hash() { return 4092585489U; }
class UScriptStruct* FWaterBrushEffectCurves::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushEffectCurves, Z_Construct_UPackage__Script_Water(), TEXT("WaterBrushEffectCurves"), sizeof(FWaterBrushEffectCurves), Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBrushEffectCurves>()
{
	return FWaterBrushEffectCurves::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushEffectCurves(FWaterBrushEffectCurves::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBrushEffectCurves"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectCurves
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectCurves()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushEffectCurves>(FName(TEXT("WaterBrushEffectCurves")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectCurves;
	struct Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCurveChannel_MetaData[];
#endif
		static void NewProp_bUseCurveChannel_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCurveChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElevationCurveAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ElevationCurveAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelEdgeOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChannelEdgeOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ChannelDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurveRampWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurveRampWidth;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushEffectCurves>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_bUseCurveChannel_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_bUseCurveChannel_SetBit(void* Obj)
	{
		((FWaterBrushEffectCurves*)Obj)->bUseCurveChannel = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_bUseCurveChannel = { "bUseCurveChannel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FWaterBrushEffectCurves), &Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_bUseCurveChannel_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_bUseCurveChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_bUseCurveChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ElevationCurveAsset_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ElevationCurveAsset = { "ElevationCurveAsset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurves, ElevationCurveAsset), Z_Construct_UClass_UCurveFloat_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ElevationCurveAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ElevationCurveAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset = { "ChannelEdgeOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurves, ChannelEdgeOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelDepth_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelDepth = { "ChannelDepth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurves, ChannelDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_CurveRampWidth_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_CurveRampWidth = { "CurveRampWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurves, CurveRampWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_CurveRampWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_CurveRampWidth_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_bUseCurveChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ElevationCurveAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelEdgeOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_ChannelDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::NewProp_CurveRampWidth,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBrushEffectCurves",
		sizeof(FWaterBrushEffectCurves),
		alignof(FWaterBrushEffectCurves),
		Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectCurves()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushEffectCurves"), sizeof(FWaterBrushEffectCurves), Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurves_Hash() { return 1938451377U; }
class UScriptStruct* FWaterBrushEffectCurlNoise::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise, Z_Construct_UPackage__Script_Water(), TEXT("WaterBrushEffectCurlNoise"), sizeof(FWaterBrushEffectCurlNoise), Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBrushEffectCurlNoise>()
{
	return FWaterBrushEffectCurlNoise::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushEffectCurlNoise(FWaterBrushEffectCurlNoise::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBrushEffectCurlNoise"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectCurlNoise
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectCurlNoise()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushEffectCurlNoise>(FName(TEXT("WaterBrushEffectCurlNoise")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectCurlNoise;
	struct Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl1Amount_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl1Amount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl2Amount_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl2Amount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl1Tiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl1Tiling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Curl2Tiling_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Curl2Tiling;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushEffectCurlNoise>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Amount_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Amount = { "Curl1Amount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurlNoise, Curl1Amount), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Amount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Amount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Amount_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Amount = { "Curl2Amount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurlNoise, Curl2Amount), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Amount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Amount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling = { "Curl1Tiling", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurlNoise, Curl1Tiling), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling = { "Curl2Tiling", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectCurlNoise, Curl2Tiling), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Amount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Amount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl1Tiling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::NewProp_Curl2Tiling,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBrushEffectCurlNoise",
		sizeof(FWaterBrushEffectCurlNoise),
		alignof(FWaterBrushEffectCurlNoise),
		Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushEffectCurlNoise"), sizeof(FWaterBrushEffectCurlNoise), Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectCurlNoise_Hash() { return 1094043275U; }
class UScriptStruct* FWaterBrushEffectBlurring::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring, Z_Construct_UPackage__Script_Water(), TEXT("WaterBrushEffectBlurring"), sizeof(FWaterBrushEffectBlurring), Get_Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterBrushEffectBlurring>()
{
	return FWaterBrushEffectBlurring::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterBrushEffectBlurring(FWaterBrushEffectBlurring::StaticStruct, TEXT("/Script/Water"), TEXT("WaterBrushEffectBlurring"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectBlurring
{
	FScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectBlurring()
	{
		UScriptStruct::DeferCppStructOps<FWaterBrushEffectBlurring>(FName(TEXT("WaterBrushEffectBlurring")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterBrushEffectBlurring;
	struct Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBlurShape_MetaData[];
#endif
		static void NewProp_bBlurShape_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBlurShape;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Radius;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterBrushEffectBlurring>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_bBlurShape_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_bBlurShape_SetBit(void* Obj)
	{
		((FWaterBrushEffectBlurring*)Obj)->bBlurShape = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_bBlurShape = { "bBlurShape", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FWaterBrushEffectBlurring), &Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_bBlurShape_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_bBlurShape_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_bBlurShape_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "BrushEffects" },
		{ "ModuleRelativePath", "Public/WaterBrushEffects.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterBrushEffectBlurring, Radius), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_Radius_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_bBlurShape,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::NewProp_Radius,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterBrushEffectBlurring",
		sizeof(FWaterBrushEffectBlurring),
		alignof(FWaterBrushEffectBlurring),
		Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterBrushEffectBlurring()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterBrushEffectBlurring"), sizeof(FWaterBrushEffectBlurring), Get_Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterBrushEffectBlurring_Hash() { return 1011238080U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
