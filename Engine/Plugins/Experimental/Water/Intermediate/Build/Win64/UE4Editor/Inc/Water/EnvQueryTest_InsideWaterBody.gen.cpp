// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/EnvQueryTest_InsideWaterBody.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEnvQueryTest_InsideWaterBody() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UEnvQueryTest_InsideWaterBody();
	AIMODULE_API UClass* Z_Construct_UClass_UEnvQueryTest();
	UPackage* Z_Construct_UPackage__Script_Water();
// End Cross Module References
	void UEnvQueryTest_InsideWaterBody::StaticRegisterNativesUEnvQueryTest_InsideWaterBody()
	{
	}
	UClass* Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_NoRegister()
	{
		return UEnvQueryTest_InsideWaterBody::StaticClass();
	}
	struct Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIncludeWaves_MetaData[];
#endif
		static void NewProp_bIncludeWaves_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIncludeWaves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSimpleWaves_MetaData[];
#endif
		static void NewProp_bSimpleWaves_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSimpleWaves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreExclusionVolumes_MetaData[];
#endif
		static void NewProp_bIgnoreExclusionVolumes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreExclusionVolumes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEnvQueryTest,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EnvQueryTest_InsideWaterBody.h" },
		{ "ModuleRelativePath", "Public/EnvQueryTest_InsideWaterBody.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIncludeWaves_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Whether waves should be taken into account in the query. */" },
		{ "ModuleRelativePath", "Public/EnvQueryTest_InsideWaterBody.h" },
		{ "ToolTip", "Whether waves should be taken into account in the query." },
	};
#endif
	void Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIncludeWaves_SetBit(void* Obj)
	{
		((UEnvQueryTest_InsideWaterBody*)Obj)->bIncludeWaves = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIncludeWaves = { "bIncludeWaves", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEnvQueryTest_InsideWaterBody), &Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIncludeWaves_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIncludeWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIncludeWaves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bSimpleWaves_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Use the simple (faster) version of waves computation. */" },
		{ "EditCondition", "!bIncludeWaves" },
		{ "ModuleRelativePath", "Public/EnvQueryTest_InsideWaterBody.h" },
		{ "ToolTip", "Use the simple (faster) version of waves computation." },
	};
#endif
	void Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bSimpleWaves_SetBit(void* Obj)
	{
		((UEnvQueryTest_InsideWaterBody*)Obj)->bSimpleWaves = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bSimpleWaves = { "bSimpleWaves", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEnvQueryTest_InsideWaterBody), &Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bSimpleWaves_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bSimpleWaves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bSimpleWaves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIgnoreExclusionVolumes_MetaData[] = {
		{ "Category", "Water" },
		{ "Comment", "/** Whether water body exclusion volumes should be taken into account in the query. */" },
		{ "ModuleRelativePath", "Public/EnvQueryTest_InsideWaterBody.h" },
		{ "ToolTip", "Whether water body exclusion volumes should be taken into account in the query." },
	};
#endif
	void Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIgnoreExclusionVolumes_SetBit(void* Obj)
	{
		((UEnvQueryTest_InsideWaterBody*)Obj)->bIgnoreExclusionVolumes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIgnoreExclusionVolumes = { "bIgnoreExclusionVolumes", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UEnvQueryTest_InsideWaterBody), &Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIgnoreExclusionVolumes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIgnoreExclusionVolumes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIgnoreExclusionVolumes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIncludeWaves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bSimpleWaves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::NewProp_bIgnoreExclusionVolumes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEnvQueryTest_InsideWaterBody>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::ClassParams = {
		&UEnvQueryTest_InsideWaterBody::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEnvQueryTest_InsideWaterBody()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEnvQueryTest_InsideWaterBody_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEnvQueryTest_InsideWaterBody, 3279690023);
	template<> WATER_API UClass* StaticClass<UEnvQueryTest_InsideWaterBody>()
	{
		return UEnvQueryTest_InsideWaterBody::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEnvQueryTest_InsideWaterBody(Z_Construct_UClass_UEnvQueryTest_InsideWaterBody, &UEnvQueryTest_InsideWaterBody::StaticClass, TEXT("/Script/Water"), TEXT("UEnvQueryTest_InsideWaterBody"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEnvQueryTest_InsideWaterBody);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
