// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyOceanActor.h"
#include "Runtime/Public/WaterBodyActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyOceanActor() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UOceanGenerator_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UOceanGenerator();
	WATER_API UClass* Z_Construct_UClass_UWaterBodyGenerator();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UClass* Z_Construct_UClass_UOceanBoxCollisionComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UOceanCollisionComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyOcean_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyOcean();
	WATER_API UClass* Z_Construct_UClass_AWaterBody();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void UOceanGenerator::StaticRegisterNativesUOceanGenerator()
	{
	}
	UClass* Z_Construct_UClass_UOceanGenerator_NoRegister()
	{
		return UOceanGenerator::StaticClass();
	}
	struct Z_Construct_UClass_UOceanGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionBoxes_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionBoxes_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CollisionBoxes;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CollisionHullSets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionHullSets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CollisionHullSets;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UOceanGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOceanGenerator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "IncludePath", "WaterBodyOceanActor.h" },
		{ "ModuleRelativePath", "Public/WaterBodyOceanActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionBoxes_Inner = { "CollisionBoxes", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UOceanBoxCollisionComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionBoxes_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyOceanActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionBoxes = { "CollisionBoxes", nullptr, (EPropertyFlags)0x0040808000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOceanGenerator, CollisionBoxes), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionBoxes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionBoxes_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionHullSets_Inner = { "CollisionHullSets", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UOceanCollisionComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionHullSets_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyOceanActor.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionHullSets = { "CollisionHullSets", nullptr, (EPropertyFlags)0x0040808000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UOceanGenerator, CollisionHullSets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionHullSets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionHullSets_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UOceanGenerator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionBoxes_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionBoxes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionHullSets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UOceanGenerator_Statics::NewProp_CollisionHullSets,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UOceanGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UOceanGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UOceanGenerator_Statics::ClassParams = {
		&UOceanGenerator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UOceanGenerator_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UOceanGenerator_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UOceanGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UOceanGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UOceanGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UOceanGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UOceanGenerator, 3084804366);
	template<> WATER_API UClass* StaticClass<UOceanGenerator>()
	{
		return UOceanGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UOceanGenerator(Z_Construct_UClass_UOceanGenerator, &UOceanGenerator::StaticClass, TEXT("/Script/Water"), TEXT("UOceanGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UOceanGenerator);
	void AWaterBodyOcean::StaticRegisterNativesAWaterBodyOcean()
	{
	}
	UClass* Z_Construct_UClass_AWaterBodyOcean_NoRegister()
	{
		return AWaterBodyOcean::StaticClass();
	}
	struct Z_Construct_UClass_AWaterBodyOcean_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OceanGenerator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OceanGenerator;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CollisionExtents_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CollisionExtents;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HeightOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HeightOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterBodyOcean_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AWaterBody,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyOcean_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "HideCategories", "Tags Activation Cooking Replication Input Actor AssetUserData" },
		{ "IncludePath", "WaterBodyOceanActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyOceanActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_OceanGenerator_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyOceanActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_OceanGenerator = { "OceanGenerator", nullptr, (EPropertyFlags)0x0020880000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyOcean, OceanGenerator), Z_Construct_UClass_UOceanGenerator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_OceanGenerator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_OceanGenerator_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_CollisionExtents_MetaData[] = {
		{ "Category", "Collision" },
		{ "ModuleRelativePath", "Public/WaterBodyOceanActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_CollisionExtents = { "CollisionExtents", nullptr, (EPropertyFlags)0x0020080000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyOcean, CollisionExtents), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_CollisionExtents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_CollisionExtents_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_HeightOffset_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyOceanActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_HeightOffset = { "HeightOffset", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyOcean, HeightOffset), METADATA_PARAMS(Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_HeightOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_HeightOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterBodyOcean_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_OceanGenerator,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_CollisionExtents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyOcean_Statics::NewProp_HeightOffset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterBodyOcean_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterBodyOcean>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterBodyOcean_Statics::ClassParams = {
		&AWaterBodyOcean::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWaterBodyOcean_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyOcean_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterBodyOcean_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyOcean_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterBodyOcean()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterBodyOcean_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterBodyOcean, 1430828942);
	template<> WATER_API UClass* StaticClass<AWaterBodyOcean>()
	{
		return AWaterBodyOcean::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterBodyOcean(Z_Construct_UClass_AWaterBodyOcean, &AWaterBodyOcean::StaticClass, TEXT("/Script/Water"), TEXT("AWaterBodyOcean"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterBodyOcean);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
