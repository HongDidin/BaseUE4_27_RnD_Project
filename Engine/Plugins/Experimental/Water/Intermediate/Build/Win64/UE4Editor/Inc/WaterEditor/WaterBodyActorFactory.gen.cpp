// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Private/WaterBodyActorFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyActorFactory() {}
// Cross Module References
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyActorFactory_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyActorFactory();
	UNREALED_API UClass* Z_Construct_UClass_UActorFactory();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyRiverActorFactory_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyRiverActorFactory();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyOceanActorFactory_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyOceanActorFactory();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyLakeActorFactory_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyLakeActorFactory();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyCustomActorFactory_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyCustomActorFactory();
// End Cross Module References
	void UWaterBodyActorFactory::StaticRegisterNativesUWaterBodyActorFactory()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyActorFactory_NoRegister()
	{
		return UWaterBodyActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "WaterBodyActorFactory.h" },
		{ "ModuleRelativePath", "Private/WaterBodyActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyActorFactory_Statics::ClassParams = {
		&UWaterBodyActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ADu,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyActorFactory, 2731690928);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyActorFactory>()
	{
		return UWaterBodyActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyActorFactory(Z_Construct_UClass_UWaterBodyActorFactory, &UWaterBodyActorFactory::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyActorFactory);
	void UWaterBodyRiverActorFactory::StaticRegisterNativesUWaterBodyRiverActorFactory()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyRiverActorFactory_NoRegister()
	{
		return UWaterBodyRiverActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "WaterBodyActorFactory.h" },
		{ "ModuleRelativePath", "Private/WaterBodyActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyRiverActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics::ClassParams = {
		&UWaterBodyRiverActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyRiverActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyRiverActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyRiverActorFactory, 1943494728);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyRiverActorFactory>()
	{
		return UWaterBodyRiverActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyRiverActorFactory(Z_Construct_UClass_UWaterBodyRiverActorFactory, &UWaterBodyRiverActorFactory::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyRiverActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyRiverActorFactory);
	void UWaterBodyOceanActorFactory::StaticRegisterNativesUWaterBodyOceanActorFactory()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyOceanActorFactory_NoRegister()
	{
		return UWaterBodyOceanActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "WaterBodyActorFactory.h" },
		{ "ModuleRelativePath", "Private/WaterBodyActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyOceanActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics::ClassParams = {
		&UWaterBodyOceanActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyOceanActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyOceanActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyOceanActorFactory, 1791524089);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyOceanActorFactory>()
	{
		return UWaterBodyOceanActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyOceanActorFactory(Z_Construct_UClass_UWaterBodyOceanActorFactory, &UWaterBodyOceanActorFactory::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyOceanActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyOceanActorFactory);
	void UWaterBodyLakeActorFactory::StaticRegisterNativesUWaterBodyLakeActorFactory()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyLakeActorFactory_NoRegister()
	{
		return UWaterBodyLakeActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "WaterBodyActorFactory.h" },
		{ "ModuleRelativePath", "Private/WaterBodyActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyLakeActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics::ClassParams = {
		&UWaterBodyLakeActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyLakeActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyLakeActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyLakeActorFactory, 2067822741);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyLakeActorFactory>()
	{
		return UWaterBodyLakeActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyLakeActorFactory(Z_Construct_UClass_UWaterBodyLakeActorFactory, &UWaterBodyLakeActorFactory::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyLakeActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyLakeActorFactory);
	void UWaterBodyCustomActorFactory::StaticRegisterNativesUWaterBodyCustomActorFactory()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyCustomActorFactory_NoRegister()
	{
		return UWaterBodyCustomActorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyActorFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object" },
		{ "IncludePath", "WaterBodyActorFactory.h" },
		{ "ModuleRelativePath", "Private/WaterBodyActorFactory.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyCustomActorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics::ClassParams = {
		&UWaterBodyCustomActorFactory::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000830ACu,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyCustomActorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyCustomActorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyCustomActorFactory, 3899591097);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyCustomActorFactory>()
	{
		return UWaterBodyCustomActorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyCustomActorFactory(Z_Construct_UClass_UWaterBodyCustomActorFactory, &UWaterBodyCustomActorFactory::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyCustomActorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyCustomActorFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
