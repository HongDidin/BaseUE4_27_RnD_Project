// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWater_init() {}
	WATER_API UFunction* Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature();
	WATER_API UFunction* Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature();
	WATER_API UFunction* Z_Construct_UDelegateFunction_Water_OnCameraUnderwaterStateChanged__DelegateSignature();
	WATER_API UFunction* Z_Construct_UDelegateFunction_Water_OnWaterScalabilityChanged__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_Water()
	{
		static UPackage* ReturnPackage = nullptr;
		if (!ReturnPackage)
		{
			static UObject* (*const SingletonFuncArray[])() = {
				(UObject* (*)())Z_Construct_UDelegateFunction_Water_OnPontoonEnteredWater__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Water_OnPontoonExitedWater__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Water_OnCameraUnderwaterStateChanged__DelegateSignature,
				(UObject* (*)())Z_Construct_UDelegateFunction_Water_OnWaterScalabilityChanged__DelegateSignature,
			};
			static const UE4CodeGen_Private::FPackageParams PackageParams = {
				"/Script/Water",
				SingletonFuncArray,
				UE_ARRAY_COUNT(SingletonFuncArray),
				PKG_CompiledIn | 0x00000000,
				0xABAD14B4,
				0x83B5F7EE,
				METADATA_PARAMS(nullptr, 0)
			};
			UE4CodeGen_Private::ConstructUPackage(ReturnPackage, PackageParams);
		}
		return ReturnPackage;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
