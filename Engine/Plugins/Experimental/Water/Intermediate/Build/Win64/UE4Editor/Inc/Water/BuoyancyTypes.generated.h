// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef WATER_BuoyancyTypes_generated_h
#error "BuoyancyTypes.generated.h already included, missing '#pragma once' in BuoyancyTypes.h"
#endif
#define WATER_BuoyancyTypes_generated_h

#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyTypes_h_172_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FBuoyancyData_Statics; \
	WATER_API static class UScriptStruct* StaticStruct();


template<> WATER_API UScriptStruct* StaticStruct<struct FBuoyancyData>();

#define Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyTypes_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSphericalPontoon_Statics; \
	WATER_API static class UScriptStruct* StaticStruct();


template<> WATER_API UScriptStruct* StaticStruct<struct FSphericalPontoon>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Water_Source_Runtime_Public_BuoyancyTypes_h


#define FOREACH_ENUM_EBUOYANCYEVENT(op) \
	op(EBuoyancyEvent::EnteredWaterBody) \
	op(EBuoyancyEvent::ExitedWaterBody) 

enum class EBuoyancyEvent : uint8;
template<> WATER_API UEnum* StaticEnum<EBuoyancyEvent>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
