// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/BuoyancyTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBuoyancyTypes() {}
// Cross Module References
	WATER_API UEnum* Z_Construct_UEnum_Water_EBuoyancyEvent();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FBuoyancyData();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FSphericalPontoon();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FQuat();
	WATER_API UClass* Z_Construct_UClass_AWaterBody_NoRegister();
// End Cross Module References
	static UEnum* EBuoyancyEvent_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Water_EBuoyancyEvent, Z_Construct_UPackage__Script_Water(), TEXT("EBuoyancyEvent"));
		}
		return Singleton;
	}
	template<> WATER_API UEnum* StaticEnum<EBuoyancyEvent>()
	{
		return EBuoyancyEvent_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EBuoyancyEvent(EBuoyancyEvent_StaticEnum, TEXT("/Script/Water"), TEXT("EBuoyancyEvent"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Water_EBuoyancyEvent_Hash() { return 2048287004U; }
	UEnum* Z_Construct_UEnum_Water_EBuoyancyEvent()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EBuoyancyEvent"), 0, Get_Z_Construct_UEnum_Water_EBuoyancyEvent_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EBuoyancyEvent::EnteredWaterBody", (int64)EBuoyancyEvent::EnteredWaterBody },
				{ "EBuoyancyEvent::ExitedWaterBody", (int64)EBuoyancyEvent::ExitedWaterBody },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "EnteredWaterBody.Name", "EBuoyancyEvent::EnteredWaterBody" },
				{ "ExitedWaterBody.Name", "EBuoyancyEvent::ExitedWaterBody" },
				{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Water,
				nullptr,
				"EBuoyancyEvent",
				"EBuoyancyEvent",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FBuoyancyData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FBuoyancyData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FBuoyancyData, Z_Construct_UPackage__Script_Water(), TEXT("BuoyancyData"), sizeof(FBuoyancyData), Get_Z_Construct_UScriptStruct_FBuoyancyData_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FBuoyancyData>()
{
	return FBuoyancyData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FBuoyancyData(FBuoyancyData::StaticStruct, TEXT("/Script/Water"), TEXT("BuoyancyData"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFBuoyancyData
{
	FScriptStruct_Water_StaticRegisterNativesFBuoyancyData()
	{
		UScriptStruct::DeferCppStructOps<FBuoyancyData>(FName(TEXT("BuoyancyData")));
	}
} ScriptStruct_Water_StaticRegisterNativesFBuoyancyData;
	struct Z_Construct_UScriptStruct_FBuoyancyData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Pontoons_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pontoons_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Pontoons;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyCoefficient_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BuoyancyCoefficient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyDamp_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BuoyancyDamp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyDamp2_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BuoyancyDamp2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyRampMinVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BuoyancyRampMinVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyRampMaxVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BuoyancyRampMaxVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BuoyancyRampMax_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_BuoyancyRampMax;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxBuoyantForce_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxBuoyantForce;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterShorePushFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaterShorePushFactor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterVelocityStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaterVelocityStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxWaterForce_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxWaterForce;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DragCoefficient_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DragCoefficient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DragCoefficient2_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DragCoefficient2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularDragCoefficient_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngularDragCoefficient;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaxDragSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MaxDragSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bApplyDragForcesInWater_MetaData[];
#endif
		static void NewProp_bApplyDragForcesInWater_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bApplyDragForcesInWater;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FBuoyancyData>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_Pontoons_Inner = { "Pontoons", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSphericalPontoon, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_Pontoons_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_Pontoons = { "Pontoons", nullptr, (EPropertyFlags)0x0010000000010015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, Pontoons), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_Pontoons_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_Pontoons_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyCoefficient_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Increases buoyant force applied on each pontoon. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Increases buoyant force applied on each pontoon." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyCoefficient = { "BuoyancyCoefficient", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, BuoyancyCoefficient), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyCoefficient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyCoefficient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Damping factor to scale damping based on Z velocity. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Damping factor to scale damping based on Z velocity." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp = { "BuoyancyDamp", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, BuoyancyDamp), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp2_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/**Second Order Damping factor to scale damping based on Z velocity. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Second Order Damping factor to scale damping based on Z velocity." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp2 = { "BuoyancyDamp2", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, BuoyancyDamp2), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMinVelocity_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Minimum velocity to start applying a ramp to buoyancy. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Minimum velocity to start applying a ramp to buoyancy." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMinVelocity = { "BuoyancyRampMinVelocity", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, BuoyancyRampMinVelocity), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMinVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMinVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMaxVelocity_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Maximum velocity until which the buoyancy can ramp up. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Maximum velocity until which the buoyancy can ramp up." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMaxVelocity = { "BuoyancyRampMaxVelocity", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, BuoyancyRampMaxVelocity), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMaxVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMaxVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMax_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Maximum value that buoyancy can ramp to (at or beyond max velocity). */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Maximum value that buoyancy can ramp to (at or beyond max velocity)." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMax = { "BuoyancyRampMax", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, BuoyancyRampMax), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMax_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMax_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxBuoyantForce_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Maximum buoyant force in the Up direction. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Maximum buoyant force in the Up direction." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxBuoyantForce = { "MaxBuoyantForce", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, MaxBuoyantForce), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxBuoyantForce_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxBuoyantForce_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterShorePushFactor_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Coefficient for nudging objects to shore (for perf reasons). */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Coefficient for nudging objects to shore (for perf reasons)." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterShorePushFactor = { "WaterShorePushFactor", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, WaterShorePushFactor), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterShorePushFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterShorePushFactor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterVelocityStrength_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Coefficient for applying push force in rivers. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Coefficient for applying push force in rivers." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterVelocityStrength = { "WaterVelocityStrength", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, WaterVelocityStrength), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterVelocityStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterVelocityStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxWaterForce_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Maximum push force that can be applied by rivers. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Maximum push force that can be applied by rivers." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxWaterForce = { "MaxWaterForce", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, MaxWaterForce), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxWaterForce_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxWaterForce_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "EditCondition", "bApplyDragForcesInWater" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient = { "DragCoefficient", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, DragCoefficient), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient2_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "EditCondition", "bApplyDragForcesInWater" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient2 = { "DragCoefficient2", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, DragCoefficient2), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient2_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_AngularDragCoefficient_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "EditCondition", "bApplyDragForcesInWater" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_AngularDragCoefficient = { "AngularDragCoefficient", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, AngularDragCoefficient), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_AngularDragCoefficient_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_AngularDragCoefficient_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxDragSpeed_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "EditCondition", "bApplyDragForcesInWater" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxDragSpeed = { "MaxDragSpeed", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FBuoyancyData, MaxDragSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxDragSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxDragSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_bApplyDragForcesInWater_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_bApplyDragForcesInWater_SetBit(void* Obj)
	{
		((FBuoyancyData*)Obj)->bApplyDragForcesInWater = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_bApplyDragForcesInWater = { "bApplyDragForcesInWater", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FBuoyancyData), &Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_bApplyDragForcesInWater_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_bApplyDragForcesInWater_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_bApplyDragForcesInWater_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FBuoyancyData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_Pontoons_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_Pontoons,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyCoefficient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyDamp2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMinVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMaxVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_BuoyancyRampMax,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxBuoyantForce,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterShorePushFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_WaterVelocityStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxWaterForce,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_DragCoefficient2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_AngularDragCoefficient,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_MaxDragSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FBuoyancyData_Statics::NewProp_bApplyDragForcesInWater,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FBuoyancyData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"BuoyancyData",
		sizeof(FBuoyancyData),
		alignof(FBuoyancyData),
		Z_Construct_UScriptStruct_FBuoyancyData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FBuoyancyData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FBuoyancyData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FBuoyancyData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FBuoyancyData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("BuoyancyData"), sizeof(FBuoyancyData), Get_Z_Construct_UScriptStruct_FBuoyancyData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FBuoyancyData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FBuoyancyData_Hash() { return 2228439159U; }
class UScriptStruct* FSphericalPontoon::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FSphericalPontoon_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSphericalPontoon, Z_Construct_UPackage__Script_Water(), TEXT("SphericalPontoon"), sizeof(FSphericalPontoon), Get_Z_Construct_UScriptStruct_FSphericalPontoon_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FSphericalPontoon>()
{
	return FSphericalPontoon::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSphericalPontoon(FSphericalPontoon::StaticStruct, TEXT("/Script/Water"), TEXT("SphericalPontoon"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFSphericalPontoon
{
	FScriptStruct_Water_StaticRegisterNativesFSphericalPontoon()
	{
		UScriptStruct::DeferCppStructOps<FSphericalPontoon>(FName(TEXT("SphericalPontoon")));
	}
} ScriptStruct_Water_StaticRegisterNativesFSphericalPontoon;
	struct Z_Construct_UScriptStruct_FSphericalPontoon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CenterSocket_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_CenterSocket;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RelativeLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RelativeLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Radius_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Radius;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LocalForce_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LocalForce;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CenterLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CenterLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SocketRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SocketRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Offset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Offset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaterHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_WaterDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImmersionDepth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ImmersionDepth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterPlaneLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterPlaneLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterPlaneNormal_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterPlaneNormal;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterSurfacePosition_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterSurfacePosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterBodyIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_WaterBodyIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentWaterBody_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentWaterBody;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSphericalPontoon>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterSocket_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** The socket to center this pontoon on */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "The socket to center this pontoon on" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterSocket = { "CenterSocket", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, CenterSocket), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterSocket_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterSocket_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_RelativeLocation_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** Relative Location of pontoon WRT parent actor. Overridden by Center Socket. */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "Relative Location of pontoon WRT parent actor. Overridden by Center Socket." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_RelativeLocation = { "RelativeLocation", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, RelativeLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_RelativeLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_RelativeLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Radius_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "Comment", "/** The radius of the pontoon */" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
		{ "ToolTip", "The radius of the pontoon" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Radius = { "Radius", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, Radius), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Radius_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Radius_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_LocalForce_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_LocalForce = { "LocalForce", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, LocalForce), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_LocalForce_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_LocalForce_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterLocation_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterLocation = { "CenterLocation", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, CenterLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_SocketRotation_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_SocketRotation = { "SocketRotation", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, SocketRotation), Z_Construct_UScriptStruct_FQuat, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_SocketRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_SocketRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Offset_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Offset = { "Offset", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, Offset), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Offset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Offset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterHeight_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterHeight = { "WaterHeight", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, WaterHeight), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterDepth_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterDepth = { "WaterDepth", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, WaterDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_ImmersionDepth_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_ImmersionDepth = { "ImmersionDepth", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, ImmersionDepth), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_ImmersionDepth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_ImmersionDepth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneLocation_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneLocation = { "WaterPlaneLocation", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, WaterPlaneLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneNormal_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneNormal = { "WaterPlaneNormal", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, WaterPlaneNormal), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneNormal_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneNormal_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterSurfacePosition_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterSurfacePosition = { "WaterSurfacePosition", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, WaterSurfacePosition), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterSurfacePosition_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterSurfacePosition_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterVelocity_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterVelocity = { "WaterVelocity", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, WaterVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterBodyIndex_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterBodyIndex = { "WaterBodyIndex", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, WaterBodyIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterBodyIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterBodyIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CurrentWaterBody_MetaData[] = {
		{ "Category", "Buoyancy" },
		{ "ModuleRelativePath", "Public/BuoyancyTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CurrentWaterBody = { "CurrentWaterBody", nullptr, (EPropertyFlags)0x0010000000002014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSphericalPontoon, CurrentWaterBody), Z_Construct_UClass_AWaterBody_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CurrentWaterBody_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CurrentWaterBody_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSphericalPontoon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterSocket,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_RelativeLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Radius,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_LocalForce,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CenterLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_SocketRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_Offset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_ImmersionDepth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterPlaneNormal,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterSurfacePosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_WaterBodyIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSphericalPontoon_Statics::NewProp_CurrentWaterBody,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSphericalPontoon_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"SphericalPontoon",
		sizeof(FSphericalPontoon),
		alignof(FSphericalPontoon),
		Z_Construct_UScriptStruct_FSphericalPontoon_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSphericalPontoon_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSphericalPontoon()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSphericalPontoon_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SphericalPontoon"), sizeof(FSphericalPontoon), Get_Z_Construct_UScriptStruct_FSphericalPontoon_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSphericalPontoon_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSphericalPontoon_Hash() { return 3404530510U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
