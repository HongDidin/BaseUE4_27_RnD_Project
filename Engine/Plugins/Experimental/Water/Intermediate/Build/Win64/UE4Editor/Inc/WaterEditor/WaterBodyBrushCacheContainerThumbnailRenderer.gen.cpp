// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Editor/Private/WaterBodyBrushCacheContainerThumbnailRenderer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyBrushCacheContainerThumbnailRenderer() {}
// Cross Module References
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_NoRegister();
	WATEREDITOR_API UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer();
	UNREALED_API UClass* Z_Construct_UClass_UTextureThumbnailRenderer();
	UPackage* Z_Construct_UPackage__Script_WaterEditor();
// End Cross Module References
	void UWaterBodyBrushCacheContainerThumbnailRenderer::StaticRegisterNativesUWaterBodyBrushCacheContainerThumbnailRenderer()
	{
	}
	UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_NoRegister()
	{
		return UWaterBodyBrushCacheContainerThumbnailRenderer::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UTextureThumbnailRenderer,
		(UObject* (*)())Z_Construct_UPackage__Script_WaterEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WaterBodyBrushCacheContainerThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Private/WaterBodyBrushCacheContainerThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UWaterBodyBrushCacheContainerThumbnailRenderer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics::ClassParams = {
		&UWaterBodyBrushCacheContainerThumbnailRenderer::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBodyBrushCacheContainerThumbnailRenderer, 1195239454);
	template<> WATEREDITOR_API UClass* StaticClass<UWaterBodyBrushCacheContainerThumbnailRenderer>()
	{
		return UWaterBodyBrushCacheContainerThumbnailRenderer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer(Z_Construct_UClass_UWaterBodyBrushCacheContainerThumbnailRenderer, &UWaterBodyBrushCacheContainerThumbnailRenderer::StaticClass, TEXT("/Script/WaterEditor"), TEXT("UWaterBodyBrushCacheContainerThumbnailRenderer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBodyBrushCacheContainerThumbnailRenderer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
