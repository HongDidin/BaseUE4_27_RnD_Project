// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterFalloffSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterFalloffSettings() {}
// Cross Module References
	WATER_API UEnum* Z_Construct_UEnum_Water_EWaterBrushFalloffMode();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterFalloffSettings();
// End Cross Module References
	static UEnum* EWaterBrushFalloffMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Water_EWaterBrushFalloffMode, Z_Construct_UPackage__Script_Water(), TEXT("EWaterBrushFalloffMode"));
		}
		return Singleton;
	}
	template<> WATER_API UEnum* StaticEnum<EWaterBrushFalloffMode>()
	{
		return EWaterBrushFalloffMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EWaterBrushFalloffMode(EWaterBrushFalloffMode_StaticEnum, TEXT("/Script/Water"), TEXT("EWaterBrushFalloffMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Water_EWaterBrushFalloffMode_Hash() { return 703002005U; }
	UEnum* Z_Construct_UEnum_Water_EWaterBrushFalloffMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EWaterBrushFalloffMode"), 0, Get_Z_Construct_UEnum_Water_EWaterBrushFalloffMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EWaterBrushFalloffMode::Angle", (int64)EWaterBrushFalloffMode::Angle },
				{ "EWaterBrushFalloffMode::Width", (int64)EWaterBrushFalloffMode::Width },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Angle.Name", "EWaterBrushFalloffMode::Angle" },
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/WaterFalloffSettings.h" },
				{ "Width.Name", "EWaterBrushFalloffMode::Width" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Water,
				nullptr,
				"EWaterBrushFalloffMode",
				"EWaterBrushFalloffMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FWaterFalloffSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern WATER_API uint32 Get_Z_Construct_UScriptStruct_FWaterFalloffSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FWaterFalloffSettings, Z_Construct_UPackage__Script_Water(), TEXT("WaterFalloffSettings"), sizeof(FWaterFalloffSettings), Get_Z_Construct_UScriptStruct_FWaterFalloffSettings_Hash());
	}
	return Singleton;
}
template<> WATER_API UScriptStruct* StaticStruct<FWaterFalloffSettings>()
{
	return FWaterFalloffSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FWaterFalloffSettings(FWaterFalloffSettings::StaticStruct, TEXT("/Script/Water"), TEXT("WaterFalloffSettings"), false, nullptr, nullptr);
static struct FScriptStruct_Water_StaticRegisterNativesFWaterFalloffSettings
{
	FScriptStruct_Water_StaticRegisterNativesFWaterFalloffSettings()
	{
		UScriptStruct::DeferCppStructOps<FWaterFalloffSettings>(FName(TEXT("WaterFalloffSettings")));
	}
} ScriptStruct_Water_StaticRegisterNativesFWaterFalloffSettings;
	struct Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_FalloffMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_FalloffMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FalloffAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FalloffWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FalloffWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EdgeOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EdgeOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ZOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/WaterFalloffSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FWaterFalloffSettings>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffMode_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterFalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffMode = { "FalloffMode", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterFalloffSettings, FalloffMode), Z_Construct_UEnum_Water_EWaterBrushFalloffMode, METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffAngle_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterFalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffAngle = { "FalloffAngle", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterFalloffSettings, FalloffAngle), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffWidth_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterFalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffWidth = { "FalloffWidth", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterFalloffSettings, FalloffWidth), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_EdgeOffset_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterFalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_EdgeOffset = { "EdgeOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterFalloffSettings, EdgeOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_EdgeOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_EdgeOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_ZOffset_MetaData[] = {
		{ "Category", "FalloffSettings" },
		{ "ModuleRelativePath", "Public/WaterFalloffSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_ZOffset = { "ZOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FWaterFalloffSettings, ZOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_ZOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_ZOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_FalloffWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_EdgeOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::NewProp_ZOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
		nullptr,
		&NewStructOps,
		"WaterFalloffSettings",
		sizeof(FWaterFalloffSettings),
		alignof(FWaterFalloffSettings),
		Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FWaterFalloffSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FWaterFalloffSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("WaterFalloffSettings"), sizeof(FWaterFalloffSettings), Get_Z_Construct_UScriptStruct_FWaterFalloffSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FWaterFalloffSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FWaterFalloffSettings_Hash() { return 3808790361U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
