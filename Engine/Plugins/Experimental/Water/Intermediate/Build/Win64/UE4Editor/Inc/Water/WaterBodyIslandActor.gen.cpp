// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyIslandActor.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyIslandActor() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_AWaterBodyIsland_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyIsland();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Water();
	WATER_API UClass* Z_Construct_UClass_UWaterSplineComponent_NoRegister();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterCurveSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings();
	WATER_API UScriptStruct* Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings();
	ENGINE_API UClass* Z_Construct_UClass_UBillboardComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterBrushActorInterface_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AWaterBodyIsland::execGetWaterSpline)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UWaterSplineComponent**)Z_Param__Result=P_THIS->GetWaterSpline();
		P_NATIVE_END;
	}
	void AWaterBodyIsland::StaticRegisterNativesAWaterBodyIsland()
	{
		UClass* Class = AWaterBodyIsland::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWaterSpline", &AWaterBodyIsland::execGetWaterSpline },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics
	{
		struct WaterBodyIsland_eventGetWaterSpline_Parms
		{
			UWaterSplineComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(WaterBodyIsland_eventGetWaterSpline_Parms, ReturnValue), Z_Construct_UClass_UWaterSplineComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::Function_MetaDataParams[] = {
		{ "Category", "Water" },
		{ "Comment", "//~ End IWaterBrushActorInterface interface\n" },
		{ "ModuleRelativePath", "Public/WaterBodyIslandActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AWaterBodyIsland, nullptr, "GetWaterSpline", nullptr, nullptr, sizeof(WaterBodyIsland_eventGetWaterSpline_Parms), Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AWaterBodyIsland_NoRegister()
	{
		return AWaterBodyIsland::StaticClass();
	}
	struct Z_Construct_UClass_AWaterBodyIsland_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterCurveSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterCurveSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterHeightmapSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterHeightmapSettings;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WaterWeightmapSettings_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_WaterWeightmapSettings_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WaterWeightmapSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_WaterWeightmapSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorIcon_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorIcon;
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SplineComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SplineComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterBodyIsland_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AWaterBodyIsland_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AWaterBodyIsland_GetWaterSpline, "GetWaterSpline" }, // 922779414
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyIsland_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "WaterBodyIslandActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyIslandActor.h" },
	};
#endif
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterCurveSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterBodyIslandActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterCurveSettings = { "WaterCurveSettings", nullptr, (EPropertyFlags)0x0010000800000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyIsland, WaterCurveSettings), Z_Construct_UScriptStruct_FWaterCurveSettings, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterCurveSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterCurveSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterHeightmapSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterBodyIslandActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterHeightmapSettings = { "WaterHeightmapSettings", nullptr, (EPropertyFlags)0x0010000800000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyIsland, WaterHeightmapSettings), Z_Construct_UScriptStruct_FWaterBodyHeightmapSettings, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterHeightmapSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterHeightmapSettings_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings_ValueProp = { "WaterWeightmapSettings", nullptr, (EPropertyFlags)0x0000000800000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FWaterBodyWeightmapSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings_Key_KeyProp = { "WaterWeightmapSettings_Key", nullptr, (EPropertyFlags)0x0000000800000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings_MetaData[] = {
		{ "Category", "Terrain" },
		{ "ModuleRelativePath", "Public/WaterBodyIslandActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings = { "WaterWeightmapSettings", nullptr, (EPropertyFlags)0x0010000800000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyIsland, WaterWeightmapSettings), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_ActorIcon_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyIslandActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_ActorIcon = { "ActorIcon", nullptr, (EPropertyFlags)0x0010000800082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyIsland, ActorIcon), Z_Construct_UClass_UBillboardComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_ActorIcon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_ActorIcon_MetaData)) };
#endif // WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_SplineComp_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Water" },
		{ "Comment", "/**\n\x09 * The spline data attached to this water type.\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyIslandActor.h" },
		{ "ToolTip", "The spline data attached to this water type." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_SplineComp = { "SplineComp", nullptr, (EPropertyFlags)0x00200800000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyIsland, SplineComp), Z_Construct_UClass_UWaterSplineComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_SplineComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_SplineComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterBodyIsland_Statics::PropPointers[] = {
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterCurveSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterHeightmapSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_WaterWeightmapSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_ActorIcon,
#endif // WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyIsland_Statics::NewProp_SplineComp,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AWaterBodyIsland_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UWaterBrushActorInterface_NoRegister, (int32)VTABLE_OFFSET(AWaterBodyIsland, IWaterBrushActorInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterBodyIsland_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterBodyIsland>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterBodyIsland_Statics::ClassParams = {
		&AWaterBodyIsland::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AWaterBodyIsland_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyIsland_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterBodyIsland_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyIsland_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterBodyIsland()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterBodyIsland_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterBodyIsland, 2603299572);
	template<> WATER_API UClass* StaticClass<AWaterBodyIsland>()
	{
		return AWaterBodyIsland::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterBodyIsland(Z_Construct_UClass_AWaterBodyIsland, &AWaterBodyIsland::StaticClass, TEXT("/Script/Water"), TEXT("AWaterBodyIsland"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterBodyIsland);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(AWaterBodyIsland)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
