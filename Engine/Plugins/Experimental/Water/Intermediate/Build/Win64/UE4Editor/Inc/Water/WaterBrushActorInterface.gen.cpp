// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBrushActorInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBrushActorInterface() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UWaterBrushActorInterface_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UWaterBrushActorInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Water();
// End Cross Module References
	void UWaterBrushActorInterface::StaticRegisterNativesUWaterBrushActorInterface()
	{
	}
	UClass* Z_Construct_UClass_UWaterBrushActorInterface_NoRegister()
	{
		return UWaterBrushActorInterface::StaticClass();
	}
	struct Z_Construct_UClass_UWaterBrushActorInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UWaterBrushActorInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UWaterBrushActorInterface_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/WaterBrushActorInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UWaterBrushActorInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IWaterBrushActorInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UWaterBrushActorInterface_Statics::ClassParams = {
		&UWaterBrushActorInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UWaterBrushActorInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UWaterBrushActorInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UWaterBrushActorInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UWaterBrushActorInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UWaterBrushActorInterface, 3355912972);
	template<> WATER_API UClass* StaticClass<UWaterBrushActorInterface>()
	{
		return UWaterBrushActorInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UWaterBrushActorInterface(Z_Construct_UClass_UWaterBrushActorInterface, &UWaterBrushActorInterface::StaticClass, TEXT("/Script/Water"), TEXT("UWaterBrushActorInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UWaterBrushActorInterface);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
