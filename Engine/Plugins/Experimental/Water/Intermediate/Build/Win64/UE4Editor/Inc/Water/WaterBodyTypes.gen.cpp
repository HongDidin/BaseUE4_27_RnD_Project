// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyTypes() {}
// Cross Module References
	WATER_API UEnum* Z_Construct_UEnum_Water_EWaterBodyType();
	UPackage* Z_Construct_UPackage__Script_Water();
// End Cross Module References
	static UEnum* EWaterBodyType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Water_EWaterBodyType, Z_Construct_UPackage__Script_Water(), TEXT("EWaterBodyType"));
		}
		return Singleton;
	}
	template<> WATER_API UEnum* StaticEnum<EWaterBodyType>()
	{
		return EWaterBodyType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EWaterBodyType(EWaterBodyType_StaticEnum, TEXT("/Script/Water"), TEXT("EWaterBodyType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Water_EWaterBodyType_Hash() { return 556781095U; }
	UEnum* Z_Construct_UEnum_Water_EWaterBodyType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Water();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EWaterBodyType"), 0, Get_Z_Construct_UEnum_Water_EWaterBodyType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EWaterBodyType::River", (int64)EWaterBodyType::River },
				{ "EWaterBodyType::Lake", (int64)EWaterBodyType::Lake },
				{ "EWaterBodyType::Ocean", (int64)EWaterBodyType::Ocean },
				{ "EWaterBodyType::Transition", (int64)EWaterBodyType::Transition },
				{ "EWaterBodyType::Num", (int64)EWaterBodyType::Num },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Lake.Comment", "/** Lakes defined by a close loop spline around the shore with water in the middle*/" },
				{ "Lake.Name", "EWaterBodyType::Lake" },
				{ "Lake.ToolTip", "Lakes defined by a close loop spline around the shore with water in the middle" },
				{ "ModuleRelativePath", "Public/WaterBodyTypes.h" },
				{ "Num.Comment", "/** Max value */" },
				{ "Num.Hidden", "" },
				{ "Num.Name", "EWaterBodyType::Num" },
				{ "Num.ToolTip", "Max value" },
				{ "Ocean.Comment", "/* Ocean defined by a shoreline spline and rendered out to a far distance */" },
				{ "Ocean.Name", "EWaterBodyType::Ocean" },
				{ "Ocean.ToolTip", "Ocean defined by a shoreline spline and rendered out to a far distance" },
				{ "River.Comment", "/** Rivers defined by a spline down the middle */" },
				{ "River.Name", "EWaterBodyType::River" },
				{ "River.ToolTip", "Rivers defined by a spline down the middle" },
				{ "Transition.Comment", "/** A custom water body that can be used for gameplay reasons.  Uses a spline down the middle to encode gameplay data. Requires a custom mesh to render, doesn't affect landscape */" },
				{ "Transition.DisplayName", "Custom" },
				{ "Transition.Name", "EWaterBodyType::Transition" },
				{ "Transition.ToolTip", "A custom water body that can be used for gameplay reasons.  Uses a spline down the middle to encode gameplay data. Requires a custom mesh to render, doesn't affect landscape" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Water,
				nullptr,
				"EWaterBodyType",
				"EWaterBodyType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
