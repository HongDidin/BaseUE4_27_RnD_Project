// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Runtime/Public/WaterBodyCustomActor.h"
#include "Runtime/Public/WaterBodyActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWaterBodyCustomActor() {}
// Cross Module References
	WATER_API UClass* Z_Construct_UClass_UCustomMeshGenerator_NoRegister();
	WATER_API UClass* Z_Construct_UClass_UCustomMeshGenerator();
	WATER_API UClass* Z_Construct_UClass_UWaterBodyGenerator();
	UPackage* Z_Construct_UPackage__Script_Water();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyCustom_NoRegister();
	WATER_API UClass* Z_Construct_UClass_AWaterBodyCustom();
	WATER_API UClass* Z_Construct_UClass_AWaterBody();
// End Cross Module References
	void UCustomMeshGenerator::StaticRegisterNativesUCustomMeshGenerator()
	{
	}
	UClass* Z_Construct_UClass_UCustomMeshGenerator_NoRegister()
	{
		return UCustomMeshGenerator::StaticClass();
	}
	struct Z_Construct_UClass_UCustomMeshGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComp;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCustomMeshGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWaterBodyGenerator,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomMeshGenerator_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "IncludePath", "WaterBodyCustomActor.h" },
		{ "ModuleRelativePath", "Public/WaterBodyCustomActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCustomMeshGenerator_Statics::NewProp_MeshComp_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyCustomActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCustomMeshGenerator_Statics::NewProp_MeshComp = { "MeshComp", nullptr, (EPropertyFlags)0x0040800000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCustomMeshGenerator, MeshComp), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCustomMeshGenerator_Statics::NewProp_MeshComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCustomMeshGenerator_Statics::NewProp_MeshComp_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCustomMeshGenerator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCustomMeshGenerator_Statics::NewProp_MeshComp,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCustomMeshGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCustomMeshGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCustomMeshGenerator_Statics::ClassParams = {
		&UCustomMeshGenerator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCustomMeshGenerator_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCustomMeshGenerator_Statics::PropPointers),
		0,
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCustomMeshGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCustomMeshGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCustomMeshGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCustomMeshGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCustomMeshGenerator, 4165683514);
	template<> WATER_API UClass* StaticClass<UCustomMeshGenerator>()
	{
		return UCustomMeshGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCustomMeshGenerator(Z_Construct_UClass_UCustomMeshGenerator, &UCustomMeshGenerator::StaticClass, TEXT("/Script/Water"), TEXT("UCustomMeshGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCustomMeshGenerator);
	void AWaterBodyCustom::StaticRegisterNativesAWaterBodyCustom()
	{
	}
	UClass* Z_Construct_UClass_AWaterBodyCustom_NoRegister()
	{
		return AWaterBodyCustom::StaticClass();
	}
	struct Z_Construct_UClass_AWaterBodyCustom_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomGenerator_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CustomGenerator;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWaterBodyCustom_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AWaterBody,
		(UObject* (*)())Z_Construct_UPackage__Script_Water,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyCustom_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ----------------------------------------------------------------------------------\n" },
		{ "HideCategories", "Tags Activation Cooking Replication Input Actor AssetUserData" },
		{ "IncludePath", "WaterBodyCustomActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/WaterBodyCustomActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWaterBodyCustom_Statics::NewProp_CustomGenerator_MetaData[] = {
		{ "ModuleRelativePath", "Public/WaterBodyCustomActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWaterBodyCustom_Statics::NewProp_CustomGenerator = { "CustomGenerator", nullptr, (EPropertyFlags)0x0020880000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWaterBodyCustom, CustomGenerator), Z_Construct_UClass_UCustomMeshGenerator_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AWaterBodyCustom_Statics::NewProp_CustomGenerator_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyCustom_Statics::NewProp_CustomGenerator_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWaterBodyCustom_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWaterBodyCustom_Statics::NewProp_CustomGenerator,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWaterBodyCustom_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWaterBodyCustom>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWaterBodyCustom_Statics::ClassParams = {
		&AWaterBodyCustom::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWaterBodyCustom_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyCustom_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWaterBodyCustom_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWaterBodyCustom_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWaterBodyCustom()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWaterBodyCustom_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWaterBodyCustom, 3320907130);
	template<> WATER_API UClass* StaticClass<AWaterBodyCustom>()
	{
		return AWaterBodyCustom::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWaterBodyCustom(Z_Construct_UClass_AWaterBodyCustom, &AWaterBodyCustom::StaticClass, TEXT("/Script/Water"), TEXT("AWaterBodyCustom"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWaterBodyCustom);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
