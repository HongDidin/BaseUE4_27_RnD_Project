// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHRUNTIME_DatasmithRuntime_generated_h
#error "DatasmithRuntime.generated.h already included, missing '#pragma once' in DatasmithRuntime.h"
#endif
#define DATASMITHRUNTIME_DatasmithRuntime_generated_h

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_107_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDatasmithRuntimeImportOptions_Statics; \
	DATASMITHRUNTIME_API static class UScriptStruct* StaticStruct();


template<> DATASMITHRUNTIME_API UScriptStruct* StaticStruct<struct FDatasmithRuntimeImportOptions>();

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_SPARSE_DATA
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execLoadFile); \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execIsReceiving); \
	DECLARE_FUNCTION(execGetSourceIndex); \
	DECLARE_FUNCTION(execCloseConnection); \
	DECLARE_FUNCTION(execOpenConnectionWithIndex); \
	DECLARE_FUNCTION(execGetSourceName); \
	DECLARE_FUNCTION(execGetDestinationName); \
	DECLARE_FUNCTION(execIsConnected);


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execLoadFile); \
	DECLARE_FUNCTION(execReset); \
	DECLARE_FUNCTION(execIsReceiving); \
	DECLARE_FUNCTION(execGetSourceIndex); \
	DECLARE_FUNCTION(execCloseConnection); \
	DECLARE_FUNCTION(execOpenConnectionWithIndex); \
	DECLARE_FUNCTION(execGetSourceName); \
	DECLARE_FUNCTION(execGetDestinationName); \
	DECLARE_FUNCTION(execIsConnected);


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesADatasmithRuntimeActor(); \
	friend struct Z_Construct_UClass_ADatasmithRuntimeActor_Statics; \
public: \
	DECLARE_CLASS(ADatasmithRuntimeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(ADatasmithRuntimeActor)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_INCLASS \
private: \
	static void StaticRegisterNativesADatasmithRuntimeActor(); \
	friend struct Z_Construct_UClass_ADatasmithRuntimeActor_Statics; \
public: \
	DECLARE_CLASS(ADatasmithRuntimeActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(ADatasmithRuntimeActor)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ADatasmithRuntimeActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ADatasmithRuntimeActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADatasmithRuntimeActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADatasmithRuntimeActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADatasmithRuntimeActor(ADatasmithRuntimeActor&&); \
	NO_API ADatasmithRuntimeActor(const ADatasmithRuntimeActor&); \
public:


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ADatasmithRuntimeActor(ADatasmithRuntimeActor&&); \
	NO_API ADatasmithRuntimeActor(const ADatasmithRuntimeActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ADatasmithRuntimeActor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ADatasmithRuntimeActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ADatasmithRuntimeActor)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_142_PROLOG
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_INCLASS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h_147_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHRUNTIME_API UClass* StaticClass<class ADatasmithRuntimeActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntime_h


#define FOREACH_ENUM_EBUILDHIERARCHYMETHOD(op) \
	op(EBuildHierarchyMethod::None) \
	op(EBuildHierarchyMethod::Simplified) \
	op(EBuildHierarchyMethod::Unfiltered) 

enum class EBuildHierarchyMethod : uint8;
template<> DATASMITHRUNTIME_API UEnum* StaticEnum<EBuildHierarchyMethod>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
