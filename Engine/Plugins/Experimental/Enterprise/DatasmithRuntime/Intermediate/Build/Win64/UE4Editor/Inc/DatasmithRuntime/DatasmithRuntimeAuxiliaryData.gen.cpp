// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/DatasmithRuntimeAuxiliaryData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithRuntimeAuxiliaryData() {}
// Cross Module References
	DATASMITHRUNTIME_API UClass* Z_Construct_UClass_UDatasmithRuntimeTHelper_NoRegister();
	DATASMITHRUNTIME_API UClass* Z_Construct_UClass_UDatasmithRuntimeTHelper();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_DatasmithRuntime();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UInterface_AssetUserData_NoRegister();
	DATASMITHRUNTIME_API UClass* Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_NoRegister();
	DATASMITHRUNTIME_API UClass* Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetUserData();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UDatasmithRuntimeTHelper::StaticRegisterNativesUDatasmithRuntimeTHelper()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithRuntimeTHelper_NoRegister()
	{
		return UDatasmithRuntimeTHelper::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetUserData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetUserData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AssetUserData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Utility class to hold on FAssetData entities while their associated texture is being built\n */" },
		{ "IncludePath", "DatasmithRuntimeAuxiliaryData.h" },
		{ "ModuleRelativePath", "Private/DatasmithRuntimeAuxiliaryData.h" },
		{ "ToolTip", "Utility class to hold on FAssetData entities while their associated texture is being built" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::NewProp_AssetUserData_Inner = { "AssetUserData", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UAssetUserData_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::NewProp_AssetUserData_MetaData[] = {
		{ "Comment", "/** Array of user data stored with the asset */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/DatasmithRuntimeAuxiliaryData.h" },
		{ "ToolTip", "Array of user data stored with the asset" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::NewProp_AssetUserData = { "AssetUserData", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithRuntimeTHelper, AssetUserData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::NewProp_AssetUserData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::NewProp_AssetUserData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::NewProp_AssetUserData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::NewProp_AssetUserData,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInterface_AssetUserData_NoRegister, (int32)VTABLE_OFFSET(UDatasmithRuntimeTHelper, IInterface_AssetUserData), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithRuntimeTHelper>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::ClassParams = {
		&UDatasmithRuntimeTHelper::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x008800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithRuntimeTHelper()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithRuntimeTHelper, 811865308);
	template<> DATASMITHRUNTIME_API UClass* StaticClass<UDatasmithRuntimeTHelper>()
	{
		return UDatasmithRuntimeTHelper::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithRuntimeTHelper(Z_Construct_UClass_UDatasmithRuntimeTHelper, &UDatasmithRuntimeTHelper::StaticClass, TEXT("/Script/DatasmithRuntime"), TEXT("UDatasmithRuntimeTHelper"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithRuntimeTHelper);
	void UDatasmithRuntimeAuxiliaryData::StaticRegisterNativesUDatasmithRuntimeAuxiliaryData()
	{
	}
	UClass* Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_NoRegister()
	{
		return UDatasmithRuntimeAuxiliaryData::StaticClass();
	}
	struct Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Auxiliary_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Auxiliary;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetUserData,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Asset user data that can be used with DatasmithRuntime on Actors and other objects  */" },
		{ "IncludePath", "DatasmithRuntimeAuxiliaryData.h" },
		{ "ModuleRelativePath", "Private/DatasmithRuntimeAuxiliaryData.h" },
		{ "ToolTip", "Asset user data that can be used with DatasmithRuntime on Actors and other objects" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::NewProp_Auxiliary_MetaData[] = {
		{ "Category", "DatasmithRuntime Internal" },
		{ "ModuleRelativePath", "Private/DatasmithRuntimeAuxiliaryData.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::NewProp_Auxiliary = { "Auxiliary", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDatasmithRuntimeAuxiliaryData, Auxiliary), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::NewProp_Auxiliary_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::NewProp_Auxiliary_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::NewProp_Auxiliary,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDatasmithRuntimeAuxiliaryData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::ClassParams = {
		&UDatasmithRuntimeAuxiliaryData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::PropPointers),
		0,
		0x002810A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDatasmithRuntimeAuxiliaryData, 2003593788);
	template<> DATASMITHRUNTIME_API UClass* StaticClass<UDatasmithRuntimeAuxiliaryData>()
	{
		return UDatasmithRuntimeAuxiliaryData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDatasmithRuntimeAuxiliaryData(Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData, &UDatasmithRuntimeAuxiliaryData::StaticClass, TEXT("/Script/DatasmithRuntime"), TEXT("UDatasmithRuntimeAuxiliaryData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDatasmithRuntimeAuxiliaryData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
