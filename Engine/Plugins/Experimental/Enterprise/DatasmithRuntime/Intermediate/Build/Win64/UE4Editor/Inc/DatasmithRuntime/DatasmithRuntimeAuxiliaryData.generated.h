// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHRUNTIME_DatasmithRuntimeAuxiliaryData_generated_h
#error "DatasmithRuntimeAuxiliaryData.generated.h already included, missing '#pragma once' in DatasmithRuntimeAuxiliaryData.h"
#endif
#define DATASMITHRUNTIME_DatasmithRuntimeAuxiliaryData_generated_h

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_SPARSE_DATA
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithRuntimeTHelper(); \
	friend struct Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics; \
public: \
	DECLARE_CLASS(UDatasmithRuntimeTHelper, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), DATASMITHRUNTIME_API) \
	DECLARE_SERIALIZER(UDatasmithRuntimeTHelper) \
	virtual UObject* _getUObject() const override { return const_cast<UDatasmithRuntimeTHelper*>(this); }


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithRuntimeTHelper(); \
	friend struct Z_Construct_UClass_UDatasmithRuntimeTHelper_Statics; \
public: \
	DECLARE_CLASS(UDatasmithRuntimeTHelper, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), DATASMITHRUNTIME_API) \
	DECLARE_SERIALIZER(UDatasmithRuntimeTHelper) \
	virtual UObject* _getUObject() const override { return const_cast<UDatasmithRuntimeTHelper*>(this); }


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATASMITHRUNTIME_API UDatasmithRuntimeTHelper(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithRuntimeTHelper) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATASMITHRUNTIME_API, UDatasmithRuntimeTHelper); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithRuntimeTHelper); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATASMITHRUNTIME_API UDatasmithRuntimeTHelper(UDatasmithRuntimeTHelper&&); \
	DATASMITHRUNTIME_API UDatasmithRuntimeTHelper(const UDatasmithRuntimeTHelper&); \
public:


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATASMITHRUNTIME_API UDatasmithRuntimeTHelper(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATASMITHRUNTIME_API UDatasmithRuntimeTHelper(UDatasmithRuntimeTHelper&&); \
	DATASMITHRUNTIME_API UDatasmithRuntimeTHelper(const UDatasmithRuntimeTHelper&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATASMITHRUNTIME_API, UDatasmithRuntimeTHelper); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithRuntimeTHelper); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithRuntimeTHelper)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AssetUserData() { return STRUCT_OFFSET(UDatasmithRuntimeTHelper, AssetUserData); }


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_20_PROLOG
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_INCLASS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHRUNTIME_API UClass* StaticClass<class UDatasmithRuntimeTHelper>();

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_SPARSE_DATA
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithRuntimeAuxiliaryData(); \
	friend struct Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics; \
public: \
	DECLARE_CLASS(UDatasmithRuntimeAuxiliaryData, UAssetUserData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), DATASMITHRUNTIME_API) \
	DECLARE_SERIALIZER(UDatasmithRuntimeAuxiliaryData)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithRuntimeAuxiliaryData(); \
	friend struct Z_Construct_UClass_UDatasmithRuntimeAuxiliaryData_Statics; \
public: \
	DECLARE_CLASS(UDatasmithRuntimeAuxiliaryData, UAssetUserData, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), DATASMITHRUNTIME_API) \
	DECLARE_SERIALIZER(UDatasmithRuntimeAuxiliaryData)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	DATASMITHRUNTIME_API UDatasmithRuntimeAuxiliaryData(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithRuntimeAuxiliaryData) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATASMITHRUNTIME_API, UDatasmithRuntimeAuxiliaryData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithRuntimeAuxiliaryData); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATASMITHRUNTIME_API UDatasmithRuntimeAuxiliaryData(UDatasmithRuntimeAuxiliaryData&&); \
	DATASMITHRUNTIME_API UDatasmithRuntimeAuxiliaryData(const UDatasmithRuntimeAuxiliaryData&); \
public:


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	DATASMITHRUNTIME_API UDatasmithRuntimeAuxiliaryData(UDatasmithRuntimeAuxiliaryData&&); \
	DATASMITHRUNTIME_API UDatasmithRuntimeAuxiliaryData(const UDatasmithRuntimeAuxiliaryData&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(DATASMITHRUNTIME_API, UDatasmithRuntimeAuxiliaryData); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithRuntimeAuxiliaryData); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDatasmithRuntimeAuxiliaryData)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_81_PROLOG
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_INCLASS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h_84_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHRUNTIME_API UClass* StaticClass<class UDatasmithRuntimeAuxiliaryData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeAuxiliaryData_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
