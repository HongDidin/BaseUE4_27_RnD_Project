// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FDatasmithRuntimeSourceInfo;
class ADatasmithRuntimeActor;
class UDirectLinkProxy;
#ifdef DATASMITHRUNTIME_DatasmithRuntimeBlueprintLibrary_generated_h
#error "DatasmithRuntimeBlueprintLibrary.generated.h already included, missing '#pragma once' in DatasmithRuntimeBlueprintLibrary.h"
#endif
#define DATASMITHRUNTIME_DatasmithRuntimeBlueprintLibrary_generated_h

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_23_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FDatasmithRuntimeSourceInfo_Statics; \
	static class UScriptStruct* StaticStruct();


template<> DATASMITHRUNTIME_API UScriptStruct* StaticStruct<struct FDatasmithRuntimeSourceInfo>();

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_41_DELEGATE \
static inline void FDatasmithRuntimeChangeEvent_DelegateWrapper(const FMulticastScriptDelegate& DatasmithRuntimeChangeEvent) \
{ \
	DatasmithRuntimeChangeEvent.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_SPARSE_DATA
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetListOfSources); \
	DECLARE_FUNCTION(execGetEndPointName);


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetListOfSources); \
	DECLARE_FUNCTION(execGetEndPointName);


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDirectLinkProxy(); \
	friend struct Z_Construct_UClass_UDirectLinkProxy_Statics; \
public: \
	DECLARE_CLASS(UDirectLinkProxy, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDirectLinkProxy)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUDirectLinkProxy(); \
	friend struct Z_Construct_UClass_UDirectLinkProxy_Statics; \
public: \
	DECLARE_CLASS(UDirectLinkProxy, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDirectLinkProxy)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDirectLinkProxy(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDirectLinkProxy) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDirectLinkProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDirectLinkProxy); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDirectLinkProxy(UDirectLinkProxy&&); \
	NO_API UDirectLinkProxy(const UDirectLinkProxy&); \
public:


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDirectLinkProxy(UDirectLinkProxy&&); \
	NO_API UDirectLinkProxy(const UDirectLinkProxy&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDirectLinkProxy); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDirectLinkProxy); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UDirectLinkProxy)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_44_PROLOG
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_INCLASS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHRUNTIME_API UClass* StaticClass<class UDirectLinkProxy>();

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_SPARSE_DATA
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execLoadFileFromExplorer); \
	DECLARE_FUNCTION(execGetDirectLinkProxy); \
	DECLARE_FUNCTION(execResetActor); \
	DECLARE_FUNCTION(execLoadFile);


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execLoadFileFromExplorer); \
	DECLARE_FUNCTION(execGetDirectLinkProxy); \
	DECLARE_FUNCTION(execResetActor); \
	DECLARE_FUNCTION(execLoadFile);


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDatasmithRuntimeLibrary(); \
	friend struct Z_Construct_UClass_UDatasmithRuntimeLibrary_Statics; \
public: \
	DECLARE_CLASS(UDatasmithRuntimeLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithRuntimeLibrary)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_INCLASS \
private: \
	static void StaticRegisterNativesUDatasmithRuntimeLibrary(); \
	friend struct Z_Construct_UClass_UDatasmithRuntimeLibrary_Statics; \
public: \
	DECLARE_CLASS(UDatasmithRuntimeLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(UDatasmithRuntimeLibrary)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithRuntimeLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithRuntimeLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithRuntimeLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithRuntimeLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithRuntimeLibrary(UDatasmithRuntimeLibrary&&); \
	NO_API UDatasmithRuntimeLibrary(const UDatasmithRuntimeLibrary&); \
public:


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDatasmithRuntimeLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDatasmithRuntimeLibrary(UDatasmithRuntimeLibrary&&); \
	NO_API UDatasmithRuntimeLibrary(const UDatasmithRuntimeLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDatasmithRuntimeLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDatasmithRuntimeLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDatasmithRuntimeLibrary)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_67_PROLOG
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_INCLASS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h_70_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHRUNTIME_API UClass* StaticClass<class UDatasmithRuntimeLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Public_DatasmithRuntimeBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
