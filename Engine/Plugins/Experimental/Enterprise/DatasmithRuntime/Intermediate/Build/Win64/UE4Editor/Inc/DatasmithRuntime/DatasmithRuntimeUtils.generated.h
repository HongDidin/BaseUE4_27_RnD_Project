// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATASMITHRUNTIME_DatasmithRuntimeUtils_generated_h
#error "DatasmithRuntimeUtils.generated.h already included, missing '#pragma once' in DatasmithRuntimeUtils.h"
#endif
#define DATASMITHRUNTIME_DatasmithRuntimeUtils_generated_h

#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_SPARSE_DATA
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURuntimeMesh(); \
	friend struct Z_Construct_UClass_URuntimeMesh_Statics; \
public: \
	DECLARE_CLASS(URuntimeMesh, UStaticMesh, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(URuntimeMesh)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_INCLASS \
private: \
	static void StaticRegisterNativesURuntimeMesh(); \
	friend struct Z_Construct_UClass_URuntimeMesh_Statics; \
public: \
	DECLARE_CLASS(URuntimeMesh, UStaticMesh, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DatasmithRuntime"), NO_API) \
	DECLARE_SERIALIZER(URuntimeMesh)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URuntimeMesh(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URuntimeMesh) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URuntimeMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URuntimeMesh); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URuntimeMesh(URuntimeMesh&&); \
	NO_API URuntimeMesh(const URuntimeMesh&); \
public:


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URuntimeMesh(URuntimeMesh&&); \
	NO_API URuntimeMesh(const URuntimeMesh&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URuntimeMesh); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URuntimeMesh); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URuntimeMesh)


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_40_PROLOG
#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_INCLASS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATASMITHRUNTIME_API UClass* StaticClass<class URuntimeMesh>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Enterprise_DatasmithRuntime_Source_Private_DatasmithRuntimeUtils_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
