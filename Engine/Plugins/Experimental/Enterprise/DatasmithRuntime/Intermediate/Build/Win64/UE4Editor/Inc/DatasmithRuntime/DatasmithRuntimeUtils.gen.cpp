// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/DatasmithRuntimeUtils.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDatasmithRuntimeUtils() {}
// Cross Module References
	DATASMITHRUNTIME_API UClass* Z_Construct_UClass_URuntimeMesh_NoRegister();
	DATASMITHRUNTIME_API UClass* Z_Construct_UClass_URuntimeMesh();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMesh();
	UPackage* Z_Construct_UPackage__Script_DatasmithRuntime();
// End Cross Module References
	void URuntimeMesh::StaticRegisterNativesURuntimeMesh()
	{
	}
	UClass* Z_Construct_UClass_URuntimeMesh_NoRegister()
	{
		return URuntimeMesh::StaticClass();
	}
	struct Z_Construct_UClass_URuntimeMesh_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URuntimeMesh_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UStaticMesh,
		(UObject* (*)())Z_Construct_UPackage__Script_DatasmithRuntime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URuntimeMesh_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Class deriving from UStaticMesh to allow the cooking of collision meshes at runtime\n// To do so, bAllowCPUAccess must be true AND  the metod GetWorld() must return a valid world\n" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "DatasmithRuntimeUtils.h" },
		{ "ModuleRelativePath", "Private/DatasmithRuntimeUtils.h" },
		{ "ToolTip", "Class deriving from UStaticMesh to allow the cooking of collision meshes at runtime\nTo do so, bAllowCPUAccess must be true AND  the metod GetWorld() must return a valid world" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URuntimeMesh_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URuntimeMesh>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URuntimeMesh_Statics::ClassParams = {
		&URuntimeMesh::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URuntimeMesh_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URuntimeMesh_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URuntimeMesh()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URuntimeMesh_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URuntimeMesh, 1989168101);
	template<> DATASMITHRUNTIME_API UClass* StaticClass<URuntimeMesh>()
	{
		return URuntimeMesh::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URuntimeMesh(Z_Construct_UClass_URuntimeMesh, &URuntimeMesh::StaticClass, TEXT("/Script/DatasmithRuntime"), TEXT("URuntimeMesh"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URuntimeMesh);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
