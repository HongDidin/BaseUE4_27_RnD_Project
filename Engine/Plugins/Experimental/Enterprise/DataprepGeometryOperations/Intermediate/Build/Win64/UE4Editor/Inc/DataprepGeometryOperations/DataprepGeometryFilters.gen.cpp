// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/DataprepGeometryFilters.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepGeometryFilters() {}
// Cross Module References
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepJacketingFilter_NoRegister();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepJacketingFilter();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepFilterNoFetcher();
	UPackage* Z_Construct_UPackage__Script_DataprepGeometryOperations();
// End Cross Module References
	void UDataprepJacketingFilter::StaticRegisterNativesUDataprepJacketingFilter()
	{
	}
	UClass* Z_Construct_UClass_UDataprepJacketingFilter_NoRegister()
	{
		return UDataprepJacketingFilter::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepJacketingFilter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Accuracy_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Accuracy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MergeDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MergeDistance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepJacketingFilter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepFilterNoFetcher,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepJacketingFilter_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Jacketing/Select Hidden" },
		{ "HideCategories", "Filter" },
		{ "IncludePath", "DataprepGeometryFilters.h" },
		{ "IsBlueprintBase", "false" },
		{ "ModuleRelativePath", "Private/DataprepGeometryFilters.h" },
		{ "ToolTip", "Apply mesh jacketing to selected objects" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_Accuracy_MetaData[] = {
		{ "Category", "JacketingFilter" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Accuracy of the distance field approximation, in cm. */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryFilters.h" },
		{ "ToolTip", "Accuracy of the distance field approximation, in cm." },
		{ "UIMax", "100" },
		{ "UIMin", "0.1" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_Accuracy = { "Accuracy", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepJacketingFilter, Accuracy), METADATA_PARAMS(Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_Accuracy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_Accuracy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_MergeDistance_MetaData[] = {
		{ "Category", "JacketingFilter" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Merge distance used to fill gap, in cm. */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryFilters.h" },
		{ "ToolTip", "Merge distance used to fill gap, in cm." },
		{ "UIMax", "100" },
		{ "UIMin", "0.1" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_MergeDistance = { "MergeDistance", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepJacketingFilter, MergeDistance), METADATA_PARAMS(Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_MergeDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_MergeDistance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepJacketingFilter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_Accuracy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepJacketingFilter_Statics::NewProp_MergeDistance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepJacketingFilter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepJacketingFilter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepJacketingFilter_Statics::ClassParams = {
		&UDataprepJacketingFilter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepJacketingFilter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepJacketingFilter_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepJacketingFilter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepJacketingFilter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepJacketingFilter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepJacketingFilter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepJacketingFilter, 4090271615);
	template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<UDataprepJacketingFilter>()
	{
		return UDataprepJacketingFilter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepJacketingFilter(Z_Construct_UClass_UDataprepJacketingFilter, &UDataprepJacketingFilter::StaticClass, TEXT("/Script/DataprepGeometryOperations"), TEXT("UDataprepJacketingFilter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepJacketingFilter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
