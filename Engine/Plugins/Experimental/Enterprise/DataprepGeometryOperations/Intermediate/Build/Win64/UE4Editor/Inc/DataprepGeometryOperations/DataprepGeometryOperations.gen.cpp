// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/DataprepGeometryOperations.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepGeometryOperations() {}
// Cross Module References
	DATAPREPGEOMETRYOPERATIONS_API UEnum* Z_Construct_UEnum_DataprepGeometryOperations_EPlaneCutKeepSide();
	UPackage* Z_Construct_UPackage__Script_DataprepGeometryOperations();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepRemeshOperation_NoRegister();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepRemeshOperation();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepEditingOperation();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ERemeshType();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepBakeTransformOperation_NoRegister();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepBakeTransformOperation();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepWeldEdgesOperation_NoRegister();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepWeldEdgesOperation();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepSimplifyMeshOperation_NoRegister();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepSimplifyMeshOperation();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepPlaneCutOperation_NoRegister();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepPlaneCutOperation();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	static UEnum* EPlaneCutKeepSide_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_DataprepGeometryOperations_EPlaneCutKeepSide, Z_Construct_UPackage__Script_DataprepGeometryOperations(), TEXT("EPlaneCutKeepSide"));
		}
		return Singleton;
	}
	template<> DATAPREPGEOMETRYOPERATIONS_API UEnum* StaticEnum<EPlaneCutKeepSide>()
	{
		return EPlaneCutKeepSide_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EPlaneCutKeepSide(EPlaneCutKeepSide_StaticEnum, TEXT("/Script/DataprepGeometryOperations"), TEXT("EPlaneCutKeepSide"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_DataprepGeometryOperations_EPlaneCutKeepSide_Hash() { return 3040175385U; }
	UEnum* Z_Construct_UEnum_DataprepGeometryOperations_EPlaneCutKeepSide()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_DataprepGeometryOperations();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EPlaneCutKeepSide"), 0, Get_Z_Construct_UEnum_DataprepGeometryOperations_EPlaneCutKeepSide_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EPlaneCutKeepSide::Positive", (int64)EPlaneCutKeepSide::Positive },
				{ "EPlaneCutKeepSide::Negative", (int64)EPlaneCutKeepSide::Negative },
				{ "EPlaneCutKeepSide::Both", (int64)EPlaneCutKeepSide::Both },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Both.Name", "EPlaneCutKeepSide::Both" },
				{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
				{ "Negative.Name", "EPlaneCutKeepSide::Negative" },
				{ "Positive.Name", "EPlaneCutKeepSide::Positive" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
				nullptr,
				"EPlaneCutKeepSide",
				"EPlaneCutKeepSide",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UDataprepRemeshOperation::StaticRegisterNativesUDataprepRemeshOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepRemeshOperation_NoRegister()
	{
		return UDataprepRemeshOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepRemeshOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetTriangleCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TargetTriangleCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDiscardAttributes_MetaData[];
#endif
		static void NewProp_bDiscardAttributes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDiscardAttributes;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_RemeshType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemeshType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_RemeshType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemeshIterations_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_RemeshIterations;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MeshBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MeshBoundaryConstraint;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GroupBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GroupBoundaryConstraint;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaterialBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MaterialBoundaryConstraint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepRemeshOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Remesh" },
		{ "IncludePath", "DataprepGeometryOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Experimental - Remesh input meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_TargetTriangleCount_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Target triangle count */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Target triangle count" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_TargetTriangleCount = { "TargetTriangleCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRemeshOperation, TargetTriangleCount), METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_TargetTriangleCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_TargetTriangleCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_SmoothingStrength_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of Vertex Smoothing applied within Remeshing */" },
		{ "DisplayName", "Smoothing Rate" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Amount of Vertex Smoothing applied within Remeshing" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_SmoothingStrength = { "SmoothingStrength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRemeshOperation, SmoothingStrength), METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_SmoothingStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_SmoothingStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_bDiscardAttributes_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** If true, UVs and Normals are discarded  */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "If true, UVs and Normals are discarded" },
	};
#endif
	void Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_bDiscardAttributes_SetBit(void* Obj)
	{
		((UDataprepRemeshOperation*)Obj)->bDiscardAttributes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_bDiscardAttributes = { "bDiscardAttributes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepRemeshOperation), &Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_bDiscardAttributes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_bDiscardAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_bDiscardAttributes_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshType_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "Comment", "/** Remeshing type */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Remeshing type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshType = { "RemeshType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRemeshOperation, RemeshType), Z_Construct_UEnum_ModelingOperators_ERemeshType, METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshIterations_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Number of Remeshing passes */" },
		{ "EditCondition", "RemeshType == ERemeshType::FullPass" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Number of Remeshing passes" },
		{ "UIMax", "50" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshIterations = { "RemeshIterations", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRemeshOperation, RemeshIterations), METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshIterations_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshIterations_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MeshBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MeshBoundaryConstraint_MetaData[] = {
		{ "Category", "Remeshing|Boundary Constraints" },
		{ "Comment", "/** Mesh Boundary Constraint Type */" },
		{ "DisplayName", "Mesh Boundary" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Mesh Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MeshBoundaryConstraint = { "MeshBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRemeshOperation, MeshBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MeshBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MeshBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_GroupBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_GroupBoundaryConstraint_MetaData[] = {
		{ "Category", "Remeshing|Boundary Constraints" },
		{ "Comment", "/** Group Boundary Constraint Type */" },
		{ "DisplayName", "Group Boundary" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Group Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_GroupBoundaryConstraint = { "GroupBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRemeshOperation, GroupBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_GroupBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_GroupBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MaterialBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MaterialBoundaryConstraint_MetaData[] = {
		{ "Category", "Remeshing|Boundary Constraints" },
		{ "Comment", "/** Material Boundary Constraint Type */" },
		{ "DisplayName", "Material Boundary" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Material Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MaterialBoundaryConstraint = { "MaterialBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepRemeshOperation, MaterialBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MaterialBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MaterialBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepRemeshOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_TargetTriangleCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_SmoothingStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_bDiscardAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_RemeshIterations,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MeshBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MeshBoundaryConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_GroupBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_GroupBoundaryConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MaterialBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepRemeshOperation_Statics::NewProp_MaterialBoundaryConstraint,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepRemeshOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepRemeshOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepRemeshOperation_Statics::ClassParams = {
		&UDataprepRemeshOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepRemeshOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepRemeshOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepRemeshOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepRemeshOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepRemeshOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepRemeshOperation, 2075448554);
	template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<UDataprepRemeshOperation>()
	{
		return UDataprepRemeshOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepRemeshOperation(Z_Construct_UClass_UDataprepRemeshOperation, &UDataprepRemeshOperation::StaticClass, TEXT("/Script/DataprepGeometryOperations"), TEXT("UDataprepRemeshOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepRemeshOperation);
	void UDataprepBakeTransformOperation::StaticRegisterNativesUDataprepBakeTransformOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepBakeTransformOperation_NoRegister()
	{
		return UDataprepBakeTransformOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepBakeTransformOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBakeRotation_MetaData[];
#endif
		static void NewProp_bBakeRotation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBakeRotation;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_BakeScale_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BakeScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_BakeScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecenterPivot_MetaData[];
#endif
		static void NewProp_bRecenterPivot_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecenterPivot;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Bake Transform" },
		{ "IncludePath", "DataprepGeometryOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Experimental - Bake transform of input meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bBakeRotation_MetaData[] = {
		{ "Category", "BakeTransform" },
		{ "Comment", "/** Bake rotation */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Bake rotation" },
	};
#endif
	void Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bBakeRotation_SetBit(void* Obj)
	{
		((UDataprepBakeTransformOperation*)Obj)->bBakeRotation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bBakeRotation = { "bBakeRotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepBakeTransformOperation), &Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bBakeRotation_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bBakeRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bBakeRotation_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_BakeScale_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_BakeScale_MetaData[] = {
		{ "Category", "BakeTransform" },
		{ "Comment", "/** Bake scale */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Bake scale" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_BakeScale = { "BakeScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepBakeTransformOperation, BakeScale), Z_Construct_UEnum_MeshModelingTools_EBakeScaleMethod, METADATA_PARAMS(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_BakeScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_BakeScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bRecenterPivot_MetaData[] = {
		{ "Category", "BakeTransform" },
		{ "Comment", "/** Recenter pivot after baking transform */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Recenter pivot after baking transform" },
	};
#endif
	void Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bRecenterPivot_SetBit(void* Obj)
	{
		((UDataprepBakeTransformOperation*)Obj)->bRecenterPivot = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bRecenterPivot = { "bRecenterPivot", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepBakeTransformOperation), &Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bRecenterPivot_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bRecenterPivot_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bRecenterPivot_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bBakeRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_BakeScale_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_BakeScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::NewProp_bRecenterPivot,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepBakeTransformOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::ClassParams = {
		&UDataprepBakeTransformOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepBakeTransformOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepBakeTransformOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepBakeTransformOperation, 1245477558);
	template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<UDataprepBakeTransformOperation>()
	{
		return UDataprepBakeTransformOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepBakeTransformOperation(Z_Construct_UClass_UDataprepBakeTransformOperation, &UDataprepBakeTransformOperation::StaticClass, TEXT("/Script/DataprepGeometryOperations"), TEXT("UDataprepBakeTransformOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepBakeTransformOperation);
	void UDataprepWeldEdgesOperation::StaticRegisterNativesUDataprepWeldEdgesOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepWeldEdgesOperation_NoRegister()
	{
		return UDataprepWeldEdgesOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Tolerance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyUnique_MetaData[];
#endif
		static void NewProp_bOnlyUnique_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyUnique;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Weld Edges" },
		{ "IncludePath", "DataprepGeometryOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Experimental - Weld edges of input meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_Tolerance_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1000.0" },
		{ "ClampMin", "0.00000001" },
		{ "Comment", "/** Merge search tolerance */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Merge search tolerance" },
		{ "UIMax", "0.01" },
		{ "UIMin", "0.000001" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_Tolerance = { "Tolerance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepWeldEdgesOperation, Tolerance), METADATA_PARAMS(Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_Tolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_Tolerance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_bOnlyUnique_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Apply to only unique pairs */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Apply to only unique pairs" },
	};
#endif
	void Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_bOnlyUnique_SetBit(void* Obj)
	{
		((UDataprepWeldEdgesOperation*)Obj)->bOnlyUnique = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_bOnlyUnique = { "bOnlyUnique", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepWeldEdgesOperation), &Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_bOnlyUnique_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_bOnlyUnique_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_bOnlyUnique_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_Tolerance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::NewProp_bOnlyUnique,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepWeldEdgesOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::ClassParams = {
		&UDataprepWeldEdgesOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepWeldEdgesOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepWeldEdgesOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepWeldEdgesOperation, 3551458917);
	template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<UDataprepWeldEdgesOperation>()
	{
		return UDataprepWeldEdgesOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepWeldEdgesOperation(Z_Construct_UClass_UDataprepWeldEdgesOperation, &UDataprepWeldEdgesOperation::StaticClass, TEXT("/Script/DataprepGeometryOperations"), TEXT("UDataprepWeldEdgesOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepWeldEdgesOperation);
	void UDataprepSimplifyMeshOperation::StaticRegisterNativesUDataprepSimplifyMeshOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepSimplifyMeshOperation_NoRegister()
	{
		return UDataprepSimplifyMeshOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TargetPercentage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDiscardAttributes_MetaData[];
#endif
		static void NewProp_bDiscardAttributes_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDiscardAttributes;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MeshBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MeshBoundaryConstraint;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GroupBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GroupBoundaryConstraint;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaterialBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MaterialBoundaryConstraint;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "ObjectOperation" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Simplify Mesh" },
		{ "IncludePath", "DataprepGeometryOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Experimental - Simplify input meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_TargetPercentage_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Target percentage of original triangle count */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Target percentage of original triangle count" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_TargetPercentage = { "TargetPercentage", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSimplifyMeshOperation, TargetPercentage), METADATA_PARAMS(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_TargetPercentage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_TargetPercentage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_bDiscardAttributes_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** If true, UVs and Normals are discarded  */" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "If true, UVs and Normals are discarded" },
	};
#endif
	void Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_bDiscardAttributes_SetBit(void* Obj)
	{
		((UDataprepSimplifyMeshOperation*)Obj)->bDiscardAttributes = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_bDiscardAttributes = { "bDiscardAttributes", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepSimplifyMeshOperation), &Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_bDiscardAttributes_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_bDiscardAttributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_bDiscardAttributes_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MeshBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MeshBoundaryConstraint_MetaData[] = {
		{ "Category", "SimplifyMesh|Boundary Constraints" },
		{ "Comment", "/** Mesh Boundary Constraint Type */" },
		{ "DisplayName", "Mesh Boundary" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Mesh Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MeshBoundaryConstraint = { "MeshBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSimplifyMeshOperation, MeshBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MeshBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MeshBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_GroupBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_GroupBoundaryConstraint_MetaData[] = {
		{ "Category", "SimplifyMesh|Boundary Constraints" },
		{ "Comment", "/** Group Boundary Constraint Type */" },
		{ "DisplayName", "Group Boundary" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Group Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_GroupBoundaryConstraint = { "GroupBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSimplifyMeshOperation, GroupBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_GroupBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_GroupBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MaterialBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MaterialBoundaryConstraint_MetaData[] = {
		{ "Category", "SimplifyMesh|Boundary Constraints" },
		{ "Comment", "/** Material Boundary Constraint Type */" },
		{ "DisplayName", "Material Boundary" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Material Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MaterialBoundaryConstraint = { "MaterialBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepSimplifyMeshOperation, MaterialBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MaterialBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MaterialBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_TargetPercentage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_bDiscardAttributes,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MeshBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MeshBoundaryConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_GroupBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_GroupBoundaryConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MaterialBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::NewProp_MaterialBoundaryConstraint,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepSimplifyMeshOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::ClassParams = {
		&UDataprepSimplifyMeshOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepSimplifyMeshOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepSimplifyMeshOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepSimplifyMeshOperation, 2068800579);
	template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<UDataprepSimplifyMeshOperation>()
	{
		return UDataprepSimplifyMeshOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepSimplifyMeshOperation(Z_Construct_UClass_UDataprepSimplifyMeshOperation, &UDataprepSimplifyMeshOperation::StaticClass, TEXT("/Script/DataprepGeometryOperations"), TEXT("UDataprepSimplifyMeshOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepSimplifyMeshOperation);
	void UDataprepPlaneCutOperation::StaticRegisterNativesUDataprepPlaneCutOperation()
	{
	}
	UClass* Z_Construct_UClass_UDataprepPlaneCutOperation_NoRegister()
	{
		return UDataprepPlaneCutOperation::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepPlaneCutOperation_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutPlaneOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CutPlaneOrigin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutPlaneNormalAngles_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CutPlaneNormalAngles;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_CutPlaneKeepSide_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CutPlaneKeepSide_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_CutPlaneKeepSide;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpacingBetweenHalves_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SpacingBetweenHalves;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFillCutHole_MetaData[];
#endif
		static void NewProp_bFillCutHole_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFillCutHole;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportSeparatePieces_MetaData[];
#endif
		static void NewProp_bExportSeparatePieces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportSeparatePieces;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepEditingOperation,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::Class_MetaDataParams[] = {
		{ "Category", "MeshOperation" },
		{ "DevelopmentStatus", "Experimental" },
		{ "DisplayName", "Plane Cut" },
		{ "IncludePath", "DataprepGeometryOperations.h" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Experimental - Plane cut input meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneOrigin_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "/** Origin of the cutting plane */" },
		{ "DisplayName", "Plane's Origin" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Origin of the cutting plane" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneOrigin = { "CutPlaneOrigin", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepPlaneCutOperation, CutPlaneOrigin), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneOrigin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneNormalAngles_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "/** Euler angles of the normal to the cutting plane (default plane is XY plane) */" },
		{ "DisplayName", "Plane's Orientation" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Euler angles of the normal to the cutting plane (default plane is XY plane)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneNormalAngles = { "CutPlaneNormalAngles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepPlaneCutOperation, CutPlaneNormalAngles), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneNormalAngles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneNormalAngles_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneKeepSide_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneKeepSide_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "/** Specify which section(s) of the cut to keep. If 'Keep Both' is selected, both sides of the \n\x09\x09""cut are computed and a new actor added with the negative side. \n\x09*/" },
		{ "DisplayName", "Side(s) To Keep" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "Specify which section(s) of the cut to keep. If 'Keep Both' is selected, both sides of the\n              cut are computed and a new actor added with the negative side." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneKeepSide = { "CutPlaneKeepSide", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepPlaneCutOperation, CutPlaneKeepSide), Z_Construct_UEnum_DataprepGeometryOperations_EPlaneCutKeepSide, METADATA_PARAMS(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneKeepSide_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneKeepSide_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_SpacingBetweenHalves_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "ClampMin", "0" },
		{ "Comment", "/** If keeping both halves, separate the two pieces by this amount */" },
		{ "EditCondition", "CutPlaneKeepSide == EPlaneCutKeepSide::Both" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "If keeping both halves, separate the two pieces by this amount" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_SpacingBetweenHalves = { "SpacingBetweenHalves", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepPlaneCutOperation, SpacingBetweenHalves), METADATA_PARAMS(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_SpacingBetweenHalves_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_SpacingBetweenHalves_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bFillCutHole_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "/** If true, the cut surface is filled with simple planar hole fill surface(s) */" },
		{ "DisplayName", "Fill Holes" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "If true, the cut surface is filled with simple planar hole fill surface(s)" },
	};
#endif
	void Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bFillCutHole_SetBit(void* Obj)
	{
		((UDataprepPlaneCutOperation*)Obj)->bFillCutHole = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bFillCutHole = { "bFillCutHole", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepPlaneCutOperation), &Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bFillCutHole_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bFillCutHole_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bFillCutHole_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bExportSeparatePieces_MetaData[] = {
		{ "Category", "PlaneCut" },
		{ "Comment", "/** If true, meshes cut into multiple pieces will be saved as separate assets. */" },
		{ "DisplayName", "Export Separated Pieces As New Mesh Assets" },
		{ "ModuleRelativePath", "Private/DataprepGeometryOperations.h" },
		{ "ToolTip", "If true, meshes cut into multiple pieces will be saved as separate assets." },
	};
#endif
	void Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bExportSeparatePieces_SetBit(void* Obj)
	{
		((UDataprepPlaneCutOperation*)Obj)->bExportSeparatePieces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bExportSeparatePieces = { "bExportSeparatePieces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepPlaneCutOperation), &Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bExportSeparatePieces_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bExportSeparatePieces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bExportSeparatePieces_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneOrigin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneNormalAngles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneKeepSide_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_CutPlaneKeepSide,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_SpacingBetweenHalves,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bFillCutHole,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::NewProp_bExportSeparatePieces,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepPlaneCutOperation>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::ClassParams = {
		&UDataprepPlaneCutOperation::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepPlaneCutOperation()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepPlaneCutOperation_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepPlaneCutOperation, 1521336772);
	template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<UDataprepPlaneCutOperation>()
	{
		return UDataprepPlaneCutOperation::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepPlaneCutOperation(Z_Construct_UClass_UDataprepPlaneCutOperation, &UDataprepPlaneCutOperation::StaticClass, TEXT("/Script/DataprepGeometryOperations"), TEXT("UDataprepPlaneCutOperation"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepPlaneCutOperation);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
