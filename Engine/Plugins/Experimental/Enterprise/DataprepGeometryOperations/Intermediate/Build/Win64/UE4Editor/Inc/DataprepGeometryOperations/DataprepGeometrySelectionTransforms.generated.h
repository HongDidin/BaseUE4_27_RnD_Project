// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef DATAPREPGEOMETRYOPERATIONS_DataprepGeometrySelectionTransforms_generated_h
#error "DataprepGeometrySelectionTransforms.generated.h already included, missing '#pragma once' in DataprepGeometrySelectionTransforms.h"
#endif
#define DATAPREPGEOMETRYOPERATIONS_DataprepGeometrySelectionTransforms_generated_h

#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDataprepOverlappingActorsSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepOverlappingActorsSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepGeometryOperations"), NO_API) \
	DECLARE_SERIALIZER(UDataprepOverlappingActorsSelectionTransform)


#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUDataprepOverlappingActorsSelectionTransform(); \
	friend struct Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics; \
public: \
	DECLARE_CLASS(UDataprepOverlappingActorsSelectionTransform, UDataprepSelectionTransform, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/DataprepGeometryOperations"), NO_API) \
	DECLARE_SERIALIZER(UDataprepOverlappingActorsSelectionTransform)


#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepOverlappingActorsSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepOverlappingActorsSelectionTransform) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepOverlappingActorsSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepOverlappingActorsSelectionTransform); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepOverlappingActorsSelectionTransform(UDataprepOverlappingActorsSelectionTransform&&); \
	NO_API UDataprepOverlappingActorsSelectionTransform(const UDataprepOverlappingActorsSelectionTransform&); \
public:


#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDataprepOverlappingActorsSelectionTransform(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDataprepOverlappingActorsSelectionTransform(UDataprepOverlappingActorsSelectionTransform&&); \
	NO_API UDataprepOverlappingActorsSelectionTransform(const UDataprepOverlappingActorsSelectionTransform&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDataprepOverlappingActorsSelectionTransform); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDataprepOverlappingActorsSelectionTransform); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDataprepOverlappingActorsSelectionTransform)


#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__JacketingAccuracy() { return STRUCT_OFFSET(UDataprepOverlappingActorsSelectionTransform, JacketingAccuracy); } \
	FORCEINLINE static uint32 __PPO__bSelectOverlapping() { return STRUCT_OFFSET(UDataprepOverlappingActorsSelectionTransform, bSelectOverlapping); }


#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_17_PROLOG
#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_INCLASS \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<class UDataprepOverlappingActorsSelectionTransform>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Enterprise_DataprepGeometryOperations_Source_Private_DataprepGeometrySelectionTransforms_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
