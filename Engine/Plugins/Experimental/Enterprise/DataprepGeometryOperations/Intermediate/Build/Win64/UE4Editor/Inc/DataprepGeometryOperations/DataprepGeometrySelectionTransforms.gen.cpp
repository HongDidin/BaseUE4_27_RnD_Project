// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Source/Private/DataprepGeometrySelectionTransforms.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDataprepGeometrySelectionTransforms() {}
// Cross Module References
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_NoRegister();
	DATAPREPGEOMETRYOPERATIONS_API UClass* Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform();
	DATAPREPCORE_API UClass* Z_Construct_UClass_UDataprepSelectionTransform();
	UPackage* Z_Construct_UPackage__Script_DataprepGeometryOperations();
// End Cross Module References
	void UDataprepOverlappingActorsSelectionTransform::StaticRegisterNativesUDataprepOverlappingActorsSelectionTransform()
	{
	}
	UClass* Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_NoRegister()
	{
		return UDataprepOverlappingActorsSelectionTransform::StaticClass();
	}
	struct Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JacketingAccuracy_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_JacketingAccuracy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectOverlapping_MetaData[];
#endif
		static void NewProp_bSelectOverlapping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectOverlapping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDataprepSelectionTransform,
		(UObject* (*)())Z_Construct_UPackage__Script_DataprepGeometryOperations,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::Class_MetaDataParams[] = {
		{ "Category", "SelectionTransform" },
		{ "DisplayName", "Select In Volume" },
		{ "IncludePath", "DataprepGeometrySelectionTransforms.h" },
		{ "ModuleRelativePath", "Private/DataprepGeometrySelectionTransforms.h" },
		{ "ToolTip", "Return all actors overlapping the selected actors" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_JacketingAccuracy_MetaData[] = {
		{ "Category", "SelectionTransform" },
		{ "ClampMin", "0" },
		{ "Comment", "// Accuracy of the distance field approximation\n" },
		{ "ModuleRelativePath", "Private/DataprepGeometrySelectionTransforms.h" },
		{ "ToolTip", "Accuracy of the distance field approximation" },
		{ "UIMax", "100" },
		{ "UIMin", "0.1" },
		{ "Units", "cm" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_JacketingAccuracy = { "JacketingAccuracy", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDataprepOverlappingActorsSelectionTransform, JacketingAccuracy), METADATA_PARAMS(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_JacketingAccuracy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_JacketingAccuracy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_bSelectOverlapping_MetaData[] = {
		{ "Category", "SelectionTransform" },
		{ "Comment", "// If checked, select fully inside + overlapping actors. Else, select only actors that are fully inside.\n" },
		{ "ModuleRelativePath", "Private/DataprepGeometrySelectionTransforms.h" },
		{ "ToolTip", "If checked, select fully inside + overlapping actors. Else, select only actors that are fully inside." },
	};
#endif
	void Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_bSelectOverlapping_SetBit(void* Obj)
	{
		((UDataprepOverlappingActorsSelectionTransform*)Obj)->bSelectOverlapping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_bSelectOverlapping = { "bSelectOverlapping", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UDataprepOverlappingActorsSelectionTransform), &Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_bSelectOverlapping_SetBit, METADATA_PARAMS(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_bSelectOverlapping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_bSelectOverlapping_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_JacketingAccuracy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::NewProp_bSelectOverlapping,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDataprepOverlappingActorsSelectionTransform>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::ClassParams = {
		&UDataprepOverlappingActorsSelectionTransform::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDataprepOverlappingActorsSelectionTransform, 2637980708);
	template<> DATAPREPGEOMETRYOPERATIONS_API UClass* StaticClass<UDataprepOverlappingActorsSelectionTransform>()
	{
		return UDataprepOverlappingActorsSelectionTransform::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDataprepOverlappingActorsSelectionTransform(Z_Construct_UClass_UDataprepOverlappingActorsSelectionTransform, &UDataprepOverlappingActorsSelectionTransform::StaticClass, TEXT("/Script/DataprepGeometryOperations"), TEXT("UDataprepOverlappingActorsSelectionTransform"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDataprepOverlappingActorsSelectionTransform);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
