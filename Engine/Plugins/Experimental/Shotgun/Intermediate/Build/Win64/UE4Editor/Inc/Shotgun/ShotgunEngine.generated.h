// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UObject;
class UShotgunEngine;
struct FShotgunMenuItem;
#ifdef SHOTGUN_ShotgunEngine_generated_h
#error "ShotgunEngine.generated.h already included, missing '#pragma once' in ShotgunEngine.h"
#endif
#define SHOTGUN_ShotgunEngine_generated_h

#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_15_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FShotgunMenuItem_Statics; \
	SHOTGUN_API static class UScriptStruct* StaticStruct();


template<> SHOTGUN_API UScriptStruct* StaticStruct<struct FShotgunMenuItem>();

#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_SPARSE_DATA
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetSelectedActors); \
	DECLARE_FUNCTION(execGetShotgunWorkDir); \
	DECLARE_FUNCTION(execGetReferencedAssets); \
	DECLARE_FUNCTION(execOnEngineInitialized); \
	DECLARE_FUNCTION(execGetInstance);


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetSelectedActors); \
	DECLARE_FUNCTION(execGetShotgunWorkDir); \
	DECLARE_FUNCTION(execGetReferencedAssets); \
	DECLARE_FUNCTION(execOnEngineInitialized); \
	DECLARE_FUNCTION(execGetInstance);


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_EVENT_PARMS \
	struct ShotgunEngine_eventExecuteCommand_Parms \
	{ \
		FString CommandName; \
	}; \
	struct ShotgunEngine_eventGetShotgunMenuItems_Parms \
	{ \
		TArray<FShotgunMenuItem> ReturnValue; \
	};


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShotgunEngine(); \
	friend struct Z_Construct_UClass_UShotgunEngine_Statics; \
public: \
	DECLARE_CLASS(UShotgunEngine, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Shotgun"), NO_API) \
	DECLARE_SERIALIZER(UShotgunEngine)


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUShotgunEngine(); \
	friend struct Z_Construct_UClass_UShotgunEngine_Statics; \
public: \
	DECLARE_CLASS(UShotgunEngine, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Shotgun"), NO_API) \
	DECLARE_SERIALIZER(UShotgunEngine)


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShotgunEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShotgunEngine) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShotgunEngine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShotgunEngine); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShotgunEngine(UShotgunEngine&&); \
	NO_API UShotgunEngine(const UShotgunEngine&); \
public:


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShotgunEngine(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShotgunEngine(UShotgunEngine&&); \
	NO_API UShotgunEngine(const UShotgunEngine&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShotgunEngine); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShotgunEngine); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShotgunEngine)


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_40_PROLOG \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_EVENT_PARMS


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_INCLASS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_SPARSE_DATA \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOTGUN_API UClass* StaticClass<class UShotgunEngine>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunEngine_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
