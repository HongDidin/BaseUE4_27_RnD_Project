// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Shotgun/Private/ShotgunEngine.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShotgunEngine() {}
// Cross Module References
	SHOTGUN_API UScriptStruct* Z_Construct_UScriptStruct_FShotgunMenuItem();
	UPackage* Z_Construct_UPackage__Script_Shotgun();
	SHOTGUN_API UClass* Z_Construct_UClass_UShotgunEngine_NoRegister();
	SHOTGUN_API UClass* Z_Construct_UClass_UShotgunEngine();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FAssetData();
// End Cross Module References
class UScriptStruct* FShotgunMenuItem::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SHOTGUN_API uint32 Get_Z_Construct_UScriptStruct_FShotgunMenuItem_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FShotgunMenuItem, Z_Construct_UPackage__Script_Shotgun(), TEXT("ShotgunMenuItem"), sizeof(FShotgunMenuItem), Get_Z_Construct_UScriptStruct_FShotgunMenuItem_Hash());
	}
	return Singleton;
}
template<> SHOTGUN_API UScriptStruct* StaticStruct<FShotgunMenuItem>()
{
	return FShotgunMenuItem::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FShotgunMenuItem(FShotgunMenuItem::StaticStruct, TEXT("/Script/Shotgun"), TEXT("ShotgunMenuItem"), false, nullptr, nullptr);
static struct FScriptStruct_Shotgun_StaticRegisterNativesFShotgunMenuItem
{
	FScriptStruct_Shotgun_StaticRegisterNativesFShotgunMenuItem()
	{
		UScriptStruct::DeferCppStructOps<FShotgunMenuItem>(FName(TEXT("ShotgunMenuItem")));
	}
} ScriptStruct_Shotgun_StaticRegisterNativesFShotgunMenuItem;
	struct Z_Construct_UScriptStruct_FShotgunMenuItem_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Title_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Title;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Description_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Description;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Type_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Type;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FShotgunMenuItem>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Command name for internal use */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Command name for internal use" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FShotgunMenuItem, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Title_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Text to display in the menu */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Text to display in the menu" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Title = { "Title", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FShotgunMenuItem, Title), METADATA_PARAMS(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Title_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Title_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Description_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Description text for the tooltip */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Description text for the tooltip" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Description = { "Description", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FShotgunMenuItem, Description), METADATA_PARAMS(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Description_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Description_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Type_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Menu item type to help interpret the command */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Menu item type to help interpret the command" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Type = { "Type", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FShotgunMenuItem, Type), METADATA_PARAMS(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Type_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Type_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Title,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Description,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::NewProp_Type,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Shotgun,
		nullptr,
		&NewStructOps,
		"ShotgunMenuItem",
		sizeof(FShotgunMenuItem),
		alignof(FShotgunMenuItem),
		Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FShotgunMenuItem()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FShotgunMenuItem_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Shotgun();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ShotgunMenuItem"), sizeof(FShotgunMenuItem), Get_Z_Construct_UScriptStruct_FShotgunMenuItem_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FShotgunMenuItem_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FShotgunMenuItem_Hash() { return 3983110948U; }
	DEFINE_FUNCTION(UShotgunEngine::execGetSelectedActors)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<AActor*>*)Z_Param__Result=P_THIS->GetSelectedActors();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UShotgunEngine::execGetShotgunWorkDir)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UShotgunEngine::GetShotgunWorkDir();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UShotgunEngine::execGetReferencedAssets)
	{
		P_GET_OBJECT(AActor,Z_Param_Actor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=P_THIS->GetReferencedAssets(Z_Param_Actor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UShotgunEngine::execOnEngineInitialized)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnEngineInitialized();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UShotgunEngine::execGetInstance)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UShotgunEngine**)Z_Param__Result=UShotgunEngine::GetInstance();
		P_NATIVE_END;
	}
	static FName NAME_UShotgunEngine_ExecuteCommand = FName(TEXT("ExecuteCommand"));
	void UShotgunEngine::ExecuteCommand(const FString& CommandName) const
	{
		ShotgunEngine_eventExecuteCommand_Parms Parms;
		Parms.CommandName=CommandName;
		const_cast<UShotgunEngine*>(this)->ProcessEvent(FindFunctionChecked(NAME_UShotgunEngine_ExecuteCommand),&Parms);
	}
	static FName NAME_UShotgunEngine_GetShotgunMenuItems = FName(TEXT("GetShotgunMenuItems"));
	TArray<FShotgunMenuItem> UShotgunEngine::GetShotgunMenuItems() const
	{
		ShotgunEngine_eventGetShotgunMenuItems_Parms Parms;
		const_cast<UShotgunEngine*>(this)->ProcessEvent(FindFunctionChecked(NAME_UShotgunEngine_GetShotgunMenuItems),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UShotgunEngine_Shutdown = FName(TEXT("Shutdown"));
	void UShotgunEngine::Shutdown() const
	{
		const_cast<UShotgunEngine*>(this)->ProcessEvent(FindFunctionChecked(NAME_UShotgunEngine_Shutdown),NULL);
	}
	void UShotgunEngine::StaticRegisterNativesUShotgunEngine()
	{
		UClass* Class = UShotgunEngine::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetInstance", &UShotgunEngine::execGetInstance },
			{ "GetReferencedAssets", &UShotgunEngine::execGetReferencedAssets },
			{ "GetSelectedActors", &UShotgunEngine::execGetSelectedActors },
			{ "GetShotgunWorkDir", &UShotgunEngine::execGetShotgunWorkDir },
			{ "OnEngineInitialized", &UShotgunEngine::execOnEngineInitialized },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CommandName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CommandName;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::NewProp_CommandName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::NewProp_CommandName = { "CommandName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ShotgunEngine_eventExecuteCommand_Parms, CommandName), METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::NewProp_CommandName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::NewProp_CommandName_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::NewProp_CommandName,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Execute a Shotgun command by name in the Python Shotgun Engine */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Execute a Shotgun command by name in the Python Shotgun Engine" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "ExecuteCommand", nullptr, nullptr, sizeof(ShotgunEngine_eventExecuteCommand_Parms), Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_ExecuteCommand()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_ExecuteCommand_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics
	{
		struct ShotgunEngine_eventGetInstance_Parms
		{
			UShotgunEngine* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ShotgunEngine_eventGetInstance_Parms, ReturnValue), Z_Construct_UClass_UShotgunEngine_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Get the instance of the Python Shotgun Engine */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Get the instance of the Python Shotgun Engine" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "GetInstance", nullptr, nullptr, sizeof(ShotgunEngine_eventGetInstance_Parms), Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_GetInstance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_GetInstance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics
	{
		struct ShotgunEngine_eventGetReferencedAssets_Parms
		{
			const AActor* Actor;
			TArray<UObject*> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_Actor_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ShotgunEngine_eventGetReferencedAssets_Parms, Actor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_Actor_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_Actor_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ShotgunEngine_eventGetReferencedAssets_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Get the assets that are referenced by the given Actor */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Get the assets that are referenced by the given Actor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "GetReferencedAssets", nullptr, nullptr, sizeof(ShotgunEngine_eventGetReferencedAssets_Parms), Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics
	{
		struct ShotgunEngine_eventGetSelectedActors_Parms
		{
			TArray<AActor*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ShotgunEngine_eventGetSelectedActors_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Selected actors to be used for Shotgun commands */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Selected actors to be used for Shotgun commands" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "GetSelectedActors", nullptr, nullptr, sizeof(ShotgunEngine_eventGetSelectedActors_Parms), Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_GetSelectedActors()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_GetSelectedActors_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FShotgunMenuItem, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ShotgunEngine_eventGetShotgunMenuItems_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Get the available Shotgun commands from the Python Shotgun Engine */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Get the available Shotgun commands from the Python Shotgun Engine" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "GetShotgunMenuItems", nullptr, nullptr, sizeof(ShotgunEngine_eventGetShotgunMenuItems_Parms), Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics
	{
		struct ShotgunEngine_eventGetShotgunWorkDir_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ShotgunEngine_eventGetShotgunWorkDir_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Get the root path for the Shotgun work area */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Get the root path for the Shotgun work area" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "GetShotgunWorkDir", nullptr, nullptr, sizeof(ShotgunEngine_eventGetShotgunWorkDir_Parms), Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Callback for when the Python Shotgun Engine has finished initialization */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Callback for when the Python Shotgun Engine has finished initialization" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "OnEngineInitialized", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UShotgunEngine_Shutdown_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UShotgunEngine_Shutdown_Statics::Function_MetaDataParams[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Shut down the Python Shotgun Engine */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Shut down the Python Shotgun Engine" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UShotgunEngine_Shutdown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UShotgunEngine, nullptr, "Shutdown", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x48020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UShotgunEngine_Shutdown_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UShotgunEngine_Shutdown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UShotgunEngine_Shutdown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UShotgunEngine_Shutdown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UShotgunEngine_NoRegister()
	{
		return UShotgunEngine::StaticClass();
	}
	struct Z_Construct_UClass_UShotgunEngine_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SelectedAssets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedAssets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectedAssets;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectedActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UShotgunEngine_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Shotgun,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UShotgunEngine_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UShotgunEngine_ExecuteCommand, "ExecuteCommand" }, // 2590076244
		{ &Z_Construct_UFunction_UShotgunEngine_GetInstance, "GetInstance" }, // 1661806157
		{ &Z_Construct_UFunction_UShotgunEngine_GetReferencedAssets, "GetReferencedAssets" }, // 4233641653
		{ &Z_Construct_UFunction_UShotgunEngine_GetSelectedActors, "GetSelectedActors" }, // 3657029169
		{ &Z_Construct_UFunction_UShotgunEngine_GetShotgunMenuItems, "GetShotgunMenuItems" }, // 684662705
		{ &Z_Construct_UFunction_UShotgunEngine_GetShotgunWorkDir, "GetShotgunWorkDir" }, // 3784330742
		{ &Z_Construct_UFunction_UShotgunEngine_OnEngineInitialized, "OnEngineInitialized" }, // 3094227715
		{ &Z_Construct_UFunction_UShotgunEngine_Shutdown, "Shutdown" }, // 4051339298
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShotgunEngine_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Wrapper for the Python Shotgun Engine\n * The functions are implemented in Python by a class that derives from this one\n */" },
		{ "IncludePath", "ShotgunEngine.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Wrapper for the Python Shotgun Engine\nThe functions are implemented in Python by a class that derives from this one" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedAssets_Inner = { "SelectedAssets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAssetData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedAssets_MetaData[] = {
		{ "Category", "Python" },
		{ "Comment", "/** Selected assets to be used for Shotgun commands */" },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
		{ "ToolTip", "Selected assets to be used for Shotgun commands" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedAssets = { "SelectedAssets", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShotgunEngine, SelectedAssets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedAssets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedAssets_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedActors_Inner = { "SelectedActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedActors_MetaData[] = {
		{ "Category", "Python" },
		{ "Deprecated", "" },
		{ "DeprecationMessage", "SelectedActors is deprecated, use GetSelectedActors instead." },
		{ "ModuleRelativePath", "Private/ShotgunEngine.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedActors = { "SelectedActors", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShotgunEngine, SelectedActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UShotgunEngine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedAssets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShotgunEngine_Statics::NewProp_SelectedActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UShotgunEngine_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UShotgunEngine>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UShotgunEngine_Statics::ClassParams = {
		&UShotgunEngine::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UShotgunEngine_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UShotgunEngine_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UShotgunEngine_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UShotgunEngine_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UShotgunEngine()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UShotgunEngine_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UShotgunEngine, 2726906293);
	template<> SHOTGUN_API UClass* StaticClass<UShotgunEngine>()
	{
		return UShotgunEngine::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UShotgunEngine(Z_Construct_UClass_UShotgunEngine, &UShotgunEngine::StaticClass, TEXT("/Script/Shotgun"), TEXT("UShotgunEngine"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UShotgunEngine);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
