// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SHOTGUN_ShotgunSettings_generated_h
#error "ShotgunSettings.generated.h already included, missing '#pragma once' in ShotgunSettings.h"
#endif
#define SHOTGUN_ShotgunSettings_generated_h

#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_SPARSE_DATA
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_RPC_WRAPPERS
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUShotgunSettings(); \
	friend struct Z_Construct_UClass_UShotgunSettings_Statics; \
public: \
	DECLARE_CLASS(UShotgunSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Shotgun"), NO_API) \
	DECLARE_SERIALIZER(UShotgunSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUShotgunSettings(); \
	friend struct Z_Construct_UClass_UShotgunSettings_Statics; \
public: \
	DECLARE_CLASS(UShotgunSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Shotgun"), NO_API) \
	DECLARE_SERIALIZER(UShotgunSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShotgunSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShotgunSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShotgunSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShotgunSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShotgunSettings(UShotgunSettings&&); \
	NO_API UShotgunSettings(const UShotgunSettings&); \
public:


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UShotgunSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UShotgunSettings(UShotgunSettings&&); \
	NO_API UShotgunSettings(const UShotgunSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UShotgunSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UShotgunSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UShotgunSettings)


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_9_PROLOG
#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_RPC_WRAPPERS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_INCLASS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SHOTGUN_API UClass* StaticClass<class UShotgunSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_Shotgun_Source_Shotgun_Private_ShotgunSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
