// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Shotgun/Private/ShotgunSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeShotgunSettings() {}
// Cross Module References
	SHOTGUN_API UClass* Z_Construct_UClass_UShotgunSettings_NoRegister();
	SHOTGUN_API UClass* Z_Construct_UClass_UShotgunSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Shotgun();
// End Cross Module References
	void UShotgunSettings::StaticRegisterNativesUShotgunSettings()
	{
	}
	UClass* Z_Construct_UClass_UShotgunSettings_NoRegister()
	{
		return UShotgunSettings::StaticClass();
	}
	struct Z_Construct_UClass_UShotgunSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_MetaDataTagsForAssetRegistry_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MetaDataTagsForAssetRegistry_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_MetaDataTagsForAssetRegistry;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UShotgunSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Shotgun,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShotgunSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ShotgunSettings.h" },
		{ "ModuleRelativePath", "Private/ShotgunSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_UShotgunSettings_Statics::NewProp_MetaDataTagsForAssetRegistry_ElementProp = { "MetaDataTagsForAssetRegistry", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UShotgunSettings_Statics::NewProp_MetaDataTagsForAssetRegistry_MetaData[] = {
		{ "Category", "Asset Registry" },
		{ "Comment", "/** The metadata tags to be transferred to the Asset Registry. */" },
		{ "DisplayName", "Metadata Tags For Asset Registry" },
		{ "ModuleRelativePath", "Private/ShotgunSettings.h" },
		{ "ToolTip", "The metadata tags to be transferred to the Asset Registry." },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_UShotgunSettings_Statics::NewProp_MetaDataTagsForAssetRegistry = { "MetaDataTagsForAssetRegistry", nullptr, (EPropertyFlags)0x0010040000004001, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UShotgunSettings, MetaDataTagsForAssetRegistry), METADATA_PARAMS(Z_Construct_UClass_UShotgunSettings_Statics::NewProp_MetaDataTagsForAssetRegistry_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UShotgunSettings_Statics::NewProp_MetaDataTagsForAssetRegistry_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UShotgunSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShotgunSettings_Statics::NewProp_MetaDataTagsForAssetRegistry_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UShotgunSettings_Statics::NewProp_MetaDataTagsForAssetRegistry,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UShotgunSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UShotgunSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UShotgunSettings_Statics::ClassParams = {
		&UShotgunSettings::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UShotgunSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UShotgunSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UShotgunSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UShotgunSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UShotgunSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UShotgunSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UShotgunSettings, 505249186);
	template<> SHOTGUN_API UClass* StaticClass<UShotgunSettings>()
	{
		return UShotgunSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UShotgunSettings(Z_Construct_UClass_UShotgunSettings, &UShotgunSettings::StaticClass, TEXT("/Script/Shotgun"), TEXT("UShotgunSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UShotgunSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
