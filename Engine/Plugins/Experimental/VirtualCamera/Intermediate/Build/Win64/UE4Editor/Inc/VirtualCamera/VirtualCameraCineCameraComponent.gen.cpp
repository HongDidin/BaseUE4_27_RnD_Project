// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/VirtualCameraCineCameraComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVirtualCameraCineCameraComponent() {}
// Cross Module References
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraCineCameraComponent_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraCineCameraComponent();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	CINEMATICCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FCameraFilmbackSettings();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
// End Cross Module References
	void UVirtualCameraCineCameraComponent::StaticRegisterNativesUVirtualCameraCineCameraComponent()
	{
	}
	UClass* Z_Construct_UClass_UVirtualCameraCineCameraComponent_NoRegister()
	{
		return UVirtualCameraCineCameraComponent::StaticClass();
	}
	struct Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ApertureOptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ApertureOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ApertureOptions;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FocalLengthOptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocalLengthOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FocalLengthOptions;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MatteOptions_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatteOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_MatteOptions;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilmbackOptions_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilmbackOptions_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilmbackOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FilmbackOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesiredFilmbackSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DesiredFilmbackSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatteAspectRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MatteAspectRatio;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatteOpacity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MatteOpacity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewSizeRatio_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewSizeRatio;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCineCameraComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "CameraSettings Mobility Rendering LOD Trigger PhysicsVolume" },
		{ "HideFunctions", "SetFieldOfView SetAspectRatio" },
		{ "IncludePath", "VirtualCameraCineCameraComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ApertureOptions_Inner = { "ApertureOptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ApertureOptions_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "Comment", "/** List of preset aperture options, aperture will always be one of these values */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "List of preset aperture options, aperture will always be one of these values" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ApertureOptions = { "ApertureOptions", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, ApertureOptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ApertureOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ApertureOptions_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FocalLengthOptions_Inner = { "FocalLengthOptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FocalLengthOptions_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "Comment", "/** List of preset focal length options, focal length will be one of these values, unless manually zooming */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "List of preset focal length options, focal length will be one of these values, unless manually zooming" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FocalLengthOptions = { "FocalLengthOptions", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, FocalLengthOptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FocalLengthOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FocalLengthOptions_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOptions_Inner = { "MatteOptions", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOptions_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "Comment", "/** List of preset matte options to chose from, UI options will only pull from this, unless a filmback option with a custom matte is selected */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "List of preset matte options to chose from, UI options will only pull from this, unless a filmback option with a custom matte is selected" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOptions = { "MatteOptions", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, MatteOptions), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOptions_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions_ValueProp = { "FilmbackOptions", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FCameraFilmbackSettings, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions_Key_KeyProp = { "FilmbackOptions_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "Comment", "/** List of preset filmback options, filmback will always be one of these values */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "List of preset filmback options, filmback will always be one of these values" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions = { "FilmbackOptions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, FilmbackOptions), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_DesiredFilmbackSettings_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "Comment", "/** The desired filmback settings to be shown in the viewport within Virtual Camera UI window */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "The desired filmback settings to be shown in the viewport within Virtual Camera UI window" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_DesiredFilmbackSettings = { "DesiredFilmbackSettings", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, DesiredFilmbackSettings), Z_Construct_UScriptStruct_FCameraFilmbackSettings, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_DesiredFilmbackSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_DesiredFilmbackSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteAspectRatio_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "Comment", "/** The filmback settings to be used for additional letterboxing if desired */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "The filmback settings to be used for additional letterboxing if desired" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteAspectRatio = { "MatteAspectRatio", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, MatteAspectRatio), METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteAspectRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteAspectRatio_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOpacity_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** The opacity of the matte in the camera view */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "The opacity of the matte in the camera view" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOpacity = { "MatteOpacity", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, MatteOpacity), METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOpacity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOpacity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ViewSizeRatio_MetaData[] = {
		{ "Category", "Current Camera Settings" },
		{ "Comment", "/** The X and Y ratios of Desired View Size to Actual View Size (calculated as Desired/ Actual) */" },
		{ "ModuleRelativePath", "Public/VirtualCameraCineCameraComponent.h" },
		{ "ToolTip", "The X and Y ratios of Desired View Size to Actual View Size (calculated as Desired/ Actual)" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ViewSizeRatio = { "ViewSizeRatio", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraCineCameraComponent, ViewSizeRatio), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ViewSizeRatio_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ViewSizeRatio_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ApertureOptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ApertureOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FocalLengthOptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FocalLengthOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOptions_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_FilmbackOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_DesiredFilmbackSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteAspectRatio,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_MatteOpacity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::NewProp_ViewSizeRatio,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVirtualCameraCineCameraComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::ClassParams = {
		&UVirtualCameraCineCameraComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVirtualCameraCineCameraComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVirtualCameraCineCameraComponent, 776561896);
	template<> VIRTUALCAMERA_API UClass* StaticClass<UVirtualCameraCineCameraComponent>()
	{
		return UVirtualCameraCineCameraComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVirtualCameraCineCameraComponent(Z_Construct_UClass_UVirtualCameraCineCameraComponent, &UVirtualCameraCineCameraComponent::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("UVirtualCameraCineCameraComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVirtualCameraCineCameraComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
