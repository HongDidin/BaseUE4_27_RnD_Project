// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_VCamOutputRemoteSession_generated_h
#error "VCamOutputRemoteSession.generated.h already included, missing '#pragma once' in VCamOutputRemoteSession.h"
#endif
#define VCAMCORE_VCamOutputRemoteSession_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamOutputRemoteSession(); \
	friend struct Z_Construct_UClass_UVCamOutputRemoteSession_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputRemoteSession, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputRemoteSession)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUVCamOutputRemoteSession(); \
	friend struct Z_Construct_UClass_UVCamOutputRemoteSession_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputRemoteSession, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputRemoteSession)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputRemoteSession(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamOutputRemoteSession) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputRemoteSession); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputRemoteSession); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputRemoteSession(UVCamOutputRemoteSession&&); \
	NO_API UVCamOutputRemoteSession(const UVCamOutputRemoteSession&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputRemoteSession() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputRemoteSession(UVCamOutputRemoteSession&&); \
	NO_API UVCamOutputRemoteSession(const UVCamOutputRemoteSession&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputRemoteSession); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputRemoteSession); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVCamOutputRemoteSession)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaOutput() { return STRUCT_OFFSET(UVCamOutputRemoteSession, MediaOutput); } \
	FORCEINLINE static uint32 __PPO__MediaCapture() { return STRUCT_OFFSET(UVCamOutputRemoteSession, MediaCapture); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_18_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamOutputRemoteSession>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputRemoteSession_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
