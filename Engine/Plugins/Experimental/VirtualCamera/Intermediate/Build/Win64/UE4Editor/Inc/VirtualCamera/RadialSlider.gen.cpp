// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/Components/RadialSlider.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRadialSlider() {}
// Cross Module References
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_URadialSlider_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_URadialSlider();
	UMG_API UClass* Z_Construct_UClass_UWidget();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FRuntimeFloatCurve();
	UMG_API UFunction* Z_Construct_UDelegateFunction_UWidget_GetFloat__DelegateSignature();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSliderStyle();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics
	{
		struct _Script_VirtualCamera_eventOnFloatValueChangedEvent_Parms
		{
			float Value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_VirtualCamera_eventOnFloatValueChangedEvent_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnFloatValueChangedEvent__DelegateSignature", nullptr, nullptr, sizeof(_Script_VirtualCamera_eventOnFloatValueChangedEvent_Parms), Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnControllerCaptureEndEvent__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnControllerCaptureBeginEvent__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnMouseCaptureEndEvent__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnMouseCaptureBeginEvent__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(URadialSlider::execSetSliderHandleColor)
	{
		P_GET_STRUCT(FLinearColor,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSliderHandleColor(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetSliderProgressColor)
	{
		P_GET_STRUCT(FLinearColor,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSliderProgressColor(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetSliderBarColor)
	{
		P_GET_STRUCT(FLinearColor,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSliderBarColor(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetStepSize)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStepSize(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetLocked)
	{
		P_GET_UBOOL(Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetLocked(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetAngularOffset)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetAngularOffset(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetSliderHandleEndAngle)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSliderHandleEndAngle(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetSliderHandleStartAngle)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSliderHandleStartAngle(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetValueTags)
	{
		P_GET_TARRAY_REF(float,Z_Param_Out_InValueTags);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetValueTags(Z_Param_Out_InValueTags);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetSliderRange)
	{
		P_GET_STRUCT_REF(FRuntimeFloatCurve,Z_Param_Out_InSliderRange);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSliderRange(Z_Param_Out_InSliderRange);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetCustomDefaultValue)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetCustomDefaultValue(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execSetValue)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetValue(Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execGetNormalizedSliderHandlePosition)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetNormalizedSliderHandlePosition();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execGetCustomDefaultValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetCustomDefaultValue();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(URadialSlider::execGetValue)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetValue();
		P_NATIVE_END;
	}
	void URadialSlider::StaticRegisterNativesURadialSlider()
	{
		UClass* Class = URadialSlider::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCustomDefaultValue", &URadialSlider::execGetCustomDefaultValue },
			{ "GetNormalizedSliderHandlePosition", &URadialSlider::execGetNormalizedSliderHandlePosition },
			{ "GetValue", &URadialSlider::execGetValue },
			{ "SetAngularOffset", &URadialSlider::execSetAngularOffset },
			{ "SetCustomDefaultValue", &URadialSlider::execSetCustomDefaultValue },
			{ "SetLocked", &URadialSlider::execSetLocked },
			{ "SetSliderBarColor", &URadialSlider::execSetSliderBarColor },
			{ "SetSliderHandleColor", &URadialSlider::execSetSliderHandleColor },
			{ "SetSliderHandleEndAngle", &URadialSlider::execSetSliderHandleEndAngle },
			{ "SetSliderHandleStartAngle", &URadialSlider::execSetSliderHandleStartAngle },
			{ "SetSliderProgressColor", &URadialSlider::execSetSliderProgressColor },
			{ "SetSliderRange", &URadialSlider::execSetSliderRange },
			{ "SetStepSize", &URadialSlider::execSetStepSize },
			{ "SetValue", &URadialSlider::execSetValue },
			{ "SetValueTags", &URadialSlider::execSetValueTags },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics
	{
		struct RadialSlider_eventGetCustomDefaultValue_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventGetCustomDefaultValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Gets the current custom default value of the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Gets the current custom default value of the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "GetCustomDefaultValue", nullptr, nullptr, sizeof(RadialSlider_eventGetCustomDefaultValue_Parms), Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics
	{
		struct RadialSlider_eventGetNormalizedSliderHandlePosition_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventGetNormalizedSliderHandlePosition_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Get the current raw slider alpha from 0 to 1 */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Get the current raw slider alpha from 0 to 1" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "GetNormalizedSliderHandlePosition", nullptr, nullptr, sizeof(RadialSlider_eventGetNormalizedSliderHandlePosition_Parms), Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_GetValue_Statics
	{
		struct RadialSlider_eventGetValue_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_GetValue_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventGetValue_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_GetValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_GetValue_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_GetValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Gets the current value of the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Gets the current value of the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_GetValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "GetValue", nullptr, nullptr, sizeof(RadialSlider_eventGetValue_Parms), Z_Construct_UFunction_URadialSlider_GetValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_GetValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_GetValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_GetValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_GetValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_GetValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics
	{
		struct RadialSlider_eventSetAngularOffset_Parms
		{
			float InValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetAngularOffset_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behaviour" },
		{ "Comment", "/** Sets the Angular Offset for the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the Angular Offset for the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetAngularOffset", nullptr, nullptr, sizeof(RadialSlider_eventSetAngularOffset_Parms), Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetAngularOffset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetAngularOffset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics
	{
		struct RadialSlider_eventSetCustomDefaultValue_Parms
		{
			float InValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetCustomDefaultValue_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Sets the current custom default value of the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the current custom default value of the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetCustomDefaultValue", nullptr, nullptr, sizeof(RadialSlider_eventSetCustomDefaultValue_Parms), Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetLocked_Statics
	{
		struct RadialSlider_eventSetLocked_Parms
		{
			bool InValue;
		};
		static void NewProp_InValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_URadialSlider_SetLocked_Statics::NewProp_InValue_SetBit(void* Obj)
	{
		((RadialSlider_eventSetLocked_Parms*)Obj)->InValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_URadialSlider_SetLocked_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(RadialSlider_eventSetLocked_Parms), &Z_Construct_UFunction_URadialSlider_SetLocked_Statics::NewProp_InValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetLocked_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetLocked_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetLocked_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Sets the handle to be interactive or fixed */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the handle to be interactive or fixed" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetLocked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetLocked", nullptr, nullptr, sizeof(RadialSlider_eventSetLocked_Parms), Z_Construct_UFunction_URadialSlider_SetLocked_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetLocked_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetLocked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetLocked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetLocked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetLocked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics
	{
		struct RadialSlider_eventSetSliderBarColor_Parms
		{
			FLinearColor InValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetSliderBarColor_Parms, InValue), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Sets the color of the slider bar */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the color of the slider bar" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetSliderBarColor", nullptr, nullptr, sizeof(RadialSlider_eventSetSliderBarColor_Parms), Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetSliderBarColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetSliderBarColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics
	{
		struct RadialSlider_eventSetSliderHandleColor_Parms
		{
			FLinearColor InValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetSliderHandleColor_Parms, InValue), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Sets the color of the handle bar */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the color of the handle bar" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetSliderHandleColor", nullptr, nullptr, sizeof(RadialSlider_eventSetSliderHandleColor_Parms), Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetSliderHandleColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetSliderHandleColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics
	{
		struct RadialSlider_eventSetSliderHandleEndAngle_Parms
		{
			float InValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetSliderHandleEndAngle_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Sets the maximum angle of the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the maximum angle of the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetSliderHandleEndAngle", nullptr, nullptr, sizeof(RadialSlider_eventSetSliderHandleEndAngle_Parms), Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics
	{
		struct RadialSlider_eventSetSliderHandleStartAngle_Parms
		{
			float InValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetSliderHandleStartAngle_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Sets the minimum angle of the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the minimum angle of the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetSliderHandleStartAngle", nullptr, nullptr, sizeof(RadialSlider_eventSetSliderHandleStartAngle_Parms), Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics
	{
		struct RadialSlider_eventSetSliderProgressColor_Parms
		{
			FLinearColor InValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetSliderProgressColor_Parms, InValue), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::Function_MetaDataParams[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Sets the progress color of the slider bar */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the progress color of the slider bar" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetSliderProgressColor", nullptr, nullptr, sizeof(RadialSlider_eventSetSliderProgressColor_Parms), Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetSliderProgressColor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetSliderProgressColor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics
	{
		struct RadialSlider_eventSetSliderRange_Parms
		{
			FRuntimeFloatCurve InSliderRange;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSliderRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InSliderRange;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::NewProp_InSliderRange_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::NewProp_InSliderRange = { "InSliderRange", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetSliderRange_Parms, InSliderRange), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::NewProp_InSliderRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::NewProp_InSliderRange_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::NewProp_InSliderRange,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behaviour" },
		{ "Comment", "/** Sets the curve for the slider range*/" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the curve for the slider range" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetSliderRange", nullptr, nullptr, sizeof(RadialSlider_eventSetSliderRange_Parms), Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetSliderRange()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetSliderRange_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetStepSize_Statics
	{
		struct RadialSlider_eventSetStepSize_Parms
		{
			float InValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetStepSize_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Sets the amount to adjust the value by, when using a controller or keyboard */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the amount to adjust the value by, when using a controller or keyboard" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetStepSize", nullptr, nullptr, sizeof(RadialSlider_eventSetStepSize_Parms), Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetStepSize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetStepSize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetValue_Statics
	{
		struct RadialSlider_eventSetValue_Parms
		{
			float InValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_SetValue_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetValue_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetValue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetValue_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetValue_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behavior" },
		{ "Comment", "/** Sets the current value of the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets the current value of the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetValue_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetValue", nullptr, nullptr, sizeof(RadialSlider_eventSetValue_Parms), Z_Construct_UFunction_URadialSlider_SetValue_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetValue_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetValue_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetValue_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetValue()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetValue_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_URadialSlider_SetValueTags_Statics
	{
		struct RadialSlider_eventSetValueTags_Parms
		{
			TArray<float> InValueTags;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InValueTags_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InValueTags_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InValueTags;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::NewProp_InValueTags_Inner = { "InValueTags", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::NewProp_InValueTags_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::NewProp_InValueTags = { "InValueTags", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(RadialSlider_eventSetValueTags_Parms, InValueTags), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::NewProp_InValueTags_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::NewProp_InValueTags_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::NewProp_InValueTags_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::NewProp_InValueTags,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::Function_MetaDataParams[] = {
		{ "Category", "Behaviour" },
		{ "Comment", "/** Adds value tags to the slider. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Adds value tags to the slider." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_URadialSlider, nullptr, "SetValueTags", nullptr, nullptr, sizeof(RadialSlider_eventSetValueTags_Parms), Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_URadialSlider_SetValueTags()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_URadialSlider_SetValueTags_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_URadialSlider_NoRegister()
	{
		return URadialSlider::StaticClass();
	}
	struct Z_Construct_UClass_URadialSlider_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Value_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_ValueDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUseCustomDefaultValue_MetaData[];
#endif
		static void NewProp_bUseCustomDefaultValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUseCustomDefaultValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomDefaultValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CustomDefaultValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliderRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SliderRange;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ValueTags_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ValueTags_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ValueTags;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliderHandleStartAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SliderHandleStartAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliderHandleEndAngle_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SliderHandleEndAngle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AngularOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AngularOffset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WidgetStyle_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_WidgetStyle;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliderBarColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SliderBarColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliderProgressColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SliderProgressColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SliderHandleColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SliderHandleColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Locked_MetaData[];
#endif
		static void NewProp_Locked_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_Locked;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MouseUsesStep_MetaData[];
#endif
		static void NewProp_MouseUsesStep_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_MouseUsesStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RequiresControllerLock_MetaData[];
#endif
		static void NewProp_RequiresControllerLock_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_RequiresControllerLock;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StepSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StepSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IsFocusable_MetaData[];
#endif
		static void NewProp_IsFocusable_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_IsFocusable;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnMouseCaptureBegin_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMouseCaptureBegin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnMouseCaptureEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMouseCaptureEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnControllerCaptureBegin_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnControllerCaptureBegin;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnControllerCaptureEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnControllerCaptureEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnValueChanged_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnValueChanged;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URadialSlider_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_URadialSlider_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_URadialSlider_GetCustomDefaultValue, "GetCustomDefaultValue" }, // 617161462
		{ &Z_Construct_UFunction_URadialSlider_GetNormalizedSliderHandlePosition, "GetNormalizedSliderHandlePosition" }, // 2422823895
		{ &Z_Construct_UFunction_URadialSlider_GetValue, "GetValue" }, // 2558253905
		{ &Z_Construct_UFunction_URadialSlider_SetAngularOffset, "SetAngularOffset" }, // 2545388109
		{ &Z_Construct_UFunction_URadialSlider_SetCustomDefaultValue, "SetCustomDefaultValue" }, // 1725542673
		{ &Z_Construct_UFunction_URadialSlider_SetLocked, "SetLocked" }, // 1001788047
		{ &Z_Construct_UFunction_URadialSlider_SetSliderBarColor, "SetSliderBarColor" }, // 2192655544
		{ &Z_Construct_UFunction_URadialSlider_SetSliderHandleColor, "SetSliderHandleColor" }, // 1062903962
		{ &Z_Construct_UFunction_URadialSlider_SetSliderHandleEndAngle, "SetSliderHandleEndAngle" }, // 4242568252
		{ &Z_Construct_UFunction_URadialSlider_SetSliderHandleStartAngle, "SetSliderHandleStartAngle" }, // 1078004618
		{ &Z_Construct_UFunction_URadialSlider_SetSliderProgressColor, "SetSliderProgressColor" }, // 2243793628
		{ &Z_Construct_UFunction_URadialSlider_SetSliderRange, "SetSliderRange" }, // 2169366317
		{ &Z_Construct_UFunction_URadialSlider_SetStepSize, "SetStepSize" }, // 3987181314
		{ &Z_Construct_UFunction_URadialSlider_SetValue, "SetValue" }, // 3963196619
		{ &Z_Construct_UFunction_URadialSlider_SetValueTags, "SetValueTags" }, // 1903462615
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A simple widget that shows a sliding bar with a handle that allows you to control the value between 0..1.\n *\n * * No Children\n */" },
		{ "IncludePath", "Components/RadialSlider.h" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "A simple widget that shows a sliding bar with a handle that allows you to control the value between 0..1.\n\n* No Children" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_Value_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** The slider value to display. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The slider value to display." },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, Value), METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_Value_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_Value_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueDelegate_MetaData[] = {
		{ "Comment", "/** A bindable delegate to allow logic to drive the value of the widget */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "A bindable delegate to allow logic to drive the value of the widget" },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueDelegate = { "ValueDelegate", nullptr, (EPropertyFlags)0x0010000000080000, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, ValueDelegate), Z_Construct_UDelegateFunction_UWidget_GetFloat__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_bUseCustomDefaultValue_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Whether the slider should draw it's progress bar from a custom value on the slider */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Whether the slider should draw it's progress bar from a custom value on the slider" },
	};
#endif
	void Z_Construct_UClass_URadialSlider_Statics::NewProp_bUseCustomDefaultValue_SetBit(void* Obj)
	{
		((URadialSlider*)Obj)->bUseCustomDefaultValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_bUseCustomDefaultValue = { "bUseCustomDefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URadialSlider), &Z_Construct_UClass_URadialSlider_Statics::NewProp_bUseCustomDefaultValue_SetBit, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_bUseCustomDefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_bUseCustomDefaultValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_CustomDefaultValue_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/**  The value where the slider should draw it's progress bar from, independent of direction */" },
		{ "EditCondition", "bUseCustomDefaultValue" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The value where the slider should draw it's progress bar from, independent of direction" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_CustomDefaultValue = { "CustomDefaultValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, CustomDefaultValue), METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_CustomDefaultValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_CustomDefaultValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderRange_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** A curve that defines how the slider should be sampled. Default is linear. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "A curve that defines how the slider should be sampled. Default is linear." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderRange = { "SliderRange", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, SliderRange), Z_Construct_UScriptStruct_FRuntimeFloatCurve, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderRange_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueTags_Inner = { "ValueTags", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueTags_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Adds text tags to the radial slider at the value's position. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Adds text tags to the radial slider at the value's position." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueTags = { "ValueTags", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, ValueTags), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueTags_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueTags_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleStartAngle_MetaData[] = {
		{ "Category", "Appearance" },
		{ "ClampMax", "360" },
		{ "ClampMin", "0" },
		{ "Comment", "/** The angle at which the Slider Handle will start. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The angle at which the Slider Handle will start." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleStartAngle = { "SliderHandleStartAngle", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, SliderHandleStartAngle), METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleStartAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleStartAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleEndAngle_MetaData[] = {
		{ "Category", "Appearance" },
		{ "ClampMax", "360" },
		{ "ClampMin", "0" },
		{ "Comment", "/** The angle at which the Slider Handle will end. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The angle at which the Slider Handle will end." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleEndAngle = { "SliderHandleEndAngle", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, SliderHandleEndAngle), METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleEndAngle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleEndAngle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_AngularOffset_MetaData[] = {
		{ "Category", "Appearance" },
		{ "ClampMax", "360" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Rotates radial slider by arbitrary offset to support full gamut of configurations. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Rotates radial slider by arbitrary offset to support full gamut of configurations." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_AngularOffset = { "AngularOffset", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, AngularOffset), METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_AngularOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_AngularOffset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_WidgetStyle_MetaData[] = {
		{ "Category", "Style" },
		{ "Comment", "/** The progress bar style */" },
		{ "DisplayName", "Style" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The progress bar style" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_WidgetStyle = { "WidgetStyle", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, WidgetStyle), Z_Construct_UScriptStruct_FSliderStyle, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_WidgetStyle_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_WidgetStyle_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderBarColor_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** The color to draw the slider bar in. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The color to draw the slider bar in." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderBarColor = { "SliderBarColor", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, SliderBarColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderBarColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderBarColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderProgressColor_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** The color to draw the completed progress of the slider bar in. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The color to draw the completed progress of the slider bar in." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderProgressColor = { "SliderProgressColor", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, SliderProgressColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderProgressColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderProgressColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleColor_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** The color to draw the slider handle in. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The color to draw the slider handle in." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleColor = { "SliderHandleColor", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, SliderHandleColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_Locked_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Whether the handle is interactive or fixed. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Whether the handle is interactive or fixed." },
	};
#endif
	void Z_Construct_UClass_URadialSlider_Statics::NewProp_Locked_SetBit(void* Obj)
	{
		((URadialSlider*)Obj)->Locked = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_Locked = { "Locked", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URadialSlider), &Z_Construct_UClass_URadialSlider_Statics::NewProp_Locked_SetBit, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_Locked_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_Locked_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_MouseUsesStep_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Sets new value if mouse position is greater/less than half the step size. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets new value if mouse position is greater/less than half the step size." },
	};
#endif
	void Z_Construct_UClass_URadialSlider_Statics::NewProp_MouseUsesStep_SetBit(void* Obj)
	{
		((URadialSlider*)Obj)->MouseUsesStep = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_MouseUsesStep = { "MouseUsesStep", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URadialSlider), &Z_Construct_UClass_URadialSlider_Statics::NewProp_MouseUsesStep_SetBit, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_MouseUsesStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_MouseUsesStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_RequiresControllerLock_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** Sets whether we have to lock input to change the slider value. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Sets whether we have to lock input to change the slider value." },
	};
#endif
	void Z_Construct_UClass_URadialSlider_Statics::NewProp_RequiresControllerLock_SetBit(void* Obj)
	{
		((URadialSlider*)Obj)->RequiresControllerLock = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_RequiresControllerLock = { "RequiresControllerLock", nullptr, (EPropertyFlags)0x0010040000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URadialSlider), &Z_Construct_UClass_URadialSlider_Statics::NewProp_RequiresControllerLock_SetBit, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_RequiresControllerLock_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_RequiresControllerLock_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_StepSize_MetaData[] = {
		{ "Category", "Appearance" },
		{ "Comment", "/** The amount to adjust the value by, when using a controller or keyboard */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "The amount to adjust the value by, when using a controller or keyboard" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_StepSize = { "StepSize", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, StepSize), METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_StepSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_StepSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_IsFocusable_MetaData[] = {
		{ "Category", "Interaction" },
		{ "Comment", "/** Should the slider be focusable? */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Should the slider be focusable?" },
	};
#endif
	void Z_Construct_UClass_URadialSlider_Statics::NewProp_IsFocusable_SetBit(void* Obj)
	{
		((URadialSlider*)Obj)->IsFocusable = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_IsFocusable = { "IsFocusable", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URadialSlider), &Z_Construct_UClass_URadialSlider_Statics::NewProp_IsFocusable_SetBit, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_IsFocusable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_IsFocusable_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureBegin_MetaData[] = {
		{ "Category", "Widget Event" },
		{ "Comment", "/** Invoked when the mouse is pressed and a capture begins. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Invoked when the mouse is pressed and a capture begins." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureBegin = { "OnMouseCaptureBegin", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, OnMouseCaptureBegin), Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureBeginEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureBegin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureBegin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureEnd_MetaData[] = {
		{ "Category", "Widget Event" },
		{ "Comment", "/** Invoked when the mouse is released and a capture ends. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Invoked when the mouse is released and a capture ends." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureEnd = { "OnMouseCaptureEnd", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, OnMouseCaptureEnd), Z_Construct_UDelegateFunction_VirtualCamera_OnMouseCaptureEndEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureBegin_MetaData[] = {
		{ "Category", "Widget Event" },
		{ "Comment", "/** Invoked when the controller capture begins. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Invoked when the controller capture begins." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureBegin = { "OnControllerCaptureBegin", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, OnControllerCaptureBegin), Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureBeginEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureBegin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureBegin_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureEnd_MetaData[] = {
		{ "Category", "Widget Event" },
		{ "Comment", "/** Invoked when the controller capture ends. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Invoked when the controller capture ends." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureEnd = { "OnControllerCaptureEnd", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, OnControllerCaptureEnd), Z_Construct_UDelegateFunction_VirtualCamera_OnControllerCaptureEndEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URadialSlider_Statics::NewProp_OnValueChanged_MetaData[] = {
		{ "Category", "Widget Event" },
		{ "Comment", "/** Called when the value is changed by slider or typing. */" },
		{ "ModuleRelativePath", "Public/Components/RadialSlider.h" },
		{ "ToolTip", "Called when the value is changed by slider or typing." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_URadialSlider_Statics::NewProp_OnValueChanged = { "OnValueChanged", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URadialSlider, OnValueChanged), Z_Construct_UDelegateFunction_VirtualCamera_OnFloatValueChangedEvent__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnValueChanged_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::NewProp_OnValueChanged_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URadialSlider_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_Value,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_bUseCustomDefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_CustomDefaultValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueTags_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_ValueTags,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleStartAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleEndAngle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_AngularOffset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_WidgetStyle,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderBarColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderProgressColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_SliderHandleColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_Locked,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_MouseUsesStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_RequiresControllerLock,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_StepSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_IsFocusable,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureBegin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_OnMouseCaptureEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureBegin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_OnControllerCaptureEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URadialSlider_Statics::NewProp_OnValueChanged,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URadialSlider_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URadialSlider>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URadialSlider_Statics::ClassParams = {
		&URadialSlider::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_URadialSlider_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::PropPointers),
		0,
		0x00B000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URadialSlider_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URadialSlider_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URadialSlider()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URadialSlider_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URadialSlider, 353931093);
	template<> VIRTUALCAMERA_API UClass* StaticClass<URadialSlider>()
	{
		return URadialSlider::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URadialSlider(Z_Construct_UClass_URadialSlider, &URadialSlider::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("URadialSlider"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URadialSlider);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
