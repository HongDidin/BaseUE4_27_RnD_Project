// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FTimecode;
struct FFrameRate;
class UCineCameraComponent;
struct FMovieSceneObjectBindingID;
class UObject;
struct FVector2D;
class ULevelSequence;
class USceneCaptureComponent2D;
class AActor;
struct FAssetData;
 
class UVirtualCameraClipsMetaData;
class UTexture;
class UVirtualCameraUserSettings;
#ifdef VIRTUALCAMERA_VCamBlueprintFunctionLibrary_generated_h
#error "VCamBlueprintFunctionLibrary.generated.h already included, missing '#pragma once' in VCamBlueprintFunctionLibrary.h"
#endif
#define VIRTUALCAMERA_VCamBlueprintFunctionLibrary_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetNextUndoDescription); \
	DECLARE_FUNCTION(execTimecodeToFrameAmount); \
	DECLARE_FUNCTION(execEnableDebugFocusPlane); \
	DECLARE_FUNCTION(execGetBoundObjects); \
	DECLARE_FUNCTION(execCalculateAutoFocusDistance); \
	DECLARE_FUNCTION(execEditorSetGameView); \
	DECLARE_FUNCTION(execCallFunctionByName); \
	DECLARE_FUNCTION(execConvertStringToFrameRate); \
	DECLARE_FUNCTION(execGetDisplayRate); \
	DECLARE_FUNCTION(execUpdatePostProcessSettingsForCapture); \
	DECLARE_FUNCTION(execPilotActor); \
	DECLARE_FUNCTION(execSortAssetsByTimecodeAssetData); \
	DECLARE_FUNCTION(execGetObjectMetadataTags); \
	DECLARE_FUNCTION(execModifyObjectMetadataTags); \
	DECLARE_FUNCTION(execEditorLoadAsset); \
	DECLARE_FUNCTION(execEditorSaveAsset); \
	DECLARE_FUNCTION(execModifyLevelSequenceMetadata); \
	DECLARE_FUNCTION(execModifyLevelSequenceMetadataForSelects); \
	DECLARE_FUNCTION(execImportSnapshotTexture); \
	DECLARE_FUNCTION(execIsCurrentLevelSequencePlaying); \
	DECLARE_FUNCTION(execGetLevelSequenceFrameAsTimecodeWithoutObject); \
	DECLARE_FUNCTION(execGetLevelSequenceFrameAsTimecode); \
	DECLARE_FUNCTION(execGetLevelSequenceLengthInFrames); \
	DECLARE_FUNCTION(execGetCurrentLevelSequenceCurrentFrame); \
	DECLARE_FUNCTION(execSetCurrentLevelSequenceCurrentFrame); \
	DECLARE_FUNCTION(execPauseCurrentLevelSequence); \
	DECLARE_FUNCTION(execPlayCurrentLevelSequence); \
	DECLARE_FUNCTION(execGetCurrentLevelSequence); \
	DECLARE_FUNCTION(execGetUserSettings); \
	DECLARE_FUNCTION(execIsGameRunning);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetNextUndoDescription); \
	DECLARE_FUNCTION(execTimecodeToFrameAmount); \
	DECLARE_FUNCTION(execEnableDebugFocusPlane); \
	DECLARE_FUNCTION(execGetBoundObjects); \
	DECLARE_FUNCTION(execCalculateAutoFocusDistance); \
	DECLARE_FUNCTION(execEditorSetGameView); \
	DECLARE_FUNCTION(execCallFunctionByName); \
	DECLARE_FUNCTION(execConvertStringToFrameRate); \
	DECLARE_FUNCTION(execGetDisplayRate); \
	DECLARE_FUNCTION(execUpdatePostProcessSettingsForCapture); \
	DECLARE_FUNCTION(execPilotActor); \
	DECLARE_FUNCTION(execSortAssetsByTimecodeAssetData); \
	DECLARE_FUNCTION(execGetObjectMetadataTags); \
	DECLARE_FUNCTION(execModifyObjectMetadataTags); \
	DECLARE_FUNCTION(execEditorLoadAsset); \
	DECLARE_FUNCTION(execEditorSaveAsset); \
	DECLARE_FUNCTION(execModifyLevelSequenceMetadata); \
	DECLARE_FUNCTION(execModifyLevelSequenceMetadataForSelects); \
	DECLARE_FUNCTION(execImportSnapshotTexture); \
	DECLARE_FUNCTION(execIsCurrentLevelSequencePlaying); \
	DECLARE_FUNCTION(execGetLevelSequenceFrameAsTimecodeWithoutObject); \
	DECLARE_FUNCTION(execGetLevelSequenceFrameAsTimecode); \
	DECLARE_FUNCTION(execGetLevelSequenceLengthInFrames); \
	DECLARE_FUNCTION(execGetCurrentLevelSequenceCurrentFrame); \
	DECLARE_FUNCTION(execSetCurrentLevelSequenceCurrentFrame); \
	DECLARE_FUNCTION(execPauseCurrentLevelSequence); \
	DECLARE_FUNCTION(execPlayCurrentLevelSequence); \
	DECLARE_FUNCTION(execGetCurrentLevelSequence); \
	DECLARE_FUNCTION(execGetUserSettings); \
	DECLARE_FUNCTION(execIsGameRunning);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamBlueprintFunctionLibrary(); \
	friend struct Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UVCamBlueprintFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVCamBlueprintFunctionLibrary) \
	static const TCHAR* StaticConfigName() {return TEXT("VirtualCamera");} \



#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUVCamBlueprintFunctionLibrary(); \
	friend struct Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(UVCamBlueprintFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVCamBlueprintFunctionLibrary) \
	static const TCHAR* StaticConfigName() {return TEXT("VirtualCamera");} \



#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamBlueprintFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamBlueprintFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamBlueprintFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamBlueprintFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamBlueprintFunctionLibrary(UVCamBlueprintFunctionLibrary&&); \
	NO_API UVCamBlueprintFunctionLibrary(const UVCamBlueprintFunctionLibrary&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamBlueprintFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamBlueprintFunctionLibrary(UVCamBlueprintFunctionLibrary&&); \
	NO_API UVCamBlueprintFunctionLibrary(const UVCamBlueprintFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamBlueprintFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamBlueprintFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamBlueprintFunctionLibrary)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_18_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class UVCamBlueprintFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VCamBlueprintFunctionLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
