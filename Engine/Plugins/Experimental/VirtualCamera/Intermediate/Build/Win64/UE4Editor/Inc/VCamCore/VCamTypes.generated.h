// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_VCamTypes_generated_h
#error "VCamTypes.generated.h already included, missing '#pragma once' in VCamTypes.h"
#endif
#define VCAMCORE_VCamTypes_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamTypes_h_19_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FModifierStackEntry_Statics; \
	VCAMCORE_API static class UScriptStruct* StaticStruct();


template<> VCAMCORE_API UScriptStruct* StaticStruct<struct FModifierStackEntry>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
