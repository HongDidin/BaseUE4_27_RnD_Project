// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FHitResult;
struct FVirtualCameraTransform;
class AActor;
struct FVector;
class IBlendableInterface;
struct FTransform;
struct FLiveLinkSubjectRepresentation;
class IVirtualCameraOptions;
class IVirtualCameraPresetContainer;
class ULevelSequencePlaybackController;
class USceneCaptureComponent2D;
class UCineCameraComponent;
#ifdef VIRTUALCAMERA_IVirtualCameraController_generated_h
#error "IVirtualCameraController.generated.h already included, missing '#pragma once' in IVirtualCameraController.h"
#endif
#define VIRTUALCAMERA_IVirtualCameraController_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_65_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVirtualCameraTransform_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FVirtualCameraTransform>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_42_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FTrackingOffset_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FTrackingOffset>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_74_DELEGATE \
struct _Script_VirtualCamera_eventVirtualCameraTickDelegate_Parms \
{ \
	float DeltaTime; \
}; \
static inline void FVirtualCameraTickDelegate_DelegateWrapper(const FScriptDelegate& VirtualCameraTickDelegate, float DeltaTime) \
{ \
	_Script_VirtualCamera_eventVirtualCameraTickDelegate_Parms Parms; \
	Parms.DeltaTime=DeltaTime; \
	VirtualCameraTickDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_73_DELEGATE \
struct _Script_VirtualCamera_eventVirtualCameraTickDelegateGroup_Parms \
{ \
	float DeltaTime; \
}; \
static inline void FVirtualCameraTickDelegateGroup_DelegateWrapper(const FMulticastScriptDelegate& VirtualCameraTickDelegateGroup, float DeltaTime) \
{ \
	_Script_VirtualCamera_eventVirtualCameraTickDelegateGroup_Parms Parms; \
	Parms.DeltaTime=DeltaTime; \
	VirtualCameraTickDelegateGroup.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_72_DELEGATE \
struct _Script_VirtualCamera_eventOnActorClickedDelegate_Parms \
{ \
	FHitResult HitResult; \
}; \
static inline void FOnActorClickedDelegate_DelegateWrapper(const FScriptDelegate& OnActorClickedDelegate, FHitResult HitResult) \
{ \
	_Script_VirtualCamera_eventOnActorClickedDelegate_Parms Parms; \
	Parms.HitResult=HitResult; \
	OnActorClickedDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_71_DELEGATE \
struct _Script_VirtualCamera_eventPreSetVirtualCameraTransform_Parms \
{ \
	FVirtualCameraTransform CameraTransform; \
	FVirtualCameraTransform ReturnValue; \
}; \
static inline FVirtualCameraTransform FPreSetVirtualCameraTransform_DelegateWrapper(const FScriptDelegate& PreSetVirtualCameraTransform, FVirtualCameraTransform CameraTransform) \
{ \
	_Script_VirtualCamera_eventPreSetVirtualCameraTransform_Parms Parms; \
	Parms.CameraTransform=CameraTransform; \
	PreSetVirtualCameraTransform.ProcessDelegate<UObject>(&Parms); \
	return Parms.ReturnValue; \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_RPC_WRAPPERS \
	virtual void RemoveOnVirtualCameraUpdatedDelegate_Implementation(FVirtualCameraTickDelegate const& InDelegate) {}; \
	virtual void AddOnVirtualCameraUpdatedDelegate_Implementation(FVirtualCameraTickDelegate const& InDelegate) {}; \
	virtual void SetOnActorClickedDelegate_Implementation(FOnActorClickedDelegate const& InDelegate) {}; \
	virtual void SetBeforeSetVirtualCameraTransformDelegate_Implementation(FPreSetVirtualCameraTransform const& InDelegate) {}; \
	virtual void SetFocusVisualization_Implementation(bool bInShowFocusVisualization) {}; \
	virtual void SetTrackedActorForFocus_Implementation(AActor* InActorToTrack, FVector const& TrackingPointOffset) {}; \
	virtual void AddBlendableToCamera_Implementation(TScriptInterface<IBlendableInterface> const& InBlendableToAdd, float InWeight) {}; \
	virtual FTransform GetRelativeTransform_Implementation() const { return FTransform(); }; \
	virtual void SetSaveSettingsOnStopStreaming_Implementation(bool bShouldSettingsSave) {}; \
	virtual bool ShouldSaveSettingsOnStopStreaming_Implementation() const { return false; }; \
	virtual bool IsStreaming_Implementation() const { return false; }; \
	virtual void SetLiveLinkRepresentation_Implementation(FLiveLinkSubjectRepresentation const& InLiveLinkRepresenation) {}; \
	virtual FLiveLinkSubjectRepresentation GetLiveLinkRepresentation_Implementation() const { return FLiveLinkSubjectRepresentation(); }; \
	virtual TScriptInterface<IVirtualCameraOptions> GetOptions_Implementation() { return NULL; }; \
	virtual TScriptInterface<IVirtualCameraPresetContainer> GetPresetContainer_Implementation() { return NULL; }; \
	virtual ULevelSequencePlaybackController* GetSequenceController_Implementation() const { return NULL; }; \
	virtual USceneCaptureComponent2D* GetSceneCaptureComponent_Implementation() const { return NULL; }; \
	virtual UCineCameraComponent* GetStreamedCameraComponent_Implementation() const { return NULL; }; \
 \
	DECLARE_FUNCTION(execRemoveOnVirtualCameraUpdatedDelegate); \
	DECLARE_FUNCTION(execAddOnVirtualCameraUpdatedDelegate); \
	DECLARE_FUNCTION(execSetOnActorClickedDelegate); \
	DECLARE_FUNCTION(execSetBeforeSetVirtualCameraTransformDelegate); \
	DECLARE_FUNCTION(execSetFocusVisualization); \
	DECLARE_FUNCTION(execSetTrackedActorForFocus); \
	DECLARE_FUNCTION(execAddBlendableToCamera); \
	DECLARE_FUNCTION(execGetRelativeTransform); \
	DECLARE_FUNCTION(execSetSaveSettingsOnStopStreaming); \
	DECLARE_FUNCTION(execShouldSaveSettingsOnStopStreaming); \
	DECLARE_FUNCTION(execIsStreaming); \
	DECLARE_FUNCTION(execSetLiveLinkRepresentation); \
	DECLARE_FUNCTION(execGetLiveLinkRepresentation); \
	DECLARE_FUNCTION(execGetOptions); \
	DECLARE_FUNCTION(execGetPresetContainer); \
	DECLARE_FUNCTION(execGetSequenceController); \
	DECLARE_FUNCTION(execGetSceneCaptureComponent); \
	DECLARE_FUNCTION(execGetStreamedCameraComponent);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void RemoveOnVirtualCameraUpdatedDelegate_Implementation(FVirtualCameraTickDelegate const& InDelegate) {}; \
	virtual void AddOnVirtualCameraUpdatedDelegate_Implementation(FVirtualCameraTickDelegate const& InDelegate) {}; \
	virtual void SetOnActorClickedDelegate_Implementation(FOnActorClickedDelegate const& InDelegate) {}; \
	virtual void SetBeforeSetVirtualCameraTransformDelegate_Implementation(FPreSetVirtualCameraTransform const& InDelegate) {}; \
	virtual void SetFocusVisualization_Implementation(bool bInShowFocusVisualization) {}; \
	virtual void SetTrackedActorForFocus_Implementation(AActor* InActorToTrack, FVector const& TrackingPointOffset) {}; \
	virtual void AddBlendableToCamera_Implementation(TScriptInterface<IBlendableInterface> const& InBlendableToAdd, float InWeight) {}; \
	virtual FTransform GetRelativeTransform_Implementation() const { return FTransform(); }; \
	virtual void SetSaveSettingsOnStopStreaming_Implementation(bool bShouldSettingsSave) {}; \
	virtual bool ShouldSaveSettingsOnStopStreaming_Implementation() const { return false; }; \
	virtual bool IsStreaming_Implementation() const { return false; }; \
	virtual void SetLiveLinkRepresentation_Implementation(FLiveLinkSubjectRepresentation const& InLiveLinkRepresenation) {}; \
	virtual FLiveLinkSubjectRepresentation GetLiveLinkRepresentation_Implementation() const { return FLiveLinkSubjectRepresentation(); }; \
	virtual TScriptInterface<IVirtualCameraOptions> GetOptions_Implementation() { return NULL; }; \
	virtual TScriptInterface<IVirtualCameraPresetContainer> GetPresetContainer_Implementation() { return NULL; }; \
	virtual ULevelSequencePlaybackController* GetSequenceController_Implementation() const { return NULL; }; \
	virtual USceneCaptureComponent2D* GetSceneCaptureComponent_Implementation() const { return NULL; }; \
	virtual UCineCameraComponent* GetStreamedCameraComponent_Implementation() const { return NULL; }; \
 \
	DECLARE_FUNCTION(execRemoveOnVirtualCameraUpdatedDelegate); \
	DECLARE_FUNCTION(execAddOnVirtualCameraUpdatedDelegate); \
	DECLARE_FUNCTION(execSetOnActorClickedDelegate); \
	DECLARE_FUNCTION(execSetBeforeSetVirtualCameraTransformDelegate); \
	DECLARE_FUNCTION(execSetFocusVisualization); \
	DECLARE_FUNCTION(execSetTrackedActorForFocus); \
	DECLARE_FUNCTION(execAddBlendableToCamera); \
	DECLARE_FUNCTION(execGetRelativeTransform); \
	DECLARE_FUNCTION(execSetSaveSettingsOnStopStreaming); \
	DECLARE_FUNCTION(execShouldSaveSettingsOnStopStreaming); \
	DECLARE_FUNCTION(execIsStreaming); \
	DECLARE_FUNCTION(execSetLiveLinkRepresentation); \
	DECLARE_FUNCTION(execGetLiveLinkRepresentation); \
	DECLARE_FUNCTION(execGetOptions); \
	DECLARE_FUNCTION(execGetPresetContainer); \
	DECLARE_FUNCTION(execGetSequenceController); \
	DECLARE_FUNCTION(execGetSceneCaptureComponent); \
	DECLARE_FUNCTION(execGetStreamedCameraComponent);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_EVENT_PARMS \
	struct VirtualCameraController_eventAddBlendableToCamera_Parms \
	{ \
		TScriptInterface<IBlendableInterface> InBlendableToAdd; \
		float InWeight; \
	}; \
	struct VirtualCameraController_eventAddOnVirtualCameraUpdatedDelegate_Parms \
	{ \
		FVirtualCameraTickDelegate InDelegate; \
	}; \
	struct VirtualCameraController_eventGetLiveLinkRepresentation_Parms \
	{ \
		FLiveLinkSubjectRepresentation ReturnValue; \
	}; \
	struct VirtualCameraController_eventGetOptions_Parms \
	{ \
		TScriptInterface<IVirtualCameraOptions> ReturnValue; \
	}; \
	struct VirtualCameraController_eventGetPresetContainer_Parms \
	{ \
		TScriptInterface<IVirtualCameraPresetContainer> ReturnValue; \
	}; \
	struct VirtualCameraController_eventGetRelativeTransform_Parms \
	{ \
		FTransform ReturnValue; \
	}; \
	struct VirtualCameraController_eventGetSceneCaptureComponent_Parms \
	{ \
		USceneCaptureComponent2D* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		VirtualCameraController_eventGetSceneCaptureComponent_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	}; \
	struct VirtualCameraController_eventGetSequenceController_Parms \
	{ \
		ULevelSequencePlaybackController* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		VirtualCameraController_eventGetSequenceController_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	}; \
	struct VirtualCameraController_eventGetStreamedCameraComponent_Parms \
	{ \
		UCineCameraComponent* ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		VirtualCameraController_eventGetStreamedCameraComponent_Parms() \
			: ReturnValue(NULL) \
		{ \
		} \
	}; \
	struct VirtualCameraController_eventIsStreaming_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		VirtualCameraController_eventIsStreaming_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct VirtualCameraController_eventRemoveOnVirtualCameraUpdatedDelegate_Parms \
	{ \
		FVirtualCameraTickDelegate InDelegate; \
	}; \
	struct VirtualCameraController_eventSetBeforeSetVirtualCameraTransformDelegate_Parms \
	{ \
		FPreSetVirtualCameraTransform InDelegate; \
	}; \
	struct VirtualCameraController_eventSetFocusVisualization_Parms \
	{ \
		bool bInShowFocusVisualization; \
	}; \
	struct VirtualCameraController_eventSetLiveLinkRepresentation_Parms \
	{ \
		FLiveLinkSubjectRepresentation InLiveLinkRepresenation; \
	}; \
	struct VirtualCameraController_eventSetOnActorClickedDelegate_Parms \
	{ \
		FOnActorClickedDelegate InDelegate; \
	}; \
	struct VirtualCameraController_eventSetSaveSettingsOnStopStreaming_Parms \
	{ \
		bool bShouldSettingsSave; \
	}; \
	struct VirtualCameraController_eventSetTrackedActorForFocus_Parms \
	{ \
		AActor* InActorToTrack; \
		FVector TrackingPointOffset; \
	}; \
	struct VirtualCameraController_eventShouldSaveSettingsOnStopStreaming_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		VirtualCameraController_eventShouldSaveSettingsOnStopStreaming_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	};


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualCameraController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraController(UVirtualCameraController&&); \
	NO_API UVirtualCameraController(const UVirtualCameraController&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraController(UVirtualCameraController&&); \
	NO_API UVirtualCameraController(const UVirtualCameraController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraController); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualCameraController)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUVirtualCameraController(); \
	friend struct Z_Construct_UClass_UVirtualCameraController_Statics; \
public: \
	DECLARE_CLASS(UVirtualCameraController, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVirtualCameraController)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IVirtualCameraController() {} \
public: \
	typedef UVirtualCameraController UClassType; \
	typedef IVirtualCameraController ThisClass; \
	static void Execute_AddBlendableToCamera(UObject* O, TScriptInterface<IBlendableInterface> const& InBlendableToAdd, float InWeight); \
	static void Execute_AddOnVirtualCameraUpdatedDelegate(UObject* O, FVirtualCameraTickDelegate const& InDelegate); \
	static FLiveLinkSubjectRepresentation Execute_GetLiveLinkRepresentation(const UObject* O); \
	static TScriptInterface<IVirtualCameraOptions> Execute_GetOptions(UObject* O); \
	static TScriptInterface<IVirtualCameraPresetContainer> Execute_GetPresetContainer(UObject* O); \
	static FTransform Execute_GetRelativeTransform(const UObject* O); \
	static USceneCaptureComponent2D* Execute_GetSceneCaptureComponent(const UObject* O); \
	static ULevelSequencePlaybackController* Execute_GetSequenceController(const UObject* O); \
	static UCineCameraComponent* Execute_GetStreamedCameraComponent(const UObject* O); \
	static bool Execute_IsStreaming(const UObject* O); \
	static void Execute_RemoveOnVirtualCameraUpdatedDelegate(UObject* O, FVirtualCameraTickDelegate const& InDelegate); \
	static void Execute_SetBeforeSetVirtualCameraTransformDelegate(UObject* O, FPreSetVirtualCameraTransform const& InDelegate); \
	static void Execute_SetFocusVisualization(UObject* O, bool bInShowFocusVisualization); \
	static void Execute_SetLiveLinkRepresentation(UObject* O, FLiveLinkSubjectRepresentation const& InLiveLinkRepresenation); \
	static void Execute_SetOnActorClickedDelegate(UObject* O, FOnActorClickedDelegate const& InDelegate); \
	static void Execute_SetSaveSettingsOnStopStreaming(UObject* O, bool bShouldSettingsSave); \
	static void Execute_SetTrackedActorForFocus(UObject* O, AActor* InActorToTrack, FVector const& TrackingPointOffset); \
	static bool Execute_ShouldSaveSettingsOnStopStreaming(const UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_INCLASS_IINTERFACE \
protected: \
	virtual ~IVirtualCameraController() {} \
public: \
	typedef UVirtualCameraController UClassType; \
	typedef IVirtualCameraController ThisClass; \
	static void Execute_AddBlendableToCamera(UObject* O, TScriptInterface<IBlendableInterface> const& InBlendableToAdd, float InWeight); \
	static void Execute_AddOnVirtualCameraUpdatedDelegate(UObject* O, FVirtualCameraTickDelegate const& InDelegate); \
	static FLiveLinkSubjectRepresentation Execute_GetLiveLinkRepresentation(const UObject* O); \
	static TScriptInterface<IVirtualCameraOptions> Execute_GetOptions(UObject* O); \
	static TScriptInterface<IVirtualCameraPresetContainer> Execute_GetPresetContainer(UObject* O); \
	static FTransform Execute_GetRelativeTransform(const UObject* O); \
	static USceneCaptureComponent2D* Execute_GetSceneCaptureComponent(const UObject* O); \
	static ULevelSequencePlaybackController* Execute_GetSequenceController(const UObject* O); \
	static UCineCameraComponent* Execute_GetStreamedCameraComponent(const UObject* O); \
	static bool Execute_IsStreaming(const UObject* O); \
	static void Execute_RemoveOnVirtualCameraUpdatedDelegate(UObject* O, FVirtualCameraTickDelegate const& InDelegate); \
	static void Execute_SetBeforeSetVirtualCameraTransformDelegate(UObject* O, FPreSetVirtualCameraTransform const& InDelegate); \
	static void Execute_SetFocusVisualization(UObject* O, bool bInShowFocusVisualization); \
	static void Execute_SetLiveLinkRepresentation(UObject* O, FLiveLinkSubjectRepresentation const& InLiveLinkRepresenation); \
	static void Execute_SetOnActorClickedDelegate(UObject* O, FOnActorClickedDelegate const& InDelegate); \
	static void Execute_SetSaveSettingsOnStopStreaming(UObject* O, bool bShouldSettingsSave); \
	static void Execute_SetTrackedActorForFocus(UObject* O, AActor* InActorToTrack, FVector const& TrackingPointOffset); \
	static bool Execute_ShouldSaveSettingsOnStopStreaming(const UObject* O); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_76_PROLOG \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_84_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_84_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h_79_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class UVirtualCameraController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_IVirtualCameraController_h


#define FOREACH_ENUM_EVIRTUALCAMERAFOCUSMETHOD(op) \
	op(EVirtualCameraFocusMethod::None) \
	op(EVirtualCameraFocusMethod::Manual) \
	op(EVirtualCameraFocusMethod::Tracking) \
	op(EVirtualCameraFocusMethod::Auto) 

enum class EVirtualCameraFocusMethod : uint8;
template<> VIRTUALCAMERA_API UEnum* StaticEnum<EVirtualCameraFocusMethod>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
