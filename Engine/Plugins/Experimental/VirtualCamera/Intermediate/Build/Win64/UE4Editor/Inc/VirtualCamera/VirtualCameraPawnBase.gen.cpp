// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/VirtualCameraPawnBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVirtualCameraPawnBase() {}
// Cross Module References
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_AVirtualCameraPawnBase_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_AVirtualCameraPawnBase();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraCineCameraComponent_NoRegister();
	VIRTUALCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FVirtualCameraSettingsPreset();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraPresetContainer_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraOptions_NoRegister();
// End Cross Module References
	static FName NAME_AVirtualCameraPawnBase_HighlightTappedActor = FName(TEXT("HighlightTappedActor"));
	void AVirtualCameraPawnBase::HighlightTappedActor(AActor* HighlightedActor)
	{
		VirtualCameraPawnBase_eventHighlightTappedActor_Parms Parms;
		Parms.HighlightedActor=HighlightedActor;
		ProcessEvent(FindFunctionChecked(NAME_AVirtualCameraPawnBase_HighlightTappedActor),&Parms);
	}
	static FName NAME_AVirtualCameraPawnBase_LoadFinished = FName(TEXT("LoadFinished"));
	void AVirtualCameraPawnBase::LoadFinished()
	{
		ProcessEvent(FindFunctionChecked(NAME_AVirtualCameraPawnBase_LoadFinished),NULL);
	}
	static FName NAME_AVirtualCameraPawnBase_TriggerFocusPlaneTimer = FName(TEXT("TriggerFocusPlaneTimer"));
	void AVirtualCameraPawnBase::TriggerFocusPlaneTimer()
	{
		ProcessEvent(FindFunctionChecked(NAME_AVirtualCameraPawnBase_TriggerFocusPlaneTimer),NULL);
	}
	void AVirtualCameraPawnBase::StaticRegisterNativesAVirtualCameraPawnBase()
	{
	}
	struct Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HighlightedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::NewProp_HighlightedActor = { "HighlightedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VirtualCameraPawnBase_eventHighlightTappedActor_Parms, HighlightedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::NewProp_HighlightedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Blueprint event to trigger the highlighting of a specific actor.\n\x09 * @param HighlightedActor - The Actor on which the focus is set\n\x09 */" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Blueprint event to trigger the highlighting of a specific actor.\n@param HighlightedActor - The Actor on which the focus is set" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AVirtualCameraPawnBase, nullptr, "HighlightTappedActor", nullptr, nullptr, sizeof(VirtualCameraPawnBase_eventHighlightTappedActor_Parms), Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Blueprint event for signaling UI that game settings have been loaded.\n\x09 */" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Blueprint event for signaling UI that game settings have been loaded." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AVirtualCameraPawnBase, nullptr, "LoadFinished", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/**\n\x09 * Blueprint event to trigger focus plane visualization for a set amount of time.\n\x09 */" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Blueprint event to trigger focus plane visualization for a set amount of time." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AVirtualCameraPawnBase, nullptr, "TriggerFocusPlaneTimer", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AVirtualCameraPawnBase_NoRegister()
	{
		return AVirtualCameraPawnBase::StaticClass();
	}
	struct Z_Construct_UClass_AVirtualCameraPawnBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CineCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CineCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSaveSettingsWhenClosing_MetaData[];
#endif
		static void NewProp_bSaveSettingsWhenClosing_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSaveSettingsWhenClosing;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SavedSettingsSlotName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SavedSettingsSlotName;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SettingsPresets_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SettingsPresets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SettingsPresets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_SettingsPresets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowFocusVisualization_MetaData[];
#endif
		static void NewProp_bAllowFocusVisualization_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowFocusVisualization;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVirtualCameraPawnBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AVirtualCameraPawnBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AVirtualCameraPawnBase_HighlightTappedActor, "HighlightTappedActor" }, // 500124777
		{ &Z_Construct_UFunction_AVirtualCameraPawnBase_LoadFinished, "LoadFinished" }, // 3981523519
		{ &Z_Construct_UFunction_AVirtualCameraPawnBase_TriggerFocusPlaneTimer, "TriggerFocusPlaneTimer" }, // 1558362561
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraPawnBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * A class to handle aspects of virtual Camera related to general settings, and communicating with components.\n */" },
		{ "HideCategories", "Pawn Camera Rendering Replication Input Actor HLOD Navigation" },
		{ "IncludePath", "VirtualCameraPawnBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "A class to handle aspects of virtual Camera related to general settings, and communicating with components." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_CineCamera_MetaData[] = {
		{ "Category", "Cinematic Camera" },
		{ "Comment", "/** Cinematic camera used for view */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Cinematic camera used for view" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_CineCamera = { "CineCamera", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraPawnBase, CineCamera), Z_Construct_UClass_UVirtualCameraCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_CineCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_CineCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bSaveSettingsWhenClosing_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Determines if values should be saved between sessions */" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Determines if values should be saved between sessions" },
	};
#endif
	void Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bSaveSettingsWhenClosing_SetBit(void* Obj)
	{
		((AVirtualCameraPawnBase*)Obj)->bSaveSettingsWhenClosing = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bSaveSettingsWhenClosing = { "bSaveSettingsWhenClosing", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AVirtualCameraPawnBase), &Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bSaveSettingsWhenClosing_SetBit, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bSaveSettingsWhenClosing_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bSaveSettingsWhenClosing_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SavedSettingsSlotName_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Stores the name of the save slot being used currently */" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Stores the name of the save slot being used currently" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SavedSettingsSlotName = { "SavedSettingsSlotName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraPawnBase, SavedSettingsSlotName), METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SavedSettingsSlotName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SavedSettingsSlotName_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets_ValueProp = { "SettingsPresets", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FVirtualCameraSettingsPreset, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets_Key_KeyProp = { "SettingsPresets_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/* Stores the list of settings presets, and saved presets */" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Stores the list of settings presets, and saved presets" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets = { "SettingsPresets", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraPawnBase, SettingsPresets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bAllowFocusVisualization_MetaData[] = {
		{ "Category", "Default" },
		{ "Comment", "/** Should focus plane be shown on all touch focus events */" },
		{ "ModuleRelativePath", "Public/VirtualCameraPawnBase.h" },
		{ "ToolTip", "Should focus plane be shown on all touch focus events" },
	};
#endif
	void Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bAllowFocusVisualization_SetBit(void* Obj)
	{
		((AVirtualCameraPawnBase*)Obj)->bAllowFocusVisualization = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bAllowFocusVisualization = { "bAllowFocusVisualization", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AVirtualCameraPawnBase), &Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bAllowFocusVisualization_SetBit, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bAllowFocusVisualization_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bAllowFocusVisualization_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AVirtualCameraPawnBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_CineCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bSaveSettingsWhenClosing,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SavedSettingsSlotName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_SettingsPresets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraPawnBase_Statics::NewProp_bAllowFocusVisualization,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UVirtualCameraPresetContainer_NoRegister, (int32)VTABLE_OFFSET(AVirtualCameraPawnBase, IVirtualCameraPresetContainer), false },
			{ Z_Construct_UClass_UVirtualCameraOptions_NoRegister, (int32)VTABLE_OFFSET(AVirtualCameraPawnBase, IVirtualCameraOptions), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVirtualCameraPawnBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVirtualCameraPawnBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVirtualCameraPawnBase_Statics::ClassParams = {
		&AVirtualCameraPawnBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AVirtualCameraPawnBase_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A5u,
		METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraPawnBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVirtualCameraPawnBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVirtualCameraPawnBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVirtualCameraPawnBase, 330352917);
	template<> VIRTUALCAMERA_API UClass* StaticClass<AVirtualCameraPawnBase>()
	{
		return AVirtualCameraPawnBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVirtualCameraPawnBase(Z_Construct_UClass_AVirtualCameraPawnBase, &AVirtualCameraPawnBase::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("AVirtualCameraPawnBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AVirtualCameraPawnBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
