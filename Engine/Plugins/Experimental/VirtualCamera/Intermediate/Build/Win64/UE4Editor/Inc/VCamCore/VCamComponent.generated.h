// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UVCamComponent;
struct FLiveLinkCameraBlueprintData;
class UVCamOutputProviderBase;
class UVCamModifierContext;
class UInterface;
class UVCamModifier;
class UCineCameraComponent;
class UObject; class UObject;
#ifdef VCAMCORE_VCamComponent_generated_h
#error "VCamComponent.generated.h already included, missing '#pragma once' in VCamComponent.h"
#endif
#define VCAMCORE_VCamComponent_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_29_DELEGATE \
struct _Script_VCamCore_eventOnComponentReplaced_Parms \
{ \
	UVCamComponent* NewComponent; \
}; \
static inline void FOnComponentReplaced_DelegateWrapper(const FMulticastScriptDelegate& OnComponentReplaced, UVCamComponent* NewComponent) \
{ \
	_Script_VCamCore_eventOnComponentReplaced_Parms Parms; \
	Parms.NewComponent=NewComponent; \
	OnComponentReplaced.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetLiveLinkDataForCurrentFrame); \
	DECLARE_FUNCTION(execGetOutputProvidersByClass); \
	DECLARE_FUNCTION(execGetOutputProviderByIndex); \
	DECLARE_FUNCTION(execGetAllOutputProviders); \
	DECLARE_FUNCTION(execGetNumberOfOutputProviders); \
	DECLARE_FUNCTION(execRemoveOutputProviderByIndex); \
	DECLARE_FUNCTION(execRemoveOutputProvider); \
	DECLARE_FUNCTION(execRemoveAllOutputProviders); \
	DECLARE_FUNCTION(execSetOutputProviderIndex); \
	DECLARE_FUNCTION(execInsertOutputProvider); \
	DECLARE_FUNCTION(execAddOutputProvider); \
	DECLARE_FUNCTION(execGetModifierContext); \
	DECLARE_FUNCTION(execSetModifierContextClass); \
	DECLARE_FUNCTION(execGetModifiersByInterface); \
	DECLARE_FUNCTION(execGetModifiersByClass); \
	DECLARE_FUNCTION(execGetModifierByName); \
	DECLARE_FUNCTION(execGetModifierByIndex); \
	DECLARE_FUNCTION(execGetAllModifiers); \
	DECLARE_FUNCTION(execGetNumberOfModifiers); \
	DECLARE_FUNCTION(execRemoveModifierByName); \
	DECLARE_FUNCTION(execRemoveModifierByIndex); \
	DECLARE_FUNCTION(execRemoveModifier); \
	DECLARE_FUNCTION(execRemoveAllModifiers); \
	DECLARE_FUNCTION(execSetModifierIndex); \
	DECLARE_FUNCTION(execInsertModifier); \
	DECLARE_FUNCTION(execAddModifier); \
	DECLARE_FUNCTION(execGetTargetCamera); \
	DECLARE_FUNCTION(execIsEnabled); \
	DECLARE_FUNCTION(execSetEnabled); \
	DECLARE_FUNCTION(execHandleObjectReplaced);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetLiveLinkDataForCurrentFrame); \
	DECLARE_FUNCTION(execGetOutputProvidersByClass); \
	DECLARE_FUNCTION(execGetOutputProviderByIndex); \
	DECLARE_FUNCTION(execGetAllOutputProviders); \
	DECLARE_FUNCTION(execGetNumberOfOutputProviders); \
	DECLARE_FUNCTION(execRemoveOutputProviderByIndex); \
	DECLARE_FUNCTION(execRemoveOutputProvider); \
	DECLARE_FUNCTION(execRemoveAllOutputProviders); \
	DECLARE_FUNCTION(execSetOutputProviderIndex); \
	DECLARE_FUNCTION(execInsertOutputProvider); \
	DECLARE_FUNCTION(execAddOutputProvider); \
	DECLARE_FUNCTION(execGetModifierContext); \
	DECLARE_FUNCTION(execSetModifierContextClass); \
	DECLARE_FUNCTION(execGetModifiersByInterface); \
	DECLARE_FUNCTION(execGetModifiersByClass); \
	DECLARE_FUNCTION(execGetModifierByName); \
	DECLARE_FUNCTION(execGetModifierByIndex); \
	DECLARE_FUNCTION(execGetAllModifiers); \
	DECLARE_FUNCTION(execGetNumberOfModifiers); \
	DECLARE_FUNCTION(execRemoveModifierByName); \
	DECLARE_FUNCTION(execRemoveModifierByIndex); \
	DECLARE_FUNCTION(execRemoveModifier); \
	DECLARE_FUNCTION(execRemoveAllModifiers); \
	DECLARE_FUNCTION(execSetModifierIndex); \
	DECLARE_FUNCTION(execInsertModifier); \
	DECLARE_FUNCTION(execAddModifier); \
	DECLARE_FUNCTION(execGetTargetCamera); \
	DECLARE_FUNCTION(execIsEnabled); \
	DECLARE_FUNCTION(execSetEnabled); \
	DECLARE_FUNCTION(execHandleObjectReplaced);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamComponent(); \
	friend struct Z_Construct_UClass_UVCamComponent_Statics; \
public: \
	DECLARE_CLASS(UVCamComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_INCLASS \
private: \
	static void StaticRegisterNativesUVCamComponent(); \
	friend struct Z_Construct_UClass_UVCamComponent_Statics; \
public: \
	DECLARE_CLASS(UVCamComponent, USceneComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamComponent(UVCamComponent&&); \
	NO_API UVCamComponent(const UVCamComponent&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamComponent(UVCamComponent&&); \
	NO_API UVCamComponent(const UVCamComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVCamComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bEnabled() { return STRUCT_OFFSET(UVCamComponent, bEnabled); } \
	FORCEINLINE static uint32 __PPO__ModifierContext() { return STRUCT_OFFSET(UVCamComponent, ModifierContext); } \
	FORCEINLINE static uint32 __PPO__ModifierStack() { return STRUCT_OFFSET(UVCamComponent, ModifierStack); } \
	FORCEINLINE static uint32 __PPO__bIsLockedToViewport() { return STRUCT_OFFSET(UVCamComponent, bIsLockedToViewport); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_41_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamComponent_h


#define FOREACH_ENUM_EVCAMTARGETVIEWPORTID(op) \
	op(EVCamTargetViewportID::CurrentlySelected) \
	op(EVCamTargetViewportID::Viewport1) \
	op(EVCamTargetViewportID::Viewport2) \
	op(EVCamTargetViewportID::Viewport3) \
	op(EVCamTargetViewportID::Viewport4) 

enum class EVCamTargetViewportID : uint8;
template<> VCAMCORE_API UEnum* StaticEnum<EVCamTargetViewportID>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
