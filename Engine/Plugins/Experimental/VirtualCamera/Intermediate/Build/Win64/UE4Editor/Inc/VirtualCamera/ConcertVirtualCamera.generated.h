// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIRTUALCAMERA_ConcertVirtualCamera_generated_h
#error "ConcertVirtualCamera.generated.h already included, missing '#pragma once' in ConcertVirtualCamera.h"
#endif
#define VIRTUALCAMERA_ConcertVirtualCamera_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Private_ConcertVirtualCamera_h_107_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FConcertVirtualCameraControllerEvent>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Private_ConcertVirtualCamera_h_86_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FConcertVirtualCameraCameraComponentEvent>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Private_ConcertVirtualCamera_h_48_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FConcertVirtualCameraCameraData>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Private_ConcertVirtualCamera_h_25_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FConcertVirtualCameraCameraFocusData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Private_ConcertVirtualCamera_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
