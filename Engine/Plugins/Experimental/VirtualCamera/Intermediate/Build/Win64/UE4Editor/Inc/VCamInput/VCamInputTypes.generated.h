// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMotionEvent;
struct FPointerEvent;
struct FAnalogInputEvent;
struct FKeyEvent;
#ifdef VCAMINPUT_VCamInputTypes_generated_h
#error "VCamInputTypes.generated.h already included, missing '#pragma once' in VCamInputTypes.h"
#endif
#define VCAMINPUT_VCamInputTypes_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputTypes_h_15_DELEGATE \
struct _Script_VCamInput_eventMotionInputEvent_Parms \
{ \
	float DeltaTime; \
	FMotionEvent MotionEvent; \
}; \
static inline void FMotionInputEvent_DelegateWrapper(const FScriptDelegate& MotionInputEvent, float DeltaTime, FMotionEvent const& MotionEvent) \
{ \
	_Script_VCamInput_eventMotionInputEvent_Parms Parms; \
	Parms.DeltaTime=DeltaTime; \
	Parms.MotionEvent=MotionEvent; \
	MotionInputEvent.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputTypes_h_14_DELEGATE \
struct _Script_VCamInput_eventPointerInputDelegate_Parms \
{ \
	float DeltaTime; \
	FPointerEvent MouseEvent; \
}; \
static inline void FPointerInputDelegate_DelegateWrapper(const FScriptDelegate& PointerInputDelegate, float DeltaTime, FPointerEvent const& MouseEvent) \
{ \
	_Script_VCamInput_eventPointerInputDelegate_Parms Parms; \
	Parms.DeltaTime=DeltaTime; \
	Parms.MouseEvent=MouseEvent; \
	PointerInputDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputTypes_h_13_DELEGATE \
struct _Script_VCamInput_eventAnalogInputDelegate_Parms \
{ \
	float DeltaTime; \
	FAnalogInputEvent AnalogEvent; \
}; \
static inline void FAnalogInputDelegate_DelegateWrapper(const FScriptDelegate& AnalogInputDelegate, float DeltaTime, FAnalogInputEvent const& AnalogEvent) \
{ \
	_Script_VCamInput_eventAnalogInputDelegate_Parms Parms; \
	Parms.DeltaTime=DeltaTime; \
	Parms.AnalogEvent=AnalogEvent; \
	AnalogInputDelegate.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputTypes_h_12_DELEGATE \
struct _Script_VCamInput_eventKeyInputDelegate_Parms \
{ \
	float DeltaTime; \
	FKeyEvent KeyEvent; \
}; \
static inline void FKeyInputDelegate_DelegateWrapper(const FScriptDelegate& KeyInputDelegate, float DeltaTime, FKeyEvent const& KeyEvent) \
{ \
	_Script_VCamInput_eventKeyInputDelegate_Parms Parms; \
	Parms.DeltaTime=DeltaTime; \
	Parms.KeyEvent=KeyEvent; \
	KeyInputDelegate.ProcessDelegate<UObject>(&Parms); \
}


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputTypes_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
