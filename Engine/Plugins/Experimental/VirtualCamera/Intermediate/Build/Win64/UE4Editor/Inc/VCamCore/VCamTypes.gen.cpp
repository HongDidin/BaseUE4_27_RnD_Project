// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamTypes() {}
// Cross Module References
	VCAMCORE_API UScriptStruct* Z_Construct_UScriptStruct_FModifierStackEntry();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifier_NoRegister();
// End Cross Module References
class UScriptStruct* FModifierStackEntry::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VCAMCORE_API uint32 Get_Z_Construct_UScriptStruct_FModifierStackEntry_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FModifierStackEntry, Z_Construct_UPackage__Script_VCamCore(), TEXT("ModifierStackEntry"), sizeof(FModifierStackEntry), Get_Z_Construct_UScriptStruct_FModifierStackEntry_Hash());
	}
	return Singleton;
}
template<> VCAMCORE_API UScriptStruct* StaticStruct<FModifierStackEntry>()
{
	return FModifierStackEntry::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FModifierStackEntry(FModifierStackEntry::StaticStruct, TEXT("/Script/VCamCore"), TEXT("ModifierStackEntry"), false, nullptr, nullptr);
static struct FScriptStruct_VCamCore_StaticRegisterNativesFModifierStackEntry
{
	FScriptStruct_VCamCore_StaticRegisterNativesFModifierStackEntry()
	{
		UScriptStruct::DeferCppStructOps<FModifierStackEntry>(FName(TEXT("ModifierStackEntry")));
	}
} ScriptStruct_VCamCore_StaticRegisterNativesFModifierStackEntry;
	struct Z_Construct_UScriptStruct_FModifierStackEntry_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GeneratedModifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GeneratedModifier;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FModifierStackEntry_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "// Links a Modifier with a Name for use in a Modifier Stack\n" },
		{ "ModuleRelativePath", "Public/VCamTypes.h" },
		{ "ToolTip", "Links a Modifier with a Name for use in a Modifier Stack" },
	};
#endif
	void* Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FModifierStackEntry>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Modifier" },
		{ "Comment", "// Identifier for this modifier in the stack\n" },
		{ "ModuleRelativePath", "Public/VCamTypes.h" },
		{ "ToolTip", "Identifier for this modifier in the stack" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FModifierStackEntry, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Modifier" },
		{ "Comment", "// Controls whether the modifier actually gets applied\n" },
		{ "ModuleRelativePath", "Public/VCamTypes.h" },
		{ "ToolTip", "Controls whether the modifier actually gets applied" },
	};
#endif
	void Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((FModifierStackEntry*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FModifierStackEntry), &Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_GeneratedModifier_MetaData[] = {
		{ "Category", "Modifier" },
		{ "Comment", "// The current generated modifier instance\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VCamTypes.h" },
		{ "ToolTip", "The current generated modifier instance" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_GeneratedModifier = { "GeneratedModifier", nullptr, (EPropertyFlags)0x0012000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FModifierStackEntry, GeneratedModifier), Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_GeneratedModifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_GeneratedModifier_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FModifierStackEntry_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FModifierStackEntry_Statics::NewProp_GeneratedModifier,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FModifierStackEntry_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
		nullptr,
		&NewStructOps,
		"ModifierStackEntry",
		sizeof(FModifierStackEntry),
		alignof(FModifierStackEntry),
		Z_Construct_UScriptStruct_FModifierStackEntry_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FModifierStackEntry_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FModifierStackEntry()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FModifierStackEntry_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VCamCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ModifierStackEntry"), sizeof(FModifierStackEntry), Get_Z_Construct_UScriptStruct_FModifierStackEntry_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FModifierStackEntry_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FModifierStackEntry_Hash() { return 2590858293U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
