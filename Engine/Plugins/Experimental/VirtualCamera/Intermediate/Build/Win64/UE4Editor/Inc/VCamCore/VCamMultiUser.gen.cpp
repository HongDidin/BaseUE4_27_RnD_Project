// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamMultiUser.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamMultiUser() {}
// Cross Module References
	VCAMCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	VCAMCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMultiUserVCamCameraData();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	VCAMCORE_API UScriptStruct* Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData();
	CINEMATICCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FCameraLensSettings();
	CINEMATICCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FCameraFilmbackSettings();
// End Cross Module References
class UScriptStruct* FMultiUserVCamCameraComponentEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VCAMCORE_API uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent, Z_Construct_UPackage__Script_VCamCore(), TEXT("MultiUserVCamCameraComponentEvent"), sizeof(FMultiUserVCamCameraComponentEvent), Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Hash());
	}
	return Singleton;
}
template<> VCAMCORE_API UScriptStruct* StaticStruct<FMultiUserVCamCameraComponentEvent>()
{
	return FMultiUserVCamCameraComponentEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMultiUserVCamCameraComponentEvent(FMultiUserVCamCameraComponentEvent::StaticStruct, TEXT("/Script/VCamCore"), TEXT("MultiUserVCamCameraComponentEvent"), false, nullptr, nullptr);
static struct FScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraComponentEvent
{
	FScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraComponentEvent()
	{
		UScriptStruct::DeferCppStructOps<FMultiUserVCamCameraComponentEvent>(FName(TEXT("MultiUserVCamCameraComponentEvent")));
	}
} ScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraComponentEvent;
	struct Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TrackingName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMultiUserVCamCameraComponentEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_TrackingName_MetaData[] = {
		{ "Comment", "/** Name of the tracking camera */" },
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
		{ "ToolTip", "Name of the tracking camera" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_TrackingName = { "TrackingName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraComponentEvent, TrackingName), METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_TrackingName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_TrackingName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_CameraData_MetaData[] = {
		{ "Comment", "/** Camera data */" },
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
		{ "ToolTip", "Camera data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_CameraData = { "CameraData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraComponentEvent, CameraData), Z_Construct_UScriptStruct_FMultiUserVCamCameraData, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_CameraData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_CameraData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_TrackingName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::NewProp_CameraData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
		nullptr,
		&NewStructOps,
		"MultiUserVCamCameraComponentEvent",
		sizeof(FMultiUserVCamCameraComponentEvent),
		alignof(FMultiUserVCamCameraComponentEvent),
		Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VCamCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MultiUserVCamCameraComponentEvent"), sizeof(FMultiUserVCamCameraComponentEvent), Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Hash() { return 539651292U; }
class UScriptStruct* FMultiUserVCamCameraData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VCAMCORE_API uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMultiUserVCamCameraData, Z_Construct_UPackage__Script_VCamCore(), TEXT("MultiUserVCamCameraData"), sizeof(FMultiUserVCamCameraData), Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Hash());
	}
	return Singleton;
}
template<> VCAMCORE_API UScriptStruct* StaticStruct<FMultiUserVCamCameraData>()
{
	return FMultiUserVCamCameraData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMultiUserVCamCameraData(FMultiUserVCamCameraData::StaticStruct, TEXT("/Script/VCamCore"), TEXT("MultiUserVCamCameraData"), false, nullptr, nullptr);
static struct FScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraData
{
	FScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraData()
	{
		UScriptStruct::DeferCppStructOps<FMultiUserVCamCameraData>(FName(TEXT("MultiUserVCamCameraData")));
	}
} ScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraData;
	struct Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraActorLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraActorLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraActorRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraActorRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponentLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraComponentLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponentRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraComponentRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentAperture_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentAperture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentFocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentFocalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LensSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilmbackSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilmbackSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMultiUserVCamCameraData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorLocation_MetaData[] = {
		{ "Comment", "/** Camera transform */" },
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
		{ "ToolTip", "Camera transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorLocation = { "CameraActorLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, CameraActorLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorRotation = { "CameraActorRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, CameraActorRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentLocation = { "CameraComponentLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, CameraComponentLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentRotation_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentRotation = { "CameraComponentRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, CameraComponentRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentAperture_MetaData[] = {
		{ "Comment", "/** Camera settings */" },
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
		{ "ToolTip", "Camera settings" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentAperture = { "CurrentAperture", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, CurrentAperture), METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentAperture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentAperture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentFocalLength_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentFocalLength = { "CurrentFocalLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, CurrentFocalLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentFocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentFocalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FocusSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FocusSettings = { "FocusSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, FocusSettings), Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FocusSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FocusSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_LensSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_LensSettings = { "LensSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, LensSettings), Z_Construct_UScriptStruct_FCameraLensSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_LensSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_LensSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FilmbackSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FilmbackSettings = { "FilmbackSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraData, FilmbackSettings), Z_Construct_UScriptStruct_FCameraFilmbackSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FilmbackSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FilmbackSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraActorRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CameraComponentRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentAperture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_CurrentFocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FocusSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_LensSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::NewProp_FilmbackSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
		nullptr,
		&NewStructOps,
		"MultiUserVCamCameraData",
		sizeof(FMultiUserVCamCameraData),
		alignof(FMultiUserVCamCameraData),
		Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMultiUserVCamCameraData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VCamCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MultiUserVCamCameraData"), sizeof(FMultiUserVCamCameraData), Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Hash() { return 3506987081U; }
class UScriptStruct* FMultiUserVCamCameraFocusData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VCAMCORE_API uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData, Z_Construct_UPackage__Script_VCamCore(), TEXT("MultiUserVCamCameraFocusData"), sizeof(FMultiUserVCamCameraFocusData), Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Hash());
	}
	return Singleton;
}
template<> VCAMCORE_API UScriptStruct* StaticStruct<FMultiUserVCamCameraFocusData>()
{
	return FMultiUserVCamCameraFocusData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMultiUserVCamCameraFocusData(FMultiUserVCamCameraFocusData::StaticStruct, TEXT("/Script/VCamCore"), TEXT("MultiUserVCamCameraFocusData"), false, nullptr, nullptr);
static struct FScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraFocusData
{
	FScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraFocusData()
	{
		UScriptStruct::DeferCppStructOps<FMultiUserVCamCameraFocusData>(FName(TEXT("MultiUserVCamCameraFocusData")));
	}
} ScriptStruct_VCamCore_StaticRegisterNativesFMultiUserVCamCameraFocusData;
	struct Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ManualFocusDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ManualFocusDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusSmoothingInterpSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FocusSmoothingInterpSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSmoothFocusChanges_MetaData[];
#endif
		static void NewProp_bSmoothFocusChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSmoothFocusChanges;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMultiUserVCamCameraFocusData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_ManualFocusDistance_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_ManualFocusDistance = { "ManualFocusDistance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraFocusData, ManualFocusDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_ManualFocusDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_ManualFocusDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed = { "FocusSmoothingInterpSpeed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMultiUserVCamCameraFocusData, FocusSmoothingInterpSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_bSmoothFocusChanges_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamMultiUser.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_bSmoothFocusChanges_SetBit(void* Obj)
	{
		((FMultiUserVCamCameraFocusData*)Obj)->bSmoothFocusChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_bSmoothFocusChanges = { "bSmoothFocusChanges", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMultiUserVCamCameraFocusData), &Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_bSmoothFocusChanges_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_bSmoothFocusChanges_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_bSmoothFocusChanges_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_ManualFocusDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::NewProp_bSmoothFocusChanges,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
		nullptr,
		&NewStructOps,
		"MultiUserVCamCameraFocusData",
		sizeof(FMultiUserVCamCameraFocusData),
		alignof(FMultiUserVCamCameraFocusData),
		Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VCamCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MultiUserVCamCameraFocusData"), sizeof(FMultiUserVCamCameraFocusData), Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Hash() { return 3808597029U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
