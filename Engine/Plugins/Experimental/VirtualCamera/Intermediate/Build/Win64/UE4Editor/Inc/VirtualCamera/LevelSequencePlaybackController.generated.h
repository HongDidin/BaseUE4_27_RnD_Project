// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class ULevelSequence;
struct FFrameNumber;
struct FTimecode;
struct FFrameTime;
struct FFrameRate;
struct FLevelSequenceData;
#ifdef VIRTUALCAMERA_LevelSequencePlaybackController_generated_h
#error "LevelSequencePlaybackController.generated.h already included, missing '#pragma once' in LevelSequencePlaybackController.h"
#endif
#define VIRTUALCAMERA_LevelSequencePlaybackController_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_27_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLevelSequenceData_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FLevelSequenceData>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execClearActiveLevelSequence); \
	DECLARE_FUNCTION(execSetActiveLevelSequence); \
	DECLARE_FUNCTION(execStopLevelSequencePlay); \
	DECLARE_FUNCTION(execPlayLevelSequenceReverse); \
	DECLARE_FUNCTION(execPlayLevelSequence); \
	DECLARE_FUNCTION(execPauseLevelSequence); \
	DECLARE_FUNCTION(execIsSequencePlaybackActive); \
	DECLARE_FUNCTION(execJumpToPlaybackPosition); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackTimecode); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackPosition); \
	DECLARE_FUNCTION(execGetCurrentSequenceDuration); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackEnd); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackStart); \
	DECLARE_FUNCTION(execSetSequencerLockedToCameraCut); \
	DECLARE_FUNCTION(execIsSequencerLockedToCameraCut); \
	DECLARE_FUNCTION(execGetCurrentSequenceFrameRate); \
	DECLARE_FUNCTION(execGetActiveLevelSequence); \
	DECLARE_FUNCTION(execGetActiveLevelSequenceName); \
	DECLARE_FUNCTION(execGetLevelSequences);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execClearActiveLevelSequence); \
	DECLARE_FUNCTION(execSetActiveLevelSequence); \
	DECLARE_FUNCTION(execStopLevelSequencePlay); \
	DECLARE_FUNCTION(execPlayLevelSequenceReverse); \
	DECLARE_FUNCTION(execPlayLevelSequence); \
	DECLARE_FUNCTION(execPauseLevelSequence); \
	DECLARE_FUNCTION(execIsSequencePlaybackActive); \
	DECLARE_FUNCTION(execJumpToPlaybackPosition); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackTimecode); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackPosition); \
	DECLARE_FUNCTION(execGetCurrentSequenceDuration); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackEnd); \
	DECLARE_FUNCTION(execGetCurrentSequencePlaybackStart); \
	DECLARE_FUNCTION(execSetSequencerLockedToCameraCut); \
	DECLARE_FUNCTION(execIsSequencerLockedToCameraCut); \
	DECLARE_FUNCTION(execGetCurrentSequenceFrameRate); \
	DECLARE_FUNCTION(execGetActiveLevelSequence); \
	DECLARE_FUNCTION(execGetActiveLevelSequenceName); \
	DECLARE_FUNCTION(execGetLevelSequences);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSequencePlaybackController(); \
	friend struct Z_Construct_UClass_ULevelSequencePlaybackController_Statics; \
public: \
	DECLARE_CLASS(ULevelSequencePlaybackController, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequencePlaybackController)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_INCLASS \
private: \
	static void StaticRegisterNativesULevelSequencePlaybackController(); \
	friend struct Z_Construct_UClass_ULevelSequencePlaybackController_Statics; \
public: \
	DECLARE_CLASS(ULevelSequencePlaybackController, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequencePlaybackController)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequencePlaybackController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequencePlaybackController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequencePlaybackController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequencePlaybackController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequencePlaybackController(ULevelSequencePlaybackController&&); \
	NO_API ULevelSequencePlaybackController(const ULevelSequencePlaybackController&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequencePlaybackController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequencePlaybackController(ULevelSequencePlaybackController&&); \
	NO_API ULevelSequencePlaybackController(const ULevelSequencePlaybackController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequencePlaybackController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequencePlaybackController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequencePlaybackController)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ActiveLevelSequence() { return STRUCT_OFFSET(ULevelSequencePlaybackController, ActiveLevelSequence); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_47_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h_50_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LevelSequencePlaybackController."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class ULevelSequencePlaybackController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_LevelSequencePlaybackController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
