// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamModifierInterface.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamModifierInterface() {}
// Cross Module References
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifierInterface_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifierInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(IVCamModifierInterface::execOnVCamComponentChanged)
	{
		P_GET_OBJECT(UVCamComponent,Z_Param_VCam);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnVCamComponentChanged_Implementation(Z_Param_VCam);
		P_NATIVE_END;
	}
	void IVCamModifierInterface::OnVCamComponentChanged(UVCamComponent* VCam)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_OnVCamComponentChanged instead.");
	}
	void UVCamModifierInterface::StaticRegisterNativesUVCamModifierInterface()
	{
		UClass* Class = UVCamModifierInterface::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnVCamComponentChanged", &IVCamModifierInterface::execOnVCamComponentChanged },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VCam_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VCam;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::NewProp_VCam_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::NewProp_VCam = { "VCam", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamModifierInterface_eventOnVCamComponentChanged_Parms, VCam), Z_Construct_UClass_UVCamComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::NewProp_VCam_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::NewProp_VCam_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::NewProp_VCam,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::Function_MetaDataParams[] = {
		{ "Category", "Active VCam Update" },
		{ "Comment", "//Function used to monitor the status of a VCam. If it changed, function will be triggered.\n" },
		{ "ModuleRelativePath", "Public/VCamModifierInterface.h" },
		{ "ToolTip", "Function used to monitor the status of a VCam. If it changed, function will be triggered." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamModifierInterface, nullptr, "OnVCamComponentChanged", nullptr, nullptr, sizeof(VCamModifierInterface_eventOnVCamComponentChanged_Parms), Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVCamModifierInterface_NoRegister()
	{
		return UVCamModifierInterface::StaticClass();
	}
	struct Z_Construct_UClass_UVCamModifierInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamModifierInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVCamModifierInterface_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVCamModifierInterface_OnVCamComponentChanged, "OnVCamComponentChanged" }, // 201710928
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamModifierInterface_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VCamModifierInterface.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamModifierInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IVCamModifierInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamModifierInterface_Statics::ClassParams = {
		&UVCamModifierInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamModifierInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamModifierInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamModifierInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamModifierInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamModifierInterface, 1670903583);
	template<> VCAMCORE_API UClass* StaticClass<UVCamModifierInterface>()
	{
		return UVCamModifierInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamModifierInterface(Z_Construct_UClass_UVCamModifierInterface, &UVCamModifierInterface::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamModifierInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamModifierInterface);
	static FName NAME_UVCamModifierInterface_OnVCamComponentChanged = FName(TEXT("OnVCamComponentChanged"));
	void IVCamModifierInterface::Execute_OnVCamComponentChanged(UObject* O, UVCamComponent* VCam)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVCamModifierInterface::StaticClass()));
		VCamModifierInterface_eventOnVCamComponentChanged_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UVCamModifierInterface_OnVCamComponentChanged);
		if (Func)
		{
			Parms.VCam=VCam;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IVCamModifierInterface*)(O->GetNativeInterfaceAddress(UVCamModifierInterface::StaticClass())))
		{
			I->OnVCamComponentChanged_Implementation(VCam);
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
