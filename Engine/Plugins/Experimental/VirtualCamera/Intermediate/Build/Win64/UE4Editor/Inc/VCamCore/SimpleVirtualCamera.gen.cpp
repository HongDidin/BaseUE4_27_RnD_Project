// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/SimpleVirtualCamera.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSimpleVirtualCamera() {}
// Cross Module References
	VCAMCORE_API UClass* Z_Construct_UClass_ASimpleVirtualCamera_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_ASimpleVirtualCamera();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_ACineCameraActor();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamComponent_NoRegister();
// End Cross Module References
	void ASimpleVirtualCamera::StaticRegisterNativesASimpleVirtualCamera()
	{
	}
	UClass* Z_Construct_UClass_ASimpleVirtualCamera_NoRegister()
	{
		return ASimpleVirtualCamera::StaticClass();
	}
	struct Z_Construct_UClass_ASimpleVirtualCamera_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VCamComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VCamComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASimpleVirtualCamera_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACineCameraActor,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASimpleVirtualCamera_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * A simple native virtual camera actor\n */" },
		{ "HideCategories", "Input Rendering AutoPlayerActivation Input Rendering" },
		{ "IncludePath", "SimpleVirtualCamera.h" },
		{ "ModuleRelativePath", "Public/SimpleVirtualCamera.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
		{ "ToolTip", "A simple native virtual camera actor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASimpleVirtualCamera_Statics::NewProp_VCamComponent_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SimpleVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ASimpleVirtualCamera_Statics::NewProp_VCamComponent = { "VCamComponent", nullptr, (EPropertyFlags)0x0040000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASimpleVirtualCamera, VCamComponent), Z_Construct_UClass_UVCamComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ASimpleVirtualCamera_Statics::NewProp_VCamComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASimpleVirtualCamera_Statics::NewProp_VCamComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASimpleVirtualCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASimpleVirtualCamera_Statics::NewProp_VCamComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASimpleVirtualCamera_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASimpleVirtualCamera>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASimpleVirtualCamera_Statics::ClassParams = {
		&ASimpleVirtualCamera::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASimpleVirtualCamera_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASimpleVirtualCamera_Statics::PropPointers),
		0,
		0x008000A5u,
		METADATA_PARAMS(Z_Construct_UClass_ASimpleVirtualCamera_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASimpleVirtualCamera_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASimpleVirtualCamera()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASimpleVirtualCamera_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASimpleVirtualCamera, 3063939206);
	template<> VCAMCORE_API UClass* StaticClass<ASimpleVirtualCamera>()
	{
		return ASimpleVirtualCamera::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASimpleVirtualCamera(Z_Construct_UClass_ASimpleVirtualCamera, &ASimpleVirtualCamera::StaticClass, TEXT("/Script/VCamCore"), TEXT("ASimpleVirtualCamera"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASimpleVirtualCamera);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
