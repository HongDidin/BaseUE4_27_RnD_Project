// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIRTUALCAMERA_VirtualCameraSaveGame_generated_h
#error "VirtualCameraSaveGame.generated.h already included, missing '#pragma once' in VirtualCameraSaveGame.h"
#endif
#define VIRTUALCAMERA_VirtualCameraSaveGame_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_274_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVirtualCameraSettingsPreset_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FVirtualCameraSettingsPreset>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_255_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVirtualCameraScreenshot_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FVirtualCameraScreenshot>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_219_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVirtualCameraWaypoint_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FVirtualCameraWaypoint>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_155_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVirtualCameraSettings_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FVirtualCameraSettings>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_37_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FVirtualCameraAxisSettings_Statics; \
	VIRTUALCAMERA_API static class UScriptStruct* StaticStruct();


template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<struct FVirtualCameraAxisSettings>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVirtualCameraSaveGame(); \
	friend struct Z_Construct_UClass_UVirtualCameraSaveGame_Statics; \
public: \
	DECLARE_CLASS(UVirtualCameraSaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVirtualCameraSaveGame)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_INCLASS \
private: \
	static void StaticRegisterNativesUVirtualCameraSaveGame(); \
	friend struct Z_Construct_UClass_UVirtualCameraSaveGame_Statics; \
public: \
	DECLARE_CLASS(UVirtualCameraSaveGame, USaveGame, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVirtualCameraSaveGame)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraSaveGame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualCameraSaveGame) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraSaveGame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraSaveGame); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraSaveGame(UVirtualCameraSaveGame&&); \
	NO_API UVirtualCameraSaveGame(const UVirtualCameraSaveGame&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraSaveGame(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraSaveGame(UVirtualCameraSaveGame&&); \
	NO_API UVirtualCameraSaveGame(const UVirtualCameraSaveGame&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraSaveGame); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraSaveGame); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualCameraSaveGame)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_312_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h_315_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VirtualCameraSaveGame."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class UVirtualCameraSaveGame>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraSaveGame_h


#define FOREACH_ENUM_EVIRTUALCAMERAAXIS(op) \
	op(EVirtualCameraAxis::LocationX) \
	op(EVirtualCameraAxis::LocationY) \
	op(EVirtualCameraAxis::LocationZ) \
	op(EVirtualCameraAxis::RotationX) \
	op(EVirtualCameraAxis::RotationY) \
	op(EVirtualCameraAxis::RotationZ) 

enum class EVirtualCameraAxis : uint8;
template<> VIRTUALCAMERA_API UEnum* StaticEnum<EVirtualCameraAxis>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
