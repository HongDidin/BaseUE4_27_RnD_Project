// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Private/ConcertVirtualCamera.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConcertVirtualCamera() {}
// Cross Module References
	VIRTUALCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	VIRTUALCAMERA_API UEnum* Z_Construct_UEnum_VirtualCamera_ETrackerInputSource();
	VIRTUALCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData();
	VIRTUALCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FRotator();
	VIRTUALCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData();
	CINEMATICCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FCameraLensSettings();
	CINEMATICCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FCameraFilmbackSettings();
// End Cross Module References
class UScriptStruct* FConcertVirtualCameraControllerEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VIRTUALCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent, Z_Construct_UPackage__Script_VirtualCamera(), TEXT("ConcertVirtualCameraControllerEvent"), sizeof(FConcertVirtualCameraControllerEvent), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Hash());
	}
	return Singleton;
}
template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<FConcertVirtualCameraControllerEvent>()
{
	return FConcertVirtualCameraControllerEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertVirtualCameraControllerEvent(FConcertVirtualCameraControllerEvent::StaticStruct, TEXT("/Script/VirtualCamera"), TEXT("ConcertVirtualCameraControllerEvent"), false, nullptr, nullptr);
static struct FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraControllerEvent
{
	FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraControllerEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertVirtualCameraControllerEvent>(FName(TEXT("ConcertVirtualCameraControllerEvent")));
	}
} ScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraControllerEvent;
	struct Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InputSource_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InputSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InputSource;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertVirtualCameraControllerEvent>();
	}
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_InputSource_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_InputSource_MetaData[] = {
		{ "Comment", "/** Controller settings */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
		{ "ToolTip", "Controller settings" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_InputSource = { "InputSource", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraControllerEvent, InputSource), Z_Construct_UEnum_VirtualCamera_ETrackerInputSource, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_InputSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_InputSource_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_CameraData_MetaData[] = {
		{ "Comment", "/** Camera data */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
		{ "ToolTip", "Camera data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_CameraData = { "CameraData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraControllerEvent, CameraData), Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_CameraData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_CameraData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_InputSource_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_InputSource,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::NewProp_CameraData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
		nullptr,
		&NewStructOps,
		"ConcertVirtualCameraControllerEvent",
		sizeof(FConcertVirtualCameraControllerEvent),
		alignof(FConcertVirtualCameraControllerEvent),
		Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VirtualCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertVirtualCameraControllerEvent"), sizeof(FConcertVirtualCameraControllerEvent), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraControllerEvent_Hash() { return 2207747874U; }
class UScriptStruct* FConcertVirtualCameraCameraComponentEvent::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VIRTUALCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent, Z_Construct_UPackage__Script_VirtualCamera(), TEXT("ConcertVirtualCameraCameraComponentEvent"), sizeof(FConcertVirtualCameraCameraComponentEvent), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Hash());
	}
	return Singleton;
}
template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<FConcertVirtualCameraCameraComponentEvent>()
{
	return FConcertVirtualCameraCameraComponentEvent::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent(FConcertVirtualCameraCameraComponentEvent::StaticStruct, TEXT("/Script/VirtualCamera"), TEXT("ConcertVirtualCameraCameraComponentEvent"), false, nullptr, nullptr);
static struct FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraComponentEvent
{
	FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraComponentEvent()
	{
		UScriptStruct::DeferCppStructOps<FConcertVirtualCameraCameraComponentEvent>(FName(TEXT("ConcertVirtualCameraCameraComponentEvent")));
	}
} ScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraComponentEvent;
	struct Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackingName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_TrackingName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertVirtualCameraCameraComponentEvent>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_TrackingName_MetaData[] = {
		{ "Comment", "/** Name of the tracking camera */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
		{ "ToolTip", "Name of the tracking camera" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_TrackingName = { "TrackingName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraComponentEvent, TrackingName), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_TrackingName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_TrackingName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_CameraData_MetaData[] = {
		{ "Comment", "/** Camera data */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
		{ "ToolTip", "Camera data" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_CameraData = { "CameraData", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraComponentEvent, CameraData), Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_CameraData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_CameraData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_TrackingName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::NewProp_CameraData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
		nullptr,
		&NewStructOps,
		"ConcertVirtualCameraCameraComponentEvent",
		sizeof(FConcertVirtualCameraCameraComponentEvent),
		alignof(FConcertVirtualCameraCameraComponentEvent),
		Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VirtualCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertVirtualCameraCameraComponentEvent"), sizeof(FConcertVirtualCameraCameraComponentEvent), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraComponentEvent_Hash() { return 1946868413U; }
class UScriptStruct* FConcertVirtualCameraCameraData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VIRTUALCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData, Z_Construct_UPackage__Script_VirtualCamera(), TEXT("ConcertVirtualCameraCameraData"), sizeof(FConcertVirtualCameraCameraData), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Hash());
	}
	return Singleton;
}
template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<FConcertVirtualCameraCameraData>()
{
	return FConcertVirtualCameraCameraData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertVirtualCameraCameraData(FConcertVirtualCameraCameraData::StaticStruct, TEXT("/Script/VirtualCamera"), TEXT("ConcertVirtualCameraCameraData"), false, nullptr, nullptr);
static struct FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraData
{
	FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraData()
	{
		UScriptStruct::DeferCppStructOps<FConcertVirtualCameraCameraData>(FName(TEXT("ConcertVirtualCameraCameraData")));
	}
} ScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraData;
	struct Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraActorLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraActorLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraActorRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraActorRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponentLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraComponentLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponentRotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraComponentRotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentAperture_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentAperture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentFocalLength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CurrentFocalLength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FocusSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LensSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LensSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilmbackSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilmbackSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertVirtualCameraCameraData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorLocation_MetaData[] = {
		{ "Comment", "/** Camera transform */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
		{ "ToolTip", "Camera transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorLocation = { "CameraActorLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, CameraActorLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorRotation_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorRotation = { "CameraActorRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, CameraActorRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentLocation_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentLocation = { "CameraComponentLocation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, CameraComponentLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentRotation_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentRotation = { "CameraComponentRotation", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, CameraComponentRotation), Z_Construct_UScriptStruct_FRotator, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentRotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentRotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentAperture_MetaData[] = {
		{ "Comment", "/** Camera settings */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
		{ "ToolTip", "Camera settings" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentAperture = { "CurrentAperture", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, CurrentAperture), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentAperture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentAperture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentFocalLength_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentFocalLength = { "CurrentFocalLength", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, CurrentFocalLength), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentFocalLength_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentFocalLength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FocusSettings_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FocusSettings = { "FocusSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, FocusSettings), Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FocusSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FocusSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_LensSettings_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_LensSettings = { "LensSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, LensSettings), Z_Construct_UScriptStruct_FCameraLensSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_LensSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_LensSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FilmbackSettings_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FilmbackSettings = { "FilmbackSettings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraData, FilmbackSettings), Z_Construct_UScriptStruct_FCameraFilmbackSettings, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FilmbackSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FilmbackSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraActorRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CameraComponentRotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentAperture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_CurrentFocalLength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FocusSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_LensSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::NewProp_FilmbackSettings,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
		nullptr,
		&NewStructOps,
		"ConcertVirtualCameraCameraData",
		sizeof(FConcertVirtualCameraCameraData),
		alignof(FConcertVirtualCameraCameraData),
		Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VirtualCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertVirtualCameraCameraData"), sizeof(FConcertVirtualCameraCameraData), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraData_Hash() { return 480917555U; }
class UScriptStruct* FConcertVirtualCameraCameraFocusData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern VIRTUALCAMERA_API uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData, Z_Construct_UPackage__Script_VirtualCamera(), TEXT("ConcertVirtualCameraCameraFocusData"), sizeof(FConcertVirtualCameraCameraFocusData), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Hash());
	}
	return Singleton;
}
template<> VIRTUALCAMERA_API UScriptStruct* StaticStruct<FConcertVirtualCameraCameraFocusData>()
{
	return FConcertVirtualCameraCameraFocusData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FConcertVirtualCameraCameraFocusData(FConcertVirtualCameraCameraFocusData::StaticStruct, TEXT("/Script/VirtualCamera"), TEXT("ConcertVirtualCameraCameraFocusData"), false, nullptr, nullptr);
static struct FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraFocusData
{
	FScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraFocusData()
	{
		UScriptStruct::DeferCppStructOps<FConcertVirtualCameraCameraFocusData>(FName(TEXT("ConcertVirtualCameraCameraFocusData")));
	}
} ScriptStruct_VirtualCamera_StaticRegisterNativesFConcertVirtualCameraCameraFocusData;
	struct Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ManualFocusDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ManualFocusDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FocusSmoothingInterpSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FocusSmoothingInterpSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSmoothFocusChanges_MetaData[];
#endif
		static void NewProp_bSmoothFocusChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSmoothFocusChanges;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::Struct_MetaDataParams[] = {
		{ "Comment", "/**\n * Can't use FCameraFocusSettings since it use a reference to an actor\n * The camera will always be in Manual, and will transfer the CurrentFocusDistance.\n */" },
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
		{ "ToolTip", "Can't use FCameraFocusSettings since it use a reference to an actor\nThe camera will always be in Manual, and will transfer the CurrentFocusDistance." },
	};
#endif
	void* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FConcertVirtualCameraCameraFocusData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_ManualFocusDistance_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_ManualFocusDistance = { "ManualFocusDistance", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraFocusData, ManualFocusDistance), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_ManualFocusDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_ManualFocusDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed = { "FocusSmoothingInterpSpeed", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FConcertVirtualCameraCameraFocusData, FocusSmoothingInterpSpeed), METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_bSmoothFocusChanges_MetaData[] = {
		{ "ModuleRelativePath", "Private/ConcertVirtualCamera.h" },
	};
#endif
	void Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_bSmoothFocusChanges_SetBit(void* Obj)
	{
		((FConcertVirtualCameraCameraFocusData*)Obj)->bSmoothFocusChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_bSmoothFocusChanges = { "bSmoothFocusChanges", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FConcertVirtualCameraCameraFocusData), &Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_bSmoothFocusChanges_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_bSmoothFocusChanges_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_bSmoothFocusChanges_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_ManualFocusDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_FocusSmoothingInterpSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::NewProp_bSmoothFocusChanges,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
		nullptr,
		&NewStructOps,
		"ConcertVirtualCameraCameraFocusData",
		sizeof(FConcertVirtualCameraCameraFocusData),
		alignof(FConcertVirtualCameraCameraFocusData),
		Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_VirtualCamera();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("ConcertVirtualCameraCameraFocusData"), sizeof(FConcertVirtualCameraCameraFocusData), Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FConcertVirtualCameraCameraFocusData_Hash() { return 4245359147U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
