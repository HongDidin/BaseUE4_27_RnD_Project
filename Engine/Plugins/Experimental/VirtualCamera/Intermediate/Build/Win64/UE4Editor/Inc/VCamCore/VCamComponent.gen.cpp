// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamComponent() {}
// Cross Module References
	VCAMCORE_API UFunction* Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamComponent_NoRegister();
	VCAMCORE_API UEnum* Z_Construct_UEnum_VCamCore_EVCamTargetViewportID();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifier_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputProviderBase_NoRegister();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraBlueprintData();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifierContext_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	GAMEPLAYTAGS_API UScriptStruct* Z_Construct_UScriptStruct_FGameplayTag();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkSubjectName();
	VCAMCORE_API UScriptStruct* Z_Construct_UScriptStruct_FModifierStackEntry();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics
	{
		struct _Script_VCamCore_eventOnComponentReplaced_Parms
		{
			UVCamComponent* NewComponent;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NewComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::NewProp_NewComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::NewProp_NewComponent = { "NewComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_VCamCore_eventOnComponentReplaced_Parms, NewComponent), Z_Construct_UClass_UVCamComponent_NoRegister, METADATA_PARAMS(Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::NewProp_NewComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::NewProp_NewComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::NewProp_NewComponent,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VCamCore, nullptr, "OnComponentReplaced__DelegateSignature", nullptr, nullptr, sizeof(_Script_VCamCore_eventOnComponentReplaced_Parms), Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	static UEnum* EVCamTargetViewportID_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_VCamCore_EVCamTargetViewportID, Z_Construct_UPackage__Script_VCamCore(), TEXT("EVCamTargetViewportID"));
		}
		return Singleton;
	}
	template<> VCAMCORE_API UEnum* StaticEnum<EVCamTargetViewportID>()
	{
		return EVCamTargetViewportID_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVCamTargetViewportID(EVCamTargetViewportID_StaticEnum, TEXT("/Script/VCamCore"), TEXT("EVCamTargetViewportID"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_VCamCore_EVCamTargetViewportID_Hash() { return 1430688349U; }
	UEnum* Z_Construct_UEnum_VCamCore_EVCamTargetViewportID()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_VCamCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVCamTargetViewportID"), 0, Get_Z_Construct_UEnum_VCamCore_EVCamTargetViewportID_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVCamTargetViewportID::CurrentlySelected", (int64)EVCamTargetViewportID::CurrentlySelected },
				{ "EVCamTargetViewportID::Viewport1", (int64)EVCamTargetViewportID::Viewport1 },
				{ "EVCamTargetViewportID::Viewport2", (int64)EVCamTargetViewportID::Viewport2 },
				{ "EVCamTargetViewportID::Viewport3", (int64)EVCamTargetViewportID::Viewport3 },
				{ "EVCamTargetViewportID::Viewport4", (int64)EVCamTargetViewportID::Viewport4 },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "CurrentlySelected.Name", "EVCamTargetViewportID::CurrentlySelected" },
				{ "DisplayName", "VCam Target Viewport ID" },
				{ "ModuleRelativePath", "Public/VCamComponent.h" },
				{ "Viewport1.Name", "EVCamTargetViewportID::Viewport1" },
				{ "Viewport2.Name", "EVCamTargetViewportID::Viewport2" },
				{ "Viewport3.Name", "EVCamTargetViewportID::Viewport3" },
				{ "Viewport4.Name", "EVCamTargetViewportID::Viewport4" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_VCamCore,
				nullptr,
				"EVCamTargetViewportID",
				"EVCamTargetViewportID",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetLiveLinkDataForCurrentFrame)
	{
		P_GET_STRUCT_REF(FLiveLinkCameraBlueprintData,Z_Param_Out_LiveLinkData);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetLiveLinkDataForCurrentFrame(Z_Param_Out_LiveLinkData);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetOutputProvidersByClass)
	{
		P_GET_OBJECT(UClass,Z_Param_ProviderClass);
		P_GET_TARRAY_REF(UVCamOutputProviderBase*,Z_Param_Out_FoundProviders);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetOutputProvidersByClass(Z_Param_ProviderClass,Z_Param_Out_FoundProviders);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetOutputProviderByIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_ProviderIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UVCamOutputProviderBase**)Z_Param__Result=P_THIS->GetOutputProviderByIndex(Z_Param_ProviderIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetAllOutputProviders)
	{
		P_GET_TARRAY_REF(UVCamOutputProviderBase*,Z_Param_Out_Providers);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAllOutputProviders(Z_Param_Out_Providers);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetNumberOfOutputProviders)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetNumberOfOutputProviders();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execRemoveOutputProviderByIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_ProviderIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveOutputProviderByIndex(Z_Param_ProviderIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execRemoveOutputProvider)
	{
		P_GET_OBJECT(UVCamOutputProviderBase,Z_Param_Provider);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveOutputProvider(Z_Param_Provider);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execRemoveAllOutputProviders)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveAllOutputProviders();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execSetOutputProviderIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_OriginalIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_NewIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetOutputProviderIndex(Z_Param_OriginalIndex,Z_Param_NewIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execInsertOutputProvider)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_GET_OBJECT(UClass,Z_Param_ProviderClass);
		P_GET_OBJECT_REF(UVCamOutputProviderBase,Z_Param_Out_CreatedProvider);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->InsertOutputProvider(Z_Param_Index,Z_Param_ProviderClass,Z_Param_Out_CreatedProvider);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execAddOutputProvider)
	{
		P_GET_OBJECT(UClass,Z_Param_ProviderClass);
		P_GET_OBJECT_REF(UVCamOutputProviderBase,Z_Param_Out_CreatedProvider);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->AddOutputProvider(Z_Param_ProviderClass,Z_Param_Out_CreatedProvider);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetModifierContext)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UVCamModifierContext**)Z_Param__Result=P_THIS->GetModifierContext();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execSetModifierContextClass)
	{
		P_GET_OBJECT(UClass,Z_Param_ContextClass);
		P_GET_OBJECT_REF(UVCamModifierContext,Z_Param_Out_CreatedContext);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetModifierContextClass(Z_Param_ContextClass,Z_Param_Out_CreatedContext);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetModifiersByInterface)
	{
		P_GET_OBJECT(UClass,Z_Param_InterfaceClass);
		P_GET_TARRAY_REF(UVCamModifier*,Z_Param_Out_FoundModifiers);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetModifiersByInterface(Z_Param_InterfaceClass,Z_Param_Out_FoundModifiers);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetModifiersByClass)
	{
		P_GET_OBJECT(UClass,Z_Param_ModifierClass);
		P_GET_TARRAY_REF(UVCamModifier*,Z_Param_Out_FoundModifiers);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetModifiersByClass(Z_Param_ModifierClass,Z_Param_Out_FoundModifiers);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetModifierByName)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UVCamModifier**)Z_Param__Result=P_THIS->GetModifierByName(Z_Param_Name);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetModifierByIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UVCamModifier**)Z_Param__Result=P_THIS->GetModifierByIndex(Z_Param_Index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetAllModifiers)
	{
		P_GET_TARRAY_REF(UVCamModifier*,Z_Param_Out_Modifiers);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetAllModifiers(Z_Param_Out_Modifiers);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetNumberOfModifiers)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetNumberOfModifiers();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execRemoveModifierByName)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveModifierByName(Z_Param_Name);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execRemoveModifierByIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_ModifierIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveModifierByIndex(Z_Param_ModifierIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execRemoveModifier)
	{
		P_GET_OBJECT(UVCamModifier,Z_Param_Modifier);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->RemoveModifier(Z_Param_Modifier);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execRemoveAllModifiers)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RemoveAllModifiers();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execSetModifierIndex)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_OriginalIndex);
		P_GET_PROPERTY(FIntProperty,Z_Param_NewIndex);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->SetModifierIndex(Z_Param_OriginalIndex,Z_Param_NewIndex);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execInsertModifier)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_GET_OBJECT(UClass,Z_Param_ModifierClass);
		P_GET_OBJECT_REF(UVCamModifier,Z_Param_Out_CreatedModifier);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->InsertModifier(Z_Param_Name,Z_Param_Index,Z_Param_ModifierClass,Z_Param_Out_CreatedModifier);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execAddModifier)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_Name);
		P_GET_OBJECT(UClass,Z_Param_ModifierClass);
		P_GET_OBJECT_REF(UVCamModifier,Z_Param_Out_CreatedModifier);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->AddModifier(Z_Param_Name,Z_Param_ModifierClass,Z_Param_Out_CreatedModifier);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execGetTargetCamera)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UCineCameraComponent**)Z_Param__Result=P_THIS->GetTargetCamera();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execIsEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execSetEnabled)
	{
		P_GET_UBOOL(Z_Param_bNewEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnabled(Z_Param_bNewEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamComponent::execHandleObjectReplaced)
	{
		P_GET_TMAP_REF(UObject*,UObject*,Z_Param_Out_ReplacementMap);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleObjectReplaced(Z_Param_Out_ReplacementMap);
		P_NATIVE_END;
	}
	void UVCamComponent::StaticRegisterNativesUVCamComponent()
	{
		UClass* Class = UVCamComponent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddModifier", &UVCamComponent::execAddModifier },
			{ "AddOutputProvider", &UVCamComponent::execAddOutputProvider },
			{ "GetAllModifiers", &UVCamComponent::execGetAllModifiers },
			{ "GetAllOutputProviders", &UVCamComponent::execGetAllOutputProviders },
			{ "GetLiveLinkDataForCurrentFrame", &UVCamComponent::execGetLiveLinkDataForCurrentFrame },
			{ "GetModifierByIndex", &UVCamComponent::execGetModifierByIndex },
			{ "GetModifierByName", &UVCamComponent::execGetModifierByName },
			{ "GetModifierContext", &UVCamComponent::execGetModifierContext },
			{ "GetModifiersByClass", &UVCamComponent::execGetModifiersByClass },
			{ "GetModifiersByInterface", &UVCamComponent::execGetModifiersByInterface },
			{ "GetNumberOfModifiers", &UVCamComponent::execGetNumberOfModifiers },
			{ "GetNumberOfOutputProviders", &UVCamComponent::execGetNumberOfOutputProviders },
			{ "GetOutputProviderByIndex", &UVCamComponent::execGetOutputProviderByIndex },
			{ "GetOutputProvidersByClass", &UVCamComponent::execGetOutputProvidersByClass },
			{ "GetTargetCamera", &UVCamComponent::execGetTargetCamera },
			{ "HandleObjectReplaced", &UVCamComponent::execHandleObjectReplaced },
			{ "InsertModifier", &UVCamComponent::execInsertModifier },
			{ "InsertOutputProvider", &UVCamComponent::execInsertOutputProvider },
			{ "IsEnabled", &UVCamComponent::execIsEnabled },
			{ "RemoveAllModifiers", &UVCamComponent::execRemoveAllModifiers },
			{ "RemoveAllOutputProviders", &UVCamComponent::execRemoveAllOutputProviders },
			{ "RemoveModifier", &UVCamComponent::execRemoveModifier },
			{ "RemoveModifierByIndex", &UVCamComponent::execRemoveModifierByIndex },
			{ "RemoveModifierByName", &UVCamComponent::execRemoveModifierByName },
			{ "RemoveOutputProvider", &UVCamComponent::execRemoveOutputProvider },
			{ "RemoveOutputProviderByIndex", &UVCamComponent::execRemoveOutputProviderByIndex },
			{ "SetEnabled", &UVCamComponent::execSetEnabled },
			{ "SetModifierContextClass", &UVCamComponent::execSetModifierContextClass },
			{ "SetModifierIndex", &UVCamComponent::execSetModifierIndex },
			{ "SetOutputProviderIndex", &UVCamComponent::execSetOutputProviderIndex },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVCamComponent_AddModifier_Statics
	{
		struct VCamComponent_eventAddModifier_Parms
		{
			FName Name;
			TSubclassOf<UVCamModifier>  ModifierClass;
			UVCamModifier* CreatedModifier;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifierClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ModifierClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CreatedModifier;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventAddModifier_Parms, Name), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_Name_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ModifierClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ModifierClass = { "ModifierClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventAddModifier_Parms, ModifierClass), Z_Construct_UClass_UVCamModifier_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ModifierClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ModifierClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_CreatedModifier = { "CreatedModifier", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventAddModifier_Parms, CreatedModifier), Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventAddModifier_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventAddModifier_Parms), &Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ModifierClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_CreatedModifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Add a modifier to the stack with a given name.\n// If that name is already in use then the modifier will not be added.\n// Returns the created modifier if the Add succeeded\n" },
		{ "DeterminesOutputType", "ModifierClass" },
		{ "DynamicOutputParam", "CreatedModifier" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ReturnDisplayName", "Success" },
		{ "ToolTip", "Add a modifier to the stack with a given name.\nIf that name is already in use then the modifier will not be added.\nReturns the created modifier if the Add succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "AddModifier", nullptr, nullptr, sizeof(VCamComponent_eventAddModifier_Parms), Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_AddModifier()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_AddModifier_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics
	{
		struct VCamComponent_eventAddOutputProvider_Parms
		{
			TSubclassOf<UVCamOutputProviderBase>  ProviderClass;
			UVCamOutputProviderBase* CreatedProvider;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ProviderClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CreatedProvider;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ProviderClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ProviderClass = { "ProviderClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventAddOutputProvider_Parms, ProviderClass), Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ProviderClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ProviderClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_CreatedProvider = { "CreatedProvider", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventAddOutputProvider_Parms, CreatedProvider), Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventAddOutputProvider_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventAddOutputProvider_Parms), &Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ProviderClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_CreatedProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Output Provider access\n" },
		{ "DeterminesOutputType", "ProviderClass" },
		{ "DynamicOutputParam", "CreatedProvider" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ReturnDisplayName", "Success" },
		{ "ToolTip", "Output Provider access" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "AddOutputProvider", nullptr, nullptr, sizeof(VCamComponent_eventAddOutputProvider_Parms), Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_AddOutputProvider()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_AddOutputProvider_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics
	{
		struct VCamComponent_eventGetAllModifiers_Parms
		{
			TArray<UVCamModifier*> Modifiers;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modifiers_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Modifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::NewProp_Modifiers_Inner = { "Modifiers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::NewProp_Modifiers = { "Modifiers", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetAllModifiers_Parms, Modifiers), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::NewProp_Modifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::NewProp_Modifiers,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Returns all the Modifiers in the Component's Stack\n// Note: It's possible not all Modifiers will be valid (such as if the user has not set a class for the modifier in the details panel)\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Returns all the Modifiers in the Component's Stack\nNote: It's possible not all Modifiers will be valid (such as if the user has not set a class for the modifier in the details panel)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetAllModifiers", nullptr, nullptr, sizeof(VCamComponent_eventGetAllModifiers_Parms), Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetAllModifiers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetAllModifiers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics
	{
		struct VCamComponent_eventGetAllOutputProviders_Parms
		{
			TArray<UVCamOutputProviderBase*> Providers;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Providers_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Providers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::NewProp_Providers_Inner = { "Providers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::NewProp_Providers = { "Providers", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetAllOutputProviders_Parms, Providers), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::NewProp_Providers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::NewProp_Providers,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetAllOutputProviders", nullptr, nullptr, sizeof(VCamComponent_eventGetAllOutputProviders_Parms), Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics
	{
		struct VCamComponent_eventGetLiveLinkDataForCurrentFrame_Parms
		{
			FLiveLinkCameraBlueprintData LiveLinkData;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LiveLinkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::NewProp_LiveLinkData = { "LiveLinkData", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetLiveLinkDataForCurrentFrame_Parms, LiveLinkData), Z_Construct_UScriptStruct_FLiveLinkCameraBlueprintData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::NewProp_LiveLinkData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetLiveLinkDataForCurrentFrame", nullptr, nullptr, sizeof(VCamComponent_eventGetLiveLinkDataForCurrentFrame_Parms), Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics
	{
		struct VCamComponent_eventGetModifierByIndex_Parms
		{
			int32 Index;
			UVCamModifier* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Index_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::NewProp_Index_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifierByIndex_Parms, Index), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::NewProp_Index_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::NewProp_Index_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifierByIndex_Parms, ReturnValue), Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Returns the Modifier in the Stack with the given index if it exist.\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Returns the Modifier in the Stack with the given index if it exist." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetModifierByIndex", nullptr, nullptr, sizeof(VCamComponent_eventGetModifierByIndex_Parms), Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetModifierByIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetModifierByIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics
	{
		struct VCamComponent_eventGetModifierByName_Parms
		{
			FName Name;
			UVCamModifier* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::NewProp_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifierByName_Parms, Name), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifierByName_Parms, ReturnValue), Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Tries to find a Modifier in the Stack with the given name.\n// The returned Modifier must be checked before it is used.\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Tries to find a Modifier in the Stack with the given name.\nThe returned Modifier must be checked before it is used." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetModifierByName", nullptr, nullptr, sizeof(VCamComponent_eventGetModifierByName_Parms), Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetModifierByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetModifierByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics
	{
		struct VCamComponent_eventGetModifierContext_Parms
		{
			UVCamModifierContext* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifierContext_Parms, ReturnValue), Z_Construct_UClass_UVCamModifierContext_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/*\n\x09Get the current Modifier Context\n\x09@return Current Context\n\x09*/" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Get the current Modifier Context\n@return Current Context" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetModifierContext", nullptr, nullptr, sizeof(VCamComponent_eventGetModifierContext_Parms), Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetModifierContext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetModifierContext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics
	{
		struct VCamComponent_eventGetModifiersByClass_Parms
		{
			TSubclassOf<UVCamModifier>  ModifierClass;
			TArray<UVCamModifier*> FoundModifiers;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifierClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ModifierClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FoundModifiers_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FoundModifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_ModifierClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_ModifierClass = { "ModifierClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifiersByClass_Parms, ModifierClass), Z_Construct_UClass_UVCamModifier_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_ModifierClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_ModifierClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_FoundModifiers_Inner = { "FoundModifiers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_FoundModifiers = { "FoundModifiers", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifiersByClass_Parms, FoundModifiers), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_ModifierClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_FoundModifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::NewProp_FoundModifiers,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Given a specific Modifier class, returns a list of matching Modifiers\n" },
		{ "DeterminesOutputType", "ModifierClass" },
		{ "DynamicOutputParam", "FoundModifiers" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Given a specific Modifier class, returns a list of matching Modifiers" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetModifiersByClass", nullptr, nullptr, sizeof(VCamComponent_eventGetModifiersByClass_Parms), Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetModifiersByClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetModifiersByClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics
	{
		struct VCamComponent_eventGetModifiersByInterface_Parms
		{
			TSubclassOf<UInterface>  InterfaceClass;
			TArray<UVCamModifier*> FoundModifiers;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InterfaceClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InterfaceClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FoundModifiers_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FoundModifiers;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_InterfaceClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_InterfaceClass = { "InterfaceClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifiersByInterface_Parms, InterfaceClass), Z_Construct_UClass_UInterface, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_InterfaceClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_InterfaceClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_FoundModifiers_Inner = { "FoundModifiers", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_FoundModifiers = { "FoundModifiers", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetModifiersByInterface_Parms, FoundModifiers), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_InterfaceClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_FoundModifiers_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::NewProp_FoundModifiers,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Given a specific Interface class, returns a list of matching Modifiers\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Given a specific Interface class, returns a list of matching Modifiers" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetModifiersByInterface", nullptr, nullptr, sizeof(VCamComponent_eventGetModifiersByInterface_Parms), Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics
	{
		struct VCamComponent_eventGetNumberOfModifiers_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetNumberOfModifiers_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Returns the number of Modifiers in the Component's Stack\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Returns the number of Modifiers in the Component's Stack" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetNumberOfModifiers", nullptr, nullptr, sizeof(VCamComponent_eventGetNumberOfModifiers_Parms), Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics
	{
		struct VCamComponent_eventGetNumberOfOutputProviders_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetNumberOfOutputProviders_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetNumberOfOutputProviders", nullptr, nullptr, sizeof(VCamComponent_eventGetNumberOfOutputProviders_Parms), Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics
	{
		struct VCamComponent_eventGetOutputProviderByIndex_Parms
		{
			int32 ProviderIndex;
			UVCamOutputProviderBase* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ProviderIndex;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::NewProp_ProviderIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::NewProp_ProviderIndex = { "ProviderIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetOutputProviderByIndex_Parms, ProviderIndex), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::NewProp_ProviderIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::NewProp_ProviderIndex_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetOutputProviderByIndex_Parms, ReturnValue), Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::NewProp_ProviderIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetOutputProviderByIndex", nullptr, nullptr, sizeof(VCamComponent_eventGetOutputProviderByIndex_Parms), Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics
	{
		struct VCamComponent_eventGetOutputProvidersByClass_Parms
		{
			TSubclassOf<UVCamOutputProviderBase>  ProviderClass;
			TArray<UVCamOutputProviderBase*> FoundProviders;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ProviderClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FoundProviders_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FoundProviders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_ProviderClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_ProviderClass = { "ProviderClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetOutputProvidersByClass_Parms, ProviderClass), Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_ProviderClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_ProviderClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_FoundProviders_Inner = { "FoundProviders", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_FoundProviders = { "FoundProviders", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetOutputProvidersByClass_Parms, FoundProviders), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_ProviderClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_FoundProviders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::NewProp_FoundProviders,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "DeterminesOutputType", "ProviderClass" },
		{ "DynamicOutputParam", "FoundProviders" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetOutputProvidersByClass", nullptr, nullptr, sizeof(VCamComponent_eventGetOutputProvidersByClass_Parms), Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics
	{
		struct VCamComponent_eventGetTargetCamera_Parms
		{
			UCineCameraComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventGetTargetCamera_Parms, ReturnValue), Z_Construct_UClass_UCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Returns the Target CineCameraComponent\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Returns the Target CineCameraComponent" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "GetTargetCamera", nullptr, nullptr, sizeof(VCamComponent_eventGetTargetCamera_Parms), Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_GetTargetCamera()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_GetTargetCamera_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics
	{
		struct VCamComponent_eventHandleObjectReplaced_Parms
		{
			TMap<UObject*,UObject*> ReplacementMap;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReplacementMap_ValueProp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReplacementMap_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReplacementMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReplacementMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap_ValueProp = { "ReplacementMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap_Key_KeyProp = { "ReplacementMap_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap = { "ReplacementMap", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventHandleObjectReplaced_Parms, ReplacementMap), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::NewProp_ReplacementMap,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "HandleObjectReplaced", nullptr, nullptr, sizeof(VCamComponent_eventHandleObjectReplaced_Parms), Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics
	{
		struct VCamComponent_eventInsertModifier_Parms
		{
			FName Name;
			int32 Index;
			TSubclassOf<UVCamModifier>  ModifierClass;
			UVCamModifier* CreatedModifier;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifierClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ModifierClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CreatedModifier;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventInsertModifier_Parms, Name), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventInsertModifier_Parms, Index), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ModifierClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ModifierClass = { "ModifierClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventInsertModifier_Parms, ModifierClass), Z_Construct_UClass_UVCamModifier_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ModifierClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ModifierClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_CreatedModifier = { "CreatedModifier", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventInsertModifier_Parms, CreatedModifier), Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventInsertModifier_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventInsertModifier_Parms), &Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ModifierClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_CreatedModifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Insert a modifier to the stack with a given name and index.\n// If that name is already in use then the modifier will not be added.\n// The index must be between zero and the number of existing modifiers inclusive\n// Returns the created modifier if the Add succeeded\n" },
		{ "DeterminesOutputType", "ModifierClass" },
		{ "DynamicOutputParam", "CreatedModifier" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ReturnDisplayName", "Success" },
		{ "ToolTip", "Insert a modifier to the stack with a given name and index.\nIf that name is already in use then the modifier will not be added.\nThe index must be between zero and the number of existing modifiers inclusive\nReturns the created modifier if the Add succeeded" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "InsertModifier", nullptr, nullptr, sizeof(VCamComponent_eventInsertModifier_Parms), Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_InsertModifier()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_InsertModifier_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics
	{
		struct VCamComponent_eventInsertOutputProvider_Parms
		{
			int32 Index;
			TSubclassOf<UVCamOutputProviderBase>  ProviderClass;
			UVCamOutputProviderBase* CreatedProvider;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ProviderClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CreatedProvider;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventInsertOutputProvider_Parms, Index), METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ProviderClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ProviderClass = { "ProviderClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventInsertOutputProvider_Parms, ProviderClass), Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ProviderClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ProviderClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_CreatedProvider = { "CreatedProvider", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventInsertOutputProvider_Parms, CreatedProvider), Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventInsertOutputProvider_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventInsertOutputProvider_Parms), &Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ProviderClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_CreatedProvider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "DeterminesOutputType", "ProviderClass" },
		{ "DynamicOutputParam", "CreatedProvider" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ReturnDisplayName", "Success" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "InsertOutputProvider", nullptr, nullptr, sizeof(VCamComponent_eventInsertOutputProvider_Parms), Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_InsertOutputProvider()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_InsertOutputProvider_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics
	{
		struct VCamComponent_eventIsEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventIsEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventIsEnabled_Parms), &Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::Function_MetaDataParams[] = {
		{ "BlueprintGetter", "" },
		{ "Comment", "// Returns whether or not the VCamComponent will update every frame\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Returns whether or not the VCamComponent will update every frame" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "IsEnabled", nullptr, nullptr, sizeof(VCamComponent_eventIsEnabled_Parms), Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_IsEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_IsEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Remove all Modifiers from the Stack.\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Remove all Modifiers from the Stack." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "RemoveAllModifiers", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Remove all Output Providers from the Component.\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Remove all Output Providers from the Component." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "RemoveAllOutputProviders", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics
	{
		struct VCamComponent_eventRemoveModifier_Parms
		{
			const UVCamModifier* Modifier;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Modifier_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Modifier;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_Modifier_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_Modifier = { "Modifier", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventRemoveModifier_Parms, Modifier), Z_Construct_UClass_UVCamModifier_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_Modifier_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_Modifier_MetaData)) };
	void Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventRemoveModifier_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventRemoveModifier_Parms), &Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_Modifier,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Remove the given Modifier from the Stack.\n// Returns true if the modifier was removed successfully\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Remove the given Modifier from the Stack.\nReturns true if the modifier was removed successfully" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "RemoveModifier", nullptr, nullptr, sizeof(VCamComponent_eventRemoveModifier_Parms), Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_RemoveModifier()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_RemoveModifier_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics
	{
		struct VCamComponent_eventRemoveModifierByIndex_Parms
		{
			int32 ModifierIndex;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifierIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ModifierIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ModifierIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ModifierIndex = { "ModifierIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventRemoveModifierByIndex_Parms, ModifierIndex), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ModifierIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ModifierIndex_MetaData)) };
	void Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventRemoveModifierByIndex_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventRemoveModifierByIndex_Parms), &Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ModifierIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Remove the Modifier at a specified index from the Stack.\n// Returns true if the modifier was removed successfully\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Remove the Modifier at a specified index from the Stack.\nReturns true if the modifier was removed successfully" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "RemoveModifierByIndex", nullptr, nullptr, sizeof(VCamComponent_eventRemoveModifierByIndex_Parms), Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics
	{
		struct VCamComponent_eventRemoveModifierByName_Parms
		{
			FName Name;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Name;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_Name_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventRemoveModifierByName_Parms, Name), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_Name_MetaData)) };
	void Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventRemoveModifierByName_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventRemoveModifierByName_Parms), &Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_Name,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Remove the Modifier with a specific name from the Stack.\n// Returns true if the modifier was removed successfully\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Remove the Modifier with a specific name from the Stack.\nReturns true if the modifier was removed successfully" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "RemoveModifierByName", nullptr, nullptr, sizeof(VCamComponent_eventRemoveModifierByName_Parms), Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_RemoveModifierByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_RemoveModifierByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics
	{
		struct VCamComponent_eventRemoveOutputProvider_Parms
		{
			const UVCamOutputProviderBase* Provider;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Provider_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Provider;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_Provider_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_Provider = { "Provider", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventRemoveOutputProvider_Parms, Provider), Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_Provider_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_Provider_MetaData)) };
	void Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventRemoveOutputProvider_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventRemoveOutputProvider_Parms), &Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_Provider,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "RemoveOutputProvider", nullptr, nullptr, sizeof(VCamComponent_eventRemoveOutputProvider_Parms), Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics
	{
		struct VCamComponent_eventRemoveOutputProviderByIndex_Parms
		{
			int32 ProviderIndex;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ProviderIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ProviderIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ProviderIndex_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ProviderIndex = { "ProviderIndex", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventRemoveOutputProviderByIndex_Parms, ProviderIndex), METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ProviderIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ProviderIndex_MetaData)) };
	void Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventRemoveOutputProviderByIndex_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventRemoveOutputProviderByIndex_Parms), &Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ProviderIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "RemoveOutputProviderByIndex", nullptr, nullptr, sizeof(VCamComponent_eventRemoveOutputProviderByIndex_Parms), Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics
	{
		struct VCamComponent_eventSetEnabled_Parms
		{
			bool bNewEnabled;
		};
		static void NewProp_bNewEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::NewProp_bNewEnabled_SetBit(void* Obj)
	{
		((VCamComponent_eventSetEnabled_Parms*)Obj)->bNewEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::NewProp_bNewEnabled = { "bNewEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventSetEnabled_Parms), &Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::NewProp_bNewEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::NewProp_bNewEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "Comment", "// Sets if the VCamComponent will update every frame or not\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Sets if the VCamComponent will update every frame or not" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "SetEnabled", nullptr, nullptr, sizeof(VCamComponent_eventSetEnabled_Parms), Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_SetEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_SetEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics
	{
		struct VCamComponent_eventSetModifierContextClass_Parms
		{
			TSubclassOf<UVCamModifierContext>  ContextClass;
			UVCamModifierContext* CreatedContext;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ContextClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ContextClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CreatedContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::NewProp_ContextClass_MetaData[] = {
		{ "AllowAbstract", "false" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::NewProp_ContextClass = { "ContextClass", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventSetModifierContextClass_Parms, ContextClass), Z_Construct_UClass_UVCamModifierContext_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::NewProp_ContextClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::NewProp_ContextClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::NewProp_CreatedContext = { "CreatedContext", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventSetModifierContextClass_Parms, CreatedContext), Z_Construct_UClass_UVCamModifierContext_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::NewProp_ContextClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::NewProp_CreatedContext,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::Function_MetaDataParams[] = {
		{ "AllowAbstract", "false" },
		{ "Category", "VirtualCamera" },
		{ "Comment", "/*\n\x09Sets the Modifier Context to a new instance of the provided class\n\x09@param ContextClass The Class to create the context from\n\x09@param CreatedContext The created Context, can be invalid if Context Class was None\n\x09*/" },
		{ "DeterminesOutputType", "ContextClass" },
		{ "DynamicOutputParam", "CreatedContext" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Sets the Modifier Context to a new instance of the provided class\n@param ContextClass The Class to create the context from\n@param CreatedContext The created Context, can be invalid if Context Class was None" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "SetModifierContextClass", nullptr, nullptr, sizeof(VCamComponent_eventSetModifierContextClass_Parms), Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_SetModifierContextClass()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_SetModifierContextClass_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics
	{
		struct VCamComponent_eventSetModifierIndex_Parms
		{
			int32 OriginalIndex;
			int32 NewIndex;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginalIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_OriginalIndex = { "OriginalIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventSetModifierIndex_Parms, OriginalIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_NewIndex = { "NewIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventSetModifierIndex_Parms, NewIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventSetModifierIndex_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventSetModifierIndex_Parms), &Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_OriginalIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_NewIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Moves an existing modifier in the stack from its current index to a new index\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ReturnDisplayName", "Success" },
		{ "ToolTip", "Moves an existing modifier in the stack from its current index to a new index" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "SetModifierIndex", nullptr, nullptr, sizeof(VCamComponent_eventSetModifierIndex_Parms), Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_SetModifierIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_SetModifierIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics
	{
		struct VCamComponent_eventSetOutputProviderIndex_Parms
		{
			int32 OriginalIndex;
			int32 NewIndex;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OriginalIndex;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewIndex;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_OriginalIndex = { "OriginalIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventSetOutputProviderIndex_Parms, OriginalIndex), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_NewIndex = { "NewIndex", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamComponent_eventSetOutputProviderIndex_Parms, NewIndex), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamComponent_eventSetOutputProviderIndex_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamComponent_eventSetOutputProviderIndex_Parms), &Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_OriginalIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_NewIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Moves an existing Output Provider in the stack from its current index to a new index\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ReturnDisplayName", "Success" },
		{ "ToolTip", "Moves an existing Output Provider in the stack from its current index to a new index" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamComponent, nullptr, "SetOutputProviderIndex", nullptr, nullptr, sizeof(VCamComponent_eventSetOutputProviderIndex_Parms), Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVCamComponent_NoRegister()
	{
		return UVCamComponent::StaticClass();
	}
	struct Z_Construct_UClass_UVCamComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnComponentReplaced_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnComponentReplaced;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Role_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Role;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LiveLinkSubject_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LiveLinkSubject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bLockViewportToCamera_MetaData[];
#endif
		static void NewProp_bLockViewportToCamera_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLockViewportToCamera;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisableComponentWhenSpawnedBySequencer_MetaData[];
#endif
		static void NewProp_bDisableComponentWhenSpawnedBySequencer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableComponentWhenSpawnedBySequencer;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDisableOutputOnMultiUserReceiver_MetaData[];
#endif
		static void NewProp_bDisableOutputOnMultiUserReceiver_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDisableOutputOnMultiUserReceiver;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UpdateFrequencyMs_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UpdateFrequencyMs;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TargetViewport_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetViewport_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TargetViewport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputProviders_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutputProviders_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputProviders_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_OutputProviders;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifierContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ModifierContext;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ModifierStack_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModifierStack_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ModifierStack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsLockedToViewport_MetaData[];
#endif
		static void NewProp_bIsLockedToViewport_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsLockedToViewport;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVCamComponent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVCamComponent_AddModifier, "AddModifier" }, // 3896360694
		{ &Z_Construct_UFunction_UVCamComponent_AddOutputProvider, "AddOutputProvider" }, // 1437995696
		{ &Z_Construct_UFunction_UVCamComponent_GetAllModifiers, "GetAllModifiers" }, // 3244884904
		{ &Z_Construct_UFunction_UVCamComponent_GetAllOutputProviders, "GetAllOutputProviders" }, // 1851084238
		{ &Z_Construct_UFunction_UVCamComponent_GetLiveLinkDataForCurrentFrame, "GetLiveLinkDataForCurrentFrame" }, // 186099253
		{ &Z_Construct_UFunction_UVCamComponent_GetModifierByIndex, "GetModifierByIndex" }, // 3639239489
		{ &Z_Construct_UFunction_UVCamComponent_GetModifierByName, "GetModifierByName" }, // 1175297442
		{ &Z_Construct_UFunction_UVCamComponent_GetModifierContext, "GetModifierContext" }, // 945624566
		{ &Z_Construct_UFunction_UVCamComponent_GetModifiersByClass, "GetModifiersByClass" }, // 895135810
		{ &Z_Construct_UFunction_UVCamComponent_GetModifiersByInterface, "GetModifiersByInterface" }, // 2264188916
		{ &Z_Construct_UFunction_UVCamComponent_GetNumberOfModifiers, "GetNumberOfModifiers" }, // 2010732526
		{ &Z_Construct_UFunction_UVCamComponent_GetNumberOfOutputProviders, "GetNumberOfOutputProviders" }, // 296994756
		{ &Z_Construct_UFunction_UVCamComponent_GetOutputProviderByIndex, "GetOutputProviderByIndex" }, // 1333900435
		{ &Z_Construct_UFunction_UVCamComponent_GetOutputProvidersByClass, "GetOutputProvidersByClass" }, // 3785075070
		{ &Z_Construct_UFunction_UVCamComponent_GetTargetCamera, "GetTargetCamera" }, // 3587444308
		{ &Z_Construct_UFunction_UVCamComponent_HandleObjectReplaced, "HandleObjectReplaced" }, // 3238889463
		{ &Z_Construct_UFunction_UVCamComponent_InsertModifier, "InsertModifier" }, // 2592349124
		{ &Z_Construct_UFunction_UVCamComponent_InsertOutputProvider, "InsertOutputProvider" }, // 124585947
		{ &Z_Construct_UFunction_UVCamComponent_IsEnabled, "IsEnabled" }, // 2786105230
		{ &Z_Construct_UFunction_UVCamComponent_RemoveAllModifiers, "RemoveAllModifiers" }, // 583521230
		{ &Z_Construct_UFunction_UVCamComponent_RemoveAllOutputProviders, "RemoveAllOutputProviders" }, // 2161880159
		{ &Z_Construct_UFunction_UVCamComponent_RemoveModifier, "RemoveModifier" }, // 3297265906
		{ &Z_Construct_UFunction_UVCamComponent_RemoveModifierByIndex, "RemoveModifierByIndex" }, // 848415220
		{ &Z_Construct_UFunction_UVCamComponent_RemoveModifierByName, "RemoveModifierByName" }, // 374454992
		{ &Z_Construct_UFunction_UVCamComponent_RemoveOutputProvider, "RemoveOutputProvider" }, // 952079117
		{ &Z_Construct_UFunction_UVCamComponent_RemoveOutputProviderByIndex, "RemoveOutputProviderByIndex" }, // 812847226
		{ &Z_Construct_UFunction_UVCamComponent_SetEnabled, "SetEnabled" }, // 1178461010
		{ &Z_Construct_UFunction_UVCamComponent_SetModifierContextClass, "SetModifierContextClass" }, // 1826513965
		{ &Z_Construct_UFunction_UVCamComponent_SetModifierIndex, "SetModifierIndex" }, // 2937022343
		{ &Z_Construct_UFunction_UVCamComponent_SetOutputProviderIndex, "SetOutputProviderIndex" }, // 2424160022
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintSpawnableComponent", "" },
		{ "BlueprintType", "true" },
		{ "ClassGroupNames", "VCam" },
		{ "HideCategories", "Trigger PhysicsVolume" },
		{ "IncludePath", "VCamComponent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_OnComponentReplaced_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// There are situations in the editor where the component may be replaced by another component as part of the actor being reconstructed\n// This event will notify you of that change and give you a reference to the new component. \n// Bindings will be copied to the new component so you do not need to rebind this event\n// \n// Note: When the component is replaced you will need to get all properties on the component again such as Modifiers and Output Providers\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "There are situations in the editor where the component may be replaced by another component as part of the actor being reconstructed\nThis event will notify you of that change and give you a reference to the new component.\nBindings will be copied to the new component so you do not need to rebind this event\n\nNote: When the component is replaced you will need to get all properties on the component again such as Modifiers and Output Providers" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_OnComponentReplaced = { "OnComponentReplaced", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, OnComponentReplaced), Z_Construct_UDelegateFunction_VCamCore_OnComponentReplaced__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_OnComponentReplaced_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_OnComponentReplaced_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_bEnabled_MetaData[] = {
		{ "BlueprintGetter", "IsEnabled" },
		{ "BlueprintSetter", "SetEnabled" },
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Enabled state of the component\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Enabled state of the component" },
	};
#endif
	void Z_Construct_UClass_UVCamComponent_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((UVCamComponent*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0040000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVCamComponent), &Z_Construct_UClass_UVCamComponent_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_Role_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/**\n\x09 * The role of this virtual camera.  If this value is set and the corresponding tag set on the editor matches this value, then this\n\x09 * camera is the sender and the authority in the case when connected to a multi-user session.\n\x09 */" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "The role of this virtual camera.  If this value is set and the corresponding tag set on the editor matches this value, then this\ncamera is the sender and the authority in the case when connected to a multi-user session." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_Role = { "Role", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, Role), Z_Construct_UScriptStruct_FGameplayTag, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_Role_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_Role_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_LiveLinkSubject_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// LiveLink subject name for the incoming camera transform\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "LiveLink subject name for the incoming camera transform" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_LiveLinkSubject = { "LiveLinkSubject", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, LiveLinkSubject), Z_Construct_UScriptStruct_FLiveLinkSubjectName, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_LiveLinkSubject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_LiveLinkSubject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_bLockViewportToCamera_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// If true, render the viewport from the point of view of the parented CineCamera\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "If true, render the viewport from the point of view of the parented CineCamera" },
	};
#endif
	void Z_Construct_UClass_UVCamComponent_Statics::NewProp_bLockViewportToCamera_SetBit(void* Obj)
	{
		((UVCamComponent*)Obj)->bLockViewportToCamera = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_bLockViewportToCamera = { "bLockViewportToCamera", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVCamComponent), &Z_Construct_UClass_UVCamComponent_Statics::NewProp_bLockViewportToCamera_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bLockViewportToCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bLockViewportToCamera_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableComponentWhenSpawnedBySequencer_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// If true, the component will force bEnabled to false when it is part of a spawnable in Sequencer\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "If true, the component will force bEnabled to false when it is part of a spawnable in Sequencer" },
	};
#endif
	void Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableComponentWhenSpawnedBySequencer_SetBit(void* Obj)
	{
		((UVCamComponent*)Obj)->bDisableComponentWhenSpawnedBySequencer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableComponentWhenSpawnedBySequencer = { "bDisableComponentWhenSpawnedBySequencer", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVCamComponent), &Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableComponentWhenSpawnedBySequencer_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableComponentWhenSpawnedBySequencer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableComponentWhenSpawnedBySequencer_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableOutputOnMultiUserReceiver_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Do we disable the output if the virtual camera is in a Multi-user session and the camera is a \"receiver\" from multi-user */" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Do we disable the output if the virtual camera is in a Multi-user session and the camera is a \"receiver\" from multi-user" },
	};
#endif
	void Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableOutputOnMultiUserReceiver_SetBit(void* Obj)
	{
		((UVCamComponent*)Obj)->bDisableOutputOnMultiUserReceiver = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableOutputOnMultiUserReceiver = { "bDisableOutputOnMultiUserReceiver", nullptr, (EPropertyFlags)0x0010040000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVCamComponent), &Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableOutputOnMultiUserReceiver_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableOutputOnMultiUserReceiver_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableOutputOnMultiUserReceiver_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_UpdateFrequencyMs_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "ClampMin", "30.0" },
		{ "Comment", "/** Indicates the frequency which camera updates are sent when in Multi-user mode. This has a minimum value of 30ms. */" },
		{ "DisplayName", "Update Frequencey" },
		{ "ForceUnits", "ms" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Indicates the frequency which camera updates are sent when in Multi-user mode. This has a minimum value of 30ms." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_UpdateFrequencyMs = { "UpdateFrequencyMs", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, UpdateFrequencyMs), METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_UpdateFrequencyMs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_UpdateFrequencyMs_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_TargetViewport_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_TargetViewport_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Which viewport to use for this VCam\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Which viewport to use for this VCam" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_TargetViewport = { "TargetViewport", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, TargetViewport), Z_Construct_UEnum_VCamCore_EVCamTargetViewportID, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_TargetViewport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_TargetViewport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_Inner_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// List of Output Providers (executed in order)\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "List of Output Providers (executed in order)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_Inner = { "OutputProviders", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVCamOutputProviderBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// List of Output Providers (executed in order)\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "List of Output Providers (executed in order)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders = { "OutputProviders", nullptr, (EPropertyFlags)0x0010008000000009, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, OutputProviders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierContext_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// Modifier Context object that can be accessed by the Modifier Stack\n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "Modifier Context object that can be accessed by the Modifier Stack" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierContext = { "ModifierContext", nullptr, (EPropertyFlags)0x0042000000080009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, ModifierContext), Z_Construct_UClass_UVCamModifierContext_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierContext_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierStack_Inner = { "ModifierStack", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FModifierStackEntry, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierStack_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "// List of Modifiers (executed in order)\n" },
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
		{ "ToolTip", "List of Modifiers (executed in order)" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierStack = { "ModifierStack", nullptr, (EPropertyFlags)0x0040008000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamComponent, ModifierStack), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierStack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierStack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamComponent_Statics::NewProp_bIsLockedToViewport_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamComponent.h" },
	};
#endif
	void Z_Construct_UClass_UVCamComponent_Statics::NewProp_bIsLockedToViewport_SetBit(void* Obj)
	{
		((UVCamComponent*)Obj)->bIsLockedToViewport = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVCamComponent_Statics::NewProp_bIsLockedToViewport = { "bIsLockedToViewport", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVCamComponent), &Z_Construct_UClass_UVCamComponent_Statics::NewProp_bIsLockedToViewport_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bIsLockedToViewport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::NewProp_bIsLockedToViewport_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVCamComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_OnComponentReplaced,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_Role,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_LiveLinkSubject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_bLockViewportToCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableComponentWhenSpawnedBySequencer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_bDisableOutputOnMultiUserReceiver,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_UpdateFrequencyMs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_TargetViewport_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_TargetViewport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_OutputProviders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierStack_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_ModifierStack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamComponent_Statics::NewProp_bIsLockedToViewport,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamComponent_Statics::ClassParams = {
		&UVCamComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVCamComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamComponent, 521724739);
	template<> VCAMCORE_API UClass* StaticClass<UVCamComponent>()
	{
		return UVCamComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamComponent(Z_Construct_UClass_UVCamComponent, &UVCamComponent::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
