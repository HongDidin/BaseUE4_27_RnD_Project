// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/VirtualCameraActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVirtualCameraActor() {}
// Cross Module References
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_AVirtualCameraActor_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_AVirtualCameraActor();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_ACineCameraActor();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkSubjectRepresentation();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraMovement_NoRegister();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaOutput_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UMG_API UClass* Z_Construct_UClass_UUserWidget_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	VPUTILITIES_API UClass* Z_Construct_UClass_UVPFullScreenUserWidget_NoRegister();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaCapture_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_PreSetVirtualCameraTransform__DelegateSignature();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnActorClickedDelegate__DelegateSignature();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraTickDelegateGroup__DelegateSignature();
	VIRTUALCAMERA_API UScriptStruct* Z_Construct_UScriptStruct_FVirtualCameraSettingsPreset();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraController_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraPresetContainer_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraOptions_NoRegister();
// End Cross Module References
	void AVirtualCameraActor::StaticRegisterNativesAVirtualCameraActor()
	{
	}
	UClass* Z_Construct_UClass_AVirtualCameraActor_NoRegister()
	{
		return AVirtualCameraActor::StaticClass();
	}
	struct Z_Construct_UClass_AVirtualCameraActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneCaptureComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneCaptureComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LiveLinkSubject_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LiveLinkSubject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MovementComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraUMGClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_CameraUMGClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetDeviceResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TargetDeviceResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RemoteSessionPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_RemoteSessionPort;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraScreenWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraScreenWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaCapture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorWorld_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ActorWorld;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviousViewTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviousViewTarget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowFocusVisualization_MetaData[];
#endif
		static void NewProp_bAllowFocusVisualization_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowFocusVisualization;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnPreSetVirtualCameraTransform_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnPreSetVirtualCameraTransform;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnActorClickedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnActorClickedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnVirtualCameraUpdatedDelegates_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnVirtualCameraUpdatedDelegates;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SettingsPresets_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SettingsPresets_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SettingsPresets_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_SettingsPresets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StreamedCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StreamedCamera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AVirtualCameraActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACineCameraActor,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "VirtualCamera" },
		{ "DisplayName", "VirtualCameraActor" },
		{ "HideCategories", "Input Rendering AutoPlayerActivation Input Rendering" },
		{ "IncludePath", "VirtualCameraActor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SceneCaptureComponent_MetaData[] = {
		{ "Category", "VirtualCamera | Component" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SceneCaptureComponent = { "SceneCaptureComponent", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, SceneCaptureComponent), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SceneCaptureComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SceneCaptureComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_LiveLinkSubject_MetaData[] = {
		{ "Category", "VirtualCamera | Movement" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_LiveLinkSubject = { "LiveLinkSubject", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, LiveLinkSubject), Z_Construct_UScriptStruct_FLiveLinkSubjectRepresentation, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_LiveLinkSubject_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_LiveLinkSubject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MovementComponent_MetaData[] = {
		{ "Category", "VirtualCamera | Movement" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MovementComponent = { "MovementComponent", nullptr, (EPropertyFlags)0x0010000000020005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, MovementComponent), Z_Construct_UClass_UVirtualCameraMovement_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MovementComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MovementComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaOutput_MetaData[] = {
		{ "Category", "VirtualCamera | MediaOutput" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0010000000012001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, MediaOutput), Z_Construct_UClass_URemoteSessionMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraUMGClass_MetaData[] = {
		{ "Category", "VirtualCamera | UMG" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraUMGClass = { "CameraUMGClass", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, CameraUMGClass), Z_Construct_UClass_UUserWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraUMGClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraUMGClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_TargetDeviceResolution_MetaData[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_TargetDeviceResolution = { "TargetDeviceResolution", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, TargetDeviceResolution), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_TargetDeviceResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_TargetDeviceResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_RemoteSessionPort_MetaData[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_RemoteSessionPort = { "RemoteSessionPort", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, RemoteSessionPort), METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_RemoteSessionPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_RemoteSessionPort_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraScreenWidget_MetaData[] = {
		{ "Category", "VirtualCamera | UMG" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraScreenWidget = { "CameraScreenWidget", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, CameraScreenWidget), Z_Construct_UClass_UVPFullScreenUserWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraScreenWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraScreenWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaCapture_MetaData[] = {
		{ "Category", "VirtualCamera | MediaOutput" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaCapture = { "MediaCapture", nullptr, (EPropertyFlags)0x0020080000012001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, MediaCapture), Z_Construct_UClass_URemoteSessionMediaCapture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaCapture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_ActorWorld_MetaData[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_ActorWorld = { "ActorWorld", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, ActorWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_ActorWorld_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_ActorWorld_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_PreviousViewTarget_MetaData[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_PreviousViewTarget = { "PreviousViewTarget", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, PreviousViewTarget), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_PreviousViewTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_PreviousViewTarget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_bAllowFocusVisualization_MetaData[] = {
		{ "Category", "VirtualCamera | Focus" },
		{ "Comment", "/** Should focus plane be shown on all touch focus events */" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
		{ "ToolTip", "Should focus plane be shown on all touch focus events" },
	};
#endif
	void Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_bAllowFocusVisualization_SetBit(void* Obj)
	{
		((AVirtualCameraActor*)Obj)->bAllowFocusVisualization = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_bAllowFocusVisualization = { "bAllowFocusVisualization", nullptr, (EPropertyFlags)0x0020080000000014, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(AVirtualCameraActor), &Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_bAllowFocusVisualization_SetBit, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_bAllowFocusVisualization_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_bAllowFocusVisualization_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnPreSetVirtualCameraTransform_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/**\n\x09 * Delegate that will is triggered before transform is set onto Actor.\n\x09 * @param FVirtualCameraTransform Transform data that is passed to delegate.\n\x09 * @return FVirtualCameraTransform Manipulated transform that will be set onto Actor.\n\x09 */" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
		{ "ToolTip", "Delegate that will is triggered before transform is set onto Actor.\n@param FVirtualCameraTransform Transform data that is passed to delegate.\n@return FVirtualCameraTransform Manipulated transform that will be set onto Actor." },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnPreSetVirtualCameraTransform = { "OnPreSetVirtualCameraTransform", nullptr, (EPropertyFlags)0x0020080000080001, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, OnPreSetVirtualCameraTransform), Z_Construct_UDelegateFunction_VirtualCamera_PreSetVirtualCameraTransform__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnPreSetVirtualCameraTransform_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnPreSetVirtualCameraTransform_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnActorClickedDelegate_MetaData[] = {
		{ "Category", "VirtualCamera | Focus" },
		{ "Comment", "/**\n\x09 * Delegate that will be triggered when an actor has been clicked/touched.\n\x09 * @note Delegate will run on Touch/Mouse-Down\n\x09 * @param AActor* Pointer to the actor that was clicked.\n\x09 */" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
		{ "ToolTip", "Delegate that will be triggered when an actor has been clicked/touched.\n@note Delegate will run on Touch/Mouse-Down\n@param AActor* Pointer to the actor that was clicked." },
	};
#endif
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnActorClickedDelegate = { "OnActorClickedDelegate", nullptr, (EPropertyFlags)0x0020080000080001, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, OnActorClickedDelegate), Z_Construct_UDelegateFunction_VirtualCamera_OnActorClickedDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnActorClickedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnActorClickedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnVirtualCameraUpdatedDelegates_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/**\n\x09 * This delegate is triggered at the end of a tick in editor/pie/game.\n\x09 * @note The Actor is only ticked while it is being streamed.\n\x09 * @param float Delta Time in seconds.\n\x09 */" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
		{ "ToolTip", "This delegate is triggered at the end of a tick in editor/pie/game.\n@note The Actor is only ticked while it is being streamed.\n@param float Delta Time in seconds." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnVirtualCameraUpdatedDelegates = { "OnVirtualCameraUpdatedDelegates", nullptr, (EPropertyFlags)0x0020080000080001, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, OnVirtualCameraUpdatedDelegates), Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraTickDelegateGroup__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnVirtualCameraUpdatedDelegates_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnVirtualCameraUpdatedDelegates_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets_ValueProp = { "SettingsPresets", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FVirtualCameraSettingsPreset, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets_Key_KeyProp = { "SettingsPresets_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets_MetaData[] = {
		{ "Category", "VirtualCamera | Settings" },
		{ "Comment", "/* Stores the list of settings presets, and saved presets */" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
		{ "ToolTip", "Stores the list of settings presets, and saved presets" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets = { "SettingsPresets", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, SettingsPresets), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_StreamedCamera_MetaData[] = {
		{ "BlueprintGetter", "GetCineCameraComponent" },
		{ "Category", "VirtualCamera | Component" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_StreamedCamera = { "StreamedCamera", nullptr, (EPropertyFlags)0x004000000008001c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AVirtualCameraActor, StreamedCamera), Z_Construct_UClass_UCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_StreamedCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_StreamedCamera_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AVirtualCameraActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SceneCaptureComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_LiveLinkSubject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MovementComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraUMGClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_TargetDeviceResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_RemoteSessionPort,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_CameraScreenWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_MediaCapture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_ActorWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_PreviousViewTarget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_bAllowFocusVisualization,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnPreSetVirtualCameraTransform,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnActorClickedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_OnVirtualCameraUpdatedDelegates,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_SettingsPresets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AVirtualCameraActor_Statics::NewProp_StreamedCamera,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AVirtualCameraActor_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UVirtualCameraController_NoRegister, (int32)VTABLE_OFFSET(AVirtualCameraActor, IVirtualCameraController), false },
			{ Z_Construct_UClass_UVirtualCameraPresetContainer_NoRegister, (int32)VTABLE_OFFSET(AVirtualCameraActor, IVirtualCameraPresetContainer), false },
			{ Z_Construct_UClass_UVirtualCameraOptions_NoRegister, (int32)VTABLE_OFFSET(AVirtualCameraActor, IVirtualCameraOptions), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AVirtualCameraActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AVirtualCameraActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AVirtualCameraActor_Statics::ClassParams = {
		&AVirtualCameraActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AVirtualCameraActor_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A5u,
		METADATA_PARAMS(Z_Construct_UClass_AVirtualCameraActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AVirtualCameraActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AVirtualCameraActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AVirtualCameraActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AVirtualCameraActor, 2281913345);
	template<> VIRTUALCAMERA_API UClass* StaticClass<AVirtualCameraActor>()
	{
		return AVirtualCameraActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AVirtualCameraActor(Z_Construct_UClass_AVirtualCameraActor, &AVirtualCameraActor::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("AVirtualCameraActor"), false, nullptr, nullptr, nullptr);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
