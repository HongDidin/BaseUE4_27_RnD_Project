// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_VCamOutputMediaOutput_generated_h
#error "VCamOutputMediaOutput.generated.h already included, missing '#pragma once' in VCamOutputMediaOutput.h"
#endif
#define VCAMCORE_VCamOutputMediaOutput_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamOutputMediaOutput(); \
	friend struct Z_Construct_UClass_UVCamOutputMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputMediaOutput, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputMediaOutput)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUVCamOutputMediaOutput(); \
	friend struct Z_Construct_UClass_UVCamOutputMediaOutput_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputMediaOutput, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputMediaOutput)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputMediaOutput(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamOutputMediaOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputMediaOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputMediaOutput(UVCamOutputMediaOutput&&); \
	NO_API UVCamOutputMediaOutput(const UVCamOutputMediaOutput&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputMediaOutput() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputMediaOutput(UVCamOutputMediaOutput&&); \
	NO_API UVCamOutputMediaOutput(const UVCamOutputMediaOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputMediaOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputMediaOutput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVCamOutputMediaOutput)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__MediaCapture() { return STRUCT_OFFSET(UVCamOutputMediaOutput, MediaCapture); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_14_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamOutputMediaOutput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputMediaOutput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
