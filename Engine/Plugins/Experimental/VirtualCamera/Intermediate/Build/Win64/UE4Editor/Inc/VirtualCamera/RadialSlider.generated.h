// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLinearColor;
struct FRuntimeFloatCurve;
#ifdef VIRTUALCAMERA_RadialSlider_generated_h
#error "RadialSlider.generated.h already included, missing '#pragma once' in RadialSlider.h"
#endif
#define VIRTUALCAMERA_RadialSlider_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_18_DELEGATE \
struct _Script_VirtualCamera_eventOnFloatValueChangedEvent_Parms \
{ \
	float Value; \
}; \
static inline void FOnFloatValueChangedEvent_DelegateWrapper(const FMulticastScriptDelegate& OnFloatValueChangedEvent, float Value) \
{ \
	_Script_VirtualCamera_eventOnFloatValueChangedEvent_Parms Parms; \
	Parms.Value=Value; \
	OnFloatValueChangedEvent.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_17_DELEGATE \
static inline void FOnControllerCaptureEndEvent_DelegateWrapper(const FMulticastScriptDelegate& OnControllerCaptureEndEvent) \
{ \
	OnControllerCaptureEndEvent.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_16_DELEGATE \
static inline void FOnControllerCaptureBeginEvent_DelegateWrapper(const FMulticastScriptDelegate& OnControllerCaptureBeginEvent) \
{ \
	OnControllerCaptureBeginEvent.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_15_DELEGATE \
static inline void FOnMouseCaptureEndEvent_DelegateWrapper(const FMulticastScriptDelegate& OnMouseCaptureEndEvent) \
{ \
	OnMouseCaptureEndEvent.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_14_DELEGATE \
static inline void FOnMouseCaptureBeginEvent_DelegateWrapper(const FMulticastScriptDelegate& OnMouseCaptureBeginEvent) \
{ \
	OnMouseCaptureBeginEvent.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetSliderHandleColor); \
	DECLARE_FUNCTION(execSetSliderProgressColor); \
	DECLARE_FUNCTION(execSetSliderBarColor); \
	DECLARE_FUNCTION(execSetStepSize); \
	DECLARE_FUNCTION(execSetLocked); \
	DECLARE_FUNCTION(execSetAngularOffset); \
	DECLARE_FUNCTION(execSetSliderHandleEndAngle); \
	DECLARE_FUNCTION(execSetSliderHandleStartAngle); \
	DECLARE_FUNCTION(execSetValueTags); \
	DECLARE_FUNCTION(execSetSliderRange); \
	DECLARE_FUNCTION(execSetCustomDefaultValue); \
	DECLARE_FUNCTION(execSetValue); \
	DECLARE_FUNCTION(execGetNormalizedSliderHandlePosition); \
	DECLARE_FUNCTION(execGetCustomDefaultValue); \
	DECLARE_FUNCTION(execGetValue);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetSliderHandleColor); \
	DECLARE_FUNCTION(execSetSliderProgressColor); \
	DECLARE_FUNCTION(execSetSliderBarColor); \
	DECLARE_FUNCTION(execSetStepSize); \
	DECLARE_FUNCTION(execSetLocked); \
	DECLARE_FUNCTION(execSetAngularOffset); \
	DECLARE_FUNCTION(execSetSliderHandleEndAngle); \
	DECLARE_FUNCTION(execSetSliderHandleStartAngle); \
	DECLARE_FUNCTION(execSetValueTags); \
	DECLARE_FUNCTION(execSetSliderRange); \
	DECLARE_FUNCTION(execSetCustomDefaultValue); \
	DECLARE_FUNCTION(execSetValue); \
	DECLARE_FUNCTION(execGetNormalizedSliderHandlePosition); \
	DECLARE_FUNCTION(execGetCustomDefaultValue); \
	DECLARE_FUNCTION(execGetValue);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURadialSlider(); \
	friend struct Z_Construct_UClass_URadialSlider_Statics; \
public: \
	DECLARE_CLASS(URadialSlider, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(URadialSlider)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_INCLASS \
private: \
	static void StaticRegisterNativesURadialSlider(); \
	friend struct Z_Construct_UClass_URadialSlider_Statics; \
public: \
	DECLARE_CLASS(URadialSlider, UWidget, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(URadialSlider)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URadialSlider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URadialSlider) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URadialSlider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URadialSlider); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URadialSlider(URadialSlider&&); \
	NO_API URadialSlider(const URadialSlider&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URadialSlider(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URadialSlider(URadialSlider&&); \
	NO_API URadialSlider(const URadialSlider&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URadialSlider); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URadialSlider); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URadialSlider)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_26_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h_29_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class RadialSlider."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class URadialSlider>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_Components_RadialSlider_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
