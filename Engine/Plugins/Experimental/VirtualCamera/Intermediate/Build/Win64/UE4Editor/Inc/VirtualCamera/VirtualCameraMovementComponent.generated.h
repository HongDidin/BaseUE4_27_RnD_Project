// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIRTUALCAMERA_VirtualCameraMovementComponent_generated_h
#error "VirtualCameraMovementComponent.generated.h already included, missing '#pragma once' in VirtualCameraMovementComponent.h"
#endif
#define VIRTUALCAMERA_VirtualCameraMovementComponent_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_9_DELEGATE \
static inline void FVirtualCameraResetOffsetsDelegate_DelegateWrapper(const FMulticastScriptDelegate& VirtualCameraResetOffsetsDelegate) \
{ \
	VirtualCameraResetOffsetsDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVirtualCameraMovementComponent(); \
	friend struct Z_Construct_UClass_UVirtualCameraMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UVirtualCameraMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVirtualCameraMovementComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUVirtualCameraMovementComponent(); \
	friend struct Z_Construct_UClass_UVirtualCameraMovementComponent_Statics; \
public: \
	DECLARE_CLASS(UVirtualCameraMovementComponent, UPawnMovementComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVirtualCameraMovementComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualCameraMovementComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraMovementComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraMovementComponent(UVirtualCameraMovementComponent&&); \
	NO_API UVirtualCameraMovementComponent(const UVirtualCameraMovementComponent&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraMovementComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraMovementComponent(UVirtualCameraMovementComponent&&); \
	NO_API UVirtualCameraMovementComponent(const UVirtualCameraMovementComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraMovementComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraMovementComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualCameraMovementComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RootUpdatedComponent() { return STRUCT_OFFSET(UVirtualCameraMovementComponent, RootUpdatedComponent); } \
	FORCEINLINE static uint32 __PPO__RootUpdatedPrimitive() { return STRUCT_OFFSET(UVirtualCameraMovementComponent, RootUpdatedPrimitive); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_11_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VirtualCameraMovementComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class UVirtualCameraMovementComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraMovementComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
