// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_VCamMultiUser_generated_h
#error "VCamMultiUser.generated.h already included, missing '#pragma once' in VCamMultiUser.h"
#endif
#define VCAMCORE_VCamMultiUser_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamMultiUser_h_85_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMultiUserVCamCameraComponentEvent_Statics; \
	VCAMCORE_API static class UScriptStruct* StaticStruct();


template<> VCAMCORE_API UScriptStruct* StaticStruct<struct FMultiUserVCamCameraComponentEvent>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamMultiUser_h_40_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMultiUserVCamCameraData_Statics; \
	VCAMCORE_API static class UScriptStruct* StaticStruct();


template<> VCAMCORE_API UScriptStruct* StaticStruct<struct FMultiUserVCamCameraData>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamMultiUser_h_21_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMultiUserVCamCameraFocusData_Statics; \
	VCAMCORE_API static class UScriptStruct* StaticStruct();


template<> VCAMCORE_API UScriptStruct* StaticStruct<struct FMultiUserVCamCameraFocusData>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamMultiUser_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
