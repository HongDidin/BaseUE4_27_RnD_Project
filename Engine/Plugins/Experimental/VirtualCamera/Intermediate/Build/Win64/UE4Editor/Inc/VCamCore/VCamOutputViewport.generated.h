// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_VCamOutputViewport_generated_h
#error "VCamOutputViewport.generated.h already included, missing '#pragma once' in VCamOutputViewport.h"
#endif
#define VCAMCORE_VCamOutputViewport_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamOutputViewport(); \
	friend struct Z_Construct_UClass_UVCamOutputViewport_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputViewport, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputViewport)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUVCamOutputViewport(); \
	friend struct Z_Construct_UClass_UVCamOutputViewport_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputViewport, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputViewport)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputViewport(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamOutputViewport) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputViewport); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputViewport); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputViewport(UVCamOutputViewport&&); \
	NO_API UVCamOutputViewport(const UVCamOutputViewport&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputViewport() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputViewport(UVCamOutputViewport&&); \
	NO_API UVCamOutputViewport(const UVCamOutputViewport&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputViewport); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputViewport); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVCamOutputViewport)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_9_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamOutputViewport>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputViewport_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
