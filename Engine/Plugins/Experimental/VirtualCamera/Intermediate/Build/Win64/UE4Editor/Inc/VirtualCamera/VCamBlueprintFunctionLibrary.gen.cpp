// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/VCamBlueprintFunctionLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamBlueprintFunctionLibrary() {}
// Cross Module References
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVCamBlueprintFunctionLibrary_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVCamBlueprintFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneObjectBindingID();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTimecode();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraUserSettings_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraClipsMetaData_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FAssetData();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetNextUndoDescription)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetNextUndoDescription();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execTimecodeToFrameAmount)
	{
		P_GET_STRUCT(FTimecode,Z_Param_Timecode);
		P_GET_STRUCT_REF(FFrameRate,Z_Param_Out_InFrameRate);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UVCamBlueprintFunctionLibrary::TimecodeToFrameAmount(Z_Param_Timecode,Z_Param_Out_InFrameRate);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execEnableDebugFocusPlane)
	{
		P_GET_OBJECT(UCineCameraComponent,Z_Param_CineCamera);
		P_GET_UBOOL(Z_Param_bEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamBlueprintFunctionLibrary::EnableDebugFocusPlane(Z_Param_CineCamera,Z_Param_bEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetBoundObjects)
	{
		P_GET_STRUCT(FMovieSceneObjectBindingID,Z_Param_CameraBindingID);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetBoundObjects(Z_Param_CameraBindingID);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execCalculateAutoFocusDistance)
	{
		P_GET_STRUCT(FVector2D,Z_Param_ReticlePosition);
		P_GET_OBJECT(UCineCameraComponent,Z_Param_CineCamera);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=UVCamBlueprintFunctionLibrary::CalculateAutoFocusDistance(Z_Param_ReticlePosition,Z_Param_CineCamera);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execEditorSetGameView)
	{
		P_GET_UBOOL(Z_Param_bIsToggled);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamBlueprintFunctionLibrary::EditorSetGameView(Z_Param_bIsToggled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execCallFunctionByName)
	{
		P_GET_OBJECT(UObject,Z_Param_ObjPtr);
		P_GET_PROPERTY(FNameProperty,Z_Param_FunctionName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVCamBlueprintFunctionLibrary::CallFunctionByName(Z_Param_ObjPtr,Z_Param_FunctionName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execConvertStringToFrameRate)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InFrameRateString);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FFrameRate*)Z_Param__Result=UVCamBlueprintFunctionLibrary::ConvertStringToFrameRate(Z_Param_InFrameRateString);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetDisplayRate)
	{
		P_GET_OBJECT(ULevelSequence,Z_Param_LevelSequence);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FFrameRate*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetDisplayRate(Z_Param_LevelSequence);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execUpdatePostProcessSettingsForCapture)
	{
		P_GET_OBJECT(USceneCaptureComponent2D,Z_Param_CaptureComponent);
		P_GET_PROPERTY(FFloatProperty,Z_Param_DepthOfField);
		P_GET_PROPERTY(FFloatProperty,Z_Param_FStopValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVCamBlueprintFunctionLibrary::UpdatePostProcessSettingsForCapture(Z_Param_CaptureComponent,Z_Param_DepthOfField,Z_Param_FStopValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execPilotActor)
	{
		P_GET_OBJECT(AActor,Z_Param_SelectedActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamBlueprintFunctionLibrary::PilotActor(Z_Param_SelectedActor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execSortAssetsByTimecodeAssetData)
	{
		P_GET_TARRAY(FAssetData,Z_Param_LevelSequenceAssets);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FAssetData>*)Z_Param__Result=UVCamBlueprintFunctionLibrary::SortAssetsByTimecodeAssetData(Z_Param_LevelSequenceAssets);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetObjectMetadataTags)
	{
		P_GET_OBJECT(UObject,Z_Param_InObject);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TMap<FName,FString>*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetObjectMetadataTags(Z_Param_InObject);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execModifyObjectMetadataTags)
	{
		P_GET_OBJECT(UObject,Z_Param_InObject);
		P_GET_PROPERTY(FNameProperty,Z_Param_InTag);
		P_GET_PROPERTY(FStrProperty,Z_Param_InValue);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamBlueprintFunctionLibrary::ModifyObjectMetadataTags(Z_Param_InObject,Z_Param_InTag,Z_Param_InValue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execEditorLoadAsset)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AssetPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UObject**)Z_Param__Result=UVCamBlueprintFunctionLibrary::EditorLoadAsset(Z_Param_AssetPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execEditorSaveAsset)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_AssetPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVCamBlueprintFunctionLibrary::EditorSaveAsset(Z_Param_AssetPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execModifyLevelSequenceMetadata)
	{
		P_GET_OBJECT(UVirtualCameraClipsMetaData,Z_Param_LevelSequenceMetaData);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVCamBlueprintFunctionLibrary::ModifyLevelSequenceMetadata(Z_Param_LevelSequenceMetaData);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execModifyLevelSequenceMetadataForSelects)
	{
		P_GET_OBJECT(UVirtualCameraClipsMetaData,Z_Param_LevelSequenceMetaData);
		P_GET_UBOOL(Z_Param_bIsSelected);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVCamBlueprintFunctionLibrary::ModifyLevelSequenceMetadataForSelects(Z_Param_LevelSequenceMetaData,Z_Param_bIsSelected);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execImportSnapshotTexture)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_FileName);
		P_GET_PROPERTY(FStrProperty,Z_Param_SubFolderName);
		P_GET_PROPERTY(FStrProperty,Z_Param_AbsolutePathPackage);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTexture**)Z_Param__Result=UVCamBlueprintFunctionLibrary::ImportSnapshotTexture(Z_Param_FileName,Z_Param_SubFolderName,Z_Param_AbsolutePathPackage);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execIsCurrentLevelSequencePlaying)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVCamBlueprintFunctionLibrary::IsCurrentLevelSequencePlaying();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetLevelSequenceFrameAsTimecodeWithoutObject)
	{
		P_GET_STRUCT(FFrameRate,Z_Param_DisplayRate);
		P_GET_PROPERTY(FIntProperty,Z_Param_InFrame);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTimecode*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetLevelSequenceFrameAsTimecodeWithoutObject(Z_Param_DisplayRate,Z_Param_InFrame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetLevelSequenceFrameAsTimecode)
	{
		P_GET_OBJECT(ULevelSequence,Z_Param_LevelSequence);
		P_GET_PROPERTY(FIntProperty,Z_Param_InFrame);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FTimecode*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetLevelSequenceFrameAsTimecode(Z_Param_LevelSequence,Z_Param_InFrame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetLevelSequenceLengthInFrames)
	{
		P_GET_OBJECT(ULevelSequence,Z_Param_LevelSequence);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetLevelSequenceLengthInFrames(Z_Param_LevelSequence);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetCurrentLevelSequenceCurrentFrame)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetCurrentLevelSequenceCurrentFrame();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execSetCurrentLevelSequenceCurrentFrame)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_NewFrame);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamBlueprintFunctionLibrary::SetCurrentLevelSequenceCurrentFrame(Z_Param_NewFrame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execPauseCurrentLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamBlueprintFunctionLibrary::PauseCurrentLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execPlayCurrentLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamBlueprintFunctionLibrary::PlayCurrentLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetCurrentLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSequence**)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetCurrentLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execGetUserSettings)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UVirtualCameraUserSettings**)Z_Param__Result=UVCamBlueprintFunctionLibrary::GetUserSettings();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamBlueprintFunctionLibrary::execIsGameRunning)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UVCamBlueprintFunctionLibrary::IsGameRunning();
		P_NATIVE_END;
	}
	void UVCamBlueprintFunctionLibrary::StaticRegisterNativesUVCamBlueprintFunctionLibrary()
	{
		UClass* Class = UVCamBlueprintFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CalculateAutoFocusDistance", &UVCamBlueprintFunctionLibrary::execCalculateAutoFocusDistance },
			{ "CallFunctionByName", &UVCamBlueprintFunctionLibrary::execCallFunctionByName },
			{ "ConvertStringToFrameRate", &UVCamBlueprintFunctionLibrary::execConvertStringToFrameRate },
			{ "EditorLoadAsset", &UVCamBlueprintFunctionLibrary::execEditorLoadAsset },
			{ "EditorSaveAsset", &UVCamBlueprintFunctionLibrary::execEditorSaveAsset },
			{ "EditorSetGameView", &UVCamBlueprintFunctionLibrary::execEditorSetGameView },
			{ "EnableDebugFocusPlane", &UVCamBlueprintFunctionLibrary::execEnableDebugFocusPlane },
			{ "GetBoundObjects", &UVCamBlueprintFunctionLibrary::execGetBoundObjects },
			{ "GetCurrentLevelSequence", &UVCamBlueprintFunctionLibrary::execGetCurrentLevelSequence },
			{ "GetCurrentLevelSequenceCurrentFrame", &UVCamBlueprintFunctionLibrary::execGetCurrentLevelSequenceCurrentFrame },
			{ "GetDisplayRate", &UVCamBlueprintFunctionLibrary::execGetDisplayRate },
			{ "GetLevelSequenceFrameAsTimecode", &UVCamBlueprintFunctionLibrary::execGetLevelSequenceFrameAsTimecode },
			{ "GetLevelSequenceFrameAsTimecodeWithoutObject", &UVCamBlueprintFunctionLibrary::execGetLevelSequenceFrameAsTimecodeWithoutObject },
			{ "GetLevelSequenceLengthInFrames", &UVCamBlueprintFunctionLibrary::execGetLevelSequenceLengthInFrames },
			{ "GetNextUndoDescription", &UVCamBlueprintFunctionLibrary::execGetNextUndoDescription },
			{ "GetObjectMetadataTags", &UVCamBlueprintFunctionLibrary::execGetObjectMetadataTags },
			{ "GetUserSettings", &UVCamBlueprintFunctionLibrary::execGetUserSettings },
			{ "ImportSnapshotTexture", &UVCamBlueprintFunctionLibrary::execImportSnapshotTexture },
			{ "IsCurrentLevelSequencePlaying", &UVCamBlueprintFunctionLibrary::execIsCurrentLevelSequencePlaying },
			{ "IsGameRunning", &UVCamBlueprintFunctionLibrary::execIsGameRunning },
			{ "ModifyLevelSequenceMetadata", &UVCamBlueprintFunctionLibrary::execModifyLevelSequenceMetadata },
			{ "ModifyLevelSequenceMetadataForSelects", &UVCamBlueprintFunctionLibrary::execModifyLevelSequenceMetadataForSelects },
			{ "ModifyObjectMetadataTags", &UVCamBlueprintFunctionLibrary::execModifyObjectMetadataTags },
			{ "PauseCurrentLevelSequence", &UVCamBlueprintFunctionLibrary::execPauseCurrentLevelSequence },
			{ "PilotActor", &UVCamBlueprintFunctionLibrary::execPilotActor },
			{ "PlayCurrentLevelSequence", &UVCamBlueprintFunctionLibrary::execPlayCurrentLevelSequence },
			{ "SetCurrentLevelSequenceCurrentFrame", &UVCamBlueprintFunctionLibrary::execSetCurrentLevelSequenceCurrentFrame },
			{ "SortAssetsByTimecodeAssetData", &UVCamBlueprintFunctionLibrary::execSortAssetsByTimecodeAssetData },
			{ "TimecodeToFrameAmount", &UVCamBlueprintFunctionLibrary::execTimecodeToFrameAmount },
			{ "UpdatePostProcessSettingsForCapture", &UVCamBlueprintFunctionLibrary::execUpdatePostProcessSettingsForCapture },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventCalculateAutoFocusDistance_Parms
		{
			FVector2D ReticlePosition;
			UCineCameraComponent* CineCamera;
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReticlePosition;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CineCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CineCamera;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_ReticlePosition = { "ReticlePosition", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventCalculateAutoFocusDistance_Parms, ReticlePosition), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_CineCamera_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_CineCamera = { "CineCamera", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventCalculateAutoFocusDistance_Parms, CineCamera), Z_Construct_UClass_UCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_CineCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_CineCamera_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventCalculateAutoFocusDistance_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_ReticlePosition,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_CineCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Calculates auto focus */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Calculates auto focus" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "CalculateAutoFocusDistance", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventCalculateAutoFocusDistance_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04822401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventCallFunctionByName_Parms
		{
			UObject* ObjPtr;
			FName FunctionName;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjPtr;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_FunctionName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_ObjPtr = { "ObjPtr", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventCallFunctionByName_Parms, ObjPtr), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_FunctionName = { "FunctionName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventCallFunctionByName_Parms, FunctionName), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventCallFunctionByName_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventCallFunctionByName_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_ObjPtr,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_FunctionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Returns true if the function was found & executed correctly. */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Returns true if the function was found & executed correctly." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "CallFunctionByName", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventCallFunctionByName_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics
	{
		struct FFrameRate
		{
			int32 Numerator;
			int32 Denominator;
		};

		struct VCamBlueprintFunctionLibrary_eventConvertStringToFrameRate_Parms
		{
			FString InFrameRateString;
			FFrameRate ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InFrameRateString;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::NewProp_InFrameRateString = { "InFrameRateString", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventConvertStringToFrameRate_Parms, InFrameRateString), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventConvertStringToFrameRate_Parms, ReturnValue), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::NewProp_InFrameRateString,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Converts a double framerate to a FFrameRate */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Converts a double framerate to a FFrameRate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "ConvertStringToFrameRate", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventConvertStringToFrameRate_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventEditorLoadAsset_Parms
		{
			FString AssetPath;
			UObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventEditorLoadAsset_Parms, AssetPath), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventEditorLoadAsset_Parms, ReturnValue), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::NewProp_AssetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Clips" },
		{ "Comment", "/** Load an asset through path. */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Load an asset through path." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "EditorLoadAsset", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventEditorLoadAsset_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventEditorSaveAsset_Parms
		{
			FString AssetPath;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AssetPath;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::NewProp_AssetPath = { "AssetPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventEditorSaveAsset_Parms, AssetPath), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventEditorSaveAsset_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventEditorSaveAsset_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::NewProp_AssetPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Clips" },
		{ "Comment", "/** Save an asset through path. Returns true on success. */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Save an asset through path. Returns true on success." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "EditorSaveAsset", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventEditorSaveAsset_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventEditorSetGameView_Parms
		{
			bool bIsToggled;
		};
		static void NewProp_bIsToggled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsToggled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::NewProp_bIsToggled_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventEditorSetGameView_Parms*)Obj)->bIsToggled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::NewProp_bIsToggled = { "bIsToggled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventEditorSetGameView_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::NewProp_bIsToggled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::NewProp_bIsToggled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Sets the current game view */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Sets the current game view" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "EditorSetGameView", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventEditorSetGameView_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventEnableDebugFocusPlane_Parms
		{
			UCineCameraComponent* CineCamera;
			bool bEnabled;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CineCamera_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CineCamera;
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_CineCamera_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_CineCamera = { "CineCamera", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventEnableDebugFocusPlane_Parms, CineCamera), Z_Construct_UClass_UCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_CineCamera_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_CineCamera_MetaData)) };
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventEnableDebugFocusPlane_Parms*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventEnableDebugFocusPlane_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_CineCamera,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::NewProp_bEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Enable/Disable debug focus plane*/" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Enable/Disable debug focus plane" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "EnableDebugFocusPlane", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventEnableDebugFocusPlane_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventGetBoundObjects_Parms
		{
			FMovieSceneObjectBindingID CameraBindingID;
			TArray<UObject*> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CameraBindingID;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::NewProp_CameraBindingID = { "CameraBindingID", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetBoundObjects_Parms, CameraBindingID), Z_Construct_UScriptStruct_FMovieSceneObjectBindingID, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetBoundObjects_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::NewProp_CameraBindingID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Get UObject from Camera Object Bindings*/" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Get UObject from Camera Object Bindings" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetBoundObjects", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetBoundObjects_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventGetCurrentLevelSequence_Parms
		{
			ULevelSequence* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetCurrentLevelSequence_Parms, ReturnValue), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Get the currently opened level sequence asset */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Get the currently opened level sequence asset" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetCurrentLevelSequence", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetCurrentLevelSequence_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventGetCurrentLevelSequenceCurrentFrame_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetCurrentLevelSequenceCurrentFrame_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Get the current playback position in frames */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Get the current playback position in frames" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetCurrentLevelSequenceCurrentFrame", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetCurrentLevelSequenceCurrentFrame_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics
	{
		struct FFrameRate
		{
			int32 Numerator;
			int32 Denominator;
		};

		struct VCamBlueprintFunctionLibrary_eventGetDisplayRate_Parms
		{
			ULevelSequence* LevelSequence;
			FFrameRate ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequence;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::NewProp_LevelSequence = { "LevelSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetDisplayRate_Parms, LevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetDisplayRate_Parms, ReturnValue), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::NewProp_LevelSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Grab the display rate from a LevelSequences' MovieScene */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Grab the display rate from a LevelSequences' MovieScene" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetDisplayRate", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetDisplayRate_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics
	{
		struct FTimecode
		{
			int32 Hours;
			int32 Minutes;
			int32 Seconds;
			int32 Frames;
			bool bDropFrameFormat;
		};

		struct VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecode_Parms
		{
			const ULevelSequence* LevelSequence;
			int32 InFrame;
			FTimecode ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequence;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InFrame;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_LevelSequence_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_LevelSequence = { "LevelSequence", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecode_Parms, LevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_LevelSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_LevelSequence_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_InFrame = { "InFrame", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecode_Parms, InFrame), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecode_Parms, ReturnValue), Z_Construct_UScriptStruct_FTimecode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_LevelSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_InFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Convert a frame from a level sequence to timecode */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Convert a frame from a level sequence to timecode" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetLevelSequenceFrameAsTimecode", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecode_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics
	{
		struct FTimecode
		{
			int32 Hours;
			int32 Minutes;
			int32 Seconds;
			int32 Frames;
			bool bDropFrameFormat;
		};

		struct FFrameRate
		{
			int32 Numerator;
			int32 Denominator;
		};

		struct VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecodeWithoutObject_Parms
		{
			FFrameRate DisplayRate;
			int32 InFrame;
			FTimecode ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DisplayRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DisplayRate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InFrame;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_DisplayRate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_DisplayRate = { "DisplayRate", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecodeWithoutObject_Parms, DisplayRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_DisplayRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_DisplayRate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_InFrame = { "InFrame", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecodeWithoutObject_Parms, InFrame), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecodeWithoutObject_Parms, ReturnValue), Z_Construct_UScriptStruct_FTimecode, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_DisplayRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_InFrame,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Convert a frame from a level sequence to timecode using only a provided display rate */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Convert a frame from a level sequence to timecode using only a provided display rate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetLevelSequenceFrameAsTimecodeWithoutObject", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetLevelSequenceFrameAsTimecodeWithoutObject_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventGetLevelSequenceLengthInFrames_Parms
		{
			const ULevelSequence* LevelSequence;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequence;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::NewProp_LevelSequence_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::NewProp_LevelSequence = { "LevelSequence", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceLengthInFrames_Parms, LevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::NewProp_LevelSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::NewProp_LevelSequence_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetLevelSequenceLengthInFrames_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::NewProp_LevelSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Get length in frames of a level sequence */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Get length in frames of a level sequence" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetLevelSequenceLengthInFrames", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetLevelSequenceLengthInFrames_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventGetNextUndoDescription_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetNextUndoDescription_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Returns the description of the undo action that will be performed next.*/" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Returns the description of the undo action that will be performed next." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetNextUndoDescription", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetNextUndoDescription_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventGetObjectMetadataTags_Parms
		{
			UObject* InObject;
			TMap<FName,FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObject;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ReturnValue_Key_KeyProp;
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_InObject = { "InObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetObjectMetadataTags_Parms, InObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_ReturnValue_ValueProp = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_ReturnValue_Key_KeyProp = { "ReturnValue_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetObjectMetadataTags_Parms, ReturnValue), EMapPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_InObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_ReturnValue_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_ReturnValue_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Clips" },
		{ "Comment", "/** Retrieves UObject's metadata tags */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Retrieves UObject's metadata tags" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetObjectMetadataTags", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetObjectMetadataTags_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventGetUserSettings_Parms
		{
			UVirtualCameraUserSettings* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventGetUserSettings_Parms, ReturnValue), Z_Construct_UClass_UVirtualCameraUserSettings_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "GetUserSettings", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventGetUserSettings_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventImportSnapshotTexture_Parms
		{
			FString FileName;
			FString SubFolderName;
			FString AbsolutePathPackage;
			UTexture* ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SubFolderName;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AbsolutePathPackage;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_FileName = { "FileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventImportSnapshotTexture_Parms, FileName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_SubFolderName = { "SubFolderName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventImportSnapshotTexture_Parms, SubFolderName), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_AbsolutePathPackage = { "AbsolutePathPackage", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventImportSnapshotTexture_Parms, AbsolutePathPackage), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventImportSnapshotTexture_Parms, ReturnValue), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_FileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_SubFolderName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_AbsolutePathPackage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Imports image as a uasset */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Imports image as a uasset" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "ImportSnapshotTexture", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventImportSnapshotTexture_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventIsCurrentLevelSequencePlaying_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventIsCurrentLevelSequencePlaying_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventIsCurrentLevelSequencePlaying_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Check whether the sequence is actively playing. */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Check whether the sequence is actively playing." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "IsCurrentLevelSequencePlaying", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventIsCurrentLevelSequencePlaying_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventIsGameRunning_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventIsGameRunning_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventIsGameRunning_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Returns true if not in editor or if running the game in PIE or Simulate*/" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Returns true if not in editor or if running the game in PIE or Simulate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "IsGameRunning", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventIsGameRunning_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadata_Parms
		{
			UVirtualCameraClipsMetaData* LevelSequenceMetaData;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequenceMetaData;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::NewProp_LevelSequenceMetaData = { "LevelSequenceMetaData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadata_Parms, LevelSequenceMetaData), Z_Construct_UClass_UVirtualCameraClipsMetaData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadata_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadata_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::NewProp_LevelSequenceMetaData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Metadata" },
		{ "Comment", "/** Marks a LevelSequence as dirty and saves it, persisting metadata changes */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Marks a LevelSequence as dirty and saves it, persisting metadata changes" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "ModifyLevelSequenceMetadata", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadata_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadataForSelects_Parms
		{
			UVirtualCameraClipsMetaData* LevelSequenceMetaData;
			bool bIsSelected;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequenceMetaData;
		static void NewProp_bIsSelected_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsSelected;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_LevelSequenceMetaData = { "LevelSequenceMetaData", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadataForSelects_Parms, LevelSequenceMetaData), Z_Construct_UClass_UVirtualCameraClipsMetaData_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_bIsSelected_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadataForSelects_Parms*)Obj)->bIsSelected = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_bIsSelected = { "bIsSelected", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadataForSelects_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_bIsSelected_SetBit, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadataForSelects_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadataForSelects_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_LevelSequenceMetaData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_bIsSelected,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Metadata" },
		{ "Comment", "/** Saves UVirtualCameraClipsMetaData with updated selects information. */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Saves UVirtualCameraClipsMetaData with updated selects information." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "ModifyLevelSequenceMetadataForSelects", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventModifyLevelSequenceMetadataForSelects_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventModifyObjectMetadataTags_Parms
		{
			UObject* InObject;
			FName InTag;
			FString InValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObject;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InTag;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::NewProp_InObject = { "InObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventModifyObjectMetadataTags_Parms, InObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::NewProp_InTag = { "InTag", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventModifyObjectMetadataTags_Parms, InTag), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::NewProp_InValue = { "InValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventModifyObjectMetadataTags_Parms, InValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::NewProp_InObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::NewProp_InTag,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::NewProp_InValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Clips" },
		{ "Comment", "/** Modifies a UObject's metadata tags, adding a tag if the tag does not exist. */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Modifies a UObject's metadata tags, adding a tag if the tag does not exist." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "ModifyObjectMetadataTags", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventModifyObjectMetadataTags_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Pause the current level sequence */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Pause the current level sequence" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "PauseCurrentLevelSequence", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventPilotActor_Parms
		{
			AActor* SelectedActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::NewProp_SelectedActor = { "SelectedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventPilotActor_Parms, SelectedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::NewProp_SelectedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "Comment", "/** Pilot the provided actor using editor scripting */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Pilot the provided actor using editor scripting" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "PilotActor", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventPilotActor_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Play the current level sequence */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Play the current level sequence" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "PlayCurrentLevelSequence", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventSetCurrentLevelSequenceCurrentFrame_Parms
		{
			int32 NewFrame;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::NewProp_NewFrame = { "NewFrame", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventSetCurrentLevelSequenceCurrentFrame_Parms, NewFrame), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::NewProp_NewFrame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Set playback position for the current level sequence in frames */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Set playback position for the current level sequence in frames" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "SetCurrentLevelSequenceCurrentFrame", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventSetCurrentLevelSequenceCurrentFrame_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventSortAssetsByTimecodeAssetData_Parms
		{
			TArray<FAssetData> LevelSequenceAssets;
			TArray<FAssetData> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LevelSequenceAssets_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LevelSequenceAssets;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_LevelSequenceAssets_Inner = { "LevelSequenceAssets", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAssetData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_LevelSequenceAssets = { "LevelSequenceAssets", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventSortAssetsByTimecodeAssetData_Parms, LevelSequenceAssets), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FAssetData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventSortAssetsByTimecodeAssetData_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_LevelSequenceAssets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_LevelSequenceAssets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Clips" },
		{ "Comment", "/** Sort array of FAssetData by metadata timecode **/" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Sort array of FAssetData by metadata timecode *" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "SortAssetsByTimecodeAssetData", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventSortAssetsByTimecodeAssetData_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics
	{
		struct FFrameRate
		{
			int32 Numerator;
			int32 Denominator;
		};

		struct FTimecode
		{
			int32 Hours;
			int32 Minutes;
			int32 Seconds;
			int32 Frames;
			bool bDropFrameFormat;
		};

		struct VCamBlueprintFunctionLibrary_eventTimecodeToFrameAmount_Parms
		{
			FTimecode Timecode;
			FFrameRate InFrameRate;
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Timecode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFrameRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InFrameRate;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_Timecode = { "Timecode", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventTimecodeToFrameAmount_Parms, Timecode), Z_Construct_UScriptStruct_FTimecode, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_InFrameRate_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_InFrameRate = { "InFrameRate", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventTimecodeToFrameAmount_Parms, InFrameRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_InFrameRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_InFrameRate_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventTimecodeToFrameAmount_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_Timecode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_InFrameRate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Convert timecode to amount of frames at a given framerate */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Convert timecode to amount of frames at a given framerate" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "TimecodeToFrameAmount", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventTimecodeToFrameAmount_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics
	{
		struct VCamBlueprintFunctionLibrary_eventUpdatePostProcessSettingsForCapture_Parms
		{
			USceneCaptureComponent2D* CaptureComponent;
			float DepthOfField;
			float FStopValue;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CaptureComponent;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DepthOfField;
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FStopValue;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_CaptureComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_CaptureComponent = { "CaptureComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventUpdatePostProcessSettingsForCapture_Parms, CaptureComponent), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_CaptureComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_CaptureComponent_MetaData)) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_DepthOfField = { "DepthOfField", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventUpdatePostProcessSettingsForCapture_Parms, DepthOfField), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_FStopValue = { "FStopValue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintFunctionLibrary_eventUpdatePostProcessSettingsForCapture_Parms, FStopValue), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamBlueprintFunctionLibrary_eventUpdatePostProcessSettingsForCapture_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamBlueprintFunctionLibrary_eventUpdatePostProcessSettingsForCapture_Parms), &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_CaptureComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_DepthOfField,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_FStopValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/** Updates the provided USceneCaptureComponent2D's PostProcessingSettings. Returns true on success. */" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
		{ "ToolTip", "Updates the provided USceneCaptureComponent2D's PostProcessingSettings. Returns true on success." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintFunctionLibrary, nullptr, "UpdatePostProcessSettingsForCapture", nullptr, nullptr, sizeof(VCamBlueprintFunctionLibrary_eventUpdatePostProcessSettingsForCapture_Parms), Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVCamBlueprintFunctionLibrary_NoRegister()
	{
		return UVCamBlueprintFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CalculateAutoFocusDistance, "CalculateAutoFocusDistance" }, // 4152886558
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_CallFunctionByName, "CallFunctionByName" }, // 2319577264
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ConvertStringToFrameRate, "ConvertStringToFrameRate" }, // 519515010
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorLoadAsset, "EditorLoadAsset" }, // 1863443551
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSaveAsset, "EditorSaveAsset" }, // 1512559223
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EditorSetGameView, "EditorSetGameView" }, // 2515901579
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_EnableDebugFocusPlane, "EnableDebugFocusPlane" }, // 51254031
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetBoundObjects, "GetBoundObjects" }, // 139266622
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequence, "GetCurrentLevelSequence" }, // 2217954663
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetCurrentLevelSequenceCurrentFrame, "GetCurrentLevelSequenceCurrentFrame" }, // 4074510391
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetDisplayRate, "GetDisplayRate" }, // 659447201
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecode, "GetLevelSequenceFrameAsTimecode" }, // 575917575
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceFrameAsTimecodeWithoutObject, "GetLevelSequenceFrameAsTimecodeWithoutObject" }, // 1110606201
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetLevelSequenceLengthInFrames, "GetLevelSequenceLengthInFrames" }, // 1476138979
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetNextUndoDescription, "GetNextUndoDescription" }, // 1309712306
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetObjectMetadataTags, "GetObjectMetadataTags" }, // 2614936080
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_GetUserSettings, "GetUserSettings" }, // 828009912
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ImportSnapshotTexture, "ImportSnapshotTexture" }, // 2777738295
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsCurrentLevelSequencePlaying, "IsCurrentLevelSequencePlaying" }, // 3133542406
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_IsGameRunning, "IsGameRunning" }, // 4237481946
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadata, "ModifyLevelSequenceMetadata" }, // 1464720821
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyLevelSequenceMetadataForSelects, "ModifyLevelSequenceMetadataForSelects" }, // 3014353818
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_ModifyObjectMetadataTags, "ModifyObjectMetadataTags" }, // 1550750206
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PauseCurrentLevelSequence, "PauseCurrentLevelSequence" }, // 1246038683
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PilotActor, "PilotActor" }, // 1830150040
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_PlayCurrentLevelSequence, "PlayCurrentLevelSequence" }, // 323900137
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SetCurrentLevelSequenceCurrentFrame, "SetCurrentLevelSequenceCurrentFrame" }, // 1308498195
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_SortAssetsByTimecodeAssetData, "SortAssetsByTimecodeAssetData" }, // 2903256440
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_TimecodeToFrameAmount, "TimecodeToFrameAmount" }, // 437752178
		{ &Z_Construct_UFunction_UVCamBlueprintFunctionLibrary_UpdatePostProcessSettingsForCapture, "UpdatePostProcessSettingsForCapture" }, // 3233369553
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "VCamBlueprintFunctionLibrary.h" },
		{ "ModuleRelativePath", "Public/VCamBlueprintFunctionLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamBlueprintFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::ClassParams = {
		&UVCamBlueprintFunctionLibrary::StaticClass,
		"VirtualCamera",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamBlueprintFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamBlueprintFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamBlueprintFunctionLibrary, 1442362887);
	template<> VIRTUALCAMERA_API UClass* StaticClass<UVCamBlueprintFunctionLibrary>()
	{
		return UVCamBlueprintFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamBlueprintFunctionLibrary(Z_Construct_UClass_UVCamBlueprintFunctionLibrary, &UVCamBlueprintFunctionLibrary::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("UVCamBlueprintFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamBlueprintFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
