// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamOutputRemoteSession.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamOutputRemoteSession() {}
// Cross Module References
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputRemoteSession_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputRemoteSession();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputProviderBase();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaOutput_NoRegister();
	REMOTESESSION_API UClass* Z_Construct_UClass_URemoteSessionMediaCapture_NoRegister();
// End Cross Module References
	void UVCamOutputRemoteSession::StaticRegisterNativesUVCamOutputRemoteSession()
	{
	}
	UClass* Z_Construct_UClass_UVCamOutputRemoteSession_NoRegister()
	{
		return UVCamOutputRemoteSession::StaticClass();
	}
	struct Z_Construct_UClass_UVCamOutputRemoteSession_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PortNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PortNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FromComposureOutputProviderIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FromComposureOutputProviderIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaOutput_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaOutput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaCapture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamOutputRemoteSession_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UVCamOutputProviderBase,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputRemoteSession_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Unreal Remote Output Provider" },
		{ "IncludePath", "VCamOutputRemoteSession.h" },
		{ "ModuleRelativePath", "Public/VCamOutputRemoteSession.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_PortNumber_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "// Network port number - change this only if connecting multiple RemoteSession devices to the same PC\n" },
		{ "ModuleRelativePath", "Public/VCamOutputRemoteSession.h" },
		{ "ToolTip", "Network port number - change this only if connecting multiple RemoteSession devices to the same PC" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_PortNumber = { "PortNumber", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputRemoteSession, PortNumber), METADATA_PARAMS(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_PortNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_PortNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_FromComposureOutputProviderIndex_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "// If using the output from a Composure Output Provider, specify it here\n" },
		{ "ModuleRelativePath", "Public/VCamOutputRemoteSession.h" },
		{ "ToolTip", "If using the output from a Composure Output Provider, specify it here" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_FromComposureOutputProviderIndex = { "FromComposureOutputProviderIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputRemoteSession, FromComposureOutputProviderIndex), METADATA_PARAMS(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_FromComposureOutputProviderIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_FromComposureOutputProviderIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaOutput_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamOutputRemoteSession.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaOutput = { "MediaOutput", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputRemoteSession, MediaOutput), Z_Construct_UClass_URemoteSessionMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaOutput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaOutput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaCapture_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamOutputRemoteSession.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaCapture = { "MediaCapture", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputRemoteSession, MediaCapture), Z_Construct_UClass_URemoteSessionMediaCapture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaCapture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVCamOutputRemoteSession_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_PortNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_FromComposureOutputProviderIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaOutput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputRemoteSession_Statics::NewProp_MediaCapture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamOutputRemoteSession_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamOutputRemoteSession>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamOutputRemoteSession_Statics::ClassParams = {
		&UVCamOutputRemoteSession::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVCamOutputRemoteSession_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputRemoteSession_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamOutputRemoteSession()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamOutputRemoteSession_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamOutputRemoteSession, 3763697504);
	template<> VCAMCORE_API UClass* StaticClass<UVCamOutputRemoteSession>()
	{
		return UVCamOutputRemoteSession::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamOutputRemoteSession(Z_Construct_UClass_UVCamOutputRemoteSession, &UVCamOutputRemoteSession::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamOutputRemoteSession"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamOutputRemoteSession);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
