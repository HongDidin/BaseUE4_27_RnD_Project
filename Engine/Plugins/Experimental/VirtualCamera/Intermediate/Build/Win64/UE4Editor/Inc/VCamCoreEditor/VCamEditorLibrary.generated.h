// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UVCamComponent;
#ifdef VCAMCOREEDITOR_VCamEditorLibrary_generated_h
#error "VCamEditorLibrary.generated.h already included, missing '#pragma once' in VCamEditorLibrary.h"
#endif
#define VCAMCOREEDITOR_VCamEditorLibrary_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetAllVCamComponentsInLevel);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetAllVCamComponentsInLevel);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamEditorLibrary(); \
	friend struct Z_Construct_UClass_UVCamEditorLibrary_Statics; \
public: \
	DECLARE_CLASS(UVCamEditorLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCoreEditor"), NO_API) \
	DECLARE_SERIALIZER(UVCamEditorLibrary)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUVCamEditorLibrary(); \
	friend struct Z_Construct_UClass_UVCamEditorLibrary_Statics; \
public: \
	DECLARE_CLASS(UVCamEditorLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCoreEditor"), NO_API) \
	DECLARE_SERIALIZER(UVCamEditorLibrary)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamEditorLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamEditorLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamEditorLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamEditorLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamEditorLibrary(UVCamEditorLibrary&&); \
	NO_API UVCamEditorLibrary(const UVCamEditorLibrary&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamEditorLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamEditorLibrary(UVCamEditorLibrary&&); \
	NO_API UVCamEditorLibrary(const UVCamEditorLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamEditorLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamEditorLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamEditorLibrary)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_12_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCOREEDITOR_API UClass* StaticClass<class UVCamEditorLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCoreEditor_Public_VCamEditorLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
