// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_VCamOutputComposure_generated_h
#error "VCamOutputComposure.generated.h already included, missing '#pragma once' in VCamOutputComposure.h"
#endif
#define VCAMCORE_VCamOutputComposure_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamOutputComposure(); \
	friend struct Z_Construct_UClass_UVCamOutputComposure_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputComposure, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputComposure)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUVCamOutputComposure(); \
	friend struct Z_Construct_UClass_UVCamOutputComposure_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputComposure, UVCamOutputProviderBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputComposure)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputComposure(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamOutputComposure) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputComposure); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputComposure); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputComposure(UVCamOutputComposure&&); \
	NO_API UVCamOutputComposure(const UVCamOutputComposure&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputComposure() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputComposure(UVCamOutputComposure&&); \
	NO_API UVCamOutputComposure(const UVCamOutputComposure&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputComposure); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputComposure); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVCamOutputComposure)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_14_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamOutputComposure>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputComposure_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
