// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UVCamComponent;
#ifdef VCAMCORE_VCamModifierInterface_generated_h
#error "VCamModifierInterface.generated.h already included, missing '#pragma once' in VCamModifierInterface.h"
#endif
#define VCAMCORE_VCamModifierInterface_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_RPC_WRAPPERS \
	virtual void OnVCamComponentChanged_Implementation(UVCamComponent* VCam) {}; \
 \
	DECLARE_FUNCTION(execOnVCamComponentChanged);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnVCamComponentChanged);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_EVENT_PARMS \
	struct VCamModifierInterface_eventOnVCamComponentChanged_Parms \
	{ \
		UVCamComponent* VCam; \
	};


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VCAMCORE_API UVCamModifierInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamModifierInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VCAMCORE_API, UVCamModifierInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamModifierInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VCAMCORE_API UVCamModifierInterface(UVCamModifierInterface&&); \
	VCAMCORE_API UVCamModifierInterface(const UVCamModifierInterface&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	VCAMCORE_API UVCamModifierInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	VCAMCORE_API UVCamModifierInterface(UVCamModifierInterface&&); \
	VCAMCORE_API UVCamModifierInterface(const UVCamModifierInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(VCAMCORE_API, UVCamModifierInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamModifierInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamModifierInterface)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUVCamModifierInterface(); \
	friend struct Z_Construct_UClass_UVCamModifierInterface_Statics; \
public: \
	DECLARE_CLASS(UVCamModifierInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/VCamCore"), VCAMCORE_API) \
	DECLARE_SERIALIZER(UVCamModifierInterface)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IVCamModifierInterface() {} \
public: \
	typedef UVCamModifierInterface UClassType; \
	typedef IVCamModifierInterface ThisClass; \
	static void Execute_OnVCamComponentChanged(UObject* O, UVCamComponent* VCam); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_INCLASS_IINTERFACE \
protected: \
	virtual ~IVCamModifierInterface() {} \
public: \
	typedef UVCamModifierInterface UClassType; \
	typedef IVCamModifierInterface ThisClass; \
	static void Execute_OnVCamComponentChanged(UObject* O, UVCamComponent* VCam); \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_11_PROLOG \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h_14_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamModifierInterface>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierInterface_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
