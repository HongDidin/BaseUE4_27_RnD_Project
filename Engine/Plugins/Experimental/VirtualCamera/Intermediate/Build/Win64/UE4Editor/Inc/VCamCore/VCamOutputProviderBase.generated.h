// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UUserWidget;
class UCineCameraComponent;
#ifdef VCAMCORE_VCamOutputProviderBase_generated_h
#error "VCamOutputProviderBase.generated.h already included, missing '#pragma once' in VCamOutputProviderBase.h"
#endif
#define VCAMCORE_VCamOutputProviderBase_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetUMGClass); \
	DECLARE_FUNCTION(execSetTargetCamera); \
	DECLARE_FUNCTION(execIsInitialized); \
	DECLARE_FUNCTION(execIsActive); \
	DECLARE_FUNCTION(execSetActive);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetUMGClass); \
	DECLARE_FUNCTION(execSetTargetCamera); \
	DECLARE_FUNCTION(execIsInitialized); \
	DECLARE_FUNCTION(execIsActive); \
	DECLARE_FUNCTION(execSetActive);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamOutputProviderBase(); \
	friend struct Z_Construct_UClass_UVCamOutputProviderBase_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputProviderBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputProviderBase)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUVCamOutputProviderBase(); \
	friend struct Z_Construct_UClass_UVCamOutputProviderBase_Statics; \
public: \
	DECLARE_CLASS(UVCamOutputProviderBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamOutputProviderBase)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamOutputProviderBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamOutputProviderBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputProviderBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputProviderBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputProviderBase(UVCamOutputProviderBase&&); \
	NO_API UVCamOutputProviderBase(const UVCamOutputProviderBase&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamOutputProviderBase(UVCamOutputProviderBase&&); \
	NO_API UVCamOutputProviderBase(const UVCamOutputProviderBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamOutputProviderBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamOutputProviderBase); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UVCamOutputProviderBase)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bIsActive() { return STRUCT_OFFSET(UVCamOutputProviderBase, bIsActive); } \
	FORCEINLINE static uint32 __PPO__bInitialized() { return STRUCT_OFFSET(UVCamOutputProviderBase, bInitialized); } \
	FORCEINLINE static uint32 __PPO__UMGWidget() { return STRUCT_OFFSET(UVCamOutputProviderBase, UMGWidget); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_23_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamOutputProviderBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamOutputProviderBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
