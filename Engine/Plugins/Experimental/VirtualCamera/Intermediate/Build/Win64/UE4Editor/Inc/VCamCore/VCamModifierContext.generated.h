// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_VCamModifierContext_generated_h
#error "VCamModifierContext.generated.h already included, missing '#pragma once' in VCamModifierContext.h"
#endif
#define VCAMCORE_VCamModifierContext_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamModifierContext(); \
	friend struct Z_Construct_UClass_UVCamModifierContext_Statics; \
public: \
	DECLARE_CLASS(UVCamModifierContext, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamModifierContext)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUVCamModifierContext(); \
	friend struct Z_Construct_UClass_UVCamModifierContext_Statics; \
public: \
	DECLARE_CLASS(UVCamModifierContext, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamModifierContext)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamModifierContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamModifierContext) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamModifierContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamModifierContext); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamModifierContext(UVCamModifierContext&&); \
	NO_API UVCamModifierContext(const UVCamModifierContext&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamModifierContext(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamModifierContext(UVCamModifierContext&&); \
	NO_API UVCamModifierContext(const UVCamModifierContext&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamModifierContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamModifierContext); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamModifierContext)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_8_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamModifierContext>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifierContext_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
