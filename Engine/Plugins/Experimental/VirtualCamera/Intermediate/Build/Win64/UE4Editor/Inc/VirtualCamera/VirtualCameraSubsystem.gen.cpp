// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/VirtualCameraSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVirtualCameraSubsystem() {}
// Cross Module References
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature();
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraSubsystem_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UEngineSubsystem();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraController_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_ULevelSequencePlaybackController_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics
	{
		struct _Script_VirtualCamera_eventOnSelectedViewportActor_Parms
		{
			AActor* SelectedActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::NewProp_SelectedActor = { "SelectedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_VirtualCamera_eventOnSelectedViewportActor_Parms, SelectedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::NewProp_SelectedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnSelectedViewportActor__DelegateSignature", nullptr, nullptr, sizeof(_Script_VirtualCamera_eventOnSelectedViewportActor_Parms), Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics
	{
		struct _Script_VirtualCamera_eventOnSelectedAnyActor_Parms
		{
			AActor* SelectedActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::NewProp_SelectedActor = { "SelectedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_VirtualCamera_eventOnSelectedAnyActor_Parms, SelectedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::NewProp_SelectedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnSelectedAnyActor__DelegateSignature", nullptr, nullptr, sizeof(_Script_VirtualCamera_eventOnSelectedAnyActor_Parms), Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnStreamStopped__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "OnStreamStarted__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UVirtualCameraSubsystem::execSetVirtualCameraController)
	{
		P_GET_TINTERFACE(IVirtualCameraController,Z_Param_VirtualCamera);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetVirtualCameraController(Z_Param_VirtualCamera);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVirtualCameraSubsystem::execGetVirtualCameraController)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TScriptInterface<IVirtualCameraController>*)Z_Param__Result=P_THIS->GetVirtualCameraController();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVirtualCameraSubsystem::execHandleSelectObjectEvent)
	{
		P_GET_OBJECT(UObject,Z_Param_SelectedObject);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleSelectObjectEvent(Z_Param_SelectedObject);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVirtualCameraSubsystem::execHandleSelectionChangedEvent)
	{
		P_GET_OBJECT(UObject,Z_Param_SelectedObject);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->HandleSelectionChangedEvent(Z_Param_SelectedObject);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVirtualCameraSubsystem::execIsStreaming)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsStreaming();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVirtualCameraSubsystem::execStopStreaming)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->StopStreaming();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVirtualCameraSubsystem::execStartStreaming)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->StartStreaming();
		P_NATIVE_END;
	}
	void UVirtualCameraSubsystem::StaticRegisterNativesUVirtualCameraSubsystem()
	{
		UClass* Class = UVirtualCameraSubsystem::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetVirtualCameraController", &UVirtualCameraSubsystem::execGetVirtualCameraController },
			{ "HandleSelectionChangedEvent", &UVirtualCameraSubsystem::execHandleSelectionChangedEvent },
			{ "HandleSelectObjectEvent", &UVirtualCameraSubsystem::execHandleSelectObjectEvent },
			{ "IsStreaming", &UVirtualCameraSubsystem::execIsStreaming },
			{ "SetVirtualCameraController", &UVirtualCameraSubsystem::execSetVirtualCameraController },
			{ "StartStreaming", &UVirtualCameraSubsystem::execStartStreaming },
			{ "StopStreaming", &UVirtualCameraSubsystem::execStopStreaming },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics
	{
		struct VirtualCameraSubsystem_eventGetVirtualCameraController_Parms
		{
			TScriptInterface<IVirtualCameraController> ReturnValue;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0014000000000580, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VirtualCameraSubsystem_eventGetVirtualCameraController_Parms, ReturnValue), Z_Construct_UClass_UVirtualCameraController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraSubsystem, nullptr, "GetVirtualCameraController", nullptr, nullptr, sizeof(VirtualCameraSubsystem_eventGetVirtualCameraController_Parms), Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics
	{
		struct VirtualCameraSubsystem_eventHandleSelectionChangedEvent_Parms
		{
			UObject* SelectedObject;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::NewProp_SelectedObject = { "SelectedObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VirtualCameraSubsystem_eventHandleSelectionChangedEvent_Parms, SelectedObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::NewProp_SelectedObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraSubsystem, nullptr, "HandleSelectionChangedEvent", nullptr, nullptr, sizeof(VirtualCameraSubsystem_eventHandleSelectionChangedEvent_Parms), Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics
	{
		struct VirtualCameraSubsystem_eventHandleSelectObjectEvent_Parms
		{
			UObject* SelectedObject;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectedObject;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::NewProp_SelectedObject = { "SelectedObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VirtualCameraSubsystem_eventHandleSelectObjectEvent_Parms, SelectedObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::NewProp_SelectedObject,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraSubsystem, nullptr, "HandleSelectObjectEvent", nullptr, nullptr, sizeof(VirtualCameraSubsystem_eventHandleSelectObjectEvent_Parms), Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics
	{
		struct VirtualCameraSubsystem_eventIsStreaming_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VirtualCameraSubsystem_eventIsStreaming_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VirtualCameraSubsystem_eventIsStreaming_Parms), &Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraSubsystem, nullptr, "IsStreaming", nullptr, nullptr, sizeof(VirtualCameraSubsystem_eventIsStreaming_Parms), Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics
	{
		struct VirtualCameraSubsystem_eventSetVirtualCameraController_Parms
		{
			TScriptInterface<IVirtualCameraController> VirtualCamera;
		};
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_VirtualCamera;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::NewProp_VirtualCamera = { "VirtualCamera", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VirtualCameraSubsystem_eventSetVirtualCameraController_Parms, VirtualCamera), Z_Construct_UClass_UVirtualCameraController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::NewProp_VirtualCamera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraSubsystem, nullptr, "SetVirtualCameraController", nullptr, nullptr, sizeof(VirtualCameraSubsystem_eventSetVirtualCameraController_Parms), Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics
	{
		struct VirtualCameraSubsystem_eventStartStreaming_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VirtualCameraSubsystem_eventStartStreaming_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VirtualCameraSubsystem_eventStartStreaming_Parms), &Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraSubsystem, nullptr, "StartStreaming", nullptr, nullptr, sizeof(VirtualCameraSubsystem_eventStartStreaming_Parms), Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics
	{
		struct VirtualCameraSubsystem_eventStopStreaming_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VirtualCameraSubsystem_eventStopStreaming_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VirtualCameraSubsystem_eventStopStreaming_Parms), &Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraSubsystem, nullptr, "StopStreaming", nullptr, nullptr, sizeof(VirtualCameraSubsystem_eventStopStreaming_Parms), Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVirtualCameraSubsystem_NoRegister()
	{
		return UVirtualCameraSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UVirtualCameraSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SequencePlaybackController_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SequencePlaybackController;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStreamStartedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStreamStartedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnStreamStoppedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnStreamStoppedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSelectedAnyActorDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSelectedAnyActorDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnSelectedActorInViewportDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnSelectedActorInViewportDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActiveCameraController_MetaData[];
#endif
		static const UE4CodeGen_Private::FInterfacePropertyParams NewProp_ActiveCameraController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVirtualCameraSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEngineSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVirtualCameraSubsystem_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVirtualCameraSubsystem_GetVirtualCameraController, "GetVirtualCameraController" }, // 3999035110
		{ &Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectionChangedEvent, "HandleSelectionChangedEvent" }, // 182931623
		{ &Z_Construct_UFunction_UVirtualCameraSubsystem_HandleSelectObjectEvent, "HandleSelectObjectEvent" }, // 4265348567
		{ &Z_Construct_UFunction_UVirtualCameraSubsystem_IsStreaming, "IsStreaming" }, // 2485842612
		{ &Z_Construct_UFunction_UVirtualCameraSubsystem_SetVirtualCameraController, "SetVirtualCameraController" }, // 1823497930
		{ &Z_Construct_UFunction_UVirtualCameraSubsystem_StartStreaming, "StartStreaming" }, // 2196959694
		{ &Z_Construct_UFunction_UVirtualCameraSubsystem_StopStreaming, "StopStreaming" }, // 4102262459
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraSubsystem_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Category", "VirtualCamera" },
		{ "DisplayName", "VirtualCameraSubsystem" },
		{ "IncludePath", "VirtualCameraSubsystem.h" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_SequencePlaybackController_MetaData[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_SequencePlaybackController = { "SequencePlaybackController", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraSubsystem, SequencePlaybackController), Z_Construct_UClass_ULevelSequencePlaybackController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_SequencePlaybackController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_SequencePlaybackController_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStartedDelegate_MetaData[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStartedDelegate = { "OnStreamStartedDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraSubsystem, OnStreamStartedDelegate), Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStarted__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStartedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStartedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStoppedDelegate_MetaData[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStoppedDelegate = { "OnStreamStoppedDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraSubsystem, OnStreamStoppedDelegate), Z_Construct_UDelegateFunction_VirtualCamera_OnStreamStopped__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStoppedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStoppedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedAnyActorDelegate_MetaData[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedAnyActorDelegate = { "OnSelectedAnyActorDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraSubsystem, OnSelectedAnyActorDelegate), Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedAnyActor__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedAnyActorDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedAnyActorDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedActorInViewportDelegate_MetaData[] = {
		{ "Category", "VirtualCamera | Streaming" },
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedActorInViewportDelegate = { "OnSelectedActorInViewportDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraSubsystem, OnSelectedActorInViewportDelegate), Z_Construct_UDelegateFunction_VirtualCamera_OnSelectedViewportActor__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedActorInViewportDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedActorInViewportDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_ActiveCameraController_MetaData[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FInterfacePropertyParams Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_ActiveCameraController = { "ActiveCameraController", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::Interface, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraSubsystem, ActiveCameraController), Z_Construct_UClass_UVirtualCameraController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_ActiveCameraController_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_ActiveCameraController_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVirtualCameraSubsystem_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_SequencePlaybackController,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStartedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnStreamStoppedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedAnyActorDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_OnSelectedActorInViewportDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraSubsystem_Statics::NewProp_ActiveCameraController,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVirtualCameraSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVirtualCameraSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVirtualCameraSubsystem_Statics::ClassParams = {
		&UVirtualCameraSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UVirtualCameraSubsystem_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVirtualCameraSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVirtualCameraSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVirtualCameraSubsystem, 192968472);
	template<> VIRTUALCAMERA_API UClass* StaticClass<UVirtualCameraSubsystem>()
	{
		return UVirtualCameraSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVirtualCameraSubsystem(Z_Construct_UClass_UVirtualCameraSubsystem, &UVirtualCameraSubsystem::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("UVirtualCameraSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVirtualCameraSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
