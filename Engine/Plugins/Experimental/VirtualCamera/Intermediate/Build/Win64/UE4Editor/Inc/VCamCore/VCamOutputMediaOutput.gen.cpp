// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamOutputMediaOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamOutputMediaOutput() {}
// Cross Module References
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputMediaOutput_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputMediaOutput();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputProviderBase();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaOutput_NoRegister();
	MEDIAIOCORE_API UClass* Z_Construct_UClass_UMediaCapture_NoRegister();
// End Cross Module References
	void UVCamOutputMediaOutput::StaticRegisterNativesUVCamOutputMediaOutput()
	{
	}
	UClass* Z_Construct_UClass_UVCamOutputMediaOutput_NoRegister()
	{
		return UVCamOutputMediaOutput::StaticClass();
	}
	struct Z_Construct_UClass_UVCamOutputMediaOutput_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutputConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FromComposureOutputProviderIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FromComposureOutputProviderIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MediaCapture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MediaCapture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamOutputMediaOutput_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UVCamOutputProviderBase,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputMediaOutput_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Media Output Provider" },
		{ "IncludePath", "VCamOutputMediaOutput.h" },
		{ "ModuleRelativePath", "Public/VCamOutputMediaOutput.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_OutputConfig_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "// Media Output configuration asset\n" },
		{ "ModuleRelativePath", "Public/VCamOutputMediaOutput.h" },
		{ "ToolTip", "Media Output configuration asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_OutputConfig = { "OutputConfig", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputMediaOutput, OutputConfig), Z_Construct_UClass_UMediaOutput_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_OutputConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_OutputConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_FromComposureOutputProviderIndex_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "// If using the output from a Composure Output Provider, specify it here\n" },
		{ "ModuleRelativePath", "Public/VCamOutputMediaOutput.h" },
		{ "ToolTip", "If using the output from a Composure Output Provider, specify it here" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_FromComposureOutputProviderIndex = { "FromComposureOutputProviderIndex", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputMediaOutput, FromComposureOutputProviderIndex), METADATA_PARAMS(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_FromComposureOutputProviderIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_FromComposureOutputProviderIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_MediaCapture_MetaData[] = {
		{ "ModuleRelativePath", "Public/VCamOutputMediaOutput.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_MediaCapture = { "MediaCapture", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputMediaOutput, MediaCapture), Z_Construct_UClass_UMediaCapture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_MediaCapture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_MediaCapture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVCamOutputMediaOutput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_OutputConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_FromComposureOutputProviderIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputMediaOutput_Statics::NewProp_MediaCapture,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamOutputMediaOutput_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamOutputMediaOutput>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamOutputMediaOutput_Statics::ClassParams = {
		&UVCamOutputMediaOutput::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVCamOutputMediaOutput_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputMediaOutput_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamOutputMediaOutput()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamOutputMediaOutput_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamOutputMediaOutput, 1532367);
	template<> VCAMCORE_API UClass* StaticClass<UVCamOutputMediaOutput>()
	{
		return UVCamOutputMediaOutput::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamOutputMediaOutput(Z_Construct_UClass_UVCamOutputMediaOutput, &UVCamOutputMediaOutput::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamOutputMediaOutput"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamOutputMediaOutput);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
