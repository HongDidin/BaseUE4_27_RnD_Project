// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/VirtualCameraMovementComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVirtualCameraMovementComponent() {}
// Cross Module References
	VIRTUALCAMERA_API UFunction* Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraMovementComponent_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraMovementComponent();
	ENGINE_API UClass* Z_Construct_UClass_UPawnMovementComponent();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UPrimitiveComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/VirtualCameraMovementComponent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_VirtualCamera, nullptr, "VirtualCameraResetOffsetsDelegate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_VirtualCamera_VirtualCameraResetOffsetsDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UVirtualCameraMovementComponent::StaticRegisterNativesUVirtualCameraMovementComponent()
	{
	}
	UClass* Z_Construct_UClass_UVirtualCameraMovementComponent_NoRegister()
	{
		return UVirtualCameraMovementComponent::StaticClass();
	}
	struct Z_Construct_UClass_UVirtualCameraMovementComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootUpdatedComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootUpdatedComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootUpdatedPrimitive_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootUpdatedPrimitive;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPawnMovementComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VirtualCameraMovementComponent.h" },
		{ "ModuleRelativePath", "Public/VirtualCameraMovementComponent.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedComponent_MetaData[] = {
		{ "Comment", "/**\n\x09 * The component we move and update when the input is coming from the controller.\n\x09 * If this is null UpdatedComponent will be used instead.\n\x09 * @see UpdatedComponent\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraMovementComponent.h" },
		{ "ToolTip", "The component we move and update when the input is coming from the controller.\nIf this is null UpdatedComponent will be used instead.\n@see UpdatedComponent" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedComponent = { "RootUpdatedComponent", nullptr, (EPropertyFlags)0x0040000000282008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraMovementComponent, RootUpdatedComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedPrimitive_MetaData[] = {
		{ "Comment", "/**\n\x09 * RootUpdatedComponent, cast as a UPrimitiveComponent. May be invalid if RootUpdatedComponent was null or not a UPrimitiveComponent.\n\x09 */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VirtualCameraMovementComponent.h" },
		{ "ToolTip", "RootUpdatedComponent, cast as a UPrimitiveComponent. May be invalid if RootUpdatedComponent was null or not a UPrimitiveComponent." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedPrimitive = { "RootUpdatedPrimitive", nullptr, (EPropertyFlags)0x0040000000282008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVirtualCameraMovementComponent, RootUpdatedPrimitive), Z_Construct_UClass_UPrimitiveComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedPrimitive_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedPrimitive_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::NewProp_RootUpdatedPrimitive,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVirtualCameraMovementComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::ClassParams = {
		&UVirtualCameraMovementComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::PropPointers),
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVirtualCameraMovementComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVirtualCameraMovementComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVirtualCameraMovementComponent, 2269946950);
	template<> VIRTUALCAMERA_API UClass* StaticClass<UVirtualCameraMovementComponent>()
	{
		return UVirtualCameraMovementComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVirtualCameraMovementComponent(Z_Construct_UClass_UVirtualCameraMovementComponent, &UVirtualCameraMovementComponent::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("UVirtualCameraMovementComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVirtualCameraMovementComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
