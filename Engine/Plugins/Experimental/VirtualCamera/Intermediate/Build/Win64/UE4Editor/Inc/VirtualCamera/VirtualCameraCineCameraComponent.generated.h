// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VIRTUALCAMERA_VirtualCameraCineCameraComponent_generated_h
#error "VirtualCameraCineCameraComponent.generated.h already included, missing '#pragma once' in VirtualCameraCineCameraComponent.h"
#endif
#define VIRTUALCAMERA_VirtualCameraCineCameraComponent_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVirtualCameraCineCameraComponent(); \
	friend struct Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics; \
public: \
	DECLARE_CLASS(UVirtualCameraCineCameraComponent, UCineCameraComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVirtualCameraCineCameraComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUVirtualCameraCineCameraComponent(); \
	friend struct Z_Construct_UClass_UVirtualCameraCineCameraComponent_Statics; \
public: \
	DECLARE_CLASS(UVirtualCameraCineCameraComponent, UCineCameraComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(UVirtualCameraCineCameraComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraCineCameraComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVirtualCameraCineCameraComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraCineCameraComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraCineCameraComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraCineCameraComponent(UVirtualCameraCineCameraComponent&&); \
	NO_API UVirtualCameraCineCameraComponent(const UVirtualCameraCineCameraComponent&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVirtualCameraCineCameraComponent() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVirtualCameraCineCameraComponent(UVirtualCameraCineCameraComponent&&); \
	NO_API UVirtualCameraCineCameraComponent(const UVirtualCameraCineCameraComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVirtualCameraCineCameraComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVirtualCameraCineCameraComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVirtualCameraCineCameraComponent)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_10_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VirtualCameraCineCameraComponent."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class UVirtualCameraCineCameraComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraCineCameraComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
