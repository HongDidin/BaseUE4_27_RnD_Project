// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCoreEditor/Public/VCamEditorLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamEditorLibrary() {}
// Cross Module References
	VCAMCOREEDITOR_API UClass* Z_Construct_UClass_UVCamEditorLibrary_NoRegister();
	VCAMCOREEDITOR_API UClass* Z_Construct_UClass_UVCamEditorLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_VCamCoreEditor();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UVCamEditorLibrary::execGetAllVCamComponentsInLevel)
	{
		P_GET_TARRAY_REF(UVCamComponent*,Z_Param_Out_VCamComponents);
		P_FINISH;
		P_NATIVE_BEGIN;
		UVCamEditorLibrary::GetAllVCamComponentsInLevel(Z_Param_Out_VCamComponents);
		P_NATIVE_END;
	}
	void UVCamEditorLibrary::StaticRegisterNativesUVCamEditorLibrary()
	{
		UClass* Class = UVCamEditorLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAllVCamComponentsInLevel", &UVCamEditorLibrary::execGetAllVCamComponentsInLevel },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics
	{
		struct VCamEditorLibrary_eventGetAllVCamComponentsInLevel_Parms
		{
			TArray<UVCamComponent*> VCamComponents;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VCamComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VCamComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_VCamComponents;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::NewProp_VCamComponents_Inner = { "VCamComponents", nullptr, (EPropertyFlags)0x0000000000080000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UVCamComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::NewProp_VCamComponents_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::NewProp_VCamComponents = { "VCamComponents", nullptr, (EPropertyFlags)0x0010008000000180, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamEditorLibrary_eventGetAllVCamComponentsInLevel_Parms, VCamComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::NewProp_VCamComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::NewProp_VCamComponents_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::NewProp_VCamComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::NewProp_VCamComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "Comment", "/**\n\x09 * Find all loaded VCam Components own by an actor in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n\x09 * @param VCamComponents Output List of found VCamComponents\n\x09 */" },
		{ "ModuleRelativePath", "Public/VCamEditorLibrary.h" },
		{ "ToolTip", "Find all loaded VCam Components own by an actor in the world editor. Exclude actor that are pending kill, in PIE, PreviewEditor, ...\n@param VCamComponents Output List of found VCamComponents" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamEditorLibrary, nullptr, "GetAllVCamComponentsInLevel", nullptr, nullptr, sizeof(VCamEditorLibrary_eventGetAllVCamComponentsInLevel_Parms), Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVCamEditorLibrary_NoRegister()
	{
		return UVCamEditorLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UVCamEditorLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamEditorLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCoreEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVCamEditorLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVCamEditorLibrary_GetAllVCamComponentsInLevel, "GetAllVCamComponentsInLevel" }, // 4022532456
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamEditorLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VCamEditorLibrary.h" },
		{ "ModuleRelativePath", "Public/VCamEditorLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamEditorLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamEditorLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamEditorLibrary_Statics::ClassParams = {
		&UVCamEditorLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamEditorLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamEditorLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamEditorLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamEditorLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamEditorLibrary, 3193650777);
	template<> VCAMCOREEDITOR_API UClass* StaticClass<UVCamEditorLibrary>()
	{
		return UVCamEditorLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamEditorLibrary(Z_Construct_UClass_UVCamEditorLibrary, &UVCamEditorLibrary::StaticClass, TEXT("/Script/VCamCoreEditor"), TEXT("UVCamEditorLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamEditorLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
