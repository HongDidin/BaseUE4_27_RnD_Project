// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamModifierContext.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamModifierContext() {}
// Cross Module References
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifierContext_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifierContext();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
// End Cross Module References
	void UVCamModifierContext::StaticRegisterNativesUVCamModifierContext()
	{
	}
	UClass* Z_Construct_UClass_UVCamModifierContext_NoRegister()
	{
		return UVCamModifierContext::StaticClass();
	}
	struct Z_Construct_UClass_UVCamModifierContext_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamModifierContext_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamModifierContext_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "VCamModifierContext.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VCamModifierContext.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamModifierContext_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamModifierContext>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamModifierContext_Statics::ClassParams = {
		&UVCamModifierContext::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamModifierContext_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamModifierContext_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamModifierContext()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamModifierContext_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamModifierContext, 345646210);
	template<> VCAMCORE_API UClass* StaticClass<UVCamModifierContext>()
	{
		return UVCamModifierContext::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamModifierContext(Z_Construct_UClass_UVCamModifierContext, &UVCamModifierContext::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamModifierContext"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamModifierContext);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
