// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamModifier.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamModifier() {}
// Cross Module References
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifier_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifier();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	LIVELINKINTERFACE_API UScriptStruct* Z_Construct_UScriptStruct_FLiveLinkCameraBlueprintData();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamComponent_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamBlueprintModifier_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamBlueprintModifier();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamModifierContext_NoRegister();
	CINEMATICCAMERA_API UClass* Z_Construct_UClass_UCineCameraComponent_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UVCamModifier::execIsEnabled)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsEnabled();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamModifier::execSetEnabled)
	{
		P_GET_UBOOL(Z_Param_bNewEnabled);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetEnabled(Z_Param_bNewEnabled);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamModifier::execGetCurrentLiveLinkDataFromOwningComponent)
	{
		P_GET_STRUCT_REF(FLiveLinkCameraBlueprintData,Z_Param_Out_LiveLinkData);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->GetCurrentLiveLinkDataFromOwningComponent(Z_Param_Out_LiveLinkData);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamModifier::execGetOwningVCamComponent)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UVCamComponent**)Z_Param__Result=P_THIS->GetOwningVCamComponent();
		P_NATIVE_END;
	}
	void UVCamModifier::StaticRegisterNativesUVCamModifier()
	{
		UClass* Class = UVCamModifier::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetCurrentLiveLinkDataFromOwningComponent", &UVCamModifier::execGetCurrentLiveLinkDataFromOwningComponent },
			{ "GetOwningVCamComponent", &UVCamModifier::execGetOwningVCamComponent },
			{ "IsEnabled", &UVCamModifier::execIsEnabled },
			{ "SetEnabled", &UVCamModifier::execSetEnabled },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics
	{
		struct VCamModifier_eventGetCurrentLiveLinkDataFromOwningComponent_Parms
		{
			FLiveLinkCameraBlueprintData LiveLinkData;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LiveLinkData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::NewProp_LiveLinkData = { "LiveLinkData", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamModifier_eventGetCurrentLiveLinkDataFromOwningComponent_Parms, LiveLinkData), Z_Construct_UScriptStruct_FLiveLinkCameraBlueprintData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::NewProp_LiveLinkData,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamModifier, nullptr, "GetCurrentLiveLinkDataFromOwningComponent", nullptr, nullptr, sizeof(VCamModifier_eventGetCurrentLiveLinkDataFromOwningComponent_Parms), Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics
	{
		struct VCamModifier_eventGetOwningVCamComponent_Parms
		{
			UVCamComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamModifier_eventGetOwningVCamComponent_Parms, ReturnValue), Z_Construct_UClass_UVCamComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamModifier, nullptr, "GetOwningVCamComponent", nullptr, nullptr, sizeof(VCamModifier_eventGetOwningVCamComponent_Parms), Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics
	{
		struct VCamModifier_eventIsEnabled_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamModifier_eventIsEnabled_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamModifier_eventIsEnabled_Parms), &Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
		{ "ReturnDisplayName", "Enabled" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamModifier, nullptr, "IsEnabled", nullptr, nullptr, sizeof(VCamModifier_eventIsEnabled_Parms), Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamModifier_IsEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamModifier_IsEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics
	{
		struct VCamModifier_eventSetEnabled_Parms
		{
			bool bNewEnabled;
		};
		static void NewProp_bNewEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bNewEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::NewProp_bNewEnabled_SetBit(void* Obj)
	{
		((VCamModifier_eventSetEnabled_Parms*)Obj)->bNewEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::NewProp_bNewEnabled = { "bNewEnabled", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamModifier_eventSetEnabled_Parms), &Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::NewProp_bNewEnabled_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::NewProp_bNewEnabled,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamModifier, nullptr, "SetEnabled", nullptr, nullptr, sizeof(VCamModifier_eventSetEnabled_Parms), Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamModifier_SetEnabled()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamModifier_SetEnabled_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVCamModifier_NoRegister()
	{
		return UVCamModifier::StaticClass();
	}
	struct Z_Construct_UClass_UVCamModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVCamModifier_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVCamModifier_GetCurrentLiveLinkDataFromOwningComponent, "GetCurrentLiveLinkDataFromOwningComponent" }, // 2891441668
		{ &Z_Construct_UFunction_UVCamModifier_GetOwningVCamComponent, "GetOwningVCamComponent" }, // 2696185415
		{ &Z_Construct_UFunction_UVCamModifier_IsEnabled, "IsEnabled" }, // 893220457
		{ &Z_Construct_UFunction_UVCamModifier_SetEnabled, "SetEnabled" }, // 3081703121
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamModifier_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "VCamModifier.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamModifier_Statics::ClassParams = {
		&UVCamModifier::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamModifier, 3604278593);
	template<> VCAMCORE_API UClass* StaticClass<UVCamModifier>()
	{
		return UVCamModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamModifier(Z_Construct_UClass_UVCamModifier, &UVCamModifier::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamModifier);
	static FName NAME_UVCamBlueprintModifier_OnApply = FName(TEXT("OnApply"));
	void UVCamBlueprintModifier::OnApply(UVCamModifierContext* Context, UCineCameraComponent* CameraComponent, const float DeltaTime)
	{
		VCamBlueprintModifier_eventOnApply_Parms Parms;
		Parms.Context=Context;
		Parms.CameraComponent=CameraComponent;
		Parms.DeltaTime=DeltaTime;
		ProcessEvent(FindFunctionChecked(NAME_UVCamBlueprintModifier_OnApply),&Parms);
	}
	static FName NAME_UVCamBlueprintModifier_OnInitialize = FName(TEXT("OnInitialize"));
	void UVCamBlueprintModifier::OnInitialize(UVCamModifierContext* Context)
	{
		VCamBlueprintModifier_eventOnInitialize_Parms Parms;
		Parms.Context=Context;
		ProcessEvent(FindFunctionChecked(NAME_UVCamBlueprintModifier_OnInitialize),&Parms);
	}
	void UVCamBlueprintModifier::StaticRegisterNativesUVCamBlueprintModifier()
	{
	}
	struct Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Context;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeltaTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintModifier_eventOnApply_Parms, Context), Z_Construct_UClass_UVCamModifierContext_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_CameraComponent_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_CameraComponent = { "CameraComponent", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintModifier_eventOnApply_Parms, CameraComponent), Z_Construct_UClass_UCineCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_CameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_CameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_DeltaTime_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintModifier_eventOnApply_Parms, DeltaTime), METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_DeltaTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_DeltaTime_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_Context,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_CameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintModifier, nullptr, "OnApply", nullptr, nullptr, sizeof(VCamBlueprintModifier_eventOnApply_Parms), Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintModifier_OnApply()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintModifier_OnApply_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Context;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::NewProp_Context = { "Context", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamBlueprintModifier_eventOnInitialize_Parms, Context), Z_Construct_UClass_UVCamModifierContext_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::NewProp_Context,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamBlueprintModifier, nullptr, "OnInitialize", nullptr, nullptr, sizeof(VCamBlueprintModifier_eventOnInitialize_Parms), Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVCamBlueprintModifier_NoRegister()
	{
		return UVCamBlueprintModifier::StaticClass();
	}
	struct Z_Construct_UClass_UVCamBlueprintModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamBlueprintModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UVCamModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVCamBlueprintModifier_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVCamBlueprintModifier_OnApply, "OnApply" }, // 1045662647
		{ &Z_Construct_UFunction_UVCamBlueprintModifier_OnInitialize, "OnInitialize" }, // 4148530857
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamBlueprintModifier_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VCamModifier.h" },
		{ "ModuleRelativePath", "Public/VCamModifier.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamBlueprintModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamBlueprintModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamBlueprintModifier_Statics::ClassParams = {
		&UVCamBlueprintModifier::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamBlueprintModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamBlueprintModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamBlueprintModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamBlueprintModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamBlueprintModifier, 4078043851);
	template<> VCAMCORE_API UClass* StaticClass<UVCamBlueprintModifier>()
	{
		return UVCamBlueprintModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamBlueprintModifier(Z_Construct_UClass_UVCamBlueprintModifier, &UVCamBlueprintModifier::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamBlueprintModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamBlueprintModifier);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
