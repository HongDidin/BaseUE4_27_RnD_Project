// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VirtualCamera/Public/IVirtualCameraOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeIVirtualCameraOptions() {}
// Cross Module References
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraOptions_NoRegister();
	VIRTUALCAMERA_API UClass* Z_Construct_UClass_UVirtualCameraOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_VirtualCamera();
	COREUOBJECT_API UEnum* Z_Construct_UEnum_CoreUObject_EUnit();
// End Cross Module References
	DEFINE_FUNCTION(IVirtualCameraOptions::execIsFocusVisualizationAllowed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsFocusVisualizationAllowed_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IVirtualCameraOptions::execGetDesiredDistanceUnits)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(EUnit*)Z_Param__Result=P_THIS->GetDesiredDistanceUnits_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(IVirtualCameraOptions::execSetDesiredDistanceUnits)
	{
		P_GET_ENUM(EUnit,Z_Param_DesiredUnits);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetDesiredDistanceUnits_Implementation(EUnit(Z_Param_DesiredUnits));
		P_NATIVE_END;
	}
	EUnit IVirtualCameraOptions::GetDesiredDistanceUnits()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_GetDesiredDistanceUnits instead.");
		VirtualCameraOptions_eventGetDesiredDistanceUnits_Parms Parms;
		return Parms.ReturnValue;
	}
	bool IVirtualCameraOptions::IsFocusVisualizationAllowed()
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_IsFocusVisualizationAllowed instead.");
		VirtualCameraOptions_eventIsFocusVisualizationAllowed_Parms Parms;
		return Parms.ReturnValue;
	}
	void IVirtualCameraOptions::SetDesiredDistanceUnits(const EUnit DesiredUnits)
	{
		check(0 && "Do not directly call Event functions in Interfaces. Call Execute_SetDesiredDistanceUnits instead.");
	}
	void UVirtualCameraOptions::StaticRegisterNativesUVirtualCameraOptions()
	{
		UClass* Class = UVirtualCameraOptions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetDesiredDistanceUnits", &IVirtualCameraOptions::execGetDesiredDistanceUnits },
			{ "IsFocusVisualizationAllowed", &IVirtualCameraOptions::execIsFocusVisualizationAllowed },
			{ "SetDesiredDistanceUnits", &IVirtualCameraOptions::execSetDesiredDistanceUnits },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics
	{
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ReturnValue_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::NewProp_ReturnValue_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VirtualCameraOptions_eventGetDesiredDistanceUnits_Parms, ReturnValue), Z_Construct_UEnum_CoreUObject_EUnit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::NewProp_ReturnValue_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Options" },
		{ "Comment", "/**\n\x09 * Returns previously set unit of distance.\n\x09 * @return DesiredUnits - The unit that is used for distance measures like focus distance\n\x09 */" },
		{ "ModuleRelativePath", "Public/IVirtualCameraOptions.h" },
		{ "ToolTip", "Returns previously set unit of distance.\n@return DesiredUnits - The unit that is used for distance measures like focus distance" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraOptions, nullptr, "GetDesiredDistanceUnits", nullptr, nullptr, sizeof(VirtualCameraOptions_eventGetDesiredDistanceUnits_Parms), Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics
	{
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VirtualCameraOptions_eventIsFocusVisualizationAllowed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VirtualCameraOptions_eventIsFocusVisualizationAllowed_Parms), &Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Options" },
		{ "Comment", "/**\n\x09 * Checks whether or not focus visualization can activate\n\x09 * @return the current state of touch event visualization\n\x09 */" },
		{ "ModuleRelativePath", "Public/IVirtualCameraOptions.h" },
		{ "ToolTip", "Checks whether or not focus visualization can activate\n@return the current state of touch event visualization" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraOptions, nullptr, "IsFocusVisualizationAllowed", nullptr, nullptr, sizeof(VirtualCameraOptions_eventIsFocusVisualizationAllowed_Parms), Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics
	{
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DesiredUnits_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DesiredUnits_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DesiredUnits;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::NewProp_DesiredUnits_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::NewProp_DesiredUnits_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::NewProp_DesiredUnits = { "DesiredUnits", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VirtualCameraOptions_eventSetDesiredDistanceUnits_Parms, DesiredUnits), Z_Construct_UEnum_CoreUObject_EUnit, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::NewProp_DesiredUnits_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::NewProp_DesiredUnits_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::NewProp_DesiredUnits_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::NewProp_DesiredUnits,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::Function_MetaDataParams[] = {
		{ "Category", "VirtualCamera | Options" },
		{ "Comment", "/**\n\x09 * Sets unit of distance.\n\x09 * @param DesiredUnits - The new unit to use for distance measures like focus distance\n\x09 */" },
		{ "ModuleRelativePath", "Public/IVirtualCameraOptions.h" },
		{ "ToolTip", "Sets unit of distance.\n@param DesiredUnits - The new unit to use for distance measures like focus distance" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVirtualCameraOptions, nullptr, "SetDesiredDistanceUnits", nullptr, nullptr, sizeof(VirtualCameraOptions_eventSetDesiredDistanceUnits_Parms), Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVirtualCameraOptions_NoRegister()
	{
		return UVirtualCameraOptions::StaticClass();
	}
	struct Z_Construct_UClass_UVirtualCameraOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVirtualCameraOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_VirtualCamera,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVirtualCameraOptions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVirtualCameraOptions_GetDesiredDistanceUnits, "GetDesiredDistanceUnits" }, // 1582509614
		{ &Z_Construct_UFunction_UVirtualCameraOptions_IsFocusVisualizationAllowed, "IsFocusVisualizationAllowed" }, // 1871532406
		{ &Z_Construct_UFunction_UVirtualCameraOptions_SetDesiredDistanceUnits, "SetDesiredDistanceUnits" }, // 2214861856
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVirtualCameraOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/IVirtualCameraOptions.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVirtualCameraOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IVirtualCameraOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVirtualCameraOptions_Statics::ClassParams = {
		&UVirtualCameraOptions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UVirtualCameraOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVirtualCameraOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVirtualCameraOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVirtualCameraOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVirtualCameraOptions, 2110293895);
	template<> VIRTUALCAMERA_API UClass* StaticClass<UVirtualCameraOptions>()
	{
		return UVirtualCameraOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVirtualCameraOptions(Z_Construct_UClass_UVirtualCameraOptions, &UVirtualCameraOptions::StaticClass, TEXT("/Script/VirtualCamera"), TEXT("UVirtualCameraOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVirtualCameraOptions);
	static FName NAME_UVirtualCameraOptions_GetDesiredDistanceUnits = FName(TEXT("GetDesiredDistanceUnits"));
	EUnit IVirtualCameraOptions::Execute_GetDesiredDistanceUnits(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVirtualCameraOptions::StaticClass()));
		VirtualCameraOptions_eventGetDesiredDistanceUnits_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UVirtualCameraOptions_GetDesiredDistanceUnits);
		if (Func)
		{
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IVirtualCameraOptions*)(O->GetNativeInterfaceAddress(UVirtualCameraOptions::StaticClass())))
		{
			Parms.ReturnValue = I->GetDesiredDistanceUnits_Implementation();
		}
		return Parms.ReturnValue;
	}
	static FName NAME_UVirtualCameraOptions_IsFocusVisualizationAllowed = FName(TEXT("IsFocusVisualizationAllowed"));
	bool IVirtualCameraOptions::Execute_IsFocusVisualizationAllowed(UObject* O)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVirtualCameraOptions::StaticClass()));
		VirtualCameraOptions_eventIsFocusVisualizationAllowed_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UVirtualCameraOptions_IsFocusVisualizationAllowed);
		if (Func)
		{
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IVirtualCameraOptions*)(O->GetNativeInterfaceAddress(UVirtualCameraOptions::StaticClass())))
		{
			Parms.ReturnValue = I->IsFocusVisualizationAllowed_Implementation();
		}
		return Parms.ReturnValue;
	}
	static FName NAME_UVirtualCameraOptions_SetDesiredDistanceUnits = FName(TEXT("SetDesiredDistanceUnits"));
	void IVirtualCameraOptions::Execute_SetDesiredDistanceUnits(UObject* O, const EUnit DesiredUnits)
	{
		check(O != NULL);
		check(O->GetClass()->ImplementsInterface(UVirtualCameraOptions::StaticClass()));
		VirtualCameraOptions_eventSetDesiredDistanceUnits_Parms Parms;
		UFunction* const Func = O->FindFunction(NAME_UVirtualCameraOptions_SetDesiredDistanceUnits);
		if (Func)
		{
			Parms.DesiredUnits=DesiredUnits;
			O->ProcessEvent(Func, &Parms);
		}
		else if (auto I = (IVirtualCameraOptions*)(O->GetNativeInterfaceAddress(UVirtualCameraOptions::StaticClass())))
		{
			I->SetDesiredDistanceUnits_Implementation(DesiredUnits);
		}
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
