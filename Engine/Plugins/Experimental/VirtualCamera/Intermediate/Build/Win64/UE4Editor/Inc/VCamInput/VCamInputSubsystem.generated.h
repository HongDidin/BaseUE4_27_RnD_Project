// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FKey;
#ifdef VCAMINPUT_VCamInputSubsystem_generated_h
#error "VCamInputSubsystem.generated.h already included, missing '#pragma once' in VCamInputSubsystem.h"
#endif
#define VCAMINPUT_VCamInputSubsystem_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBindMouseWheelEvent); \
	DECLARE_FUNCTION(execBindMouseDoubleClickEvent); \
	DECLARE_FUNCTION(execBindMouseButtonUpEvent); \
	DECLARE_FUNCTION(execBindMouseButtonDownEvent); \
	DECLARE_FUNCTION(execBindMouseMoveEvent); \
	DECLARE_FUNCTION(execBindAnalogEvent); \
	DECLARE_FUNCTION(execBindKeyUpEvent); \
	DECLARE_FUNCTION(execBindKeyDownEvent); \
	DECLARE_FUNCTION(execGetShouldConsumeGamepadInput); \
	DECLARE_FUNCTION(execSetShouldConsumeGamepadInput);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBindMouseWheelEvent); \
	DECLARE_FUNCTION(execBindMouseDoubleClickEvent); \
	DECLARE_FUNCTION(execBindMouseButtonUpEvent); \
	DECLARE_FUNCTION(execBindMouseButtonDownEvent); \
	DECLARE_FUNCTION(execBindMouseMoveEvent); \
	DECLARE_FUNCTION(execBindAnalogEvent); \
	DECLARE_FUNCTION(execBindKeyUpEvent); \
	DECLARE_FUNCTION(execBindKeyDownEvent); \
	DECLARE_FUNCTION(execGetShouldConsumeGamepadInput); \
	DECLARE_FUNCTION(execSetShouldConsumeGamepadInput);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamInputSubsystem(); \
	friend struct Z_Construct_UClass_UVCamInputSubsystem_Statics; \
public: \
	DECLARE_CLASS(UVCamInputSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamInput"), NO_API) \
	DECLARE_SERIALIZER(UVCamInputSubsystem)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUVCamInputSubsystem(); \
	friend struct Z_Construct_UClass_UVCamInputSubsystem_Statics; \
public: \
	DECLARE_CLASS(UVCamInputSubsystem, UEngineSubsystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamInput"), NO_API) \
	DECLARE_SERIALIZER(UVCamInputSubsystem)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamInputSubsystem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamInputSubsystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamInputSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamInputSubsystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamInputSubsystem(UVCamInputSubsystem&&); \
	NO_API UVCamInputSubsystem(const UVCamInputSubsystem&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamInputSubsystem(UVCamInputSubsystem&&); \
	NO_API UVCamInputSubsystem(const UVCamInputSubsystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamInputSubsystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamInputSubsystem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVCamInputSubsystem)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_17_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMINPUT_API UClass* StaticClass<class UVCamInputSubsystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamInput_Public_VCamInputSubsystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
