// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef VIRTUALCAMERA_VirtualCameraPawnBase_generated_h
#error "VirtualCameraPawnBase.generated.h already included, missing '#pragma once' in VirtualCameraPawnBase.h"
#endif
#define VIRTUALCAMERA_VirtualCameraPawnBase_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_EVENT_PARMS \
	struct VirtualCameraPawnBase_eventHighlightTappedActor_Parms \
	{ \
		AActor* HighlightedActor; \
	};


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAVirtualCameraPawnBase(); \
	friend struct Z_Construct_UClass_AVirtualCameraPawnBase_Statics; \
public: \
	DECLARE_CLASS(AVirtualCameraPawnBase, APawn, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(AVirtualCameraPawnBase) \
	virtual UObject* _getUObject() const override { return const_cast<AVirtualCameraPawnBase*>(this); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_INCLASS \
private: \
	static void StaticRegisterNativesAVirtualCameraPawnBase(); \
	friend struct Z_Construct_UClass_AVirtualCameraPawnBase_Statics; \
public: \
	DECLARE_CLASS(AVirtualCameraPawnBase, APawn, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/VirtualCamera"), NO_API) \
	DECLARE_SERIALIZER(AVirtualCameraPawnBase) \
	virtual UObject* _getUObject() const override { return const_cast<AVirtualCameraPawnBase*>(this); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVirtualCameraPawnBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVirtualCameraPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVirtualCameraPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVirtualCameraPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVirtualCameraPawnBase(AVirtualCameraPawnBase&&); \
	NO_API AVirtualCameraPawnBase(const AVirtualCameraPawnBase&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AVirtualCameraPawnBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AVirtualCameraPawnBase(AVirtualCameraPawnBase&&); \
	NO_API AVirtualCameraPawnBase(const AVirtualCameraPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AVirtualCameraPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AVirtualCameraPawnBase); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AVirtualCameraPawnBase)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__bAllowFocusVisualization() { return STRUCT_OFFSET(AVirtualCameraPawnBase, bAllowFocusVisualization); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_19_PROLOG \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h_22_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class VirtualCameraPawnBase."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VIRTUALCAMERA_API UClass* StaticClass<class AVirtualCameraPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VirtualCamera_Public_VirtualCameraPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
