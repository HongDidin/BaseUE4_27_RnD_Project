// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FLiveLinkCameraBlueprintData;
class UVCamComponent;
class UVCamModifierContext;
class UCineCameraComponent;
#ifdef VCAMCORE_VCamModifier_generated_h
#error "VCamModifier.generated.h already included, missing '#pragma once' in VCamModifier.h"
#endif
#define VCAMCORE_VCamModifier_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsEnabled); \
	DECLARE_FUNCTION(execSetEnabled); \
	DECLARE_FUNCTION(execGetCurrentLiveLinkDataFromOwningComponent); \
	DECLARE_FUNCTION(execGetOwningVCamComponent);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsEnabled); \
	DECLARE_FUNCTION(execSetEnabled); \
	DECLARE_FUNCTION(execGetCurrentLiveLinkDataFromOwningComponent); \
	DECLARE_FUNCTION(execGetOwningVCamComponent);


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamModifier(); \
	friend struct Z_Construct_UClass_UVCamModifier_Statics; \
public: \
	DECLARE_CLASS(UVCamModifier, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamModifier)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUVCamModifier(); \
	friend struct Z_Construct_UClass_UVCamModifier_Statics; \
public: \
	DECLARE_CLASS(UVCamModifier, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamModifier)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamModifier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamModifier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamModifier(UVCamModifier&&); \
	NO_API UVCamModifier(const UVCamModifier&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamModifier(UVCamModifier&&); \
	NO_API UVCamModifier(const UVCamModifier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamModifier); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamModifier)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_16_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamModifier>();

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_EVENT_PARMS \
	struct VCamBlueprintModifier_eventOnApply_Parms \
	{ \
		UVCamModifierContext* Context; \
		UCineCameraComponent* CameraComponent; \
		float DeltaTime; \
	}; \
	struct VCamBlueprintModifier_eventOnInitialize_Parms \
	{ \
		UVCamModifierContext* Context; \
	};


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_CALLBACK_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVCamBlueprintModifier(); \
	friend struct Z_Construct_UClass_UVCamBlueprintModifier_Statics; \
public: \
	DECLARE_CLASS(UVCamBlueprintModifier, UVCamModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamBlueprintModifier)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_INCLASS \
private: \
	static void StaticRegisterNativesUVCamBlueprintModifier(); \
	friend struct Z_Construct_UClass_UVCamBlueprintModifier_Statics; \
public: \
	DECLARE_CLASS(UVCamBlueprintModifier, UVCamModifier, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(UVCamBlueprintModifier)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamBlueprintModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamBlueprintModifier) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamBlueprintModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamBlueprintModifier); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamBlueprintModifier(UVCamBlueprintModifier&&); \
	NO_API UVCamBlueprintModifier(const UVCamBlueprintModifier&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVCamBlueprintModifier(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVCamBlueprintModifier(UVCamBlueprintModifier&&); \
	NO_API UVCamBlueprintModifier(const UVCamBlueprintModifier&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVCamBlueprintModifier); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVCamBlueprintModifier); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVCamBlueprintModifier)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_48_PROLOG \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_EVENT_PARMS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_CALLBACK_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h_51_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class UVCamBlueprintModifier>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_VCamModifier_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
