// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamInput/Public/VCamInputSubsystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamInputSubsystem() {}
// Cross Module References
	VCAMINPUT_API UClass* Z_Construct_UClass_UVCamInputSubsystem_NoRegister();
	VCAMINPUT_API UClass* Z_Construct_UClass_UVCamInputSubsystem();
	ENGINE_API UClass* Z_Construct_UClass_UEngineSubsystem();
	UPackage* Z_Construct_UPackage__Script_VCamInput();
	INPUTCORE_API UScriptStruct* Z_Construct_UScriptStruct_FKey();
	VCAMINPUT_API UFunction* Z_Construct_UDelegateFunction_VCamInput_AnalogInputDelegate__DelegateSignature();
	VCAMINPUT_API UFunction* Z_Construct_UDelegateFunction_VCamInput_KeyInputDelegate__DelegateSignature();
	VCAMINPUT_API UFunction* Z_Construct_UDelegateFunction_VCamInput_PointerInputDelegate__DelegateSignature();
// End Cross Module References
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindMouseWheelEvent)
	{
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindMouseWheelEvent(FPointerInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindMouseDoubleClickEvent)
	{
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindMouseDoubleClickEvent(Z_Param_Key,FPointerInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindMouseButtonUpEvent)
	{
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindMouseButtonUpEvent(Z_Param_Key,FPointerInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindMouseButtonDownEvent)
	{
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindMouseButtonDownEvent(Z_Param_Key,FPointerInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindMouseMoveEvent)
	{
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindMouseMoveEvent(FPointerInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindAnalogEvent)
	{
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindAnalogEvent(Z_Param_Key,FAnalogInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindKeyUpEvent)
	{
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindKeyUpEvent(Z_Param_Key,FKeyInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execBindKeyDownEvent)
	{
		P_GET_STRUCT(FKey,Z_Param_Key);
		P_GET_PROPERTY(FDelegateProperty,Z_Param_Delegate);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BindKeyDownEvent(Z_Param_Key,FKeyInputDelegate(Z_Param_Delegate));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execGetShouldConsumeGamepadInput)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetShouldConsumeGamepadInput();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UVCamInputSubsystem::execSetShouldConsumeGamepadInput)
	{
		P_GET_UBOOL(Z_Param_bInShouldConsumeGamepadInput);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetShouldConsumeGamepadInput(Z_Param_bInShouldConsumeGamepadInput);
		P_NATIVE_END;
	}
	void UVCamInputSubsystem::StaticRegisterNativesUVCamInputSubsystem()
	{
		UClass* Class = UVCamInputSubsystem::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BindAnalogEvent", &UVCamInputSubsystem::execBindAnalogEvent },
			{ "BindKeyDownEvent", &UVCamInputSubsystem::execBindKeyDownEvent },
			{ "BindKeyUpEvent", &UVCamInputSubsystem::execBindKeyUpEvent },
			{ "BindMouseButtonDownEvent", &UVCamInputSubsystem::execBindMouseButtonDownEvent },
			{ "BindMouseButtonUpEvent", &UVCamInputSubsystem::execBindMouseButtonUpEvent },
			{ "BindMouseDoubleClickEvent", &UVCamInputSubsystem::execBindMouseDoubleClickEvent },
			{ "BindMouseMoveEvent", &UVCamInputSubsystem::execBindMouseMoveEvent },
			{ "BindMouseWheelEvent", &UVCamInputSubsystem::execBindMouseWheelEvent },
			{ "GetShouldConsumeGamepadInput", &UVCamInputSubsystem::execGetShouldConsumeGamepadInput },
			{ "SetShouldConsumeGamepadInput", &UVCamInputSubsystem::execSetShouldConsumeGamepadInput },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics
	{
		struct VCamInputSubsystem_eventBindAnalogEvent_Parms
		{
			FKey Key;
			FScriptDelegate Delegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindAnalogEvent_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindAnalogEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_AnalogInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindAnalogEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindAnalogEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics
	{
		struct VCamInputSubsystem_eventBindKeyDownEvent_Parms
		{
			FKey Key;
			FScriptDelegate Delegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindKeyDownEvent_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindKeyDownEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_KeyInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindKeyDownEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindKeyDownEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics
	{
		struct VCamInputSubsystem_eventBindKeyUpEvent_Parms
		{
			FKey Key;
			FScriptDelegate Delegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindKeyUpEvent_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindKeyUpEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_KeyInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindKeyUpEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindKeyUpEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics
	{
		struct VCamInputSubsystem_eventBindMouseButtonDownEvent_Parms
		{
			FKey Key;
			FScriptDelegate Delegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseButtonDownEvent_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseButtonDownEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_PointerInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindMouseButtonDownEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindMouseButtonDownEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics
	{
		struct VCamInputSubsystem_eventBindMouseButtonUpEvent_Parms
		{
			FKey Key;
			FScriptDelegate Delegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseButtonUpEvent_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseButtonUpEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_PointerInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindMouseButtonUpEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindMouseButtonUpEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics
	{
		struct VCamInputSubsystem_eventBindMouseDoubleClickEvent_Parms
		{
			FKey Key;
			FScriptDelegate Delegate;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Key_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Key;
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::NewProp_Key_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::NewProp_Key = { "Key", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseDoubleClickEvent_Parms, Key), Z_Construct_UScriptStruct_FKey, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::NewProp_Key_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::NewProp_Key_MetaData)) };
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseDoubleClickEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_PointerInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::NewProp_Key,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindMouseDoubleClickEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindMouseDoubleClickEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics
	{
		struct VCamInputSubsystem_eventBindMouseMoveEvent_Parms
		{
			FScriptDelegate Delegate;
		};
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseMoveEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_PointerInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindMouseMoveEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindMouseMoveEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics
	{
		struct VCamInputSubsystem_eventBindMouseWheelEvent_Parms
		{
			FScriptDelegate Delegate;
		};
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_Delegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::NewProp_Delegate = { "Delegate", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(VCamInputSubsystem_eventBindMouseWheelEvent_Parms, Delegate), Z_Construct_UDelegateFunction_VCamInput_PointerInputDelegate__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::NewProp_Delegate,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "BindMouseWheelEvent", nullptr, nullptr, sizeof(VCamInputSubsystem_eventBindMouseWheelEvent_Parms), Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics
	{
		struct VCamInputSubsystem_eventGetShouldConsumeGamepadInput_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((VCamInputSubsystem_eventGetShouldConsumeGamepadInput_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamInputSubsystem_eventGetShouldConsumeGamepadInput_Parms), &Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "GetShouldConsumeGamepadInput", nullptr, nullptr, sizeof(VCamInputSubsystem_eventGetShouldConsumeGamepadInput_Parms), Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics
	{
		struct VCamInputSubsystem_eventSetShouldConsumeGamepadInput_Parms
		{
			bool bInShouldConsumeGamepadInput;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInShouldConsumeGamepadInput_MetaData[];
#endif
		static void NewProp_bInShouldConsumeGamepadInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInShouldConsumeGamepadInput;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::NewProp_bInShouldConsumeGamepadInput_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::NewProp_bInShouldConsumeGamepadInput_SetBit(void* Obj)
	{
		((VCamInputSubsystem_eventSetShouldConsumeGamepadInput_Parms*)Obj)->bInShouldConsumeGamepadInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::NewProp_bInShouldConsumeGamepadInput = { "bInShouldConsumeGamepadInput", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(VCamInputSubsystem_eventSetShouldConsumeGamepadInput_Parms), &Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::NewProp_bInShouldConsumeGamepadInput_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::NewProp_bInShouldConsumeGamepadInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::NewProp_bInShouldConsumeGamepadInput_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::NewProp_bInShouldConsumeGamepadInput,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Input" },
		{ "Comment", "// By default the editor will use gamepads to control the editor camera\n// Setting this to true will prevent this\n" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
		{ "ToolTip", "By default the editor will use gamepads to control the editor camera\nSetting this to true will prevent this" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UVCamInputSubsystem, nullptr, "SetShouldConsumeGamepadInput", nullptr, nullptr, sizeof(VCamInputSubsystem_eventSetShouldConsumeGamepadInput_Parms), Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UVCamInputSubsystem_NoRegister()
	{
		return UVCamInputSubsystem::StaticClass();
	}
	struct Z_Construct_UClass_UVCamInputSubsystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamInputSubsystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEngineSubsystem,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamInput,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UVCamInputSubsystem_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindAnalogEvent, "BindAnalogEvent" }, // 1065110586
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindKeyDownEvent, "BindKeyDownEvent" }, // 4262860565
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindKeyUpEvent, "BindKeyUpEvent" }, // 799556636
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonDownEvent, "BindMouseButtonDownEvent" }, // 2840414777
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindMouseButtonUpEvent, "BindMouseButtonUpEvent" }, // 27174040
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindMouseDoubleClickEvent, "BindMouseDoubleClickEvent" }, // 2151498034
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindMouseMoveEvent, "BindMouseMoveEvent" }, // 2923566940
		{ &Z_Construct_UFunction_UVCamInputSubsystem_BindMouseWheelEvent, "BindMouseWheelEvent" }, // 3250702324
		{ &Z_Construct_UFunction_UVCamInputSubsystem_GetShouldConsumeGamepadInput, "GetShouldConsumeGamepadInput" }, // 3427675892
		{ &Z_Construct_UFunction_UVCamInputSubsystem_SetShouldConsumeGamepadInput, "SetShouldConsumeGamepadInput" }, // 3173225619
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamInputSubsystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Currently only used for the placeholder Editor Input System\n// This subsystem will be removed once the new input system is ready\n" },
		{ "IncludePath", "VCamInputSubsystem.h" },
		{ "ModuleRelativePath", "Public/VCamInputSubsystem.h" },
		{ "ToolTip", "Currently only used for the placeholder Editor Input System\nThis subsystem will be removed once the new input system is ready" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamInputSubsystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamInputSubsystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamInputSubsystem_Statics::ClassParams = {
		&UVCamInputSubsystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamInputSubsystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamInputSubsystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamInputSubsystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamInputSubsystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamInputSubsystem, 3869632543);
	template<> VCAMINPUT_API UClass* StaticClass<UVCamInputSubsystem>()
	{
		return UVCamInputSubsystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamInputSubsystem(Z_Construct_UClass_UVCamInputSubsystem, &UVCamInputSubsystem::StaticClass, TEXT("/Script/VCamInput"), TEXT("UVCamInputSubsystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamInputSubsystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
