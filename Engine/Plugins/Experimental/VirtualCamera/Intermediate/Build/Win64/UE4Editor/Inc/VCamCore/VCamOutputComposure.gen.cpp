// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "VCamCore/Public/VCamOutputComposure.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVCamOutputComposure() {}
// Cross Module References
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputComposure_NoRegister();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputComposure();
	VCAMCORE_API UClass* Z_Construct_UClass_UVCamOutputProviderBase();
	UPackage* Z_Construct_UPackage__Script_VCamCore();
	COMPOSURE_API UClass* Z_Construct_UClass_ACompositingElement_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
// End Cross Module References
	void UVCamOutputComposure::StaticRegisterNativesUVCamOutputComposure()
	{
	}
	UClass* Z_Construct_UClass_UVCamOutputComposure_NoRegister()
	{
		return UVCamOutputComposure::StaticClass();
	}
	struct Z_Construct_UClass_UVCamOutputComposure_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_LayerTargets_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LayerTargets_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_LayerTargets;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FinalOutputRenderTarget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FinalOutputRenderTarget;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVCamOutputComposure_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UVCamOutputProviderBase,
		(UObject* (*)())Z_Construct_UPackage__Script_VCamCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputComposure_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "Composure Output Provider" },
		{ "IncludePath", "VCamOutputComposure.h" },
		{ "ModuleRelativePath", "Public/VCamOutputComposure.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_LayerTargets_Inner = { "LayerTargets", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_ACompositingElement_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_LayerTargets_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "// List of Composure stack Compositing Elements to render the requested UMG into\n" },
		{ "ModuleRelativePath", "Public/VCamOutputComposure.h" },
		{ "ToolTip", "List of Composure stack Compositing Elements to render the requested UMG into" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_LayerTargets = { "LayerTargets", nullptr, (EPropertyFlags)0x0014000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputComposure, LayerTargets), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_LayerTargets_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_LayerTargets_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_FinalOutputRenderTarget_MetaData[] = {
		{ "Category", "Output" },
		{ "Comment", "// TextureRenderTarget2D asset that contains the final output\n" },
		{ "ModuleRelativePath", "Public/VCamOutputComposure.h" },
		{ "ToolTip", "TextureRenderTarget2D asset that contains the final output" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_FinalOutputRenderTarget = { "FinalOutputRenderTarget", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVCamOutputComposure, FinalOutputRenderTarget), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_FinalOutputRenderTarget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_FinalOutputRenderTarget_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVCamOutputComposure_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_LayerTargets_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_LayerTargets,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVCamOutputComposure_Statics::NewProp_FinalOutputRenderTarget,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVCamOutputComposure_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVCamOutputComposure>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVCamOutputComposure_Statics::ClassParams = {
		&UVCamOutputComposure::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVCamOutputComposure_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputComposure_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UVCamOutputComposure_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVCamOutputComposure_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVCamOutputComposure()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVCamOutputComposure_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVCamOutputComposure, 3870386961);
	template<> VCAMCORE_API UClass* StaticClass<UVCamOutputComposure>()
	{
		return UVCamOutputComposure::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVCamOutputComposure(Z_Construct_UClass_UVCamOutputComposure, &UVCamOutputComposure::StaticClass, TEXT("/Script/VCamCore"), TEXT("UVCamOutputComposure"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVCamOutputComposure);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
