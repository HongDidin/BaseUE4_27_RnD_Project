// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef VCAMCORE_SimpleVirtualCamera_generated_h
#error "SimpleVirtualCamera.generated.h already included, missing '#pragma once' in SimpleVirtualCamera.h"
#endif
#define VCAMCORE_SimpleVirtualCamera_generated_h

#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_SPARSE_DATA
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_RPC_WRAPPERS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASimpleVirtualCamera(); \
	friend struct Z_Construct_UClass_ASimpleVirtualCamera_Statics; \
public: \
	DECLARE_CLASS(ASimpleVirtualCamera, ACineCameraActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(ASimpleVirtualCamera)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASimpleVirtualCamera(); \
	friend struct Z_Construct_UClass_ASimpleVirtualCamera_Statics; \
public: \
	DECLARE_CLASS(ASimpleVirtualCamera, ACineCameraActor, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Config), CASTCLASS_None, TEXT("/Script/VCamCore"), NO_API) \
	DECLARE_SERIALIZER(ASimpleVirtualCamera)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASimpleVirtualCamera(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASimpleVirtualCamera) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASimpleVirtualCamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASimpleVirtualCamera); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASimpleVirtualCamera(ASimpleVirtualCamera&&); \
	NO_API ASimpleVirtualCamera(const ASimpleVirtualCamera&); \
public:


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASimpleVirtualCamera(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASimpleVirtualCamera(ASimpleVirtualCamera&&); \
	NO_API ASimpleVirtualCamera(const ASimpleVirtualCamera&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASimpleVirtualCamera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASimpleVirtualCamera); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASimpleVirtualCamera)


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__VCamComponent() { return STRUCT_OFFSET(ASimpleVirtualCamera, VCamComponent); }


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_13_PROLOG
#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_RPC_WRAPPERS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_INCLASS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_SPARSE_DATA \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> VCAMCORE_API UClass* StaticClass<class ASimpleVirtualCamera>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_VirtualCamera_Source_VCamCore_Public_SimpleVirtualCamera_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
