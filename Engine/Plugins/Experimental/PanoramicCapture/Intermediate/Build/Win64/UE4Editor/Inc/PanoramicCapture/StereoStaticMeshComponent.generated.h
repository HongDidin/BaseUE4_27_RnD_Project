// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PANORAMICCAPTURE_StereoStaticMeshComponent_generated_h
#error "StereoStaticMeshComponent.generated.h already included, missing '#pragma once' in StereoStaticMeshComponent.h"
#endif
#define PANORAMICCAPTURE_StereoStaticMeshComponent_generated_h

#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_SPARSE_DATA
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStereoStaticMeshComponent(); \
	friend struct Z_Construct_UClass_UStereoStaticMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UStereoStaticMeshComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PanoramicCapture"), NO_API) \
	DECLARE_SERIALIZER(UStereoStaticMeshComponent)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUStereoStaticMeshComponent(); \
	friend struct Z_Construct_UClass_UStereoStaticMeshComponent_Statics; \
public: \
	DECLARE_CLASS(UStereoStaticMeshComponent, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PanoramicCapture"), NO_API) \
	DECLARE_SERIALIZER(UStereoStaticMeshComponent)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStereoStaticMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStereoStaticMeshComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStereoStaticMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStereoStaticMeshComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStereoStaticMeshComponent(UStereoStaticMeshComponent&&); \
	NO_API UStereoStaticMeshComponent(const UStereoStaticMeshComponent&); \
public:


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStereoStaticMeshComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStereoStaticMeshComponent(UStereoStaticMeshComponent&&); \
	NO_API UStereoStaticMeshComponent(const UStereoStaticMeshComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStereoStaticMeshComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStereoStaticMeshComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStereoStaticMeshComponent)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_22_PROLOG
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_INCLASS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_SPARSE_DATA \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PANORAMICCAPTURE_API UClass* StaticClass<class UStereoStaticMeshComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_StereoStaticMeshComponent_h


#define FOREACH_ENUM_ESPSTEREOCAMERALAYER(op) \
	op(ESPStereoCameraLayer::LeftEye) \
	op(ESPStereoCameraLayer::RightEye) \
	op(ESPStereoCameraLayer::BothEyes) 

enum class ESPStereoCameraLayer : uint8;
template<> PANORAMICCAPTURE_API UEnum* StaticEnum<ESPStereoCameraLayer>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
