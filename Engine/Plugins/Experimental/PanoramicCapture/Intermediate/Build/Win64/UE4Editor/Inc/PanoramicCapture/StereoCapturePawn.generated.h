// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UObject;
struct FLatentActionInfo;
#ifdef PANORAMICCAPTURE_StereoCapturePawn_generated_h
#error "StereoCapturePawn.generated.h already included, missing '#pragma once' in StereoCapturePawn.h"
#endif
#define PANORAMICCAPTURE_StereoCapturePawn_generated_h

#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_SPARSE_DATA
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateStereoAtlas);


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateStereoAtlas);


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStereoCapturePawn(); \
	friend struct Z_Construct_UClass_AStereoCapturePawn_Statics; \
public: \
	DECLARE_CLASS(AStereoCapturePawn, ADefaultPawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PanoramicCapture"), NO_API) \
	DECLARE_SERIALIZER(AStereoCapturePawn)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_INCLASS \
private: \
	static void StaticRegisterNativesAStereoCapturePawn(); \
	friend struct Z_Construct_UClass_AStereoCapturePawn_Statics; \
public: \
	DECLARE_CLASS(AStereoCapturePawn, ADefaultPawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PanoramicCapture"), NO_API) \
	DECLARE_SERIALIZER(AStereoCapturePawn)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStereoCapturePawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStereoCapturePawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStereoCapturePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStereoCapturePawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStereoCapturePawn(AStereoCapturePawn&&); \
	NO_API AStereoCapturePawn(const AStereoCapturePawn&); \
public:


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStereoCapturePawn(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStereoCapturePawn(AStereoCapturePawn&&); \
	NO_API AStereoCapturePawn(const AStereoCapturePawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStereoCapturePawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStereoCapturePawn); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStereoCapturePawn)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_51_PROLOG
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_SPARSE_DATA \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_INCLASS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_SPARSE_DATA \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h_55_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PANORAMICCAPTURE_API UClass* StaticClass<class AStereoCapturePawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Public_StereoCapturePawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
