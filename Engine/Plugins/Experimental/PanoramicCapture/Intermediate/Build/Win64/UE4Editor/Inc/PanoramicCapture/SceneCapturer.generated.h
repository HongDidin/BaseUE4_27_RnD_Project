// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PANORAMICCAPTURE_SceneCapturer_generated_h
#error "SceneCapturer.generated.h already included, missing '#pragma once' in SceneCapturer.h"
#endif
#define PANORAMICCAPTURE_SceneCapturer_generated_h

#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_29_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FPostProcessVolumeData_Statics; \
	PANORAMICCAPTURE_API static class UScriptStruct* StaticStruct();


template<> PANORAMICCAPTURE_API UScriptStruct* StaticStruct<struct FPostProcessVolumeData>();

#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_SPARSE_DATA
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_RPC_WRAPPERS
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSceneCapturer(); \
	friend struct Z_Construct_UClass_USceneCapturer_Statics; \
public: \
	DECLARE_CLASS(USceneCapturer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PanoramicCapture"), NO_API) \
	DECLARE_SERIALIZER(USceneCapturer)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_INCLASS \
private: \
	static void StaticRegisterNativesUSceneCapturer(); \
	friend struct Z_Construct_UClass_USceneCapturer_Statics; \
public: \
	DECLARE_CLASS(USceneCapturer, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PanoramicCapture"), NO_API) \
	DECLARE_SERIALIZER(USceneCapturer)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USceneCapturer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USceneCapturer) \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USceneCapturer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USceneCapturer(USceneCapturer&&); \
	NO_API USceneCapturer(const USceneCapturer&); \
public:


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USceneCapturer(USceneCapturer&&); \
	NO_API USceneCapturer(const USceneCapturer&); \
public: \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USceneCapturer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USceneCapturer)


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_61_PROLOG
#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_SPARSE_DATA \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_RPC_WRAPPERS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_INCLASS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_SPARSE_DATA \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h_66_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PANORAMICCAPTURE_API UClass* StaticClass<class USceneCapturer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_PanoramicCapture_Source_PanoramicCapture_Private_SceneCapturer_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
