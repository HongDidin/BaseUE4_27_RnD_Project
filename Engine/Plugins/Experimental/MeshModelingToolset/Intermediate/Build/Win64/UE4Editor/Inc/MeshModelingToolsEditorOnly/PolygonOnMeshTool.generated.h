// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLSEDITORONLY_PolygonOnMeshTool_generated_h
#error "PolygonOnMeshTool.generated.h already included, missing '#pragma once' in PolygonOnMeshTool.h"
#endif
#define MESHMODELINGTOOLSEDITORONLY_PolygonOnMeshTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_INCLASS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshToolBuilder(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonOnMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonOnMeshToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshToolBuilder(UPolygonOnMeshToolBuilder&&); \
	NO_API UPolygonOnMeshToolBuilder(const UPolygonOnMeshToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonOnMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshToolBuilder(UPolygonOnMeshToolBuilder&&); \
	NO_API UPolygonOnMeshToolBuilder(const UPolygonOnMeshToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonOnMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_33_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_36_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UPolygonOnMeshToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshToolProperties(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_INCLASS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshToolProperties(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonOnMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonOnMeshToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshToolProperties(UPolygonOnMeshToolProperties&&); \
	NO_API UPolygonOnMeshToolProperties(const UPolygonOnMeshToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonOnMeshToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshToolProperties(UPolygonOnMeshToolProperties&&); \
	NO_API UPolygonOnMeshToolProperties(const UPolygonOnMeshToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonOnMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_61_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_64_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UPolygonOnMeshToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execDrawPolygon);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execDrawPolygon);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_INCLASS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonOnMeshToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonOnMeshToolActionPropertySet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshToolActionPropertySet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshToolActionPropertySet(UPolygonOnMeshToolActionPropertySet&&); \
	NO_API UPolygonOnMeshToolActionPropertySet(const UPolygonOnMeshToolActionPropertySet&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonOnMeshToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshToolActionPropertySet(UPolygonOnMeshToolActionPropertySet&&); \
	NO_API UPolygonOnMeshToolActionPropertySet(const UPolygonOnMeshToolActionPropertySet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshToolActionPropertySet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonOnMeshToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_112_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_115_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UPolygonOnMeshToolActionPropertySet>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshTool(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshTool_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_INCLASS \
private: \
	static void StaticRegisterNativesUPolygonOnMeshTool(); \
	friend struct Z_Construct_UClass_UPolygonOnMeshTool_Statics; \
public: \
	DECLARE_CLASS(UPolygonOnMeshTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UPolygonOnMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPolygonOnMeshTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPolygonOnMeshTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshTool(UPolygonOnMeshTool&&); \
	NO_API UPolygonOnMeshTool(const UPolygonOnMeshTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPolygonOnMeshTool(UPolygonOnMeshTool&&); \
	NO_API UPolygonOnMeshTool(const UPolygonOnMeshTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPolygonOnMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPolygonOnMeshTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UPolygonOnMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__BasicProperties() { return STRUCT_OFFSET(UPolygonOnMeshTool, BasicProperties); } \
	FORCEINLINE static uint32 __PPO__ActionProperties() { return STRUCT_OFFSET(UPolygonOnMeshTool, ActionProperties); } \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(UPolygonOnMeshTool, Preview); } \
	FORCEINLINE static uint32 __PPO__DrawnLineSet() { return STRUCT_OFFSET(UPolygonOnMeshTool, DrawnLineSet); } \
	FORCEINLINE static uint32 __PPO__PlaneMechanic() { return STRUCT_OFFSET(UPolygonOnMeshTool, PlaneMechanic); } \
	FORCEINLINE static uint32 __PPO__DrawPolygonMechanic() { return STRUCT_OFFSET(UPolygonOnMeshTool, DrawPolygonMechanic); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_138_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h_141_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UPolygonOnMeshTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_PolygonOnMeshTool_h


#define FOREACH_ENUM_EPOLYGONONMESHTOOLACTIONS(op) \
	op(EPolygonOnMeshToolActions::NoAction) \
	op(EPolygonOnMeshToolActions::DrawPolygon) 

enum class EPolygonOnMeshToolActions;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EPolygonOnMeshToolActions>();

#define FOREACH_ENUM_EPOLYGONTYPE(op) \
	op(EPolygonType::Circle) \
	op(EPolygonType::Square) \
	op(EPolygonType::Rectangle) \
	op(EPolygonType::RoundRect) \
	op(EPolygonType::Custom) 

enum class EPolygonType;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EPolygonType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
