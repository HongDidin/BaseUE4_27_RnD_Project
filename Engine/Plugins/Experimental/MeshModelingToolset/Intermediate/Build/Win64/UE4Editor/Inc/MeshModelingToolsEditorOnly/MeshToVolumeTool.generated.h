// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLSEDITORONLY_MeshToVolumeTool_generated_h
#error "MeshToVolumeTool.generated.h already included, missing '#pragma once' in MeshToVolumeTool.h"
#endif
#define MESHMODELINGTOOLSEDITORONLY_MeshToVolumeTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshToVolumeToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshToVolumeToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshToVolumeToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUMeshToVolumeToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshToVolumeToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshToVolumeToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshToVolumeToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshToVolumeToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshToVolumeToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToVolumeToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToVolumeToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToVolumeToolBuilder(UMeshToVolumeToolBuilder&&); \
	NO_API UMeshToVolumeToolBuilder(const UMeshToVolumeToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshToVolumeToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToVolumeToolBuilder(UMeshToVolumeToolBuilder&&); \
	NO_API UMeshToVolumeToolBuilder(const UMeshToVolumeToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToVolumeToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToVolumeToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshToVolumeToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_19_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UMeshToVolumeToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshToVolumeToolProperties(); \
	friend struct Z_Construct_UClass_UMeshToVolumeToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshToVolumeToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshToVolumeToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUMeshToVolumeToolProperties(); \
	friend struct Z_Construct_UClass_UMeshToVolumeToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshToVolumeToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshToVolumeToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshToVolumeToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshToVolumeToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToVolumeToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToVolumeToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToVolumeToolProperties(UMeshToVolumeToolProperties&&); \
	NO_API UMeshToVolumeToolProperties(const UMeshToVolumeToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshToVolumeToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToVolumeToolProperties(UMeshToVolumeToolProperties&&); \
	NO_API UMeshToVolumeToolProperties(const UMeshToVolumeToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToVolumeToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToVolumeToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshToVolumeToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_44_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_47_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UMeshToVolumeToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshToVolumeTool(); \
	friend struct Z_Construct_UClass_UMeshToVolumeTool_Statics; \
public: \
	DECLARE_CLASS(UMeshToVolumeTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshToVolumeTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_INCLASS \
private: \
	static void StaticRegisterNativesUMeshToVolumeTool(); \
	friend struct Z_Construct_UClass_UMeshToVolumeTool_Statics; \
public: \
	DECLARE_CLASS(UMeshToVolumeTool, USingleSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingToolsEditorOnly"), NO_API) \
	DECLARE_SERIALIZER(UMeshToVolumeTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshToVolumeTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshToVolumeTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToVolumeTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToVolumeTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToVolumeTool(UMeshToVolumeTool&&); \
	NO_API UMeshToVolumeTool(const UMeshToVolumeTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToVolumeTool(UMeshToVolumeTool&&); \
	NO_API UMeshToVolumeTool(const UMeshToVolumeTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToVolumeTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToVolumeTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshToVolumeTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UMeshToVolumeTool, Settings); } \
	FORCEINLINE static uint32 __PPO__HandleSourcesProperties() { return STRUCT_OFFSET(UMeshToVolumeTool, HandleSourcesProperties); } \
	FORCEINLINE static uint32 __PPO__PreviewMesh() { return STRUCT_OFFSET(UMeshToVolumeTool, PreviewMesh); } \
	FORCEINLINE static uint32 __PPO__VolumeEdgesSet() { return STRUCT_OFFSET(UMeshToVolumeTool, VolumeEdgesSet); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_74_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h_77_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<class UMeshToVolumeTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingToolsEditorOnly_Public_MeshToVolumeTool_h


#define FOREACH_ENUM_EMESHTOVOLUMEMODE(op) \
	op(EMeshToVolumeMode::TriangulatePolygons) \
	op(EMeshToVolumeMode::MinimalPolygons) 

enum class EMeshToVolumeMode;
template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EMeshToVolumeMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
