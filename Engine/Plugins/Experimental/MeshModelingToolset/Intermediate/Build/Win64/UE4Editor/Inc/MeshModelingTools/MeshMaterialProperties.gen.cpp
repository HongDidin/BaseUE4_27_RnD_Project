// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Properties/MeshMaterialProperties.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshMaterialProperties() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshEditingMaterialModes();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ESetMeshMaterialMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExistingMeshMaterialProperties();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshEditingViewProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshEditingViewProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
// End Cross Module References
	static UEnum* EMeshEditingMaterialModes_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshEditingMaterialModes, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshEditingMaterialModes"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshEditingMaterialModes>()
	{
		return EMeshEditingMaterialModes_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshEditingMaterialModes(EMeshEditingMaterialModes_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshEditingMaterialModes"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshEditingMaterialModes_Hash() { return 3089044231U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshEditingMaterialModes()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshEditingMaterialModes"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshEditingMaterialModes_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshEditingMaterialModes::ExistingMaterial", (int64)EMeshEditingMaterialModes::ExistingMaterial },
				{ "EMeshEditingMaterialModes::Diffuse", (int64)EMeshEditingMaterialModes::Diffuse },
				{ "EMeshEditingMaterialModes::Grey", (int64)EMeshEditingMaterialModes::Grey },
				{ "EMeshEditingMaterialModes::Soft", (int64)EMeshEditingMaterialModes::Soft },
				{ "EMeshEditingMaterialModes::TangentNormal", (int64)EMeshEditingMaterialModes::TangentNormal },
				{ "EMeshEditingMaterialModes::Custom", (int64)EMeshEditingMaterialModes::Custom },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Custom.Name", "EMeshEditingMaterialModes::Custom" },
				{ "Diffuse.Name", "EMeshEditingMaterialModes::Diffuse" },
				{ "ExistingMaterial.Name", "EMeshEditingMaterialModes::ExistingMaterial" },
				{ "Grey.Name", "EMeshEditingMaterialModes::Grey" },
				{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
				{ "Soft.Name", "EMeshEditingMaterialModes::Soft" },
				{ "TangentNormal.Name", "EMeshEditingMaterialModes::TangentNormal" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshEditingMaterialModes",
				"EMeshEditingMaterialModes",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ESetMeshMaterialMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ESetMeshMaterialMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ESetMeshMaterialMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ESetMeshMaterialMode>()
	{
		return ESetMeshMaterialMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESetMeshMaterialMode(ESetMeshMaterialMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ESetMeshMaterialMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ESetMeshMaterialMode_Hash() { return 215285993U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ESetMeshMaterialMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESetMeshMaterialMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ESetMeshMaterialMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESetMeshMaterialMode::KeepOriginal", (int64)ESetMeshMaterialMode::KeepOriginal },
				{ "ESetMeshMaterialMode::Checkerboard", (int64)ESetMeshMaterialMode::Checkerboard },
				{ "ESetMeshMaterialMode::Override", (int64)ESetMeshMaterialMode::Override },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Checkerboard.Comment", "/** Checkerboard material */" },
				{ "Checkerboard.Name", "ESetMeshMaterialMode::Checkerboard" },
				{ "Checkerboard.ToolTip", "Checkerboard material" },
				{ "Comment", "/** Standard material modes for tools that need to set custom materials for visualization */" },
				{ "KeepOriginal.Comment", "/** Input material */" },
				{ "KeepOriginal.Name", "ESetMeshMaterialMode::KeepOriginal" },
				{ "KeepOriginal.ToolTip", "Input material" },
				{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
				{ "Override.Comment", "/** Override material */" },
				{ "Override.Name", "ESetMeshMaterialMode::Override" },
				{ "Override.ToolTip", "Override material" },
				{ "ToolTip", "Standard material modes for tools that need to set custom materials for visualization" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ESetMeshMaterialMode",
				"ESetMeshMaterialMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UNewMeshMaterialProperties::StaticRegisterNativesUNewMeshMaterialProperties()
	{
	}
	UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister()
	{
		return UNewMeshMaterialProperties::StaticClass();
	}
	struct Z_Construct_UClass_UNewMeshMaterialProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Material_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Material;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UVScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWorldSpaceUVScale_MetaData[];
#endif
		static void NewProp_bWorldSpaceUVScale_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWorldSpaceUVScale;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWireframe_MetaData[];
#endif
		static void NewProp_bWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowExtendedOptions_MetaData[];
#endif
		static void NewProp_bShowExtendedOptions_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowExtendedOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UNewMeshMaterialProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewMeshMaterialProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Standard material property settings for tools that generate new meshes\n" },
		{ "IncludePath", "Properties/MeshMaterialProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Standard material property settings for tools that generate new meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_Material_MetaData[] = {
		{ "Category", "Material" },
		{ "Comment", "/** Material for new mesh*/" },
		{ "DisplayName", "Material" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Material for new mesh" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_Material = { "Material", nullptr, (EPropertyFlags)0x0014000400000001, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNewMeshMaterialProperties, Material), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_Material_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_Material_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_UVScale_MetaData[] = {
		{ "Category", "Material" },
		{ "Comment", "/** Scale factor for generated UVs */" },
		{ "DisplayName", "UV Scale" },
		{ "EditCondition", "bShowExtendedOptions" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Scale factor for generated UVs" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_UVScale = { "UVScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UNewMeshMaterialProperties, UVScale), METADATA_PARAMS(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_UVScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_UVScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWorldSpaceUVScale_MetaData[] = {
		{ "Category", "Material" },
		{ "Comment", "/** If set, UV scales will be relative to world space so different objects created with the same UV scale should have the same average texel size */" },
		{ "DisplayName", "UV Scale Relative to World Space" },
		{ "EditCondition", "bShowExtendedOptions" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "If set, UV scales will be relative to world space so different objects created with the same UV scale should have the same average texel size" },
	};
#endif
	void Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWorldSpaceUVScale_SetBit(void* Obj)
	{
		((UNewMeshMaterialProperties*)Obj)->bWorldSpaceUVScale = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWorldSpaceUVScale = { "bWorldSpaceUVScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNewMeshMaterialProperties), &Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWorldSpaceUVScale_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWorldSpaceUVScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWorldSpaceUVScale_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWireframe_MetaData[] = {
		{ "Category", "Material" },
		{ "Comment", "/** Overlay wireframe on preview */" },
		{ "DisplayName", "Show Wireframe" },
		{ "EditCondition", "bShowExtendedOptions" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Overlay wireframe on preview" },
	};
#endif
	void Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWireframe_SetBit(void* Obj)
	{
		((UNewMeshMaterialProperties*)Obj)->bWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWireframe = { "bWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNewMeshMaterialProperties), &Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bShowExtendedOptions_MetaData[] = {
		{ "Comment", "// controls visibility of UV/etc properties\n" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "controls visibility of UV/etc properties" },
	};
#endif
	void Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bShowExtendedOptions_SetBit(void* Obj)
	{
		((UNewMeshMaterialProperties*)Obj)->bShowExtendedOptions = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bShowExtendedOptions = { "bShowExtendedOptions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UNewMeshMaterialProperties), &Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bShowExtendedOptions_SetBit, METADATA_PARAMS(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bShowExtendedOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bShowExtendedOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UNewMeshMaterialProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_Material,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_UVScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWorldSpaceUVScale,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UNewMeshMaterialProperties_Statics::NewProp_bShowExtendedOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UNewMeshMaterialProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UNewMeshMaterialProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UNewMeshMaterialProperties_Statics::ClassParams = {
		&UNewMeshMaterialProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UNewMeshMaterialProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UNewMeshMaterialProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UNewMeshMaterialProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UNewMeshMaterialProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UNewMeshMaterialProperties, 3024156382);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UNewMeshMaterialProperties>()
	{
		return UNewMeshMaterialProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UNewMeshMaterialProperties(Z_Construct_UClass_UNewMeshMaterialProperties, &UNewMeshMaterialProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UNewMeshMaterialProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UNewMeshMaterialProperties);
	void UExistingMeshMaterialProperties::StaticRegisterNativesUExistingMeshMaterialProperties()
	{
	}
	UClass* Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister()
	{
		return UExistingMeshMaterialProperties::StaticClass();
	}
	struct Z_Construct_UClass_UExistingMeshMaterialProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaterialMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MaterialMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CheckerDensity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_CheckerDensity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverrideMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverrideMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CheckerMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CheckerMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Standard material property settings for tools that visualize materials on existing meshes (e.g. to help show UVs)\n" },
		{ "IncludePath", "Properties/MeshMaterialProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Standard material property settings for tools that visualize materials on existing meshes (e.g. to help show UVs)" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_MaterialMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_MaterialMode_MetaData[] = {
		{ "Category", "MaterialPreview" },
		{ "Comment", "/** Material that will be used on the mesh */" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Material that will be used on the mesh" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_MaterialMode = { "MaterialMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExistingMeshMaterialProperties, MaterialMode), Z_Construct_UEnum_MeshModelingTools_ESetMeshMaterialMode, METADATA_PARAMS(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_MaterialMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_MaterialMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerDensity_MetaData[] = {
		{ "Category", "MaterialPreview" },
		{ "ClampMax", "1000.0" },
		{ "ClampMin", "0.01" },
		{ "EditCondition", "MaterialMode == ESetMeshMaterialMode::Checkerboard" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "UIMax", "40.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerDensity = { "CheckerDensity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExistingMeshMaterialProperties, CheckerDensity), METADATA_PARAMS(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerDensity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerDensity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_OverrideMaterial_MetaData[] = {
		{ "Category", "MaterialPreview" },
		{ "EditCondition", "MaterialMode == ESetMeshMaterialMode::Override" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_OverrideMaterial = { "OverrideMaterial", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExistingMeshMaterialProperties, OverrideMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_OverrideMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_OverrideMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerMaterial = { "CheckerMaterial", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UExistingMeshMaterialProperties, CheckerMaterial), Z_Construct_UClass_UMaterialInstanceDynamic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_MaterialMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_MaterialMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerDensity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_OverrideMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::NewProp_CheckerMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UExistingMeshMaterialProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::ClassParams = {
		&UExistingMeshMaterialProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UExistingMeshMaterialProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UExistingMeshMaterialProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UExistingMeshMaterialProperties, 1127622979);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UExistingMeshMaterialProperties>()
	{
		return UExistingMeshMaterialProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UExistingMeshMaterialProperties(Z_Construct_UClass_UExistingMeshMaterialProperties, &UExistingMeshMaterialProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UExistingMeshMaterialProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UExistingMeshMaterialProperties);
	void UMeshEditingViewProperties::StaticRegisterNativesUMeshEditingViewProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshEditingViewProperties_NoRegister()
	{
		return UMeshEditingViewProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshEditingViewProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_MaterialMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MaterialMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlatShading_MetaData[];
#endif
		static void NewProp_bFlatShading_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlatShading;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Color_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Color;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Image_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Image;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshEditingViewProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditingViewProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Properties/MeshMaterialProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Toggle drawing of wireframe overlay on/off [Alt+W] */" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Toggle drawing of wireframe overlay on/off [Alt+W]" },
	};
#endif
	void Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((UMeshEditingViewProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditingViewProperties), &Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bShowWireframe_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_MaterialMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_MaterialMode_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Set which material to use on object */" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Set which material to use on object" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_MaterialMode = { "MaterialMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditingViewProperties, MaterialMode), Z_Construct_UEnum_MeshModelingTools_EMeshEditingMaterialModes, METADATA_PARAMS(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_MaterialMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_MaterialMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bFlatShading_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Toggle flat shading on/off */" },
		{ "EditCondition", "MaterialMode != EMeshEditingMaterialModes::ExistingMaterial" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Toggle flat shading on/off" },
	};
#endif
	void Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bFlatShading_SetBit(void* Obj)
	{
		((UMeshEditingViewProperties*)Obj)->bFlatShading = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bFlatShading = { "bFlatShading", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshEditingViewProperties), &Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bFlatShading_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bFlatShading_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bFlatShading_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Color_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Main Color of Material */" },
		{ "EditCondition", "MaterialMode == EMeshEditingMaterialModes::Diffuse" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Main Color of Material" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Color = { "Color", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditingViewProperties, Color), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Color_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Color_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Image_MetaData[] = {
		{ "Category", "Rendering" },
		{ "Comment", "/** Image used in Image-Based Material */" },
		{ "EditCondition", "MaterialMode == EMeshEditingMaterialModes::Custom" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/Properties/MeshMaterialProperties.h" },
		{ "ToolTip", "Image used in Image-Based Material" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Image = { "Image", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshEditingViewProperties, Image), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Image_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Image_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshEditingViewProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_MaterialMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_MaterialMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_bFlatShading,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Color,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshEditingViewProperties_Statics::NewProp_Image,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshEditingViewProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshEditingViewProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshEditingViewProperties_Statics::ClassParams = {
		&UMeshEditingViewProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshEditingViewProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditingViewProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshEditingViewProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshEditingViewProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshEditingViewProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshEditingViewProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshEditingViewProperties, 2726057404);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshEditingViewProperties>()
	{
		return UMeshEditingViewProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshEditingViewProperties(Z_Construct_UClass_UMeshEditingViewProperties, &UMeshEditingViewProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshEditingViewProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshEditingViewProperties);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
