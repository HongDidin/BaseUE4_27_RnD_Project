// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/RemoveOccludedTrianglesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemoveOccludedTrianglesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionCalculationUIMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionTriangleSamplingUIMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemoveOccludedTrianglesTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
// End Cross Module References
	static UEnum* EOcclusionCalculationUIMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EOcclusionCalculationUIMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EOcclusionCalculationUIMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EOcclusionCalculationUIMode>()
	{
		return EOcclusionCalculationUIMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOcclusionCalculationUIMode(EOcclusionCalculationUIMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EOcclusionCalculationUIMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionCalculationUIMode_Hash() { return 2094470675U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionCalculationUIMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOcclusionCalculationUIMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionCalculationUIMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOcclusionCalculationUIMode::GeneralizedWindingNumber", (int64)EOcclusionCalculationUIMode::GeneralizedWindingNumber },
				{ "EOcclusionCalculationUIMode::RaycastOcclusionSamples", (int64)EOcclusionCalculationUIMode::RaycastOcclusionSamples },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "GeneralizedWindingNumber.Name", "EOcclusionCalculationUIMode::GeneralizedWindingNumber" },
				{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
				{ "RaycastOcclusionSamples.Comment", "// maps to using fast winding number approximation\n" },
				{ "RaycastOcclusionSamples.Name", "EOcclusionCalculationUIMode::RaycastOcclusionSamples" },
				{ "RaycastOcclusionSamples.ToolTip", "maps to using fast winding number approximation" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EOcclusionCalculationUIMode",
				"EOcclusionCalculationUIMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EOcclusionTriangleSamplingUIMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EOcclusionTriangleSamplingUIMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EOcclusionTriangleSamplingUIMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EOcclusionTriangleSamplingUIMode>()
	{
		return EOcclusionTriangleSamplingUIMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EOcclusionTriangleSamplingUIMode(EOcclusionTriangleSamplingUIMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EOcclusionTriangleSamplingUIMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionTriangleSamplingUIMode_Hash() { return 3464406863U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EOcclusionTriangleSamplingUIMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EOcclusionTriangleSamplingUIMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EOcclusionTriangleSamplingUIMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EOcclusionTriangleSamplingUIMode::Vertices", (int64)EOcclusionTriangleSamplingUIMode::Vertices },
				{ "EOcclusionTriangleSamplingUIMode::VerticesAndCentroids", (int64)EOcclusionTriangleSamplingUIMode::VerticesAndCentroids },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "// these UIMode enums are versions of the enums in Operations/RemoveOccludedTriangles.h, w/ some removed & some renamed to be more user friendly\n" },
				{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
				{ "ToolTip", "these UIMode enums are versions of the enums in Operations/RemoveOccludedTriangles.h, w/ some removed & some renamed to be more user friendly" },
				{ "Vertices.Comment", "// currently do not expose centroid-only option; it almost always looks bad\n" },
				{ "Vertices.Name", "EOcclusionTriangleSamplingUIMode::Vertices" },
				{ "Vertices.ToolTip", "currently do not expose centroid-only option; it almost always looks bad" },
				{ "VerticesAndCentroids.Name", "EOcclusionTriangleSamplingUIMode::VerticesAndCentroids" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EOcclusionTriangleSamplingUIMode",
				"EOcclusionTriangleSamplingUIMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void URemoveOccludedTrianglesToolBuilder::StaticRegisterNativesURemoveOccludedTrianglesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_NoRegister()
	{
		return URemoveOccludedTrianglesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "RemoveOccludedTrianglesTool.h" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveOccludedTrianglesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics::ClassParams = {
		&URemoveOccludedTrianglesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveOccludedTrianglesToolBuilder, 3792943602);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemoveOccludedTrianglesToolBuilder>()
	{
		return URemoveOccludedTrianglesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveOccludedTrianglesToolBuilder(Z_Construct_UClass_URemoveOccludedTrianglesToolBuilder, &URemoveOccludedTrianglesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemoveOccludedTrianglesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveOccludedTrianglesToolBuilder);
	void URemoveOccludedTrianglesToolProperties::StaticRegisterNativesURemoveOccludedTrianglesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_NoRegister()
	{
		return URemoveOccludedTrianglesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_OcclusionTestMethod_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OcclusionTestMethod_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_OcclusionTestMethod;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TriangleSampling_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TriangleSampling_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TriangleSampling;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WindingIsoValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_WindingIsoValue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddRandomRays_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_AddRandomRays;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AddTriangleSamples_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_AddTriangleSamples;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlySelfOcclude_MetaData[];
#endif
		static void NewProp_bOnlySelfOcclude_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlySelfOcclude;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties\n */" },
		{ "IncludePath", "RemoveOccludedTrianglesTool.h" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "Standard properties" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_OcclusionTestMethod_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_OcclusionTestMethod_MetaData[] = {
		{ "Category", "OcclusionCalculation" },
		{ "Comment", "/** The method for deciding whether a triangle is occluded */" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "The method for deciding whether a triangle is occluded" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_OcclusionTestMethod = { "OcclusionTestMethod", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesToolProperties, OcclusionTestMethod), Z_Construct_UEnum_MeshModelingTools_EOcclusionCalculationUIMode, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_OcclusionTestMethod_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_OcclusionTestMethod_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_TriangleSampling_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_TriangleSampling_MetaData[] = {
		{ "Category", "OcclusionCalculation" },
		{ "Comment", "/** Where to sample triangles to test occlusion */" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "Where to sample triangles to test occlusion" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_TriangleSampling = { "TriangleSampling", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesToolProperties, TriangleSampling), Z_Construct_UEnum_MeshModelingTools_EOcclusionTriangleSamplingUIMode, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_TriangleSampling_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_TriangleSampling_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_WindingIsoValue_MetaData[] = {
		{ "Category", "OcclusionCalculation" },
		{ "ClampMax", "2" },
		{ "ClampMin", "-2" },
		{ "Comment", "/** The winding isovalue for GeneralizedWindingNumber mode */" },
		{ "EditCondition", "OcclusionTestMethod==EOcclusionCalculationUIMode::GeneralizedWindingNumber" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "The winding isovalue for GeneralizedWindingNumber mode" },
		{ "UIMax", "1" },
		{ "UIMin", "-1" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_WindingIsoValue = { "WindingIsoValue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesToolProperties, WindingIsoValue), METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_WindingIsoValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_WindingIsoValue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddRandomRays_MetaData[] = {
		{ "Category", "OcclusionCalculation" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** For raycast-based occlusion tests, optionally add random ray direction to increase the accuracy of the visibility sampling */" },
		{ "EditCondition", "OcclusionTestMethod==EOcclusionCalculationUIMode::RaycastOcclusionSamples" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "For raycast-based occlusion tests, optionally add random ray direction to increase the accuracy of the visibility sampling" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddRandomRays = { "AddRandomRays", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesToolProperties, AddRandomRays), METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddRandomRays_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddRandomRays_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddTriangleSamples_MetaData[] = {
		{ "Category", "OcclusionCalculation" },
		{ "ClampMax", "1000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Optionally add random samples to each triangle (in addition to those from TriangleSampling) to increase the accuracy of the visibility sampling */" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "Optionally add random samples to each triangle (in addition to those from TriangleSampling) to increase the accuracy of the visibility sampling" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddTriangleSamples = { "AddTriangleSamples", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesToolProperties, AddTriangleSamples), METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddTriangleSamples_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddTriangleSamples_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_bOnlySelfOcclude_MetaData[] = {
		{ "Category", "OcclusionCalculation" },
		{ "Comment", "/** If false, when multiple meshes are selected the meshes can occlude each other.  When true, we process each selected mesh independently and only consider self-occlusions. */" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "If false, when multiple meshes are selected the meshes can occlude each other.  When true, we process each selected mesh independently and only consider self-occlusions." },
	};
#endif
	void Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_bOnlySelfOcclude_SetBit(void* Obj)
	{
		((URemoveOccludedTrianglesToolProperties*)Obj)->bOnlySelfOcclude = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_bOnlySelfOcclude = { "bOnlySelfOcclude", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemoveOccludedTrianglesToolProperties), &Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_bOnlySelfOcclude_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_bOnlySelfOcclude_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_bOnlySelfOcclude_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_OcclusionTestMethod_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_OcclusionTestMethod,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_TriangleSampling_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_TriangleSampling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_WindingIsoValue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddRandomRays,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_AddTriangleSamples,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::NewProp_bOnlySelfOcclude,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveOccludedTrianglesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::ClassParams = {
		&URemoveOccludedTrianglesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveOccludedTrianglesToolProperties, 807897007);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemoveOccludedTrianglesToolProperties>()
	{
		return URemoveOccludedTrianglesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveOccludedTrianglesToolProperties(Z_Construct_UClass_URemoveOccludedTrianglesToolProperties, &URemoveOccludedTrianglesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemoveOccludedTrianglesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveOccludedTrianglesToolProperties);
	void URemoveOccludedTrianglesAdvancedProperties::StaticRegisterNativesURemoveOccludedTrianglesAdvancedProperties()
	{
	}
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_NoRegister()
	{
		return URemoveOccludedTrianglesAdvancedProperties::StaticClass();
	}
	struct Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Advanced properties\n */" },
		{ "IncludePath", "RemoveOccludedTrianglesTool.h" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "Advanced properties" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveOccludedTrianglesAdvancedProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics::ClassParams = {
		&URemoveOccludedTrianglesAdvancedProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveOccludedTrianglesAdvancedProperties, 3182892264);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemoveOccludedTrianglesAdvancedProperties>()
	{
		return URemoveOccludedTrianglesAdvancedProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveOccludedTrianglesAdvancedProperties(Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties, &URemoveOccludedTrianglesAdvancedProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemoveOccludedTrianglesAdvancedProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveOccludedTrianglesAdvancedProperties);
	void URemoveOccludedTrianglesOperatorFactory::StaticRegisterNativesURemoveOccludedTrianglesOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_NoRegister()
	{
		return URemoveOccludedTrianglesOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n *  stores a pointer to the tool and enough info to know which specific operator it should spawn\n */" },
		{ "IncludePath", "RemoveOccludedTrianglesTool.h" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n stores a pointer to the tool and enough info to know which specific operator it should spawn" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::NewProp_Tool_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::NewProp_Tool = { "Tool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesOperatorFactory, Tool), Z_Construct_UClass_URemoveOccludedTrianglesTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::NewProp_Tool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::NewProp_Tool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::NewProp_Tool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveOccludedTrianglesOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::ClassParams = {
		&URemoveOccludedTrianglesOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveOccludedTrianglesOperatorFactory, 2301211153);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemoveOccludedTrianglesOperatorFactory>()
	{
		return URemoveOccludedTrianglesOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveOccludedTrianglesOperatorFactory(Z_Construct_UClass_URemoveOccludedTrianglesOperatorFactory, &URemoveOccludedTrianglesOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemoveOccludedTrianglesOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveOccludedTrianglesOperatorFactory);
	void URemoveOccludedTrianglesTool::StaticRegisterNativesURemoveOccludedTrianglesTool()
	{
	}
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesTool_NoRegister()
	{
		return URemoveOccludedTrianglesTool::StaticClass();
	}
	struct Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdvancedProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AdvancedProperties;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Previews_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Previews_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Previews;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewCopies_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewCopies_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PreviewCopies;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Normal Updating Tool\n */" },
		{ "IncludePath", "RemoveOccludedTrianglesTool.h" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "Simple Mesh Normal Updating Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesTool, BasicProperties), Z_Construct_UClass_URemoveOccludedTrianglesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_AdvancedProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_AdvancedProperties = { "AdvancedProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesTool, AdvancedProperties), Z_Construct_UClass_URemoveOccludedTrianglesAdvancedProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_AdvancedProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_AdvancedProperties_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_Previews_Inner = { "Previews", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_Previews_MetaData[] = {
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_Previews = { "Previews", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesTool, Previews), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_Previews_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_Previews_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_PreviewCopies_Inner = { "PreviewCopies", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_PreviewCopies_MetaData[] = {
		{ "Comment", "// When multiple meshes in the selection correspond to the same asset, only one needs a PreviewWithBackgroundCompute\n//  all others just get a plain PreviewMesh copy that is updated via OnMeshUpdated broadcast from the source Preview\n" },
		{ "ModuleRelativePath", "Public/RemoveOccludedTrianglesTool.h" },
		{ "ToolTip", "When multiple meshes in the selection correspond to the same asset, only one needs a PreviewWithBackgroundCompute\n all others just get a plain PreviewMesh copy that is updated via OnMeshUpdated broadcast from the source Preview" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_PreviewCopies = { "PreviewCopies", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemoveOccludedTrianglesTool, PreviewCopies), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_PreviewCopies_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_PreviewCopies_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_AdvancedProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_Previews_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_Previews,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_PreviewCopies_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::NewProp_PreviewCopies,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemoveOccludedTrianglesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::ClassParams = {
		&URemoveOccludedTrianglesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemoveOccludedTrianglesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemoveOccludedTrianglesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemoveOccludedTrianglesTool, 2759820141);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemoveOccludedTrianglesTool>()
	{
		return URemoveOccludedTrianglesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemoveOccludedTrianglesTool(Z_Construct_UClass_URemoveOccludedTrianglesTool, &URemoveOccludedTrianglesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemoveOccludedTrianglesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemoveOccludedTrianglesTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
