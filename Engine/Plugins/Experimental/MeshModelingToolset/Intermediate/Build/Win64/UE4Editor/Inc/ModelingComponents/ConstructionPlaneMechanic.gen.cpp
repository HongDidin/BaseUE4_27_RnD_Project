// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Mechanics/ConstructionPlaneMechanic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeConstructionPlaneMechanic() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UConstructionPlaneMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractionMechanic();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformGizmo_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UTransformProxy_NoRegister();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickInputBehavior_NoRegister();
// End Cross Module References
	void UConstructionPlaneMechanic::StaticRegisterNativesUConstructionPlaneMechanic()
	{
	}
	UClass* Z_Construct_UClass_UConstructionPlaneMechanic_NoRegister()
	{
		return UConstructionPlaneMechanic::StaticClass();
	}
	struct Z_Construct_UClass_UConstructionPlaneMechanic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformGizmo_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformGizmo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PlaneTransformProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PlaneTransformProxy;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ClickToSetPlaneBehavior_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ClickToSetPlaneBehavior;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UConstructionPlaneMechanic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractionMechanic,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstructionPlaneMechanic_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UConstructionPlaneMechanic implements an interaction in which a 3D plane can be\n * positioned using the standard 3D Gizmo, or placed at hit-locations in the existing scene.\n * A grid in the plane can optionally be rendered.\n */" },
		{ "IncludePath", "Mechanics/ConstructionPlaneMechanic.h" },
		{ "ModuleRelativePath", "Public/Mechanics/ConstructionPlaneMechanic.h" },
		{ "ToolTip", "UConstructionPlaneMechanic implements an interaction in which a 3D plane can be\npositioned using the standard 3D Gizmo, or placed at hit-locations in the existing scene.\nA grid in the plane can optionally be rendered." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformGizmo_MetaData[] = {
		{ "ModuleRelativePath", "Public/Mechanics/ConstructionPlaneMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformGizmo = { "PlaneTransformGizmo", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConstructionPlaneMechanic, PlaneTransformGizmo), Z_Construct_UClass_UTransformGizmo_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformGizmo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformGizmo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformProxy_MetaData[] = {
		{ "ModuleRelativePath", "Public/Mechanics/ConstructionPlaneMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformProxy = { "PlaneTransformProxy", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConstructionPlaneMechanic, PlaneTransformProxy), Z_Construct_UClass_UTransformProxy_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformProxy_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_ClickToSetPlaneBehavior_MetaData[] = {
		{ "ModuleRelativePath", "Public/Mechanics/ConstructionPlaneMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_ClickToSetPlaneBehavior = { "ClickToSetPlaneBehavior", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UConstructionPlaneMechanic, ClickToSetPlaneBehavior), Z_Construct_UClass_USingleClickInputBehavior_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_ClickToSetPlaneBehavior_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_ClickToSetPlaneBehavior_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UConstructionPlaneMechanic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformGizmo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_PlaneTransformProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UConstructionPlaneMechanic_Statics::NewProp_ClickToSetPlaneBehavior,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UConstructionPlaneMechanic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UConstructionPlaneMechanic>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UConstructionPlaneMechanic_Statics::ClassParams = {
		&UConstructionPlaneMechanic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UConstructionPlaneMechanic_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UConstructionPlaneMechanic_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UConstructionPlaneMechanic()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UConstructionPlaneMechanic_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UConstructionPlaneMechanic, 336664125);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UConstructionPlaneMechanic>()
	{
		return UConstructionPlaneMechanic::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UConstructionPlaneMechanic(Z_Construct_UClass_UConstructionPlaneMechanic, &UConstructionPlaneMechanic::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UConstructionPlaneMechanic"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UConstructionPlaneMechanic);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
