// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_MeshVertexChange_generated_h
#error "MeshVertexChange.generated.h already included, missing '#pragma once' in MeshVertexChange.h"
#endif
#define MODELINGCOMPONENTS_MeshVertexChange_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshVertexCommandChangeTarget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshVertexCommandChangeTarget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshVertexCommandChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshVertexCommandChangeTarget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshVertexCommandChangeTarget(UMeshVertexCommandChangeTarget&&); \
	NO_API UMeshVertexCommandChangeTarget(const UMeshVertexCommandChangeTarget&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshVertexCommandChangeTarget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshVertexCommandChangeTarget(UMeshVertexCommandChangeTarget&&); \
	NO_API UMeshVertexCommandChangeTarget(const UMeshVertexCommandChangeTarget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshVertexCommandChangeTarget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshVertexCommandChangeTarget); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshVertexCommandChangeTarget)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUMeshVertexCommandChangeTarget(); \
	friend struct Z_Construct_UClass_UMeshVertexCommandChangeTarget_Statics; \
public: \
	DECLARE_CLASS(UMeshVertexCommandChangeTarget, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UMeshVertexCommandChangeTarget)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IMeshVertexCommandChangeTarget() {} \
public: \
	typedef UMeshVertexCommandChangeTarget UClassType; \
	typedef IMeshVertexCommandChangeTarget ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_INCLASS_IINTERFACE \
protected: \
	virtual ~IMeshVertexCommandChangeTarget() {} \
public: \
	typedef UMeshVertexCommandChangeTarget UClassType; \
	typedef IMeshVertexCommandChangeTarget ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_100_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_110_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_110_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h_103_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UMeshVertexCommandChangeTarget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Changes_MeshVertexChange_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
