// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_RemeshMeshTool_generated_h
#error "RemeshMeshTool.generated.h already included, missing '#pragma once' in RemeshMeshTool.h"
#endif
#define MESHMODELINGTOOLS_RemeshMeshTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemeshMeshToolBuilder(); \
	friend struct Z_Construct_UClass_URemeshMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(URemeshMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_INCLASS \
private: \
	static void StaticRegisterNativesURemeshMeshToolBuilder(); \
	friend struct Z_Construct_UClass_URemeshMeshToolBuilder_Statics; \
public: \
	DECLARE_CLASS(URemeshMeshToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemeshMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemeshMeshToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshMeshToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshMeshToolBuilder(URemeshMeshToolBuilder&&); \
	NO_API URemeshMeshToolBuilder(const URemeshMeshToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemeshMeshToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshMeshToolBuilder(URemeshMeshToolBuilder&&); \
	NO_API URemeshMeshToolBuilder(const URemeshMeshToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshMeshToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshMeshToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemeshMeshToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_21_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URemeshMeshToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemeshMeshToolProperties(); \
	friend struct Z_Construct_UClass_URemeshMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(URemeshMeshToolProperties, URemeshProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_INCLASS \
private: \
	static void StaticRegisterNativesURemeshMeshToolProperties(); \
	friend struct Z_Construct_UClass_URemeshMeshToolProperties_Statics; \
public: \
	DECLARE_CLASS(URemeshMeshToolProperties, URemeshProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemeshMeshToolProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemeshMeshToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshMeshToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshMeshToolProperties(URemeshMeshToolProperties&&); \
	NO_API URemeshMeshToolProperties(const URemeshMeshToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshMeshToolProperties(URemeshMeshToolProperties&&); \
	NO_API URemeshMeshToolProperties(const URemeshMeshToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshMeshToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshMeshToolProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URemeshMeshToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_41_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URemeshMeshToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURemeshMeshTool(); \
	friend struct Z_Construct_UClass_URemeshMeshTool_Statics; \
public: \
	DECLARE_CLASS(URemeshMeshTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_INCLASS \
private: \
	static void StaticRegisterNativesURemeshMeshTool(); \
	friend struct Z_Construct_UClass_URemeshMeshTool_Statics; \
public: \
	DECLARE_CLASS(URemeshMeshTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URemeshMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URemeshMeshTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemeshMeshTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshMeshTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshMeshTool(URemeshMeshTool&&); \
	NO_API URemeshMeshTool(const URemeshMeshTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URemeshMeshTool(URemeshMeshTool&&); \
	NO_API URemeshMeshTool(const URemeshMeshTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URemeshMeshTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URemeshMeshTool); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URemeshMeshTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_99_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h_102_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URemeshMeshTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RemeshMeshTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
