// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/VoxelBlendMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVoxelBlendMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelBlendMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelBlendMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelBlendMeshesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelBlendMeshesTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseVoxelTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelBlendMeshesToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder();
// End Cross Module References
	void UVoxelBlendMeshesToolProperties::StaticRegisterNativesUVoxelBlendMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UVoxelBlendMeshesToolProperties_NoRegister()
	{
		return UVoxelBlendMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendPower_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_BlendPower;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendFalloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_BlendFalloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSolidifyInput_MetaData[];
#endif
		static void NewProp_bSolidifyInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSolidifyInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveInternalsAfterSolidify_MetaData[];
#endif
		static void NewProp_bRemoveInternalsAfterSolidify_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveInternalsAfterSolidify;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetSolidifySurface_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_OffsetSolidifySurface;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Properties of the blend operation\n */" },
		{ "IncludePath", "VoxelBlendMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
		{ "ToolTip", "Properties of the blend operation" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendPower_MetaData[] = {
		{ "Category", "Blend" },
		{ "ClampMax", "10" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Blend power controls the shape of the blend between shapes */" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
		{ "ToolTip", "Blend power controls the shape of the blend between shapes" },
		{ "UIMax", "4" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendPower = { "BlendPower", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelBlendMeshesToolProperties, BlendPower), METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendPower_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendPower_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendFalloff_MetaData[] = {
		{ "Category", "Blend" },
		{ "ClampMax", "1000" },
		{ "ClampMin", ".001" },
		{ "Comment", "/** Blend falloff controls the size of the blend region */" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
		{ "ToolTip", "Blend falloff controls the size of the blend region" },
		{ "UIMax", "100" },
		{ "UIMin", ".1" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendFalloff = { "BlendFalloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelBlendMeshesToolProperties, BlendFalloff), METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendFalloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendFalloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bSolidifyInput_MetaData[] = {
		{ "Category", "Solidify" },
		{ "Comment", "/** Solidify the input mesh(es) before processing, fixing results for inputs with holes and/or self-intersections */" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
		{ "ToolTip", "Solidify the input mesh(es) before processing, fixing results for inputs with holes and/or self-intersections" },
	};
#endif
	void Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bSolidifyInput_SetBit(void* Obj)
	{
		((UVoxelBlendMeshesToolProperties*)Obj)->bSolidifyInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bSolidifyInput = { "bSolidifyInput", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVoxelBlendMeshesToolProperties), &Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bSolidifyInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bSolidifyInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bSolidifyInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_MetaData[] = {
		{ "Category", "Solidify" },
		{ "Comment", "/** Remove internal surfaces from the solidified input */" },
		{ "EditCondition", "bSolidifyInput == true" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
		{ "ToolTip", "Remove internal surfaces from the solidified input" },
	};
#endif
	void Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_SetBit(void* Obj)
	{
		((UVoxelBlendMeshesToolProperties*)Obj)->bRemoveInternalsAfterSolidify = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify = { "bRemoveInternalsAfterSolidify", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVoxelBlendMeshesToolProperties), &Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface_MetaData[] = {
		{ "Category", "Solidify" },
		{ "Comment", "/** Offset surface to create when solidifying any open-boundary inputs; if 0 then no offset surfaces are created */" },
		{ "EditCondition", "bSolidifyInput == true" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
		{ "ToolTip", "Offset surface to create when solidifying any open-boundary inputs; if 0 then no offset surfaces are created" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface = { "OffsetSolidifySurface", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelBlendMeshesToolProperties, OffsetSolidifySurface), METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendPower,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_BlendFalloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bSolidifyInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelBlendMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::ClassParams = {
		&UVoxelBlendMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelBlendMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelBlendMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelBlendMeshesToolProperties, 3755998731);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelBlendMeshesToolProperties>()
	{
		return UVoxelBlendMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelBlendMeshesToolProperties(Z_Construct_UClass_UVoxelBlendMeshesToolProperties, &UVoxelBlendMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelBlendMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelBlendMeshesToolProperties);
	void UVoxelBlendMeshesTool::StaticRegisterNativesUVoxelBlendMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UVoxelBlendMeshesTool_NoRegister()
	{
		return UVoxelBlendMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelBlendMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlendProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BlendProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseVoxelTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Tool to smoothly blend meshes together\n */" },
		{ "IncludePath", "VoxelBlendMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
		{ "ToolTip", "Tool to smoothly blend meshes together" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::NewProp_BlendProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::NewProp_BlendProperties = { "BlendProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelBlendMeshesTool, BlendProperties), Z_Construct_UClass_UVoxelBlendMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::NewProp_BlendProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::NewProp_BlendProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::NewProp_BlendProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelBlendMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::ClassParams = {
		&UVoxelBlendMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelBlendMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelBlendMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelBlendMeshesTool, 265275362);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelBlendMeshesTool>()
	{
		return UVoxelBlendMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelBlendMeshesTool(Z_Construct_UClass_UVoxelBlendMeshesTool, &UVoxelBlendMeshesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelBlendMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelBlendMeshesTool);
	void UVoxelBlendMeshesToolBuilder::StaticRegisterNativesUVoxelBlendMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_NoRegister()
	{
		return UVoxelBlendMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VoxelBlendMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelBlendMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelBlendMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics::ClassParams = {
		&UVoxelBlendMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelBlendMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelBlendMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelBlendMeshesToolBuilder, 1898759393);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelBlendMeshesToolBuilder>()
	{
		return UVoxelBlendMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelBlendMeshesToolBuilder(Z_Construct_UClass_UVoxelBlendMeshesToolBuilder, &UVoxelBlendMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelBlendMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelBlendMeshesToolBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
