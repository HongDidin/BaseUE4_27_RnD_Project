// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/CombineMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCombineMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_ECombineTargetType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCombineMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCombineMeshesToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCombineMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCombineMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCombineMeshesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCombineMeshesTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister();
// End Cross Module References
	static UEnum* ECombineTargetType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_ECombineTargetType, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("ECombineTargetType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<ECombineTargetType>()
	{
		return ECombineTargetType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECombineTargetType(ECombineTargetType_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("ECombineTargetType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_ECombineTargetType_Hash() { return 4191010191U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_ECombineTargetType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECombineTargetType"), 0, Get_Z_Construct_UEnum_MeshModelingTools_ECombineTargetType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECombineTargetType::NewAsset", (int64)ECombineTargetType::NewAsset },
				{ "ECombineTargetType::FirstInputAsset", (int64)ECombineTargetType::FirstInputAsset },
				{ "ECombineTargetType::LastInputAsset", (int64)ECombineTargetType::LastInputAsset },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FirstInputAsset.Name", "ECombineTargetType::FirstInputAsset" },
				{ "LastInputAsset.Name", "ECombineTargetType::LastInputAsset" },
				{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
				{ "NewAsset.Name", "ECombineTargetType::NewAsset" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"ECombineTargetType",
				"ECombineTargetType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UCombineMeshesToolBuilder::StaticRegisterNativesUCombineMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UCombineMeshesToolBuilder_NoRegister()
	{
		return UCombineMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UCombineMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCombineMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "CombineMeshesTool.h" },
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCombineMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCombineMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCombineMeshesToolBuilder_Statics::ClassParams = {
		&UCombineMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCombineMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCombineMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCombineMeshesToolBuilder, 2882738779);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UCombineMeshesToolBuilder>()
	{
		return UCombineMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCombineMeshesToolBuilder(Z_Construct_UClass_UCombineMeshesToolBuilder, &UCombineMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UCombineMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCombineMeshesToolBuilder);
	void UCombineMeshesToolProperties::StaticRegisterNativesUCombineMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UCombineMeshesToolProperties_NoRegister()
	{
		return UCombineMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UCombineMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsDuplicateMode_MetaData[];
#endif
		static void NewProp_bIsDuplicateMode_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsDuplicateMode;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_WriteOutputTo_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WriteOutputTo_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WriteOutputTo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutputName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutputAsset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCombineMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties\n */" },
		{ "IncludePath", "CombineMeshesTool.h" },
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
		{ "ToolTip", "Standard properties" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_bIsDuplicateMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_bIsDuplicateMode_SetBit(void* Obj)
	{
		((UCombineMeshesToolProperties*)Obj)->bIsDuplicateMode = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_bIsDuplicateMode = { "bIsDuplicateMode", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UCombineMeshesToolProperties), &Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_bIsDuplicateMode_SetBit, METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_bIsDuplicateMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_bIsDuplicateMode_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_WriteOutputTo_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_WriteOutputTo_MetaData[] = {
		{ "Category", "AssetOptions" },
		{ "EditCondition", "bIsDuplicateMode == false" },
		{ "EditConditionHides", "" },
		{ "HideEditConditionToggle", "" },
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_WriteOutputTo = { "WriteOutputTo", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCombineMeshesToolProperties, WriteOutputTo), Z_Construct_UEnum_MeshModelingTools_ECombineTargetType, METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_WriteOutputTo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_WriteOutputTo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputName_MetaData[] = {
		{ "Category", "AssetOptions" },
		{ "Comment", "/** Base name for newly-generated asset */" },
		{ "EditCondition", "bIsDuplicateMode || WriteOutputTo == ECombineTargetType::NewAsset" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
		{ "ToolTip", "Base name for newly-generated asset" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputName = { "OutputName", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCombineMeshesToolProperties, OutputName), METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputAsset_MetaData[] = {
		{ "Category", "AssetOptions" },
		{ "Comment", "/** Name of asset that will be updated */" },
		{ "EditCondition", "bIsDuplicateMode == false && WriteOutputTo != ECombineTargetType::NewAsset" },
		{ "EditConditionHides", "" },
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
		{ "ToolTip", "Name of asset that will be updated" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputAsset = { "OutputAsset", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCombineMeshesToolProperties, OutputAsset), METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputAsset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCombineMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_bIsDuplicateMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_WriteOutputTo_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_WriteOutputTo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCombineMeshesToolProperties_Statics::NewProp_OutputAsset,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCombineMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCombineMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCombineMeshesToolProperties_Statics::ClassParams = {
		&UCombineMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCombineMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCombineMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCombineMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCombineMeshesToolProperties, 2927262308);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UCombineMeshesToolProperties>()
	{
		return UCombineMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCombineMeshesToolProperties(Z_Construct_UClass_UCombineMeshesToolProperties, &UCombineMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UCombineMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCombineMeshesToolProperties);
	void UCombineMeshesTool::StaticRegisterNativesUCombineMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UCombineMeshesTool_NoRegister()
	{
		return UCombineMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UCombineMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandleSourceProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HandleSourceProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCombineMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple tool to combine multiple meshes into a single mesh asset\n */" },
		{ "IncludePath", "CombineMeshesTool.h" },
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
		{ "ToolTip", "Simple tool to combine multiple meshes into a single mesh asset" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCombineMeshesTool, BasicProperties), Z_Construct_UClass_UCombineMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_HandleSourceProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/CombineMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_HandleSourceProperties = { "HandleSourceProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UCombineMeshesTool, HandleSourceProperties), Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_HandleSourceProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_HandleSourceProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UCombineMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UCombineMeshesTool_Statics::NewProp_HandleSourceProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCombineMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCombineMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCombineMeshesTool_Statics::ClassParams = {
		&UCombineMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UCombineMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UCombineMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCombineMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCombineMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCombineMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCombineMeshesTool, 4104969644);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UCombineMeshesTool>()
	{
		return UCombineMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCombineMeshesTool(Z_Construct_UClass_UCombineMeshesTool, &UCombineMeshesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UCombineMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCombineMeshesTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
