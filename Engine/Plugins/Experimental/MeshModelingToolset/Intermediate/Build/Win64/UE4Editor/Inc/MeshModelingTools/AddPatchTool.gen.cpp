// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/AddPatchTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAddPatchTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPatchToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPatchToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPatchToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPatchToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPatchTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UAddPatchTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
// End Cross Module References
	void UAddPatchToolBuilder::StaticRegisterNativesUAddPatchToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UAddPatchToolBuilder_NoRegister()
	{
		return UAddPatchToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UAddPatchToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddPatchToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "AddPatchTool.h" },
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddPatchToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddPatchToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddPatchToolBuilder_Statics::ClassParams = {
		&UAddPatchToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddPatchToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddPatchToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddPatchToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddPatchToolBuilder, 3769744613);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddPatchToolBuilder>()
	{
		return UAddPatchToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddPatchToolBuilder(Z_Construct_UClass_UAddPatchToolBuilder, &UAddPatchToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddPatchToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddPatchToolBuilder);
	void UAddPatchToolProperties::StaticRegisterNativesUAddPatchToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UAddPatchToolProperties_NoRegister()
	{
		return UAddPatchToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UAddPatchToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Rotation_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Rotation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Subdivisions_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_Subdivisions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shift_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Shift;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddPatchToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AddPatchTool.h" },
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "PatchSettings" },
		{ "ClampMax", "1000000.0" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Width of Shape */" },
		{ "DisplayName", "Width" },
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
		{ "ToolTip", "Width of Shape" },
		{ "UIMax", "1000.0" },
		{ "UIMin", "1.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPatchToolProperties, Width), METADATA_PARAMS(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Rotation_MetaData[] = {
		{ "Category", "PatchSettings" },
		{ "Comment", "/** Rotation around up axis */" },
		{ "DisplayName", "Rotation" },
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
		{ "ToolTip", "Rotation around up axis" },
		{ "UIMax", "360.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Rotation = { "Rotation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPatchToolProperties, Rotation), METADATA_PARAMS(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Rotation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Rotation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Subdivisions_MetaData[] = {
		{ "Category", "PatchSettings" },
		{ "ClampMax", "4000" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Subdivisions */" },
		{ "DisplayName", "Subdivisions" },
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
		{ "ToolTip", "Subdivisions" },
		{ "UIMax", "100" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Subdivisions = { "Subdivisions", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPatchToolProperties, Subdivisions), METADATA_PARAMS(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Subdivisions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Subdivisions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Shift_MetaData[] = {
		{ "Category", "PatchSettings" },
		{ "Comment", "/** Rotation around up axis */" },
		{ "DisplayName", "Shift" },
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
		{ "ToolTip", "Rotation around up axis" },
		{ "UIMax", "1000" },
		{ "UIMin", "-1000" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Shift = { "Shift", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPatchToolProperties, Shift), METADATA_PARAMS(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Shift_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Shift_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAddPatchToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Rotation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Subdivisions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPatchToolProperties_Statics::NewProp_Shift,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddPatchToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddPatchToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddPatchToolProperties_Statics::ClassParams = {
		&UAddPatchToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAddPatchToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddPatchToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddPatchToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddPatchToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddPatchToolProperties, 3285226151);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddPatchToolProperties>()
	{
		return UAddPatchToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddPatchToolProperties(Z_Construct_UClass_UAddPatchToolProperties, &UAddPatchToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddPatchToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddPatchToolProperties);
	void UAddPatchTool::StaticRegisterNativesUAddPatchTool()
	{
	}
	UClass* Z_Construct_UClass_UAddPatchTool_NoRegister()
	{
		return UAddPatchTool::StaticClass();
	}
	struct Z_Construct_UClass_UAddPatchTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShapeSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShapeSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAddPatchTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleClickTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "AddPatchTool.h" },
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchTool_Statics::NewProp_ShapeSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAddPatchTool_Statics::NewProp_ShapeSettings = { "ShapeSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPatchTool, ShapeSettings), Z_Construct_UClass_UAddPatchToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAddPatchTool_Statics::NewProp_ShapeSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchTool_Statics::NewProp_ShapeSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchTool_Statics::NewProp_MaterialProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAddPatchTool_Statics::NewProp_MaterialProperties = { "MaterialProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPatchTool, MaterialProperties), Z_Construct_UClass_UNewMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAddPatchTool_Statics::NewProp_MaterialProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchTool_Statics::NewProp_MaterialProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAddPatchTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/AddPatchTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UAddPatchTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UAddPatchTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UAddPatchTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchTool_Statics::NewProp_PreviewMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UAddPatchTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPatchTool_Statics::NewProp_ShapeSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPatchTool_Statics::NewProp_MaterialProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UAddPatchTool_Statics::NewProp_PreviewMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAddPatchTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAddPatchTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAddPatchTool_Statics::ClassParams = {
		&UAddPatchTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UAddPatchTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAddPatchTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAddPatchTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAddPatchTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAddPatchTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAddPatchTool, 1267688530);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UAddPatchTool>()
	{
		return UAddPatchTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAddPatchTool(Z_Construct_UClass_UAddPatchTool, &UAddPatchTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UAddPatchTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAddPatchTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
