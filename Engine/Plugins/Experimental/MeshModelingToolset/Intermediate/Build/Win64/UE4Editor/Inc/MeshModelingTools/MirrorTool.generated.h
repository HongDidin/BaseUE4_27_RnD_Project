// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MirrorTool_generated_h
#error "MirrorTool.generated.h already included, missing '#pragma once' in MirrorTool.h"
#endif
#define MESHMODELINGTOOLS_MirrorTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMirrorToolBuilder(); \
	friend struct Z_Construct_UClass_UMirrorToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMirrorToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUMirrorToolBuilder(); \
	friend struct Z_Construct_UClass_UMirrorToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMirrorToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorToolBuilder(UMirrorToolBuilder&&); \
	NO_API UMirrorToolBuilder(const UMirrorToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorToolBuilder(UMirrorToolBuilder&&); \
	NO_API UMirrorToolBuilder(const UMirrorToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_21_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMirrorToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMirrorToolProperties(); \
	friend struct Z_Construct_UClass_UMirrorToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMirrorToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_INCLASS \
private: \
	static void StaticRegisterNativesUMirrorToolProperties(); \
	friend struct Z_Construct_UClass_UMirrorToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMirrorToolProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorToolProperties(UMirrorToolProperties&&); \
	NO_API UMirrorToolProperties(const UMirrorToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorToolProperties(UMirrorToolProperties&&); \
	NO_API UMirrorToolProperties(const UMirrorToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_66_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_69_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMirrorToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMirrorOperatorFactory(); \
	friend struct Z_Construct_UClass_UMirrorOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(UMirrorOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_INCLASS \
private: \
	static void StaticRegisterNativesUMirrorOperatorFactory(); \
	friend struct Z_Construct_UClass_UMirrorOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(UMirrorOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorOperatorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorOperatorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorOperatorFactory(UMirrorOperatorFactory&&); \
	NO_API UMirrorOperatorFactory(const UMirrorOperatorFactory&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorOperatorFactory(UMirrorOperatorFactory&&); \
	NO_API UMirrorOperatorFactory(const UMirrorOperatorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorOperatorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_111_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_114_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMirrorOperatorFactory>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBackward); \
	DECLARE_FUNCTION(execForward); \
	DECLARE_FUNCTION(execDown); \
	DECLARE_FUNCTION(execUp); \
	DECLARE_FUNCTION(execRight); \
	DECLARE_FUNCTION(execLeft); \
	DECLARE_FUNCTION(execShiftToCenter);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBackward); \
	DECLARE_FUNCTION(execForward); \
	DECLARE_FUNCTION(execDown); \
	DECLARE_FUNCTION(execUp); \
	DECLARE_FUNCTION(execRight); \
	DECLARE_FUNCTION(execLeft); \
	DECLARE_FUNCTION(execShiftToCenter);


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMirrorToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UMirrorToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UMirrorToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_INCLASS \
private: \
	static void StaticRegisterNativesUMirrorToolActionPropertySet(); \
	friend struct Z_Construct_UClass_UMirrorToolActionPropertySet_Statics; \
public: \
	DECLARE_CLASS(UMirrorToolActionPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorToolActionPropertySet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorToolActionPropertySet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorToolActionPropertySet(UMirrorToolActionPropertySet&&); \
	NO_API UMirrorToolActionPropertySet(const UMirrorToolActionPropertySet&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorToolActionPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorToolActionPropertySet(UMirrorToolActionPropertySet&&); \
	NO_API UMirrorToolActionPropertySet(const UMirrorToolActionPropertySet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorToolActionPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorToolActionPropertySet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorToolActionPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_142_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_145_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMirrorToolActionPropertySet>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMirrorTool(); \
	friend struct Z_Construct_UClass_UMirrorTool_Statics; \
public: \
	DECLARE_CLASS(UMirrorTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_INCLASS \
private: \
	static void StaticRegisterNativesUMirrorTool(); \
	friend struct Z_Construct_UClass_UMirrorTool_Statics; \
public: \
	DECLARE_CLASS(UMirrorTool, UMultiSelectionTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMirrorTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMirrorTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMirrorTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorTool(UMirrorTool&&); \
	NO_API UMirrorTool(const UMirrorTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMirrorTool(UMirrorTool&&); \
	NO_API UMirrorTool(const UMirrorTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMirrorTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMirrorTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMirrorTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(UMirrorTool, Settings); } \
	FORCEINLINE static uint32 __PPO__ToolActions() { return STRUCT_OFFSET(UMirrorTool, ToolActions); } \
	FORCEINLINE static uint32 __PPO__MeshesToMirror() { return STRUCT_OFFSET(UMirrorTool, MeshesToMirror); } \
	FORCEINLINE static uint32 __PPO__Previews() { return STRUCT_OFFSET(UMirrorTool, Previews); } \
	FORCEINLINE static uint32 __PPO__PlaneMechanic() { return STRUCT_OFFSET(UMirrorTool, PlaneMechanic); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_183_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h_186_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMirrorTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_MirrorTool_h


#define FOREACH_ENUM_EMIRRORTOOLACTION(op) \
	op(EMirrorToolAction::NoAction) \
	op(EMirrorToolAction::ShiftToCenter) \
	op(EMirrorToolAction::Left) \
	op(EMirrorToolAction::Right) \
	op(EMirrorToolAction::Up) \
	op(EMirrorToolAction::Down) \
	op(EMirrorToolAction::Forward) \
	op(EMirrorToolAction::Backward) 

enum class EMirrorToolAction;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorToolAction>();

#define FOREACH_ENUM_EMIRRORCTRLCLICKBEHAVIOR(op) \
	op(EMirrorCtrlClickBehavior::Reposition) \
	op(EMirrorCtrlClickBehavior::RepositionAndReorient) 

enum class EMirrorCtrlClickBehavior : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorCtrlClickBehavior>();

#define FOREACH_ENUM_EMIRROROPERATIONMODE(op) \
	op(EMirrorOperationMode::MirrorAndAppend) \
	op(EMirrorOperationMode::MirrorExisting) 

enum class EMirrorOperationMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorOperationMode>();

#define FOREACH_ENUM_EMIRRORSAVEMODE(op) \
	op(EMirrorSaveMode::UpdateAssets) \
	op(EMirrorSaveMode::CreateNewAssets) 

enum class EMirrorSaveMode : uint8;
template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMirrorSaveMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
