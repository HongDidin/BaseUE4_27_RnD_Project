// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_TriangleSetComponent_generated_h
#error "TriangleSetComponent.generated.h already included, missing '#pragma once' in TriangleSetComponent.h"
#endif
#define MODELINGCOMPONENTS_TriangleSetComponent_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_50_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRenderableTriangle_Statics; \
	MODELINGCOMPONENTS_API static class UScriptStruct* StaticStruct();


template<> MODELINGCOMPONENTS_API UScriptStruct* StaticStruct<struct FRenderableTriangle>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FRenderableTriangleVertex_Statics; \
	MODELINGCOMPONENTS_API static class UScriptStruct* StaticStruct();


template<> MODELINGCOMPONENTS_API UScriptStruct* StaticStruct<struct FRenderableTriangleVertex>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTriangleSetComponent(); \
	friend struct Z_Construct_UClass_UTriangleSetComponent_Statics; \
public: \
	DECLARE_CLASS(UTriangleSetComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UTriangleSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_INCLASS \
private: \
	static void StaticRegisterNativesUTriangleSetComponent(); \
	friend struct Z_Construct_UClass_UTriangleSetComponent_Statics; \
public: \
	DECLARE_CLASS(UTriangleSetComponent, UMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(UTriangleSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTriangleSetComponent(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTriangleSetComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTriangleSetComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTriangleSetComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTriangleSetComponent(UTriangleSetComponent&&); \
	NO_API UTriangleSetComponent(const UTriangleSetComponent&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTriangleSetComponent(UTriangleSetComponent&&); \
	NO_API UTriangleSetComponent(const UTriangleSetComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTriangleSetComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTriangleSetComponent); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTriangleSetComponent)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Bounds() { return STRUCT_OFFSET(UTriangleSetComponent, Bounds); } \
	FORCEINLINE static uint32 __PPO__bBoundsDirty() { return STRUCT_OFFSET(UTriangleSetComponent, bBoundsDirty); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_84_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h_87_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class UTriangleSetComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Drawing_TriangleSetComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
