// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Sculpting/MeshPinchBrushOps.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshPinchBrushOps() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPinchBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPinchBrushOpProps();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSculptBrushOpProps();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
// End Cross Module References
	void UPinchBrushOpProps::StaticRegisterNativesUPinchBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UPinchBrushOpProps_NoRegister()
	{
		return UPinchBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UPinchBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPerpDamping_MetaData[];
#endif
		static void NewProp_bPerpDamping_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPerpDamping;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPinchBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSculptBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPinchBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshPinchBrushOps.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPinchBrushOps.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Strength_MetaData[] = {
		{ "Category", "PinchBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of the Brush */" },
		{ "DisplayName", "Strength" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPinchBrushOps.h" },
		{ "ToolTip", "Strength of the Brush" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Strength = { "Strength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPinchBrushOpProps, Strength), METADATA_PARAMS(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Strength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "PinchBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPinchBrushOps.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPinchBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "PinchBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "-1.0" },
		{ "Comment", "/** Depth of Brush into surface along surface normal */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPinchBrushOps.h" },
		{ "ToolTip", "Depth of Brush into surface along surface normal" },
		{ "UIMax", "0.5" },
		{ "UIMin", "-0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPinchBrushOpProps, Depth), METADATA_PARAMS(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Depth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_bPerpDamping_MetaData[] = {
		{ "Category", "PinchBrush" },
		{ "Comment", "/** When enabled, brush will damp motion of vertices that would move perpendicular to brush stroke direction */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPinchBrushOps.h" },
		{ "ToolTip", "When enabled, brush will damp motion of vertices that would move perpendicular to brush stroke direction" },
	};
#endif
	void Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_bPerpDamping_SetBit(void* Obj)
	{
		((UPinchBrushOpProps*)Obj)->bPerpDamping = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_bPerpDamping = { "bPerpDamping", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPinchBrushOpProps), &Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_bPerpDamping_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_bPerpDamping_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_bPerpDamping_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPinchBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Falloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_Depth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPinchBrushOpProps_Statics::NewProp_bPerpDamping,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPinchBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPinchBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPinchBrushOpProps_Statics::ClassParams = {
		&UPinchBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPinchBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPinchBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPinchBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPinchBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPinchBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPinchBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPinchBrushOpProps, 2046637074);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPinchBrushOpProps>()
	{
		return UPinchBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPinchBrushOpProps(Z_Construct_UClass_UPinchBrushOpProps, &UPinchBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPinchBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPinchBrushOpProps);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
