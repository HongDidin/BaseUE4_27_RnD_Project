// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/VoxelMorphologyMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVoxelMorphologyMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_EMorphologyOperation();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelMorphologyMeshesTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelMorphologyMeshesTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseVoxelTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder();
// End Cross Module References
	void UVoxelMorphologyMeshesToolProperties::StaticRegisterNativesUVoxelMorphologyMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_NoRegister()
	{
		return UVoxelMorphologyMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Operation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Operation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Operation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Distance_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_Distance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSolidifyInput_MetaData[];
#endif
		static void NewProp_bSolidifyInput_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSolidifyInput;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveInternalsAfterSolidify_MetaData[];
#endif
		static void NewProp_bRemoveInternalsAfterSolidify_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveInternalsAfterSolidify;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetSolidifySurface_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_OffsetSolidifySurface;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Properties of the morphology tool\n */" },
		{ "IncludePath", "VoxelMorphologyMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
		{ "ToolTip", "Properties of the morphology tool" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Operation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Operation_MetaData[] = {
		{ "Category", "Morphology" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Operation = { "Operation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelMorphologyMeshesToolProperties, Operation), Z_Construct_UEnum_ModelingOperators_EMorphologyOperation, METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Operation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Operation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Distance_MetaData[] = {
		{ "Category", "Morphology" },
		{ "ClampMax", "1000" },
		{ "ClampMin", ".001" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
		{ "UIMax", "100" },
		{ "UIMin", ".1" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Distance = { "Distance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelMorphologyMeshesToolProperties, Distance), METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Distance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Distance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bSolidifyInput_MetaData[] = {
		{ "Category", "Solidify" },
		{ "Comment", "/** Solidify the input mesh(es) before processing, fixing results for inputs with holes and/or self-intersections */" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
		{ "ToolTip", "Solidify the input mesh(es) before processing, fixing results for inputs with holes and/or self-intersections" },
	};
#endif
	void Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bSolidifyInput_SetBit(void* Obj)
	{
		((UVoxelMorphologyMeshesToolProperties*)Obj)->bSolidifyInput = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bSolidifyInput = { "bSolidifyInput", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVoxelMorphologyMeshesToolProperties), &Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bSolidifyInput_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bSolidifyInput_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bSolidifyInput_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_MetaData[] = {
		{ "Category", "Solidify" },
		{ "Comment", "/** Remove internal surfaces from the solidified input, before running morphology */" },
		{ "EditCondition", "bSolidifyInput == true" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
		{ "ToolTip", "Remove internal surfaces from the solidified input, before running morphology" },
	};
#endif
	void Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_SetBit(void* Obj)
	{
		((UVoxelMorphologyMeshesToolProperties*)Obj)->bRemoveInternalsAfterSolidify = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify = { "bRemoveInternalsAfterSolidify", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVoxelMorphologyMeshesToolProperties), &Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface_MetaData[] = {
		{ "Category", "Solidify" },
		{ "Comment", "/** Offset surface to create when solidifying any open-boundary inputs; if 0 then no offset surfaces are created */" },
		{ "EditCondition", "bSolidifyInput == true" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
		{ "ToolTip", "Offset surface to create when solidifying any open-boundary inputs; if 0 then no offset surfaces are created" },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface = { "OffsetSolidifySurface", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelMorphologyMeshesToolProperties, OffsetSolidifySurface), METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Operation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Operation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_Distance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bSolidifyInput,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_bRemoveInternalsAfterSolidify,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::NewProp_OffsetSolidifySurface,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelMorphologyMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::ClassParams = {
		&UVoxelMorphologyMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelMorphologyMeshesToolProperties, 3628388160);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelMorphologyMeshesToolProperties>()
	{
		return UVoxelMorphologyMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelMorphologyMeshesToolProperties(Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties, &UVoxelMorphologyMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelMorphologyMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelMorphologyMeshesToolProperties);
	void UVoxelMorphologyMeshesTool::StaticRegisterNativesUVoxelMorphologyMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UVoxelMorphologyMeshesTool_NoRegister()
	{
		return UVoxelMorphologyMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MorphologyProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MorphologyProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseVoxelTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Morphology tool -- dilate, contract, close, open operations on the input shape\n */" },
		{ "IncludePath", "VoxelMorphologyMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
		{ "ToolTip", "Morphology tool -- dilate, contract, close, open operations on the input shape" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::NewProp_MorphologyProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::NewProp_MorphologyProperties = { "MorphologyProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelMorphologyMeshesTool, MorphologyProperties), Z_Construct_UClass_UVoxelMorphologyMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::NewProp_MorphologyProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::NewProp_MorphologyProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::NewProp_MorphologyProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelMorphologyMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::ClassParams = {
		&UVoxelMorphologyMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelMorphologyMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelMorphologyMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelMorphologyMeshesTool, 1568031385);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelMorphologyMeshesTool>()
	{
		return UVoxelMorphologyMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelMorphologyMeshesTool(Z_Construct_UClass_UVoxelMorphologyMeshesTool, &UVoxelMorphologyMeshesTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelMorphologyMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelMorphologyMeshesTool);
	void UVoxelMorphologyMeshesToolBuilder::StaticRegisterNativesUVoxelMorphologyMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_NoRegister()
	{
		return UVoxelMorphologyMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseCreateFromSelectedToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VoxelMorphologyMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelMorphologyMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelMorphologyMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics::ClassParams = {
		&UVoxelMorphologyMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelMorphologyMeshesToolBuilder, 3127772050);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UVoxelMorphologyMeshesToolBuilder>()
	{
		return UVoxelMorphologyMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelMorphologyMeshesToolBuilder(Z_Construct_UClass_UVoxelMorphologyMeshesToolBuilder, &UVoxelMorphologyMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UVoxelMorphologyMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelMorphologyMeshesToolBuilder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
