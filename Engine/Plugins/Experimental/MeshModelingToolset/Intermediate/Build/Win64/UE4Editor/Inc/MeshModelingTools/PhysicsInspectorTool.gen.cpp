// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Physics/PhysicsInspectorTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePhysicsInspectorTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsInspectorToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsInspectorToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsInspectorTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsInspectorTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
// End Cross Module References
	void UPhysicsInspectorToolBuilder::StaticRegisterNativesUPhysicsInspectorToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UPhysicsInspectorToolBuilder_NoRegister()
	{
		return UPhysicsInspectorToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Physics/PhysicsInspectorTool.h" },
		{ "ModuleRelativePath", "Public/Physics/PhysicsInspectorTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhysicsInspectorToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics::ClassParams = {
		&UPhysicsInspectorToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhysicsInspectorToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhysicsInspectorToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhysicsInspectorToolBuilder, 2146618210);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPhysicsInspectorToolBuilder>()
	{
		return UPhysicsInspectorToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhysicsInspectorToolBuilder(Z_Construct_UClass_UPhysicsInspectorToolBuilder, &UPhysicsInspectorToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPhysicsInspectorToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhysicsInspectorToolBuilder);
	void UPhysicsInspectorTool::StaticRegisterNativesUPhysicsInspectorTool()
	{
	}
	UClass* Z_Construct_UClass_UPhysicsInspectorTool_NoRegister()
	{
		return UPhysicsInspectorTool::StaticClass();
	}
	struct Z_Construct_UClass_UPhysicsInspectorTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VizSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VizSettings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjectData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LineMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LineMaterial;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewElements_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewElements_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PreviewElements;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPhysicsInspectorTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsInspectorTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh Inspector Tool for visualizing mesh information\n */" },
		{ "IncludePath", "Physics/PhysicsInspectorTool.h" },
		{ "ModuleRelativePath", "Public/Physics/PhysicsInspectorTool.h" },
		{ "ToolTip", "Mesh Inspector Tool for visualizing mesh information" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_VizSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/PhysicsInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_VizSettings = { "VizSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsInspectorTool, VizSettings), Z_Construct_UClass_UCollisionGeometryVisualizationProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_VizSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_VizSettings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_ObjectData_Inner = { "ObjectData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UPhysicsObjectToolPropertySet_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_ObjectData_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/PhysicsInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_ObjectData = { "ObjectData", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsInspectorTool, ObjectData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_ObjectData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_ObjectData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_LineMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/PhysicsInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_LineMaterial = { "LineMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsInspectorTool, LineMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_LineMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_LineMaterial_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_PreviewElements_Inner = { "PreviewElements", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UPreviewGeometry_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_PreviewElements_MetaData[] = {
		{ "ModuleRelativePath", "Public/Physics/PhysicsInspectorTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_PreviewElements = { "PreviewElements", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPhysicsInspectorTool, PreviewElements), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_PreviewElements_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_PreviewElements_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPhysicsInspectorTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_VizSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_ObjectData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_ObjectData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_LineMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_PreviewElements_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPhysicsInspectorTool_Statics::NewProp_PreviewElements,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPhysicsInspectorTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPhysicsInspectorTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPhysicsInspectorTool_Statics::ClassParams = {
		&UPhysicsInspectorTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPhysicsInspectorTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsInspectorTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPhysicsInspectorTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPhysicsInspectorTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPhysicsInspectorTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPhysicsInspectorTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPhysicsInspectorTool, 4174537237);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPhysicsInspectorTool>()
	{
		return UPhysicsInspectorTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPhysicsInspectorTool(Z_Construct_UClass_UPhysicsInspectorTool, &UPhysicsInspectorTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPhysicsInspectorTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPhysicsInspectorTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
