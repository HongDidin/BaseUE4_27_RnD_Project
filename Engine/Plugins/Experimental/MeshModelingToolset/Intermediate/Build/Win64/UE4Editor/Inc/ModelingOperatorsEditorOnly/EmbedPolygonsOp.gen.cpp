// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperatorsEditorOnly/Public/CuttingOps/EmbedPolygonsOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEmbedPolygonsOp() {}
// Cross Module References
	MODELINGOPERATORSEDITORONLY_API UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_EEmbeddedPolygonOpMethod();
	UPackage* Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly();
// End Cross Module References
	static UEnum* EEmbeddedPolygonOpMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperatorsEditorOnly_EEmbeddedPolygonOpMethod, Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly(), TEXT("EEmbeddedPolygonOpMethod"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORSEDITORONLY_API UEnum* StaticEnum<EEmbeddedPolygonOpMethod>()
	{
		return EEmbeddedPolygonOpMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEmbeddedPolygonOpMethod(EEmbeddedPolygonOpMethod_StaticEnum, TEXT("/Script/ModelingOperatorsEditorOnly"), TEXT("EEmbeddedPolygonOpMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_EEmbeddedPolygonOpMethod_Hash() { return 366013762U; }
	UEnum* Z_Construct_UEnum_ModelingOperatorsEditorOnly_EEmbeddedPolygonOpMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEmbeddedPolygonOpMethod"), 0, Get_Z_Construct_UEnum_ModelingOperatorsEditorOnly_EEmbeddedPolygonOpMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEmbeddedPolygonOpMethod::TrimOutside", (int64)EEmbeddedPolygonOpMethod::TrimOutside },
				{ "EEmbeddedPolygonOpMethod::TrimInside", (int64)EEmbeddedPolygonOpMethod::TrimInside },
				{ "EEmbeddedPolygonOpMethod::InsertPolygon", (int64)EEmbeddedPolygonOpMethod::InsertPolygon },
				{ "EEmbeddedPolygonOpMethod::CutThrough", (int64)EEmbeddedPolygonOpMethod::CutThrough },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "CutThrough.Name", "EEmbeddedPolygonOpMethod::CutThrough" },
				{ "InsertPolygon.Name", "EEmbeddedPolygonOpMethod::InsertPolygon" },
				{ "ModuleRelativePath", "Public/CuttingOps/EmbedPolygonsOp.h" },
				{ "TrimInside.Name", "EEmbeddedPolygonOpMethod::TrimInside" },
				{ "TrimOutside.Name", "EEmbeddedPolygonOpMethod::TrimOutside" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperatorsEditorOnly,
				nullptr,
				"EEmbeddedPolygonOpMethod",
				"EEmbeddedPolygonOpMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
