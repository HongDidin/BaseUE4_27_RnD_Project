// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingOperators/Public/CleaningOps/EditNormalsOp.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEditNormalsOp() {}
// Cross Module References
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ESplitNormalMethod();
	UPackage* Z_Construct_UPackage__Script_ModelingOperators();
	MODELINGOPERATORS_API UEnum* Z_Construct_UEnum_ModelingOperators_ENormalCalculationMethod();
// End Cross Module References
	static UEnum* ESplitNormalMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperators_ESplitNormalMethod, Z_Construct_UPackage__Script_ModelingOperators(), TEXT("ESplitNormalMethod"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORS_API UEnum* StaticEnum<ESplitNormalMethod>()
	{
		return ESplitNormalMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESplitNormalMethod(ESplitNormalMethod_StaticEnum, TEXT("/Script/ModelingOperators"), TEXT("ESplitNormalMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperators_ESplitNormalMethod_Hash() { return 1058176917U; }
	UEnum* Z_Construct_UEnum_ModelingOperators_ESplitNormalMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperators();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESplitNormalMethod"), 0, Get_Z_Construct_UEnum_ModelingOperators_ESplitNormalMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESplitNormalMethod::UseExistingTopology", (int64)ESplitNormalMethod::UseExistingTopology },
				{ "ESplitNormalMethod::FaceNormalThreshold", (int64)ESplitNormalMethod::FaceNormalThreshold },
				{ "ESplitNormalMethod::FaceGroupID", (int64)ESplitNormalMethod::FaceGroupID },
				{ "ESplitNormalMethod::PerTriangle", (int64)ESplitNormalMethod::PerTriangle },
				{ "ESplitNormalMethod::PerVertex", (int64)ESplitNormalMethod::PerVertex },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "FaceGroupID.Comment", "/** Recompute split-normals by grouping faces around each vertex that share a face/polygroup */" },
				{ "FaceGroupID.Name", "ESplitNormalMethod::FaceGroupID" },
				{ "FaceGroupID.ToolTip", "Recompute split-normals by grouping faces around each vertex that share a face/polygroup" },
				{ "FaceNormalThreshold.Comment", "/** Recompute split-normals by grouping faces around each vertex based on an angle threshold */" },
				{ "FaceNormalThreshold.Name", "ESplitNormalMethod::FaceNormalThreshold" },
				{ "FaceNormalThreshold.ToolTip", "Recompute split-normals by grouping faces around each vertex based on an angle threshold" },
				{ "ModuleRelativePath", "Public/CleaningOps/EditNormalsOp.h" },
				{ "PerTriangle.Comment", "/** Set each triangle-vertex to have the face normal of that triangle's plane */" },
				{ "PerTriangle.Name", "ESplitNormalMethod::PerTriangle" },
				{ "PerTriangle.ToolTip", "Set each triangle-vertex to have the face normal of that triangle's plane" },
				{ "PerVertex.Comment", "/** Set each vertex to have a fully shared normal, ie no split normals  */" },
				{ "PerVertex.Name", "ESplitNormalMethod::PerVertex" },
				{ "PerVertex.ToolTip", "Set each vertex to have a fully shared normal, ie no split normals" },
				{ "UseExistingTopology.Comment", "/** Keep the existing split-normals structure on the mesh */" },
				{ "UseExistingTopology.Name", "ESplitNormalMethod::UseExistingTopology" },
				{ "UseExistingTopology.ToolTip", "Keep the existing split-normals structure on the mesh" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperators,
				nullptr,
				"ESplitNormalMethod",
				"ESplitNormalMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ENormalCalculationMethod_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_ModelingOperators_ENormalCalculationMethod, Z_Construct_UPackage__Script_ModelingOperators(), TEXT("ENormalCalculationMethod"));
		}
		return Singleton;
	}
	template<> MODELINGOPERATORS_API UEnum* StaticEnum<ENormalCalculationMethod>()
	{
		return ENormalCalculationMethod_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ENormalCalculationMethod(ENormalCalculationMethod_StaticEnum, TEXT("/Script/ModelingOperators"), TEXT("ENormalCalculationMethod"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_ModelingOperators_ENormalCalculationMethod_Hash() { return 4050402453U; }
	UEnum* Z_Construct_UEnum_ModelingOperators_ENormalCalculationMethod()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_ModelingOperators();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ENormalCalculationMethod"), 0, Get_Z_Construct_UEnum_ModelingOperators_ENormalCalculationMethod_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ENormalCalculationMethod::AreaWeighted", (int64)ENormalCalculationMethod::AreaWeighted },
				{ "ENormalCalculationMethod::AngleWeighted", (int64)ENormalCalculationMethod::AngleWeighted },
				{ "ENormalCalculationMethod::AreaAngleWeighting", (int64)ENormalCalculationMethod::AreaAngleWeighting },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AngleWeighted.Name", "ENormalCalculationMethod::AngleWeighted" },
				{ "AreaAngleWeighting.Name", "ENormalCalculationMethod::AreaAngleWeighting" },
				{ "AreaWeighted.Name", "ENormalCalculationMethod::AreaWeighted" },
				{ "ModuleRelativePath", "Public/CleaningOps/EditNormalsOp.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_ModelingOperators,
				nullptr,
				"ENormalCalculationMethod",
				"ENormalCalculationMethod",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
