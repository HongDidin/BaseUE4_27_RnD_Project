// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_MeshStatisticsProperties_generated_h
#error "MeshStatisticsProperties.generated.h already included, missing '#pragma once' in MeshStatisticsProperties.h"
#endif
#define MESHMODELINGTOOLS_MeshStatisticsProperties_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshStatisticsProperties(); \
	friend struct Z_Construct_UClass_UMeshStatisticsProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshStatisticsProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshStatisticsProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUMeshStatisticsProperties(); \
	friend struct Z_Construct_UClass_UMeshStatisticsProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshStatisticsProperties, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(UMeshStatisticsProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshStatisticsProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshStatisticsProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshStatisticsProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshStatisticsProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshStatisticsProperties(UMeshStatisticsProperties&&); \
	NO_API UMeshStatisticsProperties(const UMeshStatisticsProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshStatisticsProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshStatisticsProperties(UMeshStatisticsProperties&&); \
	NO_API UMeshStatisticsProperties(const UMeshStatisticsProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshStatisticsProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshStatisticsProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshStatisticsProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_17_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class UMeshStatisticsProperties>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_Properties_MeshStatisticsProperties_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
