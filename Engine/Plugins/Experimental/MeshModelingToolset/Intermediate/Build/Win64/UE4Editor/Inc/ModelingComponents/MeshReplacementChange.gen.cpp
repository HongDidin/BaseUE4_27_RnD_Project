// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Changes/MeshReplacementChange.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshReplacementChange() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshReplacementCommandChangeTarget_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshReplacementCommandChangeTarget();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
// End Cross Module References
	void UMeshReplacementCommandChangeTarget::StaticRegisterNativesUMeshReplacementCommandChangeTarget()
	{
	}
	UClass* Z_Construct_UClass_UMeshReplacementCommandChangeTarget_NoRegister()
	{
		return UMeshReplacementCommandChangeTarget::StaticClass();
	}
	struct Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Changes/MeshReplacementChange.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IMeshReplacementCommandChangeTarget>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics::ClassParams = {
		&UMeshReplacementCommandChangeTarget::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshReplacementCommandChangeTarget()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshReplacementCommandChangeTarget_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshReplacementCommandChangeTarget, 1158375532);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UMeshReplacementCommandChangeTarget>()
	{
		return UMeshReplacementCommandChangeTarget::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshReplacementCommandChangeTarget(Z_Construct_UClass_UMeshReplacementCommandChangeTarget, &UMeshReplacementCommandChangeTarget::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UMeshReplacementCommandChangeTarget"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshReplacementCommandChangeTarget);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
