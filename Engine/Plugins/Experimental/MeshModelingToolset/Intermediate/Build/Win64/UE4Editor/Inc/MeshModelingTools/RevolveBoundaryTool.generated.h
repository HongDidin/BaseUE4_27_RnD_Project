// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHMODELINGTOOLS_RevolveBoundaryTool_generated_h
#error "RevolveBoundaryTool.generated.h already included, missing '#pragma once' in RevolveBoundaryTool.h"
#endif
#define MESHMODELINGTOOLS_RevolveBoundaryTool_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURevolveBoundaryToolBuilder(); \
	friend struct Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_INCLASS \
private: \
	static void StaticRegisterNativesURevolveBoundaryToolBuilder(); \
	friend struct Z_Construct_UClass_URevolveBoundaryToolBuilder_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveBoundaryToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryToolBuilder(URevolveBoundaryToolBuilder&&); \
	NO_API URevolveBoundaryToolBuilder(const URevolveBoundaryToolBuilder&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryToolBuilder(URevolveBoundaryToolBuilder&&); \
	NO_API URevolveBoundaryToolBuilder(const URevolveBoundaryToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveBoundaryToolBuilder)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_22_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URevolveBoundaryToolBuilder>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURevolveBoundaryOperatorFactory(); \
	friend struct Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_INCLASS \
private: \
	static void StaticRegisterNativesURevolveBoundaryOperatorFactory(); \
	friend struct Z_Construct_UClass_URevolveBoundaryOperatorFactory_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryOperatorFactory, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveBoundaryOperatorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryOperatorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryOperatorFactory(URevolveBoundaryOperatorFactory&&); \
	NO_API URevolveBoundaryOperatorFactory(const URevolveBoundaryOperatorFactory&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryOperatorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryOperatorFactory(URevolveBoundaryOperatorFactory&&); \
	NO_API URevolveBoundaryOperatorFactory(const URevolveBoundaryOperatorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryOperatorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryOperatorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveBoundaryOperatorFactory)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_34_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_37_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URevolveBoundaryOperatorFactory>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURevolveBoundaryToolProperties(); \
	friend struct Z_Construct_UClass_URevolveBoundaryToolProperties_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryToolProperties, URevolveProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_INCLASS \
private: \
	static void StaticRegisterNativesURevolveBoundaryToolProperties(); \
	friend struct Z_Construct_UClass_URevolveBoundaryToolProperties_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryToolProperties, URevolveProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveBoundaryToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryToolProperties(URevolveBoundaryToolProperties&&); \
	NO_API URevolveBoundaryToolProperties(const URevolveBoundaryToolProperties&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryToolProperties(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryToolProperties(URevolveBoundaryToolProperties&&); \
	NO_API URevolveBoundaryToolProperties(const URevolveBoundaryToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryToolProperties); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveBoundaryToolProperties)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_47_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_50_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URevolveBoundaryToolProperties>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesURevolveBoundaryTool(); \
	friend struct Z_Construct_UClass_URevolveBoundaryTool_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryTool, UMeshBoundaryToolBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_INCLASS \
private: \
	static void StaticRegisterNativesURevolveBoundaryTool(); \
	friend struct Z_Construct_UClass_URevolveBoundaryTool_Statics; \
public: \
	DECLARE_CLASS(URevolveBoundaryTool, UMeshBoundaryToolBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshModelingTools"), NO_API) \
	DECLARE_SERIALIZER(URevolveBoundaryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryTool(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(URevolveBoundaryTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryTool(URevolveBoundaryTool&&); \
	NO_API URevolveBoundaryTool(const URevolveBoundaryTool&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API URevolveBoundaryTool() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API URevolveBoundaryTool(URevolveBoundaryTool&&); \
	NO_API URevolveBoundaryTool(const URevolveBoundaryTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, URevolveBoundaryTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(URevolveBoundaryTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(URevolveBoundaryTool)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Settings() { return STRUCT_OFFSET(URevolveBoundaryTool, Settings); } \
	FORCEINLINE static uint32 __PPO__MaterialProperties() { return STRUCT_OFFSET(URevolveBoundaryTool, MaterialProperties); } \
	FORCEINLINE static uint32 __PPO__PlaneMechanic() { return STRUCT_OFFSET(URevolveBoundaryTool, PlaneMechanic); } \
	FORCEINLINE static uint32 __PPO__Preview() { return STRUCT_OFFSET(URevolveBoundaryTool, Preview); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_79_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h_82_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHMODELINGTOOLS_API UClass* StaticClass<class URevolveBoundaryTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_MeshModelingTools_Public_RevolveBoundaryTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
