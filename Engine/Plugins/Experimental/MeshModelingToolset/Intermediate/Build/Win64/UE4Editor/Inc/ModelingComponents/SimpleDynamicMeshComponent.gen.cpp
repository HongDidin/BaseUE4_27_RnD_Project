// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/SimpleDynamicMeshComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSimpleDynamicMeshComponent() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_USimpleDynamicMeshComponent();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UBaseDynamicMeshComponent();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UEnum* Z_Construct_UEnum_ModelingComponents_EDynamicMeshTangentCalcType();
// End Cross Module References
	void USimpleDynamicMeshComponent::StaticRegisterNativesUSimpleDynamicMeshComponent()
	{
	}
	UClass* Z_Construct_UClass_USimpleDynamicMeshComponent_NoRegister()
	{
		return USimpleDynamicMeshComponent::StaticClass();
	}
	struct Z_Construct_UClass_USimpleDynamicMeshComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TangentsType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TangentsType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TangentsType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInvalidateProxyOnChange_MetaData[];
#endif
		static void NewProp_bInvalidateProxyOnChange_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInvalidateProxyOnChange;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExplicitShowWireframe_MetaData[];
#endif
		static void NewProp_bExplicitShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExplicitShowWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDrawOnTop_MetaData[];
#endif
		static void NewProp_bDrawOnTop_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDrawOnTop;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseDynamicMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "Rendering" },
		{ "Comment", "/** \n * USimpleDynamicMeshComponent is a mesh component similar to UProceduralMeshComponent,\n * except it bases the renderable geometry off an internal FDynamicMesh3 instance.\n * \n * There is some support for undo/redo on the component (@todo is this the right place?)\n * \n * This component draws wireframe-on-shaded when Wireframe is enabled, or when bExplicitShowWireframe = true\n *\n */" },
		{ "HideCategories", "LOD Physics Collision LOD Physics Collision Mobility Trigger" },
		{ "IncludePath", "SimpleDynamicMeshComponent.h" },
		{ "ModuleRelativePath", "Public/SimpleDynamicMeshComponent.h" },
		{ "ToolTip", "USimpleDynamicMeshComponent is a mesh component similar to UProceduralMeshComponent,\nexcept it bases the renderable geometry off an internal FDynamicMesh3 instance.\n\nThere is some support for undo/redo on the component (@todo is this the right place?)\n\nThis component draws wireframe-on-shaded when Wireframe is enabled, or when bExplicitShowWireframe = true" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_TangentsType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_TangentsType_MetaData[] = {
		{ "Comment", "/** How should Tangents be calculated/handled */" },
		{ "ModuleRelativePath", "Public/SimpleDynamicMeshComponent.h" },
		{ "ToolTip", "How should Tangents be calculated/handled" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_TangentsType = { "TangentsType", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USimpleDynamicMeshComponent, TangentsType), Z_Construct_UEnum_ModelingComponents_EDynamicMeshTangentCalcType, METADATA_PARAMS(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_TangentsType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_TangentsType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bInvalidateProxyOnChange_MetaData[] = {
		{ "Comment", "/** If false, we don't completely invalidate the RenderProxy when ApplyChange() is called (assumption is it will be handled elsewhere) */" },
		{ "ModuleRelativePath", "Public/SimpleDynamicMeshComponent.h" },
		{ "ToolTip", "If false, we don't completely invalidate the RenderProxy when ApplyChange() is called (assumption is it will be handled elsewhere)" },
	};
#endif
	void Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bInvalidateProxyOnChange_SetBit(void* Obj)
	{
		((USimpleDynamicMeshComponent*)Obj)->bInvalidateProxyOnChange = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bInvalidateProxyOnChange = { "bInvalidateProxyOnChange", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USimpleDynamicMeshComponent), &Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bInvalidateProxyOnChange_SetBit, METADATA_PARAMS(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bInvalidateProxyOnChange_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bInvalidateProxyOnChange_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_MetaData[] = {
		{ "Comment", "/**\n\x09 * if true, we always show the wireframe on top of the shaded mesh, even when not in wireframe mode\n\x09 */" },
		{ "ModuleRelativePath", "Public/SimpleDynamicMeshComponent.h" },
		{ "ToolTip", "if true, we always show the wireframe on top of the shaded mesh, even when not in wireframe mode" },
	};
#endif
	void Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_SetBit(void* Obj)
	{
		((USimpleDynamicMeshComponent*)Obj)->bExplicitShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe = { "bExplicitShowWireframe", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USimpleDynamicMeshComponent), &Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bDrawOnTop_MetaData[] = {
		{ "Comment", "// do not use this\n" },
		{ "ModuleRelativePath", "Public/SimpleDynamicMeshComponent.h" },
		{ "ToolTip", "do not use this" },
	};
#endif
	void Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bDrawOnTop_SetBit(void* Obj)
	{
		((USimpleDynamicMeshComponent*)Obj)->bDrawOnTop = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bDrawOnTop = { "bDrawOnTop", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USimpleDynamicMeshComponent), &Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bDrawOnTop_SetBit, METADATA_PARAMS(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bDrawOnTop_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bDrawOnTop_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_TangentsType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_TangentsType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bInvalidateProxyOnChange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bExplicitShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::NewProp_bDrawOnTop,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USimpleDynamicMeshComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::ClassParams = {
		&USimpleDynamicMeshComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::PropPointers),
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USimpleDynamicMeshComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USimpleDynamicMeshComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USimpleDynamicMeshComponent, 3625719476);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<USimpleDynamicMeshComponent>()
	{
		return USimpleDynamicMeshComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USimpleDynamicMeshComponent(Z_Construct_UClass_USimpleDynamicMeshComponent, &USimpleDynamicMeshComponent::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("USimpleDynamicMeshComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USimpleDynamicMeshComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
