// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/VoxelCSGMeshesTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVoxelCSGMeshesTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVoxelCSGOperation();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVoxelCSGMeshesToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVoxelCSGMeshesToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVoxelCSGMeshesToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVoxelCSGMeshesTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVoxelCSGMeshesTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshStatisticsProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	static UEnum* EVoxelCSGOperation_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVoxelCSGOperation, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EVoxelCSGOperation"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EVoxelCSGOperation>()
	{
		return EVoxelCSGOperation_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVoxelCSGOperation(EVoxelCSGOperation_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EVoxelCSGOperation"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVoxelCSGOperation_Hash() { return 1171730390U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVoxelCSGOperation()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVoxelCSGOperation"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVoxelCSGOperation_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVoxelCSGOperation::DifferenceAB", (int64)EVoxelCSGOperation::DifferenceAB },
				{ "EVoxelCSGOperation::DifferenceBA", (int64)EVoxelCSGOperation::DifferenceBA },
				{ "EVoxelCSGOperation::Intersect", (int64)EVoxelCSGOperation::Intersect },
				{ "EVoxelCSGOperation::Union", (int64)EVoxelCSGOperation::Union },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/**  */" },
				{ "DifferenceAB.Comment", "/** Subtracts the first object from the second */" },
				{ "DifferenceAB.DisplayName", "A - B" },
				{ "DifferenceAB.Name", "EVoxelCSGOperation::DifferenceAB" },
				{ "DifferenceAB.ToolTip", "Subtracts the first object from the second" },
				{ "DifferenceBA.Comment", "/** Subtracts the second object from the first */" },
				{ "DifferenceBA.DisplayName", "B - A" },
				{ "DifferenceBA.Name", "EVoxelCSGOperation::DifferenceBA" },
				{ "DifferenceBA.ToolTip", "Subtracts the second object from the first" },
				{ "Intersect.Comment", "/** intersection of two objects */" },
				{ "Intersect.DisplayName", "Intersect" },
				{ "Intersect.Name", "EVoxelCSGOperation::Intersect" },
				{ "Intersect.ToolTip", "intersection of two objects" },
				{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
				{ "Union.Comment", "/** union of two objects */" },
				{ "Union.DisplayName", "Union" },
				{ "Union.Name", "EVoxelCSGOperation::Union" },
				{ "Union.ToolTip", "union of two objects" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EVoxelCSGOperation",
				"EVoxelCSGOperation",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UVoxelCSGMeshesToolBuilder::StaticRegisterNativesUVoxelCSGMeshesToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_NoRegister()
	{
		return UVoxelCSGMeshesToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "VoxelCSGMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelCSGMeshesToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics::ClassParams = {
		&UVoxelCSGMeshesToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelCSGMeshesToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelCSGMeshesToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelCSGMeshesToolBuilder, 3273898946);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UVoxelCSGMeshesToolBuilder>()
	{
		return UVoxelCSGMeshesToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelCSGMeshesToolBuilder(Z_Construct_UClass_UVoxelCSGMeshesToolBuilder, &UVoxelCSGMeshesToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UVoxelCSGMeshesToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelCSGMeshesToolBuilder);
	void UVoxelCSGMeshesToolProperties::StaticRegisterNativesUVoxelCSGMeshesToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UVoxelCSGMeshesToolProperties_NoRegister()
	{
		return UVoxelCSGMeshesToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Operation_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Operation_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Operation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VoxelCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_VoxelCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshAdaptivity_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_MeshAdaptivity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OffsetDistance_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OffsetDistance;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoSimplify_MetaData[];
#endif
		static void NewProp_bAutoSimplify_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoSimplify;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties of the Voxel CSG operation\n */" },
		{ "IncludePath", "VoxelCSGMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
		{ "ToolTip", "Standard properties of the Voxel CSG operation" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_Operation_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_Operation_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** The type of operation  */" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
		{ "ToolTip", "The type of operation" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_Operation = { "Operation", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesToolProperties, Operation), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVoxelCSGOperation, METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_Operation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_Operation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_VoxelCount_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1024" },
		{ "ClampMin", "8" },
		{ "Comment", "/** The size of the geometry bounding box major axis measured in voxels.*/" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
		{ "ToolTip", "The size of the geometry bounding box major axis measured in voxels." },
		{ "UIMax", "1024" },
		{ "UIMin", "8" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_VoxelCount = { "VoxelCount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesToolProperties, VoxelCount), METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_VoxelCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_VoxelCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_MeshAdaptivity_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "1" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Remeshing adaptivity, prior to optional simplification */" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
		{ "ToolTip", "Remeshing adaptivity, prior to optional simplification" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_MeshAdaptivity = { "MeshAdaptivity", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesToolProperties, MeshAdaptivity), METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_MeshAdaptivity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_MeshAdaptivity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_OffsetDistance_MetaData[] = {
		{ "Category", "Options" },
		{ "ClampMax", "10" },
		{ "ClampMin", "-10" },
		{ "Comment", "/** Offset when remeshing, note large offsets with high voxels counts will be slow */" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
		{ "ToolTip", "Offset when remeshing, note large offsets with high voxels counts will be slow" },
		{ "UIMax", "10" },
		{ "UIMin", "-10" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_OffsetDistance = { "OffsetDistance", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesToolProperties, OffsetDistance), METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_OffsetDistance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_OffsetDistance_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_bAutoSimplify_MetaData[] = {
		{ "Category", "Options" },
		{ "Comment", "/** Automatically simplify the result of voxel-based merge.*/" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
		{ "ToolTip", "Automatically simplify the result of voxel-based merge." },
	};
#endif
	void Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_bAutoSimplify_SetBit(void* Obj)
	{
		((UVoxelCSGMeshesToolProperties*)Obj)->bAutoSimplify = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_bAutoSimplify = { "bAutoSimplify", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVoxelCSGMeshesToolProperties), &Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_bAutoSimplify_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_bAutoSimplify_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_bAutoSimplify_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_Operation_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_Operation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_VoxelCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_MeshAdaptivity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_OffsetDistance,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::NewProp_bAutoSimplify,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelCSGMeshesToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::ClassParams = {
		&UVoxelCSGMeshesToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelCSGMeshesToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelCSGMeshesToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelCSGMeshesToolProperties, 3461369652);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UVoxelCSGMeshesToolProperties>()
	{
		return UVoxelCSGMeshesToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelCSGMeshesToolProperties(Z_Construct_UClass_UVoxelCSGMeshesToolProperties, &UVoxelCSGMeshesToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UVoxelCSGMeshesToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelCSGMeshesToolProperties);
	void UVoxelCSGMeshesTool::StaticRegisterNativesUVoxelCSGMeshesTool()
	{
	}
	UClass* Z_Construct_UClass_UVoxelCSGMeshesTool_NoRegister()
	{
		return UVoxelCSGMeshesTool::StaticClass();
	}
	struct Z_Construct_UClass_UVoxelCSGMeshesTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CSGProps_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CSGProps;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshStatisticsProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshStatisticsProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HandleSourcesProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HandleSourcesProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "VoxelCSGMeshesTool.h" },
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_CSGProps_MetaData[] = {
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_CSGProps = { "CSGProps", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesTool, CSGProps), Z_Construct_UClass_UVoxelCSGMeshesToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_CSGProps_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_CSGProps_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_MeshStatisticsProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_MeshStatisticsProperties = { "MeshStatisticsProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesTool, MeshStatisticsProperties), Z_Construct_UClass_UMeshStatisticsProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_MeshStatisticsProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_MeshStatisticsProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_HandleSourcesProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_HandleSourcesProperties = { "HandleSourcesProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesTool, HandleSourcesProperties), Z_Construct_UClass_UOnAcceptHandleSourcesProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_HandleSourcesProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_HandleSourcesProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/VoxelCSGMeshesTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVoxelCSGMeshesTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_CSGProps,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_MeshStatisticsProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_HandleSourcesProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::NewProp_Preview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVoxelCSGMeshesTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::ClassParams = {
		&UVoxelCSGMeshesTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVoxelCSGMeshesTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVoxelCSGMeshesTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVoxelCSGMeshesTool, 2338136214);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UVoxelCSGMeshesTool>()
	{
		return UVoxelCSGMeshesTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVoxelCSGMeshesTool(Z_Construct_UClass_UVoxelCSGMeshesTool, &UVoxelCSGMeshesTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UVoxelCSGMeshesTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVoxelCSGMeshesTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
