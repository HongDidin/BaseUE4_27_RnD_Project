// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/DynamicMeshBrushTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeDynamicMeshBrushTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshBrushTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshBrushTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBaseBrushTool();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
// End Cross Module References
	void UDynamicMeshBrushTool::StaticRegisterNativesUDynamicMeshBrushTool()
	{
	}
	UClass* Z_Construct_UClass_UDynamicMeshBrushTool_NoRegister()
	{
		return UDynamicMeshBrushTool::StaticClass();
	}
	struct Z_Construct_UClass_UDynamicMeshBrushTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDynamicMeshBrushTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseBrushTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshBrushTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UDynamicMeshBrushTool is a base class that specializes UBaseBrushTool\n * for brushing on an FDynamicMesh3. The input FPrimitiveComponentTarget is hidden\n * and a UPreviewMesh is created and shown in its place. This UPreviewMesh is\n * used for hit-testing and dynamic rendering.\n * \n */" },
		{ "IncludePath", "DynamicMeshBrushTool.h" },
		{ "ModuleRelativePath", "Public/DynamicMeshBrushTool.h" },
		{ "ToolTip", "UDynamicMeshBrushTool is a base class that specializes UBaseBrushTool\nfor brushing on an FDynamicMesh3. The input FPrimitiveComponentTarget is hidden\nand a UPreviewMesh is created and shown in its place. This UPreviewMesh is\nused for hit-testing and dynamic rendering." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDynamicMeshBrushTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/DynamicMeshBrushTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDynamicMeshBrushTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDynamicMeshBrushTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshBrushTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshBrushTool_Statics::NewProp_PreviewMesh_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDynamicMeshBrushTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDynamicMeshBrushTool_Statics::NewProp_PreviewMesh,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDynamicMeshBrushTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDynamicMeshBrushTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDynamicMeshBrushTool_Statics::ClassParams = {
		&UDynamicMeshBrushTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDynamicMeshBrushTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshBrushTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDynamicMeshBrushTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDynamicMeshBrushTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDynamicMeshBrushTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDynamicMeshBrushTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDynamicMeshBrushTool, 2980580060);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UDynamicMeshBrushTool>()
	{
		return UDynamicMeshBrushTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDynamicMeshBrushTool(Z_Construct_UClass_UDynamicMeshBrushTool, &UDynamicMeshBrushTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UDynamicMeshBrushTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDynamicMeshBrushTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
