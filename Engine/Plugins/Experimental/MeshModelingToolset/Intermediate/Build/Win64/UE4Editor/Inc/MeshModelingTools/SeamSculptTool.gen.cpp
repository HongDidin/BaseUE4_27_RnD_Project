// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/SeamSculptTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSeamSculptTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USeamSculptToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USeamSculptToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMeshSurfacePointToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USeamSculptToolProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USeamSculptToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USeamSculptTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_USeamSculptTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UDynamicMeshBrushTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewGeometry_NoRegister();
// End Cross Module References
	void USeamSculptToolBuilder::StaticRegisterNativesUSeamSculptToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_USeamSculptToolBuilder_NoRegister()
	{
		return USeamSculptToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_USeamSculptToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USeamSculptToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSurfacePointToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USeamSculptToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "SeamSculptTool.h" },
		{ "ModuleRelativePath", "Public/SeamSculptTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USeamSculptToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USeamSculptToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USeamSculptToolBuilder_Statics::ClassParams = {
		&USeamSculptToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USeamSculptToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USeamSculptToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USeamSculptToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USeamSculptToolBuilder, 1354800727);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USeamSculptToolBuilder>()
	{
		return USeamSculptToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USeamSculptToolBuilder(Z_Construct_UClass_USeamSculptToolBuilder, &USeamSculptToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USeamSculptToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USeamSculptToolBuilder);
	void USeamSculptToolProperties::StaticRegisterNativesUSeamSculptToolProperties()
	{
	}
	UClass* Z_Construct_UClass_USeamSculptToolProperties_NoRegister()
	{
		return USeamSculptToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_USeamSculptToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHitBackFaces_MetaData[];
#endif
		static void NewProp_bHitBackFaces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHitBackFaces;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USeamSculptToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USeamSculptToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SeamSculptTool.h" },
		{ "ModuleRelativePath", "Public/SeamSculptTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/SeamSculptTool.h" },
	};
#endif
	void Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((USeamSculptToolProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USeamSculptToolProperties), &Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bShowWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bHitBackFaces_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/SeamSculptTool.h" },
	};
#endif
	void Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bHitBackFaces_SetBit(void* Obj)
	{
		((USeamSculptToolProperties*)Obj)->bHitBackFaces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bHitBackFaces = { "bHitBackFaces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USeamSculptToolProperties), &Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bHitBackFaces_SetBit, METADATA_PARAMS(Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bHitBackFaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bHitBackFaces_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USeamSculptToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bShowWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USeamSculptToolProperties_Statics::NewProp_bHitBackFaces,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USeamSculptToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USeamSculptToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USeamSculptToolProperties_Statics::ClassParams = {
		&USeamSculptToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USeamSculptToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USeamSculptToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USeamSculptToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USeamSculptToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USeamSculptToolProperties, 1218720486);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USeamSculptToolProperties>()
	{
		return USeamSculptToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USeamSculptToolProperties(Z_Construct_UClass_USeamSculptToolProperties, &USeamSculptToolProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USeamSculptToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USeamSculptToolProperties);
	void USeamSculptTool::StaticRegisterNativesUSeamSculptTool()
	{
	}
	UClass* Z_Construct_UClass_USeamSculptTool_NoRegister()
	{
		return USeamSculptTool::StaticClass();
	}
	struct Z_Construct_UClass_USeamSculptTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewGeom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewGeom;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USeamSculptTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDynamicMeshBrushTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USeamSculptTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SeamSculptTool.h" },
		{ "ModuleRelativePath", "Public/SeamSculptTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USeamSculptTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/SeamSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USeamSculptTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USeamSculptTool, Settings), Z_Construct_UClass_USeamSculptToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USeamSculptTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USeamSculptTool_Statics::NewProp_PreviewGeom_MetaData[] = {
		{ "ModuleRelativePath", "Public/SeamSculptTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USeamSculptTool_Statics::NewProp_PreviewGeom = { "PreviewGeom", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USeamSculptTool, PreviewGeom), Z_Construct_UClass_UPreviewGeometry_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USeamSculptTool_Statics::NewProp_PreviewGeom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptTool_Statics::NewProp_PreviewGeom_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USeamSculptTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USeamSculptTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USeamSculptTool_Statics::NewProp_PreviewGeom,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USeamSculptTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USeamSculptTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USeamSculptTool_Statics::ClassParams = {
		&USeamSculptTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USeamSculptTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USeamSculptTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USeamSculptTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USeamSculptTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USeamSculptTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USeamSculptTool, 506246033);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<USeamSculptTool>()
	{
		return USeamSculptTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USeamSculptTool(Z_Construct_UClass_USeamSculptTool, &USeamSculptTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("USeamSculptTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USeamSculptTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
