// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/GroupEdgeInsertionTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGroupEdgeInsertionTool() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EGroupEdgeInsertionMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionTool_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UGroupEdgeInsertionTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleSelectionTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
// End Cross Module References
	static UEnum* EGroupEdgeInsertionMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EGroupEdgeInsertionMode, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EGroupEdgeInsertionMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EGroupEdgeInsertionMode>()
	{
		return EGroupEdgeInsertionMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroupEdgeInsertionMode(EGroupEdgeInsertionMode_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EGroupEdgeInsertionMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EGroupEdgeInsertionMode_Hash() { return 4136223618U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EGroupEdgeInsertionMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroupEdgeInsertionMode"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EGroupEdgeInsertionMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroupEdgeInsertionMode::Retriangulate", (int64)EGroupEdgeInsertionMode::Retriangulate },
				{ "EGroupEdgeInsertionMode::PlaneCut", (int64)EGroupEdgeInsertionMode::PlaneCut },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
				{ "PlaneCut.Comment", "/** Keeps existing triangles and cuts them to create a new path. May result in fragmented triangles over time.*/" },
				{ "PlaneCut.Name", "EGroupEdgeInsertionMode::PlaneCut" },
				{ "PlaneCut.ToolTip", "Keeps existing triangles and cuts them to create a new path. May result in fragmented triangles over time." },
				{ "Retriangulate.Comment", "/** Existing groups will be deleted and new triangles will be created for the new groups.\n\x09 Keeps topology simple but breaks non-planar groups and loses the UV's. */" },
				{ "Retriangulate.Name", "EGroupEdgeInsertionMode::Retriangulate" },
				{ "Retriangulate.ToolTip", "Existing groups will be deleted and new triangles will be created for the new groups.\n       Keeps topology simple but breaks non-planar groups and loses the UV's." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EGroupEdgeInsertionMode",
				"EGroupEdgeInsertionMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UGroupEdgeInsertionToolBuilder::StaticRegisterNativesUGroupEdgeInsertionToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_NoRegister()
	{
		return UGroupEdgeInsertionToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GroupEdgeInsertionTool.h" },
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroupEdgeInsertionToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics::ClassParams = {
		&UGroupEdgeInsertionToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroupEdgeInsertionToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroupEdgeInsertionToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroupEdgeInsertionToolBuilder, 37399239);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UGroupEdgeInsertionToolBuilder>()
	{
		return UGroupEdgeInsertionToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroupEdgeInsertionToolBuilder(Z_Construct_UClass_UGroupEdgeInsertionToolBuilder, &UGroupEdgeInsertionToolBuilder::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UGroupEdgeInsertionToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroupEdgeInsertionToolBuilder);
	void UGroupEdgeInsertionProperties::StaticRegisterNativesUGroupEdgeInsertionProperties()
	{
	}
	UClass* Z_Construct_UClass_UGroupEdgeInsertionProperties_NoRegister()
	{
		return UGroupEdgeInsertionProperties::StaticClass();
	}
	struct Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_InsertionMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InsertionMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InsertionMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWireframe_MetaData[];
#endif
		static void NewProp_bWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWireframe;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexTolerance_MetaData[];
#endif
		static const UE4CodeGen_Private::FDoublePropertyParams NewProp_VertexTolerance;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GroupEdgeInsertionTool.h" },
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_InsertionMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_InsertionMode_MetaData[] = {
		{ "Category", "InsertEdge" },
		{ "Comment", "/** Determines how group edges are added to the geometry */" },
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
		{ "ToolTip", "Determines how group edges are added to the geometry" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_InsertionMode = { "InsertionMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroupEdgeInsertionProperties, InsertionMode), Z_Construct_UEnum_MeshModelingTools_EGroupEdgeInsertionMode, METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_InsertionMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_InsertionMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_bWireframe_MetaData[] = {
		{ "Category", "InsertEdge" },
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
	};
#endif
	void Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_bWireframe_SetBit(void* Obj)
	{
		((UGroupEdgeInsertionProperties*)Obj)->bWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_bWireframe = { "bWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UGroupEdgeInsertionProperties), &Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_bWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_bWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_bWireframe_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_VertexTolerance_MetaData[] = {
		{ "Category", "InsertEdgeLoop" },
		{ "Comment", "/** How close a new loop edge needs to pass next to an existing vertex to use that vertex rather than creating a new one (used for plane cut). */" },
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
		{ "ToolTip", "How close a new loop edge needs to pass next to an existing vertex to use that vertex rather than creating a new one (used for plane cut)." },
	};
#endif
	const UE4CodeGen_Private::FDoublePropertyParams Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_VertexTolerance = { "VertexTolerance", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Double, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroupEdgeInsertionProperties, VertexTolerance), METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_VertexTolerance_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_VertexTolerance_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_InsertionMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_InsertionMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_bWireframe,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::NewProp_VertexTolerance,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroupEdgeInsertionProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::ClassParams = {
		&UGroupEdgeInsertionProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroupEdgeInsertionProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroupEdgeInsertionProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroupEdgeInsertionProperties, 2291800046);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UGroupEdgeInsertionProperties>()
	{
		return UGroupEdgeInsertionProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroupEdgeInsertionProperties(Z_Construct_UClass_UGroupEdgeInsertionProperties, &UGroupEdgeInsertionProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UGroupEdgeInsertionProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroupEdgeInsertionProperties);
	void UGroupEdgeInsertionOperatorFactory::StaticRegisterNativesUGroupEdgeInsertionOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_NoRegister()
	{
		return UGroupEdgeInsertionOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GroupEdgeInsertionTool.h" },
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::NewProp_Tool_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::NewProp_Tool = { "Tool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroupEdgeInsertionOperatorFactory, Tool), Z_Construct_UClass_UGroupEdgeInsertionTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::NewProp_Tool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::NewProp_Tool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::NewProp_Tool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroupEdgeInsertionOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::ClassParams = {
		&UGroupEdgeInsertionOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroupEdgeInsertionOperatorFactory, 1007357422);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UGroupEdgeInsertionOperatorFactory>()
	{
		return UGroupEdgeInsertionOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroupEdgeInsertionOperatorFactory(Z_Construct_UClass_UGroupEdgeInsertionOperatorFactory, &UGroupEdgeInsertionOperatorFactory::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UGroupEdgeInsertionOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroupEdgeInsertionOperatorFactory);
	void UGroupEdgeInsertionTool::StaticRegisterNativesUGroupEdgeInsertionTool()
	{
	}
	UClass* Z_Construct_UClass_UGroupEdgeInsertionTool_NoRegister()
	{
		return UGroupEdgeInsertionTool::StaticClass();
	}
	struct Z_Construct_UClass_UGroupEdgeInsertionTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Preview_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Preview;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Tool for inserting group edges into polygons of the mesh. */" },
		{ "IncludePath", "GroupEdgeInsertionTool.h" },
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
		{ "ToolTip", "Tool for inserting group edges into polygons of the mesh." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroupEdgeInsertionTool, Settings), Z_Construct_UClass_UGroupEdgeInsertionProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Preview_MetaData[] = {
		{ "ModuleRelativePath", "Public/GroupEdgeInsertionTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Preview = { "Preview", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UGroupEdgeInsertionTool, Preview), Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Preview_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Preview_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::NewProp_Preview,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGroupEdgeInsertionTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::ClassParams = {
		&UGroupEdgeInsertionTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGroupEdgeInsertionTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGroupEdgeInsertionTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGroupEdgeInsertionTool, 1656161086);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UGroupEdgeInsertionTool>()
	{
		return UGroupEdgeInsertionTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGroupEdgeInsertionTool(Z_Construct_UClass_UGroupEdgeInsertionTool, &UGroupEdgeInsertionTool::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UGroupEdgeInsertionTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGroupEdgeInsertionTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
