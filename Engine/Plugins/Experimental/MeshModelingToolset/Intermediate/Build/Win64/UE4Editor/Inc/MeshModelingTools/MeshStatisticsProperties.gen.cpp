// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Properties/MeshStatisticsProperties.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshStatisticsProperties() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshStatisticsProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshStatisticsProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
// End Cross Module References
	void UMeshStatisticsProperties::StaticRegisterNativesUMeshStatisticsProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshStatisticsProperties_NoRegister()
	{
		return UMeshStatisticsProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshStatisticsProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Mesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Mesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UV_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UV;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Attributes_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Attributes;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshStatisticsProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshStatisticsProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Properties/MeshStatisticsProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/MeshStatisticsProperties.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Mesh_MetaData[] = {
		{ "Category", "MeshStatistics" },
		{ "ModuleRelativePath", "Public/Properties/MeshStatisticsProperties.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Mesh = { "Mesh", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshStatisticsProperties, Mesh), METADATA_PARAMS(Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Mesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Mesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_UV_MetaData[] = {
		{ "Category", "MeshStatistics" },
		{ "ModuleRelativePath", "Public/Properties/MeshStatisticsProperties.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_UV = { "UV", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshStatisticsProperties, UV), METADATA_PARAMS(Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_UV_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_UV_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Attributes_MetaData[] = {
		{ "Category", "MeshStatistics" },
		{ "ModuleRelativePath", "Public/Properties/MeshStatisticsProperties.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Attributes = { "Attributes", nullptr, (EPropertyFlags)0x0010040000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshStatisticsProperties, Attributes), METADATA_PARAMS(Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Attributes_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Attributes_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshStatisticsProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Mesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_UV,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshStatisticsProperties_Statics::NewProp_Attributes,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshStatisticsProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshStatisticsProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshStatisticsProperties_Statics::ClassParams = {
		&UMeshStatisticsProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshStatisticsProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshStatisticsProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshStatisticsProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshStatisticsProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshStatisticsProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshStatisticsProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshStatisticsProperties, 461321079);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshStatisticsProperties>()
	{
		return UMeshStatisticsProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshStatisticsProperties(Z_Construct_UClass_UMeshStatisticsProperties, &UMeshStatisticsProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshStatisticsProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshStatisticsProperties);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
