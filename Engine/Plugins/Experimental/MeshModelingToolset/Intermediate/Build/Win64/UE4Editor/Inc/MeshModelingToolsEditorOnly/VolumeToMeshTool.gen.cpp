// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/VolumeToMeshTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeVolumeToMeshTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVolumeToMeshMode();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVolumeToMeshToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVolumeToMeshToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVolumeToMeshToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVolumeToMeshToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVolumeToMeshTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UVolumeToMeshTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveTool();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPreviewMesh_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AVolume_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_ULineSetComponent_NoRegister();
// End Cross Module References
	static UEnum* EVolumeToMeshMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVolumeToMeshMode, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EVolumeToMeshMode"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EVolumeToMeshMode>()
	{
		return EVolumeToMeshMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EVolumeToMeshMode(EVolumeToMeshMode_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EVolumeToMeshMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVolumeToMeshMode_Hash() { return 3727685472U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVolumeToMeshMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EVolumeToMeshMode"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EVolumeToMeshMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EVolumeToMeshMode::TriangulatePolygons", (int64)EVolumeToMeshMode::TriangulatePolygons },
				{ "EVolumeToMeshMode::MinimalPolygons", (int64)EVolumeToMeshMode::MinimalPolygons },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "MinimalPolygons.Comment", "/** Create PolyGroups based on UV Islands */" },
				{ "MinimalPolygons.Name", "EVolumeToMeshMode::MinimalPolygons" },
				{ "MinimalPolygons.ToolTip", "Create PolyGroups based on UV Islands" },
				{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
				{ "TriangulatePolygons.Comment", "/** Convert based on Angle Tolerance between Face Normals */" },
				{ "TriangulatePolygons.Name", "EVolumeToMeshMode::TriangulatePolygons" },
				{ "TriangulatePolygons.ToolTip", "Convert based on Angle Tolerance between Face Normals" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EVolumeToMeshMode",
				"EVolumeToMeshMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UVolumeToMeshToolBuilder::StaticRegisterNativesUVolumeToMeshToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UVolumeToMeshToolBuilder_NoRegister()
	{
		return UVolumeToMeshToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "VolumeToMeshTool.h" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVolumeToMeshToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics::ClassParams = {
		&UVolumeToMeshToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVolumeToMeshToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVolumeToMeshToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVolumeToMeshToolBuilder, 2544483907);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UVolumeToMeshToolBuilder>()
	{
		return UVolumeToMeshToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVolumeToMeshToolBuilder(Z_Construct_UClass_UVolumeToMeshToolBuilder, &UVolumeToMeshToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UVolumeToMeshToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVolumeToMeshToolBuilder);
	void UVolumeToMeshToolProperties::StaticRegisterNativesUVolumeToMeshToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UVolumeToMeshToolProperties_NoRegister()
	{
		return UVolumeToMeshToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UVolumeToMeshToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWeldEdges_MetaData[];
#endif
		static void NewProp_bWeldEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWeldEdges;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoRepair_MetaData[];
#endif
		static void NewProp_bAutoRepair_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoRepair;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOptimizeMesh_MetaData[];
#endif
		static void NewProp_bOptimizeMesh_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOptimizeMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowWireframe_MetaData[];
#endif
		static void NewProp_bShowWireframe_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowWireframe;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "VolumeToMeshTool.h" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bWeldEdges_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bWeldEdges_SetBit(void* Obj)
	{
		((UVolumeToMeshToolProperties*)Obj)->bWeldEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bWeldEdges = { "bWeldEdges", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVolumeToMeshToolProperties), &Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bWeldEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bWeldEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bWeldEdges_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bAutoRepair_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bAutoRepair_SetBit(void* Obj)
	{
		((UVolumeToMeshToolProperties*)Obj)->bAutoRepair = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bAutoRepair = { "bAutoRepair", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVolumeToMeshToolProperties), &Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bAutoRepair_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bAutoRepair_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bAutoRepair_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bOptimizeMesh_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bOptimizeMesh_SetBit(void* Obj)
	{
		((UVolumeToMeshToolProperties*)Obj)->bOptimizeMesh = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bOptimizeMesh = { "bOptimizeMesh", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVolumeToMeshToolProperties), &Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bOptimizeMesh_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bOptimizeMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bOptimizeMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData[] = {
		{ "Category", "Options" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	void Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bShowWireframe_SetBit(void* Obj)
	{
		((UVolumeToMeshToolProperties*)Obj)->bShowWireframe = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bShowWireframe = { "bShowWireframe", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UVolumeToMeshToolProperties), &Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bShowWireframe_SetBit, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bShowWireframe_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bWeldEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bAutoRepair,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bOptimizeMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::NewProp_bShowWireframe,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVolumeToMeshToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::ClassParams = {
		&UVolumeToMeshToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVolumeToMeshToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVolumeToMeshToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVolumeToMeshToolProperties, 176230649);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UVolumeToMeshToolProperties>()
	{
		return UVolumeToMeshToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVolumeToMeshToolProperties(Z_Construct_UClass_UVolumeToMeshToolProperties, &UVolumeToMeshToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UVolumeToMeshToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVolumeToMeshToolProperties);
	void UVolumeToMeshTool::StaticRegisterNativesUVolumeToMeshTool()
	{
	}
	UClass* Z_Construct_UClass_UVolumeToMeshTool_NoRegister()
	{
		return UVolumeToMeshTool::StaticClass();
	}
	struct Z_Construct_UClass_UVolumeToMeshTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Settings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Settings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewMesh_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewMesh;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetVolume_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_TargetVolume;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VolumeEdgesSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VolumeEdgesSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVolumeToMeshTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "VolumeToMeshTool.h" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_Settings_MetaData[] = {
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_Settings = { "Settings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVolumeToMeshTool, Settings), Z_Construct_UClass_UVolumeToMeshToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_Settings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_Settings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_PreviewMesh_MetaData[] = {
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_PreviewMesh = { "PreviewMesh", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVolumeToMeshTool, PreviewMesh), Z_Construct_UClass_UPreviewMesh_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_PreviewMesh_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_PreviewMesh_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_TargetVolume_MetaData[] = {
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_TargetVolume = { "TargetVolume", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVolumeToMeshTool, TargetVolume), Z_Construct_UClass_AVolume_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_TargetVolume_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_TargetVolume_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_VolumeEdgesSet_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/VolumeToMeshTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_VolumeEdgesSet = { "VolumeEdgesSet", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UVolumeToMeshTool, VolumeEdgesSet), Z_Construct_UClass_ULineSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_VolumeEdgesSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_VolumeEdgesSet_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UVolumeToMeshTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_Settings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_PreviewMesh,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_TargetVolume,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UVolumeToMeshTool_Statics::NewProp_VolumeEdgesSet,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVolumeToMeshTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVolumeToMeshTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVolumeToMeshTool_Statics::ClassParams = {
		&UVolumeToMeshTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UVolumeToMeshTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVolumeToMeshTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVolumeToMeshTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVolumeToMeshTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVolumeToMeshTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVolumeToMeshTool, 1690521683);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UVolumeToMeshTool>()
	{
		return UVolumeToMeshTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVolumeToMeshTool(Z_Construct_UClass_UVolumeToMeshTool, &UVolumeToMeshTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UVolumeToMeshTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVolumeToMeshTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
