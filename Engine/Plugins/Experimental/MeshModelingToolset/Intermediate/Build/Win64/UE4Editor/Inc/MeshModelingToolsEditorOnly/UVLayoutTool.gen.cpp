// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingToolsEditorOnly/Public/UVLayoutTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUVLayoutTool() {}
// Cross Module References
	MESHMODELINGTOOLSEDITORONLY_API UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVLayoutType();
	UPackage* Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutToolBuilder_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutToolProperties_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector2D();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutOperatorFactory_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutOperatorFactory();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutTool_NoRegister();
	MESHMODELINGTOOLSEDITORONLY_API UClass* Z_Construct_UClass_UUVLayoutTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UMultiSelectionTool();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UUVLayoutPreview_NoRegister();
// End Cross Module References
	static UEnum* EUVLayoutType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVLayoutType, Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly(), TEXT("EUVLayoutType"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLSEDITORONLY_API UEnum* StaticEnum<EUVLayoutType>()
	{
		return EUVLayoutType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUVLayoutType(EUVLayoutType_StaticEnum, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("EUVLayoutType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVLayoutType_Hash() { return 2828165696U; }
	UEnum* Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVLayoutType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUVLayoutType"), 0, Get_Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVLayoutType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUVLayoutType::Transform", (int64)EUVLayoutType::Transform },
				{ "EUVLayoutType::Stack", (int64)EUVLayoutType::Stack },
				{ "EUVLayoutType::Repack", (int64)EUVLayoutType::Repack },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
				{ "Repack.Name", "EUVLayoutType::Repack" },
				{ "Stack.Name", "EUVLayoutType::Stack" },
				{ "Transform.Name", "EUVLayoutType::Transform" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
				nullptr,
				"EUVLayoutType",
				"EUVLayoutType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UUVLayoutToolBuilder::StaticRegisterNativesUUVLayoutToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UUVLayoutToolBuilder_NoRegister()
	{
		return UUVLayoutToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UUVLayoutToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVLayoutToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "UVLayoutTool.h" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVLayoutToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVLayoutToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVLayoutToolBuilder_Statics::ClassParams = {
		&UUVLayoutToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVLayoutToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVLayoutToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVLayoutToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVLayoutToolBuilder, 2128334455);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UUVLayoutToolBuilder>()
	{
		return UUVLayoutToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVLayoutToolBuilder(Z_Construct_UClass_UUVLayoutToolBuilder, &UUVLayoutToolBuilder::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UUVLayoutToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVLayoutToolBuilder);
	void UUVLayoutToolProperties::StaticRegisterNativesUUVLayoutToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UUVLayoutToolProperties_NoRegister()
	{
		return UUVLayoutToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UUVLayoutToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_LayoutType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LayoutType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_LayoutType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TextureResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVScaleFactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_UVScaleFactor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVTranslate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_UVTranslate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAllowFlips_MetaData[];
#endif
		static void NewProp_bAllowFlips_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAllowFlips;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVLayoutToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutToolProperties_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Standard properties\n */" },
		{ "IncludePath", "UVLayoutTool.h" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Standard properties" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_LayoutType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_LayoutType_MetaData[] = {
		{ "Category", "UVLayout" },
		{ "Comment", "/** Type of transformation to apply to input UV islands */" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Type of transformation to apply to input UV islands" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_LayoutType = { "LayoutType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutToolProperties, LayoutType), Z_Construct_UEnum_MeshModelingToolsEditorOnly_EUVLayoutType, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_LayoutType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_LayoutType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_TextureResolution_MetaData[] = {
		{ "Category", "UVLayout" },
		{ "ClampMax", "4096" },
		{ "ClampMin", "2" },
		{ "Comment", "/** Expected resolution of output textures; controls spacing left between charts */" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Expected resolution of output textures; controls spacing left between charts" },
		{ "UIMax", "2048" },
		{ "UIMin", "64" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_TextureResolution = { "TextureResolution", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutToolProperties, TextureResolution), METADATA_PARAMS(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_TextureResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_TextureResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVScaleFactor_MetaData[] = {
		{ "Category", "UVLayout" },
		{ "ClampMax", "10000" },
		{ "ClampMin", "0.0001" },
		{ "Comment", "/** Apply this uniform scaling to the UVs after any layout recalculation */" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Apply this uniform scaling to the UVs after any layout recalculation" },
		{ "UIMax", "5.0" },
		{ "UIMin", "0.1" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVScaleFactor = { "UVScaleFactor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutToolProperties, UVScaleFactor), METADATA_PARAMS(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVScaleFactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVScaleFactor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVTranslate_MetaData[] = {
		{ "Category", "UVLayout" },
		{ "Comment", "/** Apply this 2D translation to the UVs after any layout recalculation, and after scaling */" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Apply this 2D translation to the UVs after any layout recalculation, and after scaling" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVTranslate = { "UVTranslate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutToolProperties, UVTranslate), Z_Construct_UScriptStruct_FVector2D, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVTranslate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVTranslate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_bAllowFlips_MetaData[] = {
		{ "Category", "UVLayout" },
		{ "Comment", "/** Allow the packer to flip the orientation of UV islands if it save space. May cause problems for downstream operations, not recommended. */" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Allow the packer to flip the orientation of UV islands if it save space. May cause problems for downstream operations, not recommended." },
	};
#endif
	void Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_bAllowFlips_SetBit(void* Obj)
	{
		((UUVLayoutToolProperties*)Obj)->bAllowFlips = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_bAllowFlips = { "bAllowFlips", nullptr, (EPropertyFlags)0x0010040000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UUVLayoutToolProperties), &Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_bAllowFlips_SetBit, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_bAllowFlips_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_bAllowFlips_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVLayoutToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_LayoutType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_LayoutType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_TextureResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVScaleFactor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_UVTranslate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutToolProperties_Statics::NewProp_bAllowFlips,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVLayoutToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVLayoutToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVLayoutToolProperties_Statics::ClassParams = {
		&UUVLayoutToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVLayoutToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVLayoutToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVLayoutToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVLayoutToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVLayoutToolProperties, 192118224);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UUVLayoutToolProperties>()
	{
		return UUVLayoutToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVLayoutToolProperties(Z_Construct_UClass_UUVLayoutToolProperties, &UUVLayoutToolProperties::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UUVLayoutToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVLayoutToolProperties);
	void UUVLayoutOperatorFactory::StaticRegisterNativesUUVLayoutOperatorFactory()
	{
	}
	UClass* Z_Construct_UClass_UUVLayoutOperatorFactory_NoRegister()
	{
		return UUVLayoutOperatorFactory::StaticClass();
	}
	struct Z_Construct_UClass_UUVLayoutOperatorFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tool_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tool;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n *  stores a pointer to the tool and enough info to know which specific operator it should spawn\n */" },
		{ "IncludePath", "UVLayoutTool.h" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Factory with enough info to spawn the background-thread Operator to do a chunk of work for the tool\n stores a pointer to the tool and enough info to know which specific operator it should spawn" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::NewProp_Tool_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::NewProp_Tool = { "Tool", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutOperatorFactory, Tool), Z_Construct_UClass_UUVLayoutTool_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::NewProp_Tool_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::NewProp_Tool_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::NewProp_Tool,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVLayoutOperatorFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::ClassParams = {
		&UUVLayoutOperatorFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVLayoutOperatorFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVLayoutOperatorFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVLayoutOperatorFactory, 3114064203);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UUVLayoutOperatorFactory>()
	{
		return UUVLayoutOperatorFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVLayoutOperatorFactory(Z_Construct_UClass_UUVLayoutOperatorFactory, &UUVLayoutOperatorFactory::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UUVLayoutOperatorFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVLayoutOperatorFactory);
	void UUVLayoutTool::StaticRegisterNativesUUVLayoutTool()
	{
	}
	UClass* Z_Construct_UClass_UUVLayoutTool_NoRegister()
	{
		return UUVLayoutTool::StaticClass();
	}
	struct Z_Construct_UClass_UUVLayoutTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BasicProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BasicProperties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MaterialSettings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Previews_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Previews_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Previews;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVLayoutView_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UVLayoutView;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUVLayoutTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMultiSelectionTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingToolsEditorOnly,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Simple Mesh Normal Updating Tool\n */" },
		{ "IncludePath", "UVLayoutTool.h" },
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
		{ "ToolTip", "Simple Mesh Normal Updating Tool" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_BasicProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_BasicProperties = { "BasicProperties", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutTool, BasicProperties), Z_Construct_UClass_UUVLayoutToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_BasicProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_BasicProperties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_MaterialSettings_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_MaterialSettings = { "MaterialSettings", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutTool, MaterialSettings), Z_Construct_UClass_UExistingMeshMaterialProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_MaterialSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_MaterialSettings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_Previews_Inner = { "Previews", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshOpPreviewWithBackgroundCompute_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_Previews_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_Previews = { "Previews", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutTool, Previews), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_Previews_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_Previews_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_UVLayoutView_MetaData[] = {
		{ "ModuleRelativePath", "Public/UVLayoutTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_UVLayoutView = { "UVLayoutView", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUVLayoutTool, UVLayoutView), Z_Construct_UClass_UUVLayoutPreview_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_UVLayoutView_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_UVLayoutView_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUVLayoutTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_BasicProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_MaterialSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_Previews_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_Previews,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUVLayoutTool_Statics::NewProp_UVLayoutView,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUVLayoutTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUVLayoutTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUVLayoutTool_Statics::ClassParams = {
		&UUVLayoutTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUVLayoutTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUVLayoutTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUVLayoutTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUVLayoutTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUVLayoutTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUVLayoutTool, 1169735251);
	template<> MESHMODELINGTOOLSEDITORONLY_API UClass* StaticClass<UUVLayoutTool>()
	{
		return UUVLayoutTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUVLayoutTool(Z_Construct_UClass_UUVLayoutTool, &UUVLayoutTool::StaticClass, TEXT("/Script/MeshModelingToolsEditorOnly"), TEXT("UUVLayoutTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUVLayoutTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
