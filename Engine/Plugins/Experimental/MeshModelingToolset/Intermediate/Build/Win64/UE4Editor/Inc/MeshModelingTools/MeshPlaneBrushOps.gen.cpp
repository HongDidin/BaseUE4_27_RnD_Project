// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Sculpting/MeshPlaneBrushOps.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshPlaneBrushOps() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBasePlaneBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UBasePlaneBrushOpProps();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshSculptBrushOpProps();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UPlaneBrushOpProps();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EPlaneBrushSideMode();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UViewAlignedPlaneBrushOpProps();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UFixedPlaneBrushOpProps_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UFixedPlaneBrushOpProps();
// End Cross Module References
	void UBasePlaneBrushOpProps::StaticRegisterNativesUBasePlaneBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UBasePlaneBrushOpProps_NoRegister()
	{
		return UBasePlaneBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UBasePlaneBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBasePlaneBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshSculptBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBasePlaneBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshPlaneBrushOps.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBasePlaneBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBasePlaneBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UBasePlaneBrushOpProps_Statics::ClassParams = {
		&UBasePlaneBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UBasePlaneBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBasePlaneBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBasePlaneBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UBasePlaneBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UBasePlaneBrushOpProps, 2440334850);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UBasePlaneBrushOpProps>()
	{
		return UBasePlaneBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UBasePlaneBrushOpProps(Z_Construct_UClass_UBasePlaneBrushOpProps, &UBasePlaneBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UBasePlaneBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBasePlaneBrushOpProps);
	void UPlaneBrushOpProps::StaticRegisterNativesUPlaneBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UPlaneBrushOpProps_NoRegister()
	{
		return UPlaneBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UPlaneBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_WhichSide_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WhichSide_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WhichSide;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPlaneBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBasePlaneBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshPlaneBrushOps.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Strength_MetaData[] = {
		{ "Category", "PlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of the Brush */" },
		{ "DisplayName", "Strength" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Strength of the Brush" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Strength = { "Strength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneBrushOpProps, Strength), METADATA_PARAMS(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Strength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "PlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "PlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "-1.0" },
		{ "Comment", "/** Depth of Brush into surface along surface normal */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Depth of Brush into surface along surface normal" },
		{ "UIMax", "0.5" },
		{ "UIMin", "-0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneBrushOpProps, Depth), METADATA_PARAMS(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Depth_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_WhichSide_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData[] = {
		{ "Category", "PlaneBrush" },
		{ "Comment", "/** Control whether effect of brush should be limited to one side of the Plane  */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Control whether effect of brush should be limited to one side of the Plane" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_WhichSide = { "WhichSide", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPlaneBrushOpProps, WhichSide), Z_Construct_UEnum_MeshModelingTools_EPlaneBrushSideMode, METADATA_PARAMS(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPlaneBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Falloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_Depth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_WhichSide_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPlaneBrushOpProps_Statics::NewProp_WhichSide,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPlaneBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPlaneBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPlaneBrushOpProps_Statics::ClassParams = {
		&UPlaneBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPlaneBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPlaneBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPlaneBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPlaneBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPlaneBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPlaneBrushOpProps, 788170414);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UPlaneBrushOpProps>()
	{
		return UPlaneBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPlaneBrushOpProps(Z_Construct_UClass_UPlaneBrushOpProps, &UPlaneBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UPlaneBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPlaneBrushOpProps);
	void UViewAlignedPlaneBrushOpProps::StaticRegisterNativesUViewAlignedPlaneBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_NoRegister()
	{
		return UViewAlignedPlaneBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_WhichSide_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WhichSide_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WhichSide;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBasePlaneBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshPlaneBrushOps.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Strength_MetaData[] = {
		{ "Category", "ViewPlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of the Brush */" },
		{ "DisplayName", "Strength" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Strength of the Brush" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Strength = { "Strength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UViewAlignedPlaneBrushOpProps, Strength), METADATA_PARAMS(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Strength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "ViewPlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UViewAlignedPlaneBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "ViewPlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "-1.0" },
		{ "Comment", "/** Depth of Brush into surface along view ray */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Depth of Brush into surface along view ray" },
		{ "UIMax", "0.5" },
		{ "UIMin", "-0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UViewAlignedPlaneBrushOpProps, Depth), METADATA_PARAMS(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Depth_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_WhichSide_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData[] = {
		{ "Category", "ViewPlaneBrush" },
		{ "Comment", "/** Control whether effect of brush should be limited to one side of the Plane  */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Control whether effect of brush should be limited to one side of the Plane" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_WhichSide = { "WhichSide", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UViewAlignedPlaneBrushOpProps, WhichSide), Z_Construct_UEnum_MeshModelingTools_EPlaneBrushSideMode, METADATA_PARAMS(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Falloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_Depth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_WhichSide_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::NewProp_WhichSide,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UViewAlignedPlaneBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::ClassParams = {
		&UViewAlignedPlaneBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UViewAlignedPlaneBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UViewAlignedPlaneBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UViewAlignedPlaneBrushOpProps, 4078413760);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UViewAlignedPlaneBrushOpProps>()
	{
		return UViewAlignedPlaneBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UViewAlignedPlaneBrushOpProps(Z_Construct_UClass_UViewAlignedPlaneBrushOpProps, &UViewAlignedPlaneBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UViewAlignedPlaneBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UViewAlignedPlaneBrushOpProps);
	void UFixedPlaneBrushOpProps::StaticRegisterNativesUFixedPlaneBrushOpProps()
	{
	}
	UClass* Z_Construct_UClass_UFixedPlaneBrushOpProps_NoRegister()
	{
		return UFixedPlaneBrushOpProps::StaticClass();
	}
	struct Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Strength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Strength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Falloff_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Falloff;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Depth_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Depth;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_WhichSide_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WhichSide_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_WhichSide;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBasePlaneBrushOpProps,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Sculpting/MeshPlaneBrushOps.h" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Strength_MetaData[] = {
		{ "Category", "FixedPlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Strength of the Brush */" },
		{ "DisplayName", "Strength" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Strength of the Brush" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Strength = { "Strength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFixedPlaneBrushOpProps, Strength), METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Strength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Strength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData[] = {
		{ "Category", "FixedPlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of falloff to apply */" },
		{ "DisplayName", "Falloff" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Amount of falloff to apply" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Falloff = { "Falloff", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFixedPlaneBrushOpProps, Falloff), METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Falloff_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Depth_MetaData[] = {
		{ "Category", "FixedPlaneBrush" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "-1.0" },
		{ "Comment", "/** Depth of Brush into surface relative to plane */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Depth of Brush into surface relative to plane" },
		{ "UIMax", "0.5" },
		{ "UIMin", "-0.5" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Depth = { "Depth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFixedPlaneBrushOpProps, Depth), METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Depth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Depth_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_WhichSide_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData[] = {
		{ "Category", "FixedPlaneBrush" },
		{ "Comment", "/** Control whether effect of brush should be limited to one side of the Plane  */" },
		{ "ModuleRelativePath", "Public/Sculpting/MeshPlaneBrushOps.h" },
		{ "ToolTip", "Control whether effect of brush should be limited to one side of the Plane" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_WhichSide = { "WhichSide", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFixedPlaneBrushOpProps, WhichSide), Z_Construct_UEnum_MeshModelingTools_EPlaneBrushSideMode, METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_WhichSide_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Strength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Falloff,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_Depth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_WhichSide_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::NewProp_WhichSide,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFixedPlaneBrushOpProps>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::ClassParams = {
		&UFixedPlaneBrushOpProps::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFixedPlaneBrushOpProps()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFixedPlaneBrushOpProps_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFixedPlaneBrushOpProps, 738093612);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UFixedPlaneBrushOpProps>()
	{
		return UFixedPlaneBrushOpProps::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFixedPlaneBrushOpProps(Z_Construct_UClass_UFixedPlaneBrushOpProps, &UFixedPlaneBrushOpProps::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UFixedPlaneBrushOpProps"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFixedPlaneBrushOpProps);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
