// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MODELINGCOMPONENTS_SpaceCurveDeformationMechanic_generated_h
#error "SpaceCurveDeformationMechanic.generated.h already included, missing '#pragma once' in SpaceCurveDeformationMechanic.h"
#endif
#define MODELINGCOMPONENTS_SpaceCurveDeformationMechanic_generated_h

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpaceCurveDeformationMechanicPropertySet(); \
	friend struct Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics; \
public: \
	DECLARE_CLASS(USpaceCurveDeformationMechanicPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USpaceCurveDeformationMechanicPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_INCLASS \
private: \
	static void StaticRegisterNativesUSpaceCurveDeformationMechanicPropertySet(); \
	friend struct Z_Construct_UClass_USpaceCurveDeformationMechanicPropertySet_Statics; \
public: \
	DECLARE_CLASS(USpaceCurveDeformationMechanicPropertySet, UInteractiveToolPropertySet, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USpaceCurveDeformationMechanicPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpaceCurveDeformationMechanicPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpaceCurveDeformationMechanicPropertySet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpaceCurveDeformationMechanicPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpaceCurveDeformationMechanicPropertySet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpaceCurveDeformationMechanicPropertySet(USpaceCurveDeformationMechanicPropertySet&&); \
	NO_API USpaceCurveDeformationMechanicPropertySet(const USpaceCurveDeformationMechanicPropertySet&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpaceCurveDeformationMechanicPropertySet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpaceCurveDeformationMechanicPropertySet(USpaceCurveDeformationMechanicPropertySet&&); \
	NO_API USpaceCurveDeformationMechanicPropertySet(const USpaceCurveDeformationMechanicPropertySet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpaceCurveDeformationMechanicPropertySet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpaceCurveDeformationMechanicPropertySet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpaceCurveDeformationMechanicPropertySet)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_67_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_70_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class USpaceCurveDeformationMechanicPropertySet>();

#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_SPARSE_DATA
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_RPC_WRAPPERS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpaceCurveDeformationMechanic(); \
	friend struct Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics; \
public: \
	DECLARE_CLASS(USpaceCurveDeformationMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USpaceCurveDeformationMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_INCLASS \
private: \
	static void StaticRegisterNativesUSpaceCurveDeformationMechanic(); \
	friend struct Z_Construct_UClass_USpaceCurveDeformationMechanic_Statics; \
public: \
	DECLARE_CLASS(USpaceCurveDeformationMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/ModelingComponents"), NO_API) \
	DECLARE_SERIALIZER(USpaceCurveDeformationMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpaceCurveDeformationMechanic(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpaceCurveDeformationMechanic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpaceCurveDeformationMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpaceCurveDeformationMechanic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpaceCurveDeformationMechanic(USpaceCurveDeformationMechanic&&); \
	NO_API USpaceCurveDeformationMechanic(const USpaceCurveDeformationMechanic&); \
public:


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpaceCurveDeformationMechanic() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpaceCurveDeformationMechanic(USpaceCurveDeformationMechanic&&); \
	NO_API USpaceCurveDeformationMechanic(const USpaceCurveDeformationMechanic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpaceCurveDeformationMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpaceCurveDeformationMechanic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USpaceCurveDeformationMechanic)


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PreviewGeometryActor() { return STRUCT_OFFSET(USpaceCurveDeformationMechanic, PreviewGeometryActor); } \
	FORCEINLINE static uint32 __PPO__RenderPoints() { return STRUCT_OFFSET(USpaceCurveDeformationMechanic, RenderPoints); } \
	FORCEINLINE static uint32 __PPO__RenderSegments() { return STRUCT_OFFSET(USpaceCurveDeformationMechanic, RenderSegments); } \
	FORCEINLINE static uint32 __PPO__PointTransformProxy() { return STRUCT_OFFSET(USpaceCurveDeformationMechanic, PointTransformProxy); } \
	FORCEINLINE static uint32 __PPO__PointTransformGizmo() { return STRUCT_OFFSET(USpaceCurveDeformationMechanic, PointTransformGizmo); }


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_93_PROLOG
#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_RPC_WRAPPERS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_INCLASS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_SPARSE_DATA \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h_96_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MODELINGCOMPONENTS_API UClass* StaticClass<class USpaceCurveDeformationMechanic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Experimental_MeshModelingToolset_Source_ModelingComponents_Public_Mechanics_SpaceCurveDeformationMechanic_h


#define FOREACH_ENUM_ESPACECURVECONTROLPOINTFALLOFFTYPE(op) \
	op(ESpaceCurveControlPointFalloffType::Linear) \
	op(ESpaceCurveControlPointFalloffType::Smooth) 

enum class ESpaceCurveControlPointFalloffType;
template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<ESpaceCurveControlPointFalloffType>();

#define FOREACH_ENUM_ESPACECURVECONTROLPOINTORIGINMODE(op) \
	op(ESpaceCurveControlPointOriginMode::Shared) \
	op(ESpaceCurveControlPointOriginMode::First) \
	op(ESpaceCurveControlPointOriginMode::Last) 

enum class ESpaceCurveControlPointOriginMode;
template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<ESpaceCurveControlPointOriginMode>();

#define FOREACH_ENUM_ESPACECURVECONTROLPOINTTRANSFORMMODE(op) \
	op(ESpaceCurveControlPointTransformMode::Shared) \
	op(ESpaceCurveControlPointTransformMode::PerVertex) 

enum class ESpaceCurveControlPointTransformMode;
template<> MODELINGCOMPONENTS_API UEnum* StaticEnum<ESpaceCurveControlPointTransformMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
