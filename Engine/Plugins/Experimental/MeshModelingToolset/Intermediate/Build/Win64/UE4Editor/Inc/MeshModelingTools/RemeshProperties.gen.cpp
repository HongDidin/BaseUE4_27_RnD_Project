// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Properties/RemeshProperties.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRemeshProperties() {}
// Cross Module References
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint();
	MESHMODELINGTOOLS_API UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshConstraintProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshConstraintProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_URemeshProperties();
// End Cross Module References
	static UEnum* EMaterialBoundaryConstraint_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMaterialBoundaryConstraint"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMaterialBoundaryConstraint>()
	{
		return EMaterialBoundaryConstraint_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMaterialBoundaryConstraint(EMaterialBoundaryConstraint_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMaterialBoundaryConstraint"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint_Hash() { return 3619444447U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMaterialBoundaryConstraint"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMaterialBoundaryConstraint::Fixed", (int64)EMaterialBoundaryConstraint::Fixed },
				{ "EMaterialBoundaryConstraint::Refine", (int64)EMaterialBoundaryConstraint::Refine },
				{ "EMaterialBoundaryConstraint::Free", (int64)EMaterialBoundaryConstraint::Free },
				{ "EMaterialBoundaryConstraint::Ignore", (int64)EMaterialBoundaryConstraint::Ignore },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Material Boundary Constraint Types */" },
				{ "Fixed.DisplayName", "Fixed" },
				{ "Fixed.Name", "EMaterialBoundaryConstraint::Fixed" },
				{ "Free.DisplayName", "Free" },
				{ "Free.Name", "EMaterialBoundaryConstraint::Free" },
				{ "Ignore.DisplayName", "Ignore" },
				{ "Ignore.Name", "EMaterialBoundaryConstraint::Ignore" },
				{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
				{ "Refine.DisplayName", "Refine" },
				{ "Refine.Name", "EMaterialBoundaryConstraint::Refine" },
				{ "ToolTip", "Material Boundary Constraint Types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMaterialBoundaryConstraint",
				"EMaterialBoundaryConstraint",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EGroupBoundaryConstraint_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EGroupBoundaryConstraint"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EGroupBoundaryConstraint>()
	{
		return EGroupBoundaryConstraint_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EGroupBoundaryConstraint(EGroupBoundaryConstraint_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EGroupBoundaryConstraint"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint_Hash() { return 1740102734U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EGroupBoundaryConstraint"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EGroupBoundaryConstraint::Fixed", (int64)EGroupBoundaryConstraint::Fixed },
				{ "EGroupBoundaryConstraint::Refine", (int64)EGroupBoundaryConstraint::Refine },
				{ "EGroupBoundaryConstraint::Free", (int64)EGroupBoundaryConstraint::Free },
				{ "EGroupBoundaryConstraint::Ignore", (int64)EGroupBoundaryConstraint::Ignore },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Group Boundary Constraint Types */" },
				{ "Fixed.DisplayName", "Fixed" },
				{ "Fixed.Name", "EGroupBoundaryConstraint::Fixed" },
				{ "Free.DisplayName", "Free" },
				{ "Free.Name", "EGroupBoundaryConstraint::Free" },
				{ "Ignore.DisplayName", "Ignore" },
				{ "Ignore.Name", "EGroupBoundaryConstraint::Ignore" },
				{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
				{ "Refine.DisplayName", "Refine" },
				{ "Refine.Name", "EGroupBoundaryConstraint::Refine" },
				{ "ToolTip", "Group Boundary Constraint Types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EGroupBoundaryConstraint",
				"EGroupBoundaryConstraint",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshBoundaryConstraint_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint, Z_Construct_UPackage__Script_MeshModelingTools(), TEXT("EMeshBoundaryConstraint"));
		}
		return Singleton;
	}
	template<> MESHMODELINGTOOLS_API UEnum* StaticEnum<EMeshBoundaryConstraint>()
	{
		return EMeshBoundaryConstraint_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshBoundaryConstraint(EMeshBoundaryConstraint_StaticEnum, TEXT("/Script/MeshModelingTools"), TEXT("EMeshBoundaryConstraint"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint_Hash() { return 629798321U; }
	UEnum* Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshModelingTools();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshBoundaryConstraint"), 0, Get_Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshBoundaryConstraint::Fixed", (int64)EMeshBoundaryConstraint::Fixed },
				{ "EMeshBoundaryConstraint::Refine", (int64)EMeshBoundaryConstraint::Refine },
				{ "EMeshBoundaryConstraint::Free", (int64)EMeshBoundaryConstraint::Free },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Mesh Boundary Constraint Types */" },
				{ "Fixed.DisplayName", "Fixed" },
				{ "Fixed.Name", "EMeshBoundaryConstraint::Fixed" },
				{ "Free.DisplayName", "Free" },
				{ "Free.Name", "EMeshBoundaryConstraint::Free" },
				{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
				{ "Refine.DisplayName", "Refine" },
				{ "Refine.Name", "EMeshBoundaryConstraint::Refine" },
				{ "ToolTip", "Mesh Boundary Constraint Types" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshModelingTools,
				nullptr,
				"EMeshBoundaryConstraint",
				"EMeshBoundaryConstraint",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshConstraintProperties::StaticRegisterNativesUMeshConstraintProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshConstraintProperties_NoRegister()
	{
		return UMeshConstraintProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshConstraintProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreserveSharpEdges_MetaData[];
#endif
		static void NewProp_bPreserveSharpEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreserveSharpEdges;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MeshBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MeshBoundaryConstraint;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_GroupBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GroupBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_GroupBoundaryConstraint;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MaterialBoundaryConstraint_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MaterialBoundaryConstraint_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MaterialBoundaryConstraint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreventNormalFlips_MetaData[];
#endif
		static void NewProp_bPreventNormalFlips_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreventNormalFlips;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshConstraintProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshConstraintProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Properties/RemeshProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreserveSharpEdges_MetaData[] = {
		{ "Category", "Remeshing|Edge Constraints" },
		{ "Comment", "/** If true, sharp edges are preserved  */" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "If true, sharp edges are preserved" },
	};
#endif
	void Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreserveSharpEdges_SetBit(void* Obj)
	{
		((UMeshConstraintProperties*)Obj)->bPreserveSharpEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreserveSharpEdges = { "bPreserveSharpEdges", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshConstraintProperties), &Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreserveSharpEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreserveSharpEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreserveSharpEdges_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MeshBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MeshBoundaryConstraint_MetaData[] = {
		{ "Category", "Remeshing|Boundary Constraints" },
		{ "Comment", "/** Mesh Boundary Constraint Type */" },
		{ "DisplayName", "Mesh Boundary" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Mesh Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MeshBoundaryConstraint = { "MeshBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshConstraintProperties, MeshBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EMeshBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MeshBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MeshBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_GroupBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_GroupBoundaryConstraint_MetaData[] = {
		{ "Category", "Remeshing|Boundary Constraints" },
		{ "Comment", "/** Group Boundary Constraint Type */" },
		{ "DisplayName", "Group Boundary" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Group Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_GroupBoundaryConstraint = { "GroupBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshConstraintProperties, GroupBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EGroupBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_GroupBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_GroupBoundaryConstraint_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MaterialBoundaryConstraint_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MaterialBoundaryConstraint_MetaData[] = {
		{ "Category", "Remeshing|Boundary Constraints" },
		{ "Comment", "/** Material Boundary Constraint Type */" },
		{ "DisplayName", "Material Boundary" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Material Boundary Constraint Type" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MaterialBoundaryConstraint = { "MaterialBoundaryConstraint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshConstraintProperties, MaterialBoundaryConstraint), Z_Construct_UEnum_MeshModelingTools_EMaterialBoundaryConstraint, METADATA_PARAMS(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MaterialBoundaryConstraint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MaterialBoundaryConstraint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreventNormalFlips_MetaData[] = {
		{ "Category", "Remeshing|Edge Constraints" },
		{ "Comment", "/** Prevent normal flips */" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Prevent normal flips" },
	};
#endif
	void Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreventNormalFlips_SetBit(void* Obj)
	{
		((UMeshConstraintProperties*)Obj)->bPreventNormalFlips = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreventNormalFlips = { "bPreventNormalFlips", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshConstraintProperties), &Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreventNormalFlips_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreventNormalFlips_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreventNormalFlips_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshConstraintProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreserveSharpEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MeshBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MeshBoundaryConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_GroupBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_GroupBoundaryConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MaterialBoundaryConstraint_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_MaterialBoundaryConstraint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshConstraintProperties_Statics::NewProp_bPreventNormalFlips,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshConstraintProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshConstraintProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshConstraintProperties_Statics::ClassParams = {
		&UMeshConstraintProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshConstraintProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshConstraintProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshConstraintProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshConstraintProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshConstraintProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshConstraintProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshConstraintProperties, 2006929265);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshConstraintProperties>()
	{
		return UMeshConstraintProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshConstraintProperties(Z_Construct_UClass_UMeshConstraintProperties, &UMeshConstraintProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshConstraintProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshConstraintProperties);
	void URemeshProperties::StaticRegisterNativesURemeshProperties()
	{
	}
	UClass* Z_Construct_UClass_URemeshProperties_NoRegister()
	{
		return URemeshProperties::StaticClass();
	}
	struct Z_Construct_UClass_URemeshProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SmoothingStrength_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_SmoothingStrength;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFlips_MetaData[];
#endif
		static void NewProp_bFlips_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFlips;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSplits_MetaData[];
#endif
		static void NewProp_bSplits_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSplits;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCollapses_MetaData[];
#endif
		static void NewProp_bCollapses_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCollapses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_URemeshProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshConstraintProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Properties/RemeshProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshProperties_Statics::NewProp_SmoothingStrength_MetaData[] = {
		{ "Category", "Remeshing" },
		{ "ClampMax", "1.0" },
		{ "ClampMin", "0.0" },
		{ "Comment", "/** Amount of Vertex Smoothing applied within Remeshing */" },
		{ "DisplayName", "Smoothing Rate" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Amount of Vertex Smoothing applied within Remeshing" },
		{ "UIMax", "1.0" },
		{ "UIMin", "0.0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_URemeshProperties_Statics::NewProp_SmoothingStrength = { "SmoothingStrength", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(URemeshProperties, SmoothingStrength), METADATA_PARAMS(Z_Construct_UClass_URemeshProperties_Statics::NewProp_SmoothingStrength_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshProperties_Statics::NewProp_SmoothingStrength_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshProperties_Statics::NewProp_bFlips_MetaData[] = {
		{ "Category", "Remeshing|Edge Constraints" },
		{ "Comment", "/** Enable edge flips in Remeshing (tends to lose edges/detail) */" },
		{ "DisplayName", "Allow Flips" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Enable edge flips in Remeshing (tends to lose edges/detail)" },
	};
#endif
	void Z_Construct_UClass_URemeshProperties_Statics::NewProp_bFlips_SetBit(void* Obj)
	{
		((URemeshProperties*)Obj)->bFlips = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshProperties_Statics::NewProp_bFlips = { "bFlips", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshProperties), &Z_Construct_UClass_URemeshProperties_Statics::NewProp_bFlips_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshProperties_Statics::NewProp_bFlips_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshProperties_Statics::NewProp_bFlips_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshProperties_Statics::NewProp_bSplits_MetaData[] = {
		{ "Category", "Remeshing|Edge Constraints" },
		{ "Comment", "/** Enable edge splits in Remeshing */" },
		{ "DisplayName", "Allow Splits" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Enable edge splits in Remeshing" },
	};
#endif
	void Z_Construct_UClass_URemeshProperties_Statics::NewProp_bSplits_SetBit(void* Obj)
	{
		((URemeshProperties*)Obj)->bSplits = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshProperties_Statics::NewProp_bSplits = { "bSplits", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshProperties), &Z_Construct_UClass_URemeshProperties_Statics::NewProp_bSplits_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshProperties_Statics::NewProp_bSplits_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshProperties_Statics::NewProp_bSplits_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_URemeshProperties_Statics::NewProp_bCollapses_MetaData[] = {
		{ "Category", "Remeshing|Edge Constraints" },
		{ "Comment", "/** Enable edge collapses in Remeshing */" },
		{ "DisplayName", "Allow Collapses" },
		{ "ModuleRelativePath", "Public/Properties/RemeshProperties.h" },
		{ "ToolTip", "Enable edge collapses in Remeshing" },
	};
#endif
	void Z_Construct_UClass_URemeshProperties_Statics::NewProp_bCollapses_SetBit(void* Obj)
	{
		((URemeshProperties*)Obj)->bCollapses = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_URemeshProperties_Statics::NewProp_bCollapses = { "bCollapses", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(URemeshProperties), &Z_Construct_UClass_URemeshProperties_Statics::NewProp_bCollapses_SetBit, METADATA_PARAMS(Z_Construct_UClass_URemeshProperties_Statics::NewProp_bCollapses_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshProperties_Statics::NewProp_bCollapses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_URemeshProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshProperties_Statics::NewProp_SmoothingStrength,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshProperties_Statics::NewProp_bFlips,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshProperties_Statics::NewProp_bSplits,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_URemeshProperties_Statics::NewProp_bCollapses,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_URemeshProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<URemeshProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_URemeshProperties_Statics::ClassParams = {
		&URemeshProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_URemeshProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_URemeshProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_URemeshProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_URemeshProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_URemeshProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_URemeshProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(URemeshProperties, 2703702496);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<URemeshProperties>()
	{
		return URemeshProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_URemeshProperties(Z_Construct_UClass_URemeshProperties, &URemeshProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("URemeshProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(URemeshProperties);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
