// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshModelingTools/Public/Properties/MeshAnalysisProperties.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshAnalysisProperties() {}
// Cross Module References
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAnalysisProperties_NoRegister();
	MESHMODELINGTOOLS_API UClass* Z_Construct_UClass_UMeshAnalysisProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_MeshModelingTools();
// End Cross Module References
	void UMeshAnalysisProperties::StaticRegisterNativesUMeshAnalysisProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshAnalysisProperties_NoRegister()
	{
		return UMeshAnalysisProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshAnalysisProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SurfaceArea_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SurfaceArea;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Volume_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Volume;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshAnalysisProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshModelingTools,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAnalysisProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Properties/MeshAnalysisProperties.h" },
		{ "ModuleRelativePath", "Public/Properties/MeshAnalysisProperties.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_SurfaceArea_MetaData[] = {
		{ "Category", "MeshAnalysis" },
		{ "ModuleRelativePath", "Public/Properties/MeshAnalysisProperties.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_SurfaceArea = { "SurfaceArea", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshAnalysisProperties, SurfaceArea), METADATA_PARAMS(Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_SurfaceArea_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_SurfaceArea_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_Volume_MetaData[] = {
		{ "Category", "MeshAnalysis" },
		{ "ModuleRelativePath", "Public/Properties/MeshAnalysisProperties.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_Volume = { "Volume", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshAnalysisProperties, Volume), METADATA_PARAMS(Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_Volume_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_Volume_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshAnalysisProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_SurfaceArea,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshAnalysisProperties_Statics::NewProp_Volume,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshAnalysisProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshAnalysisProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshAnalysisProperties_Statics::ClassParams = {
		&UMeshAnalysisProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshAnalysisProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAnalysisProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshAnalysisProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshAnalysisProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshAnalysisProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshAnalysisProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshAnalysisProperties, 1073251777);
	template<> MESHMODELINGTOOLS_API UClass* StaticClass<UMeshAnalysisProperties>()
	{
		return UMeshAnalysisProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshAnalysisProperties(Z_Construct_UClass_UMeshAnalysisProperties, &UMeshAnalysisProperties::StaticClass, TEXT("/Script/MeshModelingTools"), TEXT("UMeshAnalysisProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshAnalysisProperties);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
