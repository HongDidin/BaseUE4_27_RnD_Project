// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ModelingComponents/Public/Selection/PolygonSelectionMechanic.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePolygonSelectionMechanic() {}
// Cross Module References
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanicProperties_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanicProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolPropertySet();
	UPackage* Z_Construct_UPackage__Script_ModelingComponents();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UPolygonSelectionMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractionMechanic();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_APreviewGeometryActor_NoRegister();
	MODELINGCOMPONENTS_API UClass* Z_Construct_UClass_UTriangleSetComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	void UPolygonSelectionMechanicProperties::StaticRegisterNativesUPolygonSelectionMechanicProperties()
	{
	}
	UClass* Z_Construct_UClass_UPolygonSelectionMechanicProperties_NoRegister()
	{
		return UPolygonSelectionMechanicProperties::StaticClass();
	}
	struct Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectFaces_MetaData[];
#endif
		static void NewProp_bSelectFaces_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectFaces;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectEdges_MetaData[];
#endif
		static void NewProp_bSelectEdges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectEdges;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectVertices_MetaData[];
#endif
		static void NewProp_bSelectVertices_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectVertices;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectEdgeLoops_MetaData[];
#endif
		static void NewProp_bSelectEdgeLoops_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectEdgeLoops;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectEdgeRings_MetaData[];
#endif
		static void NewProp_bSelectEdgeRings_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectEdgeRings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPreferProjectedElement_MetaData[];
#endif
		static void NewProp_bPreferProjectedElement_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPreferProjectedElement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectDownRay_MetaData[];
#endif
		static void NewProp_bSelectDownRay_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectDownRay;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIgnoreOcclusion_MetaData[];
#endif
		static void NewProp_bIgnoreOcclusion_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIgnoreOcclusion;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolPropertySet,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Selection/PolygonSelectionMechanic.h" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectFaces_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectFaces_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bSelectFaces = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectFaces = { "bSelectFaces", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectFaces_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectFaces_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectFaces_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdges_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdges_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bSelectEdges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdges = { "bSelectEdges", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdges_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdges_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectVertices_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectVertices_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bSelectVertices = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectVertices = { "bSelectVertices", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectVertices_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectVertices_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectVertices_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeLoops_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "Comment", "/** When true, will select edge loops. Edge loops are paths along a string of valence-4 vertices. */" },
		{ "EditCondition", "bSelectEdges" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
		{ "ToolTip", "When true, will select edge loops. Edge loops are paths along a string of valence-4 vertices." },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeLoops_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bSelectEdgeLoops = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeLoops = { "bSelectEdgeLoops", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeLoops_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeLoops_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeLoops_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeRings_MetaData[] = {
		{ "Category", "SelectionFilter" },
		{ "Comment", "/** When true, will select rings of edges that are opposite each other across a quad face. */" },
		{ "EditCondition", "bSelectEdges" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
		{ "ToolTip", "When true, will select rings of edges that are opposite each other across a quad face." },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeRings_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bSelectEdgeRings = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeRings = { "bSelectEdgeRings", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeRings_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeRings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeRings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bPreferProjectedElement_MetaData[] = {
		{ "Category", "SelectionFilter|Ortho Viewport Behavior" },
		{ "Comment", "/** Prefer to select an edge projected to a point rather than the point, or a face projected to an edge rather than the edge. */" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
		{ "ToolTip", "Prefer to select an edge projected to a point rather than the point, or a face projected to an edge rather than the edge." },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bPreferProjectedElement_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bPreferProjectedElement = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bPreferProjectedElement = { "bPreferProjectedElement", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bPreferProjectedElement_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bPreferProjectedElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bPreferProjectedElement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectDownRay_MetaData[] = {
		{ "Category", "SelectionFilter|Ortho Viewport Behavior" },
		{ "Comment", "/** If the closest element is valid, select other elements behind it that are aligned with it. */" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
		{ "ToolTip", "If the closest element is valid, select other elements behind it that are aligned with it." },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectDownRay_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bSelectDownRay = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectDownRay = { "bSelectDownRay", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectDownRay_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectDownRay_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectDownRay_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bIgnoreOcclusion_MetaData[] = {
		{ "Category", "SelectionFilter|Ortho Viewport Behavior" },
		{ "Comment", "/** Do not check whether the closest element is occluded from the current view. */" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
		{ "ToolTip", "Do not check whether the closest element is occluded from the current view." },
	};
#endif
	void Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bIgnoreOcclusion_SetBit(void* Obj)
	{
		((UPolygonSelectionMechanicProperties*)Obj)->bIgnoreOcclusion = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bIgnoreOcclusion = { "bIgnoreOcclusion", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UPolygonSelectionMechanicProperties), &Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bIgnoreOcclusion_SetBit, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bIgnoreOcclusion_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bIgnoreOcclusion_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectFaces,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdges,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectVertices,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeLoops,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectEdgeRings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bPreferProjectedElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bSelectDownRay,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::NewProp_bIgnoreOcclusion,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolygonSelectionMechanicProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::ClassParams = {
		&UPolygonSelectionMechanicProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolygonSelectionMechanicProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolygonSelectionMechanicProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolygonSelectionMechanicProperties, 1380090888);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UPolygonSelectionMechanicProperties>()
	{
		return UPolygonSelectionMechanicProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolygonSelectionMechanicProperties(Z_Construct_UClass_UPolygonSelectionMechanicProperties, &UPolygonSelectionMechanicProperties::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UPolygonSelectionMechanicProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolygonSelectionMechanicProperties);
	void UPolygonSelectionMechanic::StaticRegisterNativesUPolygonSelectionMechanic()
	{
	}
	UClass* Z_Construct_UClass_UPolygonSelectionMechanic_NoRegister()
	{
		return UPolygonSelectionMechanic::StaticClass();
	}
	struct Z_Construct_UClass_UPolygonSelectionMechanic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Properties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Properties;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewGeometryActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewGeometryActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DrawnTriangleSetComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DrawnTriangleSetComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HighlightedFaceMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_HighlightedFaceMaterial;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPolygonSelectionMechanic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractionMechanic,
		(UObject* (*)())Z_Construct_UPackage__Script_ModelingComponents,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanic_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * UPolygonSelectionMechanic implements the interaction for selecting a set of faces/vertices/edges\n * from a FGroupTopology on a USimpleDynamicMeshComponent. \n */" },
		{ "IncludePath", "Selection/PolygonSelectionMechanic.h" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
		{ "ToolTip", "UPolygonSelectionMechanic implements the interaction for selecting a set of faces/vertices/edges\nfrom a FGroupTopology on a USimpleDynamicMeshComponent." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_Properties_MetaData[] = {
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_Properties = { "Properties", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonSelectionMechanic, Properties), Z_Construct_UClass_UPolygonSelectionMechanicProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_Properties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_Properties_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_PreviewGeometryActor_MetaData[] = {
		{ "Comment", "/** The actor we create internally to own the DrawnTriangleSetComponent */" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
		{ "ToolTip", "The actor we create internally to own the DrawnTriangleSetComponent" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_PreviewGeometryActor = { "PreviewGeometryActor", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonSelectionMechanic, PreviewGeometryActor), Z_Construct_UClass_APreviewGeometryActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_PreviewGeometryActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_PreviewGeometryActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_DrawnTriangleSetComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_DrawnTriangleSetComponent = { "DrawnTriangleSetComponent", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonSelectionMechanic, DrawnTriangleSetComponent), Z_Construct_UClass_UTriangleSetComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_DrawnTriangleSetComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_DrawnTriangleSetComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_HighlightedFaceMaterial_MetaData[] = {
		{ "ModuleRelativePath", "Public/Selection/PolygonSelectionMechanic.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_HighlightedFaceMaterial = { "HighlightedFaceMaterial", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPolygonSelectionMechanic, HighlightedFaceMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_HighlightedFaceMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_HighlightedFaceMaterial_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPolygonSelectionMechanic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_Properties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_PreviewGeometryActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_DrawnTriangleSetComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPolygonSelectionMechanic_Statics::NewProp_HighlightedFaceMaterial,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPolygonSelectionMechanic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPolygonSelectionMechanic>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPolygonSelectionMechanic_Statics::ClassParams = {
		&UPolygonSelectionMechanic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPolygonSelectionMechanic_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPolygonSelectionMechanic_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPolygonSelectionMechanic()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPolygonSelectionMechanic_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPolygonSelectionMechanic, 2116586923);
	template<> MODELINGCOMPONENTS_API UClass* StaticClass<UPolygonSelectionMechanic>()
	{
		return UPolygonSelectionMechanic::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPolygonSelectionMechanic(Z_Construct_UClass_UPolygonSelectionMechanic, &UPolygonSelectionMechanic::StaticClass, TEXT("/Script/ModelingComponents"), TEXT("UPolygonSelectionMechanic"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPolygonSelectionMechanic);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
